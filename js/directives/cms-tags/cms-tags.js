app.directive('cmsInput', [function () {
    return {
        restrict: 'E',
    transclude: true,
    templateUrl:'js/directives/cms-input/cms-input.html',
    scope:{
        labelInput:'@cmsInputLabel',
        defaultValue:'@cmsInputDefault',
        inputModel:'=cmsInputModel',
        inputType:'@cmsInputType',
        regionInputElement:'@cmsInputRegion',
        idInputElement:'@cmsInputId'
    },
        link: function (scope, iElement, iAttrs) {
            
        }
    };
}])