app.directive('cmsInput', [function () {
    return {
        restrict: 'E',
    transclude: true,
    templateUrl:'js/directives/cms-input/cms-input.html',
    scope:{
        labelInput:'@cmsInputLabel',
        defaultValue:'@cmsInputDefault',
        defaultModel:'@cmsInputDefaultModel',
        inputModel:'=cmsInputModel',
        inputType:'@cmsInputType',
        regionInputElement:'@cmsInputRegion',
        display:'@cmsInputDisplay' ,
        idInputElement:'@cmsInputId'
    },
        link: function (scope, iElement, iAttrs) {
            
        }
    };
}])