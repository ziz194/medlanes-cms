app.directive('cmsPicture', [function () {
	return {
		restrict: 'E',
    transclude: true,
    templateUrl:'js/directives/cms-picture/cms-picture.html',
    scope:{
    	labelInput:'@cmsInputLabel',
        defaultValue:'=selected',
    	inputModel:'=cmsInputModel',
        inputType:'@cmsInputType',
        regionInputElement:'@cmsInputRegion',
    	idInputElement:'@cmsInputId'
    },
		link: function (scope, iElement, iAttrs) {
			
		}
	};
}])