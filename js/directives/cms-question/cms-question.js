app.directive('cmsQuestion', [function () {
	return {
		restrict: 'E',
    transclude: true,
    templateUrl:'js/directives/cms-question/cms-question.html',
    scope:{
    	labelInput:'@cmsInputLabel',
        defaultValue:'@cmsInputDefault',
    	inputModel:'=cmsInputModel',
        inputType:'@cmsInputType',
        regionInputElement:'@cmsInputRegion',
        iterationsInputElement:'@cmsInputIterations',
    	idInputElement:'@cmsInputId'
    },
		link: function (scope, iElement, iAttrs) {
			
		}
	};
}])