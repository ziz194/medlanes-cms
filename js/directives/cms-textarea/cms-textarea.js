app.directive('cmsTextarea', [function () {
	return {
		restrict: 'E',
    transclude: true,
    templateUrl:'js/directives/cms-textarea/cms-textarea.html',
    scope:{
    	labelInput:'@cmsInputLabel',
        defaultValue:'@cmsInputDefault',
    	inputModel:'=cmsInputModel',
        regionInputElement:'@cmsInputRegion',
    	idInputElement:'@cmsInputId'
    },
		link: function (scope, iElement, iAttrs) {
			
		}
	};
}])