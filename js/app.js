
var app = angular.module('cmsApp',[
'ngRoute',
//Login
'LoginCtrl',
//Settings Controller
'SettingsCtrl',
//Settings Controller
'UsersCtrl',
//Edit Article Conttroler 
'EditArticleCtrl',
//Language
'LanguageCtrl',
//Page
'PageCtrl',
//Templates
'TemplateCtrl',
//Assets
'AssetsCtrl',
//Elements
'ElementCtrl',
//Dashboard
'DashCtrl',
//PagesCRUD
'PagesCrudSrvc',
//ElementsCRUD
'ElementsCrudSrvc',
//UsersCRUD
'UsersCrudSrvc',
//TemplatesCRUD
'TemplatesCrudSrvc',
//ContentsCRUD
'ContentsCrudSrvc',
//AssetsCRUD
'AssetsCrudSrvc',
//LanguagesCRUD
'LanguagesCrudSrvc',
//BrandsCRUD
'BrandsCrudSrvc',
//splitter
'splitter',
//Modal Controller
'ModalCtrl',
//Modal Instance Controller
'ModalInstanceCtrl',
//Modal Instance Controller
'DeleteModalCtrl',
//Modal Delete Asset
'DeleteAssetModalCtrl',
//Modal Edit Asset
'EditAssetModalCtrl',
//Modal Delete Element
'DeleteElementModalCtrl',
//UI Bootstrap
'ui.bootstrap',
//Form Data Module
'FormDataModule',
//Pagination
'angularUtils.directives.dirPagination',
//ng Input Tags
'ngTagsInput',
// Chart.js
'chart.js',
//AuthService
'AuthSrvc'
]);
 
 app.run(function(){
 
 });

 
 //This will handle all of our routing
app.config(function($routeProvider, $locationProvider){
 
$routeProvider.when('/',{
templateUrl:'js/templates/login.html',
controller:'LoginController'
});


$routeProvider.when('/dashboard',{
templateUrl:'js/templates/dashboard.html',
controller:'DashboardController'
});


$routeProvider.when('/elements',{
templateUrl:'js/templates/elements.html',
controller:'ElementController'
});



$routeProvider.when('/templates',{
            templateUrl:'js/templates/templates.html',
            controller:'TemplatesController'
        });

$routeProvider.when('/assets',{
            templateUrl:'js/templates/assets.html',
            controller:'AssetsController'
        });

$routeProvider.when('/settings',{
            templateUrl:'js/templates/settings.html',
            controller:'SettingsController'
        });

$routeProvider.when('/users',{
            templateUrl:'js/templates/users.html',
            controller:'UsersController'
        });

$routeProvider.when('/account',{
            templateUrl:'js/templates/account.html',
            controller:'UsersController'
        });
 
});






