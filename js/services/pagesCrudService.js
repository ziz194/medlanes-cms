
var crud = angular.module('PagesCrudSrvc',[]);
 
crud.factory("PagesCRUD",function($http){
 
    return{
        all: function(){
          //get all posts
          var request = $http({method:'GET', url:'api/pages'});
           return request;
        },
        getTags: function(){
          //get all posts
          var request = $http({method:'GET', url:'api/tags'});
           return request;
        },
  
        create: function(data,html){
          //create a new post
           var request = $http({method:'GET', url:'api/pages/create',params:data});
            return request;
        },
        createDraft: function(data,html){
          //create a new post
           var request = $http({method:'POST', url:'api/pages/createDraft',params:data});
            return request;
        },
        get: function(id){
          //get a specific post
            var request = $http.get('api/pages/'+id);
            return request;
        },
        pageLanguageUrl: function(idPage,idLanguage){
            var request = $http.get('api/pages/findLanguageUrl/'+idPage+'/'+idLanguage);
            return request;
        },
        getPageTemplate: function(idPage){

            var request = $http.get('api/pages/getPageTemplate/'+idPage);

            return request;
        },
        getPageUrl: function(idPage,idLanguage){

            var request = $http.get('api/pages/getPageUrl/'+idPage+'/'+idLanguage);

            return request;
        },
        getPagesbyBrandAndLanguage: function(idBrand,idLanguage){

            var request = $http.get('api/pages/getPagesbyBrandAndLanguage/'+idBrand+'/'+idLanguage);
            return request;
        },
        getPagesbyLanguage: function(idLanguage){

            var request = $http.get('api/pages/getPagesbyLanguage/'+idLanguage);
            return request;
        },

        update: function(id,data){

            var request = $http({method:'GET', url:'api/pages/updatePage/'+id,params:data});
            return request;
        },
        activatePage: function(id,data){

            var request = $http({method:'GET', url:'api/pages/activatePage/'+id,params:data});
            return request;
        },
        deactivatePage: function(id,data){

            var request = $http({method:'GET', url:'api/pages/deactivatePage/'+id,params:data});
            return request;
        },
        delete: function(id){
          //delete a specific post

           var request = $http({method:'DELETE', url:'api/pages/'+id});
           return request;
        }
    }
 
});