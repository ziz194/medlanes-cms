
var crud = angular.module('UsersCrudSrvc',[]);
 
crud.factory("UsersCRUD",function($http){
 
    return{
        all: function(){
          //get all users
          var request = $http({method:'GET', url:'api/users'});
           return request;
        },
        create: function(data){
          //create a new user
           var request = $http({method:'GET', url:'api/users/create',params:data});
            return request;
        },
        get: function(id){
          //get a specific user
            var request = $http.get('api/users/'+id);
            return request;
        },
        pageCount: function(id){
          //get a specific user
            var request = $http.get('api/users/pageCount/'+id);
            return request;
        },
        update: function(id,data){
          //update a specific user
            var request = $http.put('api/users/'+id,data);
            return request;
        },
        delete: function(id){
          
           var request = $http({method:'DELETE', url:'api/users/'+id});
           return request;
        },
        getPagesByDate: function(startDate,endDate){
          
           var request =$http.get('api/pages/getPagesByDate/'+startDate+'/'+endDate+'/');
           return request;
        },
        getTotalPagesByDate: function(startDate,endDate){
          
           var request =$http.get('api/pages/getTotalPagesByDate/'+startDate+'/'+endDate+'/');
           return request;
        },
        getElementsByDate: function(startDate,endDate){
          
           var request =$http.get('api/elements/getElementsByDate/'+startDate+'/'+endDate+'/');
           return request;
        },
        getTotalElementsByDate: function(startDate,endDate){
          
           var request =$http.get('api/elements/getTotalElementsByDate/'+startDate+'/'+endDate+'/');
           return request;
        },
        getAssetsByDate: function(startDate,endDate){
          
           var request =$http.get('api/assets/getAssetsByDate/'+startDate+'/'+endDate+'/');
           return request;
        },
        getTotalAssetsByDate: function(startDate,endDate){
          
           var request =$http.get('api/assets/getTotalAssetsByDate/'+startDate+'/'+endDate+'/');
           return request;
        },
        getTotalElementsNumber: function(){
          
           var request =$http.get('api/elements/getTotalElementsNumber/1');
           return request;
        },
        getTotalAssetsNumber: function(){
          
           var request =$http.get('api/assets/getTotalAssetsNumber/1');
           return request;
        }
    }
 
});