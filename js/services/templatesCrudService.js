
var crud = angular.module('TemplatesCrudSrvc',[]);
 
crud.factory("TemplatesCRUD",function($http){
 
    return{
         getTemplatesb: function(idBrand,idLanguage){
            var request = $http.get('api/templates/getTemplatesbyBrandAndLanguage/'+idBrand+'/'+idLanguage);
            return request;
        },
        all: function(){
          //get all posts
          var request = $http({method:'GET', url:'api/templates'});
           return request;
        },
             getTemplateByLanguage: function(idBrand,idLanguage){
          //get all posts
            var request = $http.get('api/templates/getTemplatesbyBrandAndLanguage/'+idBrand+'/'+idLanguage);
           return request;
        },

          getPagesTemplateByLanguage: function(id){
          //get all posts
            var request = $http.get('api/templates/getPagesTemplateByLanguage/'+id);
           return request;
        },
              getElementsTemplateByLanguage: function(id){
          //get all posts
            var request = $http.get('api/templates/getElementsTemplateByLanguage/'+id);
           return request;
        },

              create: function(data){

         
         var fd = new FormData();
         fd.append('language', data.languageId);
         fd.append('brand', data.brandId);
         fd.append('file', data.templateFile);
         fd.append('name', data.templateName);
         fd.append('inputsFile', data.inputsFile);
         fd.append('typeTemplate', data.typeTemplate);

         var request = $http.post('api/templates/addTemplate', fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
         });
                     return request;

     
    },

      getContent: function(id){
            var request = $http.get('api/templates/getContent/'+id);
            return request;
        },



        get: function(id){
            var request = $http.get('api/templates/'+id);
            return request;
        },
        
        update: function(id,data){
          //update a specific post
                         var fd = new FormData();
                           fd.append('id', id);
                           fd.append('file', data.urlTemplate);
                           fd.append('name', data.nameTemplate);
                           fd.append('inputsFile', data.inputsTemplate);
                           fd.append('typeTemplate', data.typeTemplate);


         var request = $http.post('api/templates/updateTemplate', fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
         });
                     return request;


        },
        delete: function(id){
          //delete a specific post

           var request = $http({method:'DELETE', url:'api/templates/'+id});
           return request;
        }
    }
 
});