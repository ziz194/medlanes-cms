
var crud = angular.module('AssetsCrudSrvc',[]);
 
crud.factory("AssetsCRUD",function($http,FrmData){
 
    return{
        all: function(){
          //get all posts
          var request = $http({method:'GET', url:'api/assets'});
           return request;
        },
        create: function(data){

         
         var fd = new FormData();
         fd.append('file', data.assetFile);
         fd.append('name', data.assetName);
         fd.append('description', data.assetDescription);

         var request = $http.post('api/assets/uploadAsset', fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
         });
                     return request;

     
    },


        get: function(id){
          //get a specific content
            var request = $http.get('api/assets/'+id);
            return request;
        },


        update: function(data){


         var fd = new FormData();
         fd.append('file', data.assetFile);
         fd.append('id', data.idAsset);
         fd.append('name', data.nameAsset);
         fd.append('description', data.descriptionAsset);

         var request = $http.post('api/assets/updateAsset/', fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
         });
            return request;

        },
        delete: function(id){

            var request = $http({method:'DELETE', url:'api/assets/'+id});
           return request;


        }
    }
 
});