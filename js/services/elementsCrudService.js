
var crud = angular.module('ElementsCrudSrvc',[]);
 
crud.factory("ElementsCRUD",function($http){
 
    return{
        all: function(){
          //get all posts
          var request = $http({method:'GET', url:'api/elements'});
           return request;
        },
        create: function(data,html){
          //create a new post
           var request = $http({method:'GET', url:'api/elements/create',params:data});
            return request;
        },
        get: function(id){
          //get a specific post
            var request = $http.get('api/elements/'+id);
            return request;
        },
        getElementUrl: function(idElement){

            var request = $http.get('api/elements/getElementUrl/'+idElement);

            return request;
        },
         getElementTemplate: function(idTemplate){

            var request = $http.get('api/elements/getElementTemplate/'+idTemplate);

            return request;
        },
        getElementsbyLanguage: function(idLanguage){

            var request = $http.get('api/elements/getElementsbyLanguage/'+idLanguage);
            return request;
        },
        update: function(id,data){

            var request = $http({method:'GET', url:'api/elements/updateElement/'+id,params:data});
            return request;
        },
        delete: function(id){
          //delete a specific post

           var request = $http({method:'DELETE', url:'api/elements/'+id});
           return request;
        }
    }
 
});