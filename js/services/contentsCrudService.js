
var crud = angular.module('ContentsCrudSrvc',[]);
 
crud.factory("ContentsCRUD",function($http){
 
    return{
        all: function(){
          //get all posts
          var request = $http({method:'GET', url:'api/contents'});
           return request;
        },
        create: function(data,html){
          //create a new post

          //WITH POST

                   var request = $http.post('api/contents/addContents', data );
                  return request;

           // var request = $http({method:'GET', url:'api/contents/create/',params:data});
           //  return request;
        },

        //Create Element Content
              createElement: function(data,html){
          //create a new post

          //WITH POST

                   var request = $http.post('api/contents/addElementContents', data );
                  return request;

           // var request = $http({method:'GET', url:'api/contents/create/',params:data});
           //  return request;
        },
        get: function(id){
          //get a specific content
            var request = $http.get('api/contents/'+id);
            return request;
        },

        getContentsbyPage: function(idPage,idLanguage){

            var request = $http.get('api/pages/getContentsbyPage/'+idPage+'/'+idLanguage);
            return request;
        },

              getElementContents: function(idElement){

            var request = $http.get('api/elements/getElementContents/'+idElement);
            return request;
        },
        update: function(data){
          //update a specific content

           var request = $http.post('api/contents/updateContent', data );
                  return request;
            //  var request = $http({method:'GET', url:'api/contents/updateContent/'+id,params:data});
            // return request;
        },
                updateElement: function(data){
          //update a specific content

           var request = $http.post('api/contents/updateElementContent', data );
                  return request;
            //  var request = $http({method:'GET', url:'api/contents/updateContent/'+id,params:data});
            // return request;
        },
             updateScraped: function(data){
          //update a scraped content

            // var request = $http({method:'GET', url:'api/contents/updateScrapedContent/'+id,params:data});
            var request = $http.post('api/contents/updateScrapedContent', data );
            return request;
        },
        delete: function(id){
        }
    }
 
});