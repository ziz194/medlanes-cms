
var crud = angular.module('BrandsCrudSrvc',[]);
 
crud.factory("BrandsCRUD",function($http){
 
    return{
        all: function(){
          //get all languages
          var request = $http({method:'GET', url:'api/brands'});
           return request;
        },
        create: function(data){
          //create a new language
           var request = $http({method:'GET', url:'api/brands/create',params:data});
            return request;
        },
        get: function(id){
          //get a specific language
            var request = $http.get('api/brands/'+id);
            return request;
        },
        update: function(id,data){
          //update a specific language
            var request = $http.put('api/brands/'+id,data);
            return request;
        },
        delete: function(id){
          
           var request = $http({method:'DELETE', url:'api/brands/'+id});
           return request;
        }
    }
 
});