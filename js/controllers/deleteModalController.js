var article = angular.module('DeleteModalCtrl',[]);
 
article.controller('DeleteModalController',function($scope, $modalInstance, $log,idDelete,$http,PagesCRUD){



    $scope.idDelete = idDelete;


  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };


      $scope.deletePage = function () {

                   var request = PagesCRUD.delete($scope.idDelete);
                  request.success(function(response){

            });

                   $modalInstance.dismiss();
                   $('.top-left').notify({
                      message: { text: 'The page has been removed successfully' }
                    }).show();  


                            location.reload();



  };



});