var article = angular.module('TemplateCtrl',[]);
 
article.controller('TemplatesController',function($scope,$location,$sce,TemplatesCRUD,LanguagesCRUD,BrandsCRUD,SessionService){


	// Initialisations 

	       $scope.edit = false ; 
		     $scope.Data = {};
         $scope.editTemplateId = 0 ; 
         $scope.currentLanguageName = 'English' ; 
         $scope.currentLanguageId = 1 ;
         $scope.currentBrandName = 'Medlanes' ; 
         $scope.currentBrandId = 28 ;
   
		     $scope.Data.name = function() { return  SessionService.get('name') ; };
         $scope.Data.role = function() { return  SessionService.get('role') ; };
         $scope.Data.last_login = function() { return  SessionService.get('last_login') ; };



      var getALLTemplates = TemplatesCRUD.getTemplateByLanguage($scope.currentBrandId,$scope.currentLanguageId);
      getALLTemplates.success(function(response){
      $scope.templates = response;

                  });

               /* Get a list of Languages */ 
      var getALLLanguages = LanguagesCRUD.all();
      getALLLanguages.success(function(response){
                     $scope.languages = response;

                  });



               /* Get a list of Brands */ 
      var getALLBrands = BrandsCRUD.all();
      getALLBrands.success(function(response){
                     $scope.brands = response;

                  });
$scope.getLanguage = function(languageName,languageId){



      $scope.currentLanguageName = languageName ; 
      $scope.currentLanguageId = languageId ; 
      var getALLTemplates = TemplatesCRUD.getTemplateByLanguage($scope.currentBrandId,$scope.currentLanguageId);
      getALLTemplates.success(function(response){
      $scope.templates = response;

                  });
}
    

$scope.getBrand = function(brandName,brandId){



     $scope.currentBrandName = brandName ; 
      $scope.currentBrandId = brandId ; 

  var getALLTemplates = TemplatesCRUD.getTemplateByLanguage($scope.currentBrandId,$scope.currentLanguageId);
      getALLTemplates.success(function(response){
                     $scope.templates = response;

                  });

    }

$scope.createTemplate = function(){



	              if (!($scope.newTemplate.templateFile)  || !($scope.newTemplate.inputsFile)) 

	              {

	              	      //show error
                      $('.top-left').notify({

                      	 message: { 
                        text: 'Please select the template file'
                       },
                       type : 'danger' 
                    }).show();  
	              }


                $scope.newTemplate.languageId = $scope.currentLanguageId
                $scope.newTemplate.brandId = $scope.currentBrandId
 			 	  var request = TemplatesCRUD.create($scope.newTemplate);
                  request.success(function(response){

                  	      //show notification
                      $('.top-right').notify({
                      message: { text: 'The Template has been added successfully' }
                    }).show();  

  var getALLTemplates = TemplatesCRUD.getTemplateByLanguage($scope.currentBrandId,$scope.currentLanguageId);
      getALLTemplates.success(function(response){
                     $scope.templates = response;

                  });



                  });




                }



                $scope.removeTemplate = function(id){

                    var request = TemplatesCRUD.delete(id);
                  request.success(function(response){

                     //show notification
                      $('.top-right').notify({
                      message: { text: 'The Template has been deleted successfully' }
                    }).show();  

                
    var getALLTemplates = TemplatesCRUD.getTemplateByLanguage($scope.currentBrandId,$scope.currentLanguageId);
      getALLTemplates.success(function(response){
                     $scope.templates = response;

                  });


                  });

                 

              }



                $scope.setEdit = function(id,value){

                  $scope.edit = value ;
                  $scope.editTemplateId = id ; 

                  // select the element to edit 


                  var request = TemplatesCRUD.get($scope.editTemplateId);
                  request.success(function(response){
                  $scope.editTemplate = response ; 
                  console.log ($scope.editTemplate);
                          });

              }

             


                  $scope.templateEdit = function(){


                  // update the template
                    var request = TemplatesCRUD.update($scope.editTemplateId,$scope.editTemplate);
                    request.success(function(response){

                     //show notification
                      $('.top-right').notify({
                      message: { text: 'The Template has been updated successfully' }
                    }).show();  

  var getALLTemplates = TemplatesCRUD.getTemplateByLanguage($scope.currentBrandId,$scope.currentLanguageId);
      getALLTemplates.success(function(response){
                     $scope.templates = response;

                  });
                 

              });

  }





// End of Templates Controller
});


