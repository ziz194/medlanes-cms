var article = angular.module('EditAssetModalCtrl',[]);
 
article.controller('EditAssetModalController',function($scope, $modalInstance, $log,asset,$http,AssetsCRUD){



   $scope.idAsset = asset.idAsset;
   $scope.asset = asset ;



  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };


      $scope.editAsset= function () {

                   var request = AssetsCRUD.update($scope.asset);
                  request.success(function(response){
                                       location.reload();


            });

                   $modalInstance.dismiss();
                    $('.top-left').notify({
                      message: { text: 'The page has been edited successfully' }
                    }).show();  


  };



});