var article = angular.module('SettingsCtrl',[]);

article.controller('SettingsController',function($scope,$location,$sce,LanguagesCRUD,BrandsCRUD,SessionService){


  // Initialisations 

  $scope.edit = false ; 
  $scope.edit2 = false ; 
  $scope.Data = {};
  $scope.editLanguageId = 0 ; 
  $scope.editBrandId = 0 ; 




  $scope.Data.name = function() { return  SessionService.get('name') ; };
  $scope.Data.role = function() { return  SessionService.get('role') ; };
  $scope.Data.last_login = function() { return  SessionService.get('last_login') ; };




  /* Get a list of Languages */ 
  var getALLLanguages = LanguagesCRUD.all();
  getALLLanguages.success(function(response){
   $scope.languages = response;

 });

  /* Get a list of Brands */ 
  var getALLBrands = BrandsCRUD.all();
  getALLBrands.success(function(response){
   $scope.brands = response;

 });



  $scope.createLanguage = function(){



   if (!($scope.newLanguage.languageURL)) 

   {

      //show error
      $('.top-left').notify({

        message: { 
          text: 'Please fill the language URL '
        },
        type : 'danger' 
      }).show();  
    }


    $scope.newLanguage.languageId = $scope.currentLanguageId
    $scope.newLanguage.languageURL = $scope.newLanguage.languageURL.toLowerCase() ;
    var request = LanguagesCRUD.create($scope.newLanguage);
    request.success(function(response){

        //show notification
        $('.top-right').notify({
          message: { text: 'The Language has been added successfully' }
        }).show();  



        /* Get a list of Languages */ 
        var getALLLanguages = LanguagesCRUD.all();
        getALLLanguages.success(function(response){
         $scope.languages = response;

       });





      });



}

// Create new Brand 


  $scope.createBrand = function(){



   if (!($scope.newBrand.brandDomain)) 

   {

      //show error
      $('.top-left').notify({

        message: { 
          text: 'Please fill the Brand Domain '
        },
        type : 'danger' 
      }).show();  
    }


    $scope.newBrand.brandDomain = $scope.newBrand.brandDomain.toLowerCase() ;
    var request = BrandsCRUD.create($scope.newBrand);
    request.success(function(response){

        //show notification
        $('.top-right').notify({
          message: { text: 'The Brand has been added successfully' }
        }).show();  



        /* Get a list of Languages */ 
        var getALLBrands = BrandsCRUD.all();
        getALLBrands.success(function(response){
         $scope.brands = response;

       });
        location.reload();

    });



}

      $scope.removeLanguage = function(id){

        var request = LanguagesCRUD.delete(id);
        request.success(function(response){


        if(response == 'NO') 

        {
           $('.top-left').notify({
        message: { text: 'This Language has pages under it please remove the pages first in order to remove the language' },
        type : 'danger' }).show();  

        }

        else{

       //show notification
       $('.top-right').notify({
        message: { text: 'The Language has been deleted successfully' }
      }).show();  
     }



       /* Get a list of Languages */ 
       var getALLLanguages = LanguagesCRUD.all();
       getALLLanguages.success(function(response){
         $scope.languages = response;

       });



     });



      }

// Remove a Brand
      $scope.removeBrand = function(id){

        var request = BrandsCRUD.delete(id);
        request.success(function(response){


        if(response == 'NO') 

        {
           $('.top-left').notify({
        message: { text: 'This Brand has pages under it please remove the pages first in order to remove the Brand' },
        type : 'danger' }).show();  

        }

        else{

       //show notification
       $('.top-left').notify({
        message: { text: 'The Brand has been deleted successfully' },
        type : 'danger' }).show();  
     }



       /* Get a list of Languages */ 
       var getALLBrands = BrandsCRUD.all();
       getALLBrands.success(function(response){
         $scope.brands = response;

       });



     });



      }


      $scope.setEdit = function(id,value){

        $scope.edit = value ;
        $scope.editLanguageId = id ; 



    // select the element to edit 


    var request = LanguagesCRUD.get($scope.editLanguageId);
    request.success(function(response){
      $scope.editLanguage = response ; 
    });

  }



      $scope.setEdit2 = function(id,value){

        $scope.edit2 = value ;
        $scope.editBrandId = id ; 
    // select the element to edit 
    var request = BrandsCRUD.get($scope.editBrandId);
    request.success(function(response){
      $scope.editBrand = response ; 
    });

  }




  $scope.languageEdit = function(){


    // update the template

    var request = LanguagesCRUD.update($scope.editLanguageId,$scope.editLanguage);
    request.success(function(response){

       //show notification
       $('.top-right').notify({
        message: { text: 'The Language has been updated successfully' }
      }).show();  
       /* Get a list of Languages */ 
       var getALLLanguages = LanguagesCRUD.all();
       getALLLanguages.success(function(response){
         $scope.languages = response;

       });


     });

  }


  $scope.brandEdit = function(){


    // update the brand

    var request = BrandsCRUD.update($scope.editBrandId,$scope.editBrand);
    request.success(function(response){

       //show notification
       $('.top-right').notify({
        message: { text: 'The Brand has been updated successfully' }
      }).show();  
       /* Get a list of brands */ 
       var getALLBrands = BrandsCRUD.all();
       getALLBrands.success(function(response){
         $scope.brands = response;

       });
     });

  }


// End of Settings Controller
});


