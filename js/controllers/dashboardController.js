var dashboard = angular.module('DashCtrl',[]);

dashboard.controller('DashboardController',function($scope,$sce,$location,$modal,$route,$templateCache,$log,Login,SessionService,PagesCRUD,LanguagesCRUD,TemplatesCRUD,ContentsCRUD,BrandsCRUD){
  
  $scope.Data = {};
  $scope.Data.name = function() { return  SessionService.get('name') ; };
  $scope.Data.role = function() { return  SessionService.get('role') ; };
  $scope.Data.last_login = function() { return  SessionService.get('last_login') ; };

  console.log('your name is :'+$scope.Data.name()) ;    

  if($scope.Data.name() == null)  
  {
    $location.path('/');
  }
  /* Initialisations */ 
  $scope.currentPageCreator = '' ;
  $scope.currentLanguageId = 1;
  $scope.currentLanguageId2 = 1;
  $scope.currentPageId = false;
  $scope.newPageId = 3;
  $scope.currentPageName = 'Select Page';
  $scope.currentLanguageName = 'English';
  $scope.currentBrandName = 'Medlanes';
  $scope.currentBrandId = 28;
  $scope.currentLanguageName2 = 'English';
  $scope.dropTitle2 = 'Preview' ;   
  $scope.dropTitle = 'View'  ;
  $scope.edit = false ;
  $scope.edit2 = false ;
  $scope.preview = false;
  $scope.preview2 = true;
  $scope.add = true ;
  $scope.currentPageUrl = $sce.trustAsResourceUrl('https://www.medlanes.com');
  $scope.currentPageUrl2 = $sce.trustAsResourceUrl('https://www.medlanes.com');
  $scope.otherLanguageFilename1 = 'Select Filename' ; 
  $scope.masterPageFileName = 'Select Filename' ; 
  $scope.filename1  = 'Select Filename' ; 
  $scope.filename2 = 'Select Filename' ; 

      //Edit fields Array 
      var editFields = [{
       idField: 'defaultID' ,
       typeField: 'defaultType',
       regionField: 'defaultRegion'

     }]

     var dbFields = [{

       dbid: 'defaultIDdb' ,
       dbtype: 'defaultTypedb',
       region: 'defaultRegiondb'

     }]


     var editFields2 = [{

       idField: 'defaultID' ,
       typeField: 'defaultType',
       regionField: 'defaultRegion'

     }]

     var dbFields2 = [{

       dbid: 'defaultIDdb' ,
       dbtype: 'defaultTypedb',
       region: 'defaultRegiondb'

     }]


      // new contents array  
      $scope.newContents = {
        content: [{

          nameContent: 'defaultName' ,
          typeContent: 'defaultType',
          regionContent: 'defaultRegion',
          dataContent: 'defaultData',
          orderContent: 'defaultOrder',
          idPage: $scope.newPageId 

        }] 
      };


          // updated contents array  
          $scope.updatedContents = {
            content: [{

              nameContent: 'defaultName' ,
              typeContent: 'defaultType',
              regionContent: 'defaultRegion',
              dataContent: 'defaultData',
              orderContent: 'defaultOrder',
              idPage: $scope.currentPageId 

            }] 
          };

          $scope.updatedContents2 = {
            content: [{

              nameContent: 'defaultName' ,
              typeContent: 'defaultType',
              regionContent: 'defaultRegion',
              dataContent: 'defaultData',
              orderContent: 'defaultOrder',
              idPage: $scope.currentPageId 

            }] 
          };


$scope.setOtherLanguageFile=function(filename,id,pagename,languageName){
  $('.'+languageName).val(filename) ;
  $('.'+languageName).trigger('span');
  $('.'+languageName).trigger('input');
  $('.'+languageName+'idPage').val(id) ;
  $('.'+languageName+'idPage').trigger('input');
  $scope.otherLanguageFilename1 = pagename ; 
}

$scope.setMasterPageFile=function(filename,id,pagename){
  $('.masterPageDrop').val(filename) ;
  $('.masterPageDrop').trigger('span');
  $('.masterPageDrop').trigger('input');
  $('.masterPageDropIdPage').val(id) ;
  $('.masterPageDropIdPage').trigger('input');
  $scope.masterPageFilename = pagename ; 
}
// Logout functionality
$scope.logout = function(){

  SessionService.unset('name') ;
  SessionService.unset('auth') ;
  SessionService.unset('role') ;
  $location.path('/');

}
// Get the list of the pages for differnet languages dropdowns 
$scope.getPagesbyLanguage2 = function(idBrand,idLanguage){


  /* Get list of the pages according to language*/ 
  var getPages = PagesCRUD.getPagesbyBrandAndLanguage(idBrand,idLanguage);
  getPages.success(function(response){
   $scope.pages2 = response;

 });

}
/* Get list of the pages according to the active language*/ 
var getPages = PagesCRUD.getPagesbyBrandAndLanguage($scope.currentBrandId,$scope.currentLanguageId);
getPages.success(function(response){
 $scope.pages = response;

});


// Load Tags 
 $scope.loadTags = function($query) {

      var pages = $scope.pages ;
      return pages.filter(function(page) {
        return page.namePage.toLowerCase().indexOf($query.toLowerCase()) != -1;
      });
      
  }
/* Get a list of Brands */ 
var getALLBrands = BrandsCRUD.all();
getALLBrands.success(function(response){
 $scope.brands = response;

});
/* Get a list of Languages */ 
var getALLLanguages = LanguagesCRUD.getLanguagesByBrand($scope.currentBrandId);
getALLLanguages.success(function(response){
 $scope.languages = response;

});

/* Get a list of Templates */ 
var getALLTemplates = TemplatesCRUD.getTemplateByLanguage($scope.currentBrandId,$scope.currentLanguageId);
getALLTemplates.success(function(response){
 $scope.templates = response;

});



$scope.getBrand = function(idBrand,nameBrand){
  $scope.currentBrandId = idBrand ; 
  $scope.currentBrandName = nameBrand ;
  var getALLLanguages = LanguagesCRUD.getLanguagesByBrand($scope.currentBrandId);
  getALLLanguages.success(function(response){
 $scope.languages = response;
});

    var getALLPages =  PagesCRUD.getPagesbyBrandAndLanguage($scope.currentBrandId,$scope.currentLanguageId);
    getALLPages.success(function(response){
      $scope.pages = response;             
    });

        /* Get a list of Templates */ 
    var getALLTemplates = TemplatesCRUD.getTemplateByLanguage($scope.currentBrandId,$scope.currentLanguageId);
    getALLTemplates.success(function(response){
     $scope.templates = response;
   });

}

/* Get the language URLs of a specific page for the left side */ 
$scope.getLanguage = function(idPage,idLanguage,nameLanguage){

   // if(idPage==false){
   //  idPage = 2239 ;
   // }
   //    $scope.currentPageId = idPage ;  
      $scope.currentLanguageId = idLanguage ; 
      $scope.currentLanguageName = nameLanguage ;   
      console.log($scope.currentLanguageId);
      var getALLPages =  PagesCRUD.getPagesbyBrandAndLanguage($scope.currentBrandId,$scope.currentLanguageId);
    getALLPages.success(function(response){
      $scope.pages = response;             
    });

  // var request = PagesCRUD.pageLanguageUrl(idPage,idLanguage);
  // request.success(function(response){
  //   $scope.pageLanguageUrl = response.urlPage;
  //   $scope.currentPageUrl = $sce.trustAsResourceUrl($scope.pageLanguageUrl) ;
  //   $scope.currentPageName = response.namePage ;
  //   console.log(response.idPage) ;
  //   $scope.currentPageId = response.idPage ;
  //   $scope.add = false ; 
  //   $scope.edit = false ; 
  //   $scope.preview = true ; 
  //   $scope.dropTitle = 'Preview';
  //   $scope.newPage.currentLanguage =  $scope.currentLanguageId ;

  //  //Get the template fields for editing
  //  var request = PagesCRUD.getPageTemplate($scope.currentPageId);
  //  request.success(function(response){
  //   $scope.templateContentEdit =  $sce.trustAsHtml(response) ;

  // });


  //  //Set Values in Edit fields according to page content

  //  var request = ContentsCRUD.getContentsbyPage($scope.currentPageId);
  //  request.success(function(response){
  //   angular.forEach(response, function(value, key) {
  //     console.log(key + ': ' + value.nameContent + ' : '+ value.dataContent);
  //     var element = angular.element("#"+value.nameContent);
  //     var val = value.dataContent;   
  //     element.val(val);


  //   });

  // });

    /* Get a list of Templates */ 
    var getALLTemplates = TemplatesCRUD.getTemplateByLanguage($scope.currentBrandId,$scope.currentLanguageId);
    getALLTemplates.success(function(response){
     $scope.templates = response;
   });

  // });
}

/* Get the language URLs of a specific page for the right side */ 

$scope.getLanguage2 = function(idPage,idLanguage,nameLanguage){
    $scope.currentLanguageId = idLanguage ;  
      var getALLPages =  PagesCRUD.getPagesbyBrandAndLanguage($scope.currentBrandId,$scope.currentLanguageId);

    getALLPages.success(function(response){
      $scope.pages = response;             
    });
  var request = PagesCRUD.pageLanguageUrl(idPage,idLanguage);
  request.success(function(response){
    $scope.pageLanguageUrl = response.urlPage;
    $scope.currentPageUrl2 = $sce.trustAsResourceUrl($scope.pageLanguageUrl) ;
    $scope.currentLanguageName2 = nameLanguage ;  
    $scope.currentPageName2 = response.namePage ;
    $scope.currentPageId = response.idPage ;
    $scope.add2 = false ; 
    $scope.edit2 = false ; 
    $scope.preview2 = true ; 
    $scope.dropTitle2 = 'Preview';
    $scope.newPage.currentLanguage =  $scope.currentLanguageId ;



   //Get the template fields for editing
   var request = PagesCRUD.getPageTemplate($scope.currentPageId);
   request.success(function(response){
    $scope.templateContentEdit =  $sce.trustAsHtml(response) ;

  });

               //Set Values in Edit fields according to page content
               var request = ContentsCRUD.getContentsbyPage($scope.currentPageId);
               request.success(function(response){
                angular.forEach(response, function(value, key) {
                  console.log(key + ': ' + value.nameContent + ' : '+ value.dataContent);
                  var element = angular.element("#"+value.nameContent);
                  var val = value.dataContent;   
                  element.val(val);


                });

              });

    /* Get a list of Templates */ 
    var getALLTemplates = TemplatesCRUD.getTemplateByLanguage($scope.currentBrandId,$scope.currentLanguageId);
    getALLTemplates.success(function(response){
     $scope.templates = response;
   });

  });

}      

// Reset Fields 
$scope.resetFields = function() {
   $('#edit-form .dataInput').each(function(){
      $(this).val($(this).defaultValue) ;
    });
    $('#edit-form2 .dataInput').each(function(){
      $(this).val($(this).defaultValue) ;
    });
    $('#add-form .dataInput').each(function(){
      $(this).val($(this).defaultValue) ;
    });

      //scroll to the top of the page
       window.scrollTo(0,0);
}

/* Set button values and changes the views accordingly */ 
$scope.setPage = function(url,name,id) {


  $scope.currentPageId = id ;       
  $scope.currentPageName = name;
  var request = PagesCRUD.getPageUrl($scope.currentPageId,$scope.currentLanguageId);
  request.success(function(response){
    $scope.currentPageUrl = $sce.trustAsResourceUrl(response.urlPage);
    $scope.currentPageUrl2 = $sce.trustAsResourceUrl(response.urlPage);

  });


             //Get the template fields for editing
             var request = PagesCRUD.getPageTemplate($scope.currentPageId);
             request.success(function(response){
              $scope.templateContentEdit =  $sce.trustAsHtml(response) ;

            });


   //Set Values in Edit fields according to page content

         var request = ContentsCRUD.getContentsbyPage($scope.currentPageId);
         request.success(function(response){
          angular.forEach(response, function(value, key) {
            console.log(key + ': ' + value.nameContent + ' : '+ value.dataContent);
            var element = angular.element("#"+value.nameContent);
            var val = value.dataContent;   
            element.val(val);

              });

            });

               $scope.setPreview(true) ;

             }


             $scope.setPreview = function(val) {

              $scope.preview = val;
              $scope.edit = false ; 
              $scope.add = false ; 
              $scope.dropTitle = 'Preview' ;

            }

            $scope.setEdit = function(val) {



          //empty the editing arrays 
          var editFields = [{

           idField: 'defaultID' ,
           typeField: 'defaultType',
           regionField: 'defaultRegion'

         }]

         var dbFields = [{

           dbid: 'defaultIDdb' ,
           dbtype: 'defaultTypedb',
           region: 'defaultRegiondb'

         }]

         if(($scope.currentPageId)==false)
         {
                    //show error if page is npt selected
                    $('.top-left').notify({
                      message: { 
                        text: 'Please select a page'
                      },
                      type : 'danger' 
                    }).show();  

                  }

                  else {

                    $scope.edit = val
                    $scope.preview = false;
                    $scope.dropTitle = 'Edit' ; 

           //Get the template fields for editing
           var request = PagesCRUD.getPageTemplate($scope.currentPageId);
           request.success(function(response){
            $scope.templateContentEdit2 =  $sce.trustAsHtml(response) ;
          });

              //get the fields of the page configuration 
              var request = PagesCRUD.get($scope.currentPageId);
              request.success(function(response){


                $scope.editConfig =  response ;
                $scope.editConfig.currentLanguage = $scope.currentLanguageId ; 
                //Set Tags 
                $('#tags-jquery-edit').importTags($scope.editConfig.tags);

              });
              //fill editFields array 

              $('#edit-form .dataInput').each(function(){

                idField = $(this).attr('elementid') ; 
                regionField = $(this).attr('region') ; 
                typeField = $(this).attr('type') ; 

                editFields.push({
                  idField: idField,
                  regionField: regionField, 
                  typeField : typeField 
                });

              });

             //Get contents by page   
             var request = ContentsCRUD.getContentsbyPage($scope.currentPageId,$scope.currentLanguageId);
             request.success(function(response){
              $.each(response, function(value, key) {
                dbFields.push({
                  dbid: key.nameContent,
                  region: key.regionContent, 
                  dbtype : key.typeContent 
                });

              });

              var request = PagesCRUD.get($scope.currentPageId);
              request.success(function(response2){
                console.log(response2.createdBy ) ;

                if (response2.createdBy  == 'ScrapingMaster'){

                   //Link each content with the corresponding field
                   $.each(dbFields, function(index,valuedb) {
                    var flag = true; 

                    $.each(editFields, function(index2,value2) {

                      if((valuedb.region == value2.regionField) && (valuedb.dbtype == value2.typeField) && (valuedb.dbid.length == value2.idField.length) )
                      {

                        if(value2.idField != valuedb.dbid)
                        {
                          $('#edit-form #'+value2.idField).attr('id',valuedb.dbid) ; 
                            // $('#edit-form #'+valuedb.dbid).attr('elementid',valuedb.dbid) ; 
                            console.log('Match found between :'+value2.idField+' and  '+valuedb.dbid) ;

                          }

                          if(valuedb.dbtype == 'bullet' )
                          {
                           var bulletClass = value2.idField.substr(0,value2.idField.indexOf('bullet')+9) ; 
                           $('#edit-form .'+bulletClass).addClass(valuedb.dbid) ; 
                           $('#edit-form .'+valuedb.dbid).removeClass(bulletClass) ; 
                          }
                            //value2.idField = valuedb.dbid ;
                            value2.typeField = ''+Math.random();  
                            value2.regionField = ''+Math.random(); 
                            valuedb.dbtype = ''+Math.random();  
                            valuedb.region = ''+Math.random(); 
                            return false ; 
                            flag = false ; 

                          }


                        });

                        return flag ;
                      });


angular.forEach(response, function(value, key) {
  var bulletName = value.nameContent.substr(0,value.nameContent.indexOf('bullet')+9) ; 
  console.log (bulletName) ;
  if (value.typeContent == 'bullet' && value.nameContent!=bulletName)
  {
   $('#edit-form .'+bulletName).append('<div id="'+value.nameContent+'" region="'+value.regionContent+'"><input type="bullet" id="'+value.nameContent+'" elementid="'+value.nameContent+'"   region="'+value.regionContent+'"  class="form-control dataInput bullet-input" /><button  class="btn btn-danger delete-bullet-button" ><span class="glyphicon glyphicon-minus-sign"></span></button></div>') ;
 }

 var element = angular.element("#edit-form #"+value.nameContent);
 var val = value.dataContent;   
 element.val(val);


});
}
});


angular.forEach(response, function(value, key) {
  var bulletName = value.nameContent.substr(0,value.nameContent.indexOf('bullet')+9) ; 
  console.log (bulletName) ;
  if (value.typeContent == 'bullet' && value.nameContent!=bulletName)
  {
   $('#edit-form .'+bulletName).append('<div id="'+value.nameContent+'" region="'+value.regionContent+'"><input type="bullet" id="'+value.nameContent+'" elementid="'+value.nameContent+'"   region="'+value.regionContent+'"  class="form-control dataInput bullet-input" /><button  class="btn btn-danger delete-bullet-button" ><span class="glyphicon glyphicon-minus-sign"></span></button></div>') ;
 }

 var element = angular.element("#edit-form #"+value.nameContent);
 var val = value.dataContent;   
 element.val(val);


});
});
}
}

 //Set Edit in second view field
 $scope.setEdit2 = function(val) {
                  //Set Tags 
                $('#tags-jquery-edit').importTags($scope.editConfig.tags);

                 //empty the editing arrays 
                 var editFields2 = [{

                   idField: 'defaultID' ,
                   typeField: 'defaultType',
                   regionField: 'defaultRegion'

                 }]

                 var dbFields2 = [{

                   dbid: 'defaultIDdb' ,
                   dbtype: 'defaultTypedb',
                   region: 'defaultRegiondb'

                 }]



                 if(($scope.currentPageId)==false)
                 {
                    //show error if page is npt selected
                    $('.top-left').notify({
                      message: { 
                        text: 'Please select a page'
                      },
                      type : 'danger' 
                    }).show();  

                  }

                  else {

                    $scope.edit2 = val
                    $scope.preview2 = false;
                    $scope.dropTitle2 = 'Edit' ; 

           //Get the template fields for editing
           var request = PagesCRUD.getPageTemplate($scope.currentPageId);
           request.success(function(response){


             $scope.templateContentEdit2 =  $sce.trustAsHtml(response) ;



           });
              //get the fields of the page configuration 
              var request = PagesCRUD.get($scope.currentPageId);
              request.success(function(response){


                $scope.editConfig =  response ;
                $scope.editConfig.currentLanguage = $scope.currentLanguageId ; 
                //Set Tags 
                $('#tags-jquery-edit').importTags($scope.editConfig.tags);



              });

              //fill editFields array 

              $('#edit-form2 .dataInput').each(function(){

                idField = $(this).attr('elementid') ; 
                regionField = $(this).attr('region') ; 
                typeField = $(this).attr('type') ; 

                editFields2.push({
                  idField: idField,
                  regionField: regionField, 
                  typeField : typeField 
                });

              });
             //Get contents by page   
             var request = ContentsCRUD.getContentsbyPage($scope.currentPageId,$scope.currentLanguageId);
             request.success(function(response){


              $.each(response, function(value, key) {


                dbFields2.push({
                  dbid: key.nameContent,
                  region: key.regionContent, 
                  dbtype : key.typeContent 
                });

              });
              var request = PagesCRUD.get($scope.currentPageId);
              request.success(function(response2){
                console.log(response2.createdBy ) ;

                if (response2.createdBy  == 'ScrapingMaster'){

                   //Link each content with the corresponding field
                   $.each(dbFields2, function(index,valuedb) {
                    var flag = true; 

                    $.each(editFields2, function(index2,value2) {

                      if((valuedb.region == value2.regionField) && (valuedb.dbtype == value2.typeField) && (valuedb.dbid.length == value2.idField.length) )
                      {

                        if(value2.idField != valuedb.dbid)
                        {
                          $('#edit-form2 #'+value2.idField).attr('id',valuedb.dbid) ; 
                            // $('#edit-form2 #'+valuedb.dbid).attr('elementid',valuedb.dbid) ; 
                            console.log('Match found between :'+value2.idField+' and  '+valuedb.dbid) ;

                          }

                          if(valuedb.dbtype == 'bullet' )
                          {
                           var bulletClass = value2.idField.substr(0,value2.idField.indexOf('bullet')+9) ; 
                           $('#edit-form2 .'+bulletClass).addClass(valuedb.dbid) ; 
                           $('#edit-form2 .'+valuedb.dbid).removeClass(bulletClass) ; 
                         }
                            //value2.idField = valuedb.dbid ;
                            value2.typeField = ''+Math.random();  
                            value2.regionField = ''+Math.random(); 
                            valuedb.dbtype = ''+Math.random();  
                            valuedb.region = ''+Math.random(); 
                            return false ; 
                            flag = false ; 

                          }


                        });

return flag ;
});



angular.forEach(response, function(value, key) {
  var bulletName = value.nameContent.substr(0,value.nameContent.indexOf('bullet')+9) ; 
  console.log (bulletName) ;
  if (value.typeContent == 'bullet' && value.nameContent!=bulletName)
  {
   $('#edit-form2 .'+bulletName).append('<div id="'+value.nameContent+'" region="'+value.regionContent+'"><input type="bullet" id="'+value.nameContent+'" elementid="'+value.nameContent+'"   region="'+value.regionContent+'"  class="form-control dataInput bullet-input" /><button  class="btn btn-danger delete-bullet-button" ><span class="glyphicon glyphicon-minus-sign"></span></button></div>') ;
 }

 var element = angular.element("#edit-form2 #"+value.nameContent);
 var val = value.dataContent;   
 element.val(val);


});
}
});


angular.forEach(response, function(value, key) {
  var bulletName = value.nameContent.substr(0,value.nameContent.indexOf('bullet')+9) ; 
  console.log (bulletName) ;
  if (value.typeContent == 'bullet' && value.nameContent!=bulletName)
  {
   $('#edit-form2 .'+bulletName).append('<div id="'+value.nameContent+'" region="'+value.regionContent+'"><input type="bullet" id="'+value.nameContent+'" elementid="'+value.nameContent+'"   region="'+value.regionContent+'"  class="form-control dataInput bullet-input" /><button  class="btn btn-danger delete-bullet-button" ><span class="glyphicon glyphicon-minus-sign"></span></button></div>') ;
 }

 var element = angular.element("#edit-form2 #"+value.nameContent);
 var val = value.dataContent;   
 element.val(val);


});
});
}


}
$scope.setPreview2 = function(val) {

  $scope.preview2 = val;
  $scope.edit2 = false ; 
  $scope.dropTitle2 = 'Preview' ;

}

$scope.setAdd = function(val) {

  // $('#templateDropdown').val(null);
  // $('#templateDropdown').prop( "disabled", true );
  $scope.newPage.idTemplate = 0 ;
  $scope.templateContent = '' ;
  $scope.add = val
  $scope.preview = false;
  $scope.preview2 = true;
  $scope.edit = false ;


}

/* Change default file name */ 

$scope.changeFileName = function(){

  $scope.newPage.pageUrl1 = $scope.newPage.pageName.replace(/\s+/g, '-').toLowerCase() ; 


}

/* Change default file name edit */ 

$scope.changeFileNameEdit = function(){


  $scope.editConfig.pageUrl1 = $scope.editConfig.namePage.replace(/\s+/g, '-').toLowerCase() ; 

}


// Get Defaults Button 

$scope.getDefaults = function(){


                     // Set Defaults to remove redunduncy 
                     var pageName = $('#inputName').val() ; 
                     var pageUrl = $('#inputURL').val() ;

                     // alert(pageName+pageUrl) ;
                     // alert($('#metaTags_text_2').val()) ;
                     //meta tags
                     $('#metaTags_text_1').val(pageName) ;
                      $('#metaTags_text_1').trigger('input');
                     $('#metaTags_text_3').val(pageName) ;
                     $('#metaTags_text_7').val(pageName) ;
                     $('#metaTags_text_4').val(pageUrl) ;
                     $('#metaTags_text_6').val(pageUrl) ;

                     $('#metaTags_text_2').val($('#metaTags_text_2').val()+' '+pageName) ;
                     $('#metaTags_text_5').val($('#metaTags_text_5').val()+' '+pageName) ;
                     $('#metaTags_text_8').val($('#metaTags_text_8').val()+' '+pageName) ;
                     // breadcrumbs
                     $('#heading_text_10').val(pageName) ;

                     if(pageUrl.indexOf('questions')!=-1){

                       $('#breadCrumbs_text_11').val(pageUrl.substr(0, pageUrl.indexOf('-questions'))) ;
                     }

                     else{
                        $('#breadCrumbs_text_11').val(pageUrl+'-questions') ;
                     }

                     $('#breadCrumbs_text_14').val(pageUrl) ;
                     $('#breadCrumbs_text_15').val(pageName) ;
}




/* Create a  new page */ 

$scope.createPage = function(){

                    // empty new contents array  
                    $scope.newContents = {
                      content: [{

                        nameContent: 'defaultName' ,
                        typeContent: 'defaultType',
                        regionContent: 'defaultRegion',
                        dataContent: 'defaultData',
                        orderContent: 'defaultOrder',
                        idPage: $scope.newPageId 

                      }] 
                    };
                    $( "#add-form .dataInput" ).each(function(index) {

                     var inputElement = $(this) ; 

                     var type  = inputElement.attr('type') ; 
                     var region    = inputElement.attr('region') ; 
                     var value = inputElement.val() ; 
                     var order = index ; 
                     var id    = inputElement.attr('elementid') ;  ; 




                // Add the values to the new contents array

                $scope.newContents.content.push({
                  nameContent: id ,
                  typeContent: type,
                  regionContent: region,
                  dataContent: value,
                  orderContent: order,
                  idPage: $scope.newPageId 
                });


              });

                    var request = ContentsCRUD.create( $scope.newContents.content);
                    request.success(function(response){



                    });


                     //change to edit 

                     $scope.setPage($scope.currentPageUrl,$scope.currentPageName,$scope.newPageId);
                     $scope.setEdit(true) ; 


                      //show notification
                      $('.top-right').notify({
                        message: { text: 'The page has been added successfully' }
                      }).show();  

                      //scroll to the top of the page

                      window.scrollTo(0,0);


                 // Refresh pages list
                 var getPages = PagesCRUD.getPagesbyBrandAndLanguage($scope.currentBrandId,$scope.currentLanguageId);
                 getPages.success(function(response){
                   $scope.pages = response;
                   $scope.add = false ;
                   $scope.edit = true ; 

                 });

location.reload();
}


$scope.activatePage = function(){
 //API Call to Activate Page 
   $scope.editConfig.currentLanguage = $scope.currentLanguageId ;
   var request = PagesCRUD.activatePage($scope.currentPageId,$scope.editConfig);
   request.success(function(response){
                  location.reload();
                });
}

$scope.deactivatePage = function(){
 //API Call to Activate Page 
   $scope.editConfig.currentLanguage = $scope.currentLanguageId ;
   var request = PagesCRUD.deactivatePage($scope.currentPageId,$scope.editConfig);
   request.success(function(response){
                  location.reload();
                });
}


$scope.editPage = function(){

  var request = PagesCRUD.get($scope.currentPageId);
  request.success(function(response2){

    if((response2.createdBy != $scope.Data.name())  && ($scope.Data.role()=='Editor')){

                    //show error if page is npt selected
                    $('.top-left').notify({
                      message: { 
                        text: 'This page can only be edited by the page creator or an administrator'
                      },
                      type : 'danger' 
                    }).show();      

                  }

    else if($('#metaTags_text_1').val()=='') {
                      //show error if page is npt selected
                    $('.top-left').notify({
                      message: { 
                        text: 'Please fill in the meta tags'
                      },
                      type : 'danger' 
                    }).show();    
    }

                  else{

                //API Call to Edit Page Configuration
                $scope.editConfig.currentLanguage = $scope.currentLanguageId ;
                  // var tagNames_edit = $scope.tags2.map(function(tag) { return tag.namePage; });
                  $scope.editConfig.tags = $('#tags-jquery-edit').val();
                  $scope.editConfig.editedBy = $scope.Data.name() ;
                var request = PagesCRUD.update($scope.currentPageId,$scope.editConfig);
                request.success(function(response){

   
                });

                /* if data is  scraped */ if (response2.createdBy  == 'ScrapingMaster'){

                    // Empty updated contents array  
                    $scope.updatedContents = {
                      content: [{

                        nameContent: 'defaultName' ,
                        typeContent: 'defaultType',
                        regionContent: 'defaultRegion',
                        dataContent: 'defaultData',
                        orderContent: 'defaultOrder',
                        idPage: $scope.currentPageId 

                      }] 
                    };



                    $( "#edit-form .dataInput" ).each(function( index ) {


                     var inputElement = $(this) ; 
                     var type  = inputElement.attr('type') ; 
                     var id    = inputElement.attr('id') ; 
                     var region    = inputElement.attr('region') ; 
                     var value = inputElement.val() ; 
                     var order = index ; 



                // Add the values to the new contents array

                if(value!='')
                {

                  $scope.updatedContents.content.push({
                    nameContent: id ,
                    typeContent: type,
                    regionContent: region,
                    dataContent: value,
                    orderContent: order,
                    idPage: $scope.currentPageId 
                  });
                }

              });

           //API Call to Edit Content

           var request = ContentsCRUD.updateScraped($scope.updatedContents.content);
           request.success(function(response){
           });



                                //show notification
                                $('.top-right').notify({
                                  message: { text: 'Page updated successfully' }
                                }).show();  

                  //refresh preview
                  $scope.setPreview2(true) ; 
                  $scope.setEdit(true) ; 

                   //scroll to the top of the page
                   window.scrollTo(0,0);
                   $scope.currentPageUrl = $sce.trustAsResourceUrl('') ;


                 }


                 /* if data is not scraped */else {

                 // Empty updated contents array  
                 $scope.updatedContents = {
                  content: [{

                    nameContent: 'defaultName' ,
                    typeContent: 'defaultType',
                    regionContent: 'defaultRegion',
                    dataContent: 'defaultData',
                    orderContent: 'defaultOrder',
                    idPage: $scope.currentPageId 

                  }] 
                };



                $( "#edit-form .dataInput" ).each(function( index ) {


                 var inputElement = $(this) ; 
                 var type  = inputElement.attr('type') ; 
                 var id    = inputElement.attr('elementid') ; 
                 var region    = inputElement.attr('region') ; 
                 var value = inputElement.val() ; 
                 var order = index ; 



                // Add the values to the new contents array

                // if(value!='')
                // {

                  $scope.updatedContents.content.push({
                    nameContent: id ,
                    typeContent: type,
                    regionContent: region,
                    dataContent: value,
                    orderContent: order,
                    idPage: $scope.currentPageId 
                  });
              // }

            });

           //API Call to Edit Content

           var request = ContentsCRUD.update($scope.updatedContents.content);
           request.success(function(response){

                     //refresh preview

                     $scope.setPreview2(true) ; 
                     $scope.setEdit(true) ; 
                   });


                    //show notification
                    $('.top-right').notify({
                      message: { text: 'Page updated successfully' }
                    }).show();  



                   //scroll to the top of the page
                   window.scrollTo(0,0);
                                       location.reload();

                   $scope.currentPageUrl = $sce.trustAsResourceUrl('') ;



                 }     
               }

             });

}

//Edit Page Second Screen
$scope.editPage2 = function(){

 var request = PagesCRUD.get($scope.currentPageId);
 request.success(function(response2){
  if((response2.createdBy != $scope.Data.name())  && ($scope.Data.role()=='Editor')){

                    //show error if page is npt selected
                    $('.top-left').notify({
                      message: { 
                        text: 'This page can only be edited by the page creator or an administrator'
                      },
                      type : 'danger' 
                    }).show();      

                  }

                  else{

         //API Call to Edit Page Configuration
         $scope.editConfig.currentLanguage = $scope.currentLanguageId ;
         var request = PagesCRUD.update($scope.currentPageId,$scope.editConfig);
         request.success(function(response){

         });


         /* if data is  scraped */ if (response2.createdBy  == 'ScrapingMaster'){



                    // Empty updated contents array  
                    $scope.updatedContents2 = {
                      content: [{

                        nameContent: 'defaultName' ,
                        typeContent: 'defaultType',
                        regionContent: 'defaultRegion',
                        dataContent: 'defaultData',
                        orderContent: 'defaultOrder',
                        idPage: $scope.currentPageId 

                      }] 
                    };



                    $( "#edit-form2 .dataInput" ).each(function( index ) {


                     var inputElement = $(this) ; 
                     var type  = inputElement.attr('type') ; 
                     var id    = inputElement.attr('id') ; 
                     var region    = inputElement.attr('region') ; 
                     var value = inputElement.val() ; 
                     var order = index ; 



                // Add the values to the new contents array

                if(value!='')
                {

                  $scope.updatedContents.content.push({
                    nameContent: id ,
                    typeContent: type,
                    regionContent: region,
                    dataContent: value,
                    orderContent: order,
                    idPage: $scope.currentPageId 
                  });
                }

              });

           //API Call to Edit Content

           var request = ContentsCRUD.updateScraped($scope.updatedContents.content);
           request.success(function(response){
           });



                                //show notification
                                $('.top-right').notify({
                                  message: { text: 'Page updated successfully' }
                                }).show();  

                  //refresh preview
                  $scope.setPreview2(true) ; 
                  $scope.setEdit(true) ; 

                   //scroll to the top of the page
                   window.scrollTo(0,0);
                   $scope.currentPageUrl = $sce.trustAsResourceUrl('') ;
                 }
                 /* if data is not scraped */else {

                               // Empty updated contents array  
                               $scope.updatedContents2 = {
                                content: [{

                                  nameContent: 'defaultName' ,
                                  typeContent: 'defaultType',
                                  regionContent: 'defaultRegion',
                                  dataContent: 'defaultData',
                                  orderContent: 'defaultOrder',
                                  idPage: $scope.currentPageId 

                                }] 
                              };

                              $scope.editConfig.editedBy = $scope.Data.name() ;

                              $( "#edit-form2 .dataInput" ).each(function( index ) {


                               var inputElement = $(this) ; 
                               var type  = inputElement.attr('type') ; 
                               var id    = inputElement.attr('elementid') ; 
                               var region    = inputElement.attr('region') ; 
                               var value = inputElement.val() ; 
                               var order = index ; 



                // Add the values to the new contents array

                // if(value!='')
                // {

                  $scope.updatedContents.content.push({
                    nameContent: id ,
                    typeContent: type,
                    regionContent: region,
                    dataContent: value,
                    orderContent: order,
                    idPage: $scope.currentPageId 
                  });
              // }

            });

           //API Call to Edit Content

           var request = ContentsCRUD.update($scope.updatedContents.content);
           request.success(function(response){

                     //refresh preview

                     $scope.setPreview2(true) ; 
                     $scope.setEdit(true) ; 
                   });



                                //show notification
                                $('.top-right').notify({
                                  message: { text: 'Page updated successfully' }
                                }).show();  



                   //scroll to the top of the page
                   window.scrollTo(0,0);
                   $scope.currentPageUrl = $sce.trustAsResourceUrl('') ;


                 }
               }
             });

}
// Validate Page Configurations
$scope.validatePage = function(){
  // var tagNames=$scope.tags.map(function(tag) { return tag.namePage; });
  $scope.newPage.tags = $('#tags-jquery').val();
  $scope.newPage.idBrand = $scope.currentBrandId ;
  $scope.newPage.currentLanguage = $scope.currentLanguageId;

  if ($scope.Data.role()=='Editor'){
    if($('#tags-jquery').val()==''){
          //show error if page is not selected
          $('.top-left').notify({
            message: { 
              text: 'Please add one tag at least'
            },
            type : 'danger' 
          }).show();      

    }
      var request = PagesCRUD.createDraft($scope.newPage);
  }
  else if ($scope.Data.role()!='Editor' ){
    if($('#tags-jquery').val()==''){
          //show error if page is npt selected
          $('.top-left').notify({
            message: { 
              text: 'Please add one tag at least'
            },
            type : 'danger' 
          }).show();      

    }
  var request = PagesCRUD.create($scope.newPage);

  }
  request.success(function(response){


    if (response=='NO') {

                    //show error if page is npt selected
                    $('.top-left').notify({
                      message: { 
                        text: 'This page exsists please choose another page name'
                      },
                      type : 'danger' 
                    }).show();      
    }
    else{

      
  $scope.editConfig = response ;  

  $scope.dropTitle = 'Edit' ;

    $scope.newPageId = response.idPage ; 
    console.log(response.idPage);
    $scope.currentPageName = $sce.trustAsResourceUrl(response.namePage);
    $scope.flash = response.status;

    var request2 = PagesCRUD.pageLanguageUrl($scope.currentPageId,$scope.currentLanguageId);
    request2.success(function(response){
      $scope.currentPageUrl2 = $sce.trustAsResourceUrl(response.urlPage);

    });

      var request = TemplatesCRUD.getContent($scope.editConfig.idTemplate);
  request.success(function(response){
    $scope.templateContent =  $sce.trustAsHtml(response) ;

  });

    $scope.setPage(response.urlPage,response.namePage,response.idPage);
  $scope.edit = true ;
  $scope.add=false ;
  $scope.preview=false ;
  $scope.preview2=false ;

     location.reload();

 
     }
  });



}

// Delete a page
$scope.open = function (size,id) {

  $scope.id = id   ;

  var modalInstance = $modal.open({
    templateUrl: 'assetsModal.html',
    controller: 'ModalInstanceController',
    size: size,
    resolve: {
      items: function () {
        return $scope.items;


      },

      id: function() {

       return $scope.id;

     }
   }
 });

  modalInstance.result.then(function (selectedItem) {
    $scope.selected = selectedItem;
  }, function () {
    $log.info('Modal dismissed at: ' + new Date());
  });

  console.log(id) ;
};
$scope.idDelete = '';

$scope.deletePage = function(size,id){

  $scope.idDelete = id   ;
  var modalInstance = $modal.open({
    templateUrl: 'deleteModal.html',
    controller: 'DeleteModalController',
    size: size,
    resolve: {


      idDelete: function() {

       return $scope.idDelete;

     }
   }
 });
}
}); // End of controller

//jQuery Code for sliding Panels 
$(document).on("click", ".panel-heading", function () {
 $(this).next().toggle() ; 
});




