var dashboard = angular.module('ElementCtrl',[]);

dashboard.controller('ElementController',function($scope,$sce,$location,$modal,$route,$templateCache,$log,Login,SessionService,ElementsCRUD,PagesCRUD,LanguagesCRUD,TemplatesCRUD,ContentsCRUD){
  


  $scope.Data = {};
  $scope.Data.name = function() { return  SessionService.get('name') ; };
  $scope.Data.role = function() { return  SessionService.get('role') ; };
  $scope.Data.last_login = function() { return  SessionService.get('last_login') ; };

  console.log('your name is :'+$scope.Data.name()) ;    

  if($scope.Data.name() == null)  
  {
    $location.path('/');
  }
  /* Initialisations */ 
  $scope.currentElementCreator = '' ;
  $scope.currentLanguageId = 1;
  $scope.currentLanguageId2 = 1;
  $scope.currentElementId = false;
  $scope.newElementId = 3;
  $scope.currentElementName = 'Select Element';
  $scope.currentLanguageName = 'English';
  $scope.dropTitle2 = 'Preview' ;   
  $scope.dropTitle = 'View'  ;
  $scope.edit = false ;
  $scope.edit2 = false ;
  $scope.preview = false;
  $scope.preview2 = true;
  $scope.add = true ;
  $scope.currentElementUrl = $sce.trustAsResourceUrl('http://www.medlanes.com');
  $scope.currentElementUrl2 = $sce.trustAsResourceUrl('http://www.medlanes.com');
  $scope.otherLanguageFilename1 = 'Select Filename' ; 

      //Edit fields Array 
      var editFields = [{
       idField: 'defaultID' ,
       typeField: 'defaultType',
       regionField: 'defaultRegion'

     }]

     var dbFields = [{

       dbid: 'defaultIDdb' ,
       dbtype: 'defaultTypedb',
       region: 'defaultRegiondb'

     }]


     var editFields2 = [{

       idField: 'defaultID' ,
       typeField: 'defaultType',
       regionField: 'defaultRegion'

     }]

     var dbFields2 = [{

       dbid: 'defaultIDdb' ,
       dbtype: 'defaultTypedb',
       region: 'defaultRegiondb'

     }]


      // new contents array  
      $scope.newContents = {
        content: [{

          nameContent: 'defaultName' ,
          typeContent: 'defaultType',
          regionContent: 'defaultRegion',
          dataContent: 'defaultData',
          orderContent: 'defaultOrder',
          idElement: $scope.newElementId 

        }] 
      };


          // updated contents array  
          $scope.updatedContents = {
            content: [{

              nameContent: 'defaultName' ,
              typeContent: 'defaultType',
              regionContent: 'defaultRegion',
              dataContent: 'defaultData',
              orderContent: 'defaultOrder',
              idElement: $scope.currentElementId 

            }] 
          };

          $scope.updatedContents2 = {
            content: [{

              nameContent: 'defaultName' ,
              typeContent: 'defaultType',
              regionContent: 'defaultRegion',
              dataContent: 'defaultData',
              orderContent: 'defaultOrder',
              idElement: $scope.currentElementId 

            }] 
          };


          $scope.setOtherLanguageFile=function(filename,pagename,languageName){


            $('.'+languageName).val(filename) ;
            $('.'+languageName).trigger('input');
            $scope.otherLanguageFilename1 = pagename ; 
          }
// Logout functionality
$scope.logout = function(){

  SessionService.unset('name') ;
  SessionService.unset('auth') ;
  SessionService.unset('role') ;
  $location.path('/');

}

/* Get list of the pages according to the active language*/ 
var getPages = PagesCRUD.getPagesbyLanguage($scope.currentLanguageId);
getPages.success(function(response){
 $scope.pages = response;

});

/* Get list of the elements according to the active language*/ 
var getElements = ElementsCRUD.getElementsbyLanguage($scope.currentLanguageId);
getElements.success(function(response){
 $scope.elements = response;

});

// Load Tags 
 $scope.loadTags = function($query) {

      var pages = $scope.pages ;
      return pages.filter(function(page) {
        return page.namePage.toLowerCase().indexOf($query.toLowerCase()) != -1;
      });
      
  }

/* Get a list of Languages */ 
var getALLLanguages = LanguagesCRUD.all();
getALLLanguages.success(function(response){
 $scope.languages = response;

});

/* Get a list of Templates */ 
var getALLTemplates = TemplatesCRUD.getElementsTemplateByLanguage($scope.currentLanguageId);
getALLTemplates.success(function(response){
 $scope.templates = response;

});

/* Get the language URLs of a specific page for the left side */ 
$scope.getLanguage = function(idElement,idLanguage,nameLanguage){
  // var request = ElementsCRUD.getElementUrl(idElement);
  // request.success(function(response){
    // $scope.elementLanguageUrl = response;
    // $scope.currentElementUrl = $sce.trustAsResourceUrl($scope.elementLanguageUrl) ;
    $scope.currentLanguageId = idLanguage ;  
    $scope.currentLanguageName = nameLanguage ;  
    $scope.currentElementName = 'Select Page' ;
    $scope.add = true ; 
    $scope.edit = false ; 
    $scope.preview = true ; 
    $scope.dropTitle = 'Preview'
    $scope.newElement.currentLanguage =  $scope.currentLanguageId ;
/* Get list of the elements according to the active language*/ 
var getElements = ElementsCRUD.getElementsbyLanguage($scope.currentLanguageId);
getElements.success(function(response){
 $scope.elements = response;

});


    /* Get a list of Templates */ 
    var getALLTemplates = TemplatesCRUD.getTemplateByLanguage($scope.currentLanguageId);
    getALLTemplates.success(function(response){
     $scope.templates = response;
   });

  // });
}

/* Get the language URLs of a specific page for the right side */ 

$scope.getLanguage2 = function(idElement,idLanguage,nameLanguage){


  var request = ElementsCRUD.getElementUrl(idPage);
  request.success(function(response){
    $scope.elementLanguageUrl = response;
    $scope.currentElementUrl2 = $sce.trustAsResourceUrl($scope.elementLanguageUrl) ;
    $scope.currentLanguageId2 = idLanguage ;  
    $scope.currentLanguageName2 = nameLanguage ;  
    $scope.currentPageName2 = 'Select Page' ;
    $scope.edit2 = false ; 
    $scope.preview2 = true ; 
    $scope.dropTitle2 = 'Preview'
    /* Get list of the elements according to the active language*/ 
var getElements = ElementsCRUD.getElementsbyLanguage($scope.currentLanguageId);
getElements.success(function(response){
 $scope.elements = response;

});

             });

}                            
/* Set button values and changes the views accordingly */ 

$scope.setElement = function(url,name,id) {


                $scope.currentElementId = id ;       
                $scope.currentElementName = name;
                var request = ElementsCRUD.getElementUrl($scope.currentElementId);
                request.success(function(response){
                  $scope.currentElementUrl = $sce.trustAsResourceUrl(response);
                  $scope.currentElementUrl2 = $sce.trustAsResourceUrl(response);

                });


               //Get the template fields for editing
               var request = ElementsCRUD.getElementTemplate($scope.currentElementId);
               request.success(function(response){
                $scope.templateContentEdit =  $sce.trustAsHtml(response) ;

              });


               //Set Values in Edit fields according to page content

               var request = ContentsCRUD.getElementContents($scope.currentElementId);
               request.success(function(response){
                angular.forEach(response, function(value, key) {
                  console.log(key + ': ' + value.nameContent + ' : '+ value.dataContent);
                  var element = angular.element("#"+value.nameContent);
                  var val = value.dataContent;   
                  element.val(val);


                });



              });

               $scope.setEdit(true) ;

             }



          $scope.setEdit = function(val) {

    
              $scope.add = false ; 
          //empty the editing arrays 
          var editFields = [{

           idField: 'defaultID' ,
           typeField: 'defaultType',
           regionField: 'defaultRegion'

         }]

         var dbFields = [{

           dbid: 'defaultIDdb' ,
           dbtype: 'defaultTypedb',
           region: 'defaultRegiondb'

         }]



         if(($scope.currentElementId)==false)
         {
            //show error if page is npt selected
            $('.top-left').notify({
              message: { 
                text: 'Please select a page'
              },
              type : 'danger' 
            }).show();  

          }

                  else {

                    $scope.edit = val
                    $scope.preview = false;
                    $scope.dropTitle = 'Edit' ; 

           //Get the template fields for editing
           var request = ElementsCRUD.getElementTemplate($scope.currentElementId);
           request.success(function(response){
            $scope.templateContentEdit2 =  $sce.trustAsHtml(response) ;
          });

              //get the fields of the page configuration 
              var request = ElementsCRUD.get($scope.currentElementId);
              request.success(function(response){


                $scope.editConfig =  response ;
                $scope.editConfig.currentLanguage = $scope.currentLanguageId ; 
                $('#tags-jquery-element-edit').importTags($scope.editConfig.tags);


              });
              //fill editFields array 

              $('#edit-form .dataInput').each(function(){

                idField = $(this).attr('elementid') ; 
                regionField = $(this).attr('region') ; 
                typeField = $(this).attr('type') ; 

                editFields.push({
                  idField: idField,
                  regionField: regionField, 
                  typeField : typeField 
                });

              });

             //Get contents by page   
             var request = ContentsCRUD.getElementContents($scope.currentElementId);
             request.success(function(response){
              $.each(response, function(value, key) {
                dbFields.push({
                  dbid: key.nameContent,
                  region: key.regionContent, 
                  dbtype : key.typeContent 
                });

              });


              angular.forEach(response, function(value, key) {
                var bulletName = value.nameContent.substr(0,value.nameContent.indexOf('bullet')+9) ; 
                console.log (bulletName) ;
                if (value.typeContent == 'bullet' && value.nameContent!=bulletName)
                {
                 $('#edit-form .'+bulletName).append('<div id="'+value.nameContent+'" region="'+value.regionContent+'"><input type="bullet" id="'+value.nameContent+'" elementid="'+value.nameContent+'"   region="'+value.regionContent+'"  class="form-control dataInput bullet-input" /><button  class="btn btn-danger delete-bullet-button" ><span class="glyphicon glyphicon-minus-sign"></span></button></div>') ;
                 }

               var element = angular.element("#edit-form #"+value.nameContent);
               var val = value.dataContent;   
               element.val(val);


                });
                  });
}
}




$scope.setAdd = function(val) {

  // $('#templateDropdown').val(null);
  // $('#templateDropdown').prop( "disabled", true );
  $scope.newElement.idTemplate = 0 ;
  $scope.templateContent = '' ;
  $scope.add = val
  $scope.preview = false;
  $scope.preview2 = true;
  $scope.edit = false ;


}




/* Create a  new page */ 

$scope.createElement = function(){



                    // empty new contents array  
                    $scope.newContents = {
                      content: [{

                        nameContent: 'defaultName' ,
                        typeContent: 'defaultType',
                        regionContent: 'defaultRegion',
                        dataContent: 'defaultData',
                        orderContent: 'defaultOrder',
                        idPage : null , 
                        idElement: $scope.newElementId 

                      }] 
                    };




                     var link = $('#inputURL').val() ; 
                     $('#question_link').val(link) ;

                    $( "#add-form .dataInput" ).each(function(index) {

                     var inputElement = $(this) ; 

                     var type  = inputElement.attr('type') ; 
                     var region    = inputElement.attr('region') ; 
                     var value = inputElement.val() ; 
                     var order = index ; 
                     var id    = inputElement.attr('elementid') ;  ; 




                // Add the values to the new contents array

                $scope.newContents.content.push({
                  nameContent: id ,
                  typeContent: type,
                  regionContent: region,
                  dataContent: value,
                  orderContent: order,
                  idPage : null ,
                  idElement: $scope.newElementId 
                });


              });



                    var request = ContentsCRUD.createElement($scope.newContents.content);
                    request.success(function(response){

                    });


                     //change to edit 

                     $scope.setElement($scope.currentElementUrl,$scope.currentElementName,$scope.newElementId);
                     $scope.setEdit(true) ; 


                      //show notification
                      $('.top-right').notify({
                        message: { text: 'The page has been added successfully' }
                      }).show();  

                      //scroll to the top of the page

                      window.scrollTo(0,0);

                 // Refresh pages list
                 var getElements = ElementsCRUD.getElementsbyLanguage($scope.currentLanguageId);
                 getElements.success(function(response){
                   $scope.elements = response;
                   $scope.add = false ;
                   $scope.edit = true ; 

                 });

}



$scope.editElement = function(){

  var request = ElementsCRUD.get($scope.currentElementId);
  request.success(function(response2){
    if((response2.createdBy != $scope.Data.name())  && ($scope.Data.role()=='Editor')){

                    //show error if page is npt selected
                    $('.top-left').notify({
                      message: { 
                        text: 'This page can only be edited by the page creator or an administrator'
                      },
                      type : 'danger' 
                    }).show();      

                  }


                  else{


                //API Call to Edit Page Configuration
                $scope.editConfig.tags = $('#tags-jquery-element-edit').val();
                var request = ElementsCRUD.update($scope.currentElementId,$scope.editConfig);
                request.success(function(response){
                });


                               // Empty updated contents array  
                               $scope.updatedContents = {
                                content: [{

                                  nameContent: 'defaultName' ,
                                  typeContent: 'defaultType',
                                  regionContent: 'defaultRegion',
                                  dataContent: 'defaultData',
                                  orderContent: 'defaultOrder',
                                  idElement: $scope.currentElementId 

                                }] 
                              };
                      
                              
                              $('#question_link').val($scope.editConfig.urlElement) ;
                              $( "#edit-form .dataInput" ).each(function( index ) {


                               var inputElement = $(this) ; 
                               var type  = inputElement.attr('type') ; 
                               var id    = inputElement.attr('elementid') ; 
                               var region    = inputElement.attr('region') ; 
                               var value = inputElement.val() ; 
                               var order = index ; 



                // Add the values to the new contents array

                // if(value!='')
                // {

                  $scope.updatedContents.content.push({
                    nameContent: id ,
                    typeContent: type,
                    regionContent: region,
                    dataContent: value,
                    orderContent: order,
                    idElement: $scope.currentElementId 
                  });
              // }

            });


           //API Call to Edit Content

           var request = ContentsCRUD.updateElement($scope.updatedContents.content);
           request.success(function(response){

                     //refresh preview

                     $scope.setPreview2(true) ; 
                     $scope.setEdit(true) ; 
                   });



                                //show notification
                                $('.top-right').notify({
                                  message: { text: 'Page updated successfully' }
                                }).show();  



                   //scroll to the top of the page
                   window.scrollTo(0,0);
                   // location.reload();
                   $scope.currentPageUrl = $sce.trustAsResourceUrl('') ;



                 }   
                 });             

}






// Validate Element Configuration

$scope.validateElement = function(){
  $scope.newElement.tags = $('#tags-jquery-element').val();

       if($('#tags-jquery-element').val()==''){
                //show error if page is npt selected
                $('.top-left').notify({
                  message: { 
                    text: 'Please add one tag at least'
                  },
                  type : 'danger' 
                }).show();      

          }

  var request = ElementsCRUD.create($scope.newElement);
  request.success(function(response){

     if (response=='NO') {

                    //show error if page is npt selected
                    $('.top-left').notify({
                      message: { 
                        text: 'This page exsits please choose another page name'
                      },
                      type : 'danger' 
                    }).show();      
    }
    else{
  $scope.newElementId = response.idElement ; 
  console.log(response.idElement);
  $scope.currentElementName = $sce.trustAsResourceUrl(response.nameElement);
  $scope.flash = response.status;
  var request2 = ElementsCRUD.getElementUrl($scope.currentElementId);
  request2.success(function(response){
  $scope.currentElementUrl2 = $sce.trustAsResourceUrl(response);

    });

    var request = TemplatesCRUD.getContent($scope.newElement.idTemplate);
  request.success(function(response){
    $scope.templateContent =  $sce.trustAsHtml(response) ;

  });

}
  });





}




// Delete a page






$scope.open = function (size,id) {

  $scope.id = id   ;

  var modalInstance = $modal.open({
    templateUrl: 'assetsModal.html',
    controller: 'ModalInstanceController',
    size: size,
    resolve: {
      items: function () {
        return $scope.items;


      },

      id: function() {

       return $scope.id;

     }
   }
 });

  modalInstance.result.then(function (selectedItem) {
    $scope.selected = selectedItem;
  }, function () {
    $log.info('Modal dismissed at: ' + new Date());
  });

  console.log(id) ;
};







$scope.idDelete = '';

$scope.deleteElement = function(size,id){

  $scope.idDelete = id   ;
  var modalInstance = $modal.open({
    templateUrl: 'deleteModal.html',
    controller: 'DeleteElementModalController',
    size: size,
    resolve: {


      idDelete: function() {

       return $scope.idDelete;

     }
   }
 });
}








}); // End of controller



