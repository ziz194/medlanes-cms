var login = angular.module('LoginCtrl',[]);
 
login.controller('LoginController',function($scope,$location,Login,SessionService){
$scope.loginSubmit = function(){
var auth = Login.auth($scope.loginData);
auth.success(function(response){

if(response.id){


 SessionService.set('auth',true); //This sets our session key/val pair as authenticated
 SessionService.set('id',response.id ); 
 SessionService.set('name',response.first_name+' '+response.last_name); 
 SessionService.set('role',response.role ); 
 SessionService.set('last_login',response.last_login ); 

 $location.path('/dashboard');
 }else alert('Could not verify your login');
 });
 }
});




