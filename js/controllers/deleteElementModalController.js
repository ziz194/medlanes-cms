var article = angular.module('DeleteElementModalCtrl',[]);
 
article.controller('DeleteElementModalController',function($scope, $modalInstance, $log,idDelete,$http,ElementsCRUD){



    $scope.idDelete = idDelete;


  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };


      $scope.deleteElement = function () {

                   var request = ElementsCRUD.delete($scope.idDelete);
                  request.success(function(response){

            });

                   $modalInstance.dismiss();

                      $('.top-left').notify({
                      message: { text: 'The page has been removed successfully' }
                    }).show();  


                            location.reload();



  };



});