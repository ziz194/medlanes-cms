var article = angular.module('UsersCtrl',[]);

article.controller('UsersController',function($scope,$location,$sce,UsersCRUD,BrandsCRUD,ElementsCRUD,AssetsCRUD,SessionService,LanguagesCRUD){


  // Initialisations 

  $scope.edit = false ; 
  $scope.add = true ; 
  $scope.Data = {};
  $scope.editUserId = 0 ; 
  $scope.userNames =[] ;
  $scope.numberPages =[] ;
  $scope.totalPages = 0





  $scope.Data.name = function() { return  SessionService.get('name') ; };
    $scope.Data.id = function() { return  SessionService.get('id') ; };
      $scope.Data.last_login = function() { return  SessionService.get('last_login') ; };


var currentUserId = $scope.Data.id() ;

// Get Pages by Date

$scope.getPagesByDate = function(date1,date2){

   if((date1==undefined) || (date2==undefined))
                 {
                    //show error if page is npt selected
                    $('.top-left').notify({
                      message: { 
                        text: 'Please select a date range'
                      },
                      type : 'danger' 
                    }).show();  

                    return false ; 

                  }

                  else{


                
  var PagesByDate = UsersCRUD.getPagesByDate(date1,date2);
  PagesByDate.success(function(response){
   $scope.pagesByDate = response;
 });

    var TotalPagesByDate = UsersCRUD.getTotalPagesByDate(date1,date2);
  TotalPagesByDate.success(function(response){
   $scope.totalPagesByDate = response;
 });
  }
}


// Get Elements by Date

$scope.getElementsByDate = function(date1,date2){

   if((date1==undefined) || (date2==undefined))
                 {
                    //show error if page is npt selected
                    $('.top-left').notify({
                      message: { 
                        text: 'Please select a date range'
                      },
                      type : 'danger' 
                    }).show();  

                    return false ; 

                  }

                  else{


                
  var ElementsByDate = UsersCRUD.getElementsByDate(date1,date2);
  ElementsByDate.success(function(response){
   $scope.elementsByDate = response;
 });

    var TotalElementsByDate = UsersCRUD.getTotalElementsByDate(date1,date2);
  TotalElementsByDate.success(function(response){
   $scope.totalElementsByDate = response;
 });
  }
}


//Total assets
  var TotalAssetsNumber = UsersCRUD.getTotalAssetsNumber();
  TotalAssetsNumber.success(function(response){
   $scope.totalAssetsNumber = response;
 });

//Total elements
  var TotalElementsNumber = UsersCRUD.getTotalElementsNumber();
  TotalElementsNumber.success(function(response){
   $scope.totalElementsNumber = response;
 });



// Get Pages by Date

$scope.getAssetsByDate = function(date1,date2){

   if((date1==undefined) || (date2==undefined))
                 {
                    //show error if page is npt selected
                    $('.top-left').notify({
                      message: { 
                        text: 'Please select a date range'
                      },
                      type : 'danger' 
                    }).show();  

                    return false ; 

                  }

                  else{


                
  var AssetsByDate = UsersCRUD.getAssetsByDate(date1,date2);
  AssetsByDate.success(function(response){
   $scope.assetsByDate = response;
 });

    var TotalAssetsByDate = UsersCRUD.getTotalAssetsByDate(date1,date2);
  TotalAssetsByDate.success(function(response){
   $scope.totalAssetsByDate = response;
 });
  }
}
// Chart Data 


  /* Get a list of Users */ 
  var getALLUsers = UsersCRUD.all();
  getALLUsers.success(function(response){
   $scope.users = response;

    for (var i=0 ; i<$scope.users.length;i++) {
    $scope.userNames.push($scope.users[i].first_name) ;
    $scope.numberPages.push($scope.users[i].pageCount) ;
    $scope.totalPages = $scope.totalPages + $scope.users[i].pageCount ;
  };


 });

      var getALLBrands = BrandsCRUD.all();
      getALLBrands.success(function(response){
      $scope.brands = response;

                  });


            var getALLLanguages = LanguagesCRUD.all();
      getALLLanguages.success(function(response){
      $scope.languages = response;

                  });

    /* Get my account informations */ 
  var getMyAccount = UsersCRUD.get(currentUserId);
  getMyAccount.success(function(response){
   $scope.myaccount = response;

 });



                    $scope.pageCount = function(id){
           
                      var request = UsersCRUD.pageCount(id);
                      request.success(function(response){
                      return response ; 
                    });
                    }




  $scope.createUser = function(){

                      var request = UsersCRUD.create($scope.newUser);
                      request.success(function(response){

                          //show notification
                          $('.top-right').notify({
                            message: { text: 'The User has been added successfully' }
                          }).show();  



                          /* Get a list of Users */ 
                          var getALLUsers = UsersCRUD.all();
                          getALLUsers.success(function(response){
                           $scope.users = response;

                         });





                        });




                    }



                    $scope.removeUser = function(id){

                      var request = UsersCRUD.delete(id);
                      request.success(function(response){

                     //show notification
                     $('.top-right').notify({
                      message: { text: 'The User has been deleted successfully' }
                    }).show();  



                       /* Get a list of Users */ 
                          var getALLUsers = UsersCRUD.all();
                          getALLUsers.success(function(response){
                           $scope.users = response;

                         });




                   });



                    }



                    $scope.setEdit = function(id,value){

                      $scope.edit = value ;
                      $scope.add = false ;
                      $scope.editUserId = id ; 
                        // select the element to edit 


                  var request = UsersCRUD.get($scope.editUserId);
                  request.success(function(response){
                    $scope.editUser = response ; 
                  });



                }




                    $scope.setNew = function(value){

                      $scope.edit = false ;
                      $scope.add = value ;
                  
                }




                    $scope.editUserFunction = function(){

                      if (!($scope.editUser.password) ) 

                 {

                        //show error
                      $('.top-left').notify({

                         message: { 
                        text: 'Please enter a password'
                       },
                       type : 'danger' 
                    }).show();  
                }

                else{

                      var request = UsersCRUD.update($scope.editUserId,$scope.editUser);
                      request.success(function(response){

                     //show notification
                             $('.top-right').notify({
                              message: { text: 'The User has been updated successfully' }
                            }).show();  
                     /* Get a list of Users */ 
                          var getALLUsers = UsersCRUD.all();
                          getALLUsers.success(function(response){
                          $scope.users = response;

                         });



                   });
                    }

                    }

                


                    $scope.editAccount= function(){

                          if (!($scope.myaccount.password) ) 

                 {

                        //show error
                      $('.top-left').notify({

                         message: { 
                        text: 'Please enter a password'
                       },
                       type : 'danger' 
                    }).show();  
                }
                
                else{

                      var request = UsersCRUD.update(currentUserId,$scope.myaccount);
                      request.success(function(response){

                     //show notification
                             $('.top-right').notify({
                              message: { text: 'The User has been updated successfully' }
                            }).show();  
                    
                                /* Get my account informations */ 
                              var getMyAccount = UsersCRUD.get(currentUserId);
                              getMyAccount.success(function(response){
                               $scope.myaccount = response;

                             });

                               SessionService.set('name',$scope.myaccount.first_name) ; 



                   });

                      }


                }





// End of Settings Controller
});


