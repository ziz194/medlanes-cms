var article = angular.module('DeleteAssetModalCtrl',[]);
 
article.controller('DeleteAssetModalController',function($scope, $modalInstance, $log,idAsset,$http,AssetsCRUD){



    $scope.idAsset = idAsset;


  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };


      $scope.deleteAsset= function () {

                   var request = AssetsCRUD.delete($scope.idAsset);
                  request.success(function(response){

            });

                   $modalInstance.dismiss();

                      $('.top-left').notify({
                      message: { text: 'The page has been removed successfully' }
                    }).show();  


  };



});