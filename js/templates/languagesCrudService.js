
var crud = angular.module('LanguagesCrudSrvc',[]);
 
crud.factory("LanguagesCRUD",function($http){
 
    return{
        all: function(){
          //get all languages
          var request = $http({method:'GET', url:'api/languages'});
           return request;
        },
        create: function(data){
          //create a new language
           var request = $http({method:'GET', url:'api/languages/create',params:data});
            return request;
        },
        get: function(id){
          //get a specific language
            var request = $http.get('api/languages/'+id);
            return request;
        },
        update: function(id,data){
          //update a specific language
            var request = $http.put('api/languages/'+id,data);
            return request;
        },
        delete: function(id){
          
           var request = $http({method:'DELETE', url:'api/languages/'+id});
           return request;
        }
    }
 
});