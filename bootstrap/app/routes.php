<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/




Route::get('/', function()
{
	return View::make('admin');
});



	


Route::group(array('prefix'=>'api/'),function(){

//Authentification Controller
Route::get('login/auth','HomeController@Login');
Route::get('login/destroy','HomeController@Logout');
//Page Controller
Route::get('pages/findLanguageUrl/{pages}/{languages}','PageController@findLanguageUrl');
Route::get('pages/getPagesbyLanguage/{languages}','PageController@getPagesbylanguage');
Route::get('pages/getPageTemplate/{pages}','PageController@getPageTemplate');
Route::resource('pages','PageController');
Route::resource('articles','ArticleController');
Route::get('pages/getContentsbyPage/{pages}/{languages}','ContentController@getContentsbyPage');
Route::get('pages/getContents/{pages}','ContentController@getContents');

//Update page configuration
Route::get('pages/updatePage/{pages}/','PageController@updatePage');

//Language Controller
Route::resource('languages','LanguageController');
//Template Controller
Route::resource('templates','TemplateController');
Route::get('templates/getContent/{templates}','TemplateController@showContent') ;
Route::get('templates/getTemplateByLanguage/{languages}','TemplateController@getTemplateByLanguage') ;

Route::post('templates/addTemplate','TemplateController@addTemplate') ;
Route::post('templates/updateTemplate','TemplateController@updateTemplate') ;

//Content Controller
Route::resource('contents','ContentController');
Route::post('contents/addContents','ContentController@addContents') ;

//Asset Controller
Route::post('assets/uploadAsset','AssetController@uploadAsset') ;
Route::resource('assets','AssetController');

//Update Scraped Content
Route::get('contents/updateScrapedContent/{pages}/','ContentController@updateScrapedContent');
//Update Content
Route::post('contents/updateContent','ContentController@updateContent') ;



 
});

?>