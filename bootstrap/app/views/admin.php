<!doctype html>
<html lang="en">
<head>
<title>Medlanes CMS</title>
<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">

<!--css-->
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css"/>

<link rel="stylesheet" href="css/app.css"/>
<link rel="stylesheet" href="css/splitter.css"/>
<link rel="stylesheet" href="css/bootstrap-notify.css"/>
<link rel="stylesheet" href="css/assets.css"/>
<link rel="stylesheet" href="css/bootstrap-tagsinput.css"/>
<link rel="stylesheet" href="css/ng-tags-input.min.css"/>
<link rel="stylesheet" href="css/ng-tags-input.bootstrap.min.css"/>







 

<!--js-->



<!-- jQuery -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/start/jquery-ui.css" />




<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
 
<!--angular-->
<!--<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.15/angular.js"></script>-->
<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.15/angular-route.js"></script>
<!-- Bootstrap UI -->
<script src="js/ui-bootstrap-tpls-0.12.0.min.js"></script>
<!-- Bootstrap Tag input -->
<script src="js/bootstrap-tagsinput.js"></script>
<script src="js/ng-tags-input.min.js"></script>
<!-- Angular charts -->
<link rel="stylesheet" href="css/angular-chart.css">
<script src="js/Chart.js"></script>
<script src="js/angular-chart.js"></script>
<script src="js/angular-chart.min.js"></script>
<!--  -->



<!--angular controllers--><script src="js/app.js"></script>
<!--angular controllers--><script src="js/services/authService.js"></script>
<!--angular controllers--><script src="js/controllers/loginController.js"></script>
<!--angular controllers Pages--><script src="js/controllers/pageController.js"></script>
<!--angular controllers Languages--><script src="js/controllers/languageController.js"></script>
<!--angular controllers Templates--><script src="js/controllers/templateController.js"></script>
<!--angular controllers Assets--><script src="js/controllers/assetsController.js"></script>
<!--angular controllers Settings--><script src="js/controllers/settingsController.js"></script>
<!--angular controllers Settings--><script src="js/controllers/usersController.js"></script>
<!--angular controllers--><script src="js/controllers/dashboardController.js"></script>
<!--angular controllers--><script src="js/controllers/articleController.js"></script>
<!--angular controllers--><script src="js/controllers/elementController.js"></script>
<!--angular controllers--><script src="js/controllers/editArticleController.js"></script>
<!--angular controllers--><script src="js/controllers/modalController.js"></script>
<!--angular controllers--><script src="js/controllers/modalInstanceController.js"></script>
<!--angular controllers--><script src="js/controllers/deleteModalController.js"></script>
<!--angular controllers--><script src="js/controllers/deleteAssetModalController.js"></script>
<!--angular controllers--><script src="js/controllers/editAssetModalController.js"></script>
<!--angular controllers--><script src="js/controllers/deleteElementModalController.js"></script>
<!--angular controllers--><script src="js/splitter.js"></script>
<!--angular controllers--><script src="js/bootstrap-notify.js"></script>
<!--angular controllers--><script src="js/jquery.autosize.min.js"></script>




<!--angular services-->
<script src="js/services/usersCrudService.js"></script>
<script src="js/services/pagesCrudService.js"></script>
<script src="js/services/assetsCrudService.js"></script>
<script src="js/services/languagesCrudService.js"></script>
<script src="js/services/brandsCrudService.js"></script>
<script src="js/services/templatesCrudService.js"></script>
<script src="js/services/contentsCrudService.js"></script>
<script src="js/services/elementsCrudService.js"></script>
<script src="js/services/formDataModule.js"></script>

	<!--Directives start-->
	<script src="js/directives/cms-input/cms-input.js"></script>
	<script src="js/directives/cms-bullet/cms-bullet.js"></script>
	<script src="js/directives/cms-textarea/cms-textarea.js"></script>
	<script src="js/directives/cms-picture/cms-picture.js"></script>
	<script src="js/directives/cms-question/cms-question.js"></script>
	<script src="js/directives/fileModel.js"></script>
	<script src="js/directives/dirPagination.spec.js"></script>
	<script src="js/directives/dirPagination.js"></script>

	<!--Directives end-->





<!-- include summernote css/js-->
<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">
<link href="css/summernote.css" rel="stylesheet">
<script src="js/summernote.min.js"></script>
<!--  -->


<!-- ngDialog -->
<link href="css/ngDialog.min.css" rel="stylesheet">
<script src="js/ngDialog.min.js"></script>
<!--  -->

<!-- Tags jQuery -->
<script src="js/jquery.tagsinput.js"></script>
<link rel="stylesheet" type="text/css" href="css/jquery.tagsinput.css" />

</head>



<body ng-app="cmsApp">

 <div id="wrapper">
 <div class="container" id="view" ng-view>
 

 </div>
 </div>
</body>




</html>