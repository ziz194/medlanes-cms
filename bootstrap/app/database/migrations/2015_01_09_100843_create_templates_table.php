<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTemplatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
			 Schema::create('templates', function(Blueprint $table)
 			{
				$table->increments('idTemplate');
 				$table->string('nameTemplate');
				$table->string('urlTemplate')->unique();
 				$table->text('htmlTemplate');
 				$table->timestamps();//creates timestamps for created and updated at
 			});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('templates');
	}

}
