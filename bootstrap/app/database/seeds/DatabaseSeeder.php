<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		 $this->call('UserTableSeeder');
		 $this->call('TemplatesTableSeeder');
		 $this->call('PagesTableSeeder');
		 //$this->call('LanguagesTableSeeder');
		 $this->call('LanguagesPagesTableSeeder');

	}

}
