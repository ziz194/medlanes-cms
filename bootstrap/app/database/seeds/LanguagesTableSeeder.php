<?php
 
class LanguagesTableSeeder extends Seeder {
 
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    DB::table('languages')->delete();
 
        Language::create(array(
 
           'nameLanguage' => 'English',
           'urlLanguage' => 'http://www.medlanes.com/doctors'

        ));

           Language::create(array(
 
           'nameLanguage' => 'German',
           'urlLanguage' => 'http://www.medlanes.com/faq'

        ));

             
 
    }
 
}

?>