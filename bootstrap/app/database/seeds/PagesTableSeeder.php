<?php
 
class PagesTableSeeder extends Seeder {
 
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    DB::table('pages')->delete();
 
        Page::create(array(
 
           'namePage' => 'Doctors Page',
           'urlPage' => 'http://www.medlanes.com/doctors'

        ));

           Page::create(array(
 
           'namePage' => 'FAQ Page',
           'urlPage' => 'http://www.medlanes.com/faq'

        ));

                 Page::create(array(
 
           'namePage' => 'Home Page',
           'urlPage' => 'http://www.medlanes.com/'

        ));
 
    }
 
}

?>