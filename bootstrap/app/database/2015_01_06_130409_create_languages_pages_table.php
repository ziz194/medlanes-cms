<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLanguagesPagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('languages_pages', function(Blueprint $table)
		{
 
        $table->increments('id');
        $table->unsignedinteger('idLanguage');
        $table->unsignedinteger('idPage');
        $table->string('languagePageUrl');
        $table->timestamps();//creates timestamps for created and updated at


		});

		   Schema::table('languages_pages', function(Blueprint $table)
    {
        $table->foreign('idLanguage')->references('idLanguage')->on('languages');
        $table->foreign('idPage')->references('idPage')->on('pages');
    });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('languages_pages', function(Blueprint $table)
		{
			//
		});
	}

}
