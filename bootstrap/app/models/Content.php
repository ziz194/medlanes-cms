<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Content extends Eloquent {
 
    /**
     * The database table used by the model.CRUD Service
     *
     * @var string
     */
    protected $table = 'contents';
    protected $primaryKey = 'idContent';

    
 
}