<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Template extends Eloquent {
 
    /**
     * The database table used by the model.CRUD Service
     *
     * @var string
     */
    protected $table = 'templates';
    protected $primaryKey = 'idTemplate';

     public function pages()
    {
        return $this->hasMany('Page','idPage');
    }


    public function language()
    {
        return $this->hasOne('Language','idLanguage');
    }

 
}