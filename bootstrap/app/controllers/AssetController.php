<?php

class AssetController extends \BaseController {





		/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function uploadAsset()
	{


                $file =  Input::file('file') ;
                $fileName =  $file->getClientOriginalName() ;
                $size = $file->getSize() ;

                if ($file->isValid())

                {
              if($file->move('cms_assets/',$fileName))

                {

                	echo 'file moved' ; 
                	     $newPath = 'cms_assets/thumbnails/'.$fileName ;

                Image::make('cms_assets/'.$fileName,array(
					    'width' => 300,
					    'height' => 300,
					    'greyscale' => true
					))->save($newPath);





                }


     }



		     $asset = new Asset;
		     $asset->nameAsset     =   Input::get('name');
			 $asset->descriptionAsset   =   Input::get('description');
			 $asset->typeAsset   =   'image';
			 $asset->urlAsset     =   $fileName ;
			 $asset->sizeAsset    =   $size ; 
			 $asset->save() ; 


	}





	public function updateAsset()
	{

 
                $file =  Input::file('file') ;
                $fileName =  $file->getClientOriginalName() ;
                $size = $file->getSize() ;

                if ($file->isValid())

                {
              if($file->move('cms_assets/',$fileName))

                {

                	echo 'file moved' ; 
                	     $newPath = 'cms_assets/thumbnails/'.$fileName ;

                Image::make('cms_assets/'.$fileName,array(
					    'width' => 300,
					    'height' => 300,
					    'greyscale' => true
					))->save($newPath);





                }


     }


     		 $id = Input::get('id');
		     $asset = Asset::find($id);
		     $asset->nameAsset     =   Input::get('name');
			 $asset->descriptionAsset   =   Input::get('description');
			 $asset->typeAsset   =   'image';
			 $asset->urlAsset     =   $fileName ;
			 $asset->sizeAsset    =   $size ; 
			 $asset->save() ; 


	}



	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

		$assets = Asset::all();
        return $assets;

	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{




	}




	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{



	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$asset = Asset::find($id) ; 
		return $asset ; 
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		
              
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		 

	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
				$asset = Asset::find($id);		

				var_dump($asset) ;
				$asset->delete();

				//Delete the  file 
				File::delete('cms_assets/'.$asset->urlAsset);
	}




	public function getAssetsByDate($startDate,$endDate) 
	{ 
								
			$date1 = strtotime($startDate);
			$date2 = strtotime($endDate);
			$startDate = date('Y-m-d H:i:s',$date1) ; 
			$endDate =   date('Y-m-d H:i:s',$date2) ; 
  			$assets= Asset::whereBetween('created_at',array($startDate,$endDate))->orderBy('created_at', 'desc')->get();     
    		return $assets ;
	}

	public function getTotalAssetsByDate($startDate,$endDate) 
	{

			$date1 = strtotime($startDate);
			$date2 = strtotime($endDate);
			$startDate = date('Y-m-d H:i:s',$date1) ; 
			$endDate =   date('Y-m-d H:i:s',$date2) ; 
  			$totalAssets = Asset::whereBetween('created_at',array($startDate,$endDate))->orderBy('created_at', 'desc')->count();
  			return $totalAssets ; 
  	}


  		public function getTotalAssetsNumber() 
	{


  			$totalAssets = Asset::all()->count();
  			return $totalAssets ; 
  	}




}
