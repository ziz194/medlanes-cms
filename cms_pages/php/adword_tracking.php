<?php
	if(isset($_GET["gclid"])){
		$_SESSION["adword"] = $_GET["gclid"];
	}
	if(isset($_GET["network"])){
		$_SESSION["network"] = $_GET["network"];
	}
	if(isset($_GET["adposition"])){
		$_SESSION["adposition"] = $_GET["adposition"];
	}
	if(isset($_GET["devicemodel"])){
		$_SESSION["devicemodel"] = $_GET["devicemodel"];
	}
	if(isset($_GET["matchtype"])){
		$_SESSION["matchtype"] = $_GET["matchtype"];
	}
	if(isset($_GET["keyword"])){
		$_SESSION["keyword"] = $_GET["keyword"];
	}
	if(isset($_GET["creative"])){
		$_SESSION["creative"] = $_GET["creative"];
	}
	function getPaypalAdword(){
		$adwordstr = "";
		if(isset($_SESSION["adword"]) and strlen($_SESSION["adword"])>0){
			$adwordstr .= "&adword=" . $_SESSION["adword"];
		}
		if(isset($_SESSION["network"]) and strlen($_SESSION["network"])>0){
			//$adwordstr .= "&network=" . $_SESSION["network"];
		}
		if(isset($_SESSION["adposition"]) and strlen($_SESSION["adposition"])>0){
			//$adwordstr .= "&adposition=" . $_SESSION["adposition"];
		}
		if(isset($_SESSION["devicemodel"]) and strlen($_SESSION["devicemodel"])>0){
			//$adwordstr .= "&devicemodel=" . $_SESSION["devicemodel"];
		}
		if(isset($_SESSION["matchtype"]) and strlen($_SESSION["matchtype"])>0){
			//$adwordstr .= "&matchtype=" . $_SESSION["matchtype"];
		}
		if(isset($_SESSION["keyword"]) and strlen($_SESSION["keyword"])>0){
			//$adwordstr .= "&keyword=" . $_SESSION["keyword"];
		}
		if(isset($_SESSION["creative"]) and strlen($_SESSION["creative"])>0){
			//$adwordstr .= "&creative=" . $_SESSION["creative"];
		}
		if(isset($_SESSION["geo_ip"]) and strlen($_SESSION["geo_ip"])>0){
			//$adwordstr .= "&geo_ip=" . $_SESSION["geo_ip"];
		}
		if(isset($_SESSION["geo_city"]) and strlen($_SESSION["geo_city"])>0){
			$adwordstr .= "&geo_city=" . htmlentities($_SESSION["geo_city"]);
		}
		if(isset($_SESSION["geo_country"]) and strlen($_SESSION["geo_country"])>0){
			$adwordstr .= "&geo_country=" . htmlentities($_SESSION["geo_country"]);
		}
		return $adwordstr;
	}
	function getStripeAdword($meta){
		if(isset($_SESSION["adword"]) and strlen($_SESSION["adword"])>0){
			$meta["adword"] = $_SESSION["adword"];
		}
		if(isset($_SESSION["network"]) and strlen($_SESSION["network"])>0){
			$meta["network"] = $_SESSION["network"];
		}
		if(isset($_SESSION["adposition"]) and strlen($_SESSION["adposition"])>0){
			$meta["adposition"] = $_SESSION["adposition"];
		}
		if(isset($_SESSION["devicemodel"]) and strlen($_SESSION["devicemodel"])>0){
			$meta["devicemodel"] = $_SESSION["devicemodel"];
		}
		if(isset($_SESSION["matchtype"]) and strlen($_SESSION["matchtype"])>0){
			$meta["matchtype"] = $_SESSION["matchtype"];
		}
		if(isset($_SESSION["keyword"]) and strlen($_SESSION["keyword"])>0){
			$meta["keyword"] = $_SESSION["keyword"];
		}
		if(isset($_SESSION["creative"]) and strlen($_SESSION["creative"])>0){
			$meta["creative"] = $_SESSION["creative"];
		}
		if(isset($_SESSION["geo_ip"]) and strlen($_SESSION["geo_ip"])>0){
			$meta["geo_ip"] = $_SESSION["geo_ip"];
		}
		if(isset($_SESSION["geo_city"]) and strlen($_SESSION["geo_city"])>0){
			$meta["geo_city"] = $_SESSION["geo_city"];
		}
		if(isset($_SESSION["geo_country"]) and strlen($_SESSION["geo_country"])>0){
			$meta["geo_country"] = $_SESSION["geo_country"];
		}
		return $meta;
	}
?>