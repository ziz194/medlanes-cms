<?php

/**
* Api call transaction_finish
*
*
* @access	public
* @param	String Ticket ID
* @param	String Token
* @param	boolean true for Stage, false for live
* @param	String Gross amount user has paid
* @param	String Currency in which user paid, default USD
* @return	void
*/
function api_call_finish_transaction($idTicket, $token, $stage = false, $amount = null, $currency = "USD", $funnel="", $company="") {
//	var_dump($idTicket, $token, $stage, $amount, $currency, $funnel, $company);
//	die("jovan");
    $request_arr = array(
        "action" => "transaction_finish",
        "auth" => array(
            "token" => $token
        ),
        "parameters" => array(
            "id" => $idTicket,
            "currency"=>$currency
        )
    );
    if($amount != null){
        $request_arr["parameters"]["amount"] = $amount;
    }
    if($funnel != null){
    	$request_arr["parameters"]["funnel"] = $funnel;
    }
	if($company != null){
    	$request_arr["parameters"]["company"] = $company;
    }
    
    $requeststr = json_encode($request_arr);
    if ($stage == true)
        curlCall('http://stage-api.medlanes.com/index.php?/api_1/call/json', array('request' => $requeststr));
    else
        curlCall('http://api.medlanes.com/index.php?/api_1/call/json', array('request' => $requeststr));
}

function curlCall($end_point, $data) {
    //open connection
    $ch = curl_init();

    //set the url, number of POST vars, POST data
    curl_setopt_array($ch, array(
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_URL => $end_point,
        CURLOPT_USERAGENT => 'Sample cURL Request',
        CURLOPT_POST => 1,
        CURLOPT_POSTFIELDS => $data
    ));

    //execute post
    $result = curl_exec($ch);

    //close connection
    curl_close($ch);
    return $result;
}
