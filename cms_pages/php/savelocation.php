<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

if(isset($_POST["location"])){
	$json = json_decode($_POST["location"]);
	
	if($json and isset($json->city)){
		$_SESSION["geo_ip"] = $json->traits->ip_address;
		$_SESSION["geo_city"] = $json->city->names->en;
		$_SESSION["geo_country"] = $json->country->names->en;
	}
}
echo "done";
die();