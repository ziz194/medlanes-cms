<?php
/* 
------------------
Language: English
------------------
*/

$page = basename($_SERVER['PHP_SELF']); 
$class1="";
$class2="";
$class3="";
$class4="";


if($page == 'about-us.php'){
		$class1="selected";
}else if ($page == 'our-doctors.php'){
		$class2="selected";
}else if ($page == 'faq.php'){
		$class3="selected";
}else if ($page == 'contact.php'){
		$class4="selected";
}



$lang = array();
$lang['langde'] = 'class="langen"';

$lang['signup_nb'] = "5372503";





$lang['nl-1'] = "Convenient, Easy and Fast</br> Qualified Medical Advice</br> Anywhere, Anytime!";
$lang['nl-a'] = "Tired of Waiting for Hours</br> at the Doctor's Office</br>or for an Appointment?";
$lang['nl-2'] = "Coming soon";
$lang['nl-3'] = '30 DAY</p><br /><p class="bigit-now">MONEY-BACK <br /> GUARANTEE';
$lang['nl-4'] = '256-bit SSL</p><br /><p class="bigit-now">SECURITY';
$lang['nl-5'] = 'Concept seen on:';
$lang['nl-6'] = 'Use cases for Medlanes';
$lang['nl-7'] = "<b>Licensed Doctors</b> answer your medical questions.";
$lang['nl-8'] = 'Convenient';
$lang['nl-9'] = 'Simple';
$lang['nl-10'] = 'Affordable';
$lang['nl-11'] = 'Time Saving';
$lang['nl-12'] = "Getting medical advice with Medlanes is extremely convenient! Don't stress finding the time to make an appointment or schedule weeks in advance to fit in your schedule. Medlanes is available anytime and from anywhere - on your smartphone and on the web.";
$lang['nl-13'] = "Medlanes is simple, user-friendly and easy to use. Everyone can get instant access to qualified medical advice from board-certified doctors and specialists around the clock. Even while on vacation.";
$lang['nl-14'] = "Healthcare is extremely expensive, not only in direct costs but also due to opportunity costs. Having to take time off just to wait in the waiting room at the doctor's office is quite a hassle. With Medlanes medicine is affordable for everyone, and requires almost no time on your end!";
$lang['nl-15'] = "Stop waiting days for an appointment or hours at the doctor's office. With Medlanes medical advice is available 24 hours a day, 7 days a week.";


$lang['nl-person1'] = '<p>“I was really amazed how fast and easily I could get access to professional medical advice.”</p>';
$lang['nl-person2'] = '<p>“Medlanes is very convenient and easy to use. Not having to leave the office to get medical advice is priceless for me.”</p>';
$lang['nl-person3'] = '<p>“I have medical questions all the time. Getting a quick opinion is perfect to get some peace of mind!”</p>';
$lang['nl-person4'] = '<p>“High quality medical advice at an affordable price, that’s a dream come true for me.”</p>';
$lang['nl-person5'] = "<p>“As a mother of two, I often don't have the time to wait hours at the doctor's office. Medlanes saves me a lot of time.”</p>";
$lang['nl-person6'] = "<p>“I'm not a good walker anymore, getting health advice from home has been saving me a lot of hassle.”</p>";
$lang['nl-person7'] = "<p>“The freedom to get qualified advice from anywhere in the world is something I don't want to miss anymore!”</p>";
$lang['nl-person8'] = "<p>“My own symptom research led to many contradictory information. With Medlanes I have certainty!”</p>";
$lang['nl-person9'] = "					<p>“Medlanes was a quick and affordable way to get a safe feeling about my symptoms. Highly recommended!”</p>
					";
$lang['nl-person10'] = "<p>“Medlanes provides an extraordinary service, which once or twice a month, brings me peace of mind regarding my familiy's health.”</p>
					";
$lang['nl-person11'] = "<p>“It's often difficult to find a doctor available on weekends. Medlanes always fits my schedule, no matter when I need it!”</p>
					";
$lang['nl-person12'] = "<p>“For standard health issues I don't have to see the doctor in person: Medlanes is the fastest, easiest, and most convenient way.”</p>
					";
$lang['nl-person13'] = "<p>“I travel a lot, and it's not always easy to find a doctor who speaks English. These worries don't exist with Medlanes.”</p>
					";
$lang['nl-person14'] = "<p>“Knowing that I can always rely on fast and qualified medical advice enables me to sleep well at night.”</p>
					";
$lang['nl-person15'] = "<p>“I'm not very familiar with computers and apps, nevertheless, Medlanes is used almost regularly by me and my family.”</p>
					";
$lang['nl-person16'] = "<p>“I love Medlanes! This is how healthcare in the 21st century should be done.”</p>
					";




// ADD QUESTION
// 3 STEPS

$lang['STEP1'] = 'Describe your Symptoms';
$lang['STEP2'] = 'Choose your Doctor &amp; Pay';
$lang['STEP3'] = 'Get Qualified Medical Advice';

//CTA

$lang['LinktoCTA'] ="add-question.php?lang=en";
$lang['CTA'] = 'Qualified Medical Advice - Anywhere, Anytime, 24/7';
$lang['BUTTON'] = 'Yes, I Want To Ask A Doctor Now!';



// ADD QUESTION

$lang['ASK_TITLE'] = 'Ask the Doctor';
$lang['ASK_DESCRIPTION'] = 'Ask a doctor your medical questions today! Qualified answers from certified doctors, without the office visit!';	
	
$lang['signupclass'] = 'WFItem5257103';
$lang['signupcode'] = '5257103';
$lang['signuplink'] = 'http://app.getresponse.com/view_webform.js?wid=5257103&mg_param1=1&u=lACb';

$lang['signupclassppc2'] = 'WFItem6331303';
$lang['signupcodeppc2'] = '6331303';
$lang['signuplinkppc2'] = 'http://app.getresponse.com/view_webform.js?wid=6331303&mg_param1=1&u=lACb';

$lang['aq-1'] = 'Describe your symptoms and ask our doctors.';
$lang['aq-2'] = 'Pinpoint the Problems';
$lang['aq-select'] = 'Please select the affected body part(s) using the model below';
$lang['aq-describe'] = 'Describe your Symptoms';
$lang['aq-list'] = 'Please list here the symptoms you present (e.g. Abdominal pain, Nausea, Headache, Skin rash ....)';
$lang['aq-photo1'] = 'Attach relevant photos';
$lang['aq-photo2'] = 'Attach a photo';
$lang['aq-answer'] = 'Is there a question you want your doctor to answer?';
$lang['aq-ph'] = 'Your question(s) here';  
$lang['aq-confirm'] = 'Confirm';
$lang['aq-process'] = 'We will pair you with the perfect doctor and start processing your questions immediately!';
$lang['aq-3'] = 'Front';
$lang['aq-4'] = 'Back';
$lang['aq-5'] = 'Your Symptoms';
$lang['aq-6'] = 'More Information';
$lang['aq-7'] = 'Additional Information';
$lang['aq-7'] = 'Additional information';
$lang['aq-age'] = 'Choose your age';
$lang['aq-gender'] = 'Choose your  gender';
$lang['aq-male'] = 'Male';
$lang['aq-female'] = 'Female';
$lang['aq-8'] = 'When did you start to experience these symptoms?.';
$lang['aq-9'] = 'Select';
$lang['aq-10'] = 'Acute';
$lang['aq-11'] = 'Today';
$lang['aq-12'] = 'This Week';
$lang['aq-13'] = 'This Month';
$lang['aq-chronic'] = 'Chronic';
$lang['aq-yq'] = 'Your Question';
$lang['aq-experience'] = 'Did you experience any trauma / injury?';
$lang['aq-14'] = 'Yes';
$lang['aq-15'] = 'No';
$lang['aq-16'] = 'Please list any medications you are currently taking:';
$lang['aq-17'] = 'Please list relevant conditions in your medical history: (e.g. allergies, high blood pressure, COPD, Asthma, etc)';
$lang['aq-18'] = 'Additional Files';
$lang['aq-19'] = 'Attach Files';
$lang['aq-20'] = 'The perfect qualified doctor will be assigned to your question within hours.';
$lang['aq-21'] = 'Continue to final step';
$lang['aq-add'] = 'ADD';
$lang['aq-name'] = 'Enter Your Name';
$lang['aq-email'] = 'Enter Your Email';
$lang['aq-button'] = 'Yes, I Want Qualified Medical Advice Now !';

// Add question end


$lang['PAYBUTTONPPC4'] = 'Secure Checkout with PayPal';

// Body parts 

$lang['aq-bp-1'] = 'Head';
$lang['aq-bp-2'] = 'Right Arm';
$lang['aq-bp-3'] = 'Right Chest';
$lang['aq-bp-4'] = 'Left Chest';
$lang['aq-bp-5'] = 'Left Arm';
$lang['aq-bp-6'] = 'Right Stomach';
$lang['aq-bp-7'] = 'Left Stomach';
$lang['aq-bp-8'] = 'Right Hand';
$lang['aq-bp-9'] = 'Pubic Area';
$lang['aq-bp-10'] = 'Left Hand';
$lang['aq-bp-11'] = 'Right Thigh';
$lang['aq-bp-12'] = 'Left Thigh';
$lang['aq-bp-13'] = 'Right Lower Leg';
$lang['aq-bp-14'] = 'Left Lower Leg';
$lang['aq-bp-15'] = 'Right Foot';
$lang['aq-bp-16'] = 'Left Foot';

$lang['aq-bp-33'] = 'Right Upper Back';
$lang['aq-bp-44'] = 'Left Upper Back';
$lang['aq-bp-66'] = 'Right Lower Back';
$lang['aq-bp-77'] = 'Left Lower Back';
$lang['aq-bp-99'] = 'Buttocks';
// Body parts end

// sign up start
$lang['sup-class1'] ='';
$lang['sup-class2'] ='';
$lang['sup-form2'] ='Last Name';
$lang['sup-form1'] ='First Name';
$lang['sup-form2'] ='Last Name';
$lang['sup-form3'] ='eMail';
$lang['sup-form4'] ='Password';
$lang['sup-1'] = 'Create a private account';
$lang['sup-2'] = 'All information and questions are private';
$lang['sup-3'] = 'Please fill in your First name';
$lang['sup-4'] = 'Please fill in your Last name';
$lang['sup-5'] = 'Please fill in your eMail';
$lang['sup-6'] = 'Please fill in your Password';
$lang['sup-7'] = 'Please fill in your Age';
$lang['sup-8'] = 'Choose age range';
$lang['sup-9'] = 'Please select in your Gender';
$lang['sup-10'] = 'Male';
$lang['sup-11'] = 'Female';
$lang['sup-12'] = 'Optional';
$lang['sup-13'] = 'You need to agree to our Terms,Conditions and Privacy statement';
$lang['sup-14'] = 'I’ve read and  agree to the <a class="fancybox fancybox.iframe" href="terms-and-conditions.html" >terms &amp; conditions</a>';
$lang['sup-15'] = 'I’ve read and  agree to the <a class="fancybox fancybox.iframe" href="privacy-statement.html">privacy statement</a>';
$lang['sup-16'] = 'Sign up &amp; Save question';
// sign up end


// top-banners start
$lang['tb-1'] = 'Get certainty about your health now.';
$lang['tb-2'] = 'From anywhere, around the clock - for a 24€ flat fee';

// top-banners end


$lang['LOGO_HOME'] = "http://medlanes.com?lang=en";
$lang['LOGO_HOME_TEST'] = "http://medlanes.com/ask/index.php?lang=en";
$lang['LOGO_HOME_TEST2'] = "http://medlanes.com/ask/index1.php?lang=en";

$lang['PAGE_TITLE'] = 'Medlanes - qualified medical consultations - mobile & online';

$lang['HEADER_TITLE'] = 'My website header title';
$lang['SITE_NAME'] = 'My Website';
$lang['SLOGAN'] = 'My slogan here';
$lang['HEADING'] = 'Heading';

// Header----------------------

$lang['HEADER_LOGO'] = '<a class="logo" href="index.php?lang=en">Medlanes</a>';
$lang['HEADER_HOME'] = '<a href="index.php?lang=en">Home</a>';
$lang['HEADER_SERVICE'] = "<a href=service.php?lang=en>Our Service</a>";
$lang['HEADER_TREAT'] = "<a href='#'>What we treat";

$lang['HEADER_DOCTORS'] = "<a href=doctors.php?lang=en class=".$class2.">Doctors</a>";
$lang['HEADER_FAQ'] = "<a style='margin-left:-5px;' href=faq.php?lang=en>FAQ</a>";
$lang['HEADER_CONTACT'] = "<a href=contact.php?lang=en>Contact</a>";

$lang['HEADER_ASK'] = '<a class="last" href="add-question.php?lang=en">Ask a Doctor</a>';
$lang['HEADER_SIGNUP'] = '<a class="last" href="add-question.php?lang=en">Sign Up</a>';


// Footer-------------------


$lang['FOOTER_CONDITIONS'] = 'Conditions We Treat';

$lang['FOOTER_ACIDREFLUX'] = '<a  href="acid.php?lang=en">Acid Reflux</a>';
$lang['FOOTER_ANEMIA'] = '<a  href="anemia.php?lang=en">Anemia</a>';
$lang['FOOTER_ASTHMA'] = '<a  href="asthma.php?lang=en">Asthma</a>';
$lang['FOOTER_BACTERIA'] = '<a  href="index.php?lang=en">Bacterial Infections</a>';
$lang['FOOTER_BRONCHITIS'] = '<a  href="bronchitis.php?lang=en">Bronchitis</a>';
$lang['FOOTER_CELLULITIS'] = '<a  href="cellulitis.php?lang=en">Cellulitis</a>';
$lang['FOOTER_COLD'] = '<a  href="index.php?lang=en">Cold and Flu</a>';
$lang['FOOTER_CONGESTION'] = '<a  href="congestion.php?lang=en">Congestion</a>';
$lang['FOOTER_COUGH'] = '<a  href="cough.php?lang=en">Cough</a>';
$lang['FOOTER_DIARRHEA'] = '<a  href="diarrhea.php?lang=en">Diarrhea</a>';
$lang['FOOTER_FATIGUE'] = '<a  href="fatigue.php?lang=en">Fatigue and Weakness</a>';
$lang['FOOTER_FEVER'] = '<a   href="fever.php?lang=en">Fever</a>';
$lang['FOOTER_FUNGAL'] = '<a  href="index.php?lang=en">Fungal Infection</a>';
$lang['FOOTER_GOUT'] = '<a   href="gout.php?lang=en">Gout</a>';
$lang['FOOTER_HEADACHE'] = '<a  href="headache.php?lang=en">Headache</a>';
$lang['FOOTER_HIGHBLOOD'] = '<a  href="index.php?lang=en">High Blood Pressure</a>';
$lang['FOOTER_INDIGESTION'] = '<a  href="index.php?lang=en">Indigestion</a>';
$lang['FOOTER_LOWBLOOD'] = '<a  href="index.php?lang=en">Low Blood Pressure</a>';
$lang['FOOTER_MIGRAINES'] = '<a  href="migraine.php?lang=en">Migraine</a>';
$lang['FOOTER_NAUSEA'] = '<a  href="index.php?lang=en">Nausea and Vomiting</a>';
$lang['FOOTER_PINKEYE'] = '<a  href="pinkeye.php?lang=en">Pink Eye</a>';
$lang['FOOTER_PNEUMONIA'] = '<a  href="index.php?lang=en">Pneumonia</a>';
$lang['FOOTER_SORETHROAT'] = '<a  href="index.php?lang=en">Sore Throat</a>';
$lang['FOOTER_STOMACHVIRUS'] = '<a  href="index.php?lang=en">Stomach Virus</a>';
$lang['FOOTER_URINARY'] = '<a  href="index.php?lang=en">Urinary Tract Infections</a>';
$lang['FOOTER_VERTIGO'] = '<a  href="index.php?lang=en">Vertigo</a>';










$lang['FOOTER_HOME'] = '<a href="index.php?lang=en">Home</a>';
$lang['FOOTER_ABOUT'] = '<a href="about.php?lang=en">About Medlanes </a>';
$lang['FOOTER_FAQ'] = "<a href=faq.php?lang=en class=".$class3.">FAQ</a>";
$lang['FOOTER_CONTACT'] = "<a href=contact.php?lang=en class=".$class4.">Contact</a>";

$lang['FOOTER_ASK'] = '<a class="last" href="add-question.php?lang=en">Ask a Doctor</a>';
$lang['FOOTER_SIGNUP'] = '<a class="last" href="add-question.php?lang=en">Sign up</a>';

$lang['FOOTER_DOCTORS'] = "<a href=doctors.php?lang=en class=".$class2.">Doctors</a>";
$lang['FOOTER_SERVICE'] = "<a href=service.php?lang=en>Our Service</a>";


$lang['FOOTER_CAREER'] = '<a href="career.php?lang=en">Career</a>';

$lang['FOOTER_TERMS'] = '<a href="terms.php?lang=en">Terms of Service</a>';
$lang['FOOTER_PRIVACY'] = '<a href="terms.php?lang=en">Privacy Statement</a>';
$lang['FOOTER_SECURITY'] = '<a href="privacy.php?lang=en">Data Security</a>';

$lang['FOOTER_SUPPORT'] = 'Supported by :';
$lang['FOOTER_COPYRIGHT'] = 'Copyright © Medlanes GmbH  2014  All Rights Reserved';
$lang['FOOTER_IMPRINT'] = '<a href="impressum.php?lang=en">Imprint</a>';

// Home Page--------------------

$lang['HOME_PAGE_TITLE'] = 'Medlanes - Medical Questions Answered 24/7!';
$lang['HOME_PAGE_DESCRIPTION'] = 'Answers from thousands of qualified doctors. Anytime, anywhere. Skip the waiting room, ask a doctor online! .';


$lang['HOME_PAGE_banner-sliderone_content_testpage']="<h1>Got a medical problem?</h1>
				<h2>Direct advice by qualified doctors
					for a £29 fee </h2>
				<a href='signup.php'> Get advice now</a>";
				
				
$lang['HOME_PAGE_banner-sliderone_content']="<h1>Got a medical problem?</h1>
				<h2>Direct advice by qualified doctors
					for a £29 fee </h2>
				<a href='#'> Coming soon</a>";
	
$lang['HOME_PAGE_banner-slidertwo_content']="<h1>Medical advice&nbsp;<br>&nbsp;always available&nbsp;<br></h1>
				<h2>24/7 wherever&nbsp;<br>&nbsp; you might be&nbsp;</h2>
				<a href='#'> Coming soon</a>";
	
	
	$lang['HOME_PAGE_banner-sliderthree_content']="<h1>Medical advice &nbsp;<br>&nbsp;always available&nbsp;<br></h1>
				<h2>24/7 wherever&nbsp;<br>&nbsp; you might be&nbsp;</h2>
				<a href='#'> Coming soon</a>";			

$lang['SIGNUP']="Sign Up";
$lang['EMAIL_VALIDATION']="Email address is invalid";
$lang['EMAIL_PLACEHOLDER']="Start with your email";


$lang['HOME_PAGE_l-one']="Fully certified";
$lang['HOME_PAGE_l-two']="256-bit SSL";


$lang['HOME_PAGE_Concept']="Concept seen on:";


$lang['HOME_PAGE_widtitle1']="Medlanes";
$lang['HOME_PAGE_widcont1']="Getting medical advice at night, on the weekend or on vacation is difficult. Medlanes provides mobile and online advice directly from qualified doctors whenever access to healthcare seems out of reach.";

$lang['HOME_PAGE_widtitle2']="Our Service";
$lang['HOME_PAGE_widcont2']="Use the Medlanes app or webpage to ask your medical questions, we will match you with the right family doctor or specialist - no matter where you are and around the clock.";

$lang['HOME_PAGE_widtitle3']="Your Advantage";
$lang['HOME_PAGE_widcont3']="With Medlanes you don't need to wait for appointments or rely on low-quality information on the internet. Instead, your questions are answered by a leading expert without you having to leave the house Your doctor is always in your pocket!";

$lang['HOME_PAGE_bluebox1_text1']="Support";
$lang['HOME_PAGE_bluebox1_text2']="0800 - 765 43 43";
$lang['HOME_PAGE_bluebox1_text3']="toll free from landlines";
$lang['HOME_PAGE_bluebox1_text4']="E-Mail";

$lang['HOME_PAGE_bluebox2_text1']="Certified & Tested";
$lang['HOME_PAGE_bluebox2_text2']="We're dedicated to quality and reliability";
$lang['HOME_PAGE_bluebox2_text3']="<a href='about-us.php?lang=en'>About us</a>";

$lang['HOME_PAGE_bluebox3_text1']="Full-circle Medicine";
$lang['HOME_PAGE_bluebox3_text2']="Generalists & Specialists at Medlanes";
$lang['HOME_PAGE_bluebox3_text3']="<a href='our-doctors.php?lang=en'>Our doctors</a>";


// Our Service--------------------
$lang['SERVICE_TITLE'] = 'How Online Doctor Visits Work';
$lang['SERVICE_DESCRIPTION'] = 'Online Doctor visits are a convenient and inexpensive way to see your doctor. Any time, any place.';

$lang['SERVICE_HEADLINE']="A doctor in your pocket, wherever you are!";

     // Menu--------------------
$lang['SERVICE_MENU_HOW']="How it Works";
$lang['SERVICE_MENU_WHY']="Why use Medlanes";
$lang['SERVICE_MENU_USECASES']="Use Cases";
$lang['SERVICE_MENU_PRICE']="Price & Use cases";

$lang['SERVICE_BUTTON1']="Click Here To Get Started!";


     // How Medlanes works--------------------
$lang['SERVICE_HOW_HEADLINE']="How Medlanes Works";
$lang['SERVICE_HOW_TEXT']="With Medlanes, you receive a diagnosis from a qualified, board-certified general practitioner or specialist within 24 hours. There are just three simple steps to get started! You submit your questions and symptoms through our web form, choose one of the suggested doctors from your state, and pay for your doctor visit. The doctor you have selected will evaluate all of the data you have provided, and respond to your question within 24 hours. Any follow up questions that you may have will be answered within that timeline as well!";

$lang['SERVICE_HOW_DESCRIBE_h']="Describe your Symptoms"; 
$lang['SERVICE_HOW_DESCRIBE_t']="First, describe your symptoms through our simple questionnaire available online with our mobile App. At this time, you will also have the opportunity to upload any documentation or images relative to your condition. After that is completed, you can ask your doctor the specific questions you have."; 

$lang['SERVICE_HOW_CHOOSE_h']="Choose your doctor &amp; pay"; 
$lang['SERVICE_HOW_CHOOSE_t']="Choose one of the suggested doctors from your area and pay your consultation fee of just $29. If our doctors are not able to help you, you will be refunded within 24 hours of all costs in full with our money back guarantee. "; 

$lang['SERVICE_HOW_ADVICE_h']="Qualified medical advice"; 
$lang['SERVICE_HOW_ADVICE_t']="A qualified, board-certified doctor will answer your questions and diagnose your symptoms typically within an hour! Now it is possible to get an expert opinion of your condition with the push of a button!"; 

    // Why use Medlanes--------------------
$lang['SERVICE_tab-title']="With Medlanes you...";

$lang['SERVICE_tab-1']="Wait Less";
$lang['SERVICE_tab-1_content']="Never wait again to get an appointment with your doctor! Your Medlanes doctor is avaiable to provide you with medical advice 24 hours a day, 7 days a week. ";


$lang['SERVICE_tab-2']="Pay Less";
$lang['SERVICE_tab-2_content']="Healthcare is extremely expensive. Whether you have insurance or not, healthcare costs are exorbitant! Not only is paying for the doctor expensive, but you are having to take valuable time out of your busy day just to wait in the waiting room at the doctor's office. With Medlanes, medicine is affordable for everyone, and it is on your time!";

$lang['SERVICE_tab-3']="Stress Less";
$lang['SERVICE_tab-3_content']="Getting medical advice with Medlanes is extremely convenient! No more stress finding the time to make an appointment or schedule weeks in advance to fit in your schedule. No more worries on vacations! No more worries in the middle of the night! Medlanes is available anytime and from anywhere - on your smartphone and on the web!";

$lang['SERVICE_tab-4']="Worry Less";
$lang['SERVICE_tab-4_content']="Don't worry about you or your family's health. Medlanes has an extensive network of doctors of both general practitioners and specialists to give you peace of mind. No more worries! Your doctor is waiting on you, as opposed to you waiting on them!";


$lang['SERVICE_someFact-title']="Why use Medlanes";


$lang['SERVICE_someFact-fact1']="8 min.";
$lang['SERVICE_someFact-fact1-cont']="Average time span a doctor has for a visit.";

$lang['SERVICE_someFact-fact2']="14 days";
$lang['SERVICE_someFact-fact2-cont']="Average waiting time to get a doctor's appointment.";

$lang['SERVICE_someFact-fact3']="3 hours";
$lang['SERVICE_someFact-fact3-cont']="Time a doctor's visit takes on average, including getting there.";

$lang['SERVICE_someFact-fact4']="80%";
$lang['SERVICE_someFact-fact4-cont']="Percentage of visits to the doctor's office that could be done online.";

$lang['SERVICE_someFact-fact5']="50%";
$lang['SERVICE_someFact-fact5-cont']="Amount of treatments that could be improved if patients got continuous advice.";

          // Use Cases--------------------
$lang['SERVICE_benefits-title']="Always here for you, Wherever you may be!";

$lang['SERVICE_benefits-msg']="Get medical advice WHEN and WHERE you need it! Medlanes combines the convenience of an online service with the quality and reliability of a visit to your doctor! Simply download the Medlanes App or use the Medlanes Website to get answers to medical questions you may have - anytime, anywhere! Answer the questionnaire, upload documents relevant to your condition, submit your questions and Medlanes takes care of the rest! Our group of doctors is truly here to make your life easier!";

$lang['SERVICE_benefits-benifit1_title']="During Vacation";
$lang['SERVICE_benefits-benifit1_content']="It's difficult to find a doctor that speaks your language when you are travelling abroad, or in an unfamiliar area - use Medlanes to get answers to your questions!.";

$lang['SERVICE_benefits-benifit2_title']="At Work";
$lang['SERVICE_benefits-benifit2_content']="Talking time off work to see a doctor during the day can be a tough decision. Medlanes eliminates the need to waste your valuable time at the doctor's office!";

$lang['SERVICE_benefits-benifit3_title']="At Night";
$lang['SERVICE_benefits-benifit3_content']="For all non-emergency questions it's almost impossible to get advice after 6pm - not with Medlanes!";

$lang['SERVICE_benefits-benifit4_title']="On the Weekend";
$lang['SERVICE_benefits-benifit4_content']="Finding a doctor on the weekend without having to wait at an Urgent Care or the ER is very difficult - with Medlanes, you never will wait again!";

$lang['SERVICE_benefits-benifit5_title']="For Convenience";
$lang['SERVICE_benefits-benifit5_content']="Waiting to get an appointment, the dreaded waiting room - there's no lost time when using Medlanes.";

$lang['SERVICE_benefits-benifit6_title']="Long Distance";
$lang['SERVICE_benefits-benifit6_content']="Don't settle for an under-qualified doctor because he is close, get advice from the right specialist from your home!";

$lang['SERVICE_welcome_cont']="Getting good an answer from your doctor is harder than it should be. You wait weeks to get an appointment, wait again in the waiting room, then spend 3 minutes of face to face time with your doctor. On the weekends, at night, or during vacation. There is always the chance that you are unable to get the answers you need!<br />
 Medlanes changes all that! With Medlanes, you get the answers to your medical questions any time, any place. With years experience, and our doctors profound expertise, Medlanes provides a safe, convenient, and secure way to get the care that you deserve!";

$lang['SERVICE_title1']="Fast and convenient….";

$lang['SERVICE_content1']="Get medical advice faster and more convenient than ever before! Simply ask your question through the Medlanes app or webpage, we'll connect you with the right doctor no matter where you are or what time it is. We'll guide you the process so that the doctors get all the information they need to give meaningful advive and get back to you in no time!";

$lang['SERVICE_title2']="Safe and confidential….";

$lang['SERVICE_content2']="All of our doctors are licensed physicians with many years of professional experience in their respective fields. All doctors adhere to a strict ethical code and absolute doctor-patient confidentiality.";


           // Price--------------------
$lang['SERVICE_PRICE_HEADLINE']="Price & Use Cases";	
$lang['SERVICE_PRICE_TEXT']="Our mission at Medlanes is to make high quality healthcare affordable to all! Healthcare is extremely expensive, not only paying your doctor, but the time you waste actually going through the process of seeing your doctor. With Medlanes, medicine is affordable for everyone, and requires almost no time on your end! Your doctor is always at your fingertips!";	   

$lang['SERVICE_PRICE_BOX_ADVICE']="Qualified Answers";	
$lang['SERVICE_PRICE_BOX_TEXT']="Consultation/Diagnosis per Email includes up to 3 follow up questions by our board-certified United States based general practitioners or specialists.";	 
$lang['SERVICE_PRICE_BOX_PRICE']="$29";	  
          
		  // Testimonials--------------------
$lang['SERVICE_TESTIMONIALS_HEADLINE']="Testimonials";

// Doctors Page--------------------

$lang['DOCTORS_TITLE']="Our Providers";
$lang['DOCTORS_DESCRIPTION']="Thousands of qualified doctors ready to answer your medical questions 24 hours a day, 7 days a week.";

$lang['DOCTORS_HEADLINE']="Doctors of every speciality available around the clock.";

$lang['DOCTORS_title1']="Our Doctors";
$lang['DOCTORS_cont1']="Medlanes works exclusively with qualified and licensed doctors. In addition, all of our partner physicians undergo a comprehensive quality and safety assessment. With the help of carefully designed algorithms, we connect you with your personal physician.";

$lang['DOCTORS_title2']="We're currently building our base";
$lang['DOCTORS_cont2']="Medlanes only works with fully licensed and qualified doctors. Your &quot;online/mobile doctor&quot; will be matched to your specific query and will be an expert in the field. Medlanes is designed to help you with  medical queries, advice and dealing with chronic diseases. ";


$lang['DOCTORS_EXPERIENCE_HEADLINE']="Experience";
$lang['DOCTORS_WHY_HEADLINE']="Why did you join Medlanes?";
$lang['DOCTORS_EDUCATION_HEADLINE']="Education";
$lang['DOCTORS_HIGHLIGHTS_HEADLINE']="Professional Memberships";
$lang['DOCTORS_CERTIFICATION_HEADLINE']="Certification";
             //Doctor Slider -------------------------
             //Doctor 1
$lang['DOCTORS_NAME1']="Dr. Rose A. Sanford";
$lang['DOCTORS_FIELD1']="Pediatrics";
$lang['DOCTORS_IMAGE1']="<img src=\"images/pediatrics.png\" width=\"45px\" height=\"45px\" alt=\"pediatrics\" />";
$lang['DOCTORS_PORTRAIT1']="<img src=\"images/docs/Doctor5.png\"/>";
$lang['DOCTORS_REGION1']="Orlando, FL";
$lang['DOCTORS_EXPERIENCE_TEXT1']="Hospital (7 years)";
$lang['DOCTORS_WHY_TEXT1']="Sometimes parents stay at home thinking their self-diagnosis for their child is right. Yet, self-diagnosis is both misleading and dangerous, especially for young children. With Medlanes, I am easily reachable when necessary, for a proper and reliable expertise.";
$lang['DOCTORS_EDUCATION_TEXT1']="Residency Pediatrics, University of South Florida, Tampa, FL</p>
          <p>Medical School, University of Miami Miller School of Medicine, Miami, FL</p>";
$lang['DOCTORS_HIGHLIGHTS_TEXT1']="Diabetes Recognition Program</p>
            <p>Patient-Centered Medical Home Recognition Program</p>";		  
$lang['DOCTORS_CERTIFICATION_TEXT1']="American Board of Pediatrics</p>
          <p>American Board of Internal Medicine";
         
		  
		     //Doctor 2
$lang['DOCTORS_NAME2']="Dr. David Harris";
$lang['DOCTORS_FIELD2']="Dietetics";
$lang['DOCTORS_IMAGE2']="<img src=\"images/dietetics.png\" width=\"45px\" height=\"45px\" alt=\"dietetics\" />";
$lang['DOCTORS_PORTRAIT2']="<img src=\"images/docs/Doctor6.png\"/>";
$lang['DOCTORS_REGION2']="Sacramento, CA";
$lang['DOCTORS_EXPERIENCE_TEXT2']="Humanitarian (3 years)</p>
                                      <p>Private practice (2 years)</p>";
$lang['DOCTORS_WHY_TEXT2']="I defend medical ethics, and thus I like the idea of a fair and foreseeable cost proposed by online consultations, contrary to private practices that sometimes overcharge their patients.";
$lang['DOCTORS_EDUCATION_TEXT2']="Residency Nutrition, University of California, Davis, CA</p>
          <p>Medical School, The Ohio State University, Columbus, OH</p>";
$lang['DOCTORS_HIGHLIGHTS_TEXT2']="American Dietetic Association</p>
            <p>Dietetic Educators of Practitioners </p>
			<p>Dietitians in Nutrition Support</p>";		  
$lang['DOCTORS_CERTIFICATION_TEXT2']="ServSafe Certification</p>";
		  
		    //Doctor 3
$lang['DOCTORS_NAME3']="Dr. Charlene C. Fleming ";
$lang['DOCTORS_FIELD3']="Gastroenterology";
$lang['DOCTORS_IMAGE3']="<img src=\"images/gastroenterology.png\" width=\"45px\" height=\"45px\" alt=\"gastroenterology\" />";
$lang['DOCTORS_PORTRAIT3']="<img src=\"images/docs/Doctor8.png\"/>";
$lang['DOCTORS_REGION3']="Chicago, IL";
$lang['DOCTORS_EXPERIENCE_TEXT3']="Humanitarian (3 years)</p>
                                      <p>Hospital (25 years)</p>";
$lang['DOCTORS_WHY_TEXT3']="As a specialist, I know how cumbersome the standard processes to obtain an appointment can be, online consultations minimize time-wasting steps for both patients and doctors.";
$lang['DOCTORS_EDUCATION_TEXT3']="Residency Gastroenterology, University of Chicago Medicine, Chicago, IL</p>
          <p>Medical School, University of Chicago Pritzker School of Medicine, Chicago, IL</p>";
$lang['DOCTORS_HIGHLIGHTS_TEXT3']="American College of Gastroenterology</p>
                        <p>American Society of Gastroenterology Endoscopy</p>
						<p>Crohn’s and Colitis Foundation America</p>";		  
$lang['DOCTORS_CERTIFICATION_TEXT3']="American Board of Gastroenterology</p>";
		  
		  		    //Doctor 4
$lang['DOCTORS_NAME4']="Dr. Daniel J. Gray";
$lang['DOCTORS_FIELD4']="Pediatrics";
$lang['DOCTORS_IMAGE4']="<img src=\"images/pediatrics.png\" width=\"45px\" height=\"45px\" alt=\"pediatrics\" />";
$lang['DOCTORS_PORTRAIT4']="<img src=\"images/docs/Doctor7.png\"/>";
$lang['DOCTORS_REGION4']="Birmingham, AL";
$lang['DOCTORS_EXPERIENCE_TEXT4']="Private practice (8 years)";
$lang['DOCTORS_WHY_TEXT4']="Each condition is different, they can evolve rapidly and differently, therefore they need to be addressed on a regular basis to be treated efficiently, and this is exactly what is now possible with Medlanes.";
$lang['DOCTORS_EDUCATION_TEXT4']="Residency Pediatrics, University of Florida, Gainesville, FL</p>
          <p>Residency Internal Medicine, Brigham and Women’s Hospital, Boston, MA</p>
          <p>Medical School, Duke University School of Medicine </p>";
$lang['DOCTORS_HIGHLIGHTS_TEXT4']="Alpha Omega Alpha Honor Society</p>
                        <p>Academic Pediatric Association</p>
						<p>Mobile Pediatric Society</p>";		  
$lang['DOCTORS_CERTIFICATION_TEXT4']="American Board of Pediatrics</p>
          <p>Developmental-Behavioral Pediatrics</p>
          <p>Neonatal-Perinatal Medicine";
		  
		  		   //Doctor 5
$lang['DOCTORS_NAME5']="Dr. Thomas Pennington";
$lang['DOCTORS_FIELD5']="Family Medicine";
$lang['DOCTORS_IMAGE5']="<img src=\"images/family_medicine.png\" width=\"45px\" height=\"45px\" alt=\"family medicine\" />";
$lang['DOCTORS_PORTRAIT5']="<img src=\"images/docs/Doctor1.png\"/>";
$lang['DOCTORS_REGION5']="Dallas, TX";
$lang['DOCTORS_EXPERIENCE_TEXT5']="Hospital (4 years)</p>
                                      <p>Private practice (23 years)";
$lang['DOCTORS_WHY_TEXT5']="I choose Medlanes because I am convinced that new technologies can improve my daily medical practice. With online consultations, I keep a clear cross-speciality medical record for each patient, which enables more accurate diagnoses. ";
$lang['DOCTORS_EDUCATION_TEXT5']="Residency Family Practice, Rush-Presbyterian-St. Luke's Medical Center, Chicago, IL</p>
          <p>Medical School, Northeastern Ohio Universities College of Medicine, Rootstown, OH</p>
                        <p>Texas Academy of Family Physicians</p>
						<p>Ohio State Medical Society Member</p>";
$lang['DOCTORS_HIGHLIGHTS_TEXT5']="Children's Medical Center Sports Medicine Operational Committee";		  
$lang['DOCTORS_CERTIFICATION_TEXT5']="American Board of Family Medicine";
		  
		  		   //Doctor 6
$lang['DOCTORS_NAME6']="Dr. Gary J. Rush";
$lang['DOCTORS_FIELD6']="Dermatology";
$lang['DOCTORS_IMAGE6']="<img src=\"images/dermatology.png\" width=\"45px\" height=\"45px\" alt=\"dermatology\" />";
$lang['DOCTORS_PORTRAIT6']="<img src=\"images/docs/Doctor4.png\"/>";
$lang['DOCTORS_REGION6']="New York City, NY";
$lang['DOCTORS_EXPERIENCE_TEXT6']="Research (8 years)</p>
                                       <p>Hospital (6 years)</p>
                                      <p>Private practice (21 years)";
$lang['DOCTORS_WHY_TEXT6']="After a 35-year career, Medlanes is a great way for me to share my experience more broadly. I feel like bringing a better contribution to the society.";
$lang['DOCTORS_EDUCATION_TEXT6']="Residency Dermatology, New York University School of Medicine, New York, NY</p>
          <p>Medical School, Harvard Medical School, Boston, MA</p>";
$lang['DOCTORS_HIGHLIGHTS_TEXT6']="Dermatological Society of Greater New York</p>
                        <p>American Society for Dermatologic Surgery</p>
						<p>American Academy of Dermatology</p>
						<p>Massachusetts Medical Society</p>";	  
$lang['DOCTORS_CERTIFICATION_TEXT6']="American Board of Dermatology</p>";
		  
		     //Doctor 7
$lang['DOCTORS_NAME7']="Dr. Eileen B. Sheppard";
$lang['DOCTORS_FIELD7']="Family Medicine";
$lang['DOCTORS_IMAGE7']="<img src=\"images/family_medicine.png\" width=\"45px\" height=\"45px\" alt=\"family medicine\" />";
$lang['DOCTORS_PORTRAIT7']="<img src=\"images/docs/Doctor2.png\"/>";
$lang['DOCTORS_REGION7']="Denver, CO";
$lang['DOCTORS_EXPERIENCE_TEXT7']="Private practice (16 years)";
$lang['DOCTORS_WHY_TEXT7']="Through Medlanes, I can answer queries from remote or isolated persons, like dependent elderly persons, who have to drive sometimes up to 3 hours to access suitable healthcare.";
$lang['DOCTORS_EDUCATION_TEXT7']="Residency, Mercy Family Medicine Residency, Denver, CO</p>
          <p>Medical School, Oregon Health Sciences University, Portland, OR</p>";
$lang['DOCTORS_HIGHLIGHTS_TEXT7']="American Academy of Family Physicians </p>
          <p>Colorado Academy of Family Physicians </p>";		  
$lang['DOCTORS_CERTIFICATION_TEXT7']="American Board of Family Medicine</p>
          <p>American Board of Obstetrics and Gynecology</p>";
		  
		      //Doctor 8
$lang['DOCTORS_NAME8']="Dr. Richard Reeves";
$lang['DOCTORS_FIELD8']="Family Medicine";
$lang['DOCTORS_IMAGE8']="<img src=\"images/family_medicine.png\" width=\"45px\" height=\"45px\" alt=\"family medicine\" />";
$lang['DOCTORS_PORTRAIT8']="<img src=\"images/docs/Doctor3.png\"/>";
$lang['DOCTORS_REGION8']="Washington, D.C.";
$lang['DOCTORS_EXPERIENCE_TEXT8']="Private practice (8 years)";
$lang['DOCTORS_WHY_TEXT8']="Many of my patients regularly travel to other states or abroad for business; online consultation enables them to consult the same doctor from everywhere.";
$lang['DOCTORS_EDUCATION_TEXT8']="Residency Internal Medicine, University of Washington, Seattle, WA</p>
          <p>Medical School, MD Stanford University Medical School, Stanford, CA</p>";
$lang['DOCTORS_HIGHLIGHTS_TEXT8']="American Society of Bone & Mineral Research</p>
           <p>King County Medical Society </p>
		   <p>American College of Rheumatology</p>
          <p>American College of Physicians</p>";				  
$lang['DOCTORS_CERTIFICATION_TEXT8']="American Board of Rheumatology</p>
          <p>American Board of Internal Medicine</p>";
		  
		  	      //Doctor 9
$lang['DOCTORS_NAME9']="Dr. Linda S. Hewitt";
$lang['DOCTORS_FIELD9']="Gynecology";
$lang['DOCTORS_IMAGE9']="<img src=\"images/gynecology.png\" width=\"45px\" height=\"45px\" alt=\"gyeocology\" />";
$lang['DOCTORS_PORTRAIT9']="<img src=\"images/docs/Doctor10.png\"/>";
$lang['DOCTORS_REGION9']="Boston, MA";
$lang['DOCTORS_EXPERIENCE_TEXT9']="Private practice (27 years)";
$lang['DOCTORS_WHY_TEXT9']="A lot of patients feel ashamed talking directly to a doctor about their medical condition. With Medlanes we can provide medical advice anonymously.";
$lang['DOCTORS_EDUCATION_TEXT9']="Residency Obstetrics and Gynecology, Tufts University School of Medicine, Boston, MA</p>
          <p>Medical School,  University of Pennsylvania, Phladelphia, PA</p>";
$lang['DOCTORS_HIGHLIGHTS_TEXT9']="Obstetrical Society of Boston </p>
          <p>New England Society of Gynecologic Oncologists.</p>";	  
$lang['DOCTORS_CERTIFICATION_TEXT9']="American Board of Obstetrics & Gynecology</p>
          <p>American Board of Gynecologic Oncology</p>";
		  
		  		  	      //Doctor 10
$lang['DOCTORS_NAME10']="Dr. Matthew Kline";
$lang['DOCTORS_FIELD10']="Pediatrics";
$lang['DOCTORS_IMAGE10']="<img src=\"images/pediatrics.png\" width=\"45px\" height=\"45px\" alt=\"pediatrics\" />";
$lang['DOCTORS_PORTRAIT10']="<img src=\"images/docs/Doctor12.png\"/>";
$lang['DOCTORS_REGION10']="San Francisco, CA";
$lang['DOCTORS_EXPERIENCE_TEXT10']="Hospital (8 years)";
$lang['DOCTORS_WHY_TEXT10']="Sometimes parents stay at home thinking their self-diagnosis for their child is right. Yet, self-diagnosis is both misleading and dangerous, especially for young children. With Medlanes, I am easily reachable when necessary, for a proper and reliable expertise.";
$lang['DOCTORS_EDUCATION_TEXT10']="Fellow - Cardiovascular Anesthesiology, Michael Reese Hospital</p>
          <p>Resident - Anesthesiology, Michael Reese Hospital</p>
          <p>MD, Loyola University &amp; Stritch School of Medicine</p>
          <p>MSEE - Electrical and Computer Engineering, University of Wisconsin,</p>
          <p>BSE - Biomedical Engineering, Duke University";
$lang['DOCTORS_HIGHLIGHTS_TEXT10']="Chair, Membership Committee, American College of Pediatrics, 2006-present";		  
$lang['DOCTORS_CERTIFICATION_TEXT10']="American Board of Pediatrics</p>
          <p>Diplomate, American Board of Psychiatry and Neurology</p>
          <p>Diplomate, American Board of Psychiatry and Neurology - General Psychiatry.";

		    	      //Doctor 11
$lang['DOCTORS_NAME11']="Dr. Rose A. S.";
$lang['DOCTORS_FIELD11']="Pediatrics";
$lang['DOCTORS_IMAGE11']="<img src=\"images/pediatrics.png\" width=\"45px\" height=\"45px\" alt=\"pediatrics\" />";
$lang['DOCTORS_PORTRAIT11']="<img src=\"images/docs/Doctor5.png\"/>";
$lang['DOCTORS_REGION11']="Orlando";
$lang['DOCTORS_EXPERIENCE_TEXT11']="Hospital (8 years)";
$lang['DOCTORS_WHY_TEXT11']="Sometimes parents stay at home thinking their self-diagnosis for their child is right. Yet, self-diagnosis is both misleading and dangerous, especially for young children. With Medlanes, I am easily reachable when necessary, for a proper and reliable expertise.";
$lang['DOCTORS_EDUCATION_TEXT11']="Fellow - Cardiovascular Anesthesiology, Michael Reese Hospital</p>
          <p>Resident - Anesthesiology, Michael Reese Hospital</p>
          <p>MD, Loyola University &amp; Stritch School of Medicine</p>
          <p>MSEE - Electrical and Computer Engineering, University of Wisconsin,</p>
          <p>BSE - Biomedical Engineering, Duke University";
$lang['DOCTORS_HIGHLIGHTS_TEXT11']="Chair, Membership Committee, American College of Pediatrics, 2006-present";		  
$lang['DOCTORS_CERTIFICATION_TEXT11']="American Board of Pediatrics</p>
          <p>Diplomate, American Board of Psychiatry and Neurology</p>
          <p>Diplomate, American Board of Psychiatry and Neurology - General Psychiatry.";
		  
		    	      //Doctor 12
$lang['DOCTORS_NAME12']="Dr. Rose A. S.";
$lang['DOCTORS_FIELD12']="Pediatrics";
$lang['DOCTORS_IMAGE12']="<img src=\"images/pediatrics.png\" width=\"45px\" height=\"45px\" alt=\"pediatrics\" />";
$lang['DOCTORS_PORTRAIT12']="<img src=\"images/docs/Doctor5.png\"/>";
$lang['DOCTORS_REGION12']="Orlando";
$lang['DOCTORS_EXPERIENCE_TEXT12']="Hospital (8 years)";
$lang['DOCTORS_WHY_TEXT12']="Sometimes parents stay at home thinking their self-diagnosis for their child is right. Yet, self-diagnosis is both misleading and dangerous, especially for young children. With Medlanes, I am easily reachable when necessary, for a proper and reliable expertise.";
$lang['DOCTORS_EDUCATION_TEXT12']="Fellow - Cardiovascular Anesthesiology, Michael Reese Hospital</p>
          <p>Resident - Anesthesiology, Michael Reese Hospital</p>
          <p>MD, Loyola University &amp; Stritch School of Medicine</p>
          <p>MSEE - Electrical and Computer Engineering, University of Wisconsin,</p>
          <p>BSE - Biomedical Engineering, Duke University";
$lang['DOCTORS_HIGHLIGHTS_TEXT12']="Chair, Membership Committee, American College of Pediatrics, 2006-present";		  
$lang['DOCTORS_CERTIFICATION_TEXT12']="American Board of Pediatrics</p>
          <p>Diplomate, American Board of Psychiatry and Neurology</p>
          <p>Diplomate, American Board of Psychiatry and Neurology - General Psychiatry.";
		  
		    	      //Doctor 13
$lang['DOCTORS_NAME13']="Dr. Rose A. S.";
$lang['DOCTORS_FIELD13']="Pediatrics";
$lang['DOCTORS_IMAGE13']="<img src=\"images/pediatrics.png\" width=\"45px\" height=\"45px\" alt=\"pediatrics\" />";
$lang['DOCTORS_PORTRAIT13']="<img src=\"images/docs/Doctor5.png\"/>";
$lang['DOCTORS_REGION13']="Orlando";
$lang['DOCTORS_EXPERIENCE_TEXT13']="Hospital (8 years)";
$lang['DOCTORS_WHY_TEXT13']="Sometimes parents stay at home thinking their self-diagnosis for their child is right. Yet, self-diagnosis is both misleading and dangerous, especially for young children. With Medlanes, I am easily reachable when necessary, for a proper and reliable expertise.";
$lang['DOCTORS_EDUCATION_TEXT13']="Fellow - Cardiovascular Anesthesiology, Michael Reese Hospital</p>
          <p>Resident - Anesthesiology, Michael Reese Hospital</p>
          <p>MD, Loyola University &amp; Stritch School of Medicine</p>
          <p>MSEE - Electrical and Computer Engineering, University of Wisconsin,</p>
          <p>BSE - Biomedical Engineering, Duke University";
$lang['DOCTORS_HIGHLIGHTS_TEXT13']="Chair, Membership Committee, American College of Pediatrics, 2006-present";		  
$lang['DOCTORS_CERTIFICATION_TEXT13']="American Board of Pediatrics</p>
          <p>Diplomate, American Board of Psychiatry and Neurology</p>
          <p>Diplomate, American Board of Psychiatry and Neurology - General Psychiatry.";
		  
		  		  //Specialities -------------------				 	 
$lang['DOCTORS_TYPE_HEADLINE']="Our Areas of Expertise";
$lang['DOCTORS_TYPE_TEXT']="All doctors who work with Medlanes are carefully selected. Our doctors have years of experience in their field and are specially trained for online advice.";

$lang['DOCTORS-TYPE_title1']="Family Medicine";
$lang['DOCTORS-TYPE_content1']="Most questions are best answered by general physician, they are your guide through medicine! ";

$lang['DOCTORS-TYPE_title2']="Pediatrics";
$lang['DOCTORS-TYPE_content2']="It's important to keep up to date with your child's health! Our pediatric doctors help you with advice any time you need it.";

$lang['DOCTORS-TYPE_title3']="Cardiology";
$lang['DOCTORS-TYPE_content3']="Lower the risk of a heart disease! Our cardiologists can diagnose heart rhythm disturbances and determine best ways how to manage your cardiac disease.";

$lang['DOCTORS-TYPE_title4']="Gynecology";
$lang['DOCTORS-TYPE_content4']="In Gynecology a trusting relationship between patient and physician is important. Our gynecologists do their best to ensure that you are comfortable.";

$lang['DOCTORS-TYPE_title5']="Dermatology";
$lang['DOCTORS-TYPE_content5']="Up to 90% of all skin problems can be successfully diagnosed using medical photo analysis. Our dermatologists evaluate your skin problems and give treatment advice. ";

$lang['DOCTORS-TYPE_title6']="Orthopedics";
$lang['DOCTORS-TYPE_content6']="Anytime there is a suspected traumatic or repetitive motion injury to a bone, joint or nerve one of our orthopedic doctors is your best choice for treatment.";

$lang['DOCTORS-TYPE_title7']="Gastroenterology";
$lang['DOCTORS-TYPE_content7']="When you first notice troublesome signs or symptoms in your digestive system, such as constipation, diarrhea or abdominal pain, you might ask one of our gastroenterologists for qualified medical advice.";



$lang['DOCTORS-TYPE_title8']="Psychotherapy";
$lang['DOCTORS-TYPE_content8']="Mental stress is often only noticeable through physical illnesses. Do not hesitate to ask our qualified psychotherapists for advice.";

$lang['DOCTORS-SPOTLIGHT_title']="Doctor spotlight";

$lang['DOCTORS-SPOTLIGHT_Doctor-name1']="Lars Omera, M.D.";
$lang['DOCTORS-SPOTLIGHT_Doctor-type1']="Cardiology";

$lang['DOCTORS-SPOTLIGHT_Doctor-name2']="Thomas Pers, M.D., P.hD.";
$lang['DOCTORS-SPOTLIGHT_Doctor-type2']="Family medicine / Internal";

$lang['DOCTORS-SPOTLIGHT_Doctor-name3']="Ane Wertheim, M.D.";
$lang['DOCTORS-SPOTLIGHT_Doctor-type3']="Paediatrics";

$lang['DOCTORS-SPOTLIGHT_content']="From time to time we will feature a few of our doctors to get you to know the specialists that work with us to help you! More soon.";

// Privacy Page -------------------
$lang['PRIVACY_TITLE'] = 'Qualified Medical Advice - Anywhere, Anytime, 24/7';
$lang['PRIVACY_DESCRIPTION'] = 'Ask our doctors and describe your symptoms.';


// Terms Page --------------------
$lang['TERMS_TITLE'] = 'Medlanes Terms and Conditions';
$lang['TERMS_DESCRIPTION'] = 'Review the terms and use of Medlanes .';

$lang['TERMS_title']="Terms &amp; Conditions";
$lang['TERMS_cont']="
			<br />
			WE EXCLUSIVELY PROVIDE GENERAL MEDICAL INFORMATION. IN NO WAY CAN THE SERVICE BE USED FOR DIAGNOSIS, TREATMENT OR ANY INDIVIDUAL DECISION.<br />
			<br />
				<h1>1. Acceptance of this service's Terms and Conditions</h1>
				<p>By using this service you are deemed to understand, consent to and agree to be legally bound by these Terms and Conditions. If you do not understand and agree to these terms and conditions you should not use this service. </p>
				<br>
				<h1>2. Terminology</h1>
				<p> a. The term 'Medlanes' is the trading name of Medlanes GmbH</li>
				<p> b. The term 'the Medlanes website' means the URL www.medlanes.com and associated pages.
				<p> c. The term 'doctor' means a doctor working at or associated with Medlanes.
				<p> d. The term 'law' shall be construed as a reference to any law (including common or customary law), statute, judgement, regulation, directive, bye law, order, ordinance or any other legislative measure of any government, supranational, local government, statutory or regulatory body or court, in each case having the force of law.
				<p> e. The term 'Terms and Conditions' comprises of paragraphs 1 to 16 and includes the Privacy Policy and the Disclaimer.
				<br>
				<h1>3. Responsibility of our doctors for their patients </h1>
				<p>With an answer our doctors take exactly the same personal responsibility for each patient as they would if they were seeing a patient face to face. The online nature of this service in no way diminishes the professional obligations that our doctors owe to a patient. Similarly, the existence of Medlanes GmbH as the legal entity behind the Medlanes service in no way seeks to diminish the professional obligations that our doctors owe to a patient.</p>
				<br>
				<h1>4. The importance of providing true, timely and complete information</h1>
				<p>You agree that all information that you provide to our doctors will be true and complete to the best of your knowledge and that you will not purposefully omit to provide information that could reasonably be judged to be potentially relevant to our doctors in providing the service. You should not register or complete questionnaires on behalf of anyone other than yourself.</p>
				<br>
				<h1>5. Scope of services provided and contract</h1>
				<p>Our doctors provide general medical information for specific medical conditions only. Our doctors do not offer medical advice or prescriptions for medical conditions not related to the specific services we provide. In no case can the Medlanes office replace a face to face meeting with a doctor. Because Medlanes is not a pharmacy we do not dispense or deliver any medicines.</p>
				<br>
				<p>5.1 Medlanes – What we do</p>
				 <p> a.We provide general medical information based on online questionnaires you complete, telephone conversations, secure messaging and photo assessments.
				 <p> a.We do not offer any type of diagnosis, treatment of advice in medical emergencies.
				<p>5.2 Our website is only intended for use by people resident in the United Kingdom (the “Serviced Countries”). We do not accept orders from individuals outside those countries.</p>
				<p>5.3 How you should use our Services: By placing an order through our website, you confirm that:</p>
				<p> a.you are legally capable of entering into binding contracts; and
				<p> b.you are at least 18 years old;
				<p> c.you are resident in one of the Serviced Countries; and
				<p> d.you are accessing our website from that country.
				<p>5.4 You must use our website and Services with care and in compliance with the below:</p>
				<p> a.The provision of Services to you is conditional on you completing all consultation questionnaires contained on our website truthfully and honestly. You must reveal and disclose all relevant information truthfully to the best of your knowledge.
				<p> b.We cannot and are not liable for any damages which results from a failure of you to follow advice given on the website or from your failure to pass on information given on the website to your regular healthcare provider. You accept that the advice given on the website does not replace your regular healthcare provider. You must tell your regular healthcare provider about any information we supply.
				<p> c.All the information contained on our website is in English. You are solely responsible for ensuring that you understand the questions in the questionnaires you complete on our website. You must speak to your regular health care provider if you do not understand a question or are unsure how you should answer certain questions or you do not fully understand the advice or information given to you on our website.
				<p>5.5 No contract is formed between you and Medlanes until you are informed in a separate eMail that especially states the formation of the contract. Before that time you can cancel your request at any time. Medlanes is allowed to reject any request without giving a reason.  For contractual purposes, you agree to electronic means of communication and you acknowledge that all contracts, notices, information and other communications that we provide to you electronically comply with any legal requirement that such communications be in writing. This condition does not affect your statutory rights. </p>
				<br>
				<h1>6. Prescriptions written by our doctors</h1>
				<p>Our doctors do not offer any prescriptions.</p>
				<br>
				<h1>7. Informing your existing doctor about any information that is provided</h1>
				<p>You agree that if our doctors provide any information for you you and you want us to inform your existing doctor, you will provide our doctors with the full and correct contact details of your existing doctor. Our doctors will always encourage you to let us inform your existing doctor.</p>
				<br>
				<h1>8. Informed consent</h1>
				<p>You agree that by requesting information you formally give your informed consent for our doctors to provide information</p>
				<br>
				<h1>9. Your Medlanes account</h1>
				<p>a. You agree that if provided you will not share your username and password with anyone. You acknowledge that you will take all reasonable steps to ensure that a third party does not gain access to your account.</p>
				<p>b. You agree that you will take all reasonable steps to ensure that any device that you use to set up or access your account is suitably protected from potential hazards by firewalls, anti-virus software and other such security applications.</p>
				<p>c. You agree that you will not create more than one Medlanes account.</p>
				<p>d. You agree that if when creating your account you provide an email address or contact telephone number, you consent for us to contact you by email and telephone if and when the need arises.</p>
				<br>
				<h1>10. Cancellations and refunds</h1>
				<p>If charged, Medlanes offers a full refund on all request that were not answered.</p>
				<br>
				<h1>11. Privacy Policy and patient confidential data</h1>
				<p>Medlanes has a legal obligation to protect all patient confidential data. We are not allowed to share patient confidential data with third parties, including other doctors, who are unconnected to our service without your explicit consent. To provide this service to patients it is necessary that both our doctors and our non-medical staff working at have access to  patient confidential data. It is also necessary that for the purpose of maintaining our IT systems to provide access to patient data to our IT contractors. If required by law, for the purpose of financial audit Medlanes may be required to divulge very limited patient information to our financial auditors and wherever possible information used for the purpose of financial audit is anonymised. All access to patient information is kept to the minimum required to allow the safe and effective delivery of our services. You have a legal right to obtain any information that we hold about you.</p>
				<br>
				<h1>12. Vulnerabilities of electronic communications and post</h1>
				<p>The online nature of the Medlanes service necessarily relies upon using electronic forms of communication, post, telephone and SMS. There are inherent vulnerabilities in using such forms of communication and we are therefore unable to give a guarantee that a third party unrelated to our service will not intercept communications.</p>
				<br>
				<h1>13. Disclaimer</h1>
				<p>a. To the fullest extent permitted by applicable law, under no circumstances will Medlanes or our doctors be liable for any of the following losses or damage (whether such losses were foreseen, foreseeable, known or otherwise): (a) loss of emotional well-being including, but not limited to, any embarrassment caused (b) loss of data; (c) loss of revenue or anticipated profits; (d) loss of business; (e) loss of opportunity; (f) loss of goodwill or injury to reputation; (g) losses suffered by third parties; or (h) any indirect, consequential, special or exemplary damages arising from the use of the Medlanes service regardless of the form of action. Nothing in this clause is intended to exclude or limit Medlanes Doctor’s liability for death or personal injury caused by its negligence or fraud.</p>
				<p>b. You agree that to the fullest extent permitted by applicable law, Medlanes makes no representations or warranties with respect to any treatment, information, advice or action relied on or followed by any person using the Medlanes  service.</p>
				<p>c. Medlanes does not warrant that the Medlanes websites or this service in general will be either available without interruption or error-free or that the website, server, hardware and software that support the Medlanes service are free from viruses or something that may interfere with the normal operations of your systems. Medlanes will not be liable for any loss or damage resulting from the transmission of data as may occur during communications with Medlanes.</p>
				<p>d. To the fullest extent permitted by applicable law, Medlanes disclaims any liability resulting from the provision of services by third party providers. Liability shall rest with the appropriate third party provider and shall not under any circumstance be deemed to rest with Medlanes.</p>
				<br>
				<h1>14. Exclusive jurisdiction of the courts</h1>
				<p>If any of these terms and conditions should be determined to be illegal, invalid or otherwise unenforceable by reasons of the laws of any state or country in which these terms and conditions are intended to be effective, then to the extent and within the jurisdiction which that term or condition is illegal, invalid or unenforceable, it shall be severed and deleted from the agreement and the remaining terms and conditions shall survive, remain in full force and effect and continue to be binding and enforceable. The above terms and conditions shall be governed by and construed in accordance with local law and you irrevocably submit to its exclusive jurisdiction.</p>
				<br>
				<h1>15. Change to Terms and Conditions</h1>
				<p>Medlanes reserves the right to change these terms and conditions at any time by posting changes online. You are responsible for regularly reviewing information posted online to obtain timely notice of such changes. Your continued use of the website and the entire Medlanes service after changes are posted constitutes your acceptance of these terms and conditions as modified by the posted changes.</p>
			  ";










// Contact Page--------------------

$lang['CONTACT_TITLE'] = 'Contact Medlanes';
$lang['CONTACT_DESCRIPTION'] = 'Questions? Comments? Feel free to contact us 24/7. We are always here for you!';

$lang['contact_slider']="Got questions? Contact us today, we are happy to help!";
$lang['contact_title1']="Our team is at your service";
$lang['contact_cont1']="At Medlanes Support is of the highest importance. We are available around the clock and are more than happy to help you with any question that you might have. Please get in contact with us, we'll get back to you shortly.";

$lang['contact_title2']="Phone and eMail-Support";

$lang['contact_cont2']="Over 90% of requests are answered in less that 24h.";


$lang['contact_blue_box1_text1']="Phone";
$lang['contact_blue_box1_text2']="0800 - 765 43 43";
$lang['contact_blue_box1_text3']="Toll free - available 24/7 ";
$lang['contact_blue_box1_text4']="Request a call back";
$lang['contact_blue_box1_text5']="E-Mail";

$lang['contact_blue_box2_text1']="eMail-Support";
$lang['contact_blue_box2_text2']="info@medlanes.com";
$lang['contact_blue_box2_text3']="Direct answer from 8AM - 6PM";

$lang['contact_form_title']="Contact form";
$lang['contact_form_cont']="You can also send us a message using the form below.";

$lang['contact_form_label_surname']="First name";
$lang['contact_form_label_name']="Last name";
$lang['contact_form_label_email']="eMail";
$lang['contact_form_label_msg']="Message";

$lang['contact_completemsg']="<li class=msg>Thank you!</li>";
$lang['contact_errmsg']="<li class=errorMsg>Error!</li>";
$lang['contact_form_label_send'] = "Send";

// FAQ Page--------------------

$lang['FAQ_TITLE'] = 'FAQs about Online Doctor Visits7';
$lang['FAQ_DESCRIPTION'] = 'Questions about online doctor visits? You are not alone. Here are answers to the most asked questions about online doctor visits..';

$lang['FAQ_main_title']="Frequently Asked Questions";
$lang['FAQ_main_cont']="We strive to provide the best customer service possible. Here are a few of the most commonly asked questions. For everything else please contact us directly. ";

$lang['FAQ_Tab-1']="General";
$lang['FAQ_Tab-2']="Medical";
$lang['FAQ_Tab-3']="Security";
$lang['FAQ_Tab-4']="Payment";

$lang['FAQ_points_title1']="1) Who We Are?";
$lang['FAQ_points_cont1']="Medlanes is a telehealth company located in Berlin, Germany. It was founded by the two experienced professionals, Emil Kendziorra, M.D. and Erik Stoffregen, who wanted to improve the quality of, and access to healthcare. When Emil Kendziorra graduated from medical school in Goettingen, he quickly realized that the future of healthcare will be shaped online. Emil and Erik believe that only online healthcare can lower the cost, increase accessibility and reduce the barriers separating the doctors from their patients. That is why they created Medlanes.";


$lang['FAQ_points_title2']="1) What makes Medlanes unique?";
$lang['FAQ_points_cont2']="Medlanes brings medicine online. Medlanes is available 24/7 and on all devices.";


$lang['FAQ_points_title3']="2) Can I contact a doctor from abroad?";
$lang['FAQ_points_cont3']="Of course, Medlanes is available from anywhere in the world! Just remember that it might be cheaper to use the local WiFi connection compared the the mobile network. ";





$lang['FAQ_points2_title1']="1) What questions can be asked?";
$lang['FAQ_points2_cont1']="You can asked any non-emergency questions using Medlanes. In the case of emergency contact 911 right away!";


$lang['FAQ_points2_title2']="2) Is my doctor seeing patients online?";
$lang['FAQ_points2_cont2']="The amount of doctors seeing patients online increases daily. If your doctor does not yet see patients online give us their contact details and we will send them information about how to join our online practice. Feel free to ask them if they would be interested to offer an online service for their patients.";


$lang['FAQ_points2_title3']="3) What situations is Medlanes build for?";
$lang['FAQ_points2_cont3']="
Let us give you a few examples.<br />

- I don’t feel well and would like to see a doctor, but I'm at work and don't have time. <br />
- My doctor’s office is closed or on vacation and I don’t want to go to a hospital.<br />
- I recently moved and don’t know any doctors in my area.<br />
- I would like to get a second opinion.<br />
- I got a rather simple question and don’t want to wait for hours in the waiting room of my doctor. <br />
- I’m on vacation and would like to talk to a doctor who speaks my language.<br />
- It is Sunday and my child has a fever. I need convenient advice.<br />
";









$lang['FAQ_points3_title1']="1) Is it secure?";
$lang['FAQ_points3_cont1']="Yes, Medlanes uses top level security and encryption to make sure that all your data and medical history files are stored privately and securely";


$lang['FAQ_points3_title2']="2) What are my information used for?";
$lang['FAQ_points3_cont2']="Your personal information is only used to provide advice from a doctor. Your data is not used for anything else";











$lang['FAQ_points4_title1']="1) How much does the service cost? Is it covered by insurance?";
$lang['FAQ_points4_cont1']="
You can download and use the Medlanes App for free. The cost of a doctor consultation is $29.Whether or not the service is covered by your healthcare insurance depends on your individual plan.
";


$lang['FAQ_points4_title2']="2) What if I’m unhappy with my doctor or the service?";
$lang['FAQ_points4_cont2']="
Your feedback is very important to us! If you’re not totally satisfied, then please contact our customer support team at support@medlanes.com. We're more than happy to help you.
";



// About Page--------------------

$lang['ABOUT_TITLE'] = 'About Medlanes';
$lang['ABOUT_DESCRIPTION'] = 'Learn more about Medlanes, and about our vision for a healthier world.';

$lang['ABOUT_BANNER_title1']="We bring medicine online!";
$lang['ABOUT_BANNER_title2']="And make healthcare affordable for everyone.";

$lang['ABOUT_COMPANY_headline']="The Company";
$lang['ABOUT_COMPANY_text']="Medlanes GmbH is a company based in Berlin, Germany that specializes in telemedicine, and virtual doctor consultations. Since our inception in  2013, we have revolutionized the way you see your doctor! We have been awarded with several major grants for our innovations in healthcare from such names as Microsoft and Bayer Pharmaceuticals. Our doctors are available 24/7, and always fully dedicated to the wellness of our patients. No more trips to the doctor, no more hassle of waiting weeks to see a physician for a simple problem. Our doctors are always on call for you, and we complete most of our consultations within 1 hour.";

$lang['ABOUT_VISION_headline']="Our Vision";
$lang['ABOUT_VISION_text']="Medlanes was founded for one basic reason. To provide the highest quality of healthcare to anyone in need, without the common restrictions of seeing a doctor. We give everyone in the world instant access to medicine, anywhere, anytime, and with a price that is affordable. We firmly believe that you shouldn't be denied the best quality of care because of insurance issues, financial hardships, geographical limitations or simply due to time restrictions. Medlanes will revolutionize healthcare, and we aim to change the world! We truly provide healthcare without borders.";

$lang['ABOUT_PEOPLE_headline']="The Team";
$lang['ABOUT_PEOPLE_text']="We have assembled a world class team of doctors with more than 15 different specialities in every state and over 8 different countries on 2 continents! Our team is here 24/7 to provide you with on demand answers to your health issues and give you the peace of mind that your doctor is always in your back pocket.";

$lang['ABOUT_SOLUTION_headline']="Our Solution";
$lang['ABOUT_SOLUTION_text']="We are proud to introduce the Medlanes App! With the Medlanes App, it is now possible to carry your doctor with you! We are available wherever you are, whenever you need us. We provide a fast, easy, and extremely affordable option to get medical advice from your doctor around the clock! ";
// Career page



$lang['JOBS_title1']="Open positions at Medlanes";
$lang['JOBS_cont1']="Detailed job postings will be published in May 2014. We currently have a few open positions, please feel free to send us your unsolicited application.<br />
<br />
We currently have open positions for interns as well. Please apply for positions in marketing and business development. ";

$lang['JOBS_heading']="Open positions at Medlanes";
$lang['JOBS_heading2']="We are always looking for motivated individuals who want to support our Team.";
$lang['JOBS_subheading-two']="Find here the open full-time positions:";
$lang['JOBS_subheading']="Find here the open internship positions:";
$lang['JOBS_list1']="Marketing & Sales Intern (m/f)";
$lang['JOBS_list2']="Technical Research & Development Intern (m/f)";
$lang['JOBS_list3']="Internship in Healthcare Startup in Berlin";
$lang['JOBS_list4']="Executive Assistant Intern (m/f)";
$lang['JOBS_list5']="Junior Marketing (m/f)";
$lang['JOBS_list6']="Junior Sales (m/f)";
$lang['JOBS_listHR']="HR Intern (m/f)";
$lang['JOBS_listBD']="Business Development Intern (m/f)";

$lang['JOBS_list7']="Social Media Intern (m/f)";
$lang['JOBS_list8']="HR/Recruiting Intern at Digital Health Startup (m/f)";
$lang['JOBS_list9']="Technical / Online Marketing Intern (m/f)";
$lang['JOBS_list10']="Sales Trainee (m/f)";

// Impressum Page--------------------

$lang['IMPRINT_TITLE'] = 'Impressum';
$lang['IMPRINT_DESCRIPTION'] = 'Important information about Medlanes Impressum.';

$lang['IMPRESSUM_main_title']="Imprint / Impressum";

$lang['IMPRESSUM_main_content']="Hinweise gem. § 6 des Teledienstegesetzes (TDG) in der Fassung des Gesetzes über rechtliche Rahmenbedingungen für den elektronischen Geschäftsverkehr vom 20.12.2001.";

$lang['IMPRESSUM_address']="Medlanes GmbH<br />
							Stresemannstraße 66A<br />
							10963 Berlin<br />
							<br />
							Fon +49 (30) 577 004 20<br />
							eMail: info@medlanes.com<br />
							<br />
							Geschäftsführer:: Emil Kendziorra, Erik Stoffregen";

$lang['IMPRESSUM_cont_title1']="Internetredaktion/Autor";

$lang['IMPRESSUM_cont_content1']="(Inhaltlich Verantwortlicher gem. § 10 Abs. 3 MDStV und gem.§ 6 MDStV [Staatsvertrag über Mediendienste]:<br />						<br />Emil Kendziorra";

$lang['IMPRESSUM_cont_title2']="Haftungshinweis:";

$lang['IMPRESSUM_cont_content2']="Alle Texte und Bilder sind urheberrechtlich geschützt. Die Veröffentlichung, Übernahme oder Nutzung von Texten, Bildern oder anderen Daten bedürfen der schriftlichen Zustimmung des Herausgebers – eine Ausnahme besteht bei Presseinformationen und den dort veröffentlichten Bildern. Die inhaltliche Verantwortung erfolgt durch die jeweils die Autoren. Trotz aller Bemühungen um möglichst korrekte Darstellung und Prüfung von Sachverhalten sind Irrtümer oder Interpretationsfehler möglich.<br />
							<br />
							Hiermit distanziert sich der Herausgeber ausdrücklich von allen Inhalten aller gelinkten Seiten auf der Homepage eierfabrik.de und macht sich ihren Inhalt nicht zu Eigen! Diese Erklärung gilt für alle auf dieser Homepage angebrachten Links. (Urteil zur “Haftung für Links” am Landgericht Hamburg vom 12. Mai 1998, AZ: 312 O 85/98)<br />
							<br />
							Der Herausgeber nimmt den Schutz Ihrer personenbezogenen Daten sehr ernst. Wir verarbeiten personenbezogene Daten, die beim Besuch auf unserer Webseiten erhoben werden, unter Beachtung der geltenden datenschutzrechtlichen Bestimmungen. Ihre Daten werden von uns weder veröffentlicht, noch unberechtigt an Dritte weitergegeben..";

$lang['IMPRESSUM_cont_title3']="Disclaimer";

$lang['IMPRESSUM_cont_content3']="Diese Website wurde mit größtmöglicher Sorgfalt erstellt. Die Medlanes GmbH i.G. übernimmt jedoch keine Garantie für die Vollständigkeit, Richtigkeit und Aktualität der enthaltenen Informationen. 
							
							Jegliche Haftung für Schäden, die direkt oder indirekt aus der Benutzung dieser Website entstehen, wird ausgeschlossen. Dies gilt auch für Links, auf die diese Website direkt oder indirekt verweist. Die Solidmedia hat keinen Einfluss auf die Gestaltung und die Inhalte der von uns gelinkten Seiten. Deshalb distanzieren wir uns hiermit ausdrücklich von allen Inhalten aller gelinkten Seiten fremder Anbieter. <br />
							<br />
							Bei Falschinformationen oder Fehlern bitten wir Sie, uns dies mitzuteilen. <br />
							<br />
							Diese Website benutzt Google Analytics, einen Webanalysedienst der Google Inc. („Google“). Google Analytics verwendet sog. „Cookies“, Textdateien, die auf Ihrem Computer gespeichert werden und die eine Analyse der Benutzung der Website durch Sie ermöglichen. Die durch den Cookie erzeugten Informationen über Ihre Benutzung dieser Website (einschließlich Ihrer IP-Adresse) wird an einen Server von Google in den USA übertragen und dort gespeichert. Google wird diese Informationen benutzen, um Ihre Nutzung der Website auszuwerten, um Reports über die Websiteaktivitäten für die Websitebetreiber zusammenzustellen und um weitere mit der Websitenutzung und der Internetnutzung verbundene Dienstleistungen zu erbringen. Auch wird Google diese Informationen gegebenenfalls an Dritte übertragen, sofern dies gesetzlich vorgeschrieben oder soweit Dritte diese Daten im Auftrag von Google verarbeiten. Google wird in keinem Fall Ihre IP-Adresse mit anderen Daten von Google in Verbindung bringen. Sie können die Installation der Cookies durch eine entsprechende Einstellung Ihrer Browser Software verhindern; wir weisen Sie jedoch darauf hin, dass Sie in diesem Fall gegebenenfalls nicht sämtliche Funktionen dieser Website vollumfänglich nutzen können. Durch die Nutzung dieser Website erklären Sie sich mit der Bearbeitung der über Sie erhobenen Daten durch Google in der zuvor beschriebenen Art und Weise und zu dem zuvor benannten Zweck einverstanden.";



// Home Page--------------------

$lang['LOGOUT']="You have been logged out";



$lang['PPC_headline']="Get your medical questions answered now";
$lang['SLIDER_headline1']="Tired of Waiting for Hours</br> at the Doctor's Office</br>or Days for an Appointment?";
$lang['SLIDER_button1']="Yes, I Want To Ask A Doctor Now!";
$lang['SLIDER_headline2']="Qualified Medical Advice </br> Anywhere, Anytime, 24/7!";
$lang['SLIDER_button2']="Yes, I Want To Ask A Doctor Now!";
$lang['SLIDER_headline3']="Convenient, Easy and Fast </br> Access to Certified Doctors </br>From the Safety of your Home.";
$lang['SLIDER_button3']="Yes, I Want To Ask A Doctor Now!";
$lang['SLIDER_button4']="Ask A Doctor Now!";


$lang['SEEN']="As seen on:";
$lang['SEEN_images']="<img src=\"images/landingpage/concept.png\" alt=\"concept seen on\" />";

$lang['TESTIMONIAL_headline']="<b>Use cases</b> for Medlanes: ";
$lang['TESTIMONIAL1']="“I was really amazed how fast and easily I could get access to professional medical advice.”";
$lang['TESTIMONIAL1_name']=""; //Paul
$lang['TESTIMONIAL2']="“Medlanes is very convenient and easy to use. Not having to leave the office in order to see the doctor is priceless for me.”";
$lang['TESTIMONIAL2_name']=""; //Matthew
$lang['TESTIMONIAL3']="“I felt helpless about my symptoms, but the doctor from Medlanes made a quick diagnosis and on the next day I felt much better already!”";
$lang['TESTIMONIAL3_name']=""; //Chrisi
$lang['TESTIMONIAL4']="“High quality medical advice at an affordable price, that’s a dream come true for me.”";
$lang['TESTIMONIAL4_name']=""; //Carol
$lang['TESTIMONIAL5']="“As a mother of two, I often don't have the time to wait hours at the doctor's office. Medlanes saves me time and money.”";
$lang['TESTIMONIAL5_name']=""; //Lisa
$lang['TESTIMONIAL6']="“I'm not a good walker anymore, having my health checked from home has been saving me a lot of hassle.”";
$lang['TESTIMONIAL6_name']=""; //Henry
$lang['TESTIMONIAL7']="“The freedom to see a doctor from anywhere in the world is something I don't want to miss anymore!”";
$lang['TESTIMONIAL7_name']=""; //Monica
$lang['TESTIMONIAL8']="“My own symptom research led to many contradictory information. With Medlanes I have certainty!”";
$lang['TESTIMONIAL8_name']=""; //Kevin
$lang['TESTIMONIAL9']="“Medlanes was a quick and affordable way to get a safe feeling about my symptoms. Highly recommended!”";
$lang['TESTIMONIAL9_name']=""; //George
$lang['TESTIMONIAL10']="“Medlanes provides an extraordinary service, which brought me the peace of mind regarding my daughter's condition.”";
$lang['TESTIMONIAL10_name']=""; //Victoria & Lily
$lang['TESTIMONIAL11']="“It's often difficult to find a doctor available on weekends. Medlanes always fits my schedule, no matter when I need it!”";
$lang['TESTIMONIAL11_name']=""; //Janet
$lang['TESTIMONIAL12']="“For standard  health issues I don't have to see the doctor in person: Medlanes is the fastest, easiest, and most convenient way.”";
$lang['TESTIMONIAL12_name']=""; //Douglas
$lang['TESTIMONIAL13']="“I travel a lot, and it's not always easy to find a doctor who speaks English. These worries don't exist with Medlanes.”";
$lang['TESTIMONIAL13_name']=""; //Jason
$lang['TESTIMONIAL14']="“Knowing that I can always rely on the fast and qualified medical advice of Medlanes enables me to sleep well at night.”";
$lang['TESTIMONIAL14_name']=""; //Richard
$lang['TESTIMONIAL15']="“I'm not very familiar with computers and the internet. But Medlanes is very intuitive and easy to use.”";
$lang['TESTIMONIAL15_name']=""; //Linda
$lang['TESTIMONIAL16']="“I love Medlanes! This is how healthcare in the 21st century should be done.”";
$lang['TESTIMONIAL16_name']=""; //Francesca

$lang['BENEFITS_headline']="<b>Licensed Doctors</b> answer your medical questions.";
$lang['BENEFITS_tab1']="Convenient";
$lang['BENEFITS_tab2']="Simple";
$lang['BENEFITS_tab3']="Affordable";
$lang['BENEFITS_tab4']="Time-saving";
$lang['BENEFITS_content1']="Getting medical advice with Medlanes is extremely convenient! Don't stress finding the time to make an appointment or schedule weeks in advance to fit in your schedule. Medlanes is available anytime and from anywhere - on your smartphone and on the web.";
$lang['BENEFITS_content2']="Medlanes is simple, user-friendly and easy to use. Everyone can get instant access to qualified medical advice from board-certified doctors and specialists around the clock and even while being on vacation.";
$lang['BENEFITS_content3']="Healthcare is extremely expensive, not only in direct costs but also due to opportunity costs. Having to take time off just to wait in the waiting room at the doctor's office is quite a hassle. With Medlanes medicine is affordable for everyone, and requires almost no time on your end!";
$lang['BENEFITS_content4']="Stop waiting days for an appointment or hours at the doctor's office. With Medlanes medical advice is available 24 hours a day, 7 days a week.";

$lang['SPOTLIGHT_h1']="Doctor's Spotlight";
$lang['SPOTLIGHT_desc']="A wide variety of doctors is ready to answer your questions - no matter the speciality, we have an expert on board.</br>Discover some of our doctors that help us to revolutionize healthcare and to provide our convenient, fast and time-saving service.";
$lang['SPOTLIGHT_exp']="Experience";
$lang['SPOTLIGHT_why']="Why did you join Medlanes";
$lang['SPOTLIGHT_doctor1_name']="Dr. Thomas P.";
$lang['SPOTLIGHT_doctor1_specialty']="Family Medicine";
$lang['SPOTLIGHT_doctor1_location']="Dallas, TX";
$lang['SPOTLIGHT_doctor1_exp']="<p>Hospital (4 years)</p><p>Private Practice (23 years)</p>   ";
$lang['SPOTLIGHT_doctor1_explanation']="I choose Medlanes because I am convinced that new technologies can improve my daily medical practice. With online consultations, I keep a clear cross-speciality medical record for each patient, which enables more accurate diagnoses.";
$lang['SPOTLIGHT_doctor2_name']="Dr. Rose A.S.";
$lang['SPOTLIGHT_doctor2_specialty']="Pediatrics";
$lang['SPOTLIGHT_doctor2_location']="Orlando,FL";
$lang['SPOTLIGHT_doctor2_exp']="<p>Hospital (7 years)</p> ";
$lang['SPOTLIGHT_doctor2_explanation']="Sometimes parents stay home thinking their self-diagnosis about their child is right. Yet, self-diagnosis is both misleading and dangerous, moreover for young children. With Medlanes, I am easily reachable when necessary, for a proper and reliable expertise.";
$lang['SPOTLIGHT_doctor3_name']="Dr. Richard R.";
$lang['SPOTLIGHT_doctor3_specialty']="Family Medicine";
$lang['SPOTLIGHT_doctor3_location']="Washington,DC";
$lang['SPOTLIGHT_doctor3_exp']="<p>Private Practice (8 years)</p>";
$lang['SPOTLIGHT_doctor3_explanation']="Many of my patients regularly travel to other states or abroad for business; online consultation enables them to consult the same doctor from everywhere.";
$lang['SPOTLIGHT_doctor4_name']="Dr. Eileen B.S.";
$lang['SPOTLIGHT_doctor4_specialty']="Family Medicine";
$lang['SPOTLIGHT_doctor4_location']="Denver, CO";
$lang['SPOTLIGHT_doctor4_exp']=" <p>Private Practice (16 years)</p> ";
$lang['SPOTLIGHT_doctor4_explanation']="Through Medlanes, I can answer queries from remote or isolated persons, like dependent elderly persons, who have to drive sometimes up to 3 hours to access suitable healthcare.";
$lang['SPOTLIGHT_doctor5_name']="Dr. Gary J.R.";
$lang['SPOTLIGHT_doctor5_specialty']="Dermatology";
$lang['SPOTLIGHT_doctor5_location']="New York City, NY";
$lang['SPOTLIGHT_doctor5_exp']="<p>Research (8 years)</p> <p>Hospital (6 years)</p><p>Private Practice (21 years)</p>  ";
$lang['SPOTLIGHT_doctor5_explanation']="After a 35-year career, Medlanes is a great way for me to share my experience more broadly. I feel like bringing a better contribution to the society.";
$lang['SPOTLIGHT_doctor6_name']="Dr. David H.";
$lang['SPOTLIGHT_doctor6_specialty']="Dietetics";
$lang['SPOTLIGHT_doctor6_location']="Sacramento, CA";
$lang['SPOTLIGHT_doctor6_exp']="<p>Humanitarian (3 years)</p> <p>Private Practice (2 years)</p> ";
$lang['SPOTLIGHT_doctor6_explanation']="I defend medical ethics, and thus I like the idea of a fair and foreseeable cost proposed by online consultations, contrary to private practices that sometimes overcharge their patients.";
$lang['SPOTLIGHT_doctor7_name']="Dr. Daniel J.G.";
$lang['SPOTLIGHT_doctor7_specialty']="Pediatrics";
$lang['SPOTLIGHT_doctor7_location']="Birmingham, AL";
$lang['SPOTLIGHT_doctor7_exp']=" <p>Private Practice (8 years)</p> ";
$lang['SPOTLIGHT_doctor7_explanation']="Each condition is different, they can evolve rapidly and differently, therefore they need to be addressed on a regular basis to be treated efficiently, and this is exactly what is now possible with Medlanes.";
$lang['SPOTLIGHT_doctor8_name']="Dr. Charlene C.F.";
$lang['SPOTLIGHT_doctor8_specialty']="Gastroenterology";
$lang['SPOTLIGHT_doctor8_location']="Chicago,IL";
$lang['SPOTLIGHT_doctor8_exp']="<p>Humanitarian (3 years)</p> <p>Hospital (25 years)</p> ";
$lang['SPOTLIGHT_doctor8_explanation']="As a specialist, I know how cumbersome the standard processes to obtain an appointment can be, online consultations minimize time-wasting steps for both patients and doctors.";


// Payment Page--------------------
$lang['PAYMENT_TITLE'] = 'Checkout';
$lang['PAYMENT_DESCRIPTION'] = 'Pay for your online doctor visit, and get the answers to your medical questions.';

$lang['PAYMENT_PAYPAL'] = 'PayPal';	
$lang['PAYMENT_CREDITCARD'] = 'Credit Card';	
$lang['PAYMENT_CONSULTATION'] = 'Online consultation <br> by qualified doctor';
$lang['PAYMENT_PRICE'] = '$29';	
$lang['PAYMENT_CTA'] = 'Start My Consultation Now!'; 

$lang['PAYMENT_MB_h3'] = '30-Day Money Back Guarantee'; 
$lang['PAYMENT_MB_p'] = 'If our doctors are not able to help you, we will refund your costs in full.'; 
$lang['PAYMENT_BCD_h3'] = 'Board-Certified Doctors'; 
$lang['PAYMENT_BCD_p'] = 'All our doctors are certified by one of the 24 boards that certify medical specialist in the United States.'; 
$lang['PAYMENT_ST_h3'] = 'Our website is secure & trusted'; 
$lang['PAYMENT_ST_p'] = 'We actively prevent security threads with 24/7 scanning and 256-bit SSL encryption.'; 
$lang['PAYMENT_P_h3'] = 'Your Privacy is important to us'; 
$lang['PAYMENT_P_p'] = 'We will never share any personal or medical information with third parties. Only doctors have access to medical information.'; 

$lang['PAYMENT_LOOKING']="Looking for your doctor....";
$lang['PAYMENT_PROCESSING']="Please wait a few seconds while we process your data</br>and direct you to the most qualified doctors for your problem.";
$lang['PAYMENT_FOUND']="We found the right doctors for you!";
$lang['PAYMENT_CHOOSE']="Please choose among these selected doctors.";
$lang['PAYMENT_CHECKOUT'] = 'Checkout and start your consultation.';

$lang['PAYMENT_CC_DETAILS']="Payment Details";
$lang['PAYMENT_CC_NUMBER']="Card Number";
$lang['PAYMENT_CC_EXPIRATION']="Expiration Date";
$lang['PAYMENT_CC_CVC']="CVC Code";

$lang['PAYMENT_DOCTOR1_NAME']="Dr. Johanna F. B.";
$lang['PAYMENT_DOCTOR1_JOB1']="Hospital (12 years)";
$lang['PAYMENT_DOCTOR1_JOB2']="Private Practice (9 years)";

$lang['PAYMENT_DOCTOR2_NAME']="Dr. Thomas H. R.";
$lang['PAYMENT_DOCTOR2_JOB1']="Humanitarian (2 years)";
$lang['PAYMENT_DOCTOR2_JOB2']="Private Practice (24 years)";

$lang['PAYMENT_DOCTOR3_NAME']="Dr. James M.";
$lang['PAYMENT_DOCTOR3_JOB1']="Hospital (8 years)";

$lang['PAYMENT_DOCTOR4_NAME']="Dr. Abhiraj G.";
$lang['PAYMENT_DOCTOR4_JOB1']="Humanitarian (3 years)";
$lang['PAYMENT_DOCTOR4_JOB2']="Private Practice (2 years)";

$lang['PAYMENT_COST']="Your consultation fee is <span class=\"bold\">$29 only";
$lang['PAYMENT_SECURE']="Secure payment processed by PayPal";
$lang['PAYMENT_CODE']="R2EJ4X79VDWXG";
$lang['PAYMENT_BUTTON']="Pay now & Meet your doctor";

// Thank You Page--------------------

$lang['THANKYOU_TITLE'] = 'Thank you!';
$lang['THANKYOU_DESCRIPTION'] = 'Thank you for using our online doctors for your medical questions!.';	

$lang['THANKYOU_SENDING'] ="Securely sending data";
$lang['THANKYOU_THANKS'] ="Thank you.";
$lang['THANKYOU_RECEIVED'] ="We received your payment. Your chosen doctor will answer you shortly.";
$lang['THANKYOU_SUPPORT'] ="If you have any questions please don’t hesitate to contact our support team.";

// Sign Up Thank You Page
$lang['STHANKYOU_TITLE'] = 'Thank you!';
$lang['STHANKYOU_DESCRIPTION'] = 'Thank you for using our online doctors for your medical questions!.';	

$lang['STHANKYOU_SENDING'] ="Securely sending data";
$lang['STHANKYOU_THANKS'] ="Thank you.";
$lang['STHANKYOU_RECEIVED'] ="We will notify you when we launch.";
$lang['STHANKYOU_SUPPORT'] ="If you have any questions please don’t hesitate to contact our support team.";

// Conditions--------------------
// Condition Migraine--------------------
$lang['MIGRAINE_ICD_CODE'] = '346';
$lang['MIGRAINE_TITLE'] = 'Migraines';
$lang['MIGRAINE_DESCRIPTION'] = 'Get migraine and headache information and learn about their causes, triggers, treatments, and remedies.';	
$lang['MIGRAINE_OG_TITLE'] ="Migraine";
$lang['MIGRAINE_OG_URL'] ="http://medlanes.com/migraine";    
$lang['MIGRAINE_OG_DESCRIPTION'] = 'Ask our doctors and describe your symptoms.';
$lang['MIGRAINE_TWITTER_TITLE'] ="Migraine";	
$lang['MIGRAINE_TWITTER_URL'] ="http://medlanes.com/migraine";  
$lang['MIGRAINE_TWITTER_DESCRIPTION'] = 'Ask our doctors and describe your symptoms.';                    

$lang['MIGRAINE_HEADLINE'] ="Migraine";
$lang['MIGRAINE_SLIDER'] ="<span  itemprop=\"description\" itemscope itemtype=\"http://schema.org/MedicalCondition\">“Migraine is a primary headache disorder of a pulsing 
quality in one specific area of the head. ” <br><br>  <span style='font-size:16px ;float:right !important; margin-right: 5px;'>All information is validated by expert doctors.</span></span>
</span>";

$lang['MIGRAINE_DESCRIPTION_H2'] ="Description of Migraine";
$lang['MIGRAINE_DESCRIPTION_TEXT'] ="<span itemprop=\"description\" itemscope itemtype=\"http://schema.org/MedicalCondition\">Migraine is a primary headache disorder of a pulsing quality in one specific area of the head. Migraine is commonly associated with nausea, vomiting, and sensitivity to light, sound and sometimes odors.</span> </p><p>There are several different types of migraine including:";
$lang['MIGRAINE_DESCRIPTION_BULLET1'] ="<span itemprop=\"subtype\" itemscope itemtype=\"http://schema.org/MedicalCondition\"><span itemprop=\"name\">Ocular migraine</span></span>, also called ophthalmic migraine or eye migraine";
$lang['MIGRAINE_DESCRIPTION_BULLET2'] ="<span itemprop=\"subtype\" itemscope itemtype=\"http://schema.org/MedicalCondition\"><span itemprop=\"name\">Abdominal migraine</span></span>";
$lang['MIGRAINE_DESCRIPTION_BULLET3'] ="<span itemprop=\"subtype\" itemscope itemtype=\"http://schema.org/MedicalCondition\"><span itemprop=\"name\">Hemiplegic migraine</span></span>";
$lang['MIGRAINE_DESCRIPTION_BULLET4'] ="<span itemprop=\"subtype\" itemscope itemtype=\"http://schema.org/MedicalCondition\"><span itemprop=\"name\">Basilar migraine</span></span>";
$lang['MIGRAINE_DESCRIPTION_BULLET5'] ="<span itemprop=\"subtype\" itemscope itemtype=\"http://schema.org/MedicalCondition\"><span itemprop=\"name\">Vestibular migraine</span></span>";
$lang['MIGRAINE_DESCRIPTION_BULLET6'] ="<span itemprop=\"subtype\" itemscope itemtype=\"http://schema.org/MedicalCondition\"><span itemprop=\"name\">Complex migraine</span></span>";
$lang['MIGRAINE_DESCRIPTION_BULLET7'] ="<span itemprop=\"subtype\" itemscope itemtype=\"http://schema.org/MedicalCondition\"><span itemprop=\"name\">Silent migraine</span></span>";

$lang['MIGRAINE_SIGNS_H2'] ="Signs &amp; Symptoms of Migraine";
$lang['MIGRAINE_SIGNS_TEXT'] ="Depending on the different types of migraine, symptoms of migraine can occur in various combinations and can be divided into four stages:";
$lang['MIGRAINE_SIGNS_H3_1'] ="Prodrome";
$lang['MIGRAINE_SIGNS_H3_1_TEXT'] ="You may notice the following signs one or two days before a migraine attack including: ";
$lang['MIGRAINE_SIGNS_H3_1_BULLET1'] ="Constipation";
$lang['MIGRAINE_SIGNS_H3_1_BULLET2'] ="Depression";
$lang['MIGRAINE_SIGNS_H3_1_BULLET3'] ="Food cravings";
$lang['MIGRAINE_SIGNS_H3_1_BULLET4'] ="Hyperactivity";
$lang['MIGRAINE_SIGNS_H3_1_BULLET5'] ="Irritability";
$lang['MIGRAINE_SIGNS_H3_1_BULLET6'] ="Neck stiffness";
$lang['MIGRAINE_SIGNS_H3_1_BULLET7'] ="Uncontrollable yawning";
$lang['MIGRAINE_SIGNS_H3_2'] ="Aura";
$lang['MIGRAINE_SIGNS_H3_2_TEXT'] ="Most people undergo migraine attacks without aura. Some people experience sensory, motoric or verbal disturbances right before or during migraine headaches. Examples of aura include: ";
$lang['MIGRAINE_SIGNS_H3_2_BULLET1'] ="Visual phenomena such as seeing various shapes, bright spots, flashes of light";
$lang['MIGRAINE_SIGNS_H3_2_BULLET2'] ="Vision loss";
$lang['MIGRAINE_SIGNS_H3_2_BULLET3'] ="Pins and needles sensation in arm or leg";
$lang['MIGRAINE_SIGNS_H3_2_BULLET4'] ="Speech or language problem (Aphasia) ";
$lang['MIGRAINE_SIGNS_H3_2_BULLET5'] ="Limb weakness";
$lang['MIGRAINE_SIGNS_H3_3'] ="Attack";
$lang['MIGRAINE_SIGNS_H3_3_TEXT'] ="Frequencies of attacks vary from person to person. During a migraine, you may experience the following symptoms:";
$lang['MIGRAINE_SIGNS_H3_3_BULLET1'] ="Pain on one side or both sides of the head";
$lang['MIGRAINE_SIGNS_H3_3_BULLET2'] ="Pulsating and throbbing pain";
$lang['MIGRAINE_SIGNS_H3_3_BULLET3'] ="Sensitivity to light, sounds and odors";
$lang['MIGRAINE_SIGNS_H3_3_BULLET4'] ="Abdominal pain";
$lang['MIGRAINE_SIGNS_H3_3_BULLET5'] ="Nausea";
$lang['MIGRAINE_SIGNS_H3_3_BULLET6'] ="Vomiting";
$lang['MIGRAINE_SIGNS_H3_3_BULLET7'] ="Paleness";
$lang['MIGRAINE_SIGNS_H3_3_BULLET8'] ="Blurred vision";
$lang['MIGRAINE_SIGNS_H3_3_BULLET9'] ="Dizziness";
$lang['MIGRAINE_SIGNS_H3_3_BULLET10'] ="Lightheadedness";
$lang['MIGRAINE_SIGNS_H3_3_BULLET11'] ="Fainting";
$lang['MIGRAINE_SIGNS_H3_4'] ="Postdrome";
$lang['MIGRAINE_SIGNS_H3_4_TEXT'] ="Occurs after an attack and can includes: ";
$lang['MIGRAINE_SIGNS_H3_4_BULLET1'] ="Exhaustion ";
$lang['MIGRAINE_SIGNS_H3_4_BULLET2'] ="Fatigue ";
$lang['MIGRAINE_SIGNS_H3_4_BULLET3'] ="Euphoria ";

$lang['MIGRAINE_CAUSES_H2'] ="Causes of Migraine";
$lang['MIGRAINE_CAUSES_TEXT'] ="The specific cause of migraine is not known, but fluctuations in the nervous system and hormone balance appear to play a role. </p> <p>Common triggers of migraine headaches include:";

$lang['MIGRAINE_CAUSES_BULLET1'] ="<b><span itemprop=\"cause\" itemscope itemtype=\"http://schema.org/MedicalCause\"><span itemprop=\"name\">Hormonal changes</span></span>.</b> Women often report headaches before or during their periods as their Estrogen level drops. Others experience migraines during pregnancy or menopause.";
$lang['MIGRAINE_CAUSES_BULLET2'] ="<b><span itemprop=\"cause\" itemscope itemtype=\"http://schema.org/MedicalCause\"><span itemprop=\"name\">Migraines during pregnancy</span></span>.</b> Some women experience migraines during pregnancy or during menopause.";
$lang['MIGRAINE_CAUSES_BULLET3'] ="<b><span itemprop=\"cause\" itemscope itemtype=\"http://schema.org/MedicalCause\"><span itemprop=\"name\">Foods</span></span>.</b> Aged cheeses, salty foods and processed foods may trigger migraines. Skipping meals or fasting can also cause headaches.";
$lang['MIGRAINE_CAUSES_BULLET4'] ="<b><span itemprop=\"cause\" itemscope itemtype=\"http://schema.org/MedicalCause\"><span itemprop=\"name\">Food additives</span></span>.</b> Artificial sweeteners such as Aspartame, flavor enhancers such as monosodium glutamate and preservatives have been implicated in triggering attacks.";
$lang['MIGRAINE_CAUSES_BULLET5'] ="<b><span itemprop=\"cause\" itemscope itemtype=\"http://schema.org/MedicalCause\"><span itemprop=\"name\">Drinks</span></span>.</b> Red wine and other alcohols can cause migraines. Also highly caffeinated beverages can trigger attacks.";
$lang['MIGRAINE_CAUSES_BULLET6'] ="<b><span itemprop=\"cause\" itemscope itemtype=\"http://schema.org/MedicalCause\"><span itemprop=\"name\">Stress</span></span>.</b> Workplace stress and stress at home can cause migraine.";
$lang['MIGRAINE_CAUSES_BULLET7'] ="<b><span itemprop=\"cause\" itemscope itemtype=\"http://schema.org/MedicalCause\"><span itemprop=\"name\">Sensory stimuli</span></span>.</b> Sudden sensory changes such as bright lights and sun glare can induce migraines, as can loud sounds. Unusual strong smells, especially perfume, paint thinner or secondhand smoke can trigger migraines.";
$lang['MIGRAINE_CAUSES_BULLET8'] ="<b><span itemprop=\"cause\" itemscope itemtype=\"http://schema.org/MedicalCause\"><span itemprop=\"name\">Changes in wake-sleep patterns</span></span>.</b> Missing sleep, oversleeping or jet lag can prompt migraines.";
$lang['MIGRAINE_CAUSES_BULLET9'] ="<b><span itemprop=\"cause\" itemscope itemtype=\"http://schema.org/MedicalCause\"><span itemprop=\"name\">Physical factors</span></span>.</b> Intense physical exertion, including sexual activity, may provoke migraines. ";
$lang['MIGRAINE_CAUSES_BULLET10'] ="<b><span itemprop=\"cause\" itemscope itemtype=\"http://schema.org/MedicalCause\"><span itemprop=\"name\">Changes in the environment</span></span>.</b> Sudden changes of weather or barometric pressure can aggravate migraines.";
$lang['MIGRAINE_CAUSES_BULLET11'] ="<b><span itemprop=\"cause\" itemscope itemtype=\"http://schema.org/MedicalCause\"><span itemprop=\"name\">Medications</span></span>.</b> Oral contraceptives such as birth control pills and vasodilators such as nitroglycerin can induce migraine attacks.";
$lang['MIGRAINE_CAUSES_BULLET12'] ="<b><span itemprop=\"cause\" itemscope itemtype=\"http://schema.org/MedicalCause\"><span itemprop=\"name\">Migraines in children</span></span>.</b> Headaches often start in in younger ages and are carried on to adulthood. Migraines in children may present in a similar way to migraine in adults but  neurological (aura) and non-headache symptoms may be more prominent than the headache.";


$lang['MIGRAINE_DIAGNOSIS_H2'] ="Diagnosis of Migraine";
$lang['MIGRAINE_DIAGNOSIS_TEXT'] ="For diagnosing migraine headache the doctor will gather several informations from your medical history to identify a pattern of recurring headaches along with the associated symptoms. It can be useful to keep a diary of your migraine attacks for a few weeks to help with the diagnosis. The doctor might request information such as:";
$lang['MIGRAINE_DIAGNOSIS_BULLET1'] ="<b>Initial phase</b> of headaches";
$lang['MIGRAINE_DIAGNOSIS_BULLET2'] ="Single or multiple <b>type</b> of headaches";
$lang['MIGRAINE_DIAGNOSIS_BULLET3'] ="<b>Frequency</b> of attacks";
$lang['MIGRAINE_DIAGNOSIS_BULLET4'] ="<b>Situations</b> causing headaches";
$lang['MIGRAINE_DIAGNOSIS_BULLET5'] ="<b>Foods</b> triggering attacks";
$lang['MIGRAINE_DIAGNOSIS_BULLET6'] ="<b>Medications</b> causing attacks and current medication";
$lang['MIGRAINE_DIAGNOSIS_BULLET7'] ="<b>Family history</b> of headaches";
$lang['MIGRAINE_DIAGNOSIS_BULLET8'] ="<b>Symptoms accompanied</b> by attacks";
$lang['MIGRAINE_DIAGNOSIS_BULLET9'] ="<b>Influence</b> of migraine in work or school performance";
$lang['MIGRAINE_DIAGNOSIS_BULLET10'] ="<b>Location</b> of pain";
$lang['MIGRAINE_DIAGNOSIS_BULLET11'] ="<b>Development</b> of pain";
$lang['MIGRAINE_DIAGNOSIS_BULLET12'] ="<b>Duration</b> of attack";
$lang['MIGRAINE_DIAGNOSIS_BULLET13'] ="<b>Sudden or signaled occurrence</b> of attacks";
$lang['MIGRAINE_DIAGNOSIS_BULLET14'] ="<b>Time of day</b> the headache usually occurs";
$lang['MIGRAINE_DIAGNOSIS_BULLET15'] ="<b>Occurrence of aura</b> before the headache";
$lang['MIGRAINE_DIAGNOSIS_TEXT2'] ="If the the doctor has diagnosed migraine, right medication and self-help remedies can help to reduce the frequency and severity of your symptoms. For further examination physical and neurological tests might be necessary.";

$lang['MIGRAINE_TREATMENT_H2'] ="Treatment of Migraine";
$lang['MIGRAINE_TREATMENT_TEXT'] ="Currently there is no cure for Migraine, but a number of treatments help to reduce the symptoms. We here at Medlanes already helped thousands of people suffering from migraine by providing our convenient and fast medical advice. Let migraine no longer rule your life and talk to one of our qualified doctors today to get more information on how to get rid of a migraine.";

$lang['MIGRAINE_ASK_H2'] ="Ask a doctor about your Migraine today!";
$lang['MIGRAINE_ASK_TEXT'] ="Medlanes is fast & easy to use. Get your diagnosis within minutes in just three simple steps:";
$lang['MIGRAINE_ASK_BULLET1'] ="Describe your symptoms & ask your questions";
$lang['MIGRAINE_ASK_BULLET2'] ="Choose your doctor & pay";
$lang['MIGRAINE_ASK_BULLET3'] ="Get qualified medical advice";

$lang['MIGRAINE_CONTENT_H2'] ="Table of Contents";
$lang['MIGRAINE_CONTENT1'] ="Description";
$lang['MIGRAINE_CONTENT2'] ="Signs &amp; Symptoms";
$lang['MIGRAINE_CONTENT3'] ="Causes";
$lang['MIGRAINE_CONTENT4'] ="Diagnosis";
$lang['MIGRAINE_CONTENT5'] ="Treatment";

$lang['MIGRAINE_ASK2_H2'] ="Ask a Doctor about Migraine";
$lang['MIGRAINE_ASK2_TEXT'] ="The information on this page is no substitute for consulting with a doctor. </br> If you experience symptoms of migraine we offer easy, convenient and fast access to general practitioners who can give you qualified medical advice for your case.";

$lang['MIGRAINE_DOCTOR_NAME1'] ="Dr. Thomas P.";
$lang['MIGRAINE_DOCTOR_NAME2'] ="Dr. Eileen B.S.";
$lang['MIGRAINE_DOCTOR_NAME3'] ="Dr. James M.";
$lang['MIGRAINE_DOCTOR_NAME4'] ="Dr. Sandy K.";
$lang['MIGRAINE_DOCTOR_NAME5'] ="Dr. Gary J.R.";
$lang['MIGRAINE_DOCTOR_NAME6'] ="Dr. Theodor M.";


$lang['MIGRAINE_SIDEBAR_BUTTON'] ="Ask A Doctor Now For Advice";

// Condition Pink Eye--------------------
$lang['PINKEYE_ICD_CODE'] = '372.30';
$lang['PINKEYE_TITLE'] = 'Pink Eye (Conjunctivitis)';
$lang['PINKEYE_DESCRIPTION'] = 'Get information about pink eye types (viral, bacterial, allergic conjunctivitis), treatment and symptoms and how pink eye spreads.';	
$lang['PINKEYE_OG_TITLE'] ="Pink Eye (Conjunctivitis)";
$lang['PINKEYE_OG_URL'] ="http://medlanes.com/pinkeye";    
$lang['PINKEYE_OG_DESCRIPTION'] = 'Get information about pink eye types (viral, bacterial, allergic conjunctivitis), treatment and symptoms and how pink eye spreads';
$lang['PINKEYE_TWITTER_TITLE'] ="Pink Eye";	
$lang['PINKEYE_TWITTER_URL'] ="http://medlanes.com/pinkeye";  
$lang['PINKEYE_TWITTER_DESCRIPTION'] = 'Get information about pink eye types (viral, bacterial, allergic conjunctivitis), treatment and symptoms and how pink eye spreads';                   

$lang['PINKEYE_HEADLINE'] ="Pink Eye";

$lang['PINKEYE_SLIDER'] ="<span itemprop=\"description\" itemscope itemtype=\"http://schema.org/MedicalCondition\">“A pink eye is commonly caused by a viral or bacterial infection, or an allergic reaction. Although a pink eye rarely affects your vision, early diagnosis and treatment is important as an infectious pink eye is highly contagious.”</span>";

$lang['PINKEYE_DESCRIPTION_H2'] ="Description of Pink Eye";
$lang['PINKEYE_DESCRIPTION_TEXT'] ="<span itemprop=\"description\" itemscope itemtype=\"http://schema.org/MedicalCondition\">A pink eye can be a sign of an inflammation of the conjunctiva, the thin layer of tissue lining the inside of the eyelids and the white part of the eye. The inflammation of the conjunctiva is called conjunctivitis. Due to the inflammation blood vessels are more visible giving the eye a pink or red color.</span>";


$lang['PINKEYE_SIGNS_H2'] ="Signs &amp; Symptoms of Pink Eye";
$lang['PINKEYE_SIGNS_TEXT'] ="Signs and symptoms of pink eye vary depending on the cause. Pink eye symptoms can be divided by the cause and include:";
$lang['PINKEYE_SIGNS_H3_1'] ="Viral Conjunctivitis";
$lang['PINKEYE_SIGNS_H3_1_BULLET1'] ="Cold";
$lang['PINKEYE_SIGNS_H3_1_BULLET2'] ="Sore throat";
$lang['PINKEYE_SIGNS_H3_1_BULLET3'] ="Fever";
$lang['PINKEYE_SIGNS_H3_1_BULLET4'] ="Excessive watering and itching of the eye";
$lang['PINKEYE_SIGNS_H3_1_BULLET5'] ="Fine and diffuse pinkness of the conjunctiva";
$lang['PINKEYE_SIGNS_H3_1_BULLET6'] ="Infection begins in one eye and may easily spread to the other";
$lang['PINKEYE_SIGNS_H3_2'] ="Bacterial Conjunctivitis";
$lang['PINKEYE_SIGNS_H3_2_BULLET1'] ="Rapid onset of redness";
$lang['PINKEYE_SIGNS_H3_2_BULLET2'] ="Swelling of eyelid";
$lang['PINKEYE_SIGNS_H3_2_BULLET3'] ="Yellow-green discharge of pus";
$lang['PINKEYE_SIGNS_H3_2_BULLET4'] ="Ear infection";
$lang['PINKEYE_SIGNS_H3_2_BULLET5'] ="Lids stick together due to severe crusting";
$lang['PINKEYE_SIGNS_H3_2_BULLET6'] ="Scratchy feeling";
$lang['PINKEYE_SIGNS_H3_2_BULLET7'] ="Sensitivity to bright light";
$lang['PINKEYE_SIGNS_H3_2_BULLET8'] ="Infection begins in one eye and may spread to the other within 2-5 days";
$lang['PINKEYE_SIGNS_H3_3'] ="Allergic Reaction";
$lang['PINKEYE_SIGNS_H3_3_BULLET1'] ="Redness";
$lang['PINKEYE_SIGNS_H3_3_BULLET2'] ="Swelling";
$lang['PINKEYE_SIGNS_H3_3_BULLET3'] ="Itching";
$lang['PINKEYE_SIGNS_H3_3_BULLET4'] ="Sneezing";
$lang['PINKEYE_SIGNS_H3_3_BULLET5'] ="Running or blocked nose";
$lang['PINKEYE_SIGNS_H3_3_BULLET6'] ="Scratchy throat";
$lang['PINKEYE_SIGNS_H3_3_BULLET7'] ="Asthma";
$lang['PINKEYE_SIGNS_H3_3_BULLET8'] ="Increased production of tears";
$lang['PINKEYE_SIGNS_H3_4'] ="Irritant";
$lang['PINKEYE_SIGNS_H3_4_BULLET1'] ="Primarily marked redness";

$lang['PINKEYE_CAUSES_H2'] ="Causes of Pink Eye";
$lang['PINKEYE_CAUSES_TEXT'] ="The most common causes of pink eye are viruses, bacteria and allergens and include:";
$lang['PINKEYE_CAUSES_H3_1'] ="Viral Conjunctivitis";
$lang['PINKEYE_CAUSES_H3_1_BULLET1'] ="<span itemprop=\"cause\" itemscope itemtype=\"http://schema.org/MedicalCause\"><span itemprop=\"name\">Upper respiratory tract infection</span></span>";
$lang['PINKEYE_CAUSES_H3_1_BULLET2'] ="<span itemprop=\"cause\" itemscope itemtype=\"http://schema.org/MedicalCause\"><span itemprop=\"name\">Cold</span></span>";
$lang['PINKEYE_CAUSES_H3_1_BULLET3'] ="<span itemprop=\"cause\" itemscope itemtype=\"http://schema.org/MedicalCause\"><span itemprop=\"name\">Sore throat</span></span>";
$lang['PINKEYE_CAUSES_H3_1_BULLET4'] ="<span itemprop=\"cause\" itemscope itemtype=\"http://schema.org/MedicalCause\"><span itemprop=\"name\">Adenoviruses</span></span>";
$lang['PINKEYE_CAUSES_H3_1_BULLET5'] ="<span itemprop=\"cause\" itemscope itemtype=\"http://schema.org/MedicalCause\"><span itemprop=\"name\">Herpes simplex virus</span></span>";
$lang['PINKEYE_CAUSES_H3_1_BULLET6'] ="<span itemprop=\"cause\" itemscope itemtype=\"http://schema.org/MedicalCause\"><span itemprop=\"name\">Varicella zoster virus</span></span>";
$lang['PINKEYE_CAUSES_H3_1_BULLET7'] ="<span itemprop=\"cause\" itemscope itemtype=\"http://schema.org/MedicalCause\"><span itemprop=\"name\">Chicken pox</span></span>";
$lang['PINKEYE_CAUSES_H3_1_BULLET8'] ="<span itemprop=\"cause\" itemscope itemtype=\"http://schema.org/MedicalCause\"><span itemprop=\"name\">HI-Virus</span></span>";
$lang['PINKEYE_CAUSES_H3_2'] ="Bacterial Conjunctivitis";
$lang['PINKEYE_CAUSES_H3_2_BULLET1'] ="<span itemprop=\"cause\" itemscope itemtype=\"http://schema.org/MedicalCause\"><span itemprop=\"name\">Bacteria such as streptococci and staphylococci</span></span>";
$lang['PINKEYE_CAUSES_H3_2_BULLET2'] ="<span itemprop=\"cause\" itemscope itemtype=\"http://schema.org/MedicalCause\"><span itemprop=\"name\">Sexually transmitted infections such as gonorrhoea or chlamydia</span></span>";
$lang['PINKEYE_CAUSES_H3_3'] ="Allergic conjunctivitis has four main types";
$lang['PINKEYE_CAUSES_H3_3_BULLET1'] ="<span itemprop=\"cause\" itemscope itemtype=\"http://schema.org/MedicalCause\"><span itemprop=\"name\">Seasonal: pollen from grass, trees or flowers</span></span>";
$lang['PINKEYE_CAUSES_H3_3_BULLET2'] ="<span itemprop=\"cause\" itemscope itemtype=\"http://schema.org/MedicalCause\"><span itemprop=\"name\">Perennial: dust mites and flakes of dead animal skin</span></span>";
$lang['PINKEYE_CAUSES_H3_3_BULLET3'] ="<span itemprop=\"cause\" itemscope itemtype=\"http://schema.org/MedicalCause\"><span itemprop=\"name\">Contact: medication, cosmetics, chemicals</span></span>";
$lang['PINKEYE_CAUSES_H3_3_BULLET4'] ="<span itemprop=\"cause\" itemscope itemtype=\"http://schema.org/MedicalCause\"><span itemprop=\"name\">Giant: contact lenses, stitches after eye surgery, artificial parts of the eye</span></span>";
$lang['PINKEYE_CAUSES_H3_4'] ="Irritant conjunctivitis";
$lang['PINKEYE_CAUSES_H3_4_BULLET1'] ="<span itemprop=\"cause\" itemscope itemtype=\"http://schema.org/MedicalCause\"><span itemprop=\"name\">Chlorinated water</span></span>";
$lang['PINKEYE_CAUSES_H3_4_BULLET2'] ="<span itemprop=\"cause\" itemscope itemtype=\"http://schema.org/MedicalCause\"><span itemprop=\"name\">Shampoo</span></span>";
$lang['PINKEYE_CAUSES_H3_4_BULLET3'] ="<span itemprop=\"cause\" itemscope itemtype=\"http://schema.org/MedicalCause\"><span itemprop=\"name\">Stray eye lashes</span></span>";
$lang['PINKEYE_CAUSES_H3_4_BULLET4'] ="<span itemprop=\"cause\" itemscope itemtype=\"http://schema.org/MedicalCause\"><span itemprop=\"name\">Smoke</span></span>";
$lang['PINKEYE_CAUSES_H3_4_BULLET5'] ="<span itemprop=\"cause\" itemscope itemtype=\"http://schema.org/MedicalCause\"><span itemprop=\"name\">Fumes</span></span>";
$lang['PINKEYE_CAUSES_H3_4_BULLET6'] ="<span itemprop=\"cause\" itemscope itemtype=\"http://schema.org/MedicalCause\"><span itemprop=\"name\">Dust</span></span>";

$lang['PINKEYE_DIAGNOSIS_H2'] ="Diagnosis of Pink Eye";
$lang['PINKEYE_DIAGNOSIS_TEXT'] ="Pink eye has many causes, but mostly results from a viral or bacterial infection, or from a reaction to an allergen. Signs and symptoms of conjunctivitis may vary depending on the cause and are used to diagnose the type of conjunctivitis. You will provide your doctor with information including:";
$lang['PINKEYE_DIAGNOSIS_BULLET1'] ="<b>Initial phase</b> of symptoms";
$lang['PINKEYE_DIAGNOSIS_BULLET2'] ="<b>Occurrence</b> of symptoms";
$lang['PINKEYE_DIAGNOSIS_BULLET3'] ="<b>Severity</b> of symptoms";
$lang['PINKEYE_DIAGNOSIS_BULLET4'] ="<b>Worsening aspects</b>of symptoms";
$lang['PINKEYE_DIAGNOSIS_BULLET5'] ="<b>Occurrence in one or both eyes</b>";
$lang['PINKEYE_DIAGNOSIS_BULLET6'] ="<b>Use of</b> contact lenses";
$lang['PINKEYE_DIAGNOSIS_BULLET7'] ="<b>Cleaning</b> of contact lenses";
$lang['PINKEYE_DIAGNOSIS_BULLET8'] ="<b>Replacement</b> of contact lenses";
$lang['PINKEYE_DIAGNOSIS_BULLET9'] ="<b>Contact to infectious persons</b> in your surrounding";
$lang['PINKEYE_DIAGNOSIS_TEXT2'] ="Laboratory tests are not usually needed to diagnose viral conjunctivitis. In the case of a suspected bacterial conjunctivitis a sample of your eye discharge may be obtained. Allergic conjunctivitis can be diagnosed from your medical history.";

$lang['PINKEYE_TREATMENT_H2'] ="Treatment of Pink Eye";
$lang['PINKEYE_TREATMENT_TEXT'] ="Pink eye is usually mild and an effective home remedy for pink eye can ease your symptoms fast. However, it is important to seek medical advice for an early diagnoses as especially a bacterial conjunctivitis is easily spread. Get a quick diagnosis on your pink eye symptoms to evaluate the cause. Talk to one of our qualified doctors today and we will advise you on how to get rid of pink eye in not time.";

$lang['PINKEYE_ASK_H2'] ="Ask a doctor about your Pink Eye today!";
$lang['PINKEYE_ASK_TEXT'] ="Medlanes is fast & easy to use. Get your diagnosis within minutes in just three simple steps:";
$lang['PINKEYE_ASK_BULLET1'] ="Describe your symptoms & ask your questions";
$lang['PINKEYE_ASK_BULLET2'] ="Choose your doctor & pay";
$lang['PINKEYE_ASK_BULLET3'] ="Get qualified medical advice";

$lang['PINKEYE_CONTENT_H2'] ="Table of Contents";
$lang['PINKEYE_CONTENT1'] ="Description of Pink Eye";
$lang['PINKEYE_CONTENT2'] ="Signs &amp; Symptoms of Pink Eye";
$lang['PINKEYE_CONTENT3'] ="Causes of Pink Eye";
$lang['PINKEYE_CONTENT4'] ="Diagnosis of Pink Eye";
$lang['PINKEYE_CONTENT5'] ="Treatment of Pink Eye";

$lang['PINKEYE_ASK2_H2'] ="Ask a Doctor about Pink Eye";
$lang['PINKEYE_ASK2_TEXT'] ="The information on this page is no substitute for consulting with a doctor. </br> If you experience symptoms of a pink eye we offer easy, convenient and fast access to general practitioners who can give you qualified medical advice for your case.";

$lang['PINKEYE_DOCTOR_NAME1'] ="Dr. Thomas P.";
$lang['PINKEYE_DOCTOR_NAME2'] ="Dr. Eileen B.S.";
$lang['PINKEYE_DOCTOR_NAME3'] ="Dr. James M.";
$lang['PINKEYE_DOCTOR_NAME4'] ="Dr. Sandy K.";
$lang['PINKEYE_DOCTOR_NAME5'] ="Dr. Gary J.R.";
$lang['PINKEYE_DOCTOR_NAME6'] ="Dr. Theodor M.";


$lang['PINKEYE_SIDEBAR_BUTTON'] ="Ask A Doctor Now For Advice";



// Condition Anemia--------------------
$lang['ANEMIA_ICD_CODE'] = '346';

$lang['ANEMIA_TITLE'] = 'Anemia';
$lang['ANEMIA_DESCRIPTION'] = 'Information about the many types of anemia, causes of anemia, and treatment options.';	
$lang['ANEMIA_OG_TITLE'] ="Anemia";
$lang['ANEMIA_OG_URL'] ="http://medlanes.com/Anemia";    
$lang['ANEMIA_OG_DESCRIPTION'] = 'nformation about the many types of anemia, causes, and treatment options.';
$lang['ANEMIA_TWITTER_TITLE'] ="Anemia";	
$lang['ANEMIA_TWITTER_URL'] ="http://medlanes.com/anemia";  
$lang['ANEMIA_TWITTER_DESCRIPTION'] = 'Information about the many types of anemia, causes, and treatment options.';                    

$lang['ANEMIA_HEADLINE'] ="Anemia";
$lang['ANEMIA_SLIDER'] ="<span itemprop=\"description\" itemscope itemtype=\"http://schema.org/MedicalCondition\">“ Anemia is a condition which your blood lacks red cells, or hemoglobin.Hemoglobin assists in the transportation of oxygen throughout the body. ”<br><br>  <span style='font-size:16px ;float:right !important; margin-right: 5px;'>All information is validated by expert doctors.</span></span></span>";

$lang['ANEMIA_DESCRIPTION_H2'] ="Description of Anemia ";
$lang['ANEMIA_DESCRIPTION_TEXT'] ="<span itemprop=\"description\" itemscope itemtype=\"http://schema.org/MedicalANEMIA\">Anemia is a condition which your blood lacks red cells, or hemoglobin. Hemoglobin assists in the transportation of oxygen throughout the body. If your red blood cell count, or your hemoglobin is abnormal or low, the body is unable to get the oxygen that it requires which can cause a multitude of different symptoms. Anemia is extremely common, and affects more women as opposed to men.</span> </p><p>There are many different types of anemia including:";
$lang['ANEMIA_DESCRIPTION_BULLET1'] ="<span itemprop=\"subtype\" itemscope itemtype=\"http://schema.org/MedicalCondition\"><span itemprop=\"name\">Pernicious Anemia";
$lang['ANEMIA_DESCRIPTION_BULLET2'] ="<span itemprop=\"subtype\" itemscope itemtype=\"http://schema.org/MedicalCondition\"><span itemprop=\"name\">Aplastic Anemia</span></span>";
$lang['ANEMIA_DESCRIPTION_BULLET3'] ="<span itemprop=\"subtype\" itemscope itemtype=\"http://schema.org/MedicalCondition\"><span itemprop=\"name\">Hemolytic Anemia</span></span>";
$lang['ANEMIA_DESCRIPTION_BULLET4'] ="<span itemprop=\"subtype\" itemscope itemtype=\"http://schema.org/MedicalCondition\"><span itemprop=\"name\">Franconi Anemia</span></span>";
$lang['ANEMIA_DESCRIPTION_BULLET5'] ="<span itemprop=\"subtype\" itemscope itemtype=\"http://schema.org/MedicalCondition\"><span itemprop=\"name\">Macroytic Anemia</span></span>";
$lang['ANEMIA_DESCRIPTION_BULLET6'] ="<span itemprop=\"subtype\" itemscope itemtype=\"http://schema.org/MedicalCondition\"><span itemprop=\"name\">Megaloblatic Anemia</span></span>";
$lang['ANEMIA_DESCRIPTION_BULLET7'] ="<span itemprop=\"subtype\" itemscope itemtype=\"http://schema.org/MedicalCondition\"><span itemprop=\"name\">Microcytic Anemia</span></span>";
$lang['ANEMIA_DESCRIPTION_BULLET8'] ="<span itemprop=\"subtype\" itemscope itemtype=\"http://schema.org/MedicalCondition\"><span itemprop=\"name\">Sideroblastic Anemia</span></span>";


$lang['ANEMIA_SIGNS_H2'] ="Signs &amp; Symptoms of Anemia";
$lang['ANEMIA_SIGNS_TEXT'] ="Anemia typically goes undetected in many people, and symptoms can be minor and unpronounced. The signs and symptoms can also be related to the underlying cause of the anemia itself. Common symptoms of anemia include:
";
$lang['ANEMIA_SIGNS_H3_1'] ="Prodrome";
$lang['ANEMIA_SIGNS_H3_1_TEXT'] ="Other, more serious side effects include: ";
$lang['ANEMIA_SIGNS_H3_1_BULLET1'] ="Weakness";
$lang['ANEMIA_SIGNS_H3_1_BULLET2'] ="Extreme Fatigue
";
$lang['ANEMIA_SIGNS_H3_1_BULLET3'] ="General Malaise
";
$lang['ANEMIA_SIGNS_H3_1_BULLET4'] ="Dyspnea (Shortness of Breath)
";
$lang['ANEMIA_SIGNS_H3_1_BULLET5'] ="Muscular Weakness";
$lang['ANEMIA_SIGNS_H3_1_BULLET6'] ="Heart Palpitations
";
$lang['ANEMIA_SIGNS_H3_1_BULLET7'] ="Intermittent Claudication of the Legs
";
$lang['ANEMIA_SIGNS_H3_1_BULLET8'] ="Pale Skin
";
$lang['ANEMIA_SIGNS_H3_1_BULLET9'] ="Jaundice (Yellowing of the Skin)
";
$lang['ANEMIA_SIGNS_H3_1_BULLET10'] ="Spleen Enlargement
";


$lang['ANEMIA_CAUSES_H2'] ="Causes of Anemia";
$lang['ANEMIA_CAUSES_TEXT'] ="There are over 400 different types of anemia, which are divided into two sub groups:";

$lang['ANEMIA_CAUSES_BULLET1'] ="<b><span itemprop=\"cause\" itemscope itemtype=\"http://schema.org/MedicalCause\"><span itemprop=\"name\">Anemia Caused by Blood Loss - Red Blood Cells can be lost through bleeding which can occur slowly over a long period of time, and very frequently go undetected. This bleeding is commonly a result of the following:<br><br>

1. Gastrointestinal Conditions such as Ulcers, Hemorrhoids, and Gastritis <br>
2. Frequent use of nonsteroidal anti-inflammatory drugs such as aspirin, or ibuprofen. <br>
3. Menstruation and Childbirth in Women <br>
4. Iron Deficiency Anemia <br>
5. Massive Vitamin Deficiencies <br>
6. Bone Marrow or Stem Cell Problems <br>

";
$lang['ANEMIA_CAUSES_BULLET2'] ="<b><span itemprop=\"cause\" itemscope itemtype=\"http://schema.org/MedicalCause\"><span itemprop=\"name\">Anemia Caused by Destruction of Red Blood Cells - Red bloods cells are extremely fragile and cannot withstand the routine stress of the circulatory system. This is classified as Hemolytic anemia, and it can be present from birth or develop later in life. Known causes of hemolytic anemia include <br><br>
1. Inherited conditions <br>
2. Stressors such as infections, drugs, snake or spider bites <br>
3. Excess toxin buildup from advanced liver or kidney disease <br>
4. Inappropriate immune system response <br>
5. Enlarged Spleen <br>
";


$lang['ANEMIA_DIAGNOSIS_H2'] ="Diagnosis of Anemia";
$lang['ANEMIA_DIAGNOSIS_TEXT'] ="If anemia is suspected, you must see a physician for a diagnosis. It is imperative that you seek treatment immediately.Anemia can only be diagnosed through a blood test. The blood test will examine the following:";
$lang['ANEMIA_DIAGNOSIS_BULLET1'] ="Red Blood Cell Count";
$lang['ANEMIA_DIAGNOSIS_BULLET2'] ="Hemoglobin Level";
$lang['ANEMIA_DIAGNOSIS_BULLET3'] ="Mean Corpuscular Volume (MCV) - The average volume of a red blood cell";
$lang['ANEMIA_DIAGNOSIS_BULLET4'] ="Red Blood Cell Distribution Width (RCDW) - Measures the variation of red blood cell volume";


$lang['ANEMIA_TREATMENT_H2'] ="Treatment of Anemia";
$lang['ANEMIA_TREATMENT_TEXT'] ="Dependent on the type of anemia, there are different treatments that are available to you. It is of the utmost importance that you consult with a Medlanes physician today to find out the specific options that are available to you." ;
$lang['ANEMIA_ASK_H2'] ="Ask a doctor about your ANEMIA today!";
$lang['ANEMIA_ASK_TEXT'] ="Medlanes is fast & easy to use. Get your diagnosis within minutes in just three simple steps:";
$lang['ANEMIA_ASK_BULLET1'] ="Describe your symptoms & ask your questions";
$lang['ANEMIA_ASK_BULLET2'] ="Choose your doctor & pay";
$lang['ANEMIA_ASK_BULLET3'] ="Get qualified medical advice";

$lang['ANEMIA_CONTENT_H2'] ="Table of Contents";
$lang['ANEMIA_CONTENT1'] ="Description";
$lang['ANEMIA_CONTENT2'] ="Signs &amp; Symptoms";
$lang['ANEMIA_CONTENT3'] ="Causes";
$lang['ANEMIA_CONTENT4'] ="Diagnosis";
$lang['ANEMIA_CONTENT5'] ="Treatment";

$lang['ANEMIA_ASK2_H2'] ="Ask a Doctor about Anemia";
$lang['ANEMIA_ASK2_TEXT'] ="The information on this page is no substitute for consulting with a doctor. </br> If you experience symptoms of Anemia we offer easy, convenient and fast access to general practitioners who can give you qualified medical advice for your case.";

$lang['ANEMIA_DOCTOR_NAME1'] ="Dr. Thomas P.";
$lang['ANEMIA_DOCTOR_NAME2'] ="Dr. Eileen B.S.";
$lang['ANEMIA_DOCTOR_NAME3'] ="Dr. James M.";
$lang['ANEMIA_DOCTOR_NAME4'] ="Dr. Sandy K.";
$lang['ANEMIA_DOCTOR_NAME5'] ="Dr. Gary J.R.";
$lang['ANEMIA_DOCTOR_NAME6'] ="Dr. Theodor M.";


$lang['ANEMIA_SIDEBAR_BUTTON'] ="Ask A Doctor Now For Advice";







// Condition Asthma--------------------
$lang['ASTHMA_ICD_CODE'] = '346';

$lang['ASTHMA_TITLE'] = 'Asthma';
$lang['ASTHMA_DESCRIPTION'] = 'Asthma is a potentially life threatening condition. it is important to understand asthma, including treatments, triggers, and prevention.';	
$lang['ASTHMA_OG_TITLE'] ="Asthma";
$lang['ASTHMA_OG_URL'] ="http://medlanes.com/Asthma";    
$lang['ASTHMA_OG_DESCRIPTION'] = 'Asthma is a potentially life threatening condition. it is important to understand asthma, including treatments, triggers, and prevention.';
$lang['ASTHMA_TWITTER_TITLE'] ="Asthma";	
$lang['ASTHMA_TWITTER_URL'] ="http://medlanes.com/Asthma";  
$lang['ASTHMA_TWITTER_DESCRIPTION'] = 'Asthma is a potentially life threatening condition. it is important to understand asthma, including treatments, triggers, and prevention.';                    

$lang['ASTHMA_HEADLINE'] ="Asthma";
$lang['ASTHMA_SLIDER'] ="<span itemprop=\"description\" itemscope itemtype=\"http://schema.org/MedicalCondition\">“ Asthma is a common chronic inflammatory disease of the airways that is characterized by a set of recurring symptoms of wheezing, shortness of breath, chest tightness, and coughing. ”<br><br>  <span style='font-size:16px ;float:right !important; margin-right: 5px;'>All information is validated by expert doctors.</span></span></span>";

$lang['ASTHMA_DESCRIPTION_H2'] ="Description of Asthma";
$lang['ASTHMA_DESCRIPTION_TEXT'] ="<span itemprop=\"description\" itemscope itemtype=\"http://schema.org/MedicalASTHMA\"> Asthma is a common chronic inflammatory disease of the airways that is characterized by a set of recurring symptoms of wheezing, shortness of breath, chest tightness, and coughing. Although the causes of asthma are not understood, there are several different treatments and asthma remedies available. Asthma episodes are typically triggered by:
";
$lang['ASTHMA_DESCRIPTION_BULLET1'] ="<span itemprop=\"subtype\" itemscope itemtype=\"http://schema.org/MedicalCondition\"><span itemprop=\"name\">Allergies";
$lang['ASTHMA_DESCRIPTION_BULLET2'] ="<span itemprop=\"subtype\" itemscope itemtype=\"http://schema.org/MedicalCondition\"><span itemprop=\"name\">Food and Food Additives</span></span>";
$lang['ASTHMA_DESCRIPTION_BULLET3'] ="<span itemprop=\"subtype\" itemscope itemtype=\"http://schema.org/MedicalCondition\"><span itemprop=\"name\">Exercise Induced Asthma</span></span>";
$lang['ASTHMA_DESCRIPTION_BULLET4'] ="<span itemprop=\"subtype\" itemscope itemtype=\"http://schema.org/MedicalCondition\"><span itemprop=\"name\">Smoking</span></span>";
$lang['ASTHMA_DESCRIPTION_BULLET5'] ="<span itemprop=\"subtype\" itemscope itemtype=\"http://schema.org/MedicalCondition\"><span itemprop=\"name\">Upper Respiratory Infections</span></span>";
$lang['ASTHMA_DESCRIPTION_BULLET6'] ="<span itemprop=\"subtype\" itemscope itemtype=\"http://schema.org/MedicalCondition\"><span itemprop=\"name\">Weather</span></span>";



$lang['ASTHMA_SIGNS_H2'] ="Signs &amp; Symptoms of Asthma";
$lang['ASTHMA_SIGNS_TEXT'] ="Asthma Attack symptoms are severe, and immediate treatment is necessary. These symptoms include:
";

$lang['ASTHMA_SIGNS_H3_1_TEXT2'] ="The severity of an asthma attack and escalate rapidly, so it is important to treat these symptoms as soon as you recognize them.
";
$lang['ASTHMA_SIGNS_H3_1_TEXT'] ="Asthma Attack symptoms are severe, and immediate treatment is necessary. These symptoms include:
 ";
$lang['ASTHMA_SIGNS_H3_1_BULLET1'] ="Losing your breath easily or shortness of breath";
$lang['ASTHMA_SIGNS_H3_1_BULLET2'] ="Feeling very tired or weak when exercising

";
$lang['ASTHMA_SIGNS_H3_1_BULLET3'] ="Decreases or changes in lung functions as measured by a spirometer


";
$lang['ASTHMA_SIGNS_H3_1_BULLET4'] ="Signs of a cold or allergies
";
$lang['ASTHMA_SIGNS_H3_1_BULLET5'] ="Trouble Sleeping";
$lang['ASTHMA_SIGNS_H3_1_BULLET6'] ="Severe Cough that will not stop
";
$lang['ASTHMA_SIGNS_H3_1_BULLET7'] ="Severe wheezing when breathing both in and out
";
$lang['ASTHMA_SIGNS_H3_1_BULLET8'] ="Chest Pain or Pressure
";
$lang['ASTHMA_SIGNS_H3_1_BULLET9'] ="Difficulty talking
";
$lang['ASTHMA_SIGNS_H3_1_BULLET10'] ="Feelings of anxiety or panic
";
$lang['ASTHMA_SIGNS_H3_1_BULLET11'] ="Blue lips or fingernails
";
$lang['ASTHMA_SIGNS_H3_1_BULLET12'] ="Trouble Sleeping
";


$lang['ASTHMA_CAUSES_H2'] ="Causes of Asthma";
$lang['ASTHMA_CAUSES_TEXT'] ="There is no known cause for asthma, as asthma is typically caused by a combination of different factors. These factors include:
";

$lang['ASTHMA_CAUSES_BULLET1'] ="Environmental - Such as allergens, air pollution, and other environmental chemicals";
$lang['ASTHMA_CAUSES_BULLET2'] ="Genetic - Family history greatly increases the risk factors for having asthma";
$lang['ASTHMA_CAUSES_BULLET3'] ="Medical Conditions such as hayfever and allergic rhinitis";
$lang['ASTHMA_CAUSES_BULLET4'] ="Certain medications such as Beta Blockers";
$lang['ASTHMA_CAUSES_BULLET5'] ="Exacerbation";



$lang['ASTHMA_DIAGNOSIS_H2'] ="Diagnosis of Asthma";
$lang['ASTHMA_DIAGNOSIS_TEXT'] ="Asthma is difficult to diagnose, as most patients do not exhibit obvious asthma symptoms when the arrive at the doctors office. Infrequency of the symptoms of asthma also make it extremely hard to diagnose. There are a few different tests for asthma, which include:
";
$lang['ASTHMA_DIAGNOSIS_BULLET1'] ="Spirometry - A lung function test that measures how much air you are able to exhale
";
$lang['ASTHMA_DIAGNOSIS_BULLET2'] ="Peak Flow Testing - This measures the velocity at which you are able to exhale
";
$lang['ASTHMA_DIAGNOSIS_BULLET3'] ="X-Rays - A chest X-Ray will be used to rule out any other causes of your asthma symptoms
";
$lang['ASTHMA_DIAGNOSIS_BULLET4'] ="Methacholine Challenge Test 0 Methacholine is an agent, when inhaled, causes airways to spasm and narrow if asthma is present.
";


$lang['ASTHMA_TREATMENT_H2'] ="Treatment of Asthma";
$lang['ASTHMA_TREATMENT_TEXT'] ="Medications and fast acting inhalers are the typical frontline treatments for asthma. To find out which treatments options will best benefit you, it is important to consult with a Medlanes physician. A few simple questions, and you will have all of the answers you need as well as the peace of mind that your asthma is under control!
" ;


$lang['ASTHMA_ASK_H2'] ="Ask a doctor about your Asthma today!";
$lang['ASTHMA_ASK_TEXT'] ="Medlanes is fast & easy to use. Get your diagnosis within minutes in just three simple steps:";
$lang['ASTHMA_ASK_BULLET1'] ="Describe your symptoms & ask your questions";
$lang['ASTHMA_ASK_BULLET2'] ="Choose your doctor & pay";
$lang['ASTHMA_ASK_BULLET3'] ="Get qualified medical advice";

$lang['ASTHMA_CONTENT_H2'] ="Table of Contents";
$lang['ASTHMA_CONTENT1'] ="Description";
$lang['ASTHMA_CONTENT2'] ="Signs &amp; Symptoms";
$lang['ASTHMA_CONTENT3'] ="Causes";
$lang['ASTHMA_CONTENT4'] ="Diagnosis";
$lang['ASTHMA_CONTENT5'] ="Treatment";

$lang['ASTHMA_ASK2_H2'] ="Ask a Doctor about Asthma";
$lang['ASTHMA_ASK2_TEXT'] ="The information on this page is no substitute for consulting with a doctor. </br> If you experience symptoms of Asthma we offer easy, convenient and fast access to general practitioners who can give you qualified medical advice for your case.";

$lang['ASTHMA_DOCTOR_NAME1'] ="Dr. Thomas P.";
$lang['ASTHMA_DOCTOR_NAME2'] ="Dr. Eileen B.S.";
$lang['ASTHMA_DOCTOR_NAME3'] ="Dr. James M.";
$lang['ASTHMA_DOCTOR_NAME4'] ="Dr. Sandy K.";
$lang['ASTHMA_DOCTOR_NAME5'] ="Dr. Gary J.R.";
$lang['ASTHMA_DOCTOR_NAME6'] ="Dr. Theodor M.";


$lang['ASTHMA_SIDEBAR_BUTTON'] ="Ask A Doctor Now For Advice";







// Condition Bronchitis--------------------
$lang['BRONCHITIS_ICD_CODE'] = '346';

$lang['BRONCHITIS_TITLE'] = 'Bronchitis';
$lang['BRONCHITIS_DESCRIPTION'] = 'Get information about bronchitis including signs, symptoms, causes, treatment options and remedies.';	
$lang['BRONCHITIS_OG_TITLE'] ="Bronchitis";
$lang['BRONCHITIS_OG_URL'] ="http://medlanes.com/Bronchitis";    
$lang['BRONCHITIS_OG_DESCRIPTION'] = 'Get information about bronchitis including signs, symptoms, causes, treatment options and remedies.';
$lang['BRONCHITIS_TWITTER_TITLE'] ="Bronchitis";	
$lang['BRONCHITIS_TWITTER_URL'] ="http://medlanes.com/Bronchitis";  
$lang['BRONCHITIS_TWITTER_DESCRIPTION'] = 'Get information about bronchitis including signs, symptoms, causes, treatment options and remedies.';                    

$lang['BRONCHITIS_HEADLINE'] ="Bronchitis";
$lang['BRONCHITIS_SLIDER'] ="<span itemprop=\"description\" itemscope itemtype=\"http://schema.org/MedicalCondition\">“Bronchitis is a common respiratory disease in which the mucous membrane in the lungs’ bronchial passages become inflamed.”</span>";

$lang['BRONCHITIS_DESCRIPTION_H2'] ="Description of Bronchitis";
$lang['BRONCHITIS_DESCRIPTION_TEXT'] ="<span itemprop=\"description\" itemscope itemtype=\"http://schema.org/MedicalBRONCHITIS\">Bronchitis is a common respiratory disease in which the mucous membrane in the lungs’ bronchial passages become inflamed. As the membrane swells, it narrows and results in coughing, wheezing, and hacking. This may also be accompanied by breathlessness and phlegm.

<br><br> The disease comes in two forms:";
$lang['BRONCHITIS_DESCRIPTION_BULLET1'] ="<span itemprop=\"subtype\" itemscope itemtype=\"http://schema.org/MedicalCondition\"><span itemprop=\"name\">Acute - Lasting from 1 to 3 weeks";
$lang['BRONCHITIS_DESCRIPTION_BULLET2'] ="<span itemprop=\"subtype\" itemscope itemtype=\"http://schema.org/MedicalCondition\"><span itemprop=\"name\">Chronic - Lasting at least 3 months of the year for 2 years in a row</span></span>";
$lang['BRONCHITIS_DESCRIPTION_BULLET3'] ="<span itemprop=\"subtype\" itemscope itemtype=\"http://schema.org/MedicalCondition\"><span itemprop=\"name\">Chronic bronchitis, most often seen in heavy smokers, and lead to chronic obstructive pulmonary diseases (COPD)</span></span>";



$lang['BRONCHITIS_SIGNS_H2'] ="Signs &amp; Symptoms of Bronchitis";
$lang['BRONCHITIS_SIGNS_TEXT'] ="The symptoms of acute bronchitis may include:
";

$lang['BRONCHITIS_SIGNS_H3_1_TEXT2'] ="The severity of an asthma attack and escalate rapidly, so it is important to treat these symptoms as soon as you recognize them.
";
$lang['BRONCHITIS_SIGNS_H3_1_TEXT'] ="Bronchitis Attack symptoms are severe, and immediate treatment is necessary. These symptoms include:
 ";
$lang['BRONCHITIS_SIGNS_H3_1_BULLET1'] ="Hacking cough that lasts for 5 days or more
";
$lang['BRONCHITIS_SIGNS_H3_1_BULLET2'] ="Tenderness or soreness in the chest while coughing

";
$lang['BRONCHITIS_SIGNS_H3_1_BULLET3'] ="Fever


";
$lang['BRONCHITIS_SIGNS_H3_1_BULLET4'] ="Wheezing
";
$lang['BRONCHITIS_SIGNS_H3_1_BULLET5'] ="Breathlessness";
$lang['BRONCHITIS_SIGNS_H3_1_BULLET6'] ="Fatigue
";



$lang['BRONCHITIS_CAUSES_H2'] ="Causes of Bronchitis";
$lang['BRONCHITIS_CAUSES_TEXT'] ="Acute bronchitis is generally caused by lung infections, 90% of which are viral in origin. Repeated attacks which weaken and irritate the airway over time can result in chronic bronchitis. Other causes include:
";

$lang['BRONCHITIS_CAUSES_BULLET1'] ="Industrial Pollution";
$lang['BRONCHITIS_CAUSES_BULLET2'] ="Exposure to Dust or Fumes";
$lang['BRONCHITIS_CAUSES_BULLET3'] ="Exposure to certain Caustic Chemicals";
$lang['BRONCHITIS_CAUSES_BULLET4'] ="Cigarette Smoking";
$lang['BRONCHITIS_CAUSES_BULLET5'] ="Allergies";



$lang['BRONCHITIS_DIAGNOSIS_H2'] ="Diagnosis of Bronchitis";
$lang['BRONCHITIS_DIAGNOSIS_TEXT'] ="Typically, a simple visit to your doctor is enough to diagnose bronchitis. Depending on your symptoms, your physician may request additional testing such as a chest X-Ray to rule out pneumonia. This is usually the case when the symptoms of bronchitis are accompanied by a fever over 100.7 degrees fahrenheit.
";

$lang['BRONCHITIS_TREATMENT_H2'] ="Treatment of Bronchitis";
$lang['BRONCHITIS_TREATMENT_TEXT'] ="Due to the causes of bronchitis, it is important to consult with your Medlanes physician to rule out any possible underlying causes of bronchitis. Your physician will recommend a treatment plan, as well as possible lifestyle changes to prevent future attacks!
" ;


$lang['BRONCHITIS_ASK_H2'] ="Ask a doctor about your Bronchitis today!";
$lang['BRONCHITIS_ASK_TEXT'] ="Medlanes is fast & easy to use. Get your diagnosis within minutes in just three simple steps:";
$lang['BRONCHITIS_ASK_BULLET1'] ="Describe your symptoms & ask your questions";
$lang['BRONCHITIS_ASK_BULLET2'] ="Choose your doctor & pay";
$lang['BRONCHITIS_ASK_BULLET3'] ="Get qualified medical advice";

$lang['BRONCHITIS_CONTENT_H2'] ="Table of Contents";
$lang['BRONCHITIS_CONTENT1'] ="Description of Bronchitis";
$lang['BRONCHITIS_CONTENT2'] ="Signs &amp; Symptoms of Bronchitis";
$lang['BRONCHITIS_CONTENT3'] ="Causes of Bronchitis";
$lang['BRONCHITIS_CONTENT4'] ="Diagnosis of Bronchitis";
$lang['BRONCHITIS_CONTENT5'] ="Treatment of Bronchitis";

$lang['BRONCHITIS_ASK2_H2'] ="Ask a Doctor about Bronchitis";
$lang['BRONCHITIS_ASK2_TEXT'] ="The information on this page is no substitute for consulting with a doctor. </br> If you experience symptoms of Bronchitis we offer easy, convenient and fast access to general practitioners who can give you qualified medical advice for your case.";

$lang['BRONCHITIS_DOCTOR_NAME1'] ="Dr. Thomas P.";
$lang['BRONCHITIS_DOCTOR_NAME2'] ="Dr. Eileen B.S.";
$lang['BRONCHITIS_DOCTOR_NAME3'] ="Dr. James M.";
$lang['BRONCHITIS_DOCTOR_NAME4'] ="Dr. Sandy K.";
$lang['BRONCHITIS_DOCTOR_NAME5'] ="Dr. Gary J.R.";
$lang['BRONCHITIS_DOCTOR_NAME6'] ="Dr. Theodor M.";


$lang['BRONCHITIS_SIDEBAR_BUTTON'] ="Ask A Doctor Now For Advice";







// Condition Cellulitis--------------------
$lang['CELLULITIS_ICD_CODE'] = '346';

$lang['CELLULITIS_TITLE'] = 'Cellulitis';
$lang['CELLULITIS_DESCRIPTION'] = 'Information about cellulitis including signs, causes, treatments, and remedies.';	
$lang['CELLULITIS_OG_TITLE'] ="Cellulitis";
$lang['CELLULITIS_OG_URL'] ="http://medlanes.com/Cellulitis";    
$lang['CELLULITIS_OG_DESCRIPTION'] = 'Information about cellulitis including signs, causes, treatments, and remedies.';
$lang['CELLULITIS_TWITTER_TITLE'] ="Cellulitis";	
$lang['CELLULITIS_TWITTER_URL'] ="http://medlanes.com/Cellulitis";  
$lang['CELLULITIS_TWITTER_DESCRIPTION'] = 'Information about cellulitis including signs, causes, treatments, and remedies.';                    

$lang['CELLULITIS_HEADLINE'] ="Cellulitis";
$lang['CELLULITIS_SLIDER'] ="<span itemprop=\"description\" itemscope itemtype=\"http://schema.org/MedicalCondition\">“ Cellulitis is a common infection of the skin and the subcutaneous tissues underneath. ”</span>";

$lang['CELLULITIS_DESCRIPTION_H2'] ="Description of Cellulitis";
$lang['CELLULITIS_DESCRIPTION_TEXT'] ="<span itemprop=\"description\" itemscope itemtype=\"http://schema.org/MedicalCELLULITIS\">Cellulitis is a common infection of the skin and the subcutaneous tissues underneath. Cellulitis occurs when bacteria enters through a break in the skin and spreads in the subcutaneous layer. Cellulitis specifically affects the dermis and subcutaneous fat. The legs and face are the most common sites involved, though cellulitis can occur on any part of the body.
";
$lang['CELLULITIS_DESCRIPTION_BULLET1'] ="<span itemprop=\"subtype\" itemscope itemtype=\"http://schema.org/MedicalCondition\"><span itemprop=\"name\">Allergies";
$lang['CELLULITIS_DESCRIPTION_BULLET2'] ="<span itemprop=\"subtype\" itemscope itemtype=\"http://schema.org/MedicalCondition\"><span itemprop=\"name\">Food and Food Additives</span></span>";
$lang['CELLULITIS_DESCRIPTION_BULLET3'] ="<span itemprop=\"subtype\" itemscope itemtype=\"http://schema.org/MedicalCondition\"><span itemprop=\"name\">Exercise Induced Asthma</span></span>";
$lang['CELLULITIS_DESCRIPTION_BULLET4'] ="<span itemprop=\"subtype\" itemscope itemtype=\"http://schema.org/MedicalCondition\"><span itemprop=\"name\">Smoking</span></span>";
$lang['CELLULITIS_DESCRIPTION_BULLET5'] ="<span itemprop=\"subtype\" itemscope itemtype=\"http://schema.org/MedicalCondition\"><span itemprop=\"name\">Upper Respiratory Infections</span></span>";
$lang['CELLULITIS_DESCRIPTION_BULLET6'] ="<span itemprop=\"subtype\" itemscope itemtype=\"http://schema.org/MedicalCondition\"><span itemprop=\"name\">Weather</span></span>";



$lang['CELLULITIS_SIGNS_H2'] ="Signs &amp; Symptoms of Cellulitis";
$lang['CELLULITIS_SIGNS_TEXT'] ="Cellulitis can appear on almost any part of the body. Typically cellulitis will be found on damaged skin, infected wounds, dirty cutes, and areas with poor circulation. Common symptoms of cellulitis include:
";

$lang['CELLULITIS_SIGNS_H3_1_TEXT2'] ="The severity of an asthma attack and escalate rapidly, so it is important to treat these symptoms as soon as you recognize them.
";
$lang['CELLULITIS_SIGNS_H3_1_TEXT'] ="More serious symptoms that require immediate treatment include:

 ";
$lang['CELLULITIS_SIGNS_H3_1_BULLET1'] ="Redness";
$lang['CELLULITIS_SIGNS_H3_1_BULLET2'] ="Red Streaking to the affected Area caused by lymphatic response


";
$lang['CELLULITIS_SIGNS_H3_1_BULLET3'] ="Swelling


";
$lang['CELLULITIS_SIGNS_H3_1_BULLET4'] ="Warmth to the Touch

";
$lang['CELLULITIS_SIGNS_H3_1_BULLET5'] ="Pain or Tenderness in the affected Area";
$lang['CELLULITIS_SIGNS_H3_1_BULLET6'] ="Leaking of Yellow or clear Fluid or Pus
";
$lang['CELLULITIS_SIGNS_H3_1_BULLET7'] ="Nausea and Vomiting

";
$lang['CELLULITIS_SIGNS_H3_1_BULLET8'] ="Enlarging or Hardening of the affected Area
";
$lang['CELLULITIS_SIGNS_H3_1_BULLET9'] ="Increase in pain
";
$lang['CELLULITIS_SIGNS_H3_1_BULLET10'] ="Area is Numb to the Touch
";



$lang['CELLULITIS_CAUSES_H2'] ="Causes of Cellulitis";
$lang['CELLULITIS_CAUSES_TEXT'] ="Cellulitis is caused by a bacterial infection. Common causes are:";

$lang['CELLULITIS_CAUSES_BULLET1'] ="Injuries that tear the skin";
$lang['CELLULITIS_CAUSES_BULLET2'] ="Post surgical infection";
$lang['CELLULITIS_CAUSES_BULLET3'] ="Long term skin conditions such as eczema or psoriasis";
$lang['CELLULITIS_CAUSES_BULLET4'] ="Foreign objects in the skin";
$lang['CELLULITIS_CAUSES_BULLET5'] ="Bone infections underneath the skin";
$lang['CELLULITIS_CAUSES_BULLET6'] ="Poor circulation to affected areas";
$lang['CELLULITIS_CAUSES_BULLET7'] ="Those who are diagnosed with diabetes are at an increased risk for contracting cellulitis";
$lang['CELLULITIS_CAUSES_BULLET8'] ="Spider bites, blistering, and animal bites";
$lang['CELLULITIS_CAUSES_BULLET9'] ="Dry Skin";
$lang['CELLULITIS_CAUSES_BULLET10'] ="Immunodeficiency";




$lang['CELLULITIS_DIAGNOSIS_H2'] ="Diagnosis of Cellulitis";
$lang['CELLULITIS_DIAGNOSIS_TEXT'] ="Cellulitis is typically diagnosed solely by physical examination of the affected site. Depending on the severity of the problem, your doctor may order additional tests which include:

";
$lang['CELLULITIS_DIAGNOSIS_BULLET1'] ="A blood test if the infection is suspected to have spread to your blood
";
$lang['CELLULITIS_DIAGNOSIS_BULLET2'] ="An X-Ray if there is a foreign object in the skin or the bone underneath that is possibly infected
";
$lang['CELLULITIS_DIAGNOSIS_BULLET3'] ="A culture to check the specific type of bacteria
";
$lang['CELLULITIS_DIAGNOSIS_BULLET4'] ="Compression Leg Ultrasound
";


$lang['CELLULITIS_TREATMENT_H2'] ="Treatment of Cellulitis";
$lang['CELLULITIS_TREATMENT_TEXT'] ="Immediately addressing suspected cellulitis is key, as the type of bacteria could possible be of an aggressive nature such as Methicillin Resistant Staphylococcus Aureus (MRSA), a flesh eating disease. Typically, as simple round of antibiotics is sufficient in the treatment of cellulitis, but it is important to consult your physician immediately. We at Medlanes have diagnosed and treated thousands of cases of cellulitis, and are happy to help you too! 

" ;


$lang['CELLULITIS_ASK_H2'] ="Ask a doctor about your Cellulitis today!";
$lang['CELLULITIS_ASK_TEXT'] ="Medlanes is fast & easy to use. Get your diagnosis within minutes in just three simple steps:";
$lang['CELLULITIS_ASK_BULLET1'] ="Describe your symptoms & ask your questions";
$lang['CELLULITIS_ASK_BULLET2'] ="Choose your doctor & pay";
$lang['CELLULITIS_ASK_BULLET3'] ="Get qualified medical advice";

$lang['CELLULITIS_CONTENT_H2'] ="Table of Contents";
$lang['CELLULITIS_CONTENT1'] ="Description of Cellulitis";
$lang['CELLULITIS_CONTENT2'] ="Signs &amp; Symptoms of Cellulitis";
$lang['CELLULITIS_CONTENT3'] ="Causes of Cellulitis";
$lang['CELLULITIS_CONTENT4'] ="Diagnosis of Cellulitis";
$lang['CELLULITIS_CONTENT5'] ="Treatment of Cellulitis";

$lang['CELLULITIS_ASK2_H2'] ="Ask a Doctor about Cellulitis";
$lang['CELLULITIS_ASK2_TEXT'] ="The information on this page is no substitute for consulting with a doctor. </br> If you experience symptoms of Cellulitis we offer easy, convenient and fast access to general practitioners who can give you qualified medical advice for your case.";

$lang['CELLULITIS_DOCTOR_NAME1'] ="Dr. Thomas P.";
$lang['CELLULITIS_DOCTOR_NAME2'] ="Dr. Eileen B.S.";
$lang['CELLULITIS_DOCTOR_NAME3'] ="Dr. James M.";
$lang['CELLULITIS_DOCTOR_NAME4'] ="Dr. Sandy K.";
$lang['CELLULITIS_DOCTOR_NAME5'] ="Dr. Gary J.R.";
$lang['CELLULITIS_DOCTOR_NAME6'] ="Dr. Theodor M.";


$lang['CELLULITIS_SIDEBAR_BUTTON'] ="Ask A Doctor Now For Advice";













// Condition Diarrhea--------------------
$lang['DIARRHEA_ICD_CODE'] = '346';

$lang['DIARRHEA_TITLE'] = 'Diarrhea';
$lang['DIARRHEA_DESCRIPTION'] = 'Information about diarrhea including, causes, treatment options, and home remedies.';	
$lang['DIARRHEA_OG_TITLE'] ="Diarrhea";
$lang['DIARRHEA_OG_URL'] ="http://medlanes.com/Diarrhea";    
$lang['DIARRHEA_OG_DESCRIPTION'] = 'Information about diarrhea including, causes, treatment options, and home remedies.';
$lang['DIARRHEA_TWITTER_TITLE'] ="Diarrhea";	
$lang['DIARRHEA_TWITTER_URL'] ="http://medlanes.com/Diarrhea";  
$lang['DIARRHEA_TWITTER_DESCRIPTION'] = 'Information about diarrhea including, causes, treatment options, and home remedies..';                    

$lang['DIARRHEA_HEADLINE'] ="Diarrhea";
$lang['DIARRHEA_SLIDER'] ="<span itemprop=\"description\" itemscope itemtype=\"http://schema.org/MedicalCondition\">“Diarrhea is a common symptom of gastrointestinal infections caused by viruses, bacteria or parasites. ”</span>";

$lang['DIARRHEA_DESCRIPTION_H2'] ="Description of Diarrhea";
$lang['DIARRHEA_DESCRIPTION_TEXT'] ="<span itemprop=\"description\" itemscope itemtype=\"http://schema.org/MedicalDIARRHEA\"> Diarrhea is a common symptom of gastrointestinal infections caused by viruses, bacteria or parasites. Acute diarrhea usually last a couple of days. A longer occurrence is a sign of a functional disorder and is referred to as chronic diarrhea.
<br><br> Diarrhea leads to a high loss of fluid and can be life threatening. Especially diarrhea in toddlers and elderly people need to be taken seriously. <br><br>Diarrhea is the passage of looser or more frequent stools than usual. Diarrhea is classified as:
";
$lang['DIARRHEA_DESCRIPTION_BULLET1'] ="<span itemprop=\"subtype\" itemscope itemtype=\"http://schema.org/MedicalCondition\"><span itemprop=\"name\"><b>Osmotic diarrhea.</b> Water is drawn from the body into the bowel.
";
$lang['DIARRHEA_DESCRIPTION_BULLET2'] ="<span itemprop=\"subtype\" itemscope itemtype=\"http://schema.org/MedicalCondition\"><span itemprop=\"name\"><b>Secretory diarrhea.</b> Increase of the active secretion or inhibition of absorption.</span></span>";
$lang['DIARRHEA_DESCRIPTION_BULLET3'] ="<span itemprop=\"subtype\" itemscope itemtype=\"http://schema.org/MedicalCondition\"><span itemprop=\"name\"><b>Exudative diarrhea.</b> Presence of blood and pus in the stool.</span></span>";



$lang['DIARRHEA_SIGNS_H2'] ="Signs &amp; Symptoms of Diarrhea";
$lang['DIARRHEA_SIGNS_TEXT'] ="Signs and symptoms accompanied with diarrhea may include:
";

$lang['DIARRHEA_SIGNS_H3_1_TEXT2'] ="If you have persistent diarrhea your doctor may perform further testing as physical exams, blood and stool tests. Your doctor might also refer you to a gastroenterologist, a doctor who is specialized in the digestive system. ";
$lang['DIARRHEA_SIGNS_H3_1_TEXT'] ="More serious symptoms that require immediate treatment include:

 ";
$lang['DIARRHEA_SIGNS_H3_1_BULLET1'] ="Increased bowel movements";
$lang['DIARRHEA_SIGNS_H3_1_BULLET2'] ="Urgency of bowel movements


";
$lang['DIARRHEA_SIGNS_H3_1_BULLET3'] ="Incontinence


";
$lang['DIARRHEA_SIGNS_H3_1_BULLET4'] ="Rectal pain

";
$lang['DIARRHEA_SIGNS_H3_1_BULLET5'] ="Abdominal cramps
";
$lang['DIARRHEA_SIGNS_H3_1_BULLET6'] ="Abdominal pain
";
$lang['DIARRHEA_SIGNS_H3_1_BULLET7'] ="Fever

";
$lang['DIARRHEA_SIGNS_H3_1_BULLET8'] ="Bloating
";
$lang['DIARRHEA_SIGNS_H3_1_BULLET9'] ="Nausea
";
$lang['DIARRHEA_SIGNS_H3_1_BULLET10'] ="Vomiting
";
$lang['DIARRHEA_SIGNS_H3_1_BULLET11'] ="Dehydration
";



$lang['DIARRHEA_CAUSES_H2'] ="Causes of Diarrhea";
$lang['DIARRHEA_CAUSES_TEXT'] ="The most common cause of diarrhea is a viral, bacterial or parasitic bowel infection. Causes may include:";

$lang['DIARRHEA_CAUSES_BULLET1'] ="Viruses. Viruses causing an infection include Norovirus and Rotavirus.
";
$lang['DIARRHEA_CAUSES_BULLET2'] ="Bacteria. Bacteria such as Escherichia coli, salmonella and shigella can cause an infection as a result of food poisoning. 
";
$lang['DIARRHEA_CAUSES_BULLET3'] ="Parasites. Parasites such as the Giardia intestinalis parasite causes diarrhea as a result of giardiasis.";
$lang['DIARRHEA_CAUSES_BULLET4'] ="Medications. Some medications, especially antibiotics can disturb the natural balance of bacteria in the body as both good and bad bacteria are destroyed. The disturbance can lead to another bacterial infection.";
$lang['DIARRHEA_CAUSES_BULLET5'] ="Lactose intolerance. A lot of people experience diarrhea after eating dairy products which often indicates lactose intolerance. Lactose is a milk sugar which many people are not able to digest.";
$lang['DIARRHEA_CAUSES_BULLET6'] ="Fructose. Fructose is a sugar found naturally in fruits. The inability to digest Fructose can cause diarrhea after eating fruits.";
$lang['DIARRHEA_CAUSES_BULLET7'] ="Artificial Sweeteners. Artificial sweeteners such as mannitol and sorbitol are often found in chewing gum or other sugar free products.";
$lang['DIARRHEA_CAUSES_BULLET8'] ="Surgery. Abdominal surgeries or gall bladder removal surgery can lead to an infection.";
$lang['DIARRHEA_CAUSES_BULLET9'] ="Other digestive disorders. Crohn’s disease, ulcerative colitis, celiac disease, microscopic colitis and irritable bowel syndrome can result in chronic condition.";
$lang['DIARRHEA_CAUSES_BULLET10'] ="Diarrhea and pregnancy. Occurence in early pregnancy is normally short lived and often caused by changes in dietary habits. ";




$lang['DIARRHEA_DIAGNOSIS_H2'] ="Diagnosis of Diarrhea";
$lang['DIARRHEA_DIAGNOSIS_TEXT'] ="The most important tool for diagnosing the cause of your diarrhea is the information you provide. Information your doctor will need include:

";
$lang['DIARRHEA_DIAGNOSIS_BULLET1'] ="Initial phase of symptoms
";
$lang['DIARRHEA_DIAGNOSIS_BULLET2'] ="Occurrence of symptoms
";
$lang['DIARRHEA_DIAGNOSIS_BULLET3'] ="Severity of symptoms
";
$lang['DIARRHEA_DIAGNOSIS_BULLET4'] ="Improving aspects of symptoms
";
$lang['DIARRHEA_DIAGNOSIS_BULLET5'] ="Worsening aspects of symptoms
";
$lang['DIARRHEA_DIAGNOSIS_BULLET6'] ="Sleep interruption caused by diarrhea
";
$lang['DIARRHEA_DIAGNOSIS_BULLET7'] ="Occurrence of blood in stool
";
$lang['DIARRHEA_DIAGNOSIS_BULLET8'] ="Occurrence of Fever
";
$lang['DIARRHEA_DIAGNOSIS_BULLET9'] ="Recent travels
";
$lang['DIARRHEA_DIAGNOSIS_BULLET10'] ="Current Medication, especially antibiotics
";
$lang['DIARRHEA_DIAGNOSIS_BULLET11'] ="Infectious persons in your surrounding
";
$lang['DIARRHEA_DIAGNOSIS_BULLET12'] ="Recent hospital stay
";


$lang['DIARRHEA_TREATMENT_H2'] ="Treatment of Diarrhea";
$lang['DIARRHEA_TREATMENT_TEXT'] ="Constant diarrhea can quickly reduce the body’s supply of water and electrolytes. It is important to replace the lost fluids. In most cases home remedies for diarrhea can help to ease your symptoms, but sometimes medication is needed for support. Get a quick diagnosis on your diarrhea symptoms by talking to one of our qualified doctors today. Our doctors will alleviate your worries and provide you with informations such as diet for diarrhea and diarrhea cures." ;


$lang['DIARRHEA_ASK_H2'] ="Ask a doctor about your Diarrhea today!";
$lang['DIARRHEA_ASK_TEXT'] ="Medlanes is fast & easy to use. Get your diagnosis within minutes in just three simple steps:";
$lang['DIARRHEA_ASK_BULLET1'] ="Describe your symptoms & ask your questions";
$lang['DIARRHEA_ASK_BULLET2'] ="Choose your doctor & pay";
$lang['DIARRHEA_ASK_BULLET3'] ="Get qualified medical advice";

$lang['DIARRHEA_CONTENT_H2'] ="Table of Contents";
$lang['DIARRHEA_CONTENT1'] ="Description of Diarrhea";
$lang['DIARRHEA_CONTENT2'] ="Signs &amp; Symptoms of Diarrhea";
$lang['DIARRHEA_CONTENT3'] ="Causes of Diarrhea";
$lang['DIARRHEA_CONTENT4'] ="Diagnosis of Diarrhea";
$lang['DIARRHEA_CONTENT5'] ="Treatment of Diarrhea";

$lang['DIARRHEA_ASK2_H2'] ="Ask a Doctor about Diarrhea";
$lang['DIARRHEA_ASK2_TEXT'] ="The information on this page is no substitute for consulting with a doctor. </br> If you experience symptoms of Diarrhea we offer easy, convenient and fast access to general practitioners who can give you qualified medical advice for your case.";

$lang['DIARRHEA_DOCTOR_NAME1'] ="Dr. Thomas P.";
$lang['DIARRHEA_DOCTOR_NAME2'] ="Dr. Eileen B.S.";
$lang['DIARRHEA_DOCTOR_NAME3'] ="Dr. James M.";
$lang['DIARRHEA_DOCTOR_NAME4'] ="Dr. Sandy K.";
$lang['DIARRHEA_DOCTOR_NAME5'] ="Dr. Gary J.R.";
$lang['DIARRHEA_DOCTOR_NAME6'] ="Dr. Theodor M.";


$lang['DIARRHEA_SIDEBAR_BUTTON'] ="Ask A Doctor Now For Advice";





// Condition Fatigue--------------------
$lang['FATIGUE_ICD_CODE'] = '346';

$lang['FATIGUE_TITLE'] = 'Fatigue and Weakness';
$lang['FATIGUE_DESCRIPTION'] = 'Find information about fatigue and weakness including its causes, symptoms, and the treatment options that are available.';	
$lang['FATIGUE_OG_TITLE'] ="Fatigue and Weakness";
$lang['FATIGUE_OG_URL'] ="http://medlanes.com/FatigueandWeakness";    
$lang['FATIGUE_OG_DESCRIPTION'] = 'Find information about fatigue and weakness including its causes, symptoms, and the treatment options that are available.';
$lang['FATIGUE_TWITTER_TITLE'] ="Fatigue and Weakness";	
$lang['FATIGUE_TWITTER_URL'] ="http://medlanes.com/Fatigue and Weakness";  
$lang['FATIGUE_TWITTER_DESCRIPTION'] = 'Find information about fatigue and weakness including its causes, symptoms, and the treatment options that are available.';                    

$lang['FATIGUE_HEADLINE'] ="Fatigue and Weakness";
$lang['FATIGUE_SLIDER'] ="<span itemprop=\"description\" itemscope itemtype=\"http://schema.org/MedicalCondition\">“Both fatigue and weakness are symptoms, not diseases. Because these symptoms can be caused by other health problems, it is necessary to find the root cause of the symptoms.
 ”</span>";

$lang['FATIGUE_DESCRIPTION_H2'] ="Description of Fatigue and Weakness";
$lang['FATIGUE_DESCRIPTION_TEXT'] ="<span itemprop=\"description\" itemscope itemtype=\"http://schema.org/MedicalFATIGUE\"> Weakness and fatigue are terms that are often used to describe the same thing, but in reality, they are describing two different sensations. It is extremely important to identify the difference between being “fatigued” and “weak”.
<br><br>
Fatigue - Is a feeling of tiredness or exhaustion or the insatiable need to rest because of lack of energy or strength. Fatigue may result from overwork, poor sleeping patterns, worry, boredom, or lack of exercise. Fatigue is also a symptom that maybe be caused by illness, medication, anxiety or depression.
<br><br>
Weakness - Is a lack of physical or muscle strength and the feeling that extra effort is required to move your arms, legs, or other muscle groups. 
";
$lang['FATIGUE_DESCRIPTION_BULLET1'] ="<span itemprop=\"subtype\" itemscope itemtype=\"http://schema.org/MedicalCondition\"><span itemprop=\"name\"><b>Osmotic diarrhea.</b> Water is drawn from the body into the bowel.
";
$lang['FATIGUE_DESCRIPTION_BULLET2'] ="<span itemprop=\"subtype\" itemscope itemtype=\"http://schema.org/MedicalCondition\"><span itemprop=\"name\"><b>Secretory diarrhea.</b> Increase of the active secretion or inhibition of absorption.</span></span>";
$lang['FATIGUE_DESCRIPTION_BULLET3'] ="<span itemprop=\"subtype\" itemscope itemtype=\"http://schema.org/MedicalCondition\"><span itemprop=\"name\"><b>Exudative diarrhea.</b> Presence of blood and pus in the stool.</span></span>";



$lang['FATIGUE_SIGNS_H2'] ="Signs &amp; Symptoms of Fatigue and Weakness";
$lang['FATIGUE_SIGNS_TEXT'] ="Signs and symptoms accompanied with diarrhea may include:
";

$lang['FATIGUE_SIGNS_H3_1_TEXT2'] ="If you have persistent diarrhea your doctor may perform further testing as physical exams, blood and stool tests. Your doctor might also refer you to a gastroenterologist, a doctor who is specialized in the digestive system. ";
$lang['FATIGUE_SIGNS_H3_1_TEXT'] ="More serious symptoms that require immediate treatment include:

 ";
$lang['FATIGUE_SIGNS_H3_1_BULLET1'] ="Increased bowel movements";
$lang['FATIGUE_SIGNS_H3_1_BULLET2'] ="Urgency of bowel movements


";
$lang['FATIGUE_SIGNS_H3_1_BULLET3'] ="Incontinence


";
$lang['FATIGUE_SIGNS_H3_1_BULLET4'] ="Rectal pain

";
$lang['FATIGUE_SIGNS_H3_1_BULLET5'] ="Abdominal cramps
";
$lang['FATIGUE_SIGNS_H3_1_BULLET6'] ="Abdominal pain
";
$lang['FATIGUE_SIGNS_H3_1_BULLET7'] ="Fever

";
$lang['FATIGUE_SIGNS_H3_1_BULLET8'] ="Bloating
";
$lang['FATIGUE_SIGNS_H3_1_BULLET9'] ="Nausea
";
$lang['FATIGUE_SIGNS_H3_1_BULLET10'] ="Vomiting
";
$lang['FATIGUE_SIGNS_H3_1_BULLET11'] ="Dehydration
";



$lang['FATIGUE_CAUSES_H2'] ="Causes of Fatigue and Weakness";
$lang['FATIGUE_CAUSES_TEXT'] ="Weakness - Weakness occurs from over activity, such as a strenuous workout. You may feel weak, sore, and figured but these sensations typically dissipate over the course of a few days. In some cases, muscle weakness may be caused by a health problem such as:
";
$lang['FATIGUE_CAUSES_TEXT2'] ="If your muscle weakness is slowly, yet progressively getting worse, you must see your doctor. Sudden muscle weakness and loss of functions in areas of the body can be an indicator of a serious problem such as a stroke or transient ischemic attack, and must be addressed on an emergency basis.

";
$lang['FATIGUE_CAUSES_TEXT3'] ="Fatigue - Is the feeling of tiredness, exhaustion, or lack of energy. The most common causes of fatigue are:


";

$lang['FATIGUE_CAUSES_TEXT4'] ="Fatigue that lasts longer than 2 weeks requires a visit to your doctor. Fatigue lasting this amount of time can be a indicator to a much more serious health problem such as:


";

$lang['FATIGUE_CAUSES_TEXT5'] ="Chronic Fatigue Syndrome is an uncommon cause of severe and persistent fatigue.


";

$lang['FATIGUE_CAUSES_BULLET1'] ="Hypothyroidism
";
$lang['FATIGUE_CAUSES_BULLET2'] ="Hyperthyroidism
";
$lang['FATIGUE_CAUSES_BULLET3'] ="Guillain-Barre Syndrome - A nerve disorder that causes weakness in the legs, arms and other muscles";
$lang['FATIGUE_CAUSES_BULLET4'] ="Electrolyte Imbalance";
$lang['FATIGUE_CAUSES_BULLET5'] ="Overwork";
$lang['FATIGUE_CAUSES_BULLET6'] ="Lack of Sleep or Poor Sleep Quality";
$lang['FATIGUE_CAUSES_BULLET7'] ="Worry or Anxiety";
$lang['FATIGUE_CAUSES_BULLET8'] ="Surgery. Abdominal surgeries or gall bladder removal surgery can lead to an infection.";
$lang['FATIGUE_CAUSES_BULLET9'] ="Boredom";
$lang['FATIGUE_CAUSES_BULLET10'] ="Illnesses ";
$lang['FATIGUE_CAUSES_BULLET10'] ="Problems with the Heart ";
$lang['FATIGUE_CAUSES_BULLET10'] ="Metabolic Disorders such as Diabetes ";
$lang['FATIGUE_CAUSES_BULLET10'] ="Thyroid Gland Problems ";
$lang['FATIGUE_CAUSES_BULLET10'] ="Kidney or Liver Disease ";






$lang['FATIGUE_DIAGNOSIS_H2'] ="Diagnosis of Fatigue and Weakness";
$lang['FATIGUE_DIAGNOSIS_TEXT'] ="Diagnosing weakness and/or fatigue is difficult. The overall goal when evaluating a patient is to rule out any treatable condition. A physician will go over your medical history, and evaluate the qualities of the fatigue/weakness. The affected person may be able to identify patterns, sleep habits, emotional state, and other factors that will assist the doctor with their diagnosis.

";
$lang['FATIGUE_DIAGNOSIS_BULLET1'] ="Initial phase of symptoms
";
$lang['FATIGUE_DIAGNOSIS_BULLET2'] ="Occurrence of symptoms
";
$lang['FATIGUE_DIAGNOSIS_BULLET3'] ="Severity of symptoms
";
$lang['FATIGUE_DIAGNOSIS_BULLET4'] ="Improving aspects of symptoms
";
$lang['FATIGUE_DIAGNOSIS_BULLET5'] ="Worsening aspects of symptoms
";
$lang['FATIGUE_DIAGNOSIS_BULLET6'] ="Sleep interruption caused by diarrhea
";
$lang['FATIGUE_DIAGNOSIS_BULLET7'] ="Occurrence of blood in stool
";
$lang['FATIGUE_DIAGNOSIS_BULLET8'] ="Occurrence of Fever
";
$lang['FATIGUE_DIAGNOSIS_BULLET9'] ="Recent travels
";
$lang['FATIGUE_DIAGNOSIS_BULLET10'] ="Current Medication, especially antibiotics
";
$lang['FATIGUE_DIAGNOSIS_BULLET11'] ="Infectious persons in your surrounding
";
$lang['FATIGUE_DIAGNOSIS_BULLET12'] ="Recent hospital stay
";


$lang['FATIGUE_TREATMENT_H2'] ="Treatment of Fatigue and Weakness";
$lang['FATIGUE_TREATMENT_TEXT'] ="Until your doctor has determined the root cause of your fatigue and/or weakness, it is impossible to treat. It is imperative that you seek answers from your physician immediately. Here at Medlanes, we have help tens of thousands of people just like you overcome their issues with fatigue and weakness, and we would be proud to help you too!

" ;


$lang['FATIGUE_ASK_H2'] ="Ask a doctor about your Fatigue today!";
$lang['FATIGUE_ASK_TEXT'] ="Medlanes is fast & easy to use. Get your diagnosis within minutes in just three simple steps:";
$lang['FATIGUE_ASK_BULLET1'] ="Describe your symptoms & ask your questions";
$lang['FATIGUE_ASK_BULLET2'] ="Choose your doctor & pay";
$lang['FATIGUE_ASK_BULLET3'] ="Get qualified medical advice";

$lang['FATIGUE_CONTENT_H2'] ="Table of Contents";
$lang['FATIGUE_CONTENT1'] ="Description of Fatigue and Weakness";
$lang['FATIGUE_CONTENT2'] ="Signs &amp; Symptoms of Fatigue and Weakness";
$lang['FATIGUE_CONTENT3'] ="Causes of Fatigue and Weakness";
$lang['FATIGUE_CONTENT4'] ="Diagnosis of Fatigue and Weakness";
$lang['FATIGUE_CONTENT5'] ="Treatment of Fatigue and Weakness";

$lang['FATIGUE_ASK2_H2'] ="Ask a Doctor about Fatigue";
$lang['FATIGUE_ASK2_TEXT'] ="The information on this page is no substitute for consulting with a doctor. </br> If you experience symptoms of Fatigue and Weakness we offer easy, convenient and fast access to general practitioners who can give you qualified medical advice for your case.";

$lang['FATIGUE_DOCTOR_NAME1'] ="Dr. Thomas P.";
$lang['FATIGUE_DOCTOR_NAME2'] ="Dr. Eileen B.S.";
$lang['FATIGUE_DOCTOR_NAME3'] ="Dr. James M.";
$lang['FATIGUE_DOCTOR_NAME4'] ="Dr. Sandy K.";
$lang['FATIGUE_DOCTOR_NAME5'] ="Dr. Gary J.R.";
$lang['FATIGUE_DOCTOR_NAME6'] ="Dr. Theodor M.";


$lang['FATIGUE_SIDEBAR_BUTTON'] ="Ask A Doctor Now For Advice";




// Condition Congestion--------------------
$lang['CONGESTION_ICD_CODE'] = '346';

$lang['CONGESTION_TITLE'] = 'Congestion';
$lang['CONGESTION_DESCRIPTION'] = 'Find information about congestion including causes,  types of congestion, and the treatment options that are available.';	
$lang['CONGESTION_OG_TITLE'] ="Congestion";
$lang['CONGESTION_OG_URL'] ="http://medlanes.com/Congestion";    
$lang['CONGESTION_OG_DESCRIPTION'] = 'Find information about congestion including causes,  types of congestion, and the treatment options that are available..';
$lang['CONGESTION_TWITTER_TITLE'] ="Congestion";	
$lang['CONGESTION_TWITTER_URL'] ="http://medlanes.com/Congestion";  
$lang['CONGESTION_TWITTER_DESCRIPTION'] = 'Find information about congestion including causes,  types of congestion, and the treatment options that are available..';                    

$lang['CONGESTION_HEADLINE'] ="Congestion";
$lang['CONGESTION_SLIDER'] ="<span itemprop=\"description\" itemscope itemtype=\"http://schema.org/MedicalCondition\">“Congestion is characterized as a blockage or obstruction of an affected area ”<br><br>  <span style='font-size:16px ;float:right !important; margin-right: 5px;'>All information is validated by expert doctors.</span></span> </span>";

$lang['CONGESTION_DESCRIPTION_H2'] ="Description of Congestion";
$lang['CONGESTION_DESCRIPTION_TEXT'] ="<span itemprop=\"description\" itemscope itemtype=\"http://schema.org/MedicalCONGESTION\"> Congestion is characterized as a blockage or obstruction of an affected area and typically comes in the following forms:
";
$lang['CONGESTION_DESCRIPTION_BULLET1'] ="<span itemprop=\"subtype\" itemscope itemtype=\"http://schema.org/MedicalCondition\"><span itemprop=\"name\">Chest Congestion";
$lang['CONGESTION_DESCRIPTION_BULLET2'] ="<span itemprop=\"subtype\" itemscope itemtype=\"http://schema.org/MedicalCondition\"><span itemprop=\"name\">Nasal/Sinus Congestion</span></span>";
$lang['CONGESTION_DESCRIPTION_BULLET3'] ="<span itemprop=\"subtype\" itemscope itemtype=\"http://schema.org/MedicalCondition\"><span itemprop=\"name\">Ear Congestion</span></span>";




$lang['CONGESTION_SIGNS_H2'] ="Signs &amp; Symptoms of Congestion";
$lang['CONGESTION_SIGNS_TEXT'] ="Chest Congestion  - Common symptoms of chest congestion vary on the cause of the congestion, but may include:
";

$lang['CONGESTION_SIGNS_H3_1_TEXT2'] ="Ear Congestion - The symptoms of Ear Congestion may include:
";
$lang['CONGESTION_SIGNS_H3_1_TEXT'] ="Nasal/Sinus Congestion -  Nasal congestion symptoms vary, but may include :
 ";
$lang['CONGESTION_SIGNS_H3_1_BULLET1'] ="Shortness of Breath";
$lang['CONGESTION_SIGNS_H3_1_BULLET2'] ="Chest Pain and Tightness

";
$lang['CONGESTION_SIGNS_H3_1_BULLET3'] ="Coughing


";
$lang['CONGESTION_SIGNS_H3_1_BULLET4'] ="Wheezing
";
$lang['CONGESTION_SIGNS_H3_1_BULLET5'] ="Coughing up of Phlegm or Blood";
$lang['CONGESTION_SIGNS_H3_1_BULLET6'] ="Leaking of Yellow or clear Fluid or Pus
";
$lang['CONGESTION_SIGNS_H3_1_BULLET7'] ="Obstruction of Nasal Passages
";
$lang['CONGESTION_SIGNS_H3_1_BULLET8'] ="Difficulty Breathing
";
$lang['CONGESTION_SIGNS_H3_1_BULLET9'] ="Soreness under the Eyes
";
$lang['CONGESTION_SIGNS_H3_1_BULLET10'] ="Drainage of Clear, Yellow, or Green mucus
";
$lang['CONGESTION_SIGNS_H3_1_BULLET11'] ="Presence of Blood in Mucus
";
$lang['CONGESTION_SIGNS_H3_1_BULLET12'] ="Pressure in inner ear or Jaw
";

$lang['CONGESTION_SIGNS_H3_1_BULLET13'] ="Discharge from the affected Ear

";

$lang['CONGESTION_SIGNS_H3_1_BULLET14'] ="Soreness or Pain

";
$lang['CONGESTION_SIGNS_H3_1_BULLET15'] ="Tinnitus (Ringing of the Ear)

";
$lang['CONGESTION_SIGNS_H3_1_BULLET16'] ="Balance and Coordination Problems

";

$lang['CONGESTION_CAUSES_H2'] ="Causes of Congestion";
$lang['CONGESTION_CAUSES_TEXT'] ="Chest Congestion  - Is caused by the presence of mucus in the trachea and in the respiratory tracts. This is typically the response to an infection or foreign particles. There are many potential causes for chest congestion including:
";
$lang['CONGESTION_CAUSES_TEXT2'] ="Nasal/Sinus Congestion - Is the blockage of the nasal passages due to membranes lining the nose becoming swollen from inflamed blood vessels. Nasal congestion can be an annoyance or a life threatening condition, so it is important that you understand the causes. These include:
";
$lang['CONGESTION_CAUSES_TEXT3'] ="Ear Congestion - Ear congestion is usually caused by a blockage of the Eustachian tube. The Eustachian tube connects the middle ear cavity with the oral cavity and helps in equalizing the air pressure in the middle ear. Air travel also causes ear congestion, as the changes in the atmospheric pressure affect the Eustachian tube as well. Other causes of ear congestion include: ";

$lang['CONGESTION_CAUSES_BULLET1'] ="Respiratory Tract Infection";
$lang['CONGESTION_CAUSES_BULLET2'] ="Fine Particles like Dust or Pollen";
$lang['CONGESTION_CAUSES_BULLET3'] ="Pneumonia";
$lang['CONGESTION_CAUSES_BULLET4'] ="COPD";
$lang['CONGESTION_CAUSES_BULLET5'] ="Asthma";
$lang['CONGESTION_CAUSES_BULLET6'] ="Allergies";
$lang['CONGESTION_CAUSES_BULLET7'] ="Tuberculosis (TB)";
$lang['CONGESTION_CAUSES_BULLET8'] ="Allergic Reaction";
$lang['CONGESTION_CAUSES_BULLET9'] ="Common Cold or Flu";
$lang['CONGESTION_CAUSES_BULLET10'] ="Hay Fever";
$lang['CONGESTION_CAUSES_BULLET11'] ="Nasal Polyps";
$lang['CONGESTION_CAUSES_BULLET12'] ="Rhinitis";
$lang['CONGESTION_CAUSES_BULLET13'] ="Sinusitis";
$lang['CONGESTION_CAUSES_BULLET14'] ="Fungal Infection of the External Ear";
$lang['CONGESTION_CAUSES_BULLET15'] ="Otitis Media (Ear Infection)";
$lang['CONGESTION_CAUSES_BULLET16'] ="Temporomandibular Joint Disease (TMJ)";




$lang['CONGESTION_DIAGNOSIS_H2'] ="Diagnosis of Congestion";
$lang['CONGESTION_DIAGNOSIS_TEXT'] ="A simple evaluation is typically all your doctor needs to determine the cause of your congestion. Additional tests may be necessary at the discretion of your doctor.
";


$lang['CONGESTION_TREATMENT_H2'] ="Treatment of Congestion";
$lang['CONGESTION_TREATMENT_TEXT'] ="Due to the wide variety of causes of congestion, it is important to seek care from your physician if congestion persists for four days or more to rule out any seriously underlying causes. Determining the root cause for your congestion will dictate which treatment options are available to you. The doctors at Medlanes have successfully diagnosed and treated over 20 different types of congestion, all from the patients home. 

" ;


$lang['CONGESTION_ASK_H2'] ="Ask a doctor about your Congestion today!";
$lang['CONGESTION_ASK_TEXT'] ="Medlanes is fast & easy to use. Get your diagnosis within minutes in just three simple steps:";
$lang['CONGESTION_ASK_BULLET1'] ="Describe your symptoms & ask your questions";
$lang['CONGESTION_ASK_BULLET2'] ="Choose your doctor & pay";
$lang['CONGESTION_ASK_BULLET3'] ="Get qualified medical advice";

$lang['CONGESTION_CONTENT_H2'] ="Table of Contents";
$lang['CONGESTION_CONTENT1'] ="Description";
$lang['CONGESTION_CONTENT2'] ="Signs &amp; Symptoms";
$lang['CONGESTION_CONTENT3'] ="Causes";
$lang['CONGESTION_CONTENT4'] ="Diagnosis";
$lang['CONGESTION_CONTENT5'] ="Treatment";

$lang['CONGESTION_ASK2_H2'] ="Ask a Doctor about Congestion";
$lang['CONGESTION_ASK2_TEXT'] ="The information on this page is no substitute for consulting with a doctor. </br> If you experience symptoms of Congestion we offer easy, convenient and fast access to general practitioners who can give you qualified medical advice for your case.";

$lang['CONGESTION_DOCTOR_NAME1'] ="Dr. Thomas P.";
$lang['CONGESTION_DOCTOR_NAME2'] ="Dr. Eileen B.S.";
$lang['CONGESTION_DOCTOR_NAME3'] ="Dr. James M.";
$lang['CONGESTION_DOCTOR_NAME4'] ="Dr. Sandy K.";
$lang['CONGESTION_DOCTOR_NAME5'] ="Dr. Gary J.R.";
$lang['CONGESTION_DOCTOR_NAME6'] ="Dr. Theodor M.";


$lang['CONGESTION_SIDEBAR_BUTTON'] ="Ask A Doctor Now For Advice";




// Condition Cough --------------------
$lang['COUGH_ICD_CODE'] = '346';

$lang['COUGH_TITLE'] = 'Cough';
$lang['COUGH_DESCRIPTION'] = 'Ask our doctors and describe your symptoms.';	
$lang['COUGH_OG_TITLE'] ="Cough";
$lang['COUGH_OG_URL'] ="http://medlanes.com/Cough";    
$lang['COUGH_OG_DESCRIPTION'] = 'Ask our doctors and describe your symptoms.';
$lang['COUGH_TWITTER_TITLE'] ="Cough";	
$lang['COUGH_TWITTER_URL'] ="http://medlanes.com/Cough";  
$lang['COUGH_TWITTER_DESCRIPTION'] = 'Ask our doctors and describe your symptoms.';                    

$lang['COUGH_HEADLINE'] ="Cough";
$lang['COUGH_SLIDER'] ="<span itemprop=\"description\" itemscope itemtype=\"http://schema.org/MedicalCondition\">“Coughing is your body’s way of removing foreign material or mucus from the lungs and the upper airway passages, or the body’s reaction to an irritation or foreign body. ”</span>";

$lang['COUGH_DESCRIPTION_H2'] ="Description of Cough";
$lang['COUGH_DESCRIPTION_TEXT'] ="<span itemprop=\"description\" itemscope itemtype=\"http://schema.org/MedicalCOUGH\"> Coughing is your body’s way of removing foreign material or mucus from the lungs and the upper airway passages, or the body’s reaction to an irritation or foreign body. A cough is only a symptom, not a disease, thus it is imperative that you understand the cause of your cough.
";





$lang['COUGH_CAUSES_H2'] ="Causes of Cough";
$lang['COUGH_CAUSES_TEXT'] ="Coughs are categorized as either Productive or Nonproductive.

";
$lang['COUGH_CAUSES_TEXT2'] ="Productive Cough - A productive cough produces mucus. This is the body's natural way of clearing mucus from the lungs. There are many causes of a productive cough, such as:
";
$lang['COUGH_CAUSES_TEXT3'] ="Non-Productive Cough - A nonproductive cough is dry and does not produce mucus. A dry, hacking cough may develop at the end of a cold, or by exposure to an irritant such as smoke, dust, and household cleaning chemicals. There are many causes to a nonproductive cough such as:
";

$lang['COUGH_CAUSES_BULLET1'] ="Viral Illnesses
";
$lang['COUGH_CAUSES_BULLET2'] ="Infections such as a Lung Infection or Upper Respiratory Infection";
$lang['COUGH_CAUSES_BULLET3'] ="Chronic Lung Disease";
$lang['COUGH_CAUSES_BULLET4'] ="Chronic Obstructive Pulmonary Disease";
$lang['COUGH_CAUSES_BULLET5'] ="Stomach acid backing up into the esophagus. This is caused by  Gastroesophageal reflux disease (GERD)";
$lang['COUGH_CAUSES_BULLET6'] ="Postnasal Drip";
$lang['COUGH_CAUSES_BULLET7'] ="Smoking or other Tobacco use";
$lang['COUGH_CAUSES_BULLET8'] ="Viral Illnesses";
$lang['COUGH_CAUSES_BULLET9'] ="Bronchospasm";
$lang['COUGH_CAUSES_BULLET10'] ="Allergies";
$lang['COUGH_CAUSES_BULLET11'] ="Certain medications, specifically ACE Inhibitors which are used to control high blood pressure.";
$lang['COUGH_CAUSES_BULLET12'] ="Asthma";
$lang['COUGH_CAUSES_BULLET13'] ="Blockage ";
$lang['COUGH_CAUSES_BULLET14'] ="Postnasal Drip
";
$lang['COUGH_CAUSES_BULLET15'] ="Smoking or other Tobacco use";




$lang['COUGH_DIAGNOSIS_H2'] ="Diagnosis of Cough";
$lang['COUGH_DIAGNOSIS_TEXT'] ="If a persistent cough is present for more than 5 days, it is important to consult your doctor to discover the root cause of your cough. Many conditions that cause a cough require additional treatments. A cough can also be a precursor to a more complex problem, thus its a necessity that persistent coughs are evaluated.
";


$lang['COUGH_TREATMENT_H2'] ="Cough Treatments and Remedies";
$lang['COUGH_TREATMENT_TEXT'] ="There are many different treatment options and home remedies for cough available, but it is extremely important that you understand why type of cough you have as some forms of cough do not require treatment, and are the body’s natural way of expelling unwanted fluids. A quick online consultation with a Medlanes doctor can provide you with a wealth of knowledge about your cough, and provide you with customized treatment options to get you on your road to recovery! 

" ;


$lang['COUGH_ASK_H2'] ="Ask a doctor about your Cough today!";
$lang['COUGH_ASK_TEXT'] ="Medlanes is fast & easy to use. Get your diagnosis within minutes in just three simple steps:";
$lang['COUGH_ASK_BULLET1'] ="Describe your symptoms & ask your questions";
$lang['COUGH_ASK_BULLET2'] ="Choose your doctor & pay";
$lang['COUGH_ASK_BULLET3'] ="Get qualified medical advice";

$lang['COUGH_CONTENT_H2'] ="Table of Contents";
$lang['COUGH_CONTENT1'] ="Description of Cough";
$lang['COUGH_CONTENT2'] ="Signs &amp; Symptoms of Cough";
$lang['COUGH_CONTENT3'] ="Causes of Cough";
$lang['COUGH_CONTENT4'] ="Diagnosis of Cough";
$lang['COUGH_CONTENT5'] ="Treatment of Cough";

$lang['COUGH_ASK2_H2'] ="Ask a Doctor about Cough";
$lang['COUGH_ASK2_TEXT'] ="The information on this page is no substitute for consulting with a doctor. </br> If you experience symptoms of Cough we offer easy, convenient and fast access to general practitioners who can give you qualified medical advice for your case.";

$lang['COUGH_DOCTOR_NAME1'] ="Dr. Thomas P.";
$lang['COUGH_DOCTOR_NAME2'] ="Dr. Eileen B.S.";
$lang['COUGH_DOCTOR_NAME3'] ="Dr. James M.";
$lang['COUGH_DOCTOR_NAME4'] ="Dr. Sandy K.";
$lang['COUGH_DOCTOR_NAME5'] ="Dr. Gary J.R.";
$lang['COUGH_DOCTOR_NAME6'] ="Dr. Theodor M.";


$lang['COUGH_SIDEBAR_BUTTON'] ="Ask A Doctor Now For Advice";



// Condition Fever--------------------
$lang['FEVER_ICD_CODE'] = '346';

$lang['FEVER_TITLE'] = 'Fever';
$lang['FEVER_DESCRIPTION'] = 'Ask our doctors and describe your symptoms.';	
$lang['FEVER_OG_TITLE'] ="Fever";
$lang['FEVER_OG_URL'] ="http://medlanes.com/Fever";    
$lang['FEVER_OG_DESCRIPTION'] = 'Ask our doctors and describe your symptoms.';
$lang['FEVER_TWITTER_TITLE'] ="Fever";	
$lang['FEVER_TWITTER_URL'] ="http://medlanes.com/Fever";  
$lang['FEVER_TWITTER_DESCRIPTION'] = 'Ask our doctors and describe your symptoms.';                    

$lang['FEVER_HEADLINE'] ="Fever";
$lang['FEVER_SLIDER'] ="<span itemprop=\"description\" itemscope itemtype=\"http://schema.org/MedicalCondition\">“Fever is one of the most common medical signs and is characterized by an elevation of body temperature about the normal range”<br><br>  <span style='font-size:16px ;float:right !important; margin-right: 5px;'>All information is validated by expert doctors.</span></span>";

$lang['FEVER_DESCRIPTION_H2'] ="Description of Fever";
$lang['FEVER_DESCRIPTION_TEXT'] ="<span itemprop=\"description\" itemscope itemtype=\"http://schema.org/MedicalFEVER\"> Also known as pyrexia or febrile response, a fever is one of the most common medical signs and is characterized by an elevation of body temperature about the normal range of 97.7 - 99.5 degrees fahrenheit. A fever is usually a symptom of an underlying condition. In some cases, a fever is classified as an illness. Examples of these are:
";
$lang['FEVER_DESCRIPTION_BULLET1'] ="<span itemprop=\"subtype\" itemscope itemtype=\"http://schema.org/MedicalCondition\"><span itemprop=\"name\">Scarlet Fever
";
$lang['FEVER_DESCRIPTION_BULLET2'] ="<span itemprop=\"subtype\" itemscope itemtype=\"http://schema.org/MedicalCondition\"><span itemprop=\"name\">Dengue Fever";
$lang['FEVER_DESCRIPTION_BULLET3'] ="<span itemprop=\"subtype\" itemscope itemtype=\"http://schema.org/MedicalCondition\"><span itemprop=\"name\">Yellow Fever</span></span>";
$lang['FEVER_DESCRIPTION_BULLET4'] ="<span itemprop=\"subtype\" itemscope itemtype=\"http://schema.org/MedicalCondition\"><span itemprop=\"name\">Hay Fever
</span></span>";
$lang['FEVER_DESCRIPTION_BULLET5'] ="<span itemprop=\"subtype\" itemscope itemtype=\"http://schema.org/MedicalCondition\"><span itemprop=\"name\">Rheumatic Fever</span></span>";
$lang['FEVER_DESCRIPTION_BULLET6'] ="<span itemprop=\"subtype\" itemscope itemtype=\"http://schema.org/MedicalCondition\"><span itemprop=\"name\">Valley Fever</span></span>";
$lang['FEVER_DESCRIPTION_BULLET7'] ="<span itemprop=\"subtype\" itemscope itemtype=\"http://schema.org/MedicalCondition\"><span itemprop=\"name\">Typhoid Fever</span></span>";
$lang['FEVER_DESCRIPTION_BULLET8'] ="<span itemprop=\"subtype\" itemscope itemtype=\"http://schema.org/MedicalCondition\"><span itemprop=\"name\">Glandular Fever</span></span>";






$lang['FEVER_SIGNS_H2'] ="Signs &amp; Symptoms of Fever";
$lang['FEVER_SIGNS_TEXT'] ="In itself, a fever is a symptom of an underlying problem. There are still common signs of fever which include:
";

$lang['FEVER_SIGNS_H3_1_TEXT2'] ="Ear Fever - The symptoms of Ear Fever may include:
";
$lang['FEVER_SIGNS_H3_1_TEXT'] ="Nasal/Sinus Fever -  Nasal congestion symptoms vary, but may include :
 ";
$lang['FEVER_SIGNS_H3_1_BULLET1'] ="Sick Behavior
";
$lang['FEVER_SIGNS_H3_1_BULLET2'] ="General Malaise

";
$lang['FEVER_SIGNS_H3_1_BULLET3'] ="Lethargy


";
$lang['FEVER_SIGNS_H3_1_BULLET4'] ="Sensitive to Hot/Cold
";
$lang['FEVER_SIGNS_H3_1_BULLET5'] ="Chills";
$lang['FEVER_SIGNS_H3_1_BULLET6'] ="Vivid or Lucid Dreams
";
$lang['FEVER_SIGNS_H3_1_BULLET7'] ="Excessive Perspiration
";

$lang['FEVER_CAUSES_H2'] ="Causes of Fever";
$lang['FEVER_CAUSES_TEXT'] ="The elevation of body temperature is regulated by a part of the brain called the hypothalamus. In response to an infection, illness, or a different cause, the hypothalamus signals to your body to increase its base temperature. The most common causes of fever are infections such as cold and gastroenteritis. Other causes include:
";
$lang['FEVER_CAUSES_TEXT2'] ="Other causes of fever include: 

";
$lang['FEVER_CAUSES_TEXT3'] ="Ear Fever - Ear congestion is usually caused by a blockage of the Eustachian tube. The Eustachian tube connects the middle ear cavity with the oral cavity and helps in equalizing the air pressure in the middle ear. Air travel also causes ear congestion, as the changes in the atmospheric pressure affect the Eustachian tube as well. Other causes of ear congestion include: ";

$lang['FEVER_CAUSES_BULLET1'] ="Infection of the ear, lung, skin, throat, bladder, or kidney";
$lang['FEVER_CAUSES_BULLET2'] ="Conditions that cause an inflammatory response";
$lang['FEVER_CAUSES_BULLET3'] ="Side effects of medications";
$lang['FEVER_CAUSES_BULLET4'] ="Vaccines";
$lang['FEVER_CAUSES_BULLET5'] ="Cancer";
$lang['FEVER_CAUSES_BULLET6'] ="Blood Clots";
$lang['FEVER_CAUSES_BULLET7'] ="Autoimmune diseases";
$lang['FEVER_CAUSES_BULLET8'] ="Hormone disorders";




$lang['FEVER_DIAGNOSIS_H2'] ="Diagnosis of Fever";
$lang['FEVER_DIAGNOSIS_TEXT'] ="A fever is extremely easy to diagnose, but determining the cause of the fever can prove more difficult. Depending on your symptoms, medical history, and other factors, your doctor can chose to order additional diagnostic tests such as:
";

$lang['FEVER_DIAGNOSIS_BULLET1'] ="Blood Testing
";
$lang['FEVER_DIAGNOSIS_BULLET2'] ="Stool Samples
";
$lang['FEVER_DIAGNOSIS_BULLET3'] ="Urinalysis
";


$lang['FEVER_TREATMENT_H2'] ="Treatment of Fever";
$lang['FEVER_TREATMENT_TEXT'] ="Fevers can be managed using drugs like aspirin, or ibuprofen. With any fever, it is important to find the root cause, not just treat the symptom as the underlying cause could be something very serious. Ask your Medlanes physician today to get options and advice about fevers, fever treatments, and fever remedies.


" ;


$lang['FEVER_ASK_H2'] ="Ask a doctor about your Fever today!";
$lang['FEVER_ASK_TEXT'] ="Medlanes is fast & easy to use. Get your diagnosis within minutes in just three simple steps:";
$lang['FEVER_ASK_BULLET1'] ="Describe your symptoms & ask your questions";
$lang['FEVER_ASK_BULLET2'] ="Choose your doctor & pay";
$lang['FEVER_ASK_BULLET3'] ="Get qualified medical advice";

$lang['FEVER_CONTENT_H2'] ="Table of Contents";
$lang['FEVER_CONTENT1'] ="Description";
$lang['FEVER_CONTENT2'] ="Signs &amp; Symptoms";
$lang['FEVER_CONTENT3'] ="Causes";
$lang['FEVER_CONTENT4'] ="Diagnosis ";
$lang['FEVER_CONTENT5'] ="Treatments";

$lang['FEVER_ASK2_H2'] ="Ask a Doctor about Fever";
$lang['FEVER_ASK2_TEXT'] ="The information on this page is no substitute for consulting with a doctor. </br> If you experience symptoms of Fever we offer easy, convenient and fast access to general practitioners who can give you qualified medical advice for your case.";

$lang['FEVER_DOCTOR_NAME1'] ="Dr. Thomas P.";
$lang['FEVER_DOCTOR_NAME2'] ="Dr. Eileen B.S.";
$lang['FEVER_DOCTOR_NAME3'] ="Dr. James M.";
$lang['FEVER_DOCTOR_NAME4'] ="Dr. Sandy K.";
$lang['FEVER_DOCTOR_NAME5'] ="Dr. Gary J.R.";
$lang['FEVER_DOCTOR_NAME6'] ="Dr. Theodor M.";


$lang['FEVER_SIDEBAR_BUTTON'] ="Ask A Doctor Now For Advice";





// Condition Headache--------------------
$lang['HEADACHE_ICD_CODE'] = '346';

$lang['HEADACHE_TITLE'] = 'Headache';
$lang['HEADACHE_DESCRIPTION'] = 'Ask our doctors and describe your symptoms.';	
$lang['HEADACHE_OG_TITLE'] ="Headache";
$lang['HEADACHE_OG_URL'] ="http://medlanes.com/Headache";    
$lang['HEADACHE_OG_DESCRIPTION'] = 'Ask our doctors and describe your symptoms.';
$lang['HEADACHE_TWITTER_TITLE'] ="Headache";	
$lang['HEADACHE_TWITTER_URL'] ="http://medlanes.com/Headache";  
$lang['HEADACHE_TWITTER_DESCRIPTION'] = 'Ask our doctors and describe your symptoms.';                    

$lang['HEADACHE_HEADLINE'] ="Headache";
$lang['HEADACHE_SLIDER'] ="<span itemprop=\"description\" itemscope itemtype=\"http://schema.org/MedicalCondition\">“Headache is characterized as a blockage or obstruction of an affected area ”</span>";

$lang['HEADACHE_DESCRIPTION_H2'] ="Description of Headache";
$lang['HEADACHE_DESCRIPTION_TEXT'] ="<span itemprop=\"description\" itemscope itemtype=\"http://schema.org/MedicalHEADACHE\"> Headache is characterized as a blockage or obstruction of an affected area and typically comes in the following forms:
";
$lang['HEADACHE_DESCRIPTION_BULLET1'] ="<span itemprop=\"subtype\" itemscope itemtype=\"http://schema.org/MedicalCondition\"><span itemprop=\"name\">Chest Headache";
$lang['HEADACHE_DESCRIPTION_BULLET2'] ="<span itemprop=\"subtype\" itemscope itemtype=\"http://schema.org/MedicalCondition\"><span itemprop=\"name\">Nasal/Sinus Headache</span></span>";
$lang['HEADACHE_DESCRIPTION_BULLET3'] ="<span itemprop=\"subtype\" itemscope itemtype=\"http://schema.org/MedicalCondition\"><span itemprop=\"name\">Ear Headache</span></span>";




$lang['HEADACHE_SIGNS_H2'] ="Signs &amp; Symptoms of Headache";
$lang['HEADACHE_SIGNS_TEXT'] ="Chest Headache  - Common symptoms of chest congestion vary on the cause of the congestion, but may include:
";

$lang['HEADACHE_SIGNS_H3_1_TEXT2'] ="Ear Headache - The symptoms of Ear Headache may include:
";
$lang['HEADACHE_SIGNS_H3_1_TEXT'] ="Nasal/Sinus Headache -  Nasal congestion symptoms vary, but may include :
 ";
$lang['HEADACHE_SIGNS_H3_1_BULLET1'] ="Shortness of Breath";
$lang['HEADACHE_SIGNS_H3_1_BULLET2'] ="Chest Pain and Tightness

";
$lang['HEADACHE_SIGNS_H3_1_BULLET3'] ="Coughing


";
$lang['HEADACHE_SIGNS_H3_1_BULLET4'] ="Wheezing
";
$lang['HEADACHE_SIGNS_H3_1_BULLET5'] ="Coughing up of Phlegm or Blood";
$lang['HEADACHE_SIGNS_H3_1_BULLET6'] ="Leaking of Yellow or clear Fluid or Pus
";
$lang['HEADACHE_SIGNS_H3_1_BULLET7'] ="Obstruction of Nasal Passages
";
$lang['HEADACHE_SIGNS_H3_1_BULLET8'] ="Difficulty Breathing
";
$lang['HEADACHE_SIGNS_H3_1_BULLET9'] ="Soreness under the Eyes
";
$lang['HEADACHE_SIGNS_H3_1_BULLET10'] ="Drainage of Clear, Yellow, or Green mucus
";
$lang['HEADACHE_SIGNS_H3_1_BULLET11'] ="Presence of Blood in Mucus
";
$lang['HEADACHE_SIGNS_H3_1_BULLET12'] ="Pressure in inner ear or Jaw
";

$lang['HEADACHE_SIGNS_H3_1_BULLET13'] ="Discharge from the affected Ear

";

$lang['HEADACHE_SIGNS_H3_1_BULLET14'] ="Soreness or Pain

";
$lang['HEADACHE_SIGNS_H3_1_BULLET15'] ="Tinnitus (Ringing of the Ear)

";
$lang['HEADACHE_SIGNS_H3_1_BULLET16'] ="Balance and Coordination Problems

";

$lang['HEADACHE_CAUSES_H2'] ="Causes of Headache";
$lang['HEADACHE_CAUSES_TEXT'] ="Chest Headache  - Is caused by the presence of mucus in the trachea and in the respiratory tracts. This is typically the response to an infection or foreign particles. There are many potential causes for chest congestion including:
";
$lang['HEADACHE_CAUSES_TEXT2'] ="Nasal/Sinus Headache - Is the blockage of the nasal passages due to membranes lining the nose becoming swollen from inflamed blood vessels. Nasal congestion can be an annoyance or a life threatening condition, so it is important that you understand the causes. These include:
";
$lang['HEADACHE_CAUSES_TEXT3'] ="Ear Headache - Ear congestion is usually caused by a blockage of the Eustachian tube. The Eustachian tube connects the middle ear cavity with the oral cavity and helps in equalizing the air pressure in the middle ear. Air travel also causes ear congestion, as the changes in the atmospheric pressure affect the Eustachian tube as well. Other causes of ear congestion include: ";

$lang['HEADACHE_CAUSES_BULLET1'] ="Respiratory Tract Infection";
$lang['HEADACHE_CAUSES_BULLET2'] ="Fine Particles like Dust or Pollen";
$lang['HEADACHE_CAUSES_BULLET3'] ="Pneumonia";
$lang['HEADACHE_CAUSES_BULLET4'] ="COPD";
$lang['HEADACHE_CAUSES_BULLET5'] ="Asthma";
$lang['HEADACHE_CAUSES_BULLET6'] ="Allergies";
$lang['HEADACHE_CAUSES_BULLET7'] ="Tuberculosis (TB)";
$lang['HEADACHE_CAUSES_BULLET8'] ="Allergic Reaction";
$lang['HEADACHE_CAUSES_BULLET9'] ="Common Cold or Flu";
$lang['HEADACHE_CAUSES_BULLET10'] ="Hay Fever";
$lang['HEADACHE_CAUSES_BULLET11'] ="Nasal Polyps";
$lang['HEADACHE_CAUSES_BULLET12'] ="Rhinitis";
$lang['HEADACHE_CAUSES_BULLET13'] ="Sinusitis";
$lang['HEADACHE_CAUSES_BULLET14'] ="Fungal Infection of the External Ear";
$lang['HEADACHE_CAUSES_BULLET15'] ="Otitis Media (Ear Infection)";
$lang['HEADACHE_CAUSES_BULLET16'] ="Temporomandibular Joint Disease (TMJ)";




$lang['HEADACHE_DIAGNOSIS_H2'] ="Diagnosis of Headache";
$lang['HEADACHE_DIAGNOSIS_TEXT'] ="A simple evaluation is typically all your doctor needs to determine the cause of your congestion. Additional tests may be necessary at the discretion of your doctor.
";


$lang['HEADACHE_TREATMENT_H2'] ="Treatment of Headache";
$lang['HEADACHE_TREATMENT_TEXT'] ="Due to the wide variety of causes of congestion, it is important to seek care from your physician if congestion persists for four days or more to rule out any seriously underlying causes. Determining the root cause for your congestion will dictate which treatment options are available to you. The doctors at Medlanes have successfully diagnosed and treated over 20 different types of congestion, all from the patients home. 

" ;


$lang['HEADACHE_ASK_H2'] ="Ask a doctor about your Headache today!";
$lang['HEADACHE_ASK_TEXT'] ="Medlanes is fast & easy to use. Get your diagnosis within minutes in just three simple steps:";
$lang['HEADACHE_ASK_BULLET1'] ="Describe your symptoms & ask your questions";
$lang['HEADACHE_ASK_BULLET2'] ="Choose your doctor & pay";
$lang['HEADACHE_ASK_BULLET3'] ="Get qualified medical advice";

$lang['HEADACHE_CONTENT_H2'] ="Table of Contents";
$lang['HEADACHE_CONTENT1'] ="Description of Headache";
$lang['HEADACHE_CONTENT2'] ="Signs &amp; Symptoms of Headache";
$lang['HEADACHE_CONTENT3'] ="Causes of Headache";
$lang['HEADACHE_CONTENT4'] ="Diagnosis of Headache";
$lang['HEADACHE_CONTENT5'] ="Treatment of Headache";

$lang['HEADACHE_ASK2_H2'] ="Ask a Doctor about Headache";
$lang['HEADACHE_ASK2_TEXT'] ="The information on this page is no substitute for consulting with a doctor. </br> If you experience symptoms of Headache we offer easy, convenient and fast access to general practitioners who can give you qualified medical advice for your case.";

$lang['HEADACHE_DOCTOR_NAME1'] ="Dr. Thomas P.";
$lang['HEADACHE_DOCTOR_NAME2'] ="Dr. Eileen B.S.";
$lang['HEADACHE_DOCTOR_NAME3'] ="Dr. James M.";
$lang['HEADACHE_DOCTOR_NAME4'] ="Dr. Sandy K.";
$lang['HEADACHE_DOCTOR_NAME5'] ="Dr. Gary J.R.";
$lang['HEADACHE_DOCTOR_NAME6'] ="Dr. Theodor M.";


$lang['HEADACHE_SIDEBAR_BUTTON'] ="Ask A Doctor Now For Advice";






// Condition Gout--------------------
$lang['GOUT_ICD_CODE'] = '346';

$lang['GOUT_TITLE'] = 'Gout';
$lang['GOUT_DESCRIPTION'] = 'Ask our doctors and describe your symptoms.';	
$lang['GOUT_OG_TITLE'] ="Gout";
$lang['GOUT_OG_URL'] ="http://medlanes.com/Gout";    
$lang['GOUT_OG_DESCRIPTION'] = 'Ask our doctors and describe your symptoms.';
$lang['GOUT_TWITTER_TITLE'] ="Gout";	
$lang['GOUT_TWITTER_URL'] ="http://medlanes.com/Gout";  
$lang['GOUT_TWITTER_DESCRIPTION'] = 'Ask our doctors and describe your symptoms.';                    

$lang['GOUT_HEADLINE'] ="Gout";
$lang['GOUT_SLIDER'] ="<span itemprop=\"description\" itemscope itemtype=\"http://schema.org/MedicalCondition\">“Gout is a type of arthritis that can cause attacks of sudden burning pain, stiffness, and swelling in a joint. ”</span>";

$lang['GOUT_DESCRIPTION_H2'] ="Description of Gout";
$lang['GOUT_DESCRIPTION_TEXT'] ="<span itemprop=\"description\" itemscope itemtype=\"http://schema.org/MedicalGOUT\">Gout is a type of arthritis that can cause attacks of sudden burning pain, stiffness, and swelling in a joint. Gout attacks, also referred to as flares, typically afflict the big toe. Gout will recur until properly treated. If untreated, gout can cause permanent damage to tendons, joints, and other tissues. Excess uric acid in the blood causes hard crystals to form in your joints, causing a Gout attack.
";





$lang['GOUT_SIGNS_H2'] ="Signs &amp; Symptoms of Gout";
$lang['GOUT_SIGNS_TEXT'] ="The most common sign of gout are nighttime attacks of swelling, tenderness, redness and sharp pains in the big toe. Attacks last can last anywhere between a few days, to several weeks. Gout does not strictly target the big toes, as it can flare up in any joint. Other signs and symptoms of gout include:
";

$lang['GOUT_SIGNS_H3_1_TEXT2'] ="Ear Gout - The symptoms of Ear Gout may include:
";
$lang['GOUT_SIGNS_H3_1_TEXT'] ="Nasal/Sinus Gout -  Nasal congestion symptoms vary, but may include :
 ";
$lang['GOUT_SIGNS_H3_1_BULLET1'] ="Warmth, pain, swelling, and extreme tenderness in a joint
";
$lang['GOUT_SIGNS_H3_1_BULLET2'] ="Limited movement or range of motion in the affected joint


";
$lang['GOUT_SIGNS_H3_1_BULLET3'] ="Peeling and itching of the skin around the affected joint

";
$lang['GOUT_SIGNS_H3_1_BULLET4'] ="Very red or slightly purple skin around the affected joint

";

$lang['GOUT_CAUSES_H2'] ="Causes of Gout";
$lang['GOUT_CAUSES_TEXT'] ="Gout is caused by too much uric acid in the blood (hyperuricemia). The exact cause of hyperuricemia is unknown, although genetics place a roll. Gout attacks and strike without a specific cause, but are typically caused be:
";
$lang['GOUT_CAUSES_TEXT2'] ="Nasal/Sinus Gout - Is the blockage of the nasal passages due to membranes lining the nose becoming swollen from inflamed blood vessels. Nasal congestion can be an annoyance or a life threatening condition, so it is important that you understand the causes. These include:
";
$lang['GOUT_CAUSES_TEXT3'] ="Ear Gout - Ear congestion is usually caused by a blockage of the Eustachian tube. The Eustachian tube connects the middle ear cavity with the oral cavity and helps in equalizing the air pressure in the middle ear. Air travel also causes ear congestion, as the changes in the atmospheric pressure affect the Eustachian tube as well. Other causes of ear congestion include: ";

$lang['GOUT_CAUSES_BULLET1'] ="Overweight and Obese
";
$lang['GOUT_CAUSES_BULLET2'] ="Eating a diet rich in seafood and red meat";
$lang['GOUT_CAUSES_BULLET3'] ="High Blood Pressure";
$lang['GOUT_CAUSES_BULLET4'] ="Some medicines including the frequent use of aspirin, niacin, or diuretics
";





$lang['GOUT_DIAGNOSIS_H2'] ="Diagnosis of Gout";
$lang['GOUT_DIAGNOSIS_TEXT'] ="Gout is typically diagnosed solely by physical examination of the affected site. Depending on the severity of the problem, your doctor may order additional tests which include:
";
$lang['GOUT_DIAGNOSIS_BULLET1'] ="A joint fluid analysis (arthrocentesis) to detect the presence of uric acid crystals. This is the only certain way to diagnose gout.

";
$lang['GOUT_DIAGNOSIS_BULLET2'] ="Blood tests to measure the amount of uric acid in the blood";
$lang['GOUT_DIAGNOSIS_BULLET3'] ="Urinalysis";
$lang['GOUT_DIAGNOSIS_BULLET4'] ="X-Rays in specific cases (especially late stages of the disease)

";



$lang['GOUT_TREATMENT_H2'] ="Treatment of Gout";
$lang['GOUT_TREATMENT_TEXT'] ="As there is no cure for gout, there are many options as far as treating the pain and preventing future gout attacks. Consult with a Medlanes physician today! We have help tens of thousands of people overcome gout, and make the necessary lifestyle changes to remain gout free. With multiple management options, we can treat both the symptoms of gout, as well as eliminating gout itself!

" ;


$lang['GOUT_ASK_H2'] ="Ask a doctor about your Gout today!";
$lang['GOUT_ASK_TEXT'] ="Medlanes is fast & easy to use. Get your diagnosis within minutes in just three simple steps:";
$lang['GOUT_ASK_BULLET1'] ="Describe your symptoms & ask your questions";
$lang['GOUT_ASK_BULLET2'] ="Choose your doctor & pay";
$lang['GOUT_ASK_BULLET3'] ="Get qualified medical advice";

$lang['GOUT_CONTENT_H2'] ="Table of Contents";
$lang['GOUT_CONTENT1'] ="Description of Gout";
$lang['GOUT_CONTENT2'] ="Signs &amp; Symptoms of Gout";
$lang['GOUT_CONTENT3'] ="Causes of Gout";
$lang['GOUT_CONTENT4'] ="Diagnosis of Gout";
$lang['GOUT_CONTENT5'] ="Treatment of Gout";

$lang['GOUT_ASK2_H2'] ="Ask a Doctor about Gout";
$lang['GOUT_ASK2_TEXT'] ="The information on this page is no substitute for consulting with a doctor. </br> If you experience symptoms of Gout we offer easy, convenient and fast access to general practitioners who can give you qualified medical advice for your case.";

$lang['GOUT_DOCTOR_NAME1'] ="Dr. Thomas P.";
$lang['GOUT_DOCTOR_NAME2'] ="Dr. Eileen B.S.";
$lang['GOUT_DOCTOR_NAME3'] ="Dr. James M.";
$lang['GOUT_DOCTOR_NAME4'] ="Dr. Sandy K.";
$lang['GOUT_DOCTOR_NAME5'] ="Dr. Gary J.R.";
$lang['GOUT_DOCTOR_NAME6'] ="Dr. Theodor M.";


$lang['GOUT_SIDEBAR_BUTTON'] ="Ask A Doctor Now For Advice";




// Condition Acid Reflux--------------------
$lang['ACID_ICD_CODE'] = '346';

$lang['ACID_TITLE'] = 'Acid Reflux';
$lang['ACID_DESCRIPTION'] = 'Ask our doctors and describe your symptoms.';	
$lang['ACID_OG_TITLE'] ="Acid Reflux";
$lang['ACID_OG_URL'] ="http://medlanes.com/Acid Reflux";    
$lang['ACID_OG_DESCRIPTION'] = 'Ask our doctors and describe your symptoms.';
$lang['ACID_TWITTER_TITLE'] ="Acid Reflux";	
$lang['ACID_TWITTER_URL'] ="http://medlanes.com/Acid Reflux";  
$lang['ACID_TWITTER_DESCRIPTION'] = 'Ask our doctors and describe your symptoms.';                    

$lang['ACID_HEADLINE'] ="Acid Reflux";
$lang['ACID_SLIDER'] ="<span itemprop=\"description\" itemscope itemtype=\"http://schema.org/MedicalCondition\">“Acid Reflux is a type of arthritis that can cause attacks of sudden burning pain, stiffness, and swelling in a joint. ”</span>";

$lang['ACID_DESCRIPTION_H2'] ="Description of Acid Reflux";
$lang['ACID_DESCRIPTION_TEXT'] ="<span itemprop=\"description\" itemscope itemtype=\"http://schema.org/MedicalACID\">Acid Reflux is a type of arthritis that can cause attacks of sudden burning pain, stiffness, and swelling in a joint. Acid Reflux attacks, also referred to as flares, typically afflict the big toe. Acid Reflux will recur until properly treated. If untreated, gout can cause permanent damage to tendons, joints, and other tissues. Excess uric acid in the blood causes hard crystals to form in your joints, causing a Acid Reflux attack.
";





$lang['ACID_SIGNS_H2'] ="Signs &amp; Symptoms of Acid Reflux";
$lang['ACID_SIGNS_TEXT'] ="The most common sign of gout are nighttime attacks of swelling, tenderness, redness and sharp pains in the big toe. Attacks last can last anywhere between a few days, to several weeks. Acid Reflux does not strictly target the big toes, as it can flare up in any joint. Other signs and symptoms of gout include:
";

$lang['ACID_SIGNS_H3_1_TEXT2'] ="Ear Acid Reflux - The symptoms of Ear Acid Reflux may include:
";
$lang['ACID_SIGNS_H3_1_TEXT'] ="Nasal/Sinus Acid Reflux -  Nasal congestion symptoms vary, but may include :
 ";
$lang['ACID_SIGNS_H3_1_BULLET1'] ="Warmth, pain, swelling, and extreme tenderness in a joint
";
$lang['ACID_SIGNS_H3_1_BULLET2'] ="Limited movement or range of motion in the affected joint


";
$lang['ACID_SIGNS_H3_1_BULLET3'] ="Peeling and itching of the skin around the affected joint

";
$lang['ACID_SIGNS_H3_1_BULLET4'] ="Very red or slightly purple skin around the affected joint

";

$lang['ACID_CAUSES_H2'] ="Causes of Acid Reflux";
$lang['ACID_CAUSES_TEXT'] ="Acid Reflux is caused by too much uric acid in the blood (hyperuricemia). The exact cause of hyperuricemia is unknown, although genetics place a roll. Acid Reflux attacks and strike without a specific cause, but are typically caused be:
";
$lang['ACID_CAUSES_TEXT2'] ="Nasal/Sinus Acid Reflux - Is the blockage of the nasal passages due to membranes lining the nose becoming swollen from inflamed blood vessels. Nasal congestion can be an annoyance or a life threatening condition, so it is important that you understand the causes. These include:
";
$lang['ACID_CAUSES_TEXT3'] ="Ear Acid Reflux - Ear congestion is usually caused by a blockage of the Eustachian tube. The Eustachian tube connects the middle ear cavity with the oral cavity and helps in equalizing the air pressure in the middle ear. Air travel also causes ear congestion, as the changes in the atmospheric pressure affect the Eustachian tube as well. Other causes of ear congestion include: ";

$lang['ACID_CAUSES_BULLET1'] ="Overweight and Obese
";
$lang['ACID_CAUSES_BULLET2'] ="Eating a diet rich in seafood and red meat";
$lang['ACID_CAUSES_BULLET3'] ="High Blood Pressure";
$lang['ACID_CAUSES_BULLET4'] ="Some medicines including the frequent use of aspirin, niacin, or diuretics
";





$lang['ACID_DIAGNOSIS_H2'] ="Diagnosis of Acid Reflux";
$lang['ACID_DIAGNOSIS_TEXT'] ="Acid Reflux is typically diagnosed solely by physical examination of the affected site. Depending on the severity of the problem, your doctor may order additional tests which include:
";
$lang['ACID_DIAGNOSIS_BULLET1'] ="A joint fluid analysis (arthrocentesis) to detect the presence of uric acid crystals. This is the only certain way to diagnose gout.

";
$lang['ACID_DIAGNOSIS_BULLET2'] ="Blood tests to measure the amount of uric acid in the blood";
$lang['ACID_DIAGNOSIS_BULLET3'] ="Urinalysis";
$lang['ACID_DIAGNOSIS_BULLET4'] ="X-Rays in specific cases (especially late stages of the disease)

";



$lang['ACID_TREATMENT_H2'] ="Treatment of Acid Reflux";
$lang['ACID_TREATMENT_TEXT'] ="As there is no cure for gout, there are many options as far as treating the pain and preventing future gout attacks. Consult with a Medlanes physician today! We have help tens of thousands of people overcome gout, and make the necessary lifestyle changes to remain gout free. With multiple management options, we can treat both the symptoms of gout, as well as eliminating gout itself!

" ;


$lang['ACID_ASK_H2'] ="Ask a doctor about your Acid Reflux today!";
$lang['ACID_ASK_TEXT'] ="Medlanes is fast & easy to use. Get your diagnosis within minutes in just three simple steps:";
$lang['ACID_ASK_BULLET1'] ="Describe your symptoms & ask your questions";
$lang['ACID_ASK_BULLET2'] ="Choose your doctor & pay";
$lang['ACID_ASK_BULLET3'] ="Get qualified medical advice";

$lang['ACID_CONTENT_H2'] ="Table of Contents";
$lang['ACID_CONTENT1'] ="Description of Acid Reflux";
$lang['ACID_CONTENT2'] ="Signs &amp; Symptoms of Acid Reflux";
$lang['ACID_CONTENT3'] ="Causes of Acid Reflux";
$lang['ACID_CONTENT4'] ="Diagnosis of Acid Reflux";
$lang['ACID_CONTENT5'] ="Treatment of Acid Reflux";

$lang['ACID_ASK2_H2'] ="Ask a Doctor about Acid Reflux";
$lang['ACID_ASK2_TEXT'] ="The information on this page is no substitute for consulting with a doctor. </br> If you experience symptoms of Acid Reflux we offer easy, convenient and fast access to general practitioners who can give you qualified medical advice for your case.";

$lang['ACID_DOCTOR_NAME1'] ="Dr. Thomas P.";
$lang['ACID_DOCTOR_NAME2'] ="Dr. Eileen B.S.";
$lang['ACID_DOCTOR_NAME3'] ="Dr. James M.";
$lang['ACID_DOCTOR_NAME4'] ="Dr. Sandy K.";
$lang['ACID_DOCTOR_NAME5'] ="Dr. Gary J.R.";
$lang['ACID_DOCTOR_NAME6'] ="Dr. Theodor M.";


$lang['ACID_SIDEBAR_BUTTON'] ="Ask A Doctor Now For Advice";









// Portal Sexual Health-------------------
$lang['SEXUAL_ICD_CODE'] = '346';

$lang['SEXUAL_TITLE'] = 'Sexual Health';
$lang['SEXUAL_DESCRIPTION'] = 'Ask our doctors and describe your symptoms.';	
$lang['SEXUAL_OG_TITLE'] ="Sexual Health";
$lang['SEXUAL_OG_URL'] ="http://medlanes.com/SEXUAL Health";    
$lang['SEXUAL_OG_DESCRIPTION'] = 'Ask our doctors and describe your symptoms.';
$lang['SEXUAL_TWITTER_TITLE'] ="Sexual Health";	
$lang['SEXUAL_TWITTER_URL'] ="http://medlanes.com/sexualhealth";  
$lang['SEXUAL_TWITTER_DESCRIPTION'] = 'Ask our doctors and describe your symptoms.';                    

$lang['SEXUAL_HEADLINE'] ="Sexual Health";
$lang['SEXUAL_SLIDER'] ="<span itemprop=\"description\" itemscope itemtype=\"http://schema.org/MedicalCondition\">“Sexuality is a big part of being human. Love, affection and sexual intimacy all play a role in healthy relationships.  ” <br><br>  <span style='font-size:16px ;float:right !important; margin-right: 5px;'>All information is validated by expert doctors.</span></span>";

$lang['SEXUAL_DESCRIPTION_H2'] ="Overview";
$lang['SEXUAL_DESCRIPTION_TEXT'] ="<span itemprop=\"description\" itemscope itemtype=\"http://schema.org/Medicalsexual\">Sexuality is a big part of being human. Love, affection and sexual intimacy all play a role in healthy relationships. 
They also contribute to your sense of well-being. A number of disorders can affect the ability to have or enjoy 
sex in both men and women. it requires a positive and respectful approach to sexuality and sexual relationships, as well as the possibility of having
.
";





$lang['SEXUAL_TOP_H2'] ="Top Searched Conditions in Sexual Health";
$lang['SEXUAL_SIGNS_TEXT'] ="The most common sign of gout are nighttime attacks of swelling, tenderness, redness and sharp pains in the big toe. Attacks last can last anywhere between a few days, to several weeks. SEXUAL Health does not strictly target the big toes, as it can flare up in any joint. Other signs and symptoms of gout include:
";

$lang['SEXUAL_SIGNS_H3_1_TEXT2'] ="Ear SEXUAL Health - The symptoms of Ear SEXUAL Health may include:
";
$lang['SEXUAL_SIGNS_H3_1_TEXT'] ="Nasal/Sinus SEXUAL Health -  Nasal congestion symptoms vary, but may include :
 ";
$lang['SEXUAL_SIGNS_H3_1_BULLET1'] ="Warmth, pain, swelling, and extreme tenderness in a joint
";
$lang['SEXUAL_SIGNS_H3_1_BULLET2'] ="Limited movement or range of motion in the affected joint


";
$lang['SEXUAL_SIGNS_H3_1_BULLET3'] ="Peeling and itching of the skin around the affected joint

";
$lang['SEXUAL_SIGNS_H3_1_BULLET4'] ="Very red or slightly purple skin around the affected joint

";

$lang['SEXUAL_CONDITIONS_H2'] ="Conditions A-Z";
$lang['SEXUAL_CAUSES_TEXT'] ="SEXUAL Health is caused by too much uric acid in the blood (hyperuricemia). The exact cause of hyperuricemia is unknown, although genetics place a roll. SEXUAL Health attacks and strike without a specific cause, but are typically caused be:
";
$lang['SEXUAL_CAUSES_TEXT2'] ="Nasal/Sinus Acid Reflux - Is the blockage of the nasal passages due to membranes lining the nose becoming swollen from inflamed blood vessels. Nasal congestion can be an annoyance or a life threatening condition, so it is important that you understand the causes. These include:
";
$lang['SEXUAL_CAUSES_TEXT3'] ="Ear Acid Reflux - Ear congestion is usually caused by a blockage of the Eustachian tube. The Eustachian tube connects the middle ear cavity with the oral cavity and helps in equalizing the air pressure in the middle ear. Air travel also causes ear congestion, as the changes in the atmospheric pressure affect the Eustachian tube as well. Other causes of ear congestion include: ";

$lang['SEXUAL_CAUSES_BULLET1'] ="Overweight and Obese
";
$lang['SEXUAL_CAUSES_BULLET2'] ="Eating a diet rich in seafood and red meat";
$lang['SEXUAL_CAUSES_BULLET3'] ="High Blood Pressure";
$lang['SEXUAL_CAUSES_BULLET4'] ="Some medicines including the frequent use of aspirin, niacin, or diuretics
";





$lang['SEXUAL_DIAGNOSIS_H2'] ="Diagnosis of Acid Reflux";
$lang['SEXUAL_DIAGNOSIS_TEXT'] ="Acid Reflux is typically diagnosed solely by physical examination of the affected site. Depending on the severity of the problem, your doctor may order additional tests which include:
";
$lang['SEXUAL_DIAGNOSIS_BULLET1'] ="A joint fluid analysis (arthrocentesis) to detect the presence of uric acid crystals. This is the only certain way to diagnose gout.

";
$lang['SEXUAL_DIAGNOSIS_BULLET2'] ="Blood tests to measure the amount of uric acid in the blood";
$lang['SEXUAL_DIAGNOSIS_BULLET3'] ="Urinalysis";
$lang['SEXUAL_DIAGNOSIS_BULLET4'] ="X-Rays in specific cases (especially late stages of the disease)

";



$lang['SEXUAL_TREATMENT_H2'] ="Treatment of Acid Reflux";
$lang['SEXUAL_TREATMENT_TEXT'] ="As there is no cure for gout, there are many options as far as treating the pain and preventing future gout attacks. Consult with a Medlanes physician today! We have help tens of thousands of people overcome gout, and make the necessary lifestyle changes to remain gout free. With multiple management options, we can treat both the symptoms of gout, as well as eliminating gout itself!

" ;


$lang['SEXUAL_ASK_H2'] ="Ask a doctor about Sexual Health today!";
$lang['SEXUAL_ASK_TEXT'] ="Medlanes is fast & easy to use. Get your diagnosis within minutes in just three simple steps:";
$lang['SEXUAL_ASK_BULLET1'] ="Describe your symptoms & ask your questions";
$lang['SEXUAL_ASK_BULLET2'] ="Choose your doctor & pay";
$lang['SEXUAL_ASK_BULLET3'] ="Get qualified medical advice";

$lang['SEXUAL_CONTENT_H2'] ="Table of Contents";
$lang['SEXUAL_CONTENT1'] ="Description of Acid Reflux";
$lang['SEXUAL_CONTENT2'] ="Signs &amp; Symptoms of Acid Reflux";
$lang['SEXUAL_CONTENT3'] ="Causes of Acid Reflux";
$lang['SEXUAL_CONTENT4'] ="Diagnosis of Acid Reflux";
$lang['SEXUAL_CONTENT5'] ="Treatment of Acid Reflux";

$lang['SEXUAL_ASK2_H2'] ="Ask a Doctor about Sexual Health";
$lang['SEXUAL_ASK2_TEXT'] ="The information on this page is no substitute for consulting with a doctor. </br> If you experience symptoms of Sexual Health we offer easy, convenient and fast access to general practitioners who can give you qualified medical advice for your case.";

$lang['SEXUAL_DOCTOR_NAME1'] ="Dr. Thomas P.";
$lang['SEXUAL_DOCTOR_NAME2'] ="Dr. Eileen B.S.";
$lang['SEXUAL_DOCTOR_NAME3'] ="Dr. James M.";
$lang['SEXUAL_DOCTOR_NAME4'] ="Dr. Sandy K.";
$lang['SEXUAL_DOCTOR_NAME5'] ="Dr. Gary J.R.";
$lang['SEXUAL_DOCTOR_NAME6'] ="Dr. Theodor M.";


$lang['SEXUAL_SIDEBAR_BUTTON'] ="Ask A Doctor Now For Advice";





$lang['DSBOX1_DOCTOR_img'] = '<img src="./images/docs/Doctor1.png" id="doc_img"/>';
$lang['DSBOX1_DOCTOR_txt'] = 'Dr. Thomas Penning';
$lang['DSBOX1_SP_img'] = '<img src="./images/family_medicine.png" id="specialty_img" width="12px" height="12px">';
$lang['DSBOX1_SP_txt'] = 'Family Medicine';
$lang['DSBOX1_EXP_h'] = 'Experience';
$lang['DSBOX1_EXP_txt'] = '23 years of practice';
$lang['DSBOX1_RATING_h'] = 'Rating';
$lang['DSBOX1_RATING_txt'] = '4.9 (147 Ratings)';




// Condition Premature--------------------
$lang['PREMATURE_ICD_CODE'] = '346';

$lang['PREMATURE_TITLE'] = 'Premature Ejaculation';
$lang['PREMATURE_DESCRIPTION'] = 'Find information about congestion including causes,  types of congestion, and the treatment options that are available.';	
$lang['PREMATURE_OG_TITLE'] ="Premature Ejaculation";
$lang['PREMATURE_OG_URL'] ="http://medlanes.com/Premature Ejaculation";    
$lang['PREMATURE_OG_DESCRIPTION'] = 'Find information about congestion including causes,  types of congestion, and the treatment options that are available..';
$lang['PREMATURE_TWITTER_TITLE'] ="Premature Ejaculation";	
$lang['PREMATURE_TWITTER_URL'] ="http://medlanes.com/Premature Ejaculation";  
$lang['PREMATURE_TWITTER_DESCRIPTION'] = 'Find information about congestion including causes,  types of congestion, and the treatment options that are available..';                    

$lang['PREMATURE_HEADLINE'] ="Premature Ejaculation";
$lang['PREMATURE_SLIDER'] ="<span itemprop=\"description\" itemscope itemtype=\"http://schema.org/MedicalCondition\">“Premature ejaculation is uncontrolled ejaculation either before or shortly after sexual penetration, with minimal sexual stimulation and before the person wishes.”<br><br>  <span style='font-size:16px ;float:right !important; margin-right: 5px;'>All information is validated by expert doctors.</span></span> </span>";

$lang['PREMATURE_DESCRIPTION_H2'] ="Description of Premature Ejaculation";
$lang['PREMATURE_DESCRIPTION_TEXT'] ="<span itemprop=\"description\" itemscope itemtype=\"http://schema.org/MedicalPREMATURE\"> Premature ejaculation is uncontrolled ejaculation either before or shortly after sexual penetration, with minimal sexual stimulation and before the person wishes. It may result in an unsatisfactory sexual experience for both partners. This can increase the anxiety that may contribute to the problem. Premature ejaculation is one of the most common forms of male sexual dysfunction and has affected every man at some point in his life.
";
$lang['PREMATURE_DESCRIPTION_BULLET1'] ="<span itemprop=\"subtype\" itemscope itemtype=\"http://schema.org/MedicalCondition\"><span itemprop=\"name\">Chest Premature Ejaculation";
$lang['PREMATURE_DESCRIPTION_BULLET2'] ="<span itemprop=\"subtype\" itemscope itemtype=\"http://schema.org/MedicalCondition\"><span itemprop=\"name\">Nasal/Sinus Premature Ejaculation</span></span>";
$lang['PREMATURE_DESCRIPTION_BULLET3'] ="<span itemprop=\"subtype\" itemscope itemtype=\"http://schema.org/MedicalCondition\"><span itemprop=\"name\">Ear Premature Ejaculation</span></span>";




$lang['PREMATURE_SIGNS_H2'] ="Signs of Premature Ejaculation";
$lang['PREMATURE_SIGNS_TEXT'] ="The main symptom of premature ejaculation is an uncontrolled ejaculation either before or shortly after intercourse begins. Ejaculation occurs before the person wishes it, with minimal sexual stimulation.
";



$lang['PREMATURE_CAUSES_H2'] ="Causes of Premature Ejaculation";
$lang['PREMATURE_CAUSES_TEXT'] ="Most cases of premature ejaculation do not have a clear cause. With sexual experience and age, men often learn to delay orgasm. Premature ejaculation may occur with a new partner, only in certain sexual situations, or if it has been a long time since the last ejaculation. Psychological factors such as anxiety, guilt, or depression can cause premature ejaculation. In some cases, premature ejaculation may be related to a medical cause such as hormonal problems, injury, or a side effect of certain medicines.
";





$lang['PREMATURE_DIAGNOSIS_H2'] ="Diagnosis of Premature Ejaculation";
$lang['PREMATURE_DIAGNOSIS_TEXT'] ="A simple evaluation is typically all your doctor needs to determine the cause of your congestion. Additional tests may be necessary at the discretion of your doctor.
";


$lang['PREMATURE_TREATMENT_H2'] ="Treatment of Premature Ejaculation";
$lang['PREMATURE_TREATMENT_TEXT'] ="Practicing relaxation techniques or using distraction methods may help you delay ejaculation. For some men, stopping or cutting down on the use of alcohol, tobacco, or illegal drugs may improve their ability to control ejaculation.With a quick consultation with a Medlanes physician, they can recommend multiple treatments options,provide you and your partner practice specific techniques to help delay ejaculation. These techniques may involve identifying and controlling the sensations that lead up to ejaculation and communicating to slow or stop stimulation. Other options include using a condom to reduce sensation to the penis or trying a different position (such as lying on your back) during intercourse. Avoid the embarrassing visit to the doctors office, ask your questions now! 

" ;


$lang['PREMATURE_ASK_H2'] ="Ask a doctor about your Premature Ejaculation today!";
$lang['PREMATURE_ASK_TEXT'] ="Medlanes is fast & easy to use. Get your diagnosis within minutes in just three simple steps:";
$lang['PREMATURE_ASK_BULLET1'] ="Describe your symptoms & ask your questions";
$lang['PREMATURE_ASK_BULLET2'] ="Choose your doctor & pay";
$lang['PREMATURE_ASK_BULLET3'] ="Get qualified medical advice";

$lang['PREMATURE_CONTENT_H2'] ="Table of Contents";
$lang['PREMATURE_CONTENT1'] ="Description";
$lang['PREMATURE_CONTENT2'] ="Signs &amp; Symptoms";
$lang['PREMATURE_CONTENT3'] ="Causes";
$lang['PREMATURE_CONTENT4'] ="Diagnosis";
$lang['PREMATURE_CONTENT5'] ="Treatment";

$lang['PREMATURE_ASK2_H2'] ="Ask a Doctor about Premature Ejaculation";
$lang['PREMATURE_ASK2_TEXT'] ="The information on this page is no substitute for consulting with a doctor. </br> If you experience symptoms of Premature Ejaculation we offer easy, convenient and fast access to general practitioners who can give you qualified medical advice for your case.";

$lang['PREMATURE_DOCTOR_NAME1'] ="Dr. Thomas P.";
$lang['PREMATURE_DOCTOR_NAME2'] ="Dr. Eileen B.S.";
$lang['PREMATURE_DOCTOR_NAME3'] ="Dr. James M.";
$lang['PREMATURE_DOCTOR_NAME4'] ="Dr. Sandy K.";
$lang['PREMATURE_DOCTOR_NAME5'] ="Dr. Gary J.R.";
$lang['PREMATURE_DOCTOR_NAME6'] ="Dr. Theodor M.";


$lang['PREMATURE_SIDEBAR_BUTTON'] ="Ask A Doctor Now For Advice";






// Condition Female Orgasmic Disorder--------------------
$lang['FDISORDER_ICD_CODE'] = '346';

$lang['FDISORDER_TITLE'] = 'Female orgasmic disorder';
$lang['FDISORDER_DESCRIPTION'] = 'Find information about congestion including causes,  types of congestion, and the treatment options that are available.';	
$lang['FDISORDER_OG_TITLE'] ="Female orgasmic disorder";
$lang['FDISORDER_OG_URL'] ="http://medlanes.com/Female orgasmic disorder";    
$lang['FDISORDER_OG_DESCRIPTION'] = 'Find information about congestion including causes,  types of congestion, and the treatment options that are available..';
$lang['FDISORDER_TWITTER_TITLE'] ="Female orgasmic disorder";	
$lang['FDISORDER_TWITTER_URL'] ="http://medlanes.com/Female orgasmic disorder";  
$lang['FDISORDER_TWITTER_DESCRIPTION'] = 'Find information about congestion including causes,  types of congestion, and the treatment options that are available..';                    

$lang['FDISORDER_HEADLINE'] ="Female orgasmic disorder";
$lang['FDISORDER_SLIDER'] ="<span itemprop=\"description\" itemscope itemtype=\"http://schema.org/MedicalCondition\">“Female orgasmic disorder (FOD) is the persistent or recurrent inability of a woman to have an orgasm (climax or sexual release) after adequate sexual arousal and sexual stimulation  ” <br><br>  <span style='font-size:16px ;float:right !important; margin-right: 5px;'>All information is validated by expert doctors.</span></span></span>";

$lang['FDISORDER_DESCRIPTION_H2'] ="Description of Female orgasmic disorder";
$lang['FDISORDER_DESCRIPTION_TEXT'] ="<span itemprop=\"description\" itemscope itemtype=\"http://schema.org/MedicalFDISORDER\"> Female orgasmic disorder (FOD) is the persistent or recurrent inability of a woman to have an orgasm (climax or sexual release) after adequate sexual arousal and sexual stimulation. There are both physiological and psychological causes for a woman's inability to have an orgasm. FOD is the persistent or recurrent inability of a woman to achieve orgasm. This lack of response affects the quality of the woman's sexual experiences. To understand FOD, it is first necessary to understand the physiological changes that normally take place in a woman's body during sexual arousal and orgasm.
";
$lang['FDISORDER_DESCRIPTION_BULLET1'] ="<span itemprop=\"subtype\" itemscope itemtype=\"http://schema.org/MedicalCondition\"><span itemprop=\"name\">Chest Female orgasmic disorder";
$lang['FDISORDER_DESCRIPTION_BULLET2'] ="<span itemprop=\"subtype\" itemscope itemtype=\"http://schema.org/MedicalCondition\"><span itemprop=\"name\">Nasal/Sinus Female orgasmic disorder</span></span>";
$lang['FDISORDER_DESCRIPTION_BULLET3'] ="<span itemprop=\"subtype\" itemscope itemtype=\"http://schema.org/MedicalCondition\"><span itemprop=\"name\">Ear Female orgasmic disorder</span></span>";




$lang['FDISORDER_SIGNS_H2'] ="Signs &amp; Symptoms of Female orgasmic disorder";
$lang['FDISORDER_SIGNS_TEXT'] ="Orgasmic dysfunction doesn’t refer only to sexual intercourse. It also refers to the inability to reach orgasm during masturbation or direct stimulation of the clitoris.
Symptoms of female orgaismic disorder include:
";

$lang['FDISORDER_SIGNS_H3_1_TEXT2'] ="Female orgasmic disorder - The symptoms of Ear Female orgasmic disorder may include:
";
$lang['FDISORDER_SIGNS_H3_1_TEXT'] ="FOD is more likely to have a psychological, rather than a physical cause. Inadequate time spent in foreplay, inadequate arousal, lack of appropriate sexual stimulation, poor sexual communication with a partner, and failure to continue with stimulation for an adequate length of time may cause failure to climax, but are not considered causes of FOD.

 ";
$lang['FDISORDER_SIGNS_H3_1_BULLET1'] ="Primary anorgasmia: You’ve never experienced an orgasm.";
$lang['FDISORDER_SIGNS_H3_1_BULLET2'] ="Secondary anorgasmia: You experience difficulty reaching orgasm, even though you’ve experienced them before.


";
$lang['FDISORDER_SIGNS_H3_1_BULLET3'] ="Situational anorgasmia: This is the most common type of orgasmic dysfunction. It’s when a woman can only orgasm during specific situations, such as oral sex or masturbation.



";
$lang['FDISORDER_SIGNS_H3_1_BULLET4'] ="General anorgasmia: The inability to achieve orgasm under any circumstances 
";


$lang['FDISORDER_CAUSES_H2'] ="Causes of Female orgasmic disorder";
$lang['FDISORDER_CAUSES_TEXT'] ="FOD can be generalized or situation-specific. In generalized FOD, the failure to have an orgasm occurs with different partners and in many different settings. In situational FOD, inability to reach climax occurs only with specific partners or under particular circumstances. FOD may be due either to psychological factors or a combination of physiological and psychological factors, but not due to physiological factors alone.
Physiological causes of FOD include:

";
$lang['FDISORDER_CAUSES_TEXT2'] ="Psychological causes of FOD include:

";
$lang['FDISORDER_CAUSES_TEXT3'] ="Ear Female orgasmic disorder - Ear congestion is usually caused by a blockage of the Eustachian tube. The Eustachian tube connects the middle ear cavity with the oral cavity and helps in equalizing the air pressure in the middle ear. Air travel also causes ear congestion, as the changes in the atmospheric pressure affect the Eustachian tube as well. Other causes of ear congestion include: ";

$lang['FDISORDER_CAUSES_BULLET1'] ="Damage to the blood vessels of the pelvic region";
$lang['FDISORDER_CAUSES_BULLET2'] ="Spinal cord lesions or damage to the nerves in the pelvic area";
$lang['FDISORDER_CAUSES_BULLET3'] ="Side effects of medications (antipsychotics, antidepressants, narcotics) or illicit substance abuse
";
$lang['FDISORDER_CAUSES_BULLET4'] ="Removal of the clitoris (also called female genital mutilation, a cultural practice in parts of Africa, the Middle East, and Asia)
";
$lang['FDISORDER_CAUSES_BULLET5'] ="Past sexual abuse, rape, incest, or other traumatic sexual experience";
$lang['FDISORDER_CAUSES_BULLET6'] ="Emotional abuse
";
$lang['FDISORDER_CAUSES_BULLET7'] ="Fear of becoming pregnant
";
$lang['FDISORDER_CAUSES_BULLET8'] ="Fear of rejection by partner";
$lang['FDISORDER_CAUSES_BULLET9'] ="Fear of loss of control during orgasm";
$lang['FDISORDER_CAUSES_BULLET10'] ="Relationship problems with partner";
$lang['FDISORDER_CAUSES_BULLET11'] ="Life stresses, such as financial worries, job loss, or divorce";
$lang['FDISORDER_CAUSES_BULLET12'] ="Guilt about sex or sexual pleasure";
$lang['FDISORDER_CAUSES_BULLET13'] ="Religious or cultural beliefs about sex";
$lang['FDISORDER_CAUSES_BULLET14'] ="Other mental health disorders such as major depression";





$lang['FDISORDER_DIAGNOSIS_H2'] ="Diagnosis of Female orgasmic disorder";
$lang['FDISORDER_DIAGNOSIS_TEXT'] ="The first step to diagnosing female orgasmic disorder is talking with your doctor. Your doctor will ask you questions about your medical history, as well as about FOD specifically such as when the symptoms started, etc.
";


$lang['FDISORDER_TREATMENT_H2'] ="Treatment of Female orgasmic disorder";
$lang['FDISORDER_TREATMENT_TEXT'] ="There are many different treatment options for female orgasmic disorder. Your treatment options depend directly on the root cause of the FOD, thus it is impairative to find that cause immediateky. FOD is an embarrassing problem that causes both emotional and relationship problems. Ask your questions today anonymously to one of Medlanes many qualified physicians, and get back to being yourself!
" ;


$lang['FDISORDER_ASK_H2'] ="Ask a doctor about your Female orgasmic disorder today!";
$lang['FDISORDER_ASK_TEXT'] ="Medlanes is fast & easy to use. Get your diagnosis within minutes in just three simple steps:";
$lang['FDISORDER_ASK_BULLET1'] ="Describe your symptoms & ask your questions";
$lang['FDISORDER_ASK_BULLET2'] ="Choose your doctor & pay";
$lang['FDISORDER_ASK_BULLET3'] ="Get qualified medical advice";

$lang['FDISORDER_CONTENT_H2'] ="Table of Contents";
$lang['FDISORDER_CONTENT1'] ="Description";
$lang['FDISORDER_CONTENT2'] ="Signs &amp; Symptoms";
$lang['FDISORDER_CONTENT3'] ="Causes";
$lang['FDISORDER_CONTENT4'] ="Diagnosis";
$lang['FDISORDER_CONTENT5'] ="Treatment";

$lang['FDISORDER_ASK2_H2'] ="Ask a Doctor about Female orgasmic disorder";
$lang['FDISORDER_ASK2_TEXT'] ="The information on this page is no substitute for consulting with a doctor. </br> If you experience symptoms of Female orgasmic disorder we offer easy, convenient and fast access to general practitioners who can give you qualified medical advice for your case.";

$lang['FDISORDER_DOCTOR_NAME1'] ="Dr. Thomas P.";
$lang['FDISORDER_DOCTOR_NAME2'] ="Dr. Eileen B.S.";
$lang['FDISORDER_DOCTOR_NAME3'] ="Dr. James M.";
$lang['FDISORDER_DOCTOR_NAME4'] ="Dr. Sandy K.";
$lang['FDISORDER_DOCTOR_NAME5'] ="Dr. Gary J.R.";
$lang['FDISORDER_DOCTOR_NAME6'] ="Dr. Theodor M.";


$lang['FDISORDER_SIDEBAR_BUTTON'] ="Ask A Doctor Now For Advice";






// Condition Erectile Dysfunction--------------------
$lang['ERECTILE_ICD_CODE'] = '346';

$lang['ERECTILE_TITLE'] = 'Erectile Dysfunction';
$lang['ERECTILE_DESCRIPTION'] = 'Find information about congestion including causes,  types of congestion, and the treatment options that are available.';	
$lang['ERECTILE_OG_TITLE'] ="Erectile Dysfunction";
$lang['ERECTILE_OG_URL'] ="http://medlanes.com/Erectile Dysfunction";    
$lang['ERECTILE_OG_DESCRIPTION'] = 'Find information about congestion including causes,  types of congestion, and the treatment options that are available..';
$lang['ERECTILE_TWITTER_TITLE'] ="Erectile Dysfunction";	
$lang['ERECTILE_TWITTER_URL'] ="http://medlanes.com/Erectile Dysfunction";  
$lang['ERECTILE_TWITTER_DESCRIPTION'] = 'Find information about congestion including causes,  types of congestion, and the treatment options that are available..';                    

$lang['ERECTILE_HEADLINE'] ="Erectile Dysfunction";
$lang['ERECTILE_SLIDER'] ="<span itemprop=\"description\" itemscope itemtype=\"http://schema.org/MedicalCondition\">“Erectile dysfunction, also called impotence, is a sexual dysfunction characterized 
by the lack of ability to develop or maintain an erected penis hard enough to 
engage in sexual activities.” <br><br>  <span style='font-size:16px ;float:right !important; margin-right: 5px;'>All information is validated by expert doctors.</span></span></span>";

$lang['ERECTILE_DESCRIPTION_H2'] ="Description of Erectile Dysfunction";
$lang['ERECTILE_DESCRIPTION_TEXT'] ="<span itemprop=\"description\" itemscope itemtype=\"http://schema.org/MedicalERECTILE\"> Erectile dysfunction, also called impotence, is a sexual dysfunction characterized 
by the lack of ability to develop or maintain an erected penis hard enough to 
engage in sexual activities. When sexual arousal is transmitted from the brain 
to the nerves in the penis, a hydraulic effect is initiated which leads to the blood 
entering and being retained in sponge-like bodies within the penis. 
";
$lang['ERECTILE_DESCRIPTION_BULLET1'] ="<span itemprop=\"subtype\" itemscope itemtype=\"http://schema.org/MedicalCondition\"><span itemprop=\"name\">Chest Erectile Dysfunction";
$lang['ERECTILE_DESCRIPTION_BULLET2'] ="<span itemprop=\"subtype\" itemscope itemtype=\"http://schema.org/MedicalCondition\"><span itemprop=\"name\">Nasal/Sinus Erectile Dysfunction</span></span>";
$lang['ERECTILE_DESCRIPTION_BULLET3'] ="<span itemprop=\"subtype\" itemscope itemtype=\"http://schema.org/MedicalCondition\"><span itemprop=\"name\">Ear Erectile Dysfunction</span></span>";




$lang['ERECTILE_SIGNS_H2'] ="Signs &amp; Symptoms of Erectile Dysfunction";
$lang['ERECTILE_SIGNS_TEXT'] ="The main symptom of an erectile dysfunction is the inability to attain or sustain 
an erected penis. In some cases, such as overconsumption of alcohol, this is 
temporary. In other cases, this can be an symptom itself for psychological or 
medical condition. 

Common psychological symptoms include 
";

$lang['ERECTILE_SIGNS_H3_1_TEXT2'] ="Erectile Dysfunction - The symptoms of Ear Erectile Dysfunction may include:
";
$lang['ERECTILE_SIGNS_H3_1_TEXT'] ="FOD is more likely to have a psychological, rather than a physical cause. Inadequate time spent in foreplay, inadequate arousal, lack of appropriate sexual stimulation, poor sexual communication with a partner, and failure to continue with stimulation for an adequate length of time may cause failure to climax, but are not considered causes of FOD.

 ";
$lang['ERECTILE_SIGNS_H3_1_BULLET1'] ="Low sex drive";
$lang['ERECTILE_SIGNS_H3_1_BULLET2'] ="Low self-esteem


";
$lang['ERECTILE_SIGNS_H3_1_BULLET3'] ="depression



";
$lang['ERECTILE_SIGNS_H3_1_BULLET4'] ="Guilt
";


$lang['ERECTILE_CAUSES_H2'] ="Causes of Erectile Dysfunction";
$lang['ERECTILE_CAUSES_TEXT'] =" Erectile dysfunctions can have organic or psychological causes. Psychological 
impotence is when thoughts or feelings, such as stress or anxiety, prevent 
erections; this is somewhat less frequent but can often be helped. The most 
important organic causes are 


";
$lang['ERECTILE_CAUSES_TEXT2'] ="Psychological causes of FOD include:

";
$lang['ERECTILE_CAUSES_TEXT3'] ="Ear Erectile Dysfunction - Ear congestion is usually caused by a blockage of the Eustachian tube. The Eustachian tube connects the middle ear cavity with the oral cavity and helps in equalizing the air pressure in the middle ear. Air travel also causes ear congestion, as the changes in the atmospheric pressure affect the Eustachian tube as well. Other causes of ear congestion include: ";

$lang['ERECTILE_CAUSES_BULLET1'] ="Cardiovascular disease ";
$lang['ERECTILE_CAUSES_BULLET2'] ="Diabetes";
$lang['ERECTILE_CAUSES_BULLET3'] ="Neurological problems 
";
$lang['ERECTILE_CAUSES_BULLET4'] ="Hormonal insufficiencies (hypogonadism) 
";
$lang['ERECTILE_CAUSES_BULLET5'] ="Drug side effects";
$lang['ERECTILE_CAUSES_BULLET6'] ="Overconsumption of alcohol
";
$lang['ERECTILE_CAUSES_BULLET7'] ="Diabetes
";
$lang['ERECTILE_CAUSES_BULLET8'] ="Thyroid problems";
$lang['ERECTILE_CAUSES_BULLET9'] ="Kidney issues";
$lang['ERECTILE_CAUSES_BULLET10'] ="Blood vessel damage";
$lang['ERECTILE_CAUSES_BULLET11'] ="Nerve damage";
$lang['ERECTILE_CAUSES_BULLET12'] ="high blood pressure";
$lang['ERECTILE_CAUSES_BULLET13'] ="Pelvic trauma";
$lang['ERECTILE_CAUSES_BULLET14'] ="Heavy smoking";





$lang['ERECTILE_DIAGNOSIS_H2'] ="Diagnosis of Erectile Dysfunction";
$lang['ERECTILE_DIAGNOSIS_TEXT'] ="It is highly recommended to seek medical attention, to determine the reasoning  ;

behind your erectile dysfunction and obtain fast remedy for this condition. It 

is important to share your medical history with your doctor, such as previous 

conditions and current medication. An erectile dysfunction might be a symptom of 

another, more serious condition, this is why it is important to administer tests and 

physical examination to including
";


$lang['ERECTILE_DIAGNOSIS_BULLET1'] = "Urine test";
$lang['ERECTILE_DIAGNOSIS_BULLET2'] =  "Ultrasound";
$lang['ERECTILE_DIAGNOSIS_BULLET3'] =  "Diagnosis of Erectile Dysfunction";


$lang['ERECTILE_TREATMENT_H2'] ="Treatment of Erectile Dysfunction";
$lang['ERECTILE_TREATMENT_TEXT'] =" Erectile dysfunctions can subside when the underlying cause for this condition 
is found and treated. It can also be somewhat socially challenging to address an 
erectile dysfunction at your next medical examination. If you prefer completely 
anonymous medical consultation, we at Medlanes have diagnosed and treated 
thousands of cases of erectile dysfunctions and are happy to help you too!
";


$lang['ERECTILE_ASK_H2'] ="Ask a doctor about your Erectile Dysfunction today!";
$lang['ERECTILE_ASK_TEXT'] ="Medlanes is fast & easy to use. Get your diagnosis within minutes in just three simple steps:";
$lang['ERECTILE_ASK_BULLET1'] ="Describe your symptoms & ask your questions";
$lang['ERECTILE_ASK_BULLET2'] ="Choose your doctor & pay";
$lang['ERECTILE_ASK_BULLET3'] ="Get qualified medical advice";

$lang['ERECTILE_CONTENT_H2'] ="Table of Contents";
$lang['ERECTILE_CONTENT1'] ="Description";
$lang['ERECTILE_CONTENT2'] ="Signs &amp; Symptoms";
$lang['ERECTILE_CONTENT3'] ="Causes";
$lang['ERECTILE_CONTENT4'] ="Diagnosis";
$lang['ERECTILE_CONTENT5'] ="Treatment";

$lang['ERECTILE_ASK2_H2'] ="Ask a Doctor about Erectile Dysfunction";
$lang['ERECTILE_ASK2_TEXT'] ="The information on this page is no substitute for consulting with a doctor. </br> If you experience symptoms of Erectile Dysfunction we offer easy, convenient and fast access to general practitioners who can give you qualified medical advice for your case.";

$lang['ERECTILE_DOCTOR_NAME1'] ="Dr. Thomas P.";
$lang['ERECTILE_DOCTOR_NAME2'] ="Dr. Eileen B.S.";
$lang['ERECTILE_DOCTOR_NAME3'] ="Dr. James M.";
$lang['ERECTILE_DOCTOR_NAME4'] ="Dr. Sandy K.";
$lang['ERECTILE_DOCTOR_NAME5'] ="Dr. Gary J.R.";
$lang['ERECTILE_DOCTOR_NAME6'] ="Dr. Theodor M.";


$lang['ERECTILE_SIDEBAR_BUTTON'] ="Ask A Doctor Now For Advice";







//doctors


$lang['DOC_HEADER'] ="Doctor Network and Expert Advisers";
$lang['DOC_TEXT'] ="In partnership with our expert doctors, Medlanes has brought together the best and brightest medical practitioners from every specialty to better serve you!";




$lang['DOC1_NAME'] ="Dr. med. Jessica Männel";
$lang['DOC1_SPEC'] ="Family Medicine Specialist";


$lang['DOC2_NAME'] ="Dr. med. Wolf Siepen";
$lang['DOC2_SPEC'] ="Orthopedics and Trauma Specialist
";

$lang['DOC3_NAME'] ="Dr. med. Kathrin Hamann";
$lang['DOC3_SPEC'] ="Family Medicine Specialist";

$lang['DOC4_NAME'] ="Beate Broermann";
$lang['DOC4_SPEC'] ="Psychologist";

$lang['DOC5_NAME'] ="Christian Welsch";
$lang['DOC5_SPEC'] ="ENT Specialist";

$lang['DOC6_NAME'] ="Stefan Paffrath";
$lang['DOC6_SPEC'] ="Psychologist";


$lang['DOC7_NAME'] ="Dr. med. Ive Schaaf";
$lang['DOC7_SPEC'] ="Phlebology Specialist";



$lang['DOC8_NAME'] ="Dr. med. Norbert Scheufele";
$lang['DOC8_SPEC'] ="Gynaecology and Obstetrics Specialist";


$lang['DOC9_NAME'] ="Dr.rer.nat.Uta-Maria Weigel";
$lang['DOC9_SPEC'] ="Naturopathic Specialist";


$lang['DOC10_NAME'] ="Dr. med. Joachim Nowak";
$lang['DOC10_SPEC'] =" Orthopedic Specialist";

?>

