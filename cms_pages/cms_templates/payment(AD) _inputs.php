
      <div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title">Header</h3>
  </div>
  <div class="panel-body" >
    <form class="form-horizontal">
          <cms-input cms-input-id='heading_text_1' cms-input-label="Page Title MetaTag"    cms-input-region="heading" ></cms-input>
          <cms-picture cms-input-id='heading_picture_2' cms-input-label="Logo Image"    cms-input-region="heading" ></cms-picture>
          <cms-input cms-input-id='heading_text_3' cms-input-label="Logo Title"   cms-input-default='Ask a medical question and get a fast answer'  cms-input-region="heading" ></cms-input>

</div>
</div>
  </form>


      <div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title">Steps</h3>
  </div>
  <div class="panel-body" >
    <form class="form-horizontal">
          <cms-input cms-input-id='steps_text_4' cms-input-label="Ask a Question Label"    cms-input-region="steps" ></cms-input>
          <cms-input cms-input-id='steps_text_5' cms-input-label="Additional Information Label"    cms-input-region="steps" ></cms-input>
          <cms-input cms-input-id='steps_text_6' cms-input-label="Your Options Label"    cms-input-region="steps" ></cms-input>
          <cms-input cms-input-id='steps_text_7' cms-input-label="Get an answer Label"    cms-input-region="steps" ></cms-input>

         
</div>
  </form>


      <div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title">Page Core</h3>
  </div>
  <div class="panel-body" >
    <form class="form-horizontal">
          <cms-input cms-input-id='core_text_8' cms-input-label="Medical Experts are ready to answer your question "    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_9' cms-input-label="Doctor Text 1"    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_10' cms-input-label="Doctor Text 2"    cms-input-region="core" ></cms-input>
          <cms-picture cms-input-id='core_picture_11' cms-input-label="Doctor Picture"    cms-input-region="core" ></cms-picture>



          <cms-input cms-input-id='core_text_14' cms-input-label="Doctor Name"    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_15' cms-input-label="Doctor Speciality"    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_16' cms-input-label="Online Now!"    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_17' cms-input-label="Professional Licenses:"    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_18' cms-input-label="Licences Text "    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_19' cms-input-label="Experience: "    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_20' cms-input-label="Experience Text"    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_21' cms-input-label="Degree:"    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_22' cms-input-label="Degree Text"    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_23' cms-input-label="Qualifications:"    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_24' cms-input-label="Qualifications Text"    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_25' cms-input-label="How urgent is your question? "    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_26' cms-input-label="Low"    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_27' cms-input-label="Medium"    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_28' cms-input-label="High"    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_29' cms-input-label="How detailed your answer should be?"    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_30' cms-input-label="Low"    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_31' cms-input-label="Medium"    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_32' cms-input-label="High"    cms-input-region="core" ></cms-input>


          <cms-input cms-input-id='core_text_34' cms-input-label="If you are"    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_35' cms-input-label="Satisfied description"    cms-input-region="core" ></cms-input>        
          <cms-input cms-input-id='core_text_36' cms-input-label="Satisfied"    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_37' cms-input-label="´The deposit will be "    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_38' cms-input-label="Amount (ex.35)"    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_39' cms-input-label="Help About Doctors deposit"    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_40' cms-input-label="deposit page link"    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_41' cms-input-label="Get Your Answer Now Button"    cms-input-region="core" ></cms-input>


                    

       
</div>
</div>
  </form>


      
    
 <div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title">Footer</h3>
  </div>
  <div class="panel-body" >
    <form class="form-horizontal">
          <cms-input cms-input-id='footer_text_107' cms-input-label="Medlanes Link"    cms-input-region="footer" ></cms-input>
          <cms-input cms-input-id='footer_text_108' cms-input-label="Medlanes Label"    cms-input-region="footer" ></cms-input>
          <cms-input cms-input-id='footer_text_109' cms-input-label="Support Link"    cms-input-region="footer" ></cms-input>
          <cms-input cms-input-id='footer_text_110' cms-input-label="Support Label"    cms-input-region="footer" ></cms-input>

          <cms-input cms-input-id='footer_text_111' cms-input-label="Contact us Link"    cms-input-region="footer" ></cms-input>
          <cms-input cms-input-id='footer_text_112' cms-input-label="Contact us Label"    cms-input-region="footer" ></cms-input>

          <cms-input cms-input-id='footer_text_113' cms-input-label="terms Link"    cms-input-region="footer" ></cms-input>
          <cms-input cms-input-id='footer_text_114' cms-input-label="terms Label"    cms-input-region="footer" ></cms-input>

          <cms-input cms-input-id='footer_text_115' cms-input-label="Privacy Link"    cms-input-region="footer" ></cms-input>
          <cms-input cms-input-id='footer_text_116' cms-input-label="Privacy Label"    cms-input-region="footer" ></cms-input>

          <cms-input cms-input-id='footer_text_117' cms-input-label="Copyright Text"    cms-input-region="footer" ></cms-input>
          <cms-input cms-input-id='footer_text_118' cms-input-label="Ask a doctor supported by text"    cms-input-region="footer" ></cms-input>


</div>
</div>
  </form>


                 
                                                                                                      