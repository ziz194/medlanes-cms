<?php

require_once('cms_api_call.php') ;
$url = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/';
?>

<!doctype html>
<html  lang="en">
<head>


   <link rel="alternate" hreflang="de" href="<?php echo $dataContent[$order]; $order++ ?>" />

  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(n,t,e){function r(e){if(!t[e]){var o=t[e]={exports:{}};n[e][0].call(o.exports,function(t){var o=n[e][1][t];return r(o?o:t)},o,o.exports)}return t[e].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<e.length;o++)r(e[o]);return r}({QJf3ax:[function(n,t){function e(n){function t(t,e,a){n&&n(t,e,a),a||(a={});for(var u=c(t),f=u.length,s=i(a,o,r),p=0;f>p;p++)u[p].apply(s,e);return s}function a(n,t){f[n]=c(n).concat(t)}function c(n){return f[n]||[]}function u(){return e(t)}var f={};return{on:a,emit:t,create:u,listeners:c,_events:f}}function r(){return{}}var o="nr@context",i=n("gos");t.exports=e()},{gos:"7eSDFh"}],ee:[function(n,t){t.exports=n("QJf3ax")},{}],gos:[function(n,t){t.exports=n("7eSDFh")},{}],"7eSDFh":[function(n,t){function e(n,t,e){if(r.call(n,t))return n[t];var o=e();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(n,t,{value:o,writable:!0,enumerable:!1}),o}catch(i){}return n[t]=o,o}var r=Object.prototype.hasOwnProperty;t.exports=e},{}],D5DuLP:[function(n,t){function e(n,t,e){return r.listeners(n).length?r.emit(n,t,e):(o[n]||(o[n]=[]),void o[n].push(t))}var r=n("ee").create(),o={};t.exports=e,e.ee=r,r.q=o},{ee:"QJf3ax"}],handle:[function(n,t){t.exports=n("D5DuLP")},{}],XL7HBI:[function(n,t){function e(n){var t=typeof n;return!n||"object"!==t&&"function"!==t?-1:n===window?0:i(n,o,function(){return r++})}var r=1,o="nr@id",i=n("gos");t.exports=e},{gos:"7eSDFh"}],id:[function(n,t){t.exports=n("XL7HBI")},{}],loader:[function(n,t){t.exports=n("G9z0Bl")},{}],G9z0Bl:[function(n,t){function e(){var n=v.info=NREUM.info;if(n&&n.licenseKey&&n.applicationID&&f&&f.body){c(d,function(t,e){t in n||(n[t]=e)}),v.proto="https"===l.split(":")[0]||n.sslForHttp?"https://":"http://",a("mark",["onload",i()]);var t=f.createElement("script");t.src=v.proto+n.agent,f.body.appendChild(t)}}function r(){"complete"===f.readyState&&o()}function o(){a("mark",["domContent",i()])}function i(){return(new Date).getTime()}var a=n("handle"),c=n(1),u=window,f=u.document,s="addEventListener",p="attachEvent",l=(""+location).split("?")[0],d={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-515.min.js"},v=t.exports={offset:i(),origin:l,features:{}};f[s]?(f[s]("DOMContentLoaded",o,!1),u[s]("load",e,!1)):(f[p]("onreadystatechange",r),u[p]("onload",e)),a("mark",["firstbyte",i()])},{1:11,handle:"D5DuLP"}],11:[function(n,t){function e(n,t){var e=[],o="",i=0;for(o in n)r.call(n,o)&&(e[i]=t(o,n[o]),i+=1);return e}var r=Object.prototype.hasOwnProperty;t.exports=e},{}]},{},["G9z0Bl"]);</script>
    <title><?php echo $dataContent[$order]; $order++ ?></title>

  <meta name="description" content="<?php echo $dataContent[$order]; $order++ ?>">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta property="og:title" content="<?php echo $dataContent[$order]; $order++ ?>"/>
  <meta property="og:type" content="article"/>
  <meta property="og:image" content="images/17/checkk.png"/>
  <meta property="og:url" content="<?php echo $dataContent[$order]; $order++ ?>"/>
  <meta property="og:description" content="<?php echo $dataContent[$order]; $order++ ?>"/>
  <meta name="twitter:card" content="summary"/>
  <meta name="twitter:url" content="<?php echo $dataContent[$order]; $order++ ?>"/>
  <meta name="twitter:title" content="<?php echo $dataContent[$order]; $order++ ?>"/>
  <meta name="twitter:description" content="<?php echo $dataContent[$order]; $order++ ?>"/>
  <meta name="twitter:image" content="images/17/checkk.png"/>

  <link rel="shortcut icon" type="image/x-icon" href="<?php echo $url ?>cms/cms_pages/favicon.ico">
  <!-- Styles -->
  <link rel="stylesheet" type="text/css" href="<?php echo $url ?>cms/cms_pages/css/content2.css">


  <link rel="stylesheet" href="<?php echo $url ?>cms/cms_pages/css/foundation.css" />
  <!-- jQuery  -->
  <script type="text/javascript" src="<?php echo $url ?>cms/cms_pages/js/jquery-1.11.2.min.js"></script>
  <!-- Scripts -->
  <script src="<?php echo $url ?>cms/cms_pages/js/jquery.autosize.min.js"></script>
  <!-- jQuery UI -->
  <link rel="stylesheet" type="text/css" href="<?php echo $url ?>cms/cms_pages/css/jquery-ui.min.css">
  <script type="text/javascript" src="<?php echo $url ?>cms/cms_pages/js/jquery-ui.min.js"></script>
  <script type="text/javascript" src='<?php echo $url ?>cms/cms_pages/js/pagescript.js'></script>
  <script type="text/javascript" src='<?php echo $url ?>cms/cms_pages/js/jquery.sticky-kit.min.js'></script>


</script>
</head>
<body>

  <div class="row main-row">
    <div class="large-12 columns main-column">
            <?php include("header2.php"); ?>

    </div>
  </div>

  <div class="row main-row">
    <div class="large-12 columns main-column">
            <div class="heading" style='background-image:url(<?php echo $url ?>cms/cms_assets/<?php echo $dataContent[$order]; $order++ ?>)' alt='<?php echo $dataContent[$order]; $order++ ?>'>
                  <h1 class='page-title'><?php echo $dataContent[$order]; ?></h1>
                  <!-- Mobile title -->
                  <h1 class='page-title-min'><span><?php echo $dataContent[$order]; $order++?></span></h1>

            </div>  
    </div>
  </div>
  <div class="row ">
    <div class="large-12 medium-12 columns">

      <div class="row">
        <div class="small-4 columns">
          <a href="#" class="active-button expand button blue-btn">ABOUT</a>
        </div>
        <div class="small-4 columns">
          <a href="<?php echo $dataContent[$order]; $order++?>.php" class="expand button white-btn">QUESTIONS</a>
        </div>
        <div class="small-4 columns">
          <a href="#" class="expand button orange-btn"><p class='ask-bubble'>ASK DOCTOR</p></a>
        </div>
      </div>
<!-- Bread Crumbs -->

    <div class="row">
      <div class="large-12 medium-12 columns">
        <div class='bc-container'>
          <div itemscope itemtype="http://schema.org/MedicalCondition">
             <a href="<?php echo $dataContent[$order]; $order++?>" itemprop="url">
                <span class='bread-crumb' itemprop="indication"  itemtype="http://schema.org/MedicalCondition"><?php echo $dataContent[$order]; $order++?></span>
             </a> 
             ›<a href="<?php echo $dataContent[$order]; $order++?>" itemprop="url">
                <span class='bread-crumb' itemprop="indication" itemtype="http://schema.org/MedicalCondition"><?php echo $dataContent[$order]; $order++?></span>
             </a> 
         </div>
       </div>
     </div>
  </div>
<!-- Bread Crumbs -->

      <div class="row main-row">
        <div class="large-4 medium-4 columns" id='sidebar'>


          <div class="callout panel container side-nav-container">
            <p class='related-title'>CONTENTS</p>
            <ul class="side-nav side-menu" >
              <li class="active"><a href="#description">DESCRIPTION</a></li>
              <li class="divider"></li>
              <li><a href="#causes">PURPOSE</a></li>
              <li class="divider"></li>
              <li><a href="#signs">PROCEDURE</a></li>
              <li class="divider"></li>
              <li><a href="#treatments">RESULTS</a></li>

            </ul>
<!-- Responsive side menu  -->
            <ul class="side-nav menu-min" >
              <li class="active min-description-title"><a >DESCRIPTION</a><div class='min-description min-section' hidden></div></li>
              <li class="divider"></li>
              <li class="min-causes-title"><a>PURPOSE</a><div class='min-causes min-section' hidden></div></li>
              <li class="divider"></li>
               <li class="min-signs-title"><a>PROCEDURE</a><div class='min-signs min-section' hidden></div></li>
              <li class="divider"></li>
              <li class="min-treatments-title"><a>RESULTS</a><div class='min-treatments min-section' hidden></div></li>
            </ul>
        </div>
                <div class="callout panel container related-nav-container ">
          <p class='related-title'>RELATED ARTICLES</p>
           <ul class="side-nav ">
              <li class='treatments-toggle links-list'><a href="#" class='gray-text'><img class='links-icon' width='25' height='25' src='<?php echo $url ?>cms/cms_pages/img/treatment.png'/>SYMPTOMS</a>
                <ul class='treatments-list' hidden>
                  <li ><a href="">Treatment 1 </a></li>
                  <li ><a href="">Treatment 2</a></li>
                </ul>
              </li>
              <li class="divider"></li>
                <li class='related-toggle links-list'><a href="#" class='gray-text'><img class='links-icon' width='25' height='25' src='<?php echo $url ?>cms/cms_pages/img/complications.png'/>CONDITIONS</a>
                <ul class='related-list' hidden>


                    <?php
                              $related = file_get_contents("http://medlanes.com/cms/api/pages/getRelatedPages/".$idPage);
                              $relatedData=json_decode($related,true) ; 
                                foreach ($relatedData as $key => $value){
                                  $pageId[] = $value['idPage'] ;
                                  $pageName[] = $value['namePage'] ;
                                  $pageUrl[] = $value['fileName'] ; 

                               ?>

                                  <li><a href="<?php echo $pageUrl[$key] ?>"><?php echo $pageName[$key] ?></a></li>
                              <?php 
                                  }
                                ?>
                </ul>
              </li>
              <li class="divider"></li>
              <li class='diagnosis-toggle links-list'><a href="#" class='gray-text'><img class='links-icon' width='25' height='25' src='<?php echo $url ?>cms/cms_pages/img/tests.png'/>DIAGNOSIS</a>
                <ul class='diagnosis-list' hidden>
                  <li ><a href="">Diagnosis 1 </a></li>
                  <li ><a href="">Diagnosis 2</a></li>
                </ul>
              </li>
              <li class="divider"></li>
              <li class='risk-factors-toggle links-list'><a href="#" class='gray-text'><img class='links-icon' width='25' height='25' src='<?php echo $url ?>cms/cms_pages/img/tests.png'/>RISK FACTORS</a>
                <ul class='risk-factors-list' hidden>
                  <li ><a href="">Factor 1 </a></li>
                  <li ><a href="">factor 2 </a></li>
                </ul>
              </li>
              <li class='treatments-toggle links-list'><a href="#" class='gray-text'><img class='links-icon' width='25' height='25' src='<?php echo $url ?>cms/cms_pages/img/treatment.png'/>TREATMENTS</a>
                <ul class='treatments-list' hidden>
                  <li ><a href="">Treatment 1 </a></li>
                  <li ><a href="">Treatment 2 </a></li>
                </ul>
              </li>
            </ul>       
        </div>

      </div>
      <div class="large-8 medium-8 columns main-column main-content content-column">
        <div class="callout panel container">
          <h2 id='description'>Description</h2>

                    <div class='description-section'>


          <?php

           for($i=0;$i<20;$i++){
                      if($regionContent[$order]=='description'){ 
                        
                        if($typeContent[$order]=='textarea' && $dataContent[$order]!=''){
                            echo '<p class="gray-text">'.$dataContent[$order].'</p>' ;   
                            $order++; 


                        }
                          else if($typeContent[$order]=='text' && $dataContent[$order]!=''){
                            echo '<h3 class="gray-text">'.$dataContent[$order].'</h3>' ;   
                            $order++; 


                        }

                        else if ($typeContent[$order]=='bullet' && $dataContent[$order]!=''){

                            echo'<ul class="content-bullets">' ;       

                          while($typeContent[$order]=='bullet' && $dataContent[$order]!=''){

                            echo '<li class="blue-text">'.$dataContent[$order].'</li>' ;   
                            $order++; 

                          }

                            echo '</ul>' ; 

                        }

                        else if($dataContent[$order]==''){

                            $order++; 

                        }
                      }
                    }
                ?>

                       </div>  
                   <hr>
      

   <h2 id='causes'>Health Effects</h2>

          <div class='causes-section'>
          <?php

           for($i=0;$i<20;$i++){
                      if($regionContent[$order]=='causes'){ 
                        
                        if($typeContent[$order]=='textarea' && $dataContent[$order]!=''){
                            echo '<p class="gray-text">'.$dataContent[$order].'</p>' ;   
                            $order++; 


                        }
                          else if($typeContent[$order]=='text' && $dataContent[$order]!=''){
                            echo '<h3 class="gray-text">'.$dataContent[$order].'</h3>' ;   
                            $order++; 


                        }

                        else if ($typeContent[$order]=='bullet' && $dataContent[$order]!=''){

                            echo'<ul class="content-bullets">' ;       

                          while($typeContent[$order]=='bullet' && $dataContent[$order]!=''){

                            echo '<li class="blue-text">'.$dataContent[$order].'</li>' ;   
                            $order++; 

                          }

                            echo '</ul>' ; 

                        }

                        else if($dataContent[$order]==''){

                            $order++; 

                        }
                      }
                    }
                ?>

              </div>

                          <hr>
                 
   <h2 id='signs'>Conditions</h2>
             <div class='riskfactors-section'>
          <?php

           for($i=0;$i<20;$i++){
                      if($regionContent[$order]=='signs'){ 
                        
                        if($typeContent[$order]=='textarea' && $dataContent[$order]!=''){
                            echo '<p class="gray-text">'.$dataContent[$order].'</p>' ;   
                            $order++; 


                        }
                          else if($typeContent[$order]=='text' && $dataContent[$order]!=''){
                            echo '<h3 class="gray-text">'.$dataContent[$order].'</h3>' ;   
                            $order++; 


                        }

                        else if ($typeContent[$order]=='bullet' && $dataContent[$order]!=''){

                            echo'<ul class="content-bullets">' ;       

                          while($typeContent[$order]=='bullet' && $dataContent[$order]!=''){

                            echo '<li class="blue-text">'.$dataContent[$order].'</li>' ;   
                            $order++; 

                          }

                            echo '</ul>' ; 

                        }

                        else if($dataContent[$order]==''){

                            $order++; 

                        }
                      }
                    }
                ?>
              </div>

                          <hr>

              
                 <h2 id='treatments'>Prevention</h2>
                             <div class='treatments-section'>
          <?php

           for($i=0;$i<20;$i++){
                      if($regionContent[$order]=='treatment'){ 
                        
                        if($typeContent[$order]=='textarea' && $dataContent[$order]!=''){
                            echo '<p class="gray-text">'.$dataContent[$order].'</p>' ;   
                            $order++; 


                        }
                          else if($typeContent[$order]=='text' && $dataContent[$order]!=''){
                            echo '<h3 class="gray-text">'.$dataContent[$order].'</h3>' ;   
                            $order++; 


                        }

                        else if ($typeContent[$order]=='bullet' && $dataContent[$order]!=''){

                            echo'<ul class="content-bullets">' ;       

                          while($typeContent[$order]=='bullet' && $dataContent[$order]!=''){

                            echo '<li class="blue-text">'.$dataContent[$order].'</li>' ;   
                            $order++; 

                          }

                            echo '</ul>' ; 

                        }

                        else if($dataContent[$order]==''){

                            $order++; 

                        }
                      }
                    }
                ?>

              </div>
               

        </div> 
      </div>
    </div>

    <div class="row main-row">
      <div class="large-4 medium-4 columns main-column">


      </div>
      <div class="large-8 medium-8 columns main-column">

      </div>
    </div>


    <hr />


  </div>
</div>

<script src="<?php echo $url ?>cms/cms_pages/js/foundation.min.js"></script>
<script>
$(document).foundation();
</script>
</body>
</html>
