<?php

require_once('cms_api_call.php') ;
$url = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/';
?>


<!DOCTYPE html>
	<html lang="en">
	<head>
	<meta charset="UTF-8">
	<title><?php echo $dataContent[$order]; $order++ ?></title>
    <link rel="icon" type="image/ico" href="images/favicon.ico">
	<!-- css -->
	<link href='http://fonts.googleapis.com/css?family=Roboto:400,300,300italic,700,700italic,400italic' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="<?php echo $url ?>cms/cms_pages/askadoctor/css/global.css">
	<link rel="stylesheet" href="<?php echo $url ?>cms/cms_pages/askadoctor/css/index.css">
	<link rel="stylesheet" href="<?php echo $url ?>cms/cms_pages/askadoctor/css/tooltipster.css">

	<!-- scripts -->
	<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.2.min.js"></script>
	<script type="text/javascript" src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
	<script type="text/javascript" src="<?php echo $url ?>cms/cms_pages/askadoctor/js/jquery.tooltipster.min.js"></script>
	<script type="text/javascript" src="<?php echo $url ?>cms/cms_pages/askadoctor/js/jquery.bxslider.js"></script>
	<script type="text/javascript" src="<?php echo $url ?>cms/cms_pages/askadoctor/js/us/index.js"></script>

	<!-- jQuery Cookie -->
	<script type="text/javascript" src="<?php echo $url ?>cms/cms_pages/askadoctor/js/jquery.cookie.js"></script>
	<!-- End of jQuery Cookie -->

	<!-- Fingerprint -->
	<script type="text/javascript" src="<?php echo $url ?>cms/cms_pages/askadoctor/js/fingerprint.js"></script>
	<script type="text/javascript" src="<?php echo $url ?>cms/cms_pages/askadoctor/js/common.js"></script>
	<!-- End of Fingerprint -->



<style>

		.mini-wrap-one{
			width: 290px !important;
		}

		.fshow{
			display:none;
			float:left;
			margin-top:13px;
		}
		.fhide{
			display:block;
		}

		.forgot {
			font-size: 14px;
			font-family: 'robotolight', 'Roboto', sans-serif;
			font-weight: 300;
			color: #fff;
			margin-top: 10px;
			text-decoration: none;
			background: none;
			width: auto;
			height: auto;
			padding: 0;
			margin: 0;
			text-indent: 0;
			overflow: auto;
			float: none;
			float: right;
			margin-right: 95px;
			display: block;
		}
</style>

	<style></style>
	<script>
	$(document).ready(function(){

	// Email error close option
		$('.emailerror').click(function(){
			$(this).css('display','none');
		})
	});
	</script> 

</head>
<body>
		<!-- FingerPrint value -->
	<input type="hidden" id="fp" />
	<!-- End of FingerPrint value -->
	<div class="content">
		<div class=" bg-medical" style='background-image:url(<?php echo $url ?>cms/cms_assets/<?php echo $dataContent[$order]; $order++ ?>)!important' alt="<?php echo $dataContent[$order]; $order++ ?>">
		<div class="sub-wrap clearfix">
			
			<div class="left">
				<div class="head">
					<a style="display:block;float:left;" href="#"><img src="<?php echo $url ?>cms/cms_assets/<?php echo $dataContent[$order]; $order++ ?>" alt="<?php echo $dataContent[$order]; $order++ ?>"/>
					</a>
					<h1></h1>
				</div>
			
					<p></p>
	
			</div>
			<div class="right">
				<!--<p><?php//echo $lang['hiw_title']; ?></p>-->
				<div class="one">
					<img src="<?php echo $url ?>cms/cms_pages/askadoctor/images/trust.png" class="pull-left" alt="<?php echo $dataContent[$order]; $order++ ?>" />
					<a href="#" class="btn"><?php echo $dataContent[$order]; $order++ ?></a>
				</div>
			</div>
			<div class="question">
				<h1><?php echo $dataContent[$order]; $order++ ?></h1>
				<div class="choose">
					<h2 class="active"><span id="24 Doctors are Online Now" val="<?php echo $dataContent[$order]; $order++ ?>"><?php echo $dataContent[$order]; $order++ ?></span></h2>
					<h2><span id="5 OB GYNs are Online Now" val="<?php echo $dataContent[$order]; $order++ ?>"><?php echo $dataContent[$order]; $order++ ?></span></h2>
					<h2><span id="4 Psychiatrists and Psychologists are Online Now" val="<?php echo $dataContent[$order]; $order++ ?>"><?php echo $dataContent[$order]; $order++ ?></span></h2>
					<h2><span id="10 Pediatricians are Online Now" val="<?php echo $dataContent[$order]; $order++ ?>"><?php echo $dataContent[$order]; $order++ ?></span></h2>
					<h2><span id="4 Urologists are Online Now" val="<?php echo $dataContent[$order]; $order++ ?>"><?php echo $dataContent[$order]; $order++ ?></span></h2>
					<h2><span id="9 Dermatologists are Online Now" val="<?php echo $dataContent[$order]; $order++ ?>"><?php echo $dataContent[$order]; $order++ ?></span></h2>
					<h2><span id="6 Sexual Health Experts are Online Now" val="<?php echo $dataContent[$order]; $order++ ?>"><?php echo $dataContent[$order]; $order++ ?></span></h2>	
				</div>
				<div class="text">
					<textarea required name="msg" id="msg" cols="30" placeholder="<?php echo $dataContent[$order]; $order++ ?>" rows="10"></textarea>
					<div class="bottom">
						<!--<p class="changetext"></p>-->

						<div class="one">
							<a onclick="backemail()" class="removeclicks" ><?php echo $dataContent[$order]; $order++ ?></a>
							<div class="email">
								<input type="email" name="email" id="email" placeholder="<?php echo $dataContent[$order]; $order++ ?>">
								<p>Email</p>
							</div>
							<div class="z password">
								<input type="password" name="password" id="password" placeholder="Password">
								<p></p>
							</div>

							<div class="emailerror">
							Email is incorrect!
							</div>
							<p class="z spillerror" style="padding-bottom:20px;color:#f3e144"></p> <a class="ctafmp z" href="https://askadoctor.today/user/password-forgot.php">I forgot my password</a>

						</div>
						<!-- <a class="forgot" href="user/password-forgot.php">Forgot password?</a>-->
					</div>
				</div><!-- end text -->
			</div><!-- end question -->
			
        </div>
        </div>
        		<!-- concept known -->

        	<div class="sliders concept-seen-on">
				<div class="one news-one clearfix">
					<div class="box">
						<div class="press-logos">
							<div class="logos-wrap container">
                                <div class="news">
                                   <?php echo $dataContent[$order]; $order++ ?>
                                </div>
                                <div class="logo-block tooltip">
                                	<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid" width="62" height="33" viewBox="0 0 62 33">
                                		<path d="<?php echo $dataContent[$order]; $order++ ?>" />
</svg>

                                	
                                  
                                </div>
                                <div class="logo-block tooltip">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid" width="70" height="33" viewBox="0 0 70 33">	
                                		<path d="<?php echo $dataContent[$order]; $order++ ?>" />
</svg>

                                </div>
                                <div class="logo-block tooltip">
                                    
<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid" width="200" height="28" viewBox="0 0 200 28">

                                		<path d="<?php echo $dataContent[$order]; $order++ ?>" />
</svg>

                                </div>                                
                                <div class="logo-block tooltip">
                                    
<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid" width="40" height="40" viewBox="0 0 40 40">
                                		<path d="<?php echo $dataContent[$order]; $order++ ?>" />
</svg>

                                </div>
                                <div class="logo-block tooltip">
                                    
<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid" width="54" height="40" viewBox="0 0 54 40">
                                		<path d="<?php echo $dataContent[$order]; $order++ ?>" />
</svg>

                                </div>    
                                <div class="logo-block tooltip">
                                    
<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid" width="77" height="33" viewBox="0 0 77 33">
                                		<path d="<?php echo $dataContent[$order]; $order++ ?>" />
</svg>

                                </div>                                    
                                </div>

                            </div>
					</div>
				</div>
				<!-- concept known -->	
            </div>
		<div class="sliders">		
			<div class="one our-doc-one clearfix">
				<h2><?php echo $dataContent[$order]; $order++ ?></h2>
				<div class="box box3">
					<img src="<?php echo $url ?>cms/cms_pages/askadoctor/images/certified-doctor.png" class="certified-doctor" />
				<ul class="bxslider clearfix">
					 <li class="slide">
					 	<div class="media-profile">
						<img src="<?php echo $url ?>cms/cms_assets/<?php echo $dataContent[$order]; $order++ ?>"  alt="<?php echo $dataContent[$order]; $order++ ?>"/>
						<div class="stars">
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="112px" height="106.525px" viewBox="0 0 112 106.525" enable-background="new 0 0 112 106.525" class="icon_s" xml:space="preserve"><path class="star" d="M81.564 67.186l9.043 39.336L56 85.759l-34.607 20.768l9.047-39.336L0 40.684l40.203-3.542L56-0.001l15.797 37.1 L112 40.684L81.564 67.186z"></path></svg>
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="112px" height="106.525px" viewBox="0 0 112 106.525" enable-background="new 0 0 112 106.525" class="icon_s" xml:space="preserve"><path class="star" d="M81.564 67.186l9.043 39.336L56 85.759l-34.607 20.768l9.047-39.336L0 40.684l40.203-3.542L56-0.001l15.797 37.1 L112 40.684L81.564 67.186z"></path></svg>
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="112px" height="106.525px" viewBox="0 0 112 106.525" enable-background="new 0 0 112 106.525" class="icon_s" xml:space="preserve"><path class="star" d="M81.564 67.186l9.043 39.336L56 85.759l-34.607 20.768l9.047-39.336L0 40.684l40.203-3.542L56-0.001l15.797 37.1 L112 40.684L81.564 67.186z"></path></svg>
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="112px" height="106.525px" viewBox="0 0 112 106.525" enable-background="new 0 0 112 106.525" class="icon_s" xml:space="preserve"><path class="star" d="M81.564 67.186l9.043 39.336L56 85.759l-34.607 20.768l9.047-39.336L0 40.684l40.203-3.542L56-0.001l15.797 37.1 L112 40.684L81.564 67.186z"></path></svg>
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="112px" height="106.525px" viewBox="0 0 112 106.525" enable-background="new 0 0 112 106.525" class="icon_s" xml:space="preserve"><path class="star" d="M81.564 67.186l9.043 39.336L56 85.759l-34.607 20.768l9.047-39.336L0 40.684l40.203-3.542L56-0.001l15.797 37.1 L112 40.684L81.564 67.186z"></path></svg>																								
						</div>
						<span><?php echo $dataContent[$order]; $order++ ?></span>
					   </div>
					   <div class="doct-desc">
						<p style="font-weight:bold"><?php echo $dataContent[$order]; $order++ ?></p><br><p><?php echo $dataContent[$order]; $order++ ?></p><br><p><?php echo $dataContent[$order]; $order++ ?> : <b><?php echo $dataContent[$order]; $order++ ?></b></p><br><p><?php echo $dataContent[$order]; $order++ ?></p>			
						</div>	    
					</li>
				  		 <li class="slide">
					 	<div class="media-profile">
						<img src="<?php echo $url ?>cms/cms_assets/<?php echo $dataContent[$order]; $order++ ?>"  alt="<?php echo $dataContent[$order]; $order++ ?>"/>
						<div class="stars">
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="112px" height="106.525px" viewBox="0 0 112 106.525" enable-background="new 0 0 112 106.525" class="icon_s" xml:space="preserve"><path class="star" d="M81.564 67.186l9.043 39.336L56 85.759l-34.607 20.768l9.047-39.336L0 40.684l40.203-3.542L56-0.001l15.797 37.1 L112 40.684L81.564 67.186z"></path></svg>
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="112px" height="106.525px" viewBox="0 0 112 106.525" enable-background="new 0 0 112 106.525" class="icon_s" xml:space="preserve"><path class="star" d="M81.564 67.186l9.043 39.336L56 85.759l-34.607 20.768l9.047-39.336L0 40.684l40.203-3.542L56-0.001l15.797 37.1 L112 40.684L81.564 67.186z"></path></svg>
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="112px" height="106.525px" viewBox="0 0 112 106.525" enable-background="new 0 0 112 106.525" class="icon_s" xml:space="preserve"><path class="star" d="M81.564 67.186l9.043 39.336L56 85.759l-34.607 20.768l9.047-39.336L0 40.684l40.203-3.542L56-0.001l15.797 37.1 L112 40.684L81.564 67.186z"></path></svg>
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="112px" height="106.525px" viewBox="0 0 112 106.525" enable-background="new 0 0 112 106.525" class="icon_s" xml:space="preserve"><path class="star" d="M81.564 67.186l9.043 39.336L56 85.759l-34.607 20.768l9.047-39.336L0 40.684l40.203-3.542L56-0.001l15.797 37.1 L112 40.684L81.564 67.186z"></path></svg>
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="112px" height="106.525px" viewBox="0 0 112 106.525" enable-background="new 0 0 112 106.525" class="icon_s" xml:space="preserve"><path class="star" d="M81.564 67.186l9.043 39.336L56 85.759l-34.607 20.768l9.047-39.336L0 40.684l40.203-3.542L56-0.001l15.797 37.1 L112 40.684L81.564 67.186z"></path></svg>																								
						</div>
						<span><?php echo $dataContent[$order]; $order++ ?></span>
					   </div>
					   <div class="doct-desc">
						<p style="font-weight:bold"><?php echo $dataContent[$order]; $order++ ?></p><br><p><?php echo $dataContent[$order]; $order++ ?></p><br><p><?php echo $dataContent[$order]; $order++ ?> : <b><?php echo $dataContent[$order]; $order++ ?></b></p><br><p><?php echo $dataContent[$order]; $order++ ?></p>			
						</div>	    
					</li>
				 		 <li class="slide">
					 	<div class="media-profile">
						<img src="<?php echo $url ?>cms/cms_assets/<?php echo $dataContent[$order]; $order++ ?>"  alt="<?php echo $dataContent[$order]; $order++ ?>"/>
						<div class="stars">
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="112px" height="106.525px" viewBox="0 0 112 106.525" enable-background="new 0 0 112 106.525" class="icon_s" xml:space="preserve"><path class="star" d="M81.564 67.186l9.043 39.336L56 85.759l-34.607 20.768l9.047-39.336L0 40.684l40.203-3.542L56-0.001l15.797 37.1 L112 40.684L81.564 67.186z"></path></svg>
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="112px" height="106.525px" viewBox="0 0 112 106.525" enable-background="new 0 0 112 106.525" class="icon_s" xml:space="preserve"><path class="star" d="M81.564 67.186l9.043 39.336L56 85.759l-34.607 20.768l9.047-39.336L0 40.684l40.203-3.542L56-0.001l15.797 37.1 L112 40.684L81.564 67.186z"></path></svg>
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="112px" height="106.525px" viewBox="0 0 112 106.525" enable-background="new 0 0 112 106.525" class="icon_s" xml:space="preserve"><path class="star" d="M81.564 67.186l9.043 39.336L56 85.759l-34.607 20.768l9.047-39.336L0 40.684l40.203-3.542L56-0.001l15.797 37.1 L112 40.684L81.564 67.186z"></path></svg>
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="112px" height="106.525px" viewBox="0 0 112 106.525" enable-background="new 0 0 112 106.525" class="icon_s" xml:space="preserve"><path class="star" d="M81.564 67.186l9.043 39.336L56 85.759l-34.607 20.768l9.047-39.336L0 40.684l40.203-3.542L56-0.001l15.797 37.1 L112 40.684L81.564 67.186z"></path></svg>
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="112px" height="106.525px" viewBox="0 0 112 106.525" enable-background="new 0 0 112 106.525" class="icon_s" xml:space="preserve"><path class="star" d="M81.564 67.186l9.043 39.336L56 85.759l-34.607 20.768l9.047-39.336L0 40.684l40.203-3.542L56-0.001l15.797 37.1 L112 40.684L81.564 67.186z"></path></svg>																								
						</div>
						<span><?php echo $dataContent[$order]; $order++ ?></span>
					   </div>
					   <div class="doct-desc">
						<p style="font-weight:bold"><?php echo $dataContent[$order]; $order++ ?></p><br><p><?php echo $dataContent[$order]; $order++ ?></p><br><p><?php echo $dataContent[$order]; $order++ ?> : <b><?php echo $dataContent[$order]; $order++ ?></b></p><br><p><?php echo $dataContent[$order]; $order++ ?></p>			
						</div>	    
					</li>
				   		 <li class="slide">
					 	<div class="media-profile">
						<img src="<?php echo $url ?>cms/cms_assets/<?php echo $dataContent[$order]; $order++ ?>"  alt="<?php echo $dataContent[$order]; $order++ ?>"/>
						<div class="stars">
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="112px" height="106.525px" viewBox="0 0 112 106.525" enable-background="new 0 0 112 106.525" class="icon_s" xml:space="preserve"><path class="star" d="M81.564 67.186l9.043 39.336L56 85.759l-34.607 20.768l9.047-39.336L0 40.684l40.203-3.542L56-0.001l15.797 37.1 L112 40.684L81.564 67.186z"></path></svg>
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="112px" height="106.525px" viewBox="0 0 112 106.525" enable-background="new 0 0 112 106.525" class="icon_s" xml:space="preserve"><path class="star" d="M81.564 67.186l9.043 39.336L56 85.759l-34.607 20.768l9.047-39.336L0 40.684l40.203-3.542L56-0.001l15.797 37.1 L112 40.684L81.564 67.186z"></path></svg>
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="112px" height="106.525px" viewBox="0 0 112 106.525" enable-background="new 0 0 112 106.525" class="icon_s" xml:space="preserve"><path class="star" d="M81.564 67.186l9.043 39.336L56 85.759l-34.607 20.768l9.047-39.336L0 40.684l40.203-3.542L56-0.001l15.797 37.1 L112 40.684L81.564 67.186z"></path></svg>
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="112px" height="106.525px" viewBox="0 0 112 106.525" enable-background="new 0 0 112 106.525" class="icon_s" xml:space="preserve"><path class="star" d="M81.564 67.186l9.043 39.336L56 85.759l-34.607 20.768l9.047-39.336L0 40.684l40.203-3.542L56-0.001l15.797 37.1 L112 40.684L81.564 67.186z"></path></svg>
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="112px" height="106.525px" viewBox="0 0 112 106.525" enable-background="new 0 0 112 106.525" class="icon_s" xml:space="preserve"><path class="star" d="M81.564 67.186l9.043 39.336L56 85.759l-34.607 20.768l9.047-39.336L0 40.684l40.203-3.542L56-0.001l15.797 37.1 L112 40.684L81.564 67.186z"></path></svg>																								
						</div>
						<span><?php echo $dataContent[$order]; $order++ ?></span>
					   </div>
					   <div class="doct-desc">
						<p style="font-weight:bold"><?php echo $dataContent[$order]; $order++ ?></p><br><p><?php echo $dataContent[$order]; $order++ ?></p><br><p><?php echo $dataContent[$order]; $order++ ?> : <b><?php echo $dataContent[$order]; $order++ ?></b></p><br><p><?php echo $dataContent[$order]; $order++ ?></p>			
						</div>	    
					</li>
				   		 <li class="slide">
					 	<div class="media-profile">
						<img src="<?php echo $url ?>cms/cms_assets/<?php echo $dataContent[$order]; $order++ ?>"  alt="<?php echo $dataContent[$order]; $order++ ?>"/>
						<div class="stars">
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="112px" height="106.525px" viewBox="0 0 112 106.525" enable-background="new 0 0 112 106.525" class="icon_s" xml:space="preserve"><path class="star" d="M81.564 67.186l9.043 39.336L56 85.759l-34.607 20.768l9.047-39.336L0 40.684l40.203-3.542L56-0.001l15.797 37.1 L112 40.684L81.564 67.186z"></path></svg>
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="112px" height="106.525px" viewBox="0 0 112 106.525" enable-background="new 0 0 112 106.525" class="icon_s" xml:space="preserve"><path class="star" d="M81.564 67.186l9.043 39.336L56 85.759l-34.607 20.768l9.047-39.336L0 40.684l40.203-3.542L56-0.001l15.797 37.1 L112 40.684L81.564 67.186z"></path></svg>
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="112px" height="106.525px" viewBox="0 0 112 106.525" enable-background="new 0 0 112 106.525" class="icon_s" xml:space="preserve"><path class="star" d="M81.564 67.186l9.043 39.336L56 85.759l-34.607 20.768l9.047-39.336L0 40.684l40.203-3.542L56-0.001l15.797 37.1 L112 40.684L81.564 67.186z"></path></svg>
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="112px" height="106.525px" viewBox="0 0 112 106.525" enable-background="new 0 0 112 106.525" class="icon_s" xml:space="preserve"><path class="star" d="M81.564 67.186l9.043 39.336L56 85.759l-34.607 20.768l9.047-39.336L0 40.684l40.203-3.542L56-0.001l15.797 37.1 L112 40.684L81.564 67.186z"></path></svg>
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="112px" height="106.525px" viewBox="0 0 112 106.525" enable-background="new 0 0 112 106.525" class="icon_s" xml:space="preserve"><path class="star" d="M81.564 67.186l9.043 39.336L56 85.759l-34.607 20.768l9.047-39.336L0 40.684l40.203-3.542L56-0.001l15.797 37.1 L112 40.684L81.564 67.186z"></path></svg>																								
						</div>
						<span><?php echo $dataContent[$order]; $order++ ?></span>
					   </div>
					   <div class="doct-desc">
						<p style="font-weight:bold"><?php echo $dataContent[$order]; $order++ ?></p><br><p><?php echo $dataContent[$order]; $order++ ?></p><br><p><?php echo $dataContent[$order]; $order++ ?> : <b><?php echo $dataContent[$order]; $order++ ?></b></p><br><p><?php echo $dataContent[$order]; $order++ ?></p>			
						</div>	    
					</li>
				   		 <li class="slide">
					 	<div class="media-profile">
						<img src="<?php echo $url ?>cms/cms_assets/<?php echo $dataContent[$order]; $order++ ?>"  alt="<?php echo $dataContent[$order]; $order++ ?>"/>
						<div class="stars">
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="112px" height="106.525px" viewBox="0 0 112 106.525" enable-background="new 0 0 112 106.525" class="icon_s" xml:space="preserve"><path class="star" d="M81.564 67.186l9.043 39.336L56 85.759l-34.607 20.768l9.047-39.336L0 40.684l40.203-3.542L56-0.001l15.797 37.1 L112 40.684L81.564 67.186z"></path></svg>
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="112px" height="106.525px" viewBox="0 0 112 106.525" enable-background="new 0 0 112 106.525" class="icon_s" xml:space="preserve"><path class="star" d="M81.564 67.186l9.043 39.336L56 85.759l-34.607 20.768l9.047-39.336L0 40.684l40.203-3.542L56-0.001l15.797 37.1 L112 40.684L81.564 67.186z"></path></svg>
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="112px" height="106.525px" viewBox="0 0 112 106.525" enable-background="new 0 0 112 106.525" class="icon_s" xml:space="preserve"><path class="star" d="M81.564 67.186l9.043 39.336L56 85.759l-34.607 20.768l9.047-39.336L0 40.684l40.203-3.542L56-0.001l15.797 37.1 L112 40.684L81.564 67.186z"></path></svg>
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="112px" height="106.525px" viewBox="0 0 112 106.525" enable-background="new 0 0 112 106.525" class="icon_s" xml:space="preserve"><path class="star" d="M81.564 67.186l9.043 39.336L56 85.759l-34.607 20.768l9.047-39.336L0 40.684l40.203-3.542L56-0.001l15.797 37.1 L112 40.684L81.564 67.186z"></path></svg>
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="112px" height="106.525px" viewBox="0 0 112 106.525" enable-background="new 0 0 112 106.525" class="icon_s" xml:space="preserve"><path class="star" d="M81.564 67.186l9.043 39.336L56 85.759l-34.607 20.768l9.047-39.336L0 40.684l40.203-3.542L56-0.001l15.797 37.1 L112 40.684L81.564 67.186z"></path></svg>																								
						</div>
						<span><?php echo $dataContent[$order]; $order++ ?></span>
					   </div>
					   <div class="doct-desc">
						<p style="font-weight:bold"><?php echo $dataContent[$order]; $order++ ?></p><br><p><?php echo $dataContent[$order]; $order++ ?></p><br><p><?php echo $dataContent[$order]; $order++ ?> : <b><?php echo $dataContent[$order]; $order++ ?></b></p><br><p><?php echo $dataContent[$order]; $order++ ?></p>			
						</div>	    
					</li>
				  		 <li class="slide">
					 	<div class="media-profile">
						<img src="<?php echo $url ?>cms/cms_assets/<?php echo $dataContent[$order]; $order++ ?>"  alt="<?php echo $dataContent[$order]; $order++ ?>"/>
						<div class="stars">
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="112px" height="106.525px" viewBox="0 0 112 106.525" enable-background="new 0 0 112 106.525" class="icon_s" xml:space="preserve"><path class="star" d="M81.564 67.186l9.043 39.336L56 85.759l-34.607 20.768l9.047-39.336L0 40.684l40.203-3.542L56-0.001l15.797 37.1 L112 40.684L81.564 67.186z"></path></svg>
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="112px" height="106.525px" viewBox="0 0 112 106.525" enable-background="new 0 0 112 106.525" class="icon_s" xml:space="preserve"><path class="star" d="M81.564 67.186l9.043 39.336L56 85.759l-34.607 20.768l9.047-39.336L0 40.684l40.203-3.542L56-0.001l15.797 37.1 L112 40.684L81.564 67.186z"></path></svg>
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="112px" height="106.525px" viewBox="0 0 112 106.525" enable-background="new 0 0 112 106.525" class="icon_s" xml:space="preserve"><path class="star" d="M81.564 67.186l9.043 39.336L56 85.759l-34.607 20.768l9.047-39.336L0 40.684l40.203-3.542L56-0.001l15.797 37.1 L112 40.684L81.564 67.186z"></path></svg>
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="112px" height="106.525px" viewBox="0 0 112 106.525" enable-background="new 0 0 112 106.525" class="icon_s" xml:space="preserve"><path class="star" d="M81.564 67.186l9.043 39.336L56 85.759l-34.607 20.768l9.047-39.336L0 40.684l40.203-3.542L56-0.001l15.797 37.1 L112 40.684L81.564 67.186z"></path></svg>
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="112px" height="106.525px" viewBox="0 0 112 106.525" enable-background="new 0 0 112 106.525" class="icon_s" xml:space="preserve"><path class="star" d="M81.564 67.186l9.043 39.336L56 85.759l-34.607 20.768l9.047-39.336L0 40.684l40.203-3.542L56-0.001l15.797 37.1 L112 40.684L81.564 67.186z"></path></svg>																								
						</div>
						<span><?php echo $dataContent[$order]; $order++ ?></span>
					   </div>
					   <div class="doct-desc">
						<p style="font-weight:bold"><?php echo $dataContent[$order]; $order++ ?></p><br><p><?php echo $dataContent[$order]; $order++ ?></p><br><p><?php echo $dataContent[$order]; $order++ ?> : <b><?php echo $dataContent[$order]; $order++ ?></b></p><br><p><?php echo $dataContent[$order]; $order++ ?></p>			
						</div>	    
					</li>
				 		 <li class="slide">
					 	<div class="media-profile">
						<img src="<?php echo $url ?>cms/cms_assets/<?php echo $dataContent[$order]; $order++ ?>"  alt="<?php echo $dataContent[$order]; $order++ ?>"/>
						<div class="stars">
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="112px" height="106.525px" viewBox="0 0 112 106.525" enable-background="new 0 0 112 106.525" class="icon_s" xml:space="preserve"><path class="star" d="M81.564 67.186l9.043 39.336L56 85.759l-34.607 20.768l9.047-39.336L0 40.684l40.203-3.542L56-0.001l15.797 37.1 L112 40.684L81.564 67.186z"></path></svg>
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="112px" height="106.525px" viewBox="0 0 112 106.525" enable-background="new 0 0 112 106.525" class="icon_s" xml:space="preserve"><path class="star" d="M81.564 67.186l9.043 39.336L56 85.759l-34.607 20.768l9.047-39.336L0 40.684l40.203-3.542L56-0.001l15.797 37.1 L112 40.684L81.564 67.186z"></path></svg>
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="112px" height="106.525px" viewBox="0 0 112 106.525" enable-background="new 0 0 112 106.525" class="icon_s" xml:space="preserve"><path class="star" d="M81.564 67.186l9.043 39.336L56 85.759l-34.607 20.768l9.047-39.336L0 40.684l40.203-3.542L56-0.001l15.797 37.1 L112 40.684L81.564 67.186z"></path></svg>
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="112px" height="106.525px" viewBox="0 0 112 106.525" enable-background="new 0 0 112 106.525" class="icon_s" xml:space="preserve"><path class="star" d="M81.564 67.186l9.043 39.336L56 85.759l-34.607 20.768l9.047-39.336L0 40.684l40.203-3.542L56-0.001l15.797 37.1 L112 40.684L81.564 67.186z"></path></svg>
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="112px" height="106.525px" viewBox="0 0 112 106.525" enable-background="new 0 0 112 106.525" class="icon_s" xml:space="preserve"><path class="star" d="M81.564 67.186l9.043 39.336L56 85.759l-34.607 20.768l9.047-39.336L0 40.684l40.203-3.542L56-0.001l15.797 37.1 L112 40.684L81.564 67.186z"></path></svg>																								
						</div>
						<span> <?php echo $dataContent[$order]; $order++ ?></span>
					   </div>
					   <div class="doct-desc">
						<p style="font-weight:bold"><?php echo $dataContent[$order]; $order++ ?></p><br><p><?php echo $dataContent[$order]; $order++ ?></p><br><p><?php echo $dataContent[$order]; $order++ ?> : <b><?php echo $dataContent[$order]; $order++ ?></b></p><br><p><?php echo $dataContent[$order]; $order++ ?></p>			
						</div>	    
					</li>
				 </ul>
				</div>
			</div>					
			<div class="one customer-testimonials">
				<h2><?php echo $dataContent[$order]; $order++ ?></h2>
				<div class="box midbox">
				<ul class="bxslider">
					<li class="testimonial">
						<img src="<?php echo $url ?>cms/cms_assets/<?php echo $dataContent[$order]; $order++ ?>" alt="<?php echo $dataContent[$order]; $order++ ?>">
						<div class="customer-content">
							<p><?php echo $dataContent[$order]; $order++ ?></p><p style="padding:0px 10px"><?php echo $dataContent[$order]; $order++ ?></p><p style="padding:0px 10px"><?php echo $dataContent[$order]; $order++ ?></p>				
							<a href=""><?php echo $dataContent[$order]; $order++ ?></a>
						</div>
					</li>
						<li class="testimonial">
						<img src="<?php echo $url ?>cms/cms_assets/<?php echo $dataContent[$order]; $order++ ?>" alt="<?php echo $dataContent[$order]; $order++ ?>">
						<div class="customer-content">
							<p><?php echo $dataContent[$order]; $order++ ?></p><p style="padding:0px 10px"><?php echo $dataContent[$order]; $order++ ?></p><p style="padding:0px 10px"><?php echo $dataContent[$order]; $order++ ?></p>				
							<a href=""><?php echo $dataContent[$order]; $order++ ?></a>
						</div>
					</li>
						<li class="testimonial">
						<img src="<?php echo $url ?>cms/cms_assets/<?php echo $dataContent[$order]; $order++ ?>" alt="<?php echo $dataContent[$order]; $order++ ?>">
						<div class="customer-content">
							<p><?php echo $dataContent[$order]; $order++ ?></p><p style="padding:0px 10px"><?php echo $dataContent[$order]; $order++ ?></p><p style="padding:0px 10px"><?php echo $dataContent[$order]; $order++ ?></p>				
							<a href=""><?php echo $dataContent[$order]; $order++ ?></a>
						</div>
					</li>
						<li class="testimonial">
						<img src="<?php echo $url ?>cms/cms_assets/<?php echo $dataContent[$order]; $order++ ?>" alt="<?php echo $dataContent[$order]; $order++ ?>">
						<div class="customer-content">
							<p><?php echo $dataContent[$order]; $order++ ?></p><p style="padding:0px 10px"><?php echo $dataContent[$order]; $order++ ?></p><p style="padding:0px 10px"><?php echo $dataContent[$order]; $order++ ?></p>				
							<a href=""><?php echo $dataContent[$order]; $order++ ?></a>
						</div>
					</li>
				
				</ul>
				</div>
			</div>
		</div>

	</div>
<footer>
<div class="sub-wrap">
	<div class="mini-wrap-two clearfix">
		<div class="one two">
			<ul>
				<li><a href="<?php echo $dataContent[$order]; $order++ ?>"><?php echo $dataContent[$order]; $order++ ?></a></li>
				<li><a href="<?php echo $dataContent[$order]; $order++ ?>"><?php echo $dataContent[$order]; $order++ ?></a></li>
				<li><a href="<?php echo $dataContent[$order]; $order++ ?>"><?php echo $dataContent[$order]; $order++ ?></a></li>
			</ul>		
		</div>
		<div class="one two">
			<ul>
				<li><a target="_blank" href="<?php echo $dataContent[$order]; $order++ ?>"><?php echo $dataContent[$order]; $order++ ?></a></li>
				<li><a target="_blank" href="<?php echo $dataContent[$order]; $order++ ?>"><?php echo $dataContent[$order]; $order++ ?></a></li>
			</ul>								
		</div>
		<div class="support">
			<ul>
			<li><?php echo $dataContent[$order]; $order++ ?></li>
			<li><?php echo $dataContent[$order]; $order++ ?></li>
			</ul>
		</div>

		<div class="one">
			<img src="<?php echo $url ?>cms/cms_pages/askadoctor/images/supported-by.png" class="supported" />
		</div>
	</div>

</footer>


<script type="text/javascript">
(function(d, src, c) { var t=d.scripts[d.scripts.length - 1],s=d.createElement('script');s.id='la_x2s6df8d';s.async=true;s.src=src;s.onload=s.onreadystatechange=function(){var rs=this.readyState;if(rs&&(rs!='complete')&&(rs!='loaded')){return;}c(this);};t.parentElement.insertBefore(s,t.nextSibling);})(document,
'//medlanes.ladesk.com/scripts/track.js',
function(e){ LiveAgent.createButton('e022f5bc', e); });
</script>


</body>
</html>


