
      <div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title">Header</h3>
  </div>
  <div class="panel-body" >
    <form class="form-horizontal">
          <cms-input cms-input-id='heading_text_1' cms-input-label="Page Title MetaTag"    cms-input-region="heading" ></cms-input>
          <cms-picture cms-input-id='heading_picture_2' cms-input-label="Logo Image"    cms-input-region="heading" ></cms-picture>
          <cms-input cms-input-id='heading_text_3' cms-input-label="Logo Title"   cms-input-default='Ask a medical question and get a fast answer'  cms-input-region="heading" ></cms-input>

</div>
</div>
  </form>


      <div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title">Steps</h3>
  </div>
  <div class="panel-body" >
    <form class="form-horizontal">
          <cms-input cms-input-id='steps_text_4' cms-input-label="Ask a Question Label"    cms-input-region="steps" ></cms-input>
          <cms-input cms-input-id='steps_text_5' cms-input-label="Additional Information Label"    cms-input-region="steps" ></cms-input>
          <cms-input cms-input-id='steps_text_6' cms-input-label="Your Options Label"    cms-input-region="steps" ></cms-input>
          <cms-input cms-input-id='steps_text_7' cms-input-label="Get an answer Label"    cms-input-region="steps" ></cms-input>

         
</div>
  </form>


      <div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title">Page Core</h3>
  </div>
  <div class="panel-body" >
    <form class="form-horizontal">
          <cms-input cms-input-id='core_text_8' cms-input-label="Confirm your appointment with a Doctor"    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_9' cms-input-label="Place your deposit of"    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_10' cms-input-label="Amount"    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_11' cms-input-label="Currency"    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_12' cms-input-label="and get your answer within minutes!"    cms-input-region="core" ></cms-input>

         <cms-picture cms-input-id='core_picture_13' cms-input-label="Doctor Image"    cms-input-region="core" ></cms-picture>
          <cms-input cms-input-id='core_text_14' cms-input-label="Doctor Name"    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_15' cms-input-label="Speciality"    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_16' cms-input-label="Satisfaction"    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_17' cms-input-label="Online Now !"    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_18' cms-input-label="You have an appointement"    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_19' cms-input-label="Your doctor is waiting"    cms-input-region="core" ></cms-input>

          <cms-input cms-input-id='core_text_20' cms-input-label="Deposit Method"    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_21' cms-input-label="Credit Card "    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_22' cms-input-label="You will be redirected to PayPal"    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_23' cms-input-label="Card Number"    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_24' cms-input-label="Expiration Date "    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_25' cms-input-label="Month "    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_26' cms-input-label="Year "    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_27' cms-input-label="Security Code"    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_28' cms-input-label=" Where to find your security code"    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_29' cms-input-label="For your protection, Medlanes requires you..."    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_30' cms-input-label=" The security code is a 3-digit number found on the back of your card, to the right of the signature panel."    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_31' cms-input-label=" The security code is a 4-digit number found on the front of your card to the right of the card number."    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_32' cms-input-label="Your information is secure."    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_33' cms-input-label="Privacy Link"    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_34' cms-input-label="Privacy Label"    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_35' cms-input-label="By clicking "Continue" you indicate that you agree to the "    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_36' cms-input-label="Terms Link"    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_37' cms-input-label="Terms Label"    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_38' cms-input-label="and are 18+ years old"    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_40' cms-input-label="Checkout Now Button"    cms-input-region="core" ></cms-input>
          <cms-picture cms-input-id='core_picture_41' cms-input-label="Satisfaction guarantee Image"    cms-input-region="core" ></cms-picture>
          <cms-input cms-input-id='core_text_42' cms-input-label="Satisfaction guarantee Text"    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_43' cms-input-label="Satisfaction guarantee No question added"    cms-input-region="core" ></cms-input>

</div>
</div>
  </form>


      
    
 <div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title">Footer</h3>
  </div>
  <div class="panel-body" >
    <form class="form-horizontal">
          <cms-input cms-input-id='footer_text_107' cms-input-label="Medlanes Link"    cms-input-region="footer" ></cms-input>
          <cms-input cms-input-id='footer_text_108' cms-input-label="Medlanes Label"    cms-input-region="footer" ></cms-input>
          <cms-input cms-input-id='footer_text_109' cms-input-label="Support Link"    cms-input-region="footer" ></cms-input>
          <cms-input cms-input-id='footer_text_110' cms-input-label="Support Label"    cms-input-region="footer" ></cms-input>

          <cms-input cms-input-id='footer_text_111' cms-input-label="Contact us Link"    cms-input-region="footer" ></cms-input>
          <cms-input cms-input-id='footer_text_112' cms-input-label="Contact us Label"    cms-input-region="footer" ></cms-input>

          <cms-input cms-input-id='footer_text_113' cms-input-label="terms Link"    cms-input-region="footer" ></cms-input>
          <cms-input cms-input-id='footer_text_114' cms-input-label="terms Label"    cms-input-region="footer" ></cms-input>

          <cms-input cms-input-id='footer_text_115' cms-input-label="Privacy Link"    cms-input-region="footer" ></cms-input>
          <cms-input cms-input-id='footer_text_116' cms-input-label="Privacy Label"    cms-input-region="footer" ></cms-input>

          <cms-input cms-input-id='footer_text_117' cms-input-label="Copyright Text"    cms-input-region="footer" ></cms-input>
          <cms-input cms-input-id='footer_text_118' cms-input-label="Ask a doctor supported by text"    cms-input-region="footer" ></cms-input>


</div>
</div>
  </form>


                 
                                                                                                      