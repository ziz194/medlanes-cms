<?php

require_once('cms_api_call.php') ;

?>

<?php include_once 'languages/languageperch.php' ?>
<?php  include_once 'functions.php'; ?>
<!DOCTYPE html>
 <?php include("_scripts.php"); ?>
  <?php $page = 'landingpage2'; ?> 
<!DOCTYPE html>

                         
  <link rel="alternate" hreflang="de" href="<?php echo $dataContent[$order]; $order++ ?>" />
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(n,t,e){function r(e){if(!t[e]){var o=t[e]={exports:{}};n[e][0].call(o.exports,function(t){var o=n[e][1][t];return r(o?o:t)},o,o.exports)}return t[e].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<e.length;o++)r(e[o]);return r}({QJf3ax:[function(n,t){function e(n){function t(t,e,a){n&&n(t,e,a),a||(a={});for(var u=c(t),f=u.length,s=i(a,o,r),p=0;f>p;p++)u[p].apply(s,e);return s}function a(n,t){f[n]=c(n).concat(t)}function c(n){return f[n]||[]}function u(){return e(t)}var f={};return{on:a,emit:t,create:u,listeners:c,_events:f}}function r(){return{}}var o="nr@context",i=n("gos");t.exports=e()},{gos:"7eSDFh"}],ee:[function(n,t){t.exports=n("QJf3ax")},{}],gos:[function(n,t){t.exports=n("7eSDFh")},{}],"7eSDFh":[function(n,t){function e(n,t,e){if(r.call(n,t))return n[t];var o=e();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(n,t,{value:o,writable:!0,enumerable:!1}),o}catch(i){}return n[t]=o,o}var r=Object.prototype.hasOwnProperty;t.exports=e},{}],D5DuLP:[function(n,t){function e(n,t,e){return r.listeners(n).length?r.emit(n,t,e):(o[n]||(o[n]=[]),void o[n].push(t))}var r=n("ee").create(),o={};t.exports=e,e.ee=r,r.q=o},{ee:"QJf3ax"}],handle:[function(n,t){t.exports=n("D5DuLP")},{}],XL7HBI:[function(n,t){function e(n){var t=typeof n;return!n||"object"!==t&&"function"!==t?-1:n===window?0:i(n,o,function(){return r++})}var r=1,o="nr@id",i=n("gos");t.exports=e},{gos:"7eSDFh"}],id:[function(n,t){t.exports=n("XL7HBI")},{}],loader:[function(n,t){t.exports=n("G9z0Bl")},{}],G9z0Bl:[function(n,t){function e(){var n=v.info=NREUM.info;if(n&&n.licenseKey&&n.applicationID&&f&&f.body){c(d,function(t,e){t in n||(n[t]=e)}),v.proto="https"===l.split(":")[0]||n.sslForHttp?"https://":"http://",a("mark",["onload",i()]);var t=f.createElement("script");t.src=v.proto+n.agent,f.body.appendChild(t)}}function r(){"complete"===f.readyState&&o()}function o(){a("mark",["domContent",i()])}function i(){return(new Date).getTime()}var a=n("handle"),c=n(1),u=window,f=u.document,s="addEventListener",p="attachEvent",l=(""+location).split("?")[0],d={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-515.min.js"},v=t.exports={offset:i(),origin:l,features:{}};f[s]?(f[s]("DOMContentLoaded",o,!1),u[s]("load",e,!1)):(f[p]("onreadystatechange",r),u[p]("onload",e)),a("mark",["firstbyte",i()])},{1:11,handle:"D5DuLP"}],11:[function(n,t){function e(n,t){var e=[],o="",i=0;for(o in n)r.call(n,o)&&(e[i]=t(o,n[o]),i+=1);return e}var r=Object.prototype.hasOwnProperty;t.exports=e},{}]},{},["G9z0Bl"]);</script>
  <title><?php echo $dataContent[$order]; $order++ ?></title>
  <meta name="description" content="<?php echo $dataContent[$order]; $order++ ?>">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta property="og:title" content="<?php echo $dataContent[$order]; $order++ ?>"/>
  <meta property="og:type" content="article"/>
  <meta property="og:image" content="images/17/checkk.png"/>
  <meta property="og:url" content="<?php echo $dataContent[$order]; $order++ ?>"/>
  <meta property="og:description" content="<?php echo $dataContent[$order]; $order++ ?>"/>
  <meta name="twitter:card" content="summary"/>
  <meta name="twitter:url" content="<?php echo $dataContent[$order]; $order++ ?>"/>
  <meta name="twitter:title" content="<?php echo $dataContent[$order]; $order++ ?>"/>
  <meta name="twitter:description" content="<?php echo $dataContent[$order]; $order++ ?>"/>
  <meta name="twitter:image" content="images/17/checkk.png"/>
<html>
<head>
   
  <!-- scripts -->

    <link rel="stylesheet" type="text/css" href="css/font/fonts.css"/>
<script type="text/javascript" src="js/modernizr.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
<link rel="stylesheet" type="text/css" href="css/jquery-ui-1.10.4.custom.css"/>
<link rel="stylesheet" type="text/css" href="css/global1.css"/>
<script type="text/javascript" src="js/script.js"></script>
<link rel="stylesheet" type="text/css" href="addons/animate.css" />
<script src="addons/viewportchecker.js"></script>
<script type="text/javascript" src="js/min/jquery-ui-1.10.4.custom.min.js"></script>
<script language="JavaScript" src="http://www.geoplugin.net/javascript.gp" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="css/fresh.css"/>
  <script type="text/javascript" src="css/source/jquery.fancybox.js?v=2.1.5"></script>
  <script type="text/javascript" src="css/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
  <script src="js/jquery.bxslider.min.js"></script>
  
<link rel="stylesheet" type="text/css" href="css/portal.css" />
  <link href="css/header.css" rel="stylesheet" />
     <link href="css/footer.css" rel="stylesheet" />
           <link rel="stylesheet" type="text/css" href="css/global.css"/>

</head>
<body>
   <div class="fullwrap">
    <?php include("header.php"); ?>
  </div> 
  <div class="content_lo">
  <div itemscope itemtype="http://schema.org/MedicalCondition">
  <div class="content-full-wrap">
  <div id="MainContainer">
       <div class="h17" style="background:url(../cms_assets/<?php echo $dataContent[$order]; $order++ ?>)no-repeat center center;">
      <div class="smwrp">
      <div class="one">
            <h1><span itemprop="name"><?php echo $dataContent[$order]; $order++ ?></span></h1>
          <span itemprop="code" itemscope itemtype="http://schema.org/MedicalCode">
          <meta itemprop="code" content=""/>
          <meta itemprop="codingSystem" content="ICD-9"/>
             </span>
          <p id="slider_text"><span itemprop="description" itemscope itemtype="http://schema.org/MedicalCondition"><br>
           <span style='font-size:16px ;float:right !important; margin-right: 5px;'><?php echo $dataContent[$order]; $order++ ?></span></span></p>   
      </div></div>
    </div>
    <div class="b17">
      <div class="in17">
<div class="content">


  
<div class='bc-container'>
        <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
  <a class='bread-crumb' href="<?php echo $dataContent[$order]; $order++ ?>" itemprop="url">
    <span itemprop="title"><?php echo $dataContent[$order]; $order++ ?></span>
  </a> ›<a class='bread-crumb' href="<?php echo $dataContent[$order]; $order++ ?>" itemprop="url">
    <span itemprop="title"><?php echo $dataContent[$order]; $order++ ?></span>
  </a> 

</div>

</div>
   <section>
          <h2 id="portal_header">Übersicht</h2>
          <p><span itemprop="description" ><?php echo $dataContent[$order]; $order++ ?></span></p>
          </section>
          <section>
          <h2 id="twogo">Häufigste Krankheiten</h2>
          <div class="conditions_wrapper"> 
      <div class="top_condition">
      <a style="text-decoration:none; color:#13315e;" href="<?php echo $dataContent[$order]; $order++ ?>"> <h3><?php echo $dataContent[$order]; $order++ ?></h3></a>
      <img width="100px" height="100px" src="../cms_assets/<?php echo $dataContent[$order]; $order++ ?>" alt="Allergic Reaction" />
                 <p><?php echo $dataContent[$order]; $order++ ?>
 <small><a href="<?php echo $dataContent[$order]; $order++ ?>">Mehr »</a></small>
 </p></div> 
    

    

    <div class="top_condition">
      <a style="text-decoration:none; color:#13315e;" href="<?php echo $dataContent[$order]; $order++ ?>"> <h3><?php echo $dataContent[$order]; $order++ ?></h3></a>



      <img width="100px" height="100px" src="../cms_assets/<?php echo $dataContent[$order]; $order++ ?>" alt="Allergic Reaction" />
                 <p><?php echo $dataContent[$order]; $order++ ?>
 <small><a href="<?php echo $dataContent[$order]; $order++ ?>">Mehr »</a></small>
 </p></div> 
    
    

    

    <div class="top_condition">
      <a style="text-decoration:none; color:#13315e;" href="<?php echo $dataContent[$order]; $order++ ?>"> <h3><?php echo $dataContent[$order]; $order++ ?></h3></a>



      <img width="100px" height="100px" src="../cms_assets/<?php echo $dataContent[$order]; $order++ ?>" alt="Allergic Reaction" />
                 <p><?php echo $dataContent[$order]; $order++ ?>
 <small><a href="<?php echo $dataContent[$order]; $order++ ?>">Mehr »</a></small>
 </p></div> 
    

    

    <div class="top_condition">
      <a style="text-decoration:none; color:#13315e;" href="<?php echo $dataContent[$order]; $order++ ?>"> <h3><?php echo $dataContent[$order]; $order++ ?></h3></a>



      <img width="100px" height="100px" src="../cms_assets/<?php echo $dataContent[$order]; $order++ ?>" alt="Allergic Reaction" />
                 <p><?php echo $dataContent[$order]; $order++ ?>
 <small><a href="<?php echo $dataContent[$order]; $order++ ?>">Mehr »</a></small>
 </p></div> 
    
      </div>


            
      
  
            </section>



          <section>
            <!-- h2 --><h2 id="threego">Krankheiten A-Z</h2>

<div class="content-full-wrap">


<div class="symptoms" >


    <div class="inpwrp">


         <ul >

        <?php  
      while (($typeContent[$order]=='bullet') && ($regionContent[$order]=='conditions1' ) ) 
           {
         ?>

         <?php if ($dataContent[$order]=='') {

           $order++; 

          }


         else 

         {
          ?>
         
       <li><a href="<?php  if ($dataContent!=''){echo $dataContent[$order];} $order++ ;?>"><?php  if ($dataContent!=''){echo $dataContent[$order];} $order++ ;?></a></li>

           <?php

         }
              } 

           ?>
    </ul>        





         <ul >

          <?php  
      while (($typeContent[$order]=='bullet') && ($regionContent[$order]=='conditions2' ) ) 
           {
         ?>

         <?php if ($dataContent[$order]=='') {

           $order++; 

          }


        
         else 

         {
          ?>
         
       <li><a href="<?php  if ($dataContent!=''){echo $dataContent[$order];} $order++ ;?>"><?php  if ($dataContent!=''){echo $dataContent[$order];} $order++ ;?></a></li>

           <?php

         }
              } 

           ?>

    </ul>



         <ul >

        <?php  
      while (($typeContent[$order]=='bullet') && ($regionContent[$order]=='conditions3' ) ) 
           {
         ?>

         <?php if ($dataContent[$order]=='') {

           $order++; 

          }


         
         else 

         {
          ?>
         
       <li><a href="<?php  if ($dataContent!=''){echo $dataContent[$order];} $order++ ;?>"><?php  if ($dataContent!=''){echo $dataContent[$order];} $order++ ;?></a></li>

           <?php

         }
              } 

           ?>

    </ul>

             <ul >

        <?php  
      while (($typeContent[$order]=='bullet') && ($regionContent[$order]=='conditions4' )) 
           {
         ?>

         <?php if ($dataContent[$order]=='') {

           $order++; 

          }


         
         else 

         {
          ?>
         
       <li><a href="<?php  if ($dataContent!=''){echo $dataContent[$order];} $order++ ;?>"><?php  if ($dataContent!=''){echo $dataContent[$order];} $order++ ;?></a></li>

           <?php

         }
              } 

           ?>

    </ul>




    </div>
</div>


        
            </section> 


<div class='full_sidebar' >

<div class='links_sidebar'>


  <div class = 'links_container_shadow'>





    <div class = 'link_container related'>

  <span ><img class ='sidebar_icon' width='35px' height='35px' src='images/related_conditions.png' alt="Related Conditions"/>Verwandte Erkrankungen</span>
  </div>




    
         <ul  hidden class='related_list'  >

           <?php  
      while (($typeContent[$order]=='bullet')) 
           {
         ?>

         <?php if ($dataContent[$order]=='') {

           $order++; 

          }


          else if (($regionContent[$order]!='related' ))

          { 

          $order = $order ;

        }
         else 

         {
          ?>
         
       <li><img style='margin-right:8px; 'src="./images/serv/dotarrow2.png" alt="Arrow" /><a href="<?php  if ($dataContent!=''){echo $dataContent[$order];} $order++ ;?>"><?php  if ($dataContent!=''){echo $dataContent[$order];} $order++ ;?></a></li>

           <?php

         }
              } 

           ?>

    </ul>





  <div class = 'link_container risks'>


<span > <img class ='sidebar_icon' width='35px' height='35px' src='images/risk_factor.png' alt="Risk Factors" />Risikofaktoren</span>
  </div>



    
         <ul  hidden class='risks_list'  >

         
           <?php  
      while (($typeContent[$order]=='bullet')) 
           {
         ?>

         <?php if ($dataContent[$order]=='') {

           $order++; 

          }


          else if (($regionContent[$order]!='risks' ))

          { 

          $order = $order ;

        }
         else 

         {
          ?>
         
       <li><img style='margin-right:8px; 'src="./images/serv/dotarrow2.png" alt="Arrow" /><a href="<?php  if ($dataContent!=''){echo $dataContent[$order];} $order++ ;?>"><?php  if ($dataContent!=''){echo $dataContent[$order];} $order++ ;?></a></li>

           <?php

         }
              } 

           ?>

    </ul>




  <div class = 'link_container tests'>
  <span ><img class ='sidebar_icon' width='35px' height='35px' src='images/diagnosis.png' alt="Diagnosis"  />Tests</span>



</div>



         <ul  hidden class='tests_list'  >

        
           <?php  
      while (($typeContent[$order]=='bullet')) 
           {
         ?>

         <?php if ($dataContent[$order]=='') {

           $order++; 

          }


          else if (($regionContent[$order]!='test' ))

          { 

          $order = $order ;

        }
         else 

         {
          ?>
         
       <li><img style='margin-right:8px; 'src="./images/serv/dotarrow2.png" alt="Arrow" /><a href="<?php  if ($dataContent!=''){echo $dataContent[$order];} $order++ ;?>"><?php  if ($dataContent!=''){echo $dataContent[$order];} $order++ ;?></a></li>

           <?php

         }
              } 

           ?>
    </ul>






  <div class = 'link_container treatments'>

    <span> <img class ='sidebar_icon' width='35px' height='35px' src='images/treatments.png' alt="Treatments" />Behandlungen</span>
    </div>



    
         <ul  hidden class='treatments_list'  >

        
           <?php  
      while (($typeContent[$order]=='bullet')) 
           {
         ?>

         <?php if ($dataContent[$order]=='') {

           $order++; 

          }


          else if (($regionContent[$order]!='treatments' ))

          { 

          $order = $order ;

        }
         else 

         {
          ?>
         
       <li><img style='margin-right:8px; 'src="./images/serv/dotarrow2.png" alt="Arrow" /><a href="<?php  if ($dataContent!=''){echo $dataContent[$order];} $order++ ;?>"><?php  if ($dataContent!=''){echo $dataContent[$order];} $order++ ;?></a></li>

           <?php

         }
              } 

           ?>

    </ul>



    






</div>

</div>
   
   <?php include("sidebar.php"); ?>

  </div> <!-- end full_sidebar -->


           </div><!-- end content -->

  </div> 


         <!-- end big wrap -->
</div>
</div>
</div>
<?php include("footer.php"); ?>
<script type="text/javascript">
    $(document).ready(function(){
      $('.related').on('click',function(){
        $('.related_list').slideToggle() ; 
                $('.risks_list').slideUp() ; 
                        $('.treatments_list').slideUp() ; 
                                $('.tests_list').slideUp() ; 

      });
           $('.risks').on('click',function(){
        $('.risks_list').slideToggle() ; 
                  $('.related_list').slideUp() ; 
                        $('.treatments_list').slideUp() ; 
                                $('.tests_list').slideUp() ; 
              });
           $('.treatments').on('click',function(){
        $('.treatments_list').slideToggle() ; 
                 $('.risks_list').slideUp() ; 
                        $('.realted_list').slideUp() ; 
                                $('.tests_list').slideUp() ; 


              });
           $('.tests').on('click',function(){
        $('.tests_list').slideToggle() ;
                  $('.risks_list').slideUp() ; 
                        $('.treatments_list').slideUp() ; 
                                $('.related_list').slideUp() ;  
      });
  });
  </script>  
</body>
</html>