<?php

require_once('cms_api_call.php') ;

?>

<!doctype html>
<html  lang="en">
<head>


   <link rel="alternate" hreflang="de" href="<?php echo $dataContent[$order]; $order++ ?>" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(n,t,e){function r(e){if(!t[e]){var o=t[e]={exports:{}};n[e][0].call(o.exports,function(t){var o=n[e][1][t];return r(o?o:t)},o,o.exports)}return t[e].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<e.length;o++)r(e[o]);return r}({QJf3ax:[function(n,t){function e(n){function t(t,e,a){n&&n(t,e,a),a||(a={});for(var u=c(t),f=u.length,s=i(a,o,r),p=0;f>p;p++)u[p].apply(s,e);return s}function a(n,t){f[n]=c(n).concat(t)}function c(n){return f[n]||[]}function u(){return e(t)}var f={};return{on:a,emit:t,create:u,listeners:c,_events:f}}function r(){return{}}var o="nr@context",i=n("gos");t.exports=e()},{gos:"7eSDFh"}],ee:[function(n,t){t.exports=n("QJf3ax")},{}],gos:[function(n,t){t.exports=n("7eSDFh")},{}],"7eSDFh":[function(n,t){function e(n,t,e){if(r.call(n,t))return n[t];var o=e();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(n,t,{value:o,writable:!0,enumerable:!1}),o}catch(i){}return n[t]=o,o}var r=Object.prototype.hasOwnProperty;t.exports=e},{}],D5DuLP:[function(n,t){function e(n,t,e){return r.listeners(n).length?r.emit(n,t,e):(o[n]||(o[n]=[]),void o[n].push(t))}var r=n("ee").create(),o={};t.exports=e,e.ee=r,r.q=o},{ee:"QJf3ax"}],handle:[function(n,t){t.exports=n("D5DuLP")},{}],XL7HBI:[function(n,t){function e(n){var t=typeof n;return!n||"object"!==t&&"function"!==t?-1:n===window?0:i(n,o,function(){return r++})}var r=1,o="nr@id",i=n("gos");t.exports=e},{gos:"7eSDFh"}],id:[function(n,t){t.exports=n("XL7HBI")},{}],loader:[function(n,t){t.exports=n("G9z0Bl")},{}],G9z0Bl:[function(n,t){function e(){var n=v.info=NREUM.info;if(n&&n.licenseKey&&n.applicationID&&f&&f.body){c(d,function(t,e){t in n||(n[t]=e)}),v.proto="https"===l.split(":")[0]||n.sslForHttp?"https://":"http://",a("mark",["onload",i()]);var t=f.createElement("script");t.src=v.proto+n.agent,f.body.appendChild(t)}}function r(){"complete"===f.readyState&&o()}function o(){a("mark",["domContent",i()])}function i(){return(new Date).getTime()}var a=n("handle"),c=n(1),u=window,f=u.document,s="addEventListener",p="attachEvent",l=(""+location).split("?")[0],d={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-515.min.js"},v=t.exports={offset:i(),origin:l,features:{}};f[s]?(f[s]("DOMContentLoaded",o,!1),u[s]("load",e,!1)):(f[p]("onreadystatechange",r),u[p]("onload",e)),a("mark",["firstbyte",i()])},{1:11,handle:"D5DuLP"}],11:[function(n,t){function e(n,t){var e=[],o="",i=0;for(o in n)r.call(n,o)&&(e[i]=t(o,n[o]),i+=1);return e}var r=Object.prototype.hasOwnProperty;t.exports=e},{}]},{},["G9z0Bl"]);</script>
  <title><?php echo $dataContent[$order]; $order++ ?></title>
  <meta name="description" content="<?php echo $dataContent[$order]; $order++ ?>">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta property="og:title" content="<?php echo $dataContent[$order]; $order++ ?>"/>
  <meta property="og:type" content="article"/>
  <meta property="og:image" content="images/17/checkk.png"/>
  <meta property="og:url" content="<?php echo $dataContent[$order]; $order++ ?>"/>
  <meta property="og:description" content="<?php echo $dataContent[$order]; $order++ ?>"/>
  <meta name="twitter:card" content="summary"/>
  <meta name="twitter:url" content="<?php echo $dataContent[$order]; $order++ ?>"/>
  <meta name="twitter:title" content="<?php echo $dataContent[$order]; $order++ ?>"/>
  <meta name="twitter:description" content="<?php echo $dataContent[$order]; $order++ ?>"/>
  <meta name="twitter:image" content="images/17/checkk.png"/>

  <link rel="shortcut icon" type="image/x-icon" href="http://medlanes.com/medlanescms/cms_pages/favicon.ico">
  <!-- Styles -->
  <link rel="stylesheet" type="text/css" href="http://medlanes.com/medlanescms/cms_pages/css/content2.css">


  <link rel="stylesheet" href="http://medlanes.com/medlanescms/cms_pages/css/foundation.css" />
  <!-- jQuery  -->
  <script type="text/javascript" src="http://medlanes.com/medlanescms/cms_pages/js/jquery-1.11.2.min.js"></script>
  <!-- Scripts -->
  <script src="js/jquery.autosize.min.js"></script>
  <!-- jQuery UI -->
  <link rel="stylesheet" type="text/css" href="css/jquery-ui.min.css">
  <script type="text/javascript" src="http://medlanes.com/medlanescms/cms_pages/js/jquery-ui.min.js"></script>
  <script type="text/javascript" src='http://medlanes.com/medlanescms/cms_pages/js/pagescript.js'></script>
  <script type="text/javascript" src='http://medlanes.com/medlanescms/cms_pages/js/jquery.sticky-kit.min.js'></script>


</script>
</head>
<body>

  <div class="row">
    <div class="large-12 columns">
            <?php include("header2.php"); ?>

    </div>
  </div>

  <div class="row">
    <div class="large-12 columns">
            <div class="heading" style='background-image:url(http://medlanes.com/medlanescms/cms_assets/<?php echo $dataContent[$order]; $order++ ?>)'>
                  <h1 class='page-title'><?php echo $dataContent[$order]; ?></h1>
                  <!-- Mobile title -->
                  <h1 class='page-title-min'><span><?php echo $dataContent[$order]; $order++?></span></h1>

            </div>  
    </div>
  </div>
  <div class="row ">
    <div class="large-12 medium-12 columns">

      <div class="row main-row">
        <div class="small-4 columns main-column">
          <a href="#" class="active-button expand button blue-btn">LERNEN</a>
        </div>
        <div class="small-4 columns main-column">
          <a href="<?php echo $dataContent[$order]; $order++?>.php" class="expand button blue-btn">FRAGEN</a>
        </div>
        <div class="small-4 columns main-column">
          <a href="#" class="expand button orange-btn">ARZT FRAGEN</a>
        </div>
      </div>
<!-- Bread Crumbs -->

    <div class="row">
      <div class="large-12 medium-12 columns">
        <div class='bc-container'>
          <div itemscope itemtype="http://schema.org/MedicalCondition">
             <a href="<?php echo $dataContent[$order]; $order++?>" itemprop="url">
                <span class='bread-crumb' itemprop="indication"  itemtype="http://schema.org/MedicalCondition"><?php echo $dataContent[$order]; $order++?></span>
             </a> 
             ›<a href="<?php echo $dataContent[$order]; $order++?>" itemprop="url">
                <span class='bread-crumb' itemprop="indication" itemtype="http://schema.org/MedicalCondition"><?php echo $dataContent[$order]; $order++?></span>
             </a> 
         </div>
       </div>
     </div>
  </div>
<!-- Bread Crumbs -->

      <div class="row main-row">
        <div class="large-4 medium-4 columns main-column" id='sidebar'>

        <div class="callout panel container ">
           <ul class="side-nav">
              <li class='links-list'><a href="#" class='gray-text'><img class='links-icon' width='25' height='25' src='http://medlanes.com/medlanescms/cms_pages/img/treatment.png'/>BEHANDLUNGEN</a></li>
              <li class="divider"></li>
              <li class='links-list'><a href="#" class='gray-text'><img class='links-icon' width='25' height='25' src='http://medlanes.com/medlanescms/cms_pages/img/complications.png'/>VERWANDTE ARTIKELN</a></li>
              <li class="divider"></li>
              <li class='links-list'><a href="#" class='gray-text'><img class='links-icon' width='25' height='25'src='http://medlanes.com/medlanescms/cms_pages/img/tests.png'/>DIAGNOSEN</a></li>


            </ul>       
        </div>
          <div class="callout panel container no-padding">
            <ul class="side-nav side-menu" >
              <li class="active"><a href="#description" class='gray-text'>BESCHREIBUNG</a></li>
              <li class="divider"></li>
              <li><a href="#signs" class='gray-text'>ANZEICHEN & SYMPTOME</a></li>
              <li class="divider"></li>
              <li><a href="#causes" class='gray-text'>URSACHEN</a></li>
              <li class="divider"></li>
              <li><a href="#riskfactors" class='gray-text'>RISIKOFAKTOREN</a></li>
              <li class="divider"></li>
              <li><a href="#complications" class='gray-text'>KOMPLIKATIONEN</a></li>
              <li class="divider"></li>
              <li><a href="#diagnosis" class='gray-text'>DIAGNOSE</a></li>
              <li class="divider"></li>
              <li><a href="#treatments" class='gray-text'>BEHANDLUNG</a></li>
            </ul>
<!-- Responsive side menu  -->
            <ul class="side-nav menu-min no-padding" >
              <li class="active min-description-title"><a class='gray-text'>BESCHREIBUNG</a><div class='min-description min-section' hidden></div></li>
              <li class="divider"></li>
              <li class="min-signs-title"><a  class='gray-text'>ANZEICHEN & SYMPTOME</a><div class='min-signs min-section' hidden></div></li>
              <li class="divider"></li>
              <li class="min-causes-title"><a  class='gray-text'>URSACHEN</a><div class='min-causes min-section' hidden></div></li>
              <li class="divider"></li>
              <li class="min-riskfactors-title"><a  class='gray-text'>RISIKOFAKTOREN</a><div class='min-riskfactors min-section' hidden></div></li>
              <li class="divider"></li>
              <li class="min-complications-title"><a  class='gray-text'>KOMPLIKATIONEN</a><div class='min-complications min-section' hidden></div></li>
              <li class="divider"></li>
              <li class="min-diagnosis-title"><a class='gray-text'>DIAGNOSE</a><div class='min-diagnosis min-section' hidden></div></li>
              <li class="divider"></li>
              <li class="min-treatments-title"><a  class='gray-text'>BEHANDLUNG</a><div class='min-treatments min-section' hidden></div></li>
            </ul>
        </div>

      </div>
      <div class="large-8 medium-8 columns main-column main-content content-column">
        <div class="callout panel container">
          <h2 id='description'>Beschreibung</h2>

                    <div class='description-section'>


          <?php

           for($i=0;$i<20;$i++){
                      if($regionContent[$order]=='description'){ 
                        
                        if($typeContent[$order]=='textarea' && $dataContent[$order]!=''){
                            echo '<p class="gray-text">'.$dataContent[$order].'</p>' ;   
                            $order++; 


                        }

                        else if ($typeContent[$order]=='bullet' && $dataContent[$order]!=''){

                            echo'<ul class="content-bullets">' ;       

                          while($typeContent[$order]=='bullet' && $dataContent[$order]!=''){

                            echo '<li class="blue-text">'.$dataContent[$order].'</li>' ;   
                            $order++; 

                          }

                            echo '</ul>' ; 

                        }

                        else if($dataContent[$order]==''){

                            $order++; 

                        }
                      }
                    }
                ?>

                       </div>  
                   <hr>
      

      <h2 id='signs'>Anzeichen & Symptome</h2>
          <div class='signs-section'>


          <?php

           for($i=0;$i<20;$i++){
                      if($regionContent[$order]=='signs'){ 
                        
                        if($typeContent[$order]=='textarea' && $dataContent[$order]!=''){
                            echo '<p class="gray-text">'.$dataContent[$order].'</p>' ;   
                            $order++; 


                        }

                        else if ($typeContent[$order]=='bullet' && $dataContent[$order]!=''){

                            echo'<ul class="content-bullets">' ;       

                          while($typeContent[$order]=='bullet' && $dataContent[$order]!=''){

                            echo '<li class="blue-text">'.$dataContent[$order].'</li>' ;   
                            $order++; 

                          }

                            echo '</ul>' ; 

                        }

                        else if($dataContent[$order]==''){

                            $order++; 

                        }
                      }
                    }
                ?>
                           </div>
                          <hr>

   <h2 id='causes'>Ursachen</h2>

          <div class='causes-section'>
          <?php

           for($i=0;$i<20;$i++){
                      if($regionContent[$order]=='causes'){ 
                        
                        if($typeContent[$order]=='textarea' && $dataContent[$order]!=''){
                            echo '<p class="gray-text">'.$dataContent[$order].'</p>' ;   
                            $order++; 


                        }

                        else if ($typeContent[$order]=='bullet' && $dataContent[$order]!=''){

                            echo'<ul class="content-bullets">' ;       

                          while($typeContent[$order]=='bullet' && $dataContent[$order]!=''){

                            echo '<li class="blue-text">'.$dataContent[$order].'</li>' ;   
                            $order++; 

                          }

                            echo '</ul>' ; 

                        }

                        else if($dataContent[$order]==''){

                            $order++; 

                        }
                      }
                    }
                ?>

              </div>

                          <hr>


                  
   <h2 id='riskfactors'>Risikofaktoren</h2>
             <div class='riskfactors-section'>
          <?php

           for($i=0;$i<20;$i++){
                      if($regionContent[$order]=='riskfactors'){ 
                        
                        if($typeContent[$order]=='textarea' && $dataContent[$order]!=''){
                            echo '<p class="gray-text">'.$dataContent[$order].'</p>' ;   
                            $order++; 


                        }

                        else if ($typeContent[$order]=='bullet' && $dataContent[$order]!=''){

                            echo'<ul class="content-bullets">' ;       

                          while($typeContent[$order]=='bullet' && $dataContent[$order]!=''){

                            echo '<li class="blue-text">'.$dataContent[$order].'</li>' ;   
                            $order++; 

                          }

                            echo '</ul>' ; 

                        }

                        else if($dataContent[$order]==''){

                            $order++; 

                        }
                      }
                    }
                ?>
              </div>

                          <hr>

                             <h2 id='complications'>Komplikationen</h2>
             <div class='complications-section'>
          <?php

           for($i=0;$i<20;$i++){
                      if($regionContent[$order]=='complications'){ 
                        
                        if($typeContent[$order]=='textarea' && $dataContent[$order]!=''){
                            echo '<p class="gray-text">'.$dataContent[$order].'</p>' ;   
                            $order++; 


                        }

                        else if ($typeContent[$order]=='bullet' && $dataContent[$order]!=''){

                            echo'<ul class="content-bullets">' ;       

                          while($typeContent[$order]=='bullet' && $dataContent[$order]!=''){

                            echo '<li class="blue-text">'.$dataContent[$order].'</li>' ;   
                            $order++; 

                          }

                            echo '</ul>' ; 

                        }

                        else if($dataContent[$order]==''){

                            $order++; 

                        }
                      }
                    }
                ?>
              </div>

                          <hr>

                  
   <h2 id='diagnosis'>Diagnose</h2>
             <div class='diagnosis-section'>
          <?php

           for($i=0;$i<20;$i++){
                      if($regionContent[$order]=='diagnosis'){ 
                        
                        if($typeContent[$order]=='textarea' && $dataContent[$order]!=''){
                            echo '<p class="gray-text">'.$dataContent[$order].'</p>' ;   
                            $order++; 


                        }

                        else if ($typeContent[$order]=='bullet' && $dataContent[$order]!=''){

                            echo'<ul class="content-bullets">' ;       

                          while($typeContent[$order]=='bullet' && $dataContent[$order]!=''){

                            echo '<li class="blue-text">'.$dataContent[$order].'</li>' ;   
                            $order++; 

                          }

                            echo '</ul>' ; 

                        }

                        else if($dataContent[$order]==''){

                            $order++; 

                        }
                      }
                    }
                ?>
              </div>
                   <hr>

                   <h2 id='treatments'>Behandlung</h2>
                             <div class='treatments-section'>
          <?php

           for($i=0;$i<20;$i++){
                      if($regionContent[$order]=='treatment'){ 
                        
                        if($typeContent[$order]=='textarea' && $dataContent[$order]!=''){
                            echo '<p class="gray-text">'.$dataContent[$order].'</p>' ;   
                            $order++; 


                        }

                        else if ($typeContent[$order]=='bullet' && $dataContent[$order]!=''){

                            echo'<ul class="content-bullets">' ;       

                          while($typeContent[$order]=='bullet' && $dataContent[$order]!=''){

                            echo '<li class="blue-text">'.$dataContent[$order].'</li>' ;   
                            $order++; 

                          }

                            echo '</ul>' ; 

                        }

                        else if($dataContent[$order]==''){

                            $order++; 

                        }
                      }
                    }
                ?>

              </div>

               <hr>

        </div> 
      </div>
    </div>

    <div class="row main-row">
      <div class="large-4 medium-4 columns main-column">


      </div>
      <div class="large-8 medium-8 columns main-column">

      </div>
    </div>


    <hr />


  </div>
</div>

<script src="http://medlanes.com/medlanescms/cms_pages/js/foundation.min.js"></script>
<script>
$(document).foundation();
</script>
</body>
</html>
