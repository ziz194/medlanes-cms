<?php

require_once('cms_api_call.php') ;

?>
<?php include_once 'languages/languageperch.php' ?>
<?php  include_once 'functions.php'; ?>
<!DOCTYPE html>
 <?php include("_scripts.php"); ?>
  <?php $page = 'landingpage2'; ?> 

<html>
<head>
    <?php $page = 'landingpage2'; ?> 

    
                <link rel="alternate" hreflang="de" href="<?php echo $dataContent[$order]; $order++ ?>" />

  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(n,t,e){function r(e){if(!t[e]){var o=t[e]={exports:{}};n[e][0].call(o.exports,function(t){var o=n[e][1][t];return r(o?o:t)},o,o.exports)}return t[e].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<e.length;o++)r(e[o]);return r}({QJf3ax:[function(n,t){function e(n){function t(t,e,a){n&&n(t,e,a),a||(a={});for(var u=c(t),f=u.length,s=i(a,o,r),p=0;f>p;p++)u[p].apply(s,e);return s}function a(n,t){f[n]=c(n).concat(t)}function c(n){return f[n]||[]}function u(){return e(t)}var f={};return{on:a,emit:t,create:u,listeners:c,_events:f}}function r(){return{}}var o="nr@context",i=n("gos");t.exports=e()},{gos:"7eSDFh"}],ee:[function(n,t){t.exports=n("QJf3ax")},{}],gos:[function(n,t){t.exports=n("7eSDFh")},{}],"7eSDFh":[function(n,t){function e(n,t,e){if(r.call(n,t))return n[t];var o=e();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(n,t,{value:o,writable:!0,enumerable:!1}),o}catch(i){}return n[t]=o,o}var r=Object.prototype.hasOwnProperty;t.exports=e},{}],D5DuLP:[function(n,t){function e(n,t,e){return r.listeners(n).length?r.emit(n,t,e):(o[n]||(o[n]=[]),void o[n].push(t))}var r=n("ee").create(),o={};t.exports=e,e.ee=r,r.q=o},{ee:"QJf3ax"}],handle:[function(n,t){t.exports=n("D5DuLP")},{}],XL7HBI:[function(n,t){function e(n){var t=typeof n;return!n||"object"!==t&&"function"!==t?-1:n===window?0:i(n,o,function(){return r++})}var r=1,o="nr@id",i=n("gos");t.exports=e},{gos:"7eSDFh"}],id:[function(n,t){t.exports=n("XL7HBI")},{}],loader:[function(n,t){t.exports=n("G9z0Bl")},{}],G9z0Bl:[function(n,t){function e(){var n=v.info=NREUM.info;if(n&&n.licenseKey&&n.applicationID&&f&&f.body){c(d,function(t,e){t in n||(n[t]=e)}),v.proto="https"===l.split(":")[0]||n.sslForHttp?"https://":"http://",a("mark",["onload",i()]);var t=f.createElement("script");t.src=v.proto+n.agent,f.body.appendChild(t)}}function r(){"complete"===f.readyState&&o()}function o(){a("mark",["domContent",i()])}function i(){return(new Date).getTime()}var a=n("handle"),c=n(1),u=window,f=u.document,s="addEventListener",p="attachEvent",l=(""+location).split("?")[0],d={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-515.min.js"},v=t.exports={offset:i(),origin:l,features:{}};f[s]?(f[s]("DOMContentLoaded",o,!1),u[s]("load",e,!1)):(f[p]("onreadystatechange",r),u[p]("onload",e)),a("mark",["firstbyte",i()])},{1:11,handle:"D5DuLP"}],11:[function(n,t){function e(n,t){var e=[],o="",i=0;for(o in n)r.call(n,o)&&(e[i]=t(o,n[o]),i+=1);return e}var r=Object.prototype.hasOwnProperty;t.exports=e},{}]},{},["G9z0Bl"]);</script>
    <title><?php echo $dataContent[$order]; $order++ ?></title>

  <meta name="description" content="<?php echo $dataContent[$order]; $order++ ?>">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta property="og:title" content="<?php echo $dataContent[$order]; $order++ ?>"/>
    <meta property="og:type" content="article"/>
    <meta property="og:image" content="images/17/checkk.png"/>
  <meta property="og:url" content="<?php echo $dataContent[$order]; $order++ ?>"/>
  <meta property="og:description" content="<?php echo $dataContent[$order]; $order++ ?>"/>
  <meta name="twitter:card" content="summary"/>
  <meta name="twitter:url" content="<?php echo $dataContent[$order]; $order++ ?>"/>
  <meta name="twitter:title" content="<?php echo $dataContent[$order]; $order++ ?>"/>
  <meta name="twitter:description" content="<?php echo $dataContent[$order]; $order++ ?>"/>
  <meta name="twitter:image" content="images/17/checkk.png"/>


  <!-- scripts -->

  <link rel="stylesheet" type="text/css" href="css/font/fonts.css"/>
<script type="text/javascript" src="js/modernizr.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
<link rel="stylesheet" type="text/css" href="css/jquery-ui-1.10.4.custom.css"/>
<link rel="stylesheet" type="text/css" href="css/global1.css"/>
<script type="text/javascript" src="js/script.js"></script>
<link rel="stylesheet" type="text/css" href="addons/animate.css" />
<script src="addons/viewportchecker.js"></script>
<script type="text/javascript" src="js/min/jquery-ui-1.10.4.custom.min.js"></script>
<script language="JavaScript" src="http://www.geoplugin.net/javascript.gp" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="css/fresh.css"/>
  <script type="text/javascript" src="js/jquery.jsticky.js"></script>


  <script type="text/javascript" src="css/source/jquery.fancybox.js?v=2.1.5"></script>
  <script type="text/javascript" src="css/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
  <script src="js/jquery.bxslider.min.js"></script>




<link href="css/jquery.bxslider.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="css/source/jquery.fancybox.css?v=2.1.5" media="screen" />
  <link rel="stylesheet" type="text/css" href="css/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" />
<link rel="stylesheet" type="text/css" href="css/content.css" >



           <link rel="stylesheet" type="text/css" href="css/global.css"/>










</head>
<body>
  <div class="fullwrap">
      <?php include("header.php"); ?>

  <div class="content_lo">
  <div itemscope itemtype="http://schema.org/MedicalCondition">
  <div class="content-full-wrap">
  <div id="MainContainer">

       <div class="h17" style="background:url(../cms_assets/<?php echo $dataContent[$order]; $order++ ?>)no-repeat center center;">
      <div class="smwrp">
      <div class="one">
            <h1><span itemprop="name"><?php echo $dataContent[$order]; $order++ ?></span></h1>
          <span itemprop="code" itemscope itemtype="http://schema.org/MedicalCode">
          <meta itemprop="code" content=""/>
          <meta itemprop="codingSystem" content="ICD-9"/>
             </span>
          <p id="slider_text"><?php echo $dataContent[$order]; $order++ ?><span itemprop="description" itemscope itemtype="http://schema.org/MedicalCondition"><br><br> 
           <span style='font-size:16px ;float:right !important; margin-right: 5px;'></span></span></p>   
      </div></div>
    </div>


      <div class="subm">
  <ul>
<li id="one" >    <a  style="text-decoration:none;color:white;" href="#description">Beschreibung </a> </li>
    <li id="two"> <a style="text-decoration:none;color:white;"href="#signs">Anzeichen  &amp; Symptome </a> </li>
    <li id="three"><a style="text-decoration:none;color:white;"href="#causes">Ursachen</a></li>
        <li id="three"><a style="text-decoration:none;color:white;"href="#complications">Komplikationen</a></li>
    <li id="four"><a style="text-decoration:none;color:white;"href="#diagnosis">Diagnose </a></li>
        <li id="five"><a style="text-decoration:none;color:white;"href="#treatment">Behandlung</a></li>
    
  </ul>
</div>
    <div class="b17">
      <div class="in17">


<div class="content">
     <?php include("sidebar.php"); ?>   
<div class='bc-container'>
        <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
  <a class='bread-crumb' href="<?php echo $dataContent[$order]; $order++ ?>" itemprop="url">
    <span itemprop="title"><?php echo $dataContent[$order]; $order++ ?></span>
  </a> ›<a class='bread-crumb' href="<?php echo $dataContent[$order]; $order++ ?>" itemprop="url">
    <span itemprop="title"><?php echo $dataContent[$order]; $order++ ?></span>
  </a> 

</div>

</div>

       
    <section>

          <h2 id="description" style="margin-top: 25px;">Beschreibung</h2>

 <?php 

                    for($i=0;$i<20;$i++){
                      if($regionContent[$order]=='description'){ 
                        
                        if($typeContent[$order]=='textarea' && $dataContent[$order]!=''){
                            echo '<p>'.$dataContent[$order].'</p>' ;   
                            $order++; 


                        }

                        else if ($typeContent[$order]=='bullet' && $dataContent[$order]!=''){

                            echo '<div class="bullets">' ;
                            echo'<ul>' ;       

                          while($typeContent[$order]=='bullet' && $dataContent[$order]!=''){

                            echo '<li>'.$dataContent[$order].'</li>' ;   
                            $order++; 

                          }

                            echo '</ul>' ; 
                            echo '</div>' ; 

                        }

                        else if($dataContent[$order]==''){

                            $order++; 

                        }
                      }
                    }
                ?>

                   
          </section>
          <section>
             <h2 id="signs" style="margin-top: 25px;">Anzeichen & Symptome</h2>
             <?php 

                    for($i=0;$i<20;$i++){
                      if($regionContent[$order]=='signs'){ 


                       

                        if($typeContent[$order]=='textarea' && $dataContent[$order]!=''){
                            echo '<p>'.$dataContent[$order].'</p>' ;   
                            $order++; 


                        }

                        else if ($typeContent[$order]=='bullet' && $dataContent[$order]!=''){

                            echo '<div class="bullets">' ;
                            echo'<ul>' ;       

                          while($typeContent[$order]=='bullet' && $dataContent[$order]!=''){

                            echo '<li>'.$dataContent[$order].'</li>' ;   
                            $order++; 

                          }

                            echo '</ul>' ; 
                            echo '</div>' ; 

                        }

                        else if($dataContent[$order]==''){

                            $order++; 

                        }
                      }
                    }
                ?>

</section>
<section>


           <h2 id="causes" style="margin-top: 25px;">Ursachen</h2>
           <?php 

                    for($i=0;$i<20;$i++){
                      if($regionContent[$order]=='causes'){ 


                       

                        if($typeContent[$order]=='textarea' && $dataContent[$order]!=''){
                            echo '<p>'.$dataContent[$order].'</p>' ;   
                            $order++; 


                        }

                        else if ($typeContent[$order]=='bullet' && $dataContent[$order]!=''){

                            echo '<div class="bullets">' ;
                            echo'<ul>' ;       

                          while($typeContent[$order]=='bullet' && $dataContent[$order]!=''){

                            echo '<li>'.$dataContent[$order].'</li>' ;   
                            $order++; 

                          }

                            echo '</ul>' ; 
                            echo '</div>' ; 

                        }

                        else if($dataContent[$order]==''){

                            $order++; 

                        }
                      }
                    }
                ?>

</section>
<section>


           <h2 id="comlpications" style="margin-top: 25px;">Komplikationen</h2>
           <?php 

                    for($i=0;$i<20;$i++){
                      if($regionContent[$order]=='complications'){ 


                       

                        if($typeContent[$order]=='textarea' && $dataContent[$order]!=''){
                            echo '<p>'.$dataContent[$order].'</p>' ;   
                            $order++; 


                        }

                        else if ($typeContent[$order]=='bullet' && $dataContent[$order]!=''){

                            echo '<div class="bullets">' ;
                            echo'<ul>' ;       

                          while($typeContent[$order]=='bullet' && $dataContent[$order]!=''){

                            echo '<li>'.$dataContent[$order].'</li>' ;   
                            $order++; 

                          }

                            echo '</ul>' ; 
                            echo '</div>' ; 

                        }

                        else if($dataContent[$order]==''){

                            $order++; 

                        }
                      }
                    }
                ?>

</section>
<section>
              <h2 id="diagnosis" style="margin-top: 25px;">Diagnose</h2>
              <?php 

                    for($i=0;$i<20;$i++){
                      if($regionContent[$order]=='diagnosis'){ 


                       

                        if($typeContent[$order]=='textarea' && $dataContent[$order]!=''){
                            echo '<p>'.$dataContent[$order].'</p>' ;   
                            $order++; 


                        }

                        else if ($typeContent[$order]=='bullet' && $dataContent[$order]!=''){

                            echo '<div class="bullets">' ;
                            echo'<ul>' ;       

                          while($typeContent[$order]=='bullet' && $dataContent[$order]!=''){

                            echo '<li>'.$dataContent[$order].'</li>' ;   
                            $order++; 

                          }

                            echo '</ul>' ; 
                            echo '</div>' ; 

                        }

                        else if($dataContent[$order]==''){

                            $order++; 

                        }
                      }
                    }
                ?>



        

</section>
<section>
            <h2 id="treatment" style="margin-top: 25px;">Behandlung</h2>
            <?php 

                    for($i=0;$i<20;$i++){
                      if($regionContent[$order]=='treatment'){ 


                       

                        if($typeContent[$order]=='textarea' && $dataContent[$order]!=''){
                            echo '<p>'.$dataContent[$order].'</p>' ;   
                            $order++; 


                        }

                        else if ($typeContent[$order]=='bullet' && $dataContent[$order]!=''){

                            echo '<div class="bullets">' ;
                            echo'<ul>' ;       

                          while($typeContent[$order]=='bullet' && $dataContent[$order]!=''){

                            echo '<li>'.$dataContent[$order].'</li>' ;   
                            $order++; 

                          }

                            echo '</ul>' ; 
                            echo '</div>' ; 

                        }

                        else if($dataContent[$order]==''){

                            $order++; 

                        }
                      }
                    }
                ?>



   
</section>



          
          
        </div><!-- end content -->
  
<a class="back_link" onClick="window.history.back()"><img src="./images/serv/dotarrow2.png"/>Zurück<?php if ($dataContent!=''){echo $dataContent[$order];} $order++ ?>  </a>
      </div>  
         </div>     
  </div> <!-- end big wrap -->
</div>
</div>
</div>

  
  <?php include("footer.php"); ?>

<script type="text/javascript">

setTimeout(function(){var a=document.createElement("script");
  var b=document.getElementsByTagName("script")[0];
  a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0023/4893.js?"+Math.floor(new Date().getTime()/3600000);
  a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>
<script type="text/javascript">
$(document).ready(function(){

    window.stopPos = $('#treatment').offset().top-380 ; 
    $(".wrap-insidebar").sticky({
        topSpacing: 200,
        zIndex:-1,
        stopper: stopPos
      });

$('.bxslider').bxSlider({
  mode: 'fade',
  captions: true
});
    // scrolls

    $('#question').one("click", function () {
        $(this).val('');
    });

 var stickyNavTop2 = $('.subm').offset().top -200;  
      var stickyNav2 = function(){  
        var scrollTop2 = $(window).scrollTop();  
           
        if (scrollTop2 > stickyNavTop2) {   
            $('.subm').addClass('follow-me-sm');  
        } else {  
            $('.subm').removeClass('follow-me-sm');   
        }  
    };  
    stickyNav2(); 

    $(window).scroll(function() {
           stickyNav2(); 

    });
  });

  </script>

</body>
</html>