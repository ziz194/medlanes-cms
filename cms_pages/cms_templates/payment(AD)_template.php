<?php

require_once('cms_api_call.php') ;
$url = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/';
?>

 
 <!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?php echo $dataContent[$order]; $order++ ?></title>
    <link rel="icon" type="<?php echo $url ?>cms/cms_pages/askadoctor/image/ico" href="images/favicon.png">
	<!-- css -->
	<link rel="stylesheet" href="<?php echo $url ?>cms/cms_pages/askadoctor/css/global.css">
	<link rel="stylesheet" href="<?php echo $url ?>cms/cms_pages/askadoctor/css/payment.css">
	<link rel="stylesheet" href="<?php echo $url ?>cms/cms_pages/askadoctor/css/noslider.css">
	<link rel="stylesheet" href="<?php echo $url ?>cms/cms_pages/askadoctor/css/tooltipster.css" />
	<link rel="stylesheet" href="<?php echo $url ?>cms/cms_pages/askadoctor/css/sweetalert.css" />

	<!-- scripts -->
	<script src="https://code.jquery.com/jquery-1.11.2.min.js"></script>
	<script src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
	<script type="text/javascript" src="<?php echo $url ?>cms/cms_pages/askadoctor/js/jquery.tooltipster.min.js"></script>
	<script src="<?php echo $url ?>cms/cms_pages/askadoctor/js/noslider.js"></script>
	<script src="<?php echo $url ?>cms/cms_pages/askadoctor/js/us/switchpaymentEN.js"></script>
	<script src="<?php echo $url ?>cms/cms_pages/askadoctor/js/custom/sweetalert.js"></script>
	<script src="<?php echo $url ?>cms/cms_pages/askadoctor/js/custom/popupcampaign.js"></script>
	<script type="text/javascript" src="<?php echo $url ?>cms/cms_pages/askadoctor/js/us/payment.js"></script> 
<script>

</script>
	<style>
		.fshow{
			display:block;
			float:left;
			margin-top:13px;
		}
		.fhide{
			display:none;
		}

		.mini .r{
			width: 375px;
		}
	.ctext{
			display:none;
		    float: left;
		    width: 100%;
		    font-size: 14px;
		    margin-bottom:20px;
	}
	.ctext h2{
		font-size:18px;
		font-weight: 700;
		margin-bottom:20px;
	}
	</style>
	<!-- RAF COUPON SCRIPT -->
	<script>
	$(document).ready(function(){
		 var raf = sessionStorage.getItem("raf");
		 if(raf && raf != 'null'){
		 	console.log('RAF');
		 	$('.r .two').css("display" ,"none");
		 	$('.submit').css("margin-left" ,"0px");
		 	$('.raf-img').css("right" ,"0px");
		 	$('.amount').text("9");
		 	$('.obox h1').text("Congratulations, you used a discount code");
		 	$('.submit').text("Get your Answer");
		 	$('.raffill').text("Use this opportunity to pay only $9 instead of $52. This deal is one time only");
		 }


		 // USE THIS FOR SOCIAL RETARGETING CAMPAIGNS // Change text or anything else here
		 var coupon_price = sessionStorage.getItem("coupon_price");
		 if(coupon_price && coupon_price != 'null'){
		 
		 	$('.r .two').css("display" ,"none");
		 	$('.submit').css("margin-left" ,"0px");
		 	$('.raf-img').css("right" ,"0px");
		 	$('.amount').text("9");
		 	$('.obox h1').text("Congratulations, you used a discount code");
		 	$('.submit').text("Get your Answer");
		 	$('.raffill').text("Use this opportunity to pay only $9 instead of $52. This deal is one time only");
		 }

	});


	</script>
</head>
<body>




<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WKLFHR"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WKLFHR');</script>
<!-- End Google Tag Manager -->

	<div class="clearfix header-wrapper">
		<div class="content">
			<div class="sub-wrap">
				<div class="left">
					<div class="head">
						<a href="#"><img src="<?php echo $url ?>cms/cms_assets/<?php echo $dataContent[$order]; $order++ ?>" alt="<?php echo $dataContent[$order]; $order++ ?>"/>
						</a>
						<p><?php echo $dataContent[$order]; $order++ ?></p>
					</div>
				</div>
				<div class="right">
					<!--<p><?php//echo $lang['hiw_title']; ?></p>-->
					<div class="one">
						<img src="<?php echo $url ?>cms/cms_pages/askadoctor/images/trust.png" class="pull-left" />
					</div>
				</div>
			</div>
		</div>
	</div>
		<div id="checkoutprogress">
		<ul id="progress">
		    <li class="active"><p><i class="ask-queston-icon"></i><?php echo $dataContent[$order]; $order++ ?></p><b></b></li>
		    <li class="active"><p><i class="add-info-icon"></i><?php echo $dataContent[$order]; $order++ ?></p></li>
		    <li><p><i class="option-info-icon"></i><?php echo $dataContent[$order]; $order++ ?></p></li>
		    <li><p><i class="answer-icon"></i><?php echo $dataContent[$order]; $order++ ?></p></li>
		    <li></li>
		</ul>
	</div>
<div class="content sub-content">
		<div class="sub-wrap">
			<div class="left">
				<div class="obox">
					<h1><?php echo $dataContent[$order]; $order++ ?></h1>
					<p><?php echo $dataContent[$order]; $order++ ?><br><?php echo $dataContent[$order]; $order++ ?></p>	
				</div>
				<div class="mini">
					<div class="l">
						<div class="one">
							<div class="profile-avatar">
								<img class="odoc" src="<?php echo $url ?>cms/cms_assets/<?php echo $dataContent[$order]; $order++ ?>" alt="<?php echo $dataContent[$order]; $order++ ?>">
								
							</div>
							<div class="profile-desc">
							<p><b><?php echo $dataContent[$order]; $order++ ?></b></p>
							<p><?php echo $dataContent[$order]; $order++ ?></p>
							<p style='color:green'><b><?php echo $dataContent[$order]; $order++ ?></b></p>
							<!-- <img src="images/logo-verified-expert-sm.gif" alt="#" width="150px"> -->
							</div>
						</div>

						<div class="clearfix"></div>
						<div class="box">
							<div class="z">
								<p> <?php echo $dataContent[$order]; $order++ ?></p>
								<p class="desc"><?php echo $dataContent[$order]; $order++ ?></p>									
							</div>
							<div class="z">
								<p><?php echo $dataContent[$order]; $order++ ?></p>
								<p class="desc"><?php echo $dataContent[$order]; $order++ ?></p>								
							</div>
							<div class="z">
								<p><?php echo $dataContent[$order]; $order++ ?></p>
								<p class="desc"><?php echo $dataContent[$order]; $order++ ?></p>									
							</div>
							<div class="z">
								<p><?php echo $dataContent[$order]; $order++ ?></p>
								<p class="desc"><?php echo $dataContent[$order]; $order++ ?></p></div>							
						</div>
						<img src="<?php echo $url ?>cms/cms_pages/askadoctor/images/doctor-cert.png" style="position: absolute; top:0; right:0;">
					</div>
					<div class="r" style="width:335px;">					
						<div class="scroll">
							<div class="two">
								<p class="pad-this-on-ger"><b class="sliderheader"><?php echo $dataContent[$order]; $order++ ?></b></p>
								<!-- New Slider -->
								<div class="slide-wrapepr-new">
									<div class="slide-base"></div>
								    <ul id="navi">
								        <li><a class="menu active" href="javascript://"></a></li>
								        <li><a class="menu" href="javascript://"></a></li>
								        <li><a class="menu" href="javascript://"></a></li>
								    </ul>
								</div>
								<!-- /.New Slider -->
							 	<div class="testslide" id="leftslide"></div>
								<div class="text clearfix">
									<p onclick="moveit('1','left')"><?php echo $dataContent[$order]; $order++ ?></p>						
									<p onclick="moveit('2','left')"><?php echo $dataContent[$order]; $order++ ?></p>
									<p onclick="moveit('3','left')"><?php echo $dataContent[$order]; $order++ ?></p>

								</div>
								<span id="value-span"></span>
							</div>
							<div class="two">
								<p><b class="sliderheader"><?php echo $dataContent[$order]; $order++ ?></b></p>
								<!-- New Slider -->
								<div class="slide-wrapepr-new">
									<div class="slide-base"></div>
								    <ul id="navi1">
								        <li><a class="menu active" href="javascript://"></a></li>
								        <li><a class="menu" href="javascript://"></a></li>
								        <li><a class="menu" href="javascript://"></a></li>
								    </ul>
								</div>
								<!-- /.New Slider -->								
								<div class="testslide" id="rightslide">								</div>
								<div class="text clearfix">
									<p onclick="moveit('1','left')"><?php echo $dataContent[$order]; $order++ ?></p>
									<p onclick="moveit('2','left')"><?php echo $dataContent[$order]; $order++ ?></p>
									<p onclick="moveit('3','left')"><?php echo $dataContent[$order]; $order++ ?></p>
								</div>
							</div>
							<div class="last">
						<div class="ctext">
						<!--  -->
						</div>	
								<p><?php echo $dataContent[$order]; $order++ ?><a style="color: #0065a2;text-decoration: none;cursor:pointer;" class="bluebox" title="<?php echo $dataContent[$order]; $order++ ?>" > <?php echo $dataContent[$order]; $order++ ?></a> <span class="bluebox" style="cursor:pointer;" title="You can ask follow up questions until your question was answered to your complete satisfaction."></span> <?php echo $dataContent[$order]; $order++ ?><span class="amount"> <?php echo $dataContent[$order]; $order++ ?></span>.<img style="width: 16px;height:16px;margin-left:2px;cursor:pointer;" class="bluebox" title="<?php echo $dataContent[$order]; $order++ ?>" src="<?php echo $url ?>cms/cms_pages/askadoctor/images/icon-help.gif" /></p>								<p class="raffill"></p>


								<a href="<?php echo $dataContent[$order]; $order++ ?>" class="submit"><?php echo $dataContent[$order]; $order++ ?></a>								
							</div>
						</div>


					</div>
				</div>
			</div><!-- end left -->
		</div>
	</div>	
<footer>
<div class="sub-wrap">
	<div class="mini-wrap-two clearfix">
		<div class="one two">
			<ul>
				<li><a href="<?php echo $dataContent[$order]; $order++ ?>"><?php echo $dataContent[$order]; $order++ ?></a></li>
				<li><a href="<?php echo $dataContent[$order]; $order++ ?>"><?php echo $dataContent[$order]; $order++ ?></a></li>
				<li><a href="<?php echo $dataContent[$order]; $order++ ?>"><?php echo $dataContent[$order]; $order++ ?></a></li>
			</ul>		
		</div>
		<div class="one two">
			<ul>
				<li><a target="_blank" href="<?php echo $dataContent[$order]; $order++ ?>"><?php echo $dataContent[$order]; $order++ ?></a></li>
				<li><a target="_blank" href="<?php echo $dataContent[$order]; $order++ ?>"><?php echo $dataContent[$order]; $order++ ?></a></li>
			</ul>								
		</div>
		<div class="support">
			<ul>
			<li><?php echo $dataContent[$order]; $order++ ?></li>
			<li><?php echo $dataContent[$order]; $order++ ?>:</li>
			</ul>
		</div>

		<div class="one">
			<img src="<?php echo $url ?>cms/cms_pages/askadoctor/images/supported-by.png" class="supported" />
		</div>
	</div>

</footer>

<script type="text/javascript">
(function(d, src, c) { var t=d.scripts[d.scripts.length - 1],s=d.createElement('script');s.id='la_x2s6df8d';s.async=true;s.src=src;s.onload=s.onreadystatechange=function(){var rs=this.readyState;if(rs&&(rs!='complete')&&(rs!='loaded')){return;}c(this);};t.parentElement.insertBefore(s,t.nextSibling);})(document,
'//medlanes.ladesk.com/scripts/track.js',
function(e){ LiveAgent.createButton('e022f5bc', e); });
</script>

</body>
</html>