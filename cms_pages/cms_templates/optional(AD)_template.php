<?php

require_once('cms_api_call.php') ;
$url = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/';
?>

 <!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?php echo $dataContent[$order]; $order++ ?></title>
    <link rel="icon" type="image/ico" href="<?php echo $url ?>cms/cms_pages/askadoctor/images/favicon.ico">
    <!--Google Fonts-->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,300italic,700,700italic,400italic' rel='stylesheet' type='text/css'>
	<!-- css -->
	<link rel="stylesheet" href="<?php echo $url ?>cms/cms_pages/askadoctor/css/global.css">
	<link rel="stylesheet" href="<?php echo $url ?>cms/cms_pages/askadoctor/css/optional.css">


	<!-- scripts -->
	<script src="https://code.jquery.com/jquery-1.11.2.min.js"></script>
	<script src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

	<!-- jQuery Cookie -->
	<script type="text/javascript" src="<?php echo $url ?>cms/cms_pages/askadoctor/js/jquery.cookie.js"></script>
	<!-- End of jQuery Cookie -->
	<!-- Fingerprint -->
	<script type="text/javascript" src="<?php echo $url ?>cms/cms_pages/askadoctor/js/fingerprint.js"></script>
	<script type="text/javascript" src="<?php echo $url ?>cms/cms_pages/askadoctor/js/common.js"></script>

	<!-- End of Fingerprint -->

	<style>
		.fshow{
			display:block;
			float:left;
			margin-top:13px;
		}
		.fhide{
			display:none;
		}
	</style>
	<script>
		  function isNumberKey(evt){  
		     var charCode = (evt.which) ? evt.which : event.keyCode
		     if (charCode > 31 && (charCode < 48 || charCode > 57))
		        return false;

		     return true;
		  }
	</script>
</head>
<body>
		<!-- FingerPrint value -->
	<input type="hidden" id="fp" />
	<!-- End of FingerPrint value -->
	<div class="clearfix header-wrapper">
		<div class="content">
			<div class="sub-wrap">
				<div class="left">
					<div class="head">
						<a href="index.php"><img src="<?php echo $url ?>cms/cms_assets/<?php echo $dataContent[$order]; $order++ ?>" alt="<?php echo $dataContent[$order]; $order++ ?>"/>
						</a>
						<p><?php echo $dataContent[$order]; $order++ ?></p>
					</div>
				</div>
				<div class="right">
					<!--<p><?php//echo $lang['hiw_title']; ?></p>-->
					<div class="one">
						<img src="images/trust.png" class="pull-left" />
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="checkoutprogress">
		<ul id="progress">
		    <li class="active"><p><i class="ask-queston-icon"></i><?php echo $dataContent[$order]; $order++ ?></p><b></b></li>
		    <li><p><i class="add-info-icon"></i><?php echo $dataContent[$order]; $order++ ?></p></li>
		    <li><p><i class="option-info-icon"></i><?php echo $dataContent[$order]; $order++ ?></p></li>
		    <li><p><i class="answer-icon"></i><?php echo $dataContent[$order]; $order++ ?></p></li>
		    <li></li>
		</ul>
	</div>
	<div class="content sub-content">
		<div class="sub-wrap">
			<div class="left">
				<div class="obox">
					<h1><?php echo $dataContent[$order]; $order++ ?></h1>
					<p><?php echo $dataContent[$order]; $order++ ?></p>
					
				</div>
				<div class="inputs" >
					<div class="one">
						<p><?php echo $dataContent[$order]; $order++ ?></p>
						<input id="name" type="text">
					</div>
					<div class="one">
						<p><?php echo $dataContent[$order]; $order++ ?></p>
						<select name="gender" id="gender">
							<option disabled  value=""><?php echo $dataContent[$order]; $order++ ?></option>
							<option value="m"><?php echo $dataContent[$order]; $order++ ?></option>
							<option value="f"><?php echo $dataContent[$order]; $order++ ?></option>
						</select>
					</div>
					<div class="one">
						<p><?php echo $dataContent[$order]; $order++ ?></p>
						<input onkeypress="return isNumberKey(event)" maxlength="2" id="age" type="text">
					</div>
					<div class="one opt-textarea">
						<p><?php echo $dataContent[$order]; $order++ ?></p>
						<textarea name="try" id="try" cols="30" rows="10"></textarea>
					</div>
					<div class="one">
						<p> <?php echo $dataContent[$order]; $order++ ?></p>
						<input style="padding-left:10px;" placeholder="optional" id="coupon" type="text">
					</div>
					<div class="one">
						<a class="continue forward"> <?php echo $dataContent[$order]; $order++ ?></a>
						<a class="skip forward" onclick="getdetails()" ><b><?php echo $dataContent[$order]; $order++ ?></b></a>
					</div>
				</div>
			</div><!-- end left -->
			<div class="right">
				<div class="two">
					<img class="doc" src="<?php echo $url ?>cms/cms_pages/askadoctor/images/medical-payment.png" alt="">
					<div class="one">
						<h3><?php echo $dataContent[$order]; $order++ ?></h3>
						<div class="two tooltip-left">
							<p><?php echo $dataContent[$order]; $order++ ?></p></div>						<div class="two tooltip-left" >
							
							<p><?php echo $dataContent[$order]; $order++ ?></p>
					</div>		   
						<div class="two tooltip-left">
							<p><?php echo $dataContent[$order]; $order++ ?></p>
						</div>					
					</div>
				</div>	
			</div>
		</div>
	</div>
	
<footer>
<div class="sub-wrap">
	<div class="mini-wrap-two clearfix">
		<div class="one two">
			<ul>
				<li><a href="<?php echo $dataContent[$order]; $order++ ?>"><?php echo $dataContent[$order]; $order++ ?></a></li>
				<li><a href="<?php echo $dataContent[$order]; $order++ ?>"><?php echo $dataContent[$order]; $order++ ?></a></li>
				<li><a href="<?php echo $dataContent[$order]; $order++ ?>"><?php echo $dataContent[$order]; $order++ ?></a></li>
			</ul>		
		</div>
		<div class="one two">
			<ul>
				<li><a target="_blank" href="<?php echo $dataContent[$order]; $order++ ?>"><?php echo $dataContent[$order]; $order++ ?></a></li>
				<li><a target="_blank" href="<?php echo $dataContent[$order]; $order++ ?>"><?php echo $dataContent[$order]; $order++ ?></a></li>
			</ul>								
		</div>
		<div class="support">
			<ul>
			<li><?php echo $dataContent[$order]; $order++ ?></li>
			<li><?php echo $dataContent[$order]; $order++ ?>:</li>
			</ul>
		</div>

		<div class="one">
			<img src="<?php echo $url ?>cms/cms_pages/askadoctor/images/supported-by.png" class="supported" />
		</div>
	</div>

</footer>

<script type="text/javascript">
(function(d, src, c) { var t=d.scripts[d.scripts.length - 1],s=d.createElement('script');s.id='la_x2s6df8d';s.async=true;s.src=src;s.onload=s.onreadystatechange=function(){var rs=this.readyState;if(rs&&(rs!='complete')&&(rs!='loaded')){return;}c(this);};t.parentElement.insertBefore(s,t.nextSibling);})(document,
'//medlanes.ladesk.com/scripts/track.js',
function(e){ LiveAgent.createButton('e022f5bc', e); });
</script>

</body>
</html>
