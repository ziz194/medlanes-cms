<?php
require_once('cms_api_call.php') ;
?>

<!DOCTYPE html>
<?php  include_once 'functions.php';include_once 'http://medlanes.com/medlanescms/cms_pages/php/adword_tracking.php';  ?>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="author" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
  <link rel="icon" type="image/png" href="images/favicon.ico" sizes="16x16">
  <title><?php echo $dataContent[$order]; $order++ ?></title>
  <title>Medlanes</title>
  
  
  <!-- css -->
  <link rel="stylesheet" href="http://medlanes.com/medlanescms/cms_pages/css/images.css">
  <link href='//fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="http://medlanes.com/medlanescms/cms_pages/css/global-disease.css">
  <link rel="stylesheet" href="http://medlanes.com/medlanescms/cms_pages/css/disease.css">';
    

  
  <!-- <link rel="stylesheet" href="css/slick.css"> -->

  <script src="http://medlanes.com/medlanescms/cms_pages/js/jquery.js" /></script>
  <script src="http://medlanes.com/medlanescms/cms_pages/js/migrate.js" /></script>
  <script src="http://medlanes.com/medlanescms/cms_pages/js/jquery.bxslider.js"></script> 
  <script src="http://medlanes.com/medlanescms/cms_pages/js/slick.js" /></script>
  <script src="http://medlanes.com/medlanescms/cms_pages/js/index-disease.js" /></script>
  <script src="//js.maxmind.com/js/apis/geoip2/v2.1/geoip2.js" type="text/javascript"></script>
  <script type="application/javascript">
    geoip2.city( function(geoipResponse){   
      ticket = localStorage.setItem('realcountry',geoipResponse.country.names.en);  
      ticket = localStorage.setItem('realcity', geoipResponse.city.names.en); 
      
          $.ajax({
              type: "POST",
              url:  "php/savelocation.php",
        data: {"location":JSON.stringify(geoipResponse)}
        });
      return;
    }, function(error){
      console.log('Error from maxmind api: ',error);
      return;
    } );
  </script>

  <script>
  function getURLParameter(name) {
    return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null
  }
  $(document).ready(function(){
  disease = getURLParameter('d');
  if(disease){
    localStorage.setItem("disease", disease);
  }
    
  
      $('.bxslider').bxSlider({
          controls: false,
          pager: false,
          auto: true,
      pause: 6000,
      speed: 1000
      });

    $('input').focus(function(){
      $(this).addClass('whiteplace');
    });
    $('textarea').focus(function(){
      $(this).addClass('whiteplace');
    });
    $('input').focusout(function(){
      $(this).removeClass('whiteplace');
    });
    $('textarea').focusout(function(){
      $(this).removeClass('whiteplace');
    });
    // $('.slide3,.slide2,.slide1').slick({
    //   dots: true,
    //   infinite: true,
    //   speed: 500,
    //   autoplay:true
    // });
  });

  </script>
  <style>
  .alreadyregged{
    display:none;
  }
  .forgot {
    font-size: 16px;
    font-family: 'robotolight', 'Roboto', sans-serif;
    font-weight: 300;
    color: #999999;
    margin-top: 10px;
    float: left;
    margin-top: 10px;
    text-decoration: none;
  }

  </style>
</head>
<body>
<?php include "header-disease.php" ?> 
        <p style="margin-top:3px"><img class="supportcall" src="http://medlanes.com/medlanescms/cms_pages/images/support.svg" width="19" height="19" alt="#"><b><?php echo $dataContent[$order]; $order++ ?></b> or <b>service@medlanes.com</b></p>
      </div>
    </div>  
  </header>
  <div class="content index">
    <div class="heading" style='background-image:url(http://medlanes.com/medlanescms/cms_assets/<?php echo $dataContent[$order]; $order++ ?>)'>
      <div class="sub-wrap">
        <h1><?php echo $dataContent[$order]; $order++ ?></h1>
        <h2><?php echo $dataContent[$order]; $order++ ?><b>Your satisfaction is 100% guaranteed.</b></h2>
        <div class="box">
          <img src="images/trust.png" alt="#"/>
          <textarea placeholder="Your question to our experts" name="question" id="msg"></textarea>
          <div class="one">
            <div class="shadow">
              <input type="email" name="email" id="email" placeholder="Your email"/>
              <a class="cta" onclick="backemail()">
                <p>Get your answer now</p>
                <img src="images/bigar.png" alt="#"/>
              </a>              
            </div>
            <p><a class="forgot" href="https://medlanes.com/user/password-forgot.php">Forgot password?</a>*Your data is encrypted and will not be disclosed.</p>
            <h2 class="alreadyregged" style="  float: left;  width: 100%;  line-height: 23px;  color: red;  font-family: roboto;  font-size: 20px;  margin-top: 10px;">It seems that your account is already registered with our service. Please contact our support team on Service@medlanes.com</h2>
          </div>
        </div>        
      </div><!-- end sub -->
    </div><!-- end heading -->
    <div class="sub-wrap">
      <div class="concept">
        <p>Concept seen on:</p>
        <img class="bigconcept" src="http://medlanes.com/medlanescms/cms_pages/images/conceptus.png" alt="#">
        <img class="mobconcept" src="http://medlanes.com/medlanescms/cms_pages/images/conceptgermob.png" alt="#">        
      </div>    
      <div class="lists">
        <div class="one">
          <h2><?php echo $dataContent[$order]; $order++ ?></h2>
          <ul class="bxslider">
            <li class="customers">
              <img class="img-circle" id="sl1" src="http://medlanes.com/medlanescms/cms_assets/<?php echo $dataContent[$order]; $order++ ?>" alt="customers" width="64" height="64"/>
              <div class="right">
                <p><b><?php echo $dataContent[$order]; $order++ ?></b></p>
                <p><?php echo $dataContent[$order]; $order++ ?></p>
                <p><?php echo $dataContent[$order]; $order++ ?></p>
                <p><?php echo $dataContent[$order]; $order++ ?></p>
              </div>
            </li>
          </ul>       
        </div>
        <div class="one">
          <h2><?php echo $dataContent[$order]; $order++ ?></h2>
          <ul class="bxslider">
            <li class="doctors">
              <img class="img-circle" id="sl1" src="http://medlanes.com/medlanescms/cms_assets/<?php echo $dataContent[$order]; $order++ ?>" alt="doctor" width="64" height="64"/>
              <div class="right">
                <div class="sb">
                  <img src="images/icon.svg" width="16" height="18" alt="#">
                  <p><?php echo $dataContent[$order]; $order++ ?></p>
                </div>
                <p><?php echo $dataContent[$order]; $order++ ?></p>
                <p><?php echo $dataContent[$order]; $order++ ?> People Helped</p>
                <img src="images/stars.png" alt="#"/>
                <p><?php echo $dataContent[$order]; $order++ ?></p>
                <p><?php echo $dataContent[$order]; $order++ ?></p>
              </div>
            </li>
               <li class="doctors">
              <img class="img-circle" id="sl1" src="http://medlanes.com/medlanescms/cms_assets/<?php echo $dataContent[$order]; $order++ ?>" alt="doctor" width="64" height="64"/>
              <div class="right">
                <div class="sb">
                  <img src="http://medlanes.com/medlanescms/cms_pages/images/icon.svg" width="16" height="18" alt="#">
                  <p>aaa</p>
                </div>
                <p>aaa</p>
                <p>aaa</p>
                <img src="http://medlanes.com/medlanescms/cms_pages/images/stars.png" alt="#"/>
                <p>aaa</p>
                <p>aaa</p>
              </div>
            </li>
          </ul>       
        </div>
        <div class="one">
          <h2><?php echo $dataContent[$order]; $order++ ?></h2>
          <ul class="staticlink"> 
            <li class="news">
              <div class="right whatever">
                <p><?php echo $dataContent[$order]; $order++ ?></p>

                  <ul >
                  <?php  
                      while (($typeContent[$order]=='bullet') && ($regionContent[$order]=='links' )) 
                           {
                         ?>
                         <?php if ($dataContent[$order]=='') {
                           $order++; 
                          }
                         else 
                         {
                          ?>
                       <li><a  style='text-decoration:none;' href="<?php  if ($dataContent!=''){echo $dataContent[$order];} $order++ ;?>"><h2><?php  if ($dataContent!=''){echo $dataContent[$order];} $order++ ;?><h2></a></li>
                           <?php
                         }
                              } 
                           ?>
                    </ul>
              </div>
            </li> 
          </ul>       
        </div>
      </div>  
    </div><!-- end sub -->
  </div><!-- end index -->
<?php include "http://medlanes.com/medlanescms/cms_pages/footer-disease.php" ?> 
</body>
</html>


