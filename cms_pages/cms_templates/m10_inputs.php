



<div class="panel panel-success">
  <div class="panel-heading">
    <h3 class="panel-title">Heading</h3>
  </div>
  <div class="panel-body" hidden>
                   <form class="form-horizontal">
                         <cms-input   cms-input-id='page_title'    cms-input-label="Page Title"     cms-input-default='Medlanes | Qualified medical information - Expert opinions - Second opinions | Home' cms-input-region="heading" ></cms-input>
                        <cms-input   cms-input-id='page_number'    cms-input-label="Header Phone Number"     cms-input-default='(800) 413-7290' cms-input-region="heading" ></cms-input>
                        <cms-picture cms-input-id='heading_picture_1' cms-input-label="Heading Picture"       cms-input-region="heading" ></cms-picture>
                        <cms-input   cms-input-id='heading_text_2'    cms-input-label="Funnel Title"      cms-input-region="heading" ></cms-input>
                        <cms-textarea  cms-input-id='heading_textarea_3'    cms-input-label="Funnel Subtext"      cms-input-region="heading" ></cms-textarea>
</div>
</div>
  </form>



<div class="panel panel-warning">
  <div class="panel-heading">
    <h3 class="panel-title">Testiomonials</h3>
  </div>
  <div class="panel-body" hidden>
                   <form class="form-horizontal">
                        <cms-input   cms-input-id='testimonials_text_4'    cms-input-label="Section title"      cms-input-region="testimonials" ></cms-input>
                        <cms-picture cms-input-id='testimonials_picture_5' cms-input-label="Customer image"               cms-input-region="testimonials" ></cms-picture>
                        <cms-input   cms-input-id='testimonials_text_6'    cms-input-label="Testimonial Title"      cms-input-region="testimonials" ></cms-input>
                        <cms-textarea  cms-input-id='testimonials_textarea_7'    cms-input-label="Customer testimonial text"   cms-input-region="testimonials" ></cms-textarea> 
                        <cms-input   cms-input-id='testimonials_text_8'    cms-input-label="Customer Name and age"      cms-input-region="testimonials" ></cms-input>
                        <cms-input   cms-input-id='testimonials_text_9'    cms-input-label="Customer Location"      cms-input-region="testimonials" ></cms-input>



                        <cms-input   cms-input-id='testimonials_text_10'    cms-input-label="Doctor Section title"      cms-input-region="testimonials" ></cms-input>
                        <cms-picture cms-input-id='testimonials_picture_11' cms-input-label="Doctor image"               cms-input-region="testimonials" ></cms-picture>
                        <cms-input   cms-input-id='testimonials_text_12'    cms-input-label="Doctor Name"      cms-input-region="testimonials" ></cms-input>
                        <cms-input cms-input-id='testimonials_picture_13' cms-input-label="Doctor Speciality"               cms-input-region="testimonials" ></cms-input>
                        <cms-input   cms-input-id='testimonials_text_14'    cms-input-label="Number of people helped"      cms-input-region="testimonials" ></cms-input>
                        <cms-input   cms-input-id='testimonials_text_15'    cms-input-label="Rating"      cms-input-region="testimonials" ></cms-input>
                        <cms-textarea  cms-input-id='testimonials_textarea_16'    cms-input-label="Doctor testimonial text"   cms-input-region="testimonials" ></cms-textarea>     
</div>
</div>

  </form>



    <div class="panel panel-warning">
  <div class="panel-heading">
    <h3 class="panel-title">Links Section</h3>
  </div>
  <div class="panel-body" hidden>
                   <form class="form-horizontal">
                        <cms-input cms-input-id='links_picture_17' cms-input-label="Links section title"               cms-input-region="links" ></cms-input>
                        <cms-textarea  cms-input-id='links_text_18'    cms-input-label="Links section subtext"           cms-input-region="links" ></cms-textarea>
                        <cms-bullet   cms-input-id='links_bullet_19'    cms-input-label="<h2> links"    cms-input-iterations='2'     cms-input-region="links" ></cms-bullet>



          
</div>
</div>

  </form>

             
                                                     