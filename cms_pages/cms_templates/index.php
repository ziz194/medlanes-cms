<!DOCTYPE html>
<?php  include_once 'functions.php'; ?>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?php echo $lang['TITLE_INDEX']; ?></title>
	
	<!-- css -->
	<link rel="stylesheet" href="css/global.css">
	<link rel="stylesheet" href="css/index.css">
	<link href='//fonts.googleapis.com/css?family=Roboto:400,500,700,300' rel='stylesheet' type='text/css'>
	<link rel="icon" type="image/png" href="images/favicon.ico" sizes="16x16">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
	<!-- scripts -->
	<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
	<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
	<link rel="stylesheet" href="css/bslider.css">
	<script src="js/bslider.js"></script>
        <script src="//js.maxmind.com/js/apis/geoip2/v2.1/geoip2.js" type="text/javascript"></script>
		
		<script type="application/javascript">
		geoip2.city( function(geoipResponse){		
			ticket = localStorage.setItem('realcountry',geoipResponse.country.names.en);	
			ticket = localStorage.setItem('realcity', geoipResponse.city.names.en);	
			
	
			return;
		}, function(error){
			console.log('Error from maxmind api: ',error);
			return;
		} );
	</script>	

	<script type="application/javascript">
		geoip2.city( function(geoipResponse){
			$.ajax({
                            type: "POST",
                            url:  "php/savelocation.php",
                            data: {"location":JSON.stringify(geoipResponse)}
                        });
			return;
		}, function(error){
			console.log('Error from maxmind api: ',error);
			return;
		} );
	</script>

	<script>
	function getUrlParameter(sParam)
	{
	    var sPageURL = window.location.search.substring(1);
	    var sURLVariables = sPageURL.split('&');
	    for (var i = 0; i < sURLVariables.length; i++) 
	    {
	        var sParameterName = sURLVariables[i].split('=');
	        if (sParameterName[0] == sParam) 
	        {
	            return sParameterName[1];
	        }
	    }
	}     
	$(document).ready(function(){
		$('.bxslider').bxSlider({
		  controls: false,
		  pager:true,
		  auto:true,
		});
		var mobile = getUrlParameter('m');
		if(mobile){		
			sessionStorage.setItem('m10', 'yes');
		}
	});
	</script>
</head>
<body>
<?php include "header.php" ?>
<div class="content index">	
	<div class="heading">
		<div class="sub-wrap">
			<h1><?php echo $lang['INDEX_H']; ?></h1>
			<p id="hidemobile"><?php echo $lang['INDEX_H1']; ?></p>
			<a href="<?php echo $lang['HEADER_ADQ']; ?>" class="adq">
				<p><?php echo $lang['INDEX_19']; ?></p>
				<img src="images/index/arrow.png" alt="#"/>
			</a>
			<div class="one">
				<p class="small"><?php echo $lang['INDEX_18']; ?></p>
			</div>
			<div class="one">
				<img src="images/index/cta.png" alt="#"/>
			</div>	
		</div>
	</div><!-- end heading -->
		<div class="sub-wrap">
			<div class="concept">
				<p><?php echo $lang['INDEX_7']; ?></p>
				<?php 
				if (isset($_GET["lang"]) && !empty($_GET["lang"])) {
					$getparam = $_GET['lang'];
						switch ($getparam) {
						    case "en":
						        print('	
						        	<img class="bigconcept" src="images/global/concept.png" alt="#"/>
						        	<img class="mobconcept" src="images/global/c10.png" alt="#"/>
								');
						        break;
						    case "de":
						        print('	
						        	<img class="bigconcept" src="images/global/conceptde.png" alt="#"/>
						        	<img class="mobconcept" src="images/global/conceptgermob.png" alt="#"/>
								');
						        break;
						}
					 
				}else{  
			    		        print('
						        	<img class="bigconcept" src="images/global/conceptde.png" alt="#"/>
						        	<img class="mobconcept" src="images/global/conceptgermob.png" alt="#"/>
								');
				}
				?>
				
			</div>		
			<div class="concept mobileconcept">
			<p><?php echo $lang['INDEX_7']; ?></p>
				<ul class="bxslider">
				  <li><img src="images/mobileconcept/1.png" /></li>
				  <li><img src="images/mobileconcept/2.png" /></li>
				  <li><img src="images/mobileconcept/3.png" /></li>
				  <li><img src="images/mobileconcept/4.png" /></li>
				  <li><img src="images/mobileconcept/5.png" /></li>
				</ul>
			</div>
			<div class="lists">
				<div class="one">
					<p><?php echo $lang['INDEX_8']; ?></p>
					<ul class="slide1">
						<li>
							<img src="images/index/s1.png" alt="#">
							<div class="right">
								<p><?php echo $lang['INDEX_9']; ?></p>
								<p><?php echo $lang['INDEX_10']; ?></p>
							</div>
						</li>
					</ul>				
				</div>
				<div class="one">
					<p><?php echo $lang['INDEX_11']; ?></p>
					<ul class="slide2">
						<div>
							<li>
								<img src="images/index/s2.png" alt="#">
								<div class="right">
									<p><?php echo $lang['INDEX_12']; ?></p>
									<p><?php echo $lang['INDEX_13']; ?></p>
								</div>
							</li>
						</div>
					</ul>				
				</div>
				<div class="one">
					<p><?php echo $lang['INDEX_14']; ?></p>
					<ul class="slide3">
						<li>
							<img src="images/index/s3.png" alt="#">
							<div class="right">
								<div class="sb">
									<img src="images/index/dc.jpg" alt="#">
									<p><?php echo $lang['INDEX_15']; ?></p>
								</div>
								<p><?php echo $lang['INDEX_16']; ?></p>
								<img src="images/payment/stars.png" alt="#"/>
								<p><?php echo $lang['INDEX_17']; ?></p>
							</div>
						</li>
					</ul>				
				</div>
			</div>	
		</div><!-- end sub -->
</div>


<?php include "footer.php" ?>
</body>
</html>