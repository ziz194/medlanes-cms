
      <div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title">Header</h3>
  </div>
  <div class="panel-body" >
    <form class="form-horizontal">
          <cms-input cms-input-id='heading_text_1' cms-input-label="Page Title MetaTag"    cms-input-region="heading" ></cms-input>
          <cms-picture cms-input-id='heading_picture_2' cms-input-label="Logo Image"    cms-input-region="heading" ></cms-picture>
          <cms-input cms-input-id='heading_text_3' cms-input-label="Logo Title"   cms-input-default='Ask a medical question and get a fast answer'  cms-input-region="heading" ></cms-input>
                    <cms-input cms-input-id='heading_text_4' cms-input-label="Login Button"   cms-input-default='Login'  cms-input-region="heading" ></cms-input>


</div>
</div>
  </form>


      <div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title">Steps</h3>
  </div>
  <div class="panel-body" >
    <form class="form-horizontal">
          <cms-input cms-input-id='steps_text_4' cms-input-label="Ask a Question Label"    cms-input-region="steps" ></cms-input>
          <cms-input cms-input-id='steps_text_5' cms-input-label="Additional Information Label"    cms-input-region="steps" ></cms-input>
          <cms-input cms-input-id='steps_text_6' cms-input-label="Your Options Label"    cms-input-region="steps" ></cms-input>
          <cms-input cms-input-id='steps_text_7' cms-input-label="Get an answer Label"    cms-input-region="steps" ></cms-input>

         
</div>
  </form>


      <div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title">Page Core</h3>
  </div>
  <div class="panel-body" >
    <form class="form-horizontal">
          <cms-input cms-input-id='core_text_8' cms-input-label="Thank you!  "    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_9' cms-input-label="Your doctor is now reviewing your question"    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_10' cms-input-label="What happens next?"    cms-input-region="core" ></cms-input>



          <cms-input cms-input-id='core_text_14' cms-input-label="What happens next 1st Step"    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_15' cms-input-label="What happens next 2nd Step"    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_16' cms-input-label="Your satisfaction is fully guaranteed!"    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_17' cms-input-label="You will be able to ask follow-up questions until you are fully satisfied and have the answer to your question."    cms-input-region="core" ></cms-input>




       
</div>
</div>
  </form>


      
    
 <div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title">Footer</h3>
  </div>
  <div class="panel-body" >
    <form class="form-horizontal">
          <cms-input cms-input-id='footer_text_107' cms-input-label="Medlanes Link"    cms-input-region="footer" ></cms-input>
          <cms-input cms-input-id='footer_text_108' cms-input-label="Medlanes Label"    cms-input-region="footer" ></cms-input>
          <cms-input cms-input-id='footer_text_109' cms-input-label="Support Link"    cms-input-region="footer" ></cms-input>
          <cms-input cms-input-id='footer_text_110' cms-input-label="Support Label"    cms-input-region="footer" ></cms-input>

          <cms-input cms-input-id='footer_text_111' cms-input-label="Contact us Link"    cms-input-region="footer" ></cms-input>
          <cms-input cms-input-id='footer_text_112' cms-input-label="Contact us Label"    cms-input-region="footer" ></cms-input>

          <cms-input cms-input-id='footer_text_113' cms-input-label="terms Link"    cms-input-region="footer" ></cms-input>
          <cms-input cms-input-id='footer_text_114' cms-input-label="terms Label"    cms-input-region="footer" ></cms-input>

          <cms-input cms-input-id='footer_text_115' cms-input-label="Privacy Link"    cms-input-region="footer" ></cms-input>
          <cms-input cms-input-id='footer_text_116' cms-input-label="Privacy Label"    cms-input-region="footer" ></cms-input>

          <cms-input cms-input-id='footer_text_117' cms-input-label="Copyright Text"    cms-input-region="footer" ></cms-input>
          <cms-input cms-input-id='footer_text_118' cms-input-label="Ask a doctor supported by text"    cms-input-region="footer" ></cms-input>


</div>
</div>
  </form>


                 
                                                                                                      