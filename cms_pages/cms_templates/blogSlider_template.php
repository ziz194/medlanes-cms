<?php

require_once('cms_api_call.php') ;
$url = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/';
?>

<!doctype html>
<html  lang="en">
<head>

   <link rel="alternate" hreflang="de" href="<?php echo $dataContent[$order]; $order++ ?>" />

  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(n,t,e){function r(e){if(!t[e]){var o=t[e]={exports:{}};n[e][0].call(o.exports,function(t){var o=n[e][1][t];return r(o?o:t)},o,o.exports)}return t[e].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<e.length;o++)r(e[o]);return r}({QJf3ax:[function(n,t){function e(n){function t(t,e,a){n&&n(t,e,a),a||(a={});for(var u=c(t),f=u.length,s=i(a,o,r),p=0;f>p;p++)u[p].apply(s,e);return s}function a(n,t){f[n]=c(n).concat(t)}function c(n){return f[n]||[]}function u(){return e(t)}var f={};return{on:a,emit:t,create:u,listeners:c,_events:f}}function r(){return{}}var o="nr@context",i=n("gos");t.exports=e()},{gos:"7eSDFh"}],ee:[function(n,t){t.exports=n("QJf3ax")},{}],gos:[function(n,t){t.exports=n("7eSDFh")},{}],"7eSDFh":[function(n,t){function e(n,t,e){if(r.call(n,t))return n[t];var o=e();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(n,t,{value:o,writable:!0,enumerable:!1}),o}catch(i){}return n[t]=o,o}var r=Object.prototype.hasOwnProperty;t.exports=e},{}],D5DuLP:[function(n,t){function e(n,t,e){return r.listeners(n).length?r.emit(n,t,e):(o[n]||(o[n]=[]),void o[n].push(t))}var r=n("ee").create(),o={};t.exports=e,e.ee=r,r.q=o},{ee:"QJf3ax"}],handle:[function(n,t){t.exports=n("D5DuLP")},{}],XL7HBI:[function(n,t){function e(n){var t=typeof n;return!n||"object"!==t&&"function"!==t?-1:n===window?0:i(n,o,function(){return r++})}var r=1,o="nr@id",i=n("gos");t.exports=e},{gos:"7eSDFh"}],id:[function(n,t){t.exports=n("XL7HBI")},{}],loader:[function(n,t){t.exports=n("G9z0Bl")},{}],G9z0Bl:[function(n,t){function e(){var n=v.info=NREUM.info;if(n&&n.licenseKey&&n.applicationID&&f&&f.body){c(d,function(t,e){t in n||(n[t]=e)}),v.proto="https"===l.split(":")[0]||n.sslForHttp?"https://":"http://",a("mark",["onload",i()]);var t=f.createElement("script");t.src=v.proto+n.agent,f.body.appendChild(t)}}function r(){"complete"===f.readyState&&o()}function o(){a("mark",["domContent",i()])}function i(){return(new Date).getTime()}var a=n("handle"),c=n(1),u=window,f=u.document,s="addEventListener",p="attachEvent",l=(""+location).split("?")[0],d={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-515.min.js"},v=t.exports={offset:i(),origin:l,features:{}};f[s]?(f[s]("DOMContentLoaded",o,!1),u[s]("load",e,!1)):(f[p]("onreadystatechange",r),u[p]("onload",e)),a("mark",["firstbyte",i()])},{1:11,handle:"D5DuLP"}],11:[function(n,t){function e(n,t){var e=[],o="",i=0;for(o in n)r.call(n,o)&&(e[i]=t(o,n[o]),i+=1);return e}var r=Object.prototype.hasOwnProperty;t.exports=e},{}]},{},["G9z0Bl"]);</script>
    <title><?php echo $dataContent[$order]; $order++ ?></title>

  <meta name="description" content="<?php echo $dataContent[$order]; $order++ ?>">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta property="og:title" content="<?php echo $dataContent[$order]; $order++ ?>"/>
  <meta property="og:type" content="article"/>
  <meta property="og:image" content="<?php echo $url ?>cms/cms_assets/<?php  echo $dataContent[10];?>"/>
  <meta property="og:url" content="<?php echo $dataContent[$order]; $order++ ?>"/>
  <meta property="og:description" content="<?php echo $dataContent[9]; $order++ ?>"/>
    <meta property="og:type" content="TYPE"/> 
  <meta property="og:site_name" content="Medlanes"/>
  <meta name="twitter:card" content="summary"/>
  <meta name="twitter:url" content="<?php echo $dataContent[$order]; $order++ ?>"/>
  <meta name="twitter:title" content="<?php echo $dataContent[$order]; $order++ ?>"/>
  <meta name="twitter:description" content="<?php echo $dataContent[$order]; $order++ ?>"/>
  <meta name="twitter:image" content="images/17/checkk.png"/>


<meta property="fb:app_id" content="997629630269116">
<meta property="fb:admins" content="997629630269116" />


  <link rel="shortcut icon" type="image/x-icon" href="<?php echo $url ?>cms/cms_pages/favicon.ico">
  <link href='//fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
  <!-- Styles -->
  <link rel="stylesheet" type="text/css" href="<?php echo $url ?>cms/cms_pages/css/content2.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?php echo $url ?>cms/cms_pages/css/global.css">



  <link rel="stylesheet" href="<?php echo $url ?>cms/cms_pages/css/foundation.css" />
  <!-- jQuery  -->
  <script type="text/javascript" src="<?php echo $url ?>cms/cms_pages/js/jquery-1.11.2.min.js"></script>
  <!-- Scripts -->
  <script src="<?php echo $url ?>cms/cms_pages/js/jquery.autosize.min.js"></script>
  <script src="<?php echo $url ?>cms/cms_pages/js/jquery.flexslider.js"></script>
        <script src="<?php echo $url ?>cms/cms_pages/js/global.js"></script>

  <!-- jQuery UI -->
  <link rel="stylesheet" type="text/css" href="<?php echo $url ?>cms/cms_pages/css/jquery-ui.min.css">
  <script type="text/javascript" src="<?php echo $url ?>cms/cms_pages/js/jquery-ui.min.js"></script>
  <script type="text/javascript" src='<?php echo $url ?>cms/cms_pages/js/pagescript.js'></script>
  <script type="text/javascript" src='<?php echo $url ?>cms/cms_pages/js/jquery.sticky-kit.min.js'></script>

<script type="text/javascript">var switchTo5x=true;</script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">stLight.options({publisher: "b193c1a5-93fa-479c-aa26-29101633751f", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>

</head>
<body>
  <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.4&appId=997629630269116";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

  <div class="row">
            <?php
            if (isset($_SESSION['brand']) && ($_SESSION['brand']=='aad')){
            include("header.php");
            }
            else include ("header.php")
            ?>
  </div>


  <div class="row filter-buttons-row">

        <div class="small-4 columns">
          <a href="blog-overiew-page.php" class="active-button expand button blue-btn">Latest</a>
        </div>
        <div class="small-4 columns">
          <a href="blog-overiew-page.php" class="expand button white-btn">Popular</a>
        </div>
        <div class="small-4 columns">
          <a href="http://medlanes.com/add-question.php" class="expand button orange-btn">Ask a Doctor</a>
        </div>
      </div>



  <div class="row main-row blogSlider">
        <div class="large-12 medium-12 columns">
          <div class="row main-row blogSlider-row">
            <div class="large-8 medium-12 small-12 columns">

                      <?php
                              $blogPost = file_get_contents("http://medlanes.com/cms/api/pages/".$idPage);
                              $blogPostDetails=json_decode($blogPost,true) ; 
                                  $postTitle = $blogPostDetails['namePage'] ;
                                  $postUrl = $blogPostDetails['fileName'] ;
                                  $postDate = $blogPostDetails['created_at'] ;
                                  $postAuthor = $blogPostDetails['shownAuthor'] ; 
                                  $postTags = explode(',',$blogPostDetails['tags'] ) ;                                 
                               ?>
            
              <h1 class='blog-title'><?php echo $postTitle?></h1> 
            </div>
              <div class="large-4 medium-12 small-12 columns">
                     <ul class="social-network social-circle" style='float:right;' >
                        <li><a href="https://www.facebook.com/sharer/sharer.php?u=https://medlanes.com/blog/<?php echo $postUrl ?>" class="icoFacebook" title="Facebook" target="_blank"><i class="fa fa-facebook"></i></a></li>
                      <li><a href="https://twitter.com/intent/tweet?text=<?php echo $postTitle ?>&url=https://medlanes.com/blog/<?php echo $postUrl?>&via=Medlanes;" class="icoTwitter" title="Twitter" target="_blank"><i class="fa fa-twitter"></i></a></li>
                      <li><a href="https://plus.google.com/share?url=httsp://medlanes.com/blog/<?php echo $postUrl?>" class="icoGoogle" title="Google +" target="_blank"><i class="fa fa-google-plus"></i></a></li>
                      <li><a href="https://www.linkedin.com/shareArticle?mini=true&url=https://medlanes.com/blog/<?php echo $postUrl?>&title=<?php echo $postTitle ?>" class="icoLinkedin" title="Linkedin" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                      <li><a href="mailto:?subject=<?php echo $postTitle ?>" class="icoLinkedin" title="Mail" ><i class="fa fa-envelope"></i></a></li>



                      </ul>
                </div>  
          </div>
          <div class="row main-row blogSlider-row">
            <div class="large-10 columns">
              <p class='gray-text'>by <span class='blog-author'><?php echo $postAuthor?>, </span><?php echo $postDate?></p> 
            </div>
          </div>
          <div class="row main-row blogSlider-row">
                <div class="large-6 columns">
                  <ul class='blogSlider-tags-long'>
                    <?php 
                    foreach($postTags as $tag){
                      if (($tag)!=''){
                     echo  '<a href="http://medlanes.com/blog?pages=0&tag='.$tag.'"/><li class="gray-text"><img src="'.$url.'cms/cms_pages/img/tag-icon.png">'.$tag.'</li></a>' ; 
                      } 
                    }
                      ?>
     
                  </ul>
               </div>
               </div>


          </div>
              <div class="row main-row blogSlider-row">
                  <div class="large-12 medium-12 columns">
                                   <p class='gray-text blogSlider-intro'><?php echo $dataContent[$order]; $order++ ?></p> 
                  </div>

             </div>     
  <div class="row main-row blogSlider-row">
  <ul class="example-orbit-content" data-orbit data-options="animation:slide;
                  pause_on_hover:false;
                  timer:false ;
                  animation_speed:500;
                  navigation_arrows:true;
                  next_on_click: false ;
                  bullets:false;">



          <?php
            $i=0 ;
           while($regionContent[$order]=='post'){
                      if($dataContent[$order]!=''){ 

                        echo'<li data-orbit-slide="headline-'.$i.'">
                            <div class="row top-row main-row" >
                                                        <div class="large-12 medium-12 small-12" >
                                      <div class="heading-blogSlider"  style="background-image : url('.$url.'cms/cms_assets/'.$dataContent[$order].')"' ; $order++; echo 'alt="'.$dataContent[$order].'"></div></div>
                              ' ;
                              $order++; 
                        echo'<div class="row" >
                            <div class="large-12 medium-12 small-12 columns blog-details" >
                          <h2 class="sub-heading">'.$dataContent[$order].'</h2>';
                           $order++;
                         echo' <p class="gray-text">'.$dataContent[$order].' </p>' ; 
                        $order++;
                        $i++;

                echo'</div></div></div></li>' ;

           }
                      else{
                      $order++; 
                        }
                      }
                    
                ?>
</ul>
    </div>
             <div class="row main-row blogSlider-row">
                  <div class="large-12 medium-12 columns">
                                   <p class='gray-text blogSlider-ending'></p> 
                  </div>
             </div>     
                <div class="row main-row blogSlider-row">
                  <div id='comments' class="large-12 columns main-column" style="margin-bottom:50px;">
                  <div class="fb-comments" data-href="medlanes.com/blog/<?php echo $postUrl?>" data-width="100%" data-numposts="5"></div>
                       </div>
                  </div>     
                </div> 
                  <div class="row askadoctor-row ">
    <div class="large-12 columns askadoctor-box">
          <div class="large-3 columns main-column">
               <div class='row doc-image-row'>
                    <div style='background-image:url(<?php echo $url ?>cms/cms_pages/img/doctor.png)' class='doc-img-big'></div>
                </div>
                <div class='row doc-image-title'>
                  <p class='gray-text doc-text2'>
                    <strong>Dr. Jessica Maennel</strong></p> 
                    <hr class='doc-border'>
                </div>
          </div>
          <div class="large-9 columns">
            <h2>Questions?</h2>
            <p>Our doctors are here for you round-the-clock and would be glad to inform you about any medical information you may seek.</p>
                     <a href="http://medlanes.com/add-question.php" id='askDoctor-button' class="expand button askdoc-btn ask-btn"><p class="ask-bubble-right">Get Your Answer</p></a>      
        
          </div> 
    </div>
  </div>


  <div class="row main-row">
    <div class="large-12 columns main-column">
            <?php include("footer.php"); ?>

    </div>
  </div>


<script src="<?php echo $url ?>cms/cms_pages/js/foundation.min.js"></script>
<script>
$(document).foundation();
</script>
</body>
</html>
