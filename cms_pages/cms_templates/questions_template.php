<?php
require_once('cms_api_call.php') ;
$url = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/';

?>

<!doctype html>
<html  lang="en">
<head>


   <link rel="alternate" hreflang="de" href="<?php echo $dataContent[$order]; $order++ ?>" />

  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(n,t,e){function r(e){if(!t[e]){var o=t[e]={exports:{}};n[e][0].call(o.exports,function(t){var o=n[e][1][t];return r(o?o:t)},o,o.exports)}return t[e].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<e.length;o++)r(e[o]);return r}({QJf3ax:[function(n,t){function e(n){function t(t,e,a){n&&n(t,e,a),a||(a={});for(var u=c(t),f=u.length,s=i(a,o,r),p=0;f>p;p++)u[p].apply(s,e);return s}function a(n,t){f[n]=c(n).concat(t)}function c(n){return f[n]||[]}function u(){return e(t)}var f={};return{on:a,emit:t,create:u,listeners:c,_events:f}}function r(){return{}}var o="nr@context",i=n("gos");t.exports=e()},{gos:"7eSDFh"}],ee:[function(n,t){t.exports=n("QJf3ax")},{}],gos:[function(n,t){t.exports=n("7eSDFh")},{}],"7eSDFh":[function(n,t){function e(n,t,e){if(r.call(n,t))return n[t];var o=e();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(n,t,{value:o,writable:!0,enumerable:!1}),o}catch(i){}return n[t]=o,o}var r=Object.prototype.hasOwnProperty;t.exports=e},{}],D5DuLP:[function(n,t){function e(n,t,e){return r.listeners(n).length?r.emit(n,t,e):(o[n]||(o[n]=[]),void o[n].push(t))}var r=n("ee").create(),o={};t.exports=e,e.ee=r,r.q=o},{ee:"QJf3ax"}],handle:[function(n,t){t.exports=n("D5DuLP")},{}],XL7HBI:[function(n,t){function e(n){var t=typeof n;return!n||"object"!==t&&"function"!==t?-1:n===window?0:i(n,o,function(){return r++})}var r=1,o="nr@id",i=n("gos");t.exports=e},{gos:"7eSDFh"}],id:[function(n,t){t.exports=n("XL7HBI")},{}],loader:[function(n,t){t.exports=n("G9z0Bl")},{}],G9z0Bl:[function(n,t){function e(){var n=v.info=NREUM.info;if(n&&n.licenseKey&&n.applicationID&&f&&f.body){c(d,function(t,e){t in n||(n[t]=e)}),v.proto="https"===l.split(":")[0]||n.sslForHttp?"https://":"http://",a("mark",["onload",i()]);var t=f.createElement("script");t.src=v.proto+n.agent,f.body.appendChild(t)}}function r(){"complete"===f.readyState&&o()}function o(){a("mark",["domContent",i()])}function i(){return(new Date).getTime()}var a=n("handle"),c=n(1),u=window,f=u.document,s="addEventListener",p="attachEvent",l=(""+location).split("?")[0],d={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-515.min.js"},v=t.exports={offset:i(),origin:l,features:{}};f[s]?(f[s]("DOMContentLoaded",o,!1),u[s]("load",e,!1)):(f[p]("onreadystatechange",r),u[p]("onload",e)),a("mark",["firstbyte",i()])},{1:11,handle:"D5DuLP"}],11:[function(n,t){function e(n,t){var e=[],o="",i=0;for(o in n)r.call(n,o)&&(e[i]=t(o,n[o]),i+=1);return e}var r=Object.prototype.hasOwnProperty;t.exports=e},{}]},{},["G9z0Bl"]);</script>
    <title><?php echo $dataContent[$order]; $order++ ?></title>

  <meta name="description" content="<?php echo $dataContent[$order]; $order++ ?>">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta property="og:title" content="<?php echo $dataContent[$order]; $order++ ?>"/>
  <meta property="og:type" content="article"/>
  <meta property="og:image" content="images/17/checkk.png"/>
  <meta property="og:url" content="<?php echo $dataContent[$order]; $order++ ?>"/>
  <meta property="og:description" content="<?php echo $dataContent[$order]; $order++ ?>"/>
  <meta name="twitter:card" content="summary"/>
  <meta name="twitter:url" content="<?php echo $dataContent[$order]; $order++ ?>"/>
  <meta name="twitter:title" content="<?php echo $dataContent[$order]; $order++ ?>"/>
  <meta name="twitter:description" content="<?php echo $dataContent[$order]; $order++ ?>"/>
  <meta name="twitter:image" content="images/17/checkk.png"/>

  <link rel="shortcut icon" type="image/x-icon" href="<?php echo $url ?>cms/cms_pages/favicon.png">
  <link rel="stylesheet" href="<?php echo $url ?>cms/cms_pages/css/global.css">

  <!-- Styles -->
  <link rel="stylesheet" type="text/css" href="<?php echo $url ?>cms/cms_pages/css/content2.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <link href='//fonts.googleapis.com/css?family=Roboto:400,500,700,300' rel='stylesheet' type='text/css'>


  <link rel="stylesheet" href="<?php echo $url ?>cms/cms_pages/css/foundation.css" />
  <!-- jQuery  -->
  <script type="text/javascript" src="<?php echo $url ?>cms/cms_pages/js/jquery-1.11.2.min.js"></script>
  <script src="<?php echo $url ?>cms/cms_pages/js/jquery.flexslider.js"></script>
        <script src="<?php echo $url ?>cms/cms_pages/js/global.js"></script>


  <!-- Scripts -->
  <script src="<?php echo $url ?>cms/cms_pages/js/jquery.autosize.min.js"></script>
  <!-- jQuery UI -->
  <link rel="stylesheet" type="text/css" href="<?php echo $url ?>cms/cms_pages/css/jquery-ui.min.css">
  <script type="text/javascript" src="<?php echo $url ?>cms/cms_pages/js/jquery-ui.min.js"></script>
  <script type="text/javascript" src='<?php echo $url ?>cms/cms_pages/js/pagescript.js'></script>
  <script type="text/javascript" src='<?php echo $url ?>cms/cms_pages/js/jquery.sticky-kit.min.js'></script>
  <script type="text/javascript" src='<?php echo $url ?>cms/cms_pages/js/jquery.sharrre.min.js'></script>
</head>
<body>

  <div class="row">
    <div class="large-12 columns">
            <?php include("header.php"); ?>

    </div>
  </div>

  <div class="row">
    <div class="large-12 columns">
            <div class="heading" style='background-image:url(<?php echo $url ?>cms/cms_assets/<?php echo $dataContent[$order]; $order++ ?>)' alt='<?php echo $dataContent[$order]; $order++?>'>
                  <h1 class='page-title'><?php echo $dataContent[$order]; ?></h1>
                  <!-- Mobile title -->
                  <h1 class='page-title-min'><span><?php echo $dataContent[$order]; $order++?></span></h1>

            </div>  
    </div>
  </div>
  <div class="row ">
        <div class="small-4 columns main-column">
          <a href="<?php echo $dataContent[$order]; $order++?>.php" class=" expand button white-btn">ABOUT</a>
        </div>
        <div class="small-4 columns main-column">
          <a href="#" class="active-button expand button blue-btn">QUESTIONS</a>
        </div>
        <div class="small-4 columns main-column">
          <a href="http://medlanes.com/add-question.php" class="expand button orange-btn">ASK A DOCTOR</a>
        </div>
      </div>
<!-- Bread Crumbs -->
    <div class="row">
      <div class="large-12 medium-12 columns">
        <div class='bc-container'>
          <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
             <a href="<?php echo $dataContent[$order]; $order++?>" itemprop="url">
                <span class='bread-crumb' itemprop="title"><?php echo $dataContent[$order]; $order++?></span>
             </a> 
             ›<a href="<?php echo $dataContent[$order]; $order++?>" itemprop="url">
                <span class='bread-crumb' itemprop="title"><?php echo $dataContent[$order]; $order++?></span>
             </a> 
         </div>
       </div>
     </div>
  </div>
<!-- Bread Crumbs -->
     <div class="row ">
        <div class="large-4 medium-4 columns main-column">

             <div class="callout panel container side-nav-container ">
          <p class='related-title'>RELATED ARTICLES</p>
           <ul class="side-nav">

<!--               <li class="divider"></li>
 -->                <li class='related-toggle links-list'><a  class='gray-text'><img class='links-icon' width='25' height='25' src='<?php echo $url ?>cms/cms_pages/img/complications.png'/>CONDITIONS</a>
                <ul class='related-list' hidden>


                    <?php
                              $related = file_get_contents("http://medlanes.com/cms/api/pages/getRelatedPages/".$idPage);
                              $relatedData=json_decode($related,true) ; 
                                foreach ($relatedData as $key => $value){
                                  $pageId[] = $value['idPage'] ;
                                  $pageName[] = $value['namePage'] ;
                                  $pageUrl[] = $value['fileName'] ; 

                               ?>

                                  <li><a href="<?php echo $pageUrl[$key] ?>"><?php echo $pageName[$key] ?></a></li>
                              <?php 
                                  }
                                ?>
                </ul>
              </li>           
            </ul>   
           
                    <!-- Right dropdown links -->
    
        </div>

      
  
          <div class="callout panel container">
            <h2 class='grey-text doc-title'>Ask your <?php echo $dataContent[1];?></h2>

            <div >
            <img src="<?php echo $url ?>cms/cms_pages/img/doctor.png" class='doc-img'>  <p class='gray-text doc-text'>
              Dr. Jessica Maennel<br><small>Family Medicine</small><br><small>7 Years of Experience</small></p>
           </div>

                 <div>
          <textarea rows='1' name='question' id='question-box' class='question_text' placeholder='Type in your question here...'></textarea>
          </div>
    
         <a href="http://medlanes.com/add-question.php" id='askDoctor-button' class="expand button askdoc-btn ask-btn"><p class="ask-bubble">Get Your Answer</p></a>      
        </div>
            

      </div>

      <div class="large-8 medium-8 columns main-column questions-column">
        <div class="callout panel container">



          <?php
          $questions = file_get_contents("http://medlanes.com/cms/api/pages/getAllQuestionsByPage/".$idPage);

               $questionsArr=json_decode($questions,true) ; 

            //   if($tagSearch=='all'){
            //   array_shift($postsArr) ;
            // }
           
          foreach ($questionsArr as $key => $value){

        
            $questionsTitle[] = $value['namePage'] ;
            $questionsAnswer[] = $value['overviewText'] ;
            $questionsImage[] = $value['overviewImage'] ;
            $questionsName[] = $value['overviewName'] ;
            $questionsTags[] = explode(',',$value['tags'] ) ; 
            $questionsId[] =  $value['idPage'] ;
            $questionsUrl[] = $value['fileName'] ;

               // var_dump($dataContentPost[][]) ; 
            // foreach ($dataP
            ?>


                          <p class="blue-text" style="font-size:16px !important; margin-bottom:10px !important ;"> <img style='margin-right : 5px' width="25" height="25" src="<?php echo $url ?>cms/cms_pages/img/avat-icon-little.png" alt="Complete Question"><strong><a href="<?php  echo $questionsUrl[$key]?>" class="blue-text" ><?php  echo $questionsTitle[$key]?></a></strong></p>
                          <p> <img class="doc-img-small" width="40" height="40" src="<?php echo $url ?>cms/cms_assets/<?php  echo $questionsImage[$key]?>">
                            <img style="margin-right:5px ; " width="20" height="20" src="<?php echo $url ?>cms/img/medicine.png"><span class="blue-text"><?php  echo $questionsName[$key]?></span><span class="grey-text"> answered:</p>
                          <p class="gray-text question-details-box"><?php $str = $questionsAnswer[$key] ;
                  $strArray = explode(" ",$str);
                  $strArray = array_slice($strArray,0 , 30);
                  $string =  implode(" ",$strArray);
                  echo $string; ?><a href="<?php echo $questionsUrl[$key] ?>">... Read More »</a></p>
                       <hr>


          <?php
            }
          ?>

      </div>
    </div>
        </div>


   <div class="row main-row">
    <div class="large-12 columns main-column">
            <?php include("footer.php"); ?>

    </div>
  </div>





  </div>
</div>

<script src="<?php echo $url ?>cms/cms_pages/js/foundation.min.js"></script>
<script>
$(document).foundation();
</script>
</body>
</html>

