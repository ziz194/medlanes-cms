<?php
require_once('cms_api_call.php') ;
$url = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/';
?>

<!doctype html>
<html  lang="en">
<head>


   <link rel="alternate" hreflang="de" href="<?php echo $dataContent[$order]; $order++ ?>" />

  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(n,t,e){function r(e){if(!t[e]){var o=t[e]={exports:{}};n[e][0].call(o.exports,function(t){var o=n[e][1][t];return r(o?o:t)},o,o.exports)}return t[e].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<e.length;o++)r(e[o]);return r}({QJf3ax:[function(n,t){function e(n){function t(t,e,a){n&&n(t,e,a),a||(a={});for(var u=c(t),f=u.length,s=i(a,o,r),p=0;f>p;p++)u[p].apply(s,e);return s}function a(n,t){f[n]=c(n).concat(t)}function c(n){return f[n]||[]}function u(){return e(t)}var f={};return{on:a,emit:t,create:u,listeners:c,_events:f}}function r(){return{}}var o="nr@context",i=n("gos");t.exports=e()},{gos:"7eSDFh"}],ee:[function(n,t){t.exports=n("QJf3ax")},{}],gos:[function(n,t){t.exports=n("7eSDFh")},{}],"7eSDFh":[function(n,t){function e(n,t,e){if(r.call(n,t))return n[t];var o=e();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(n,t,{value:o,writable:!0,enumerable:!1}),o}catch(i){}return n[t]=o,o}var r=Object.prototype.hasOwnProperty;t.exports=e},{}],D5DuLP:[function(n,t){function e(n,t,e){return r.listeners(n).length?r.emit(n,t,e):(o[n]||(o[n]=[]),void o[n].push(t))}var r=n("ee").create(),o={};t.exports=e,e.ee=r,r.q=o},{ee:"QJf3ax"}],handle:[function(n,t){t.exports=n("D5DuLP")},{}],XL7HBI:[function(n,t){function e(n){var t=typeof n;return!n||"object"!==t&&"function"!==t?-1:n===window?0:i(n,o,function(){return r++})}var r=1,o="nr@id",i=n("gos");t.exports=e},{gos:"7eSDFh"}],id:[function(n,t){t.exports=n("XL7HBI")},{}],loader:[function(n,t){t.exports=n("G9z0Bl")},{}],G9z0Bl:[function(n,t){function e(){var n=v.info=NREUM.info;if(n&&n.licenseKey&&n.applicationID&&f&&f.body){c(d,function(t,e){t in n||(n[t]=e)}),v.proto="https"===l.split(":")[0]||n.sslForHttp?"https://":"http://",a("mark",["onload",i()]);var t=f.createElement("script");t.src=v.proto+n.agent,f.body.appendChild(t)}}function r(){"complete"===f.readyState&&o()}function o(){a("mark",["domContent",i()])}function i(){return(new Date).getTime()}var a=n("handle"),c=n(1),u=window,f=u.document,s="addEventListener",p="attachEvent",l=(""+location).split("?")[0],d={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-515.min.js"},v=t.exports={offset:i(),origin:l,features:{}};f[s]?(f[s]("DOMContentLoaded",o,!1),u[s]("load",e,!1)):(f[p]("onreadystatechange",r),u[p]("onload",e)),a("mark",["firstbyte",i()])},{1:11,handle:"D5DuLP"}],11:[function(n,t){function e(n,t){var e=[],o="",i=0;for(o in n)r.call(n,o)&&(e[i]=t(o,n[o]),i+=1);return e}var r=Object.prototype.hasOwnProperty;t.exports=e},{}]},{},["G9z0Bl"]);</script>
    <title><?php echo $dataContent[$order]; $order++ ?></title>

  <meta name="description" content="<?php echo $dataContent[$order]; $order++ ?>">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta property="og:title" content="<?php echo $dataContent[$order]; $order++ ?>"/>
  <meta property="og:type" content="article"/>
  <meta property="og:image" content="images/17/checkk.png"/>
  <meta property="og:url" content="<?php echo $dataContent[$order]; $order++ ?>"/>
  <meta property="og:description" content="<?php echo $dataContent[$order]; $order++ ?>"/>
  <meta name="twitter:card" content="summary"/>
  <meta name="twitter:url" content="<?php echo $dataContent[$order]; $order++ ?>"/>
  <meta name="twitter:title" content="<?php echo $dataContent[$order]; $order++ ?>"/>
  <meta name="twitter:description" content="<?php echo $dataContent[$order]; $order++ ?>"/>
  <meta name="twitter:image" content="images/17/checkk.png"/>

  <link rel="shortcut icon" type="image/x-icon" href="<?php echo $url ?>cms/cms_pages/favicon.ico">
  <!-- Styles -->
  <link rel="stylesheet" type="text/css" href="<?php echo $url ?>cms/cms_pages/css/content2.css">
  <link rel="stylesheet" href="<?php echo $url ?>cms/cms_pages/css/foundation.css" />
  <!-- jQuery  -->
  <script type="text/javascript" src="<?php echo $url ?>cms/cms_pages/js/jquery-1.11.2.min.js"></script>
  <!-- Scripts -->
  <script src="<?php echo $url ?>cms/cms_pages/js/jquery.autosize.min.js"></script>
  <!-- jQuery UI -->
  <link rel="stylesheet" type="text/css" href="<?php echo $url ?>cms/cms_pages/css/jquery-ui.min.css">
  <script type="text/javascript" src="<?php echo $url ?>cms/cms_pages/js/jquery-ui.min.js"></script>
  <script type="text/javascript" src='<?php echo $url ?>cms/cms_pages/js/jquery.sticky-kit.min.js'></script>
  <script type="text/javascript" src='<?php echo $url ?>cms/cms_pages/js/pagescript.js'>

</script>
</head>
<body>

  <div class="row">
    <div class="large-12 columns">
            <?php include("header2.php"); ?>

    </div>
  </div>
  <div class="row">
    <div class="large-12 columns">
         <div class="heading" style='background-image:url(<?php echo $url ?>cms/cms_assets/<?php echo $dataContent[$order]; $order++ ?>)' alt='<?php echo $dataContent[$order]; $order++ ?>'>
                  <h1 class='portal-title'><?php echo $dataContent[$order]; $order++?></h1>
                 <!--  mobile title -->
<!--                   <h1 class='page-title-min'><span>Cough, Cold and Flu Health Center</span></h1> -->
          </div>  
    </div>
  </div>

  <div class="row ">
    <div class="large-12 medium-12 columns">

      <div class="row main-row">
        <div class="small-4 columns main-column">
          <a href="#" class="expand button blue-btn">LERNEN</a>
        </div>
        <div class="small-4 columns main-column">
          <a href="<?php echo $dataContent[$order]; $order++ ?>" class="active-button expand button blue-btn">FRAGEN</a>
        </div>
        <div class="small-4 columns main-column">
          <a href="#" class="expand button orange-btn">ARZT FRAGEN</a>
        </div>
      </div>
      <!-- Bread Crumbs -->
    <div class="row">
      <div class="large-12 medium-12 columns">
        <div class='bc-container'>
          <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
             <a href="<?php echo $dataContent[$order]; $order++?>" itemprop="url">
                <span class='bread-crumb' itemprop="title"><?php echo $dataContent[$order]; $order++?></span>
             </a> 
             ›<a href="<?php echo $dataContent[$order]; $order++?>" itemprop="url">
                <span class='bread-crumb' itemprop="title"><?php echo $dataContent[$order]; $order++?></span>
             </a> 
         </div>
       </div>
     </div>
  </div>
<!-- Bread Crumbs -->
      <div class="row main-row">
        <div class="large-4 medium-4 columns main-column">


          <div class='realted-questions-column'>




 <?php
                              $questions = file_get_contents("http://medlanes.com/cms/api/pages/getQuestionsByPage/".$idPage);
                              $questionsData=json_decode($questions,true) ; 
                                foreach ($questionsData as $key => $value){
                                  $questionId[] = $value['idElement'] ;
                                  $questionName[] = $value['nameElement'] ;
                                  $questionUrl[] = $value['urlElement'] ; 
                               ?>
                                 <div class="callout panel container">
                                   <a href="<?php echo $questionUrl[$key] ?>"> <p><span class='blue-text'><strong>Frage: </strong></span><span class='gray-text'><?php echo $questionName[$key] ?></p></a>
                                </div>
  
                                  <?php 
                                    }
                                  ?>

                                </div>
        
      
  
          <div class="callout panel container">
            <p class='blue-text doc-title'><strong>Fragen Sie einen Arzt</strong></p>

            <div class='doc-title'>
            <img src="<?php echo $url ?>cms/cms_pages/img/doctor.png" class='doc-img'>  <p class='gray-text doc-text'>
 <img width='20' height='20' src="<?php echo $url ?>cms/cms_pages/img/medicine.png"> Dr. Kathrin Hamman<br><small>Allgemeinmedizin</small><br><small>12 Jahre Praxiserfahrung</small></p>
           </div>

           <div class='stars'> 
            <img src="<?php echo $url ?>cms/cms_pages/img/star.png"> 
            <img src="<?php echo $url ?>cms/cms_pages/img/star.png">
            <img src="<?php echo $url ?>cms/cms_pages/img/star.png">
            <img src="<?php echo $url ?>cms/cms_pages/img/star.png">
            <img src="<?php echo $url ?>cms/cms_pages/img/star.png">  
          </div> 
                 <div>
          <textarea rows='1' name='question' class='question_text' placeholder='Enter your question'></textarea>
          </div>
    
        
        </div>
         <a href="#" class="expand button orange-btn ask-btn">Fragen Sie einen Arzt<</a>          

      </div>
      <div class="large-8 medium-8 columns main-column portal-column">
        <div class="callout panel container">
         <h2 class='blue-text'>Übersicht</h2>
         <p class='gray-text'><?php echo $dataContent[$order]; $order++?></p>
        </div>
        <div class="callout panel container" style='border-bottom:0 !important ;'>
           <h2 class='blue-text'>Häufigste Krankheiten</h2>
           <div class='row separation-row'>
           <hr class='blue-separation'>
         </div>
           <div class='row' style='margin-right: -21px;'>
            <div class="large-4 medium-4 columns portals-column">
                      <div class="callout panel portal-container">

              <img src='<?php echo $url ?>cms/cms_assets/<?php echo $dataContent[$order]; $order++?>' alt='<?php echo $dataContent[$order]; $order++ ?>' height='50' width='50px' class='portal-image' /><a style='text-decoration: none ; color : #48b1a4 !important;' href='<?php echo $dataContent[$order]; $order++?>'><h3 class='blue-text'><?php echo $dataContent[$order]; $order++?></h3></a><br>
              <p class='gray-text portal-text'><?php echo $dataContent[$order]; $order++?></p>
            </div>
          </div>
            <div class="large-4 medium-4 columns portals-column">
                      <div class="callout panel portal-container">

              <img src='<?php echo $url ?>cms/cms_assets/<?php echo $dataContent[$order]; $order++?>' alt='<?php echo $dataContent[$order]; $order++ ?>' height='50' width='50px' class='portal-image' /><a style='text-decoration: none ; color : #48b1a4 !important;' href='<?php echo $dataContent[$order]; $order++?>'><h3 class='blue-text'><?php echo $dataContent[$order]; $order++?></h3></a><br>
              <p class='gray-text portal-text'><?php echo $dataContent[$order]; $order++?></p>
            </div>
          </div>
            <div class="large-4 medium-4 columns portals-column">
                      <div class="callout panel portal-container">

              <img src='<?php echo $url ?>cms//cms_assets/<?php echo $dataContent[$order]; $order++?>' alt='<?php echo $dataContent[$order]; $order++ ?>' height='50' width='50px' class='portal-image' /><a style='text-decoration: none ; color : #48b1a4 !important;' href='<?php echo $dataContent[$order]; $order++?>'><h3 class='blue-text'><?php echo $dataContent[$order]; $order++?></h3></a><br>
              <p class='gray-text portal-text'><?php echo $dataContent[$order]; $order++?></p>
            </div>
          </div>
          </div>
        </div>

         <div class="callout panel container main-container" style='border-top:0 !important ;'>
           <h2 class='blue-text'>Krankheiten A-Z</h2>
      <div class='row separation-row'>
           <hr class='blue-separation'>
         </div>
           
          </div>
        </div>

        </div>
      </div>

    

  </div>
</div>

<script src="<?php echo $url ?>cms/cms_pages/js/foundation.min.js"></script>
<script>
$(document).foundation();
</script>
</body>
</html>
