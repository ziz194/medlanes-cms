<?php
// require_once('cms_api_call.php') ;
$url = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/';
?>

<!doctype html>
<html  lang="en">
<head>


   <link rel="alternate" hreflang="de" href="medmedo.de/" />
   <meta name="viewport" content="width=device-width, initial-scale=1">

  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(n,t,e){function r(e){if(!t[e]){var o=t[e]={exports:{}};n[e][0].call(o.exports,function(t){var o=n[e][1][t];return r(o?o:t)},o,o.exports)}return t[e].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<e.length;o++)r(e[o]);return r}({QJf3ax:[function(n,t){function e(n){function t(t,e,a){n&&n(t,e,a),a||(a={});for(var u=c(t),f=u.length,s=i(a,o,r),p=0;f>p;p++)u[p].apply(s,e);return s}function a(n,t){f[n]=c(n).concat(t)}function c(n){return f[n]||[]}function u(){return e(t)}var f={};return{on:a,emit:t,create:u,listeners:c,_events:f}}function r(){return{}}var o="nr@context",i=n("gos");t.exports=e()},{gos:"7eSDFh"}],ee:[function(n,t){t.exports=n("QJf3ax")},{}],gos:[function(n,t){t.exports=n("7eSDFh")},{}],"7eSDFh":[function(n,t){function e(n,t,e){if(r.call(n,t))return n[t];var o=e();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(n,t,{value:o,writable:!0,enumerable:!1}),o}catch(i){}return n[t]=o,o}var r=Object.prototype.hasOwnProperty;t.exports=e},{}],D5DuLP:[function(n,t){function e(n,t,e){return r.listeners(n).length?r.emit(n,t,e):(o[n]||(o[n]=[]),void o[n].push(t))}var r=n("ee").create(),o={};t.exports=e,e.ee=r,r.q=o},{ee:"QJf3ax"}],handle:[function(n,t){t.exports=n("D5DuLP")},{}],XL7HBI:[function(n,t){function e(n){var t=typeof n;return!n||"object"!==t&&"function"!==t?-1:n===window?0:i(n,o,function(){return r++})}var r=1,o="nr@id",i=n("gos");t.exports=e},{gos:"7eSDFh"}],id:[function(n,t){t.exports=n("XL7HBI")},{}],loader:[function(n,t){t.exports=n("G9z0Bl")},{}],G9z0Bl:[function(n,t){function e(){var n=v.info=NREUM.info;if(n&&n.licenseKey&&n.applicationID&&f&&f.body){c(d,function(t,e){t in n||(n[t]=e)}),v.proto="https"===l.split(":")[0]||n.sslForHttp?"https://":"http://",a("mark",["onload",i()]);var t=f.createElement("script");t.src=v.proto+n.agent,f.body.appendChild(t)}}function r(){"complete"===f.readyState&&o()}function o(){a("mark",["domContent",i()])}function i(){return(new Date).getTime()}var a=n("handle"),c=n(1),u=window,f=u.document,s="addEventListener",p="attachEvent",l=(""+location).split("?")[0],d={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-515.min.js"},v=t.exports={offset:i(),origin:l,features:{}};f[s]?(f[s]("DOMContentLoaded",o,!1),u[s]("load",e,!1)):(f[p]("onreadystatechange",r),u[p]("onload",e)),a("mark",["firstbyte",i()])},{1:11,handle:"D5DuLP"}],11:[function(n,t){function e(n,t){var e=[],o="",i=0;for(o in n)r.call(n,o)&&(e[i]=t(o,n[o]),i+=1);return e}var r=Object.prototype.hasOwnProperty;t.exports=e},{}]},{},["G9z0Bl"]);</script>
    <title>Medlanes Blog - Overview </title>

  <meta name="description" content="<?php echo $dataContent[$order]; $order++ ?>">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta property="og:title" content="Medlanes Blog - Overview >"/>
  <meta property="og:type" content="article"/>
  <meta property="og:image" content="images/17/checkk.png"/>
  <meta property="og:url" content="<?php echo $dataContent[$order]; $order++ ?>"/>
  <meta property="og:description" content="<?php echo $dataContent[$order]; $order++ ?>"/>
  <meta name="twitter:card" content="summary"/>
  <meta name="twitter:url" content="<?php echo $dataContent[$order]; $order++ ?>"/>
  <meta name="twitter:title" content="Medlanes Blog - Overview "/>
  <meta name="twitter:description" content="<?php echo $dataContent[$order]; $order++ ?>"/>
  <meta name="twitter:image" content="images/17/checkk.png"/>

  <link rel="shortcut icon" type="image/x-icon" href="<?php echo $url ?>cms/cms_pages/favicon.png">
  <link rel="stylesheet" href="<?php echo $url ?>cms/cms_pages/css/global.css">

  <!-- Styles -->
  <link rel="stylesheet" type="text/css" href="<?php echo $url ?>cms/cms_pages/css/content2.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

  <!-- BOOTSTRAP-->


  <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
 -->  <!-- BOOTSTRAP-->

  <link href='//fonts.googleapis.com/css?family=Roboto:400,500,700,300' rel='stylesheet' type='text/css'>


  <link rel="stylesheet" href="<?php echo $url ?>cms/cms_pages/css/foundation.css" />
  <!-- jQuery  -->
  <script type="text/javascript" src="<?php echo $url ?>cms/cms_pages/js/jquery-1.11.2.min.js"></script>
  <script src="<?php echo $url ?>cms/cms_pages/js/jquery.flexslider.js"></script>
        <script src="<?php echo $url ?>cms/cms_pages/js/global.js"></script>


  <!-- Scripts -->
  <script src="<?php echo $url ?>cms/cms_pages/js/jquery.autosize.min.js"></script>
  <!-- jQuery UI -->
  <link rel="stylesheet" type="text/css" href="<?php echo $url ?>cms/cms_pages/css/jquery-ui.min.css">
  <script type="text/javascript" src="<?php echo $url ?>cms/cms_pages/js/jquery-ui.min.js"></script>
  <script type="text/javascript" src='<?php echo $url ?>cms/cms_pages/js/pagescript.js'></script>
  <script type="text/javascript" src='<?php echo $url ?>cms/cms_pages/js/jquery.sticky-kit.min.js'></script>
  <script type="text/javascript" src='<?php echo $url ?>cms/cms_pages/js/jquery.sharrre.min.js'></script>


</head>
<body>
    <div class="row">
            <?php
            if (isset($_SESSION['brand']) && ($_SESSION['brand']=='aad')){
            include("header.php");
            }
            else include ("header.php")
            ?>

  </div>

 <?php include("blog-searchbar.php") ?>


    <div class="row filter-buttons-row ">
        <div class="small-4 columns">
          <a href="http://medlanes.com/blog/" class="active-button expand button blue-btn">Latest</a>
        </div>
        <div class="small-4 columns">
          <a href="http://medlanes.com/blog/" class="expand button white-btn">Popular</a>
        </div>
        <div class="small-4 columns">
          <a href="http://medlanes.com/add-question.php" class="expand button orange-btn">Ask a Doctor</a>
        </div>
      </div>



<?php
           $articleStart = 0 ;
           $number = 1 ;
          $currentPage =1 ;

          if(isset($_GET['pages'])){
             $articleStart =  $_GET['pages'] ;
          }
           $tagSearch = 'all' ;
          if(isset($_GET['tag'])){
             $tagSearch =$_GET['tag'] ;
          }


                $searchQuery = 'a' ;
          if(isset($_GET['q'])){
             $searchQuery =$_GET['q'] ;
          }
             if(isset($_GET['pageN'])){
             $currentPage =$_GET['pageN'] ;
             $number =$_GET['pageN'] ;
          }
          $posts = file_get_contents("http://medlanes.com/cms/api/pages/getBlogPosts/".$articleStart."/".$tagSearch."/".$searchQuery."/5");
          $postsArr=json_decode($posts,true) ; 

            //   if($tagSearch=='all'){
            //   array_shift($postsArr) ;
            // }
           
          foreach ($postsArr as $key => $value){

        
            $postsTitle[] = $value['namePage'] ;
            $postsDescription[] = $value['overviewText'] ;
            $postsImage[] = $value['overviewImage'] ;


            if ($value['shownDate']==null) {
              $postsDate[] = $value['created_at'] ;
            }
            else{
             $postsDate[] = $value['shownDate'] ;
            }
            $postsUrl[] = $value['fileName'] ;
            if ($value['shownAuthor']==null || $value['shownDate']==''  ) {
              $postsAuthor[] = $value['createdBy'] ;
            }
            else{
             $postsAuthor[] = $value['shownAuthor'] ;
            }
            $postsTags[] = explode(',',$value['tags'] ) ; 
            $postsId[] =  $value['idPage'] ;

               // var_dump($dataContentPost[][]) ; 
            // foreach ($dataP
            ?>

  <div class="row top-row main-row-blog blog-overview-row" >
    <div class="large-6 columns">
           <a href="<?php echo $postsUrl[$key] ?>"> <div class='heading-blog'  style='background-image : url(<?php echo $url ?>cms/cms_assets/<?php  echo $postsImage[$key]?>)' alt='<?php echo $dataContent[$order]; $order++ ?>'></div></a>
    </div>  
    <div class="large-6 columns blog-details-overview" >
          <div class="row main-row">
            <div class="large-10 columns">
            
              <h1 class='blog-title'><a href="<?php echo $postsUrl[$key] ?>"><?php echo $postsTitle[$key] ?></a></h1> 
            </div>
          </div>
          <div class="row main-row">
            <div class="large-10 columns">
              <p class='gray-text'>by <span class='blog-author'><?php echo $postsAuthor[$key]?>, </span><?php echo $postsDate[$key]?></p> 
            </div>
          </div>
              <div class="row main-row">
                <div class="large-11 columns">
                  <ul class='blogSlider-tags'>
                    <?php 
                    foreach($postsTags[$key] as $tag){
                      if (($tag)!=''){
                     echo  '<a href="http://medlanes.com/blog?pages=0&tag='.$tag.'"/><li class="gray-text"><img src="'.$url.'cms/cms_pages/img/tag-icon.png">'.$tag.'</li></a>' ; 
                      } 
                    }
                      ?>
     
                  </ul>
          
              </div>
            </div>

            <div class="row main-row">
            <div class="large-10 columns">
              <p class='gray-text overview-p'><?php $str = $postsDescription[$key] ;
                  $strArray = explode(" ",$str);
                  $strArray = array_slice($strArray,0 , 30);
                  $string =  implode(" ",$strArray);
                  echo $string;?><a href="<?php echo $postsUrl[$key] ?>">... Read More »</a></p>

            </div>
          </div>
 

            <div class="row main-row">
                  <ul class="social-network social-circle">
                      <li><a href="https://www.facebook.com/sharer/sharer.php?u=https://medlanes.com/blog/<?php echo $postsUrl[$key] ?>" class="icoFacebook" title="Facebook" target="_blank"><i class="fa fa-facebook"></i></a></li>
                      <li><a href="https://twitter.com/intent/tweet?text=<?php echo $postsTitle[$key] ?>&url=https://medlanes.com/blog/<?php echo $postsUrl[$key]?>&via=Medlanes;" class="icoTwitter" title="Twitter" target="_blank"><i class="fa fa-twitter"></i></a></li>
                      <li><a href="https://plus.google.com/share?url=https://medlanes.com/blog/<?php echo $postsUrl[$key]?>" class="icoGoogle" title="Google +" target="_blank"><i class="fa fa-google-plus"></i></a></li>
                      <li><a href="https://www.linkedin.com/shareArticle?mini=true&url=https://medlanes.com/blog/<?php echo $postsUrl[$key]?>&title=<?php echo $postsTitle[$key] ?>" class="icoLinkedin" title="Linkedin" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                      <li><a href="mailto:?subject=<?php echo $postsTitle[$key] ?>" class="icoLinkedin" title="Mail" ><i class="fa fa-envelope"></i></a></li>
                 </ul>
          </div>
          </div>
    </div>
 
      <?php 
             }
          ?>

  <div class="row">
    <?php 
    $totalPosts =  file_get_contents("http://medlanes.com/cms/api/pages/totalPosts/0/".$tagSearch."/".$searchQuery); 
    $numberPages = round($totalPosts/5) ;
    $limit =  $numberPages-5 ;


    if(($articleStart<$totalPosts-6) && ($articleStart!=0)){
         echo '<ul style="width:62% !important" class="pagination pagination-list"><li class="arrow"><a href="http://medlanes.com/blog?pageN='.($currentPage-1).'&pages='.($articleStart-5).'&tags='.$tagSearch.'">&laquo; Previous</a></li>' ;

        if($currentPage>=5 && $currentPage<$limit){
            $positive = 5 ;
            $negative = 5 ;
  

       }
       else if ($currentPage<5){
            $positive = 10- $currentPage ;
            $negative = $currentPage ;
       }

       else if ($currentPage>=$limit){

            $positive = $numberPages-$currentPage ; 
            $negative = $currentPage -9 ;
       }
         

    for($i=$currentPage-$negative;$i<$currentPage+$positive;$i++){
        $pagesNumber = $i*5 ;
        if($articleStart== $pagesNumber){
          $current='current' ;
        }
        else $current='' ; 
        $number = $i+1 ;
        echo'<li class="'.$current.'"><a href="http://medlanes.com/blog?pageN='.$number.'&pages='.$pagesNumber.'&tags='.$tagSearch.'&q='.$searchQuery.'">'.$number.'</a></li>' ;
    }
    echo "<li class='arrow'><a href='http://medlanes.com/blog?pageN=".($currentPage+1)."&pages=".($articleStart+5)."&tags=".$tagSearch."&q=".$searchQuery."'> Next &raquo;</a></li></ul>" ;
    
     }
    else if (($articleStart==0) && ($totalPosts>5)) {
            $positive = 9 ;
     echo '<ul style="width:44% !important" class="pagination pagination-list" >' ;
     for($i=0;$i<$currentPage+$positive;$i++){
        $pagesNumber = $i*5 ;
        if($articleStart== $pagesNumber){
          $current='current' ;
        }
        else $current='' ; 
        $number = $i+1 ;
        echo'<li class="'.$current.'"><a href="http://medlanes.com/blog?pageN='.$number.'&pages='.$pagesNumber.'&tags='.$tagSearch.'&q='.$searchQuery.'">'.$number.'</a></li>' ;
    }
    echo '<li class="arrow"><a href="http://medlanes.com/blog?pageN='.($currentPage+1).'&pages='.($articleStart+5).'&tags='.$tagSearch.'&q='.$searchQuery.'"> Next &raquo;</a></li></ul>' ;
    }
    else if (($articleStart==0) && ($totalPosts!=0) && ($totalPosts<5)) {
    echo '<a  href="http://medlanes.com/blog"><i class="fa fa-arrow-left"></i> Show All Posts</a>' ;
    }

    else if ($totalPosts==0) {
          echo '<div class="top-row">
                  <div class="large-6 columns blog-no-results" style="padding : 60px !important">
                    <p>Your search did not match any blog post, please try with another search query...</p>
                  </div>
                 <div class="large-6 columns">
                  <img src="https://medlanes.com/images/404.png" /> 
                  </div>
                </div>' ;
          echo '<a  href="http://medlanes.com/blog"><i class="fa fa-arrow-left"></i> Show All Posts</a>' ;
    }
     else{
       $negative = 9 ;
       echo '<ul class="pagination pagination-list" style="width:62% !important"><li class="arrow unavailable"><a href="http://medlanes.com/blog?pageN='.($currentPage-1).'pages=$articleStart-5&tags='.$tagSearch.'">&laquo; Previous</a></li>' ;
    for($i=$currentPage-$negative;$i<$numberPages;$i++){
        $pagesNumber = $i*5 ;
        if($articleStart== $pagesNumber){
          $current='current' ;
        }
        else $current='' ; 
        $number = $i+1 ;
        echo'<li class="'.$current.'"><a href="http://medlanes.com/blog?pageN='.$number.'&pages='.$pagesNumber.'&tags='.$tagSearch.'&q='.$searchQuery.'">'.$number.'</a></li>' ;
    }
    echo '</ul>' ;
    }
          ?>
  
  </div>
   
  <div class="row">
            <?php include("footer.php"); ?>

  </div>

<script src="<?php echo $url ?>cms/cms_pages/js/foundation.min.js"></script>
<script>
$(document).foundation();
</script>
</body>
</html>
