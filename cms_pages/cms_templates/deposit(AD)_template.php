<?php

require_once('cms_api_call.php') ;
$url = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/';
?>

  <!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?php echo $dataContent[$order]; $order++ ?></title>
    <link rel="icon" type="<?php echo $url ?>cms/cms_pages/askadoctor/image/ico" href="images/favicon.ico">
	<!-- css -->
	<link rel="stylesheet" href="<?php echo $url ?>cms/cms_pages/askadoctor/css/global.css">
	<link rel="stylesheet" href="<?php echo $url ?>cms/cms_pages/askadoctor/css/deposit.css">


	<!-- scripts -->
	<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.2.min.js"></script>
	<script type="text/javascript" src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

	<script src="<?php echo $url ?>cms/cms_pages/askadoctor/js/us/switchpaymentEN.js"></script>
	<script type="text/javascript" src="<?php echo $url ?>cms/cms_pages/askadoctor/js/jquery.payment.js"></script>
	
	<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
	<script type="text/javascript" src="<?php echo $url ?>cms/cms_pages/askadoctor/js/jquery.bxslider.js"></script>
	<script type="text/javascript" src="<?php echo $url ?>cms/cms_pages/askadoctor/js/us/deposit.js"></script>



	<style>
		.fshow{
			display:block;
			float:left;
			margin-top:13px;
		}
		.fhide{
			display:none;
		}
		.error{
			float:left;
			width: 100% !important;
			font-weight: bold;
			color:red;
		}
		#continuePaypal{
			border:none !important;
		}
		.showit{
			display:none;
		}
		#continuePaypal{
			display:none;
		}
		.hideonfunnel{
			display:<br />
<font size='1'><table class='xdebug-error xe-notice' dir='ltr' border='1' cellspacing='0' cellpadding='1'>
<tr><th align='left' bgcolor='#f57900' colspan="5"><span style='background-color: #cc0000; color: #fce94f; font-size: x-large;'>( ! )</span> Notice: Undefined variable: paypal in C:\wamp\www\askadoctor\deposit.php on line <i>52</i></th></tr>
<tr><th align='left' bgcolor='#e9b96e' colspan='5'>Call Stack</th></tr>
<tr><th align='center' bgcolor='#eeeeec'>#</th><th align='left' bgcolor='#eeeeec'>Time</th><th align='left' bgcolor='#eeeeec'>Memory</th><th align='left' bgcolor='#eeeeec'>Function</th><th align='left' bgcolor='#eeeeec'>Location</th></tr>
<tr><td bgcolor='#eeeeec' align='center'>1</td><td bgcolor='#eeeeec' align='center'>0.0430</td><td bgcolor='#eeeeec' align='right'>273824</td><td bgcolor='#eeeeec'>{main}(  )</td><td title='C:\wamp\www\askadoctor\deposit.php' bgcolor='#eeeeec'>..\deposit.php<b>:</b>0</td></tr>
</table></font>
;
		}

		.docpanel{
			font-weight: bold;
		}
		.docpanel p:nth-of-type(4){
			float: left;
			margin-right: 5px;
		}
		.testimonial p:nth-child(3){
			margin-top:-10px;
			text-align: right;
		}
		.testimonial p:nth-child(2){
			text-align: right;
		}
		.right .who p:nth-of-type(2),.right .who p:nth-of-type(3),.right .who p:nth-of-type(4){
			margin-top:0px;
			margin-bottom:0px;
			font-style: italic;
			font-weight: bold;
		}

	</style>

</head>
<body>




<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WKLFHR"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>


<!-- End Google Tag Manager -->

	<div class="clearfix header-wrapper">
		<div class="content">
			<div class="sub-wrap">
				<div class="left">
					<div class="head">
						<a href="#"><img src="<?php echo $url ?>cms/cms_assets/<?php echo $dataContent[$order]; $order++ ?>" alt="<?php echo $dataContent[$order]; $order++ ?>"/>
						</a>
						<p><?php echo $dataContent[$order]; $order++ ?></p>
					</div>
				</div>
				<div class="right">
					<!--<p><?php//echo $lang['hiw_title']; ?></p>-->
					<div class="one">
						<img src="<?php echo $url ?>cms/cms_pages/askadoctor/images/trust.png" class="pull-left" />
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="checkoutprogress">
		<ul id="progress">
		    <li class="active"><p><i class="ask-queston-icon"></i><?php echo $dataContent[$order]; $order++ ?></p><b></b></li>
		    <li class="active"><p><i class="add-info-icon"></i><?php echo $dataContent[$order]; $order++ ?></p></li>
		    <li class="active"><p><i class="option-info-icon"></i><?php echo $dataContent[$order]; $order++ ?></p></li>
		    <li><p><i class="answer-icon"></i><?php echo $dataContent[$order]; $order++ ?></p></li>
		    <li></li>
		</ul>
	</div>
	<div class="content sub-content">
		<div class="sub-wrap">

			<div class="left">
				<div class="obox">
					<h1><?php echo $dataContent[$order]; $order++ ?></h1>
					<p><?php echo $dataContent[$order]; $order++ ?><b><?php echo $dataContent[$order]; $order++ ?></b><b class="amm"> <?php echo $dataContent[$order]; $order++ ?> </b><?php echo $dataContent[$order]; $order++ ?></p>
				</div>
				<div class="docpanel">
					<div class="profile-img">
						<img class="depositdoctor" src="<?php echo $url ?>cms/cms_assets/<?php echo $dataContent[$order]; $order++ ?>" alt="<?php echo $dataContent[$order]; $order++ ?>">
					</div>
					<div class="profile-desc">
						<p><b><?php echo $dataContent[$order]; $order++ ?></b></p>
						<p><?php echo $dataContent[$order]; $order++ ?></p>
						<p><?php echo $dataContent[$order]; $order++ ?></p>		
						<p style='color:green'><b><?php echo $dataContent[$order]; $order++ ?></b></p>								
						<div class="watch">
						<p style="margin-top:17px"><?php echo $dataContent[$order]; $order++ ?></p>
						<p><?php echo $dataContent[$order]; $order++ ?></p>
						</div>
					</div>
				</div>
				<div class="paymentswap">
					<div class="one">
						<p><?php echo $dataContent[$order]; $order++ ?></p>
						<select name="option" id="option">
							<option value="credit"><?php echo $dataContent[$order]; $order++ ?></option>
							<option class="hideonfunnel" value="paypal">PayPal</option>
						</select>
						<img style="float:left;margin-left:10px;" src="<?php echo $url ?>cms/cms_pages/askadoctor/images/payment-icon.png" alt="">
					</div>
					<div class="one showit">
						<p><?php echo $dataContent[$order]; $order++ ?></p>
					</div>
					<div class="one hideit">
						<p><?php echo $dataContent[$order]; $order++ ?></p>
						<input type="text" name="cn" id="cn">
					</div>
					<div class="one hideit">
						<p><?php echo $dataContent[$order]; $order++ ?></p>
						<select style="margin-right:10px;" name="month"  id="month">
							<option value="-1"><?php echo $dataContent[$order]; $order++ ?></option>
							<option value="1">01 - Jan</option>
							<option value="2">02 - Feb</option>
							<option value="3">03 - Mar</option>
							<option value="4">04 - Apr</option>
							<option value="5">05 - May</option>
							<option value="6">06 - Jun</option>
							<option value="7">07 - Jul</option>
							<option value="8">08 - Aug</option>
							<option value="9">09 - Sep</option>
							<option value="10">10 - Oct</option>
							<option value="11">11 - Nov</option>
							<option value="12">12 - Dec</option>						
						</select>
						<select name="year" id="year">
							<option value="-1"><?php echo $dataContent[$order]; $order++ ?></option>
							<option value="2015">2015</option>
							<option value="2016">2016</option>
							<option value="2017">2017</option>
							<option value="2018">2018</option>
							<option value="2019">2019</option>
							<option value="2020">2020</option>
							<option value="2021">2021</option>
							<option value="2022">2022</option>
							<option value="2023">2023</option>
							<option value="2024">2024</option>
						</select>
					</div>
					<div class="one hideit">
						<p><?php echo $dataContent[$order]; $order++ ?></p>
						<input style="width:50px" type="text" maxlength="4" minlength="3" name="sc" id="sc">
                        <img id="cvv_exp"  style="cursor:pointer;" class="helper" src="<?php echo $url ?>cms/cms_pages/askadoctor/images/icon-help.gif" alt="Where to find your Security Code"/>
                        <div class="cvc-tooltip">
                          <h3><?php echo $dataContent[$order]; $order++ ?></h3>
                          <p><?php echo $dataContent[$order]; $order++ ?> </p>
                          <div class="left">
                            <img src="<?php echo $url ?>cms/cms_pages/askadoctor/images/cc_back.png" alt="">
                          </div>
                          <div class="right">
                            <h4>Visa / Mastercard:</h4>
                            <p><?php echo $dataContent[$order]; $order++ ?></p>
                          </div>
                          <div class="left">
                            <img src="<?php echo $url ?>cms/cms_pages/askadoctor/images/amex.png" alt="">
                          </div>
                          <div class="right">
                            <h4>American Express:</h4>
                            <p><?php echo $dataContent[$order]; $order++ ?></p>
                          </div>
                        </div>
					</div>
					<div class="two">
						<img src="<?php echo $url ?>cms/cms_pages/askadoctor/images/padlock.gif" alt="#"/>
						<p><?php echo $dataContent[$order]; $order++ ?> <a target="_blank" href="<?php echo $dataContent[$order]; $order++ ?>"><?php echo $dataContent[$order]; $order++ ?></a></p>
					</div>
				</div><!-- end paymentswap -->
				<div class="checkoutnow">
					<p class="fineprint"><?php echo $dataContent[$order]; $order++ ?><a target="_blank" href="<?php echo $dataContent[$order]; $order++ ?>"><?php echo $dataContent[$order]; $order++ ?></a><?php echo $dataContent[$order]; $order++ ?></p>
				</div>
				<div class="endcta">
				<form method="post" style="display:none" id="wrform"  action="waitingroom.php">
					<input type="hidden" id="stripe" name="stripe" value="stripe">
					<input type="hidden" id="amount" name="amount" value="amount">
					<input type="hidden" id="email" name="email" value="email">
					<input type="hidden" id="token" name="token" value="token">
					<input type="hidden" id="ticket" name="ticket" value="ticket">
					<input type="hidden" id="creditcard" name="creditcard" value="creditcard">
					<input type="submit" value="Continue">
					<img src="<?php echo $url ?>cms/cms_pages/askadoctor/images/expert-satisfaction.png" alt="">
				</form>
				 <form class="paypalForm" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
					<input type="hidden" name="cmd" value="_s-xclick">
					<input type="hidden" value="http://askadoctor.today/thankyou.php" name="return">
					<input type="hidden" id="codePaypal" name="hosted_button_id" value="TGENHMWWTNY28">
					<input type="hidden" name="custom" class="custom" value="funnel=ja&company=askadoctor&geo_city=Chennai&geo_country=India">
					<input type="submit" name="submit" id="continuePaypal" value="Continue" class="noexit JA_imageButton submitBtnClass JA_oneclick cc-page-submit-original" src="images/continue_off.gif" onmouseout="this.src = 'images/continue_off.gif'" onmouseover="this.src = 'images/continue_on.gif'" alt="Continue" style="border-width:0px;" nt_name="continueButton" nte_event="customload" nte_usertype="usertype" nte_inputclicked="inputclicked" onclick="processClick();" nttracked="1">
					<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
				</form>
					<a  id="removeclick" onclick="processClick(paymentvalidation);" style="cursor:pointer;" class="continue"><?php echo $dataContent[$order]; $order++ ?></a>
				</div>
			</div><!-- end left -->
			<!-- Start Right -->
				<div class="right">
				<div class="two">
					<img class="doc" src="<?php echo $url ?>cms/cms_assets/<?php echo $dataContent[$order]; $order++ ?>" alt="<?php echo $dataContent[$order]; $order++ ?>">
					<div class="one">
							<p><?php echo $dataContent[$order]; $order++ ?><br><br><b><?php echo $dataContent[$order]; $order++ ?></b></p></div>

				</div>	
			</div>
			<!-- End Right -->
		</div>
	</div>
	<div id="maps" style="display:none"></div>
<footer>
<div class="sub-wrap">
	<div class="mini-wrap-two clearfix">
		<div class="one two">
			<ul>
				<li><a href="<?php echo $dataContent[$order]; $order++ ?>"><?php echo $dataContent[$order]; $order++ ?></a></li>
				<li><a href="<?php echo $dataContent[$order]; $order++ ?>"><?php echo $dataContent[$order]; $order++ ?></a></li>
				<li><a href="<?php echo $dataContent[$order]; $order++ ?>"><?php echo $dataContent[$order]; $order++ ?></a></li>
			</ul>		
		</div>
		<div class="one two">
			<ul>
				<li><a target="_blank" href="<?php echo $dataContent[$order]; $order++ ?>"><?php echo $dataContent[$order]; $order++ ?></a></li>
				<li><a target="_blank" href="<?php echo $dataContent[$order]; $order++ ?>"><?php echo $dataContent[$order]; $order++ ?></a></li>
			</ul>								
		</div>
		<div class="support">
			<ul>
			<li><?php echo $dataContent[$order]; $order++ ?></li>
			<li><?php echo $dataContent[$order]; $order++ ?>:</li>
			</ul>
		</div>

		<div class="one">
			<img src="<?php echo $url ?>cms/cms_pages/askadoctor/images/supported-by.png" class="supported" />
		</div>
	</div>

</footer>

<script type="text/javascript">
(function(d, src, c) { var t=d.scripts[d.scripts.length - 1],s=d.createElement('script');s.id='la_x2s6df8d';s.async=true;s.src=src;s.onload=s.onreadystatechange=function(){var rs=this.readyState;if(rs&&(rs!='complete')&&(rs!='loaded')){return;}c(this);};t.parentElement.insertBefore(s,t.nextSibling);})(document,
'//medlanes.ladesk.com/scripts/track.js',
function(e){ LiveAgent.createButton('e022f5bc', e); });
</script>

</body>
</html>
