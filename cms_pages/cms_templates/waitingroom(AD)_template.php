<?php

require_once('cms_api_call.php') ;
$url = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/';
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?php echo $dataContent[$order]; $order++ ?></title>

	<!-- css -->
	<link rel="stylesheet" href="<?php echo $url ?>cms/cms_pages/askadoctor/css/global.css">
	<link rel="stylesheet" href="<?php echo $url ?>cms/cms_pages/askadoctor/css/waitingroom.css">
<link rel="icon" type="image/ico" href="<?php echo $url ?>cms/cms_pages/askadoctor/images/favicon.ico">

	<!-- scripts -->
	<script src="https://code.jquery.com/jquery-1.11.2.min.js"></script>
	<script src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

	<style>
		.fshow{
			display:block;
			float:left;
			margin-top:13px;
		}
		.fhide{
			display:none;
		}
	</style>

</head>
<body>

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WKLFHR"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WKLFHR');</script>
<!-- End Google Tag Manager -->


	<div class="clearfix header-wrapper">
		<div class="content">
			<div class="sub-wrap">
				<div class="left">
					<div class="head">
						<a href="#"><img src="<?php echo $url ?>cms/cms_assets/<?php echo $dataContent[$order]; $order++ ?>" alt="<?php echo $dataContent[$order]; $order++ ?>"/>
						</a>
						<p><?php echo $dataContent[$order]; $order++ ?></p>
					</div>
				</div>
				<div class="right">
					<!--<p><?php//echo $lang['hiw_title']; ?></p>-->
					<div class="one">
						<img src="<?php echo $url ?>cms/cms_pages/askadoctor/images/trust.png" class="pull-left" />
						<a href="#" class="btn"><?php echo $dataContent[$order]; $order++ ?></a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="checkoutprogress">
		<ul id="progress" class="waitingroom">
		    <li class="active"><p><i class="ask-queston-icon"></i><?php echo $dataContent[$order]; $order++ ?></p><b></b></li>
		    <li class="active"><p><i class="add-info-icon"></i><?php echo $dataContent[$order]; $order++ ?></p></li>
		    <li class="active"><p><i class="option-info-icon"></i><?php echo $dataContent[$order]; $order++ ?></p></li>
		    <li class="active"><p><i class="answer-icon"></i><?php echo $dataContent[$order]; $order++ ?></p></li>
		    <li></li>
		</ul>
	</div>
<div class="content content-padd">
		<div class="sub-wrap">
			<div class="left">
				<div class="obox">
					<h1><?php echo $dataContent[$order]; $order++ ?><br><span><?php echo $dataContent[$order]; $order++ ?></span></h1>
				</div>
				<div class="thanks">
					

					<div class="box clearfix">
						<h2 class="blueout"><?php echo $dataContent[$order]; $order++ ?></h2>
						<div class="one">
							<p><span>1.</span><?php echo $dataContent[$order]; $order++ ?></p>
						</div>	
						<div class="one">
							<p><span>2.</span><?php echo $dataContent[$order]; $order++ ?></p>
						</div>
						<div class="clearfix"></div>
						<h2 class="blueout"><?php echo $dataContent[$order]; $order++ ?></h2>

						<div class="one">
							<p><?php echo $dataContent[$order]; $order++ ?></p>
					</div>
					</div>

				</div>
			</div><!-- end left -->
			<div class="right medical-seal">
				<img src="<?php echo $url ?>cms/cms_pages/askadoctor/images/medical-seal.png" alt="#">
				
			</div>
		</div>
	</div>
	
	<footer>
<div class="sub-wrap">
	<div class="mini-wrap-two clearfix">
		<div class="one two">
			<ul>
				<li><a href="<?php echo $dataContent[$order]; $order++ ?>"><?php echo $dataContent[$order]; $order++ ?></a></li>
				<li><a href="<?php echo $dataContent[$order]; $order++ ?>"><?php echo $dataContent[$order]; $order++ ?></a></li>
				<li><a href="<?php echo $dataContent[$order]; $order++ ?>"><?php echo $dataContent[$order]; $order++ ?></a></li>
			</ul>		
		</div>
		<div class="one two">
			<ul>
				<li><a target="_blank" href="<?php echo $dataContent[$order]; $order++ ?>"><?php echo $dataContent[$order]; $order++ ?></a></li>
				<li><a target="_blank" href="<?php echo $dataContent[$order]; $order++ ?>"><?php echo $dataContent[$order]; $order++ ?></a></li>
			</ul>								
		</div>
		<div class="support">
			<ul>
			<li><?php echo $dataContent[$order]; $order++ ?></li>
			<li><?php echo $dataContent[$order]; $order++ ?>:</li>
			</ul>
		</div>

		<div class="one">
			<img src="<?php echo $url ?>cms/cms_pages/askadoctor/images/supported-by.png" class="supported" />
		</div>
	</div>

</footer>
</body>
</html>