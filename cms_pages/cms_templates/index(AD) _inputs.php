
      <div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title">Header</h3>
  </div>
  <div class="panel-body" >
    <form class="form-horizontal">
                <cms-input cms-input-id='heading_title' cms-input-label="Page Title Metatag"    cms-input-region="heading" ></cms-input>
          <cms-picture cms-input-id='heading_picture_1' cms-input-label="Background Image"    cms-input-region="heading" ></cms-picture>
          <cms-picture cms-input-id='heading_picture_2' cms-input-label="Logo Image"    cms-input-region="heading" ></cms-picture>
          <cms-input cms-input-id='heading_text_3' cms-input-label="Logo Title"   cms-input-default='Ask a medical question and get a fast answer'  cms-input-region="heading" ></cms-input>
          <cms-input cms-input-id='heading_text_4' cms-input-label="Login"   cms-input-default='Login Button Text'  cms-input-region="heading" ></cms-input>

</div>
</div>
  </form>



      <div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title">Page Core</h3>
  </div>
  <div class="panel-body" >
    <form class="form-horizontal">
          <cms-input cms-input-id='core_text_5' cms-input-label="Page Title"    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_7' cms-input-label="1st Disease Title"    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_8' cms-input-label="1st Disease Placeholder"     cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_9' cms-input-label="2nd Disease Title"    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_10' cms-input-label="2nd Disease Placeholder"    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_11' cms-input-label="3rd Disease Title"     cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_12' cms-input-label="3rd Disease Placeholder"    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_13' cms-input-label="4th Disease Title"    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_14' cms-input-label="4th Disease Placeholder"     cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_15' cms-input-label="5th Disease Title"     cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_16' cms-input-label="5th Disease Placeholder"     cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_17' cms-input-label="6th Disease Title"     cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_18' cms-input-label="6th Disease Placeholder"     cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_19' cms-input-label="7th Disease Title"     cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_20' cms-input-label="7th Disease Placeholder"    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_21' cms-input-label="Text Box Placeholder"    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_22' cms-input-label="Ask a doctor now button"    cms-input-region="core" ></cms-input>

          <cms-input cms-input-id='core_text_23' cms-input-label="Email Placeholder"    cms-input-region="core" ></cms-input>
         <!--  <cms-input cms-input-id='core_text_23' cms-input-label="Password Label"    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_24' cms-input-label="Email error message"    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_25' cms-input-label="Password error message"  cms-input-default='Please sign with your password. We’ve send it to you by email when you asked your first question. '   cms-input-region="core" ></cms-input> -->
          <cms-input cms-input-id='core_text_26' cms-input-label="Ask a doctor in the news Label"    cms-input-region="core" ></cms-input>
          <cms-input cms-input-id='core_text_27' cms-input-label="Press SVG Image"    cms-input-region="press" ></cms-input>
          <cms-input cms-input-id='core_text_28' cms-input-label="Press SVG Image"    cms-input-region="press" ></cms-input>
          <cms-input cms-input-id='core_text_29' cms-input-label="Press SVG Image"    cms-input-region="press" ></cms-input>
          <cms-input cms-input-id='core_text_30' cms-input-label="Press SVG Image"    cms-input-region="press" ></cms-input>
          <cms-input cms-input-id='core_text_31' cms-input-label="Press SVG Image"    cms-input-region="press" ></cms-input>
          <cms-input cms-input-id='core_text_32' cms-input-label="Press SVG Image"    cms-input-region="press" ></cms-input>

</div>
</div>
  </form>


      <div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title">Doctors</h3>
  </div>
  <div class="panel-body" >
    <form class="form-horizontal">
          <cms-input cms-input-id='doctors_text_33' cms-input-label="Meet the doctors title"    cms-input-region="doctors" ></cms-input>
          <cms-picture cms-input-id='doctors_picture_34' cms-input-label="Doctor Image"    cms-input-region="doctors" ></cms-picture>
          <cms-input cms-input-id='doctors_text_35' cms-input-label="Doctor Rating"    cms-input-region="doctors" ></cms-input>
          <cms-input cms-input-id='doctors_text_36' cms-input-label="Doctor Name"     cms-input-region="doctors" ></cms-input>
          <cms-input cms-input-id='doctors_text_37' cms-input-label="Board Satisfied in family medicine..."    cms-input-region="doctors" ></cms-input>
          <cms-input cms-input-id='doctors_text_38' cms-input-label="Satisfied Customers"    cms-input-region="doctors" ></cms-input>
          <cms-input cms-input-id='doctors_text_39' cms-input-label="Percentage"     cms-input-region="doctors" ></cms-input>
          <cms-input cms-input-id='doctors_text_40' cms-input-label="Experience Text"     cms-input-region="doctors" ></cms-input>

          <cms-picture cms-input-id='doctors_picture_41' cms-input-label="Doctor Image 2"    cms-input-region="doctors" ></cms-picture>
          <cms-input cms-input-id='doctors_text_42' cms-input-label="Doctor Rating 2"    cms-input-region="doctors" ></cms-input>
          <cms-input cms-input-id='doctors_text_43' cms-input-label="Doctor Name 2 "     cms-input-region="doctors" ></cms-input>
          <cms-input cms-input-id='doctors_text_44' cms-input-label="Board Satisfied in family medicine... 2"    cms-input-region="doctors" ></cms-input>
          <cms-input cms-input-id='doctors_text_45' cms-input-label="Satisfied Customers 2"    cms-input-region="doctors" ></cms-input>
          <cms-input cms-input-id='doctors_text_46' cms-input-label="Percentage 2"     cms-input-region="doctors" ></cms-input>
          <cms-input cms-input-id='doctors_text_47' cms-input-label="Experience Text 2"     cms-input-region="doctors" ></cms-input>


          <cms-picture cms-input-id='doctors_picture_48' cms-input-label="Doctor Image 3"    cms-input-region="doctors" ></cms-picture>
          <cms-input cms-input-id='doctors_text_49' cms-input-label="Doctor Rating 3"    cms-input-region="doctors" ></cms-input>
          <cms-input cms-input-id='doctors_text_50' cms-input-label="Doctor Name 3"     cms-input-region="doctors" ></cms-input>
          <cms-input cms-input-id='doctors_text_51' cms-input-label="Board Satisfied in family medicine... 3"    cms-input-region="doctors" ></cms-input>
          <cms-input cms-input-id='doctors_text_52' cms-input-label="Satisfied Customers  3"    cms-input-region="doctors" ></cms-input>
          <cms-input cms-input-id='doctors_text_53' cms-input-label="Percentage 3"     cms-input-region="doctors" ></cms-input>
          <cms-input cms-input-id='doctors_text_54' cms-input-label="Experience Text 3"     cms-input-region="doctors" ></cms-input>


          <cms-picture cms-input-id='doctors_picture_55' cms-input-label="Doctor Image 4"    cms-input-region="doctors" ></cms-picture>
          <cms-input cms-input-id='doctors_text_56' cms-input-label="Doctor Rating 4"    cms-input-region="doctors" ></cms-input>
          <cms-input cms-input-id='doctors_text_57' cms-input-label="Doctor Name 4"     cms-input-region="doctors" ></cms-input>
          <cms-input cms-input-id='doctors_text_58' cms-input-label="Board Satisfied in family medicine... 4"    cms-input-region="doctors" ></cms-input>
          <cms-input cms-input-id='doctors_text_59' cms-input-label="Satisfied Customers 4"    cms-input-region="doctors" ></cms-input>
          <cms-input cms-input-id='doctors_text_60' cms-input-label="Percentage 4"     cms-input-region="doctors" ></cms-input>
          <cms-input cms-input-id='doctors_text_61' cms-input-label="Experience Text 4"     cms-input-region="doctors" ></cms-input>


          <cms-picture cms-input-id='doctors_picture_62' cms-input-label="Doctor Image 5"    cms-input-region="doctors" ></cms-picture>
          <cms-input cms-input-id='doctors_text_63' cms-input-label="Doctor Rating 5"    cms-input-region="doctors" ></cms-input>
          <cms-input cms-input-id='doctors_text_64' cms-input-label="Doctor Name 5"     cms-input-region="doctors" ></cms-input>
          <cms-input cms-input-id='doctors_text_65' cms-input-label="Board Satisfied in family medicine... 5"    cms-input-region="doctors" ></cms-input>
          <cms-input cms-input-id='doctors_text_66' cms-input-label="Satisfied Customers 5"    cms-input-region="doctors" ></cms-input>
          <cms-input cms-input-id='doctors_text_67' cms-input-label="Percentage 5"     cms-input-region="doctors" ></cms-input>
          <cms-input cms-input-id='doctors_text_68' cms-input-label="Experience Text 5"     cms-input-region="doctors" ></cms-input>


          <cms-picture cms-input-id='doctors_picture_69' cms-input-label="Doctor Image 6"    cms-input-region="doctors" ></cms-picture>
          <cms-input cms-input-id='doctors_text_70' cms-input-label="Doctor Rating 6"    cms-input-region="doctors" ></cms-input>
          <cms-input cms-input-id='doctors_text_71' cms-input-label="Doctor Name 6"     cms-input-region="doctors" ></cms-input>
          <cms-input cms-input-id='doctors_text_72' cms-input-label="Board Satisfied in family medicine... 6"    cms-input-region="doctors" ></cms-input>
          <cms-input cms-input-id='doctors_text_73' cms-input-label="Satisfied Customers 6"    cms-input-region="doctors" ></cms-input>
          <cms-input cms-input-id='doctors_text_74' cms-input-label="Percentage 6"     cms-input-region="doctors" ></cms-input>
          <cms-input cms-input-id='doctors_text_75' cms-input-label="Experience Text 6"     cms-input-region="doctors" ></cms-input>

          <cms-picture cms-input-id='doctors_picture_76' cms-input-label="Doctor Image 7"    cms-input-region="doctors" ></cms-picture>
          <cms-input cms-input-id='doctors_text_77' cms-input-label="Doctor Rating 7"    cms-input-region="doctors" ></cms-input>
          <cms-input cms-input-id='doctors_text_78' cms-input-label="Doctor Name 7"     cms-input-region="doctors" ></cms-input>
          <cms-input cms-input-id='doctors_text_79' cms-input-label="Board Satisfied in family medicine... 7"    cms-input-region="doctors" ></cms-input>
          <cms-input cms-input-id='doctors_text_80' cms-input-label="Satisfied Customers 7"    cms-input-region="doctors" ></cms-input>
          <cms-input cms-input-id='doctors_text_81' cms-input-label="Percentage 7"     cms-input-region="doctors" ></cms-input>
          <cms-input cms-input-id='doctors_text_82' cms-input-label="Experience Text 7"     cms-input-region="doctors" ></cms-input>


          <cms-picture cms-input-id='doctors_picture_83' cms-input-label="Doctor Image 8"    cms-input-region="doctors" ></cms-picture>
          <cms-input cms-input-id='doctors_text_84' cms-input-label="Doctor Rating 8"    cms-input-region="doctors" ></cms-input>
          <cms-input cms-input-id='doctors_text_85' cms-input-label="Doctor Name 8"     cms-input-region="doctors" ></cms-input>
          <cms-input cms-input-id='doctors_text_86' cms-input-label="Board Satisfied in family medicine... 8"    cms-input-region="doctors" ></cms-input>
          <cms-input cms-input-id='doctors_text_87' cms-input-label="Satisfied Customers 8"    cms-input-region="doctors" ></cms-input>
          <cms-input cms-input-id='doctors_text_88' cms-input-label="Percentage 8"     cms-input-region="doctors" ></cms-input>
          <cms-input cms-input-id='doctors_text_89' cms-input-label="Experience Text 8"     cms-input-region="doctors" ></cms-input>


</div>
</div>
  </form>


 <div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title">Testimonials</h3>
  </div>
  <div class="panel-body" >
    <form class="form-horizontal">
          <cms-input cms-input-id='testimonials_text_90' cms-input-label="Testimonials Title"    cms-input-region="testimonials" ></cms-input>
          


          <cms-picture cms-input-id='testimonials_picture_91' cms-input-label="Testimonial Picture"    cms-input-region="testimonials" ></cms-picture>
          <cms-input cms-input-id='testimonials_text_92' cms-input-label="Testimonial Text"    cms-input-region="testimonials" ></cms-input>
          <cms-input cms-input-id='testimonials_text_93' cms-input-label="Testimonial Name"    cms-input-region="testimonials" ></cms-input>
          <cms-input cms-input-id='testimonials_text_94' cms-input-label="Testimonial Country"     cms-input-region="testimonials" ></cms-input>
          <cms-input cms-input-id='testimonials_text_95' cms-input-label="Ask A Doctor Now Button"    cms-input-region="testimonials" ></cms-input>



          <cms-picture cms-input-id='testimonials_picture_96' cms-input-label="Testimonial Picture"    cms-input-region="testimonials" ></cms-picture>
          <cms-input cms-input-id='testimonials_text_97' cms-input-label="Testimonial Text"    cms-input-region="testimonials" ></cms-input>
          <cms-input cms-input-id='testimonials_text_98' cms-input-label="Testimonial Name"    cms-input-region="testimonials" ></cms-input>
          <cms-input cms-input-id='testimonials_text_99' cms-input-label="Testimonial Country"     cms-input-region="testimonials" ></cms-input>
          <cms-input cms-input-id='testimonials_text_100' cms-input-label="Ask A Doctor Now Button"    cms-input-region="testimonials" ></cms-input>



          <cms-picture cms-input-id='testimonials_picture_101' cms-input-label="Testimonial Picture"    cms-input-region="testimonials" ></cms-picture>
          <cms-input cms-input-id='testimonials_text_102' cms-input-label="Testimonial Text"    cms-input-region="testimonials" ></cms-input>
          <cms-input cms-input-id='testimonials_text_103' cms-input-label="Testimonial Name"    cms-input-region="testimonials" ></cms-input>
          <cms-input cms-input-id='testimonials_text_104' cms-input-label="Testimonial Country"     cms-input-region="testimonials" ></cms-input>
          <cms-input cms-input-id='testimonials_text_105' cms-input-label="Ask A Doctor Now Button"    cms-input-region="testimonials" ></cms-input>


          <cms-picture cms-input-id='testimonials_picture_120' cms-input-label="Testimonial Picture"    cms-input-region="testimonials" ></cms-picture>
          <cms-input cms-input-id='testimonials_text_121' cms-input-label="Testimonial Text"    cms-input-region="testimonials" ></cms-input>
          <cms-input cms-input-id='testimonials_text_122' cms-input-label="Testimonial Name"    cms-input-region="testimonials" ></cms-input>
          <cms-input cms-input-id='testimonials_text_123' cms-input-label="Testimonial Country"     cms-input-region="testimonials" ></cms-input>
          <cms-input cms-input-id='testimonials_text_124' cms-input-label="Ask A Doctor Now Button"    cms-input-region="testimonials" ></cms-input>

</div>
</div>
  </form>


                 
    
 <div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title">Footer</h3>
  </div>
  <div class="panel-body" >
    <form class="form-horizontal">
          <cms-input cms-input-id='footer_text_107' cms-input-label="Medlanes Link"    cms-input-region="footer" ></cms-input>
          <cms-input cms-input-id='footer_text_108' cms-input-label="Medlanes Label"    cms-input-region="footer" ></cms-input>
          <cms-input cms-input-id='footer_text_109' cms-input-label="Support Link"    cms-input-region="footer" ></cms-input>
          <cms-input cms-input-id='footer_text_110' cms-input-label="Support Label"    cms-input-region="footer" ></cms-input>

          <cms-input cms-input-id='footer_text_111' cms-input-label="Contact us Link"    cms-input-region="footer" ></cms-input>
          <cms-input cms-input-id='footer_text_112' cms-input-label="Contact us Label"    cms-input-region="footer" ></cms-input>

          <cms-input cms-input-id='footer_text_113' cms-input-label="terms Link"    cms-input-region="footer" ></cms-input>
          <cms-input cms-input-id='footer_text_114' cms-input-label="terms Label"    cms-input-region="footer" ></cms-input>

          <cms-input cms-input-id='footer_text_115' cms-input-label="Privacy Link"    cms-input-region="footer" ></cms-input>
          <cms-input cms-input-id='footer_text_116' cms-input-label="Privacy Label"    cms-input-region="footer" ></cms-input>

          <cms-input cms-input-id='footer_text_117' cms-input-label="Copyright Text"    cms-input-region="footer" ></cms-input>
          <cms-input cms-input-id='footer_text_118' cms-input-label="Ask a doctor supported by text"    cms-input-region="footer" ></cms-input>


</div>
</div>
  </form>


                 
                                                                                                      