
	<header>
			<div class="logo" style='background:url(<?php echo $url ?>cms/cms_pages/img/logo.png) no-repeat;'>
				<a href="http://medlanes.com/" style='background:url(..) no-repeat; !important'>Medlanes</a>
			</div>
		      <a href="#" class="humb">
		        <span></span>
		      </a>
			<nav>

				<ul class='header'>
					<li  >
						<a href="http://medlanes.com/our-service.php">Our Service</a>
					</li>
					<li  >
						<a href="http://medlanes.com/doctors.php">Doctors</a>
					</li>
					<li  >
						<a href="http://medlanes.com/faq.php">FAQ</a>
					</li>
					<li class='diseases active'  >
						<a href="#">Diseases</a>
						<div class='diseases-dropdown' >
							 <ul>
								<li><a href="http://medlanes.com/diseases/allergies">Allergies</a></li>
						 		<li><a href="http://medlanes.com/diseases/anemia">Anemia</a></li>
						 		<li><a href="http://medlanes.com/diseases/arthritis">Arthritis</a></li>
						 		<li><a href="http://medlanes.com/diseases/asthma-and-copd">Asthma and COPD</a></li>
						 		<li><a href="http://medlanes.com/diseases/autoimmune-disorders">Autoimmune Disorders</a></li>
						 		<li><a href="http://medlanes.com/diseases/blood-disorders">Blood Disorders</a></li>
						 		<li><a href="http://medlanes.com/diseases/brain-and-nervous-system">Brain and Nervous System</a></li>
						 		<li><a href="http://medlanes.com/diseases/bladder-health">Bladder Health</a></li>
						 		<li><a href="http://medlanes.com/diseases/cancer">Cancer</a></li>
						 		<li><a href="http://medlanes.com/diseases/cardiovascular-system">Cardiovascular System</a></li>
						 		<li><a href="http://medlanes.com/diseases/children's-health">Children's Health</a></li>
						 		<li><a href="http://medlanes.com/diseases/cold-cough-and-flu">Cold, Cough and Flu</a></li>
 								<li><a href="http://medlanes.com/diseases/diabetes">Diabetes</a></li>
						 		<li><a href="http://medlanes.com/diseases/digestive-disorders">Digestive Disorders</a></li>
							 	<li><a href="http://medlanes.com/diseases/ear-health">Ear Health</a></li>
						 		<li><a href="http://medlanes.com/diseases/endocrine-system">Endocrine System</a></li>
<!-- 						 	<li><a href="exercise-and-fitness">Exercise and Fitness</a></li>
 -->							<li><a href="http://medlanes.com/diseases/environmental">Environmental</a></li>
 					 			<li><a href="http://medlanes.com/diseases/eye-health">Eye Health</a></li>
 					 			<li><a href="http://medlanes.com/diseases/fever">Fever</a></li>


						 		
						 </ul>
						 <ul>
					 		<li><a href="http://medlanes.com/diseases/genetic-disorders">Genetic Disorders</a></li>
					 		<li><a href="http://medlanes.com/diseases/hair-skin-and-nails">Hair, Skin and Nails</a></li>
							<li><a href="http://medlanes.com/diseases/fatigue-and-weakness">Fatigue and Weakness</a></li>
					 		<li><a href="http://medlanes.com/diseases/headaches-and-migraines">Headaches and Migraines</a></li>
					 		<li><a href="http://medlanes.com/diseases/heartburn-and-gerd">Heartburn and GERD</a></li>
					 		<li><a href="http://medlanes.com/diseases/hiv-and-aids">HIV and Aids</a></li>
					    	<li><a href="http://medlanes.com/diseases/hypertension">Hypertension</a></li>
					 		<li><a href="http://medlanes.com/diseases/hepatic-system">Hepatic System</a></li>
						 	<li><a href="http://medlanes.com/diseases/injuries">Injuries</a></li>
					 		<li><a href="http://medlanes.com/diseases/infections">Infections</a></li>
					 		<li><a href="http://medlanes.com/diseases/men's-health">Men's Health</a></li>
                            <li><a href="http://medlanes.com/diseases/mental-health">Mental Health</a></li>
							<li><a href="http://medlanes.com/diseases/musculoskeletal-system">Musculoskeletal System</a></li>
					 	    <li><a href="http://medlanes.com/diseases/oral-health">Oral Health</a></li>
					 		<li><a href="http://medlanes.com/diseases/pain">Pain</a></li>
					 		<li><a href="http://medlanes.com/diseases/renal-system">Renal System</li>
					 		<li><a href="http://medlanes.com/diseases/respiratory-health">Respiratory Health</a></li>
					 		<li><a href="http://medlanes.com/diseases/sexual-health">Sexual Health</a></li>
<!-- 						 		<li><a href="sexually-transmitted-diseases">Sexually Transmitted Diseases</a></li>
						 		

<!--  <li><a href="sleep-disorders">Sleep Disorders</a></li>
 -->						 	
<!--  	<li><a href="weight-loss">Weight Loss</a></li>
 -->						 		<li><a href="http://medlanes.com/diseases/womens-health">Women's Health</a></li>		
						 	</ul>	 		
						 		 	<ul>
	
						 	</ul>
						</div>
					</li>
					<li  >
						<a href="http://medlanes.com/support.php">Support</a>
					</li>
					<li  >
						<a href="http://medlanes.com/for-business.php">For Business</a>
					</li>
						<li  >
						<a href="http://medlanes.com/blog">Blog</a>
					</li>
				</ul>
			</nav>
	</header>


	<style type="text/css">
		/*style the sub menu*/
@media only (max-width: 767px){

		.diseases-dropdown  {
		position: absolute !important;
	    display: none !important;
	    z-index: 2 !important;
	    cursor: pointer!important;
	    padding: 20px!important;
	    background-color: white!important;
	    border-radius: 5px!important;
	    overflow: hidden!important;
	    left: 0px!important;
		}
	}
								
	   .diseases-dropdown  {
		position: absolute;
	    display: none;
	    top: 72px;
	    z-index: 2;
	    cursor: pointer;
	    padding: 23px;
	    background-color: white;
	    overflow: hidden;
	    left: 58%;
		}
		
		.diseases-dropdown ul  {
		  margin-left: 0px !important ;
	      float: left;
    	  width: 167px;
		
		}
		.diseases-dropdown ul li {
	  	margin-left: 0px !important;
	   	line-height:1.5em;
	   	float: left;
	   	margin-left: 54px !important;
		}

		.diseases-dropdown ul  li a {
	  	font-size: 12px !important ;
		}
		

	</style>

	<script type="text/javascript">
	$(document).ready(function() {
		$('.diseases').bind('mouseover', openSubMenu);
		$('.diseases-dropdown').bind('mouseout', closeSubMenu);
		
		function openSubMenu() {
			$('.diseases-dropdown').css('display', 'block');	
			$(this).find('ul li').css('display', 'block');	
	
		};
		
		function closeSubMenu() {
			$(this).find('ul li').css('display', 'none');	
			$('.diseases-dropdown').css('display', 'none');	

		};
				   
	});
</script>