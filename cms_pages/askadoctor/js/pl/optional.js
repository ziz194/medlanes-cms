	$(document).ready(function(){
			$("#coupon").keyup(function(){
		    	var coupon = $(this).val();		    	
    			var request = {"action":"validate_coupon","parameters":{"coupon_code":coupon}};
    			var data = {"request":JSON.stringify(request)};
			    $.ajax({
			          type: "POST",
			          url:  "https://api.medlanes.com/index.php?/api_1/call/json",      
			          data: data,
			          dataType: 'json',
			          async: false,
			          success: function(data) {
			          	if(data.success == true){
			          		$('#coupon').css('color','green');
			          		var coupon = $('#coupon').val();
			          		localStorage.setItem('coupon', coupon);

			          	}else{
			          		$('#coupon').css('color','red');
			          		localStorage.removeItem('coupon', coupon);
			          	}			          	
			          },
			          failure: function(data) {
			             console.log(' failed to connect');
			          },
			           error: function(data) {
			      }
			    });
			});
	});
function getdetails(){

	$('.forward').attr('onclick','z');

	var name = $('#name').val();
	var age = $('#age').val();
	var gender = $('#gender').find(":selected").val();
	var email = localStorage.getItem("email");
	var msg = localStorage.getItem("msg");
	var tred = $("#try").val();



	var pretoken = localStorage.getItem("token");
	if(pretoken === null){
		  var dataload = {"action":"user_dummy"};
		  var stringifydataload = {"request":JSON.stringify(dataload)};

		  $.ajax({
		    type: "POST",
		    url:  "https://api.medlanes.com/index.php?/api_1/call/json",
		    data: stringifydataload,
		    success: function(stringifydataload) {
		    var object = jQuery.parseJSON( stringifydataload );
		    var gettoken = object.result.token;           
		    localStorage.setItem('token', gettoken);
		   	getlang = 0;
		    var usersignup = {"action":"user_signup","auth":{"token":gettoken},"parameters":{"age":age,"gender":gender,"funnel":"ja2","email_template":"signup","company":"askadoctor","firstname":name,"email":email,"idCountry":3,"idLanguage":3}};  
		    var requestusersignup = {"request":JSON.stringify(usersignup)};
		     $.ajax({
		           type: "POST",
		           url:  "https://api.medlanes.com/index.php?/api_1/call/json",
		           data: requestusersignup,
		               success: function(requestusersignup) {
		                 var success =JSON.parse(requestusersignup);
		                 var checkfalse = success.success;
		                     if(checkfalse == false){
		                       var emailsign2 = $('#emailsign2').val();
		                       $('#email').val(emailsign2);
		                     }else{
								var coupon = $('#coupon').val();
								var params = {"funnel":"ja2","company":"askadoctor","text":msg,"state":"stored","message_properties":[{"name":"tried","value":tred,"type":9, "datatype":"text"},{"name":"MarketingPlatform","value":"web.medlanes.com.JA","type":9, "datatype":"text"}]}
							    if(coupon != null && coupon != ""){
								        params.coupon = coupon;
							    } 
							    var request = {"action":"question_add","auth":{"token":gettoken},"parameters":params}
							    var data = {"request":JSON.stringify(request)};
							    $.ajax({
							          type: "POST",
							          url:  "https://api.medlanes.com/index.php?/api_1/call/json",
							          data: data,
							          dataType: 'json',
							          async: false,
							          success: function(data) {
							            console.log(data);
							            var getmsgidfromaddmsg = data;
							            var specmsgid = getmsgidfromaddmsg.result.firstMessage.idTicket;
							            console.log(getmsgidfromaddmsg.result);
							            var idMessage = getmsgidfromaddmsg.result.firstMessage.idMessage;
							            localStorage.setItem('specmsgid', specmsgid);
							            var coupon = localStorage.getItem('coupon');
							            additionalData();
							            if(coupon === null){
							            	location.replace("payment.php");
							            }else{
							            	location.replace("waitingroom.php");
							            }
							           
							          },
							          failure: function(data) {
							             console.log(' failed to connect');
							          },
							           error: function(data) {
							      }
							    });
		                     }         
		                   },

		               failure: function(requestusersignup) {
		                  console.log(' failed to connect');
		               }
		       });
		      }
		    });
	}else{
		var gettoken = localStorage.getItem('token');
		var coupon = $('#coupon').val();
		var params = {"funnel":"ja2","company":"askadoctor","text":msg,"state":"stored","message_properties":[{"name":"tried","value":tred,"type":9, "datatype":"text"},{"name":"MarketingPlatform","value":"web.medlanes.com.JA","type":9, "datatype":"text"}]}
	    if(coupon != null && coupon != ""){
		        params.coupon = coupon;
	    } 
	    var request = {"action":"question_add","auth":{"token":gettoken},"parameters":params}
	    var data = {"request":JSON.stringify(request)};
	    console.log(JSON.stringify(request))
	    $.ajax({
	          type: "POST",
	          url:  "https://api.medlanes.com/index.php?/api_1/call/json",      
	          data: data,
	          dataType: 'json',
	          async: false,
	          success: function(data) {
	            console.log(data);
	            var getmsgidfromaddmsg = data;
	            var specmsgid = getmsgidfromaddmsg.result.firstMessage.idTicket;
	            console.log(getmsgidfromaddmsg.result);
	            var idMessage = getmsgidfromaddmsg.result.firstMessage.idMessage;
	            localStorage.setItem('specmsgid', specmsgid);
	            var coupon = localStorage.getItem('coupon');
	            if(coupon === null){
	            	location.replace("payment.php");
	            }else{
	            	location.replace("waitingroom.php");
	            }
	           
	          },
	          failure: function(data) {
	             console.log(' failed to connect');
	          },
	           error: function(data) {
	      }
	    });
	}






}
  function isNumberKey(evt)
  {
     var charCode = (evt.which) ? evt.which : event.keyCode
     if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

     return true;
  }