/**
* Initialize FingerPrint value in optional page
**/
$(document).ready(function() {
	var fp = new Fingerprint();
	$('#fp').val(fp.get());
});