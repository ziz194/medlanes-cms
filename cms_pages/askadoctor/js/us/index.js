$(document).ready(function(){
	
	localStorage.clear();

	$('.choose h2').click(function(){
		var palceholder = $(this).text();
		var paragraph = $(this).attr('id');
		$('#msg').attr("placeholder",palceholder);
		$('.changetext').text(paragraph);
		$('.choose h2').removeClass("active");
		$(this).addClass("active");
		localStorage.setItem('span', $(this).text());
	});

	$('#email').keyup(function () {
		var keycode = (event.keyCode ? event.keyCode : event.which);
	    if(keycode == '13') {
			backemail();
		}
	});

	$(".tooltip-left").tooltipster({
		theme: 'grey-theme',
		position: 'left',
		arrowColor : '#aaa',
		contentAsHTML: true,
		offsetX : '7px 0',
	});

	$(".tooltip").tooltipster({
		theme: 'grey-theme',
		arrowColor : '#aaa',
		contentAsHTML: true
	});

	$('.midbox .bxslider').bxSlider({
		auto: false,
		pager: false,
		autoControls: false,
		pause: 10000,
	});

	$('.our-doc-one .bxslider').bxSlider({
		minSlides: 5,
		maxSlides: 5,		
		auto: false,
		pager: false,
		autoControls: true,
		slideWidth: 330,
		pause:10000,
		moveSlides: 1,
        slideMargin: 20,
        onSliderLoad: function () {
    		$('.bxslider>li:not(.bx-clone)').eq(2).addClass('active-slide');
		},
		onSlideAfter: function ($slideElement, oldIndex, newIndex) {
    		$('.slide').removeClass('active-slide');
    		$($slideElement).next().next().addClass('active-slide');        
		}
	});
});

//###############################################

function userLogin(){
	var msg = $('#msg').val();
	var span = localStorage.getItem('span');
	if(span == null){
		span = "General Medicine";
	}
	var totalmsg = span +' | '+ msg;
	var email = $('#email').val();
	var password = $('#password').val();

    var cb = function(result){
        var rp = JSON.parse(result);
		if(rp.success == true){
			localStorage.setItem('token', object.token);
			getdetails();
		}else{
			$('.spillerror').text('');
			$('.spillerror').text('Your login credentials are wrong. Please try again.');
		}
    }

	MedlanesSDK.signUp(cb, email, '', '', password);
    //TODO: always male. fix later
}


function backemail(){
	//TODO: requires auth data (?)
	var email = $('#email').val();
    var request = {"action":"check_mail","parameters":{"email":email}}

	var cb = function(result){
        var rp = JSON.parse(result);
        if(rp.success){
            if(rp.result.is_active_user){
            	MedlanesSDK.setParam("enail", email);
				localStorage.setItem("email", email);  //old
           		$('.z').css('display','block');
           		$('.removeclicks').attr('onclick','').unbind('click');
           		$('.removeclicks').attr('onclick','userLogin()').bind('click');
           	}else{	
           		getdetails();
           	}
        }else{
        	console.log("Request failed.");
        	$('#email').css('border','1px solid red');
        	$(".emailerror").fadeIn();
        }
	};

    MedlanesSDK.ajax.wrapCall(cb, request, "checkMail");
}

//TODO: replace with SDK kickbox validation later
function getdetails(){
	var msg = $('#msg').val();
	var span = localStorage.getItem('span');
	if(span == null){
		span = "General Medicine";
	}
	var totalmsg = span +' | '+ msg;
	if(msg == ''){
		$('#msg').css("border","1px solid red");
	}else{
		var email = $('#email').val();
		// var kickbox = verifyEmail(email);
		// sendex = localStorage.getItem("sendex");
		// if(kickbox == true && sendex > 0.3){
		if(verifyEmail(email)){
			localStorage.setItem("email", email);
			localStorage.setItem("msg", totalmsg);
			location.replace("optional.php");
		}
	}
}



//TODO: was kickbox, now just inner validation
function verifyEmail(email){
	return MedlanesSDK.validators.isValidMail(email);
	// var res = false;
	// $.ajax({
	// 	type: "GET",
	// 	dataType: "json",
	// 	url: "php/validationEmail.php",
	// 	async: false,
	// 	data: {'email': email},
	// 	success: function(data){
	// 		res = (data.result == 'valid');
	// 		localStorage.setItem("sendex", data.sendex);
	// 		if(res == false){
	// 			$(".emailerror").fadeIn();
	// 			event.preventDefault();
	// 		}
	// 	},
	// 	error: function(data){
	// 		var filter = /^[a-z0-9._-]+@[a-z0-9._-]+\.[a-z]{2,6}$/;
	// 		res = filter.test(email);
	// 	}
	// });
	// return res;
}
