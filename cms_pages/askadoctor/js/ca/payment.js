		var btnSubmit = false;
		$(function() {
			$(window).bind('beforeunload', function(){
				if(!btnSubmit)
					return 'WAIT! Medical Experts are online and are waiting to answer your question!\n\nPlease let us know what your are willing to pay IF you are satisfied with the answer. Remember, satisfaction is fully guaranteed. If you are not satisfied, simply request a full refund.\n\nThank You\n\nAsk A Doctor';
			});
		});
		$(document).ready(function(){
			$(".tooltip").tooltipster({
				theme: 'grey-theme',
				arrowColor : '#aaa',
				contentAsHTML: true             
			});
			$("#leftslide").noUiSlider({
				behaviour: 'tap-drag',
				direction: 'rtl',
				decimals: 0,
				start: [ 1 ],
				snap : true,
				orientation: "vertical",
				range: {
					'min': [ 1 ],		
					'50%': [ 2 ],
					'max': [ 3 ]
				}
			});

			$("#rightslide").noUiSlider({
				behaviour: 'tap-drag',
				direction: 'rtl',
				decimals: 0,
				start: [ 1 ],
				snap : true,
				orientation: "vertical",
				range: {
					'min': [ 1 ],		
					'50%': [ 2 ],
					'max': [ 3 ]
				}
			});

			$(".bluebox").tooltipster({
					theme: 'blue-theme',
					position: 'bottom',
					arrowColor : '#1b92ab',
					contentAsHTML: true
				});  


		$('#rightslide').on('slide', function(){
	      check();	
         });
        $('#leftslide').on('slide', function(){
            check(); 
         });

      });
      var level = ['High','Medium','Low'];

      function check(){
         var x = $('#leftslide').val();
         var y = $('#rightslide').val();
         var x = parseInt(x);
         var y = parseInt(y);
         change(Math.floor(x+y));
      }

      function getpaymentdetails(){
      	 btnSubmit = true;
         var amount = $('.amount').text();
         localStorage.setItem("amount", amount);
      }
  
/*
      $(window).bind('beforeunload', function(){
      	if(!btnSubmit)
      		return 'WAIT! Medical Experts are online and are waiting to answer your question!\n\nPlease let us know what your are willing to pay IF you are satisfied with the answer. Remember, satisfaction is fully guaranteed. If you are not satisfied, simply request a full refund.\n\nThank You\n\nAsk A Doctor';
      });

*/

 