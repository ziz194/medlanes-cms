$(document).ready(function(){
			localStorage.clear();

			$('.choose span').click(function(){

				var palceholder = $(this).attr('val');
				var paragraph = $(this).attr('id');
				$('#msg').attr("placeholder",palceholder);
				$('.changetext').text(paragraph);
				$('.choose span').css('background','#C8E6ED');
				$('.choose span').css('color','#1B92AB');
				$(this).css('background','#1B92AB');
				$(this).css('color','#ffffff');
				localStorage.setItem('span', $(this).text());
			});

	

			$('#email').keyup(function () {
				var keycode = (event.keyCode ? event.keyCode : event.which);
			      if(keycode == '13') {
				      backemail();
			      }
			});
				
		$(".tooltip-left").tooltipster({
			theme: 'grey-theme',
			position: 'left',
			arrowColor : '#aaa',
			contentAsHTML: true,
			offsetX : '7px 0',
		});
		
		$(".tooltip").tooltipster({
			theme: 'grey-theme',
			arrowColor : '#aaa',
			contentAsHTML: true             
		});  

		$('.bxslider').bxSlider({
			auto: true,
			pager: false,
			autoControls: true,
			pause:3000,
		});
	




		});

function userLogin(){
	var msg = $('#msg').val();
	var span = localStorage.getItem('span');
	if(span == null){
		span = "General Medicine";
	}
	var totalmsg = span +' | '+ msg;
	var email = $('#email').val();
	var password = $('#password').val();
  // var hash_a = CryptoJS.SHA1(password);
  // var hash_auth = hash_a.toString(CryptoJS.enc.Base64);
  var loginAction = {"action":"login","parameters":{"email":email,"password":password}}
  var data = {"request":JSON.stringify(loginAction)};   
    $.ajax({
          type: "POST",
          url:  "https://api.medlanes.com/index.php?/api_1/call/json",
          data: data,
              success: function(data) {
    			var object = jQuery.parseJSON( data );
    			

    			if(object.success == true){
    				localStorage.setItem('token', object.token);
    				getdetails();
    			}else{
    				$('.spillerror').text('');
    				$('.spillerror').text('Your login credentials are wrong. Please try again.');
    			}
              },

              failure: function(data) {
                 console.log(' failed to connect');
              }
      });

}

function backemail(){
	var email = $('#email').val();
    var request = {"action":"check_mail","parameters":{"email":email}}
    var data = {"request":JSON.stringify(request)};
	    $.ajax({
	          type: "POST",
	          url:  "https://api.medlanes.com/index.php?/api_1/call/json",      
	          data: data,
	          dataType: 'json',
	          async: false,
	          success: function(data) {
	            if(data.success == true){


		           	if(data.result.is_active_user == true){
						localStorage.setItem("email", email);
		           		$('.z').css('display','block');
		           		$('.removeclicks').attr('onclick','').unbind('click');
		           		$('.removeclicks').attr('onclick','userLogin()').bind('click');
		           	}else{
		           		getdetails();
		           	}
	            }else{
	            	$('#email').css('border','1px solid red');
	            }


	          },
	          failure: function(data) {
	             console.log(' failed to connect');
	          },
	           error: function(data) {
	      }
	    });
}

		function getdetails(){
			var msg = $('#msg').val();
			var span = localStorage.getItem('span');
			if(span == null){
				span = "General Medicine";
			}
			var totalmsg = span +' | '+ msg;
			if(msg == ''){
				$('#msg').css("border","1px solid red");
			}else{
				var email = $('#email').val();
				var kickbox = verifyEmail(email);
				sendex = localStorage.getItem("sendex");
				if(kickbox == true && sendex > 0.3){
					localStorage.setItem("email", email);
					localStorage.setItem("msg", totalmsg);	
					location.replace("optional.php");
				}		
			}
		}


			
		

	    function verifyEmail(email){
			var res = false;				
			$.ajax({
				type: "GET",
				dataType: "json",
				url: "php/validationEmail.php",
				async: false,
				data: {'email': email},
				success: function(data){
					res = (data.result == 'valid');
					localStorage.setItem("sendex", data.sendex);
					if(res == false){
						$('.showcross').css('display','block');
						event.preventDefault();
					}
				},
				error: function(data){
					var filter = /^[a-z0-9._-]+@[a-z0-9._-]+\.[a-z]{2,6}$/;
					res = filter.test(email); 							
				}			
			});			
			return res;
		}		
