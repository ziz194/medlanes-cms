$(document).ready(function(){
	$(function() {
		$( "#servicetabs" ).tabs();
	});
	$( ".wrappone" ).hover(
		function() {
			$(this).find(".one").addClass("fade-in-now");
  		}, function() {
  			$(this).find(".one").removeClass("fade-in-now");
  		}
  	);
	$( ".wrappone .one" ).hover(
		function() {
			$(this).removeClass("fade-in-now");
		}, function() {
			$(this).removeClass("fade-in-now");
		}
	);
	var stickyNavTop = $('.sub-menu').offset().top -100;  
	var stickyNav = function(){  
		var scrollTop = $(window).scrollTop();
		if (scrollTop > stickyNavTop) {   
		    $('.sub-menu').addClass('follow-me-sm'); 
		    $('.sub-wrap').addClass('top-padding');
		} else {  
		    $('.sub-menu').removeClass('follow-me-sm');  
		    $('.sub-wrap').removeClass('top-padding'); 
		}  
	};  
	stickyNav(); 
	$(window).scroll(function() {
	stickyNav();
	});
	$('#how').click(function(e){
		$('html, body').animate({
		scrollTop: $("#howgo").offset().top -190
		}, 500);          
	});  
	$('#why').click(function(e){
		$('html, body').animate({
		scrollTop: $("#whygo").offset().top -190
		}, 500);          
	});  
	$('#use').click(function(e){
		$('html, body').animate({
		scrollTop: $("#usego").offset().top -190
		}, 500);          
	});  
	$('#price').click(function(e){
		$('html, body').animate({
		scrollTop: $("#pricego").offset().top -190
		}, 500);          
	});  
});