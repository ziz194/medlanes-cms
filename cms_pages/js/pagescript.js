  $(document).ready(function(){





    $('.question_text').autosize();   
    $('#question-box').autosize();  
    //Sticky sidebar 
    $("#sidebar, #content_column").stick_in_parent(); 



    $('.search-icon').click(function(){
    	$('.search-input').toggle()
    });


    $('.min-description-title').click(function(){
    	$('.min-description').slideToggle()
    }) ;

    $('.min-signs-title').click(function(){
    	$('.min-signs').slideToggle()
    }) ;

    $('.min-causes-title').click(function(){
    	$('.min-causes').slideToggle()
    }) ;

    $('.min-riskfactors-title').click(function(){
    	$('.min-riskfactors').slideToggle()
    }) ;

     $('.min-complications-title').click(function(){
    	$('.min-complications').slideToggle()
    }) ;
          $('.min-diagnosis-title').click(function(){
    	$('.min-diagnosis').slideToggle()
    }) ;

     $('.min-treatments-title').click(function(){
    	$('.min-treatments').slideToggle()
    }) ;

     $('.min-conclusion-title').click(function(){
        $('.min-conclusion').slideToggle()
    }) ;


    $('.treatments-toggle').click(function(){
        $('.treatments-list').slideToggle() ; 
        $('.related-list').slideUp() ; 
        $('.diagnosis-list').slideUp() ; 
        $('.risk-factors-list').slideUp() ; 
        $('.symptoms-list').slideUp() ; 
     e.stopPropagation() ; 
     e.preventDefault() ;
     return false ;
    }) ;

    $('.related-toggle').click(function(){
        $('.related-list').slideToggle() ; 
        $('.treatments-list').slideUp() ; 
        $('.diagnosis-list').slideUp() ; 
        $('.risk-factors-list').slideUp() ; 
        $('.symptoms-list').slideUp() ; 
     e.stopPropagation() ; 
     e.preventDefault() ;
     return false ;
    }) ;

    $('.diagnosis-toggle').click(function(){
        $('.diagnosis-list').slideToggle() ; 
         $('.treatments-list').slideUp() ; 
        $('.risk-factors-list').slideUp() ; 
        $('.related-list').slideUp() ; 
        $('.symptoms-list').slideUp() ; 
     e.stopPropagation() ; 
     e.preventDefault() ;
     return false ;
    }) ;
    $('.risk-factors-toggle').click(function(){
        $('.risk-factors-list').slideToggle() ; 
        $('.related-list').slideUp() ; 
        $('.diagnosis-list').slideUp() ; 
        $('.treatments-list').slideUp() ; 
        $('.symptoms-list').slideUp() ; 
     e.stopPropagation() ; 
     e.preventDefault() ;
     return false ;
    }) ;

    $('.symptoms-toggle').click(function(e){
        $('.symptoms-list').slideToggle() ; 
        $('.related-list').slideUp() ; 
        $('.diagnosis-list').slideUp() ; 
        $('.risk-factors-list').slideUp() ; 
        $('.treatments-list').slideUp() ; 
     e.stopPropagation() ; 
     e.preventDefault() ;
     return false ;
    }) ;

     $('.min-description').html($('.description-section').html()) ;
    $('.min-signs').html($('.signs-section').html()) ;
    $('.min-causes').html($('.causes-section').html()) ;
    $('.min-riskfactors').html($('.riskfactors-section').html()) ;
    $('.min-complications').html($('.complications-section').html()) ;
    $('.min-diagnosis').html($('.diagnosis-section').html()) ;
    $('.min-treatments').html($('.treatments-section').html()) ;
    $('.min-conclusion').html($('.conclusion-section').html()) ;


  });


  // CRM FORM 

function crmFormSubmit(){
    if(checkMandatory()!=false){
        $('#submit-modal').foundation('reveal','open');
    return false ; 
} 

else {
    return false ;
}
}

