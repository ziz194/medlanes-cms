$(window).load(function () {
  $('.flexslider').flexslider({
    animation: "fade",
    slideDirection: "horizontal",
    slideshow: true,
    prevText: "",
    nextText: "",
    controlNav: false
  });
});

$(window).load(function () {
  var question = $('.tabs-desc li');

  question.on('click', function (e) {
    e.preventDefault();
    var answer = $(this).find('.answer');

    if ($(this).hasClass('active')) {
      $(this).removeClass('active');
      answer.fadeOut();
    } else {
      $(this).addClass('active');
      answer.fadeIn();
    }
  })
})

$(window).load(function () {
  var tabs = $('.tabs li');

  $('.tabs-desc .tab-item').eq(0).show();

  tabs.on('click', function () {
    var $index = $(this).index(),
      question = $('.tabs-desc .tab-item');


    if ($(this).hasClass('active')) {
      return;

    } else {
      tabs.removeClass('active');
      question.slideUp();
      $(this).addClass('active');
      question.eq($index).slideDown();
    }
  })
})

$(window).load(function () {
  var $popup = $('.starup .placeholder-img'),
    $el = $('.starup li');

  $el.on('click', function () {
    $popup.find('img').remove();
    $popup.append($(this).html());
    $el.removeClass('active');
    $(this).addClass('active');
  })

});

$(window).load(function () {
  var $humb = $('.humb'),
    $nav = $('header nav');

  $humb.on('click', function () {
    if ($(this).hasClass('active')) {
      $(this).removeClass('active');
      $nav.slideUp();
    }else{
      $(this).addClass('active');
      $nav.slideDown();
    }
  })

});

$(document).ready(function(){
    var getwidth = $(document).width();
    if ( getwidth < 768 ) {
      $(window).load(function() {
        $('.mobslider').flexslider({
          animation: "slide",
          controlNav: "false",
          slideshowSpeed : "5000"
        });
      });
    }
});


