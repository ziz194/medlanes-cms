    $( document ).ready(function() {

      $('.front-bb .head').toggle(function() {
        $('.front-bb .head').css('background-position','0px -64px');
        $(this).attr("id","front-head");
          renew('front-head');
      }, function() {
         $('.front-bb .head').css('background-position','0px 0px');
         $(this).removeAttr('id');
         renewlist();
      });

      $('.front-bb .leftarm').toggle(function() {
        $('.front-bb .leftarm').css('background-position','0px -130px');
        $(this).attr("id","front-arm-right");    
         renew('front-arm-right');    
      }, function() {
         $('.front-bb .leftarm').css('background-position','0px 0px');
         $(this).removeAttr('id');
         renewlist();
      });

      $('.front-bb .chestleft').toggle(function() {
        $('.front-bb .chestleft').css('background-position','0px -66px');
        $(this).attr("id","front-chest-right");
        renew('front-chest-right');  
      }, function() {
         $('.front-bb .chestleft').css('background-position','0px 0px');
         $(this).removeAttr('id');
         renewlist();
      });

        $('.front-bb .chestright').toggle(function() {
        $('.front-bb .chestright').css('background-position','0px -66px');
        $(this).attr("id","front-chest-left");
        renew('front-chest-left');  
      }, function() {
         $('.front-bb .chestright').css('background-position','0px 0px');
         $(this).removeAttr('id');
         renewlist();
      });

      $('.front-bb .rightarm').toggle(function() {
        $('.front-bb .rightarm').css('background-position','0px -130px');
        $(this).attr("id","front-arm-left");
        renew('front-arm-left');  
      }, function() {
         $('.front-bb .rightarm').css('background-position','0px 0px');
         $(this).removeAttr('id');
         renewlist();
      });

      $('.front-bb .stomachleft').toggle(function() {
        $('.front-bb .stomachleft').css('background-position','0px -64px');
        $(this).attr("id","front-stomach-right");
        renew('front-stomach-right');  
      }, function() {
         $('.front-bb .stomachleft').css('background-position','0px 0px');
         $(this).removeAttr('id');
         renewlist();
      });


      $('.front-bb .stomachright').toggle(function() {
        $('.front-bb .stomachright').css('background-position','0px -64px');
        $(this).attr("id","front-stomach-left");
        renew('front-stomach-left');  
      }, function() {
         $('.front-bb .stomachright').css('background-position','0px 0px');
         $(this).removeAttr('id');
         renewlist();
      });

      $('.front-bb .lefthand').toggle(function() {
        $('.front-bb .lefthand').css('background-position','0px -35px');
        $(this).attr("id","front-hand-right");
        renew('front-hand-right');  
      }, function() {
         $('.front-bb .lefthand').css('background-position','0px 0px');
         $(this).removeAttr('id');
         renewlist();
      });

      $('.front-bb .lowerabdom').toggle(function() {
        $('.front-bb .lowerabdom').css('background-position','0px -35px');
        $(this).attr("id","front-middle");
        renew('front-middle');  
      }, function() {
         $('.front-bb .lowerabdom').css('background-position','0px 0px');
         $(this).removeAttr('id');
         renewlist();
      });


      $('.front-bb .righthand').toggle(function() {
        $('.front-bb .righthand').css('background-position','0px -35px');
        $(this).attr("id","front-hand-left");
        renew('front-hand-left');  
      }, function() {
         $('.front-bb .righthand').css('background-position','0px 0px');
         $(this).removeAttr('id');
         renewlist();
      });


      $('.front-bb .lefttight').toggle(function() {
        $('.front-bb .lefttight').css('background-position','0px -53px');
        $(this).attr("id","front-legtop-right");
        renew('front-legtop-right');  
      }, function() {
         $('.front-bb .lefttight').css('background-position','0px 0px');
         $(this).removeAttr('id');
         renewlist();
      });


      $('.front-bb .righttight').toggle(function() {
        $('.front-bb .righttight').css('background-position','0px -53px');
        $(this).attr("id","front-legtop-left");
        renew('front-legtop-left');  
      }, function() {
         $('.front-bb .righttight').css('background-position','0px 0px');
         $(this).removeAttr('id');
         renewlist();
      });

      $('.front-bb .shinboneleft').toggle(function() {
        $('.front-bb .shinboneleft').css('background-position','0px -92px');
        $(this).attr("id","front-legbtm-left");
        renew('front-legbtm-left');  
      }, function() {
         $('.front-bb .shinboneleft').css('background-position','0px 0px');
         $(this).removeAttr('id');
         renewlist();
      });


      $('.front-bb .shinboneright').toggle(function() {
        $('.front-bb .shinboneright').css('background-position','0px -92px');
        $(this).attr("id","front-legbtm-right");
        renew('front-legbtm-right');  
      }, function() {
         $('.front-bb .shinboneright').css('background-position','0px 0px');
         $(this).removeAttr('id');
         renewlist();
      });


      $('.front-bb .leftfeet').toggle(function() {
        $('.front-bb .leftfeet').css('background-position','0px -38px');
        $(this).attr("id","front-foot-left");
        renew('front-foot-left');  
      }, function() {
         $('.front-bb .leftfeet').css('background-position','0px 0px');
         $(this).removeAttr('id');
         renewlist();
      });


      $('.front-bb .rightfeet').toggle(function() {
        $('.front-bb .rightfeet').css('background-position','0px -38px');
        $(this).attr("id","front-foot-right");
        renew('front-foot-right');  
      }, function() {
         $('.front-bb .rightfeet').css('background-position','0px 0px');
         $(this).removeAttr('id');
         renewlist();
      });



      $('.back-bb .head').toggle(function() {
        $('.back-bb .head').css('background-position','0px -64px');
        $(this).attr("id","back-head");
        renew('back-head');  
      }, function() {
         $('.back-bb .head').css('background-position','0px 0px');
         $(this).removeAttr('id');
         renewlist();
      });

      $('.back-bb .leftarm').toggle(function() {
        $('.back-bb .leftarm').css('background-position','0px -130px');
        $(this).attr("id","back-arm-left");
        renew('back-arm-left');  
      }, function() {
         $('.back-bb .leftarm').css('background-position','0px 0px');
         $(this).removeAttr('id');
         renewlist();
      });

      $('.back-bb .chestleft').toggle(function() {
        $('.back-bb .chestleft').css('background-position','0px -66px');
        $(this).attr("id","back-chest-left");
        renew('back-chest-left');  
      }, function() {
         $('.back-bb .chestleft').css('background-position','0px 0px');
         $(this).removeAttr('id');
         renewlist();
      });

        $('.back-bb .chestright').toggle(function() {
        $('.back-bb .chestright').css('background-position','0px -66px');
        $(this).attr("id","back-chest-right");
        renew('back-chest-right');  
      }, function() {
         $('.back-bb .chestright').css('background-position','0px 0px');
         $(this).removeAttr('id');
         renewlist();
      });

      $('.back-bb .rightarm').toggle(function() {
        $('.back-bb .rightarm').css('background-position','0px -130px');
        $(this).attr("id","back-arm-right");
        renew('back-arm-right');  
      }, function() {
         $('.back-bb .rightarm').css('background-position','0px 0px');
         $(this).removeAttr('id');
         renewlist();
      });

      $('.back-bb .stomachleft').toggle(function() {
        $('.back-bb .stomachleft').css('background-position','0px -64px');
        $(this).attr("id","back-stomach-left");
        renew('back-stomach-left');  
      }, function() {
         $('.back-bb .stomachleft').css('background-position','0px 0px');
         $(this).removeAttr('id');
         renewlist();
      });


      $('.back-bb .stomachright').toggle(function() {
        $('.back-bb .stomachright').css('background-position','0px -64px');
        $(this).attr("id","back-stomach-right");
        renew('back-stomach-right');  
      }, function() {
         $('.back-bb .stomachright').css('background-position','0px 0px');
         $(this).removeAttr('id');
         renewlist();
      });

      $('.back-bb .lefthand').toggle(function() {
        $('.back-bb .lefthand').css('background-position','0px -35px');
        $(this).attr("id","back-hand-left");
        renew('back-hand-left');  
      }, function() {
         $('.back-bb .lefthand').css('background-position','0px 0px');
         $(this).removeAttr('id');
         renewlist();
      });

      $('.back-bb .lowerabdom').toggle(function() {
        $('.back-bb .lowerabdom').css('background-position','0px -35px');
        $(this).attr("id","back-middle");
        renew('back-middle');  
      }, function() {
         $('.back-bb .lowerabdom').css('background-position','0px 0px');
         $(this).removeAttr('id');
         renewlist();
      });


      $('.back-bb .righthand').toggle(function() {
        $('.back-bb .righthand').css('background-position','0px -35px');
        $(this).attr("id","back-hand-right");
        renew('back-hand-right');  
      }, function() {
         $('.back-bb .righthand').css('background-position','0px 0px');
         $(this).removeAttr('id');
         renewlist();
      });


      $('.back-bb .lefttight').toggle(function() {
        $('.back-bb .lefttight').css('background-position','0px -53px');
        $(this).attr("id","back-legtop-left");
        renew('back-legtop-left');  
      }, function() {
         $('.back-bb .lefttight').css('background-position','0px 0px');
         $(this).removeAttr('id');
         renewlist();
      });


      $('.back-bb .righttight').toggle(function() {
        $('.back-bb .righttight').css('background-position','0px -53px');
        $(this).attr("id","back-legtop-right");
        renew('back-legtop-right');  
      }, function() {
         $('.back-bb .righttight').css('background-position','0px 0px');
         $(this).removeAttr('id');
         renewlist();
      });

      $('.back-bb .shinboneleft').toggle(function() {
        $('.back-bb .shinboneleft').css('background-position','0px -92px');
        $(this).attr("id","back-legbtm-left");
        renew('back-legbtm-left');  
      }, function() {
         $('.back-bb .shinboneleft').css('background-position','0px 0px');
         $(this).removeAttr('id');
         renewlist();
      });


      $('.back-bb .shinboneright').toggle(function() {
        $('.back-bb .shinboneright').css('background-position','0px -92px');
        $(this).attr("id","back-legbtm-right");
        renew('back-legbtm-right');  
      }, function() {
         $('.back-bb .shinboneright').css('background-position','0px 0px');
         $(this).removeAttr('id');
         renewlist();
      });


      $('.back-bb .leftfeet').toggle(function() {
        $('.back-bb .leftfeet').css('background-position','0px -38px');
        $(this).attr("id","back-foot-left");
        renew('back-foot-left');  
      }, function() {
         $('.back-bb .leftfeet').css('background-position','0px 0px');
         $(this).removeAttr('id');
         renewlist();
      });


      $('.back-bb .rightfeet').toggle(function() {
        $('.back-bb .rightfeet').css('background-position','0px -38px');
        $(this).attr("id","back-foot-right");
        renew('back-foot-right');  
      }, function() {
         $('.back-bb .rightfeet').css('background-position','0px 0px');
         $(this).removeAttr('id');
         renewlist();
      });



    });



function renew(location){
    locations = [];
      symptoms = [];



  $('.front-bb div').each(function(i, el) {
      el = $(el);
      getposition = el.attr('id');
      if(el.attr('id') != undefined){
        locations.push(getposition);
      }
      
  });
  $('.back-bb div').each(function(i, el) {
      el = $(el);
      getposition = el.attr('id');
      if(el.attr('id') != undefined){
        locations.push(getposition);
      }
      
  });



  $( "#resultList li" ).each(function( index , count ) {
      if(index == 0){}else{
        var code = $(this).find('p').attr('id');
        symptoms.push(code);       
      }
     
    


  });

var data =  {"uniquePatientID":{"symps": symptoms,"AGE": "","SEX": "","location": locations}}


        console.log(JSON.stringify(data));
        $.ajax({
            type: "POST",
            url:  "http://130.211.170.206:8080/GER/medx",
            contentType :"application/json",
            dataType: "json",
            data: JSON.stringify(data),
              success: function(requestsymptoms) {
                
                $('.diseases .two').empty();
              for (var item in requestsymptoms.score) {

                var singleitem = requestsymptoms.score[ item ];

                $.each(singleitem, function( index, value ) {
       
                    single = value+index;

                     // value = value * 100;
                    //  value = Math.floor(value * 100);
                    //  console.log(value);

                    //  if(value < 20){
                    //   console.log('red');
                    //  }else if(value > 21 && value < 40 ){
                    //   console.log("orange");
                    //  }else if(value > 41 && value < 69){
                    //   console.log("yellow");
                    //  }else if(value > 70 && value < 84){
                    //   console.log("dark green");
                    //  }else if(value > 85 && value < 100){
                    //   console.log("final green");
                    //  }
                    // giveclass = '';


                    $('.diseases .two').append('<div class="one"><p>'+index+'</p><span>'+value+'%</span></div>');
                 });
              };


                $('#enginev2').empty();
                $('#symptominput').val('');
                $('#medenginesymptoms').empty();
                $.each( requestsymptoms.suggest, function( key, value ) {
                   


                      if(key < 8){
                       
                        $.each( value, function( key, values ) {
                             $('#medenginesymptoms').append("<li id="+key+">"+values+"</li>");
                            
                        });
                      }
                    
                });

              },
              failure: function(requestuser) {
                 console.log(' failed to connect');
              }
        });

}


    function renewlist(){
    locations = [];
      symptoms = [];



  $('.front-bb div').each(function(i, el) {
      el = $(el);
      getposition = el.attr('id');
      if(el.attr('id') != undefined || el.attr('id') != null){
        locations.push(getposition);
      }
      
  });
  $('.back-bb div').each(function(i, el) {
      el = $(el);
      getposition = el.attr('id');
      if(el.attr('id') != undefined || el.attr('id') != null){
        locations.push(getposition);
      }
      
  });



  $( "#resultList li" ).each(function( index , count ) {
 
      if(index == 0){}else{
        var code = $(this).find('p').attr('id');
        symptoms.push(code);       
      }
     
    


  });

var data =  {"uniquePatientID":{"symps": symptoms,"AGE": "","SEX": "","location": locations}}


        console.log(JSON.stringify(data));
        $.ajax({
            type: "POST",
            url:  "http://130.211.170.206:8080/GER/medx",
            contentType :"application/json",
            dataType: "json",
            data: JSON.stringify(data),
              success: function(requestsymptoms) {
                
                
              $('.diseases .two').empty();
              for (var item in requestsymptoms.score) {

                var singleitem = requestsymptoms.score[ item ];

                $.each(singleitem, function( index, value ) {
       
                    single = value+index;

                     // value = value * 100;
                     value = Math.floor(value * 100);
                    
                    $('.diseases .two').append('<div class="one"><p>'+index+'</p><span>'+value+'%</span></div>');
                 });
              };

                $('#enginev2').empty();
                $('#symptominput').val('');
                $('#medenginesymptoms').empty();
                $.each( requestsymptoms.suggest, function( key, value ) {
               


                      if(key < 8){
                        console.log(key);
                        $.each( value, function( key, values ) {
                    
                             $('#medenginesymptoms').append("<li id="+key+">"+values+"</li>");
                            
                        });
                      }
                    
                });

              },
              failure: function(requestuser) {
                 console.log(' failed to connect');
              }
        });
    }