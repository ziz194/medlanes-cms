/**
 * @file Main Medlanes SDK object will be defined here.
 * @author Paul Kuroedov
 * @module sdk
 */

///= include app/app.js
///= include app/**/*.js

var MedlanesSDK = MedlanesSDK || (/** @lends module:sdk */function () {
  /**
   * @func invokeEvent
   * Invoke an event with or without additional data for callbacks.
   *
   * @param {string} eventName - Name of event to invoke.
   * @param {any} data - Any number of values to pass to the callback functions, subscribed to the event.
   * @returns Doesn't return anything.
   */
  var invokeEvent = function(eventName){
    if(arguments.length > 1){
      amplify.publish.apply(null, arguments);
    }else{
      amplify.publish(eventName);
    }
  };


  /**
   * Main Medlanes SDK namespace
   * @namespace
   * @alias MedlanesSDK
   */
  var M = {
    /**
     * Standart function to be called with results of API AJAX call.
     *
     * @callback apiCallback
     * @param {string} data - JSON string, returned by API as is.
     */


    //DONE: QUESTIONFACTORY
    //DONE: AngularJS example
    //DONE: take a look at kickbox API
    //TODO: take a look at steroidJS
    //DONE: write examples
    //TODO: add example responses?
    //TODO: skip external validations
    /**
     * SDK initialisation function. Should be run only once, sets some parameters, needed for the API calls.
     * Sets cookie "medlanes_currency" to currency argument value. Currency is cleared when browser is closed.
     * @example
     *
     *  var testPrimary = {
     *    countryId:0,
     *    languageId:0,
     *    emailTemplate:"simple",
     *    funnel:"medlanes",
     *    company:"medlanes",
     *    currency:"EUR"
     *    },
     *    testMarketing = {
     *      realCity:"Anytown",
     *      realCountry:"Anyland",
     *      device:"anyPC",
     *      page:"anyPage",
     *      specialization:"any"
     *    };
     *  //returns {success:true}
     *  MedlanesSDK.init(testPrimary, testMarketing);
     * @param  {Object} primary - Primary parameters
     * @param  {number} primary.countryId - Country ID.
     * @param  {number} primary.languageId - Language ID.
     * @param  {string} primary.emailTemplate - E-mail template.
     * @param  {string} primary.funnel - Funnel ID.
     * @param  {string} primary.company - Company name.
     * @param  {string} primary.currency - Currency code to use.
     * @param  {Object} marketing - Marketing parameters
     * @param  {string} marketing.device - Device code to use.
     * @param  {string} marketing.page - Page code to use.
     * @param  {string} marketing.specialization - Specialization value.
     * @return {string} JSON string with success:true if init was successful and success:false and errors array with error codes if anything is wrong.
     */
    init: function(primary, marketing){
      if(typeof primary === 'undefined' || typeof marketing === 'undefined'){
        return JSON.stringify({success:false, errors:[M_err.EINITPARAMETERSMISSING]});
      }
      //DONE: store currency in the cookie

      // To check whether the parameters are set in the object. Validate method will return correspondent errors for undefined parameters.
      var pToValidate = {
        countryId:primary.countryId,
        languageId:primary.languageId,
        emailTemplate:primary.emailTemplate,
        funnel:primary.funnel,
        company:primary.company,
        currency:primary.currency
      },

      mToValidate = {
        device:marketing.device,
        page:marketing.page,
        realLanguage: navigator.language || navigator.userLanguage,
        specialization: marketing.specialization
      };

      var pValidated = M_validators.validate(pToValidate);
      var mValidated= M_validators.validate(mToValidate);

      if(pValidated.errors.length>0 || mValidated.errors.length>0){
        var errors = pValidated.errors.concat(mValidated.errors);
        return JSON.stringify({success:false, errors:errors});
      }

      // set parameters
      for (var param in pValidated.data) {
        if (pValidated.data.hasOwnProperty(param)) {
          M.setParam(param, pValidated.data[param]);
        }
      }

      for (param in mValidated.data) {
        if (mValidated.data.hasOwnProperty(param)) {
          M.setParam(param, mValidated.data[param]);
        }
      }

      //set currency cookie
      document.cookie = "medlanes_currency="+pValidated.data['currency']+";";

      var onCountrySuccess = function (geoipResponse) {
        if (geoipResponse.country.iso_code) {
          M.setParam("realCountry", geoipResponse.country.iso_code);
        }
      };

      var onCitySuccess = function (geoipResponse) {
        if(geoipResponse.city.names.en){
          M.setParam("realCity", geoipResponse.city.names.en);
        }
      };

      var onCountryError = function (error) {
          M.setParam("realCountry", "geoip error");
          return;
      };

      var onCityError = function (error) {
          M.setParam("realCity", "geoip error");
          return;
      };

      if(M.geoip){
        M.geoip.country(onCountrySuccess, onCountryError);
        M.geoip.city(onCitySuccess, onCityError);
      }else{
        M.setParam("realCountry", "geoip unavaliable");
        M.setParam("realCity", "geoip unavaliable");
      }

      return JSON.stringify({success:true});
    },

    /**
     * Form the Paypal data as a query string.
     *
     * @param  {string} price - Price to send to Paypal.
     * @return {string} String, containing all of the data, needed to be sent to Paypal.
     */    
    getPaypalData: function(price){
      return 'funnel=' + M.getParam('funnel') + '&company=' + M.getParam('company') + '&ti=' + M.getParam('ticketId') + '&to=' + M.getToken() + '&price=' + price;
    },

    /**
     * Subscribe to an event
     *
     * @example
     * MedlanesSDK.on("loginFailure", function(data){
     *   console.log("An error has occurred: "+data);
     * });
     * //In case of login failure event, prints corresponding error message to console.
     *
     * @param  {string} eventName - Event name to subscribe to.
     * @param  {function} callback - Callback to be called on that event.
     * @returns Doesn't return anything;
     */
    on: function(eventName, callback){
      amplify.subscribe(eventName, callback);
    },

    /**
     * Unsubscribe from an event
     *
     * @example
     * var print = function(data){
     *   //Function is executed only once, and unsubscribed after that.
     *   MedlanesSDK.off("loginFailure", print);
     *   console.log("An error has occurred: "+data);
     * };
     * MedlanesSDK.on("loginFailure", print);
     * @param  {string} eventName - Event name to unsubscribe from.
     * @param  {function} callback - The exact same callback to unsubscribe from the event.
     * @returns Doesn't return anything;
     */
    off: function(eventName, callback){
      amplify.unsubscribe(eventName, callback);
    },

    /**
     * Getter for authorization token, stored in private var.
     *
     * @example
     * //returns current token as a string, or undefined.
     * var tokenString = MedlanesSDK.getToken();
     * @return {string} Token, stored in private variable.
     */
    getToken: function(){
      return amplify.store.sessionStorage("medlanes_token");
    },


    /**
     * Setter for authorization token, stored in private var.
     *
     * @example
     * MedlanesSDK.setToken(newTokenString);
     * @param  {string} newToken - New token to be set.
     * @returns Doesn't return anything.
     */
    setToken: function(newToken){
      amplify.store.sessionStorage("medlanes_token", newToken);
    },

    /**
     * Getter wrapper for session storage. If paramName is omitted - returns an object with all saved parameters as properties.
     *
     * @example
     * //Gets "medlanes_currency" parameter's value.
     * var currencyString = MedlanesSDK.getParam('currency');
     * @example
     * //Example of output: Object {
     * //  medlanes_company: "medlanes"
     * //  medlanes_countryId: 0
     * //  medlanes_currency: "EUR"
     * //  medlanes_emailTemplate: "simple"
     * //}
     * var allParams = MedlanesSDK.getParam();
     * @param {string} paramName - Name of parameter to get.
     * @return {string} Parameter value or undefined.
     */
    getParam: function(paramName){
      //DONE: return all params on blank
      //DONE: validate sum
      if(M_validators.isntEmptyString(paramName)){
        return amplify.store.sessionStorage("medlanes_"+paramName);
      }else{
        return amplify.store();
      }
    },


    /**
     * Setter wrapper for session storage. Stores the string passed as paramValue in session storage. Overwrites the old value, if it already exists.
     *
     * @example
     * //Sets "medlanes_pName" parameter to "pValue" value.
     * MedlanesSDK.setParam('pName', 'pValue');
     * @param {string} paramName - Name of parameter to set.
     * @param {string} paramValue - Value of parameter to set.
     * @returns Doesn't return anything.
     */
    setParam: function(paramName, paramValue){
      amplify.store.sessionStorage("medlanes_"+paramName, paramValue);
    },

    /**
     * New Dummy User creation function.
     *
     * Current API call:
     * {"action":"user_dummy"}
     *
     * Call details: always successful, returns auth token.
     *
     * @example
     * //Events style
     * //Define event listener functions
     * var onSuccessFunction = function(data){
     *   //Call was successful. Work with data.
     * }
     *
     * var onFailureFunction = function(data){
     *   //Call was unsuccessful. Handle the errors.
     * }
     * //Subscribe to both Success and Failure events with corresponding functions.
     * MedlanesSDK.on("userDummySuccess", onSuccessFunction);
     * MedlanesSDK.on("userDummyFailure", onFailureFunction);
     * //Make the call without arguments.
     * MedlanesSDK.userDummy();
     * @example
     * //Callback style
     * //Define the callback function
     * var callback = function(response){
     *   var parsedResp = MedlanesSDK.helpers.tryParseJson(response);
     *   if(!parsedResp || !parsedResp.success){
     *     //An error has occurred. 
     *   }else{
     *     //Work with the response.
     *   }
     * }
     * 
     * //Pass the callback function as the first argument.
     * MedlanesSDK.userDummy(callback);
     * @param {apiCallback} callback - The callback that handles the response.
     * @fires MedlanesSDK#userDummySuccess
     * @fires MedlanesSDK#userDummyFailure
     * @returns Doesn't return anything at all.
     */
    userDummy: function(callback){
      var request = {action:"user_dummy"};
      M_ajax.wrapCall(callback, request, "userDummy");
    },

    /**
     * New User Sign-Up function.
     *
     * Current API call:
     * {"action":"user_signup","parameters":{"email":"ciobanu_ffo2@43ideas.com","doctorCode":"your_doctor_code","firstname":"Firstname","auth_hash":"your_sha1_string","lastname":"Ciobanu","age":"20-30","gender":"m","idLanguage":1,"idCountry":1,"email_template":"signup","company":"medlanes"}}
     *
     * It seems that right now ALL of the parameters are optional. I've passed an empty object and still got a successiful response.
     *
     * Call details: email and password are required.
     * Use userDummy to create a user without email\password.
     * @example
     * //Events style
     * //Define event listener functions
     * var onSuccessFunction = function(data){
     *   //Call was successful. Work with data.
     * }
     *
     * var onFailureFunction = function(data){
     *   //Call was unsuccessful. Handle the errors.
     * }
     * //Subscribe to both Success and Failure events with corresponding functions.
     * MedlanesSDK.on("signUpSuccess", onSuccessFunction);
     * MedlanesSDK.on("signUpFailure", onFailureFunction);
     * //Make the call with needed arguments. Pass null as the callback argument.
     * //If you need to omit one or more optional string arguments, pass empty strings instead.
     * MedlanesSDK.signUp(null, "<email>", "<first name>", "<last name>", "<password>", "<age>", "<gender boolean>");
     * @example
     * //Callback style
     * //Define the callback function
     * var callback = function(response){
     *   var parsedResp = MedlanesSDK.helpers.tryParseJson(response);
     *   if(!parsedResp || !parsedResp.success){
     *     //An error has occurred. 
     *   }else{
     *     //Work with the response.
     *   }
     * }
     * 
     * //Pass the callback function as the first argument.
     * //If you need to omit one or more optional string arguments, pass empty strings instead.     
     * MedlanesSDK.signUp(callback, "<email>", "<first name>", "<last name>", "<password>", "<age>", "<gender boolean>");
     * @param {apiCallback} callback - The callback that handles the response.
     * @param {string} email - The new user's e-mail.
     * @param {string} [firstName] - The new user's first name.
     * @param {string} [lastName] - The new user's last name.
     * @param {string} password - The new user's password.
     * @param {string} [age] - The new user's age.
     * @param {boolean} [isMale] - The new user's gender. Pass 'true' if male, 'false' if female.
     * @fires MedlanesSDK#signUpSuccess
     * @fires MedlanesSDK#signUpFailure
     * @returns Doesn't return anything at all. (Should we use promises?)
     */
    signUp: function(callback, email, firstName, lastName, password, age, isMale){
      var reqData = {action:"user_signup",
                    parameters:{}},
                    dataToValidate = {};
      dataToValidate.email = email;
      dataToValidate.firstname = firstName;
      dataToValidate.lastname = lastName;
      dataToValidate.password = password;
      dataToValidate.age = age;
      dataToValidate.gender = isMale;

      var processedData = M_validators.validate(dataToValidate);
      console.log(processedData);
      reqData.parameters = processedData.data;
      if(M_helpers.isDefined(M.getParam("company"))){
        reqData.parameters.company = M.getParam("company");
      }
      if(M_helpers.isDefined(M.getParam("languageId"))){
        reqData.parameters.idLanguage = M.getParam("languageId");
      }
      if(M_helpers.isDefined(M.getParam("countryId"))){
        reqData.parameters.idCountry = M.getParam("countryId");
      }
      if(processedData.errors.length>0){
        var retValue = JSON.stringify({success:false, errors:processedData.errors});
        invokeEvent("signUpFailure", retValue);
        M_helpers.invokeCallback(callback, retValue);
        return;
      }

      M.ajax.wrapCall(callback, reqData, "signUp");
    },

    /**
     * User login function
     *
     * Current API calls:
     *
     * 1) {"action":"login","parameters":{"email":"Email id","password":"Password auth hash"}}
     *
     * 2) {"action":"login","parameters":{"login_code":"abcd1234"}}
     *
     * Call details: both params are required for the first call.
     *
     * @param {apiCallback} callback - The callback that handles the response.
     * @param {string} email - The e-mail of user trying to login.
     * @param {string} password - The password of user trying to login.
     * @fires MedlanesSDK#loginSuccess
     * @fires MedlanesSDK#loginFailure
     * @returns Doesn't return anything at all.
     * @todo Case when mail is busy
     */
    login: function(callback, email, password, logincode){
      //TODO: add login code case.
      var reqData = {action:"login",
                    parameters:{}},
                    dataToValidate = {};
      dataToValidate.email = email;
      dataToValidate.password = password;

      var processedData = M_validators.validate(dataToValidate);
      reqData.parameters = processedData.data;
      if(processedData.errors.length>0){
        var retValue = JSON.stringify({success:false, errors:processedData.errors});
        invokeEvent("loginFailure", retValue);
        M_helpers.invokeCallback(callback, retValue);
        return;
      }

      M.ajax.wrapCall(callback, reqData, "login");
    },

    /**
     * Password reset function
     *
     * As the API comments state, can be used only once per hour per e-mail.
     *
     * Current API call:
     * {"action":"user_reset","parameters":{"email":"ciobanu_ffo2@yahoo.de"}}
     *
     * Call details: e-mail is required and must be valid and belong to one of the users.
     *
     * @example
     * //Events style
     * //Define event listener functions
     * var onSuccessFunction = function(data){
     *   //Call was successful. Work with data.
     * }
     *
     * var onFailureFunction = function(data){
     *   //Call was unsuccessful. Handle the errors.
     * }
     * //Subscribe to both Success and Failure events with corresponding functions.
     * MedlanesSDK.on("resetPasswordSuccess", onSuccessFunction);
     * MedlanesSDK.on("resetPasswordFailure", onFailureFunction);
     * //Make the call with needed arguments. Pass null as the callback argument.
     * MedlanesSDK.resetPassword(null, "<email>");
     * @example
     * //Callback style
     * //Define the callback function
     * var callback = function(response){
     *   var parsedResp = MedlanesSDK.helpers.tryParseJson(response);
     *   if(!parsedResp || !parsedResp.success){
     *     //An error has occurred. 
     *   }else{
     *     //Work with the response.
     *   }
     * }
     * 
     * //Pass the callback function as the first argument.
     * MedlanesSDK.resetPassword(callback, "<email>");
     * @param {apiCallback} callback - The callback that handles the response.
     * @param {string} email - The e-mail of user trying to reset the password. Must be valid and belong to one of the users.
     * @fires MedlanesSDK#resetPasswordSuccess
     * @fires MedlanesSDK#resetPasswordFailure
     * @returns Doesn't return anything at all. (Should we use promises?)
     */
    resetPassword: function(callback, email){
      var reqData = {action:"user_reset",
                    parameters:{}},
                    dataToValidate = {};
      dataToValidate.email = email;

      var processedData = M_validators.validate(dataToValidate);
      reqData.parameters = processedData.data;
      if(processedData.errors.length>0){
        var retValue = JSON.stringify({success:false, errors:processedData.errors});
        invokeEvent("resetPasswordFailure", retValue);
        M_helpers.invokeCallback(callback, retValue);
        return;
      }

      M.ajax.wrapCall(callback, reqData, "resetPassword");
    },

    /**
     * API call wrapper for list of all questions.
     * Current API call: {"action":"question_list","auth":{"token":"2_n1rAz4Or2GHsCSGjF5Sf"}}
     *
     * @example
     * //Events style
     * //Define event listener functions
     * var onSuccessFunction = function(data){
     *   //Call was successful. Work with data.
     * }
     *
     * var onFailureFunction = function(data){
     *   //Call was unsuccessful. Handle the errors.
     * }
     * //Subscribe to both Success and Failure events with corresponding functions.
     * MedlanesSDK.on("getQuestionListSuccess", onSuccessFunction);
     * MedlanesSDK.on("getQuestionListFailure", onFailureFunction);
     * //Make the call without arguments(you may as well make the call with null, since the callback is the only argument).
     * MedlanesSDK.getQuestionList();
     * @example
     * //Callback style
     * //Define the callback function
     * var callback = function(response){
     *   var parsedResp = MedlanesSDK.helpers.tryParseJson(response);
     *   if(!parsedResp || !parsedResp.success){
     *     //An error has occurred. 
     *   }else{
     *     //Work with the response.
     *   }
     * }
     * 
     * //Pass the callback function as the first argument.
     * MedlanesSDK.getQuestionList(callback);
     * @param {apiCallback} callback - The callback that handles the response
     * @fires MedlanesSDK#getQuestionListSuccess
     * @fires MedlanesSDK#getQuestionListFailure
     * @returns Doesn't return anything.
     */
    getQuestionList: function(callback){
      var reqData = {action:"question_list",
                      auth:{}
                    };

      if(M.getToken()){
        reqData.auth.token = M.getToken();
      }else{
        var retVal = JSON.stringify({success:false, errors:[M_err.ETOKENISNOTDEFINED]});
        invokeEvent("getQuestionListFailure", retVal);
        M_helpers.invokeCallback(callback, retVal);
      }

      M.ajax.wrapCall(callback, reqData, "getQuestionList");
    },

    /**
     * Transaction finish method, used for payment testing.
     * Current API call:
     * {"action":"transaction_finish","auth":{"token":"18472_gI1Sp1fi0BhMigOqTuvj"},"parameters":{"id":"12324","amount":29}}
     *
     * @param {apiCallback} callback - The callback that handles the response
     * @param {string} ticketId - Id of ticket to be paid.
     * @param {number} amount - Amount of currency to be paid.
     * @fires MedlanesSDK#finishTransactionSuccess
     * @fires MedlanesSDK#finishTransactionFailure
     * @todo Make example?
     * @returns Doesn't return anything.
     */
    finishTransaction: function(callback, ticketId, amount){
      var request = {action:"transaction_finish",
                    auth:{token:"18472_gI1Sp1fi0BhMigOqTuvj"},
                    parameters:{}},
        dataToValidate = {},
        errors = [];
      dataToValidate.id = ticketId;
      dataToValidate.amount = amount;

      if(M.getToken()){
        request.auth.token = M.getToken();
      }else{
        errors.push(M_err.ETOKENISNOTDEFINED);
      }

      var validatedData = M_validators.validate(dataToValidate);
      if(validatedData.errors.length>0){
        var retValue = JSON.stringify({success:false, errors:validatedData.errors.concat(errors)});
        invokeEvent("finishTransactionFailure", retValue);
        M_helpers.invokeCallback(callback, retValue);
        return;
      }
      request.parameters = validatedData.data;

      M_ajax.wrapCall(callback, request, "finishTransaction");
    },

    /**
     * Coupon checking call, used for verifying coupon validity.
     * Current API call:
     * {"action":"validate_coupon","parameters":{"coupon_code":coupon}};
     *
     * @example
     * //Events style
     * //Define event listener functions
     * var onSuccessFunction = function(data){
     *   //Call was successful. Work with data.
     * }
     *
     * var onFailureFunction = function(data){
     *   //Call was unsuccessful. Handle the errors.
     * }
     * //Subscribe to both Success and Failure events with corresponding functions.
     * MedlanesSDK.on("checkCouponSuccess", onSuccessFunction);
     * MedlanesSDK.on("checkCouponFailure", onFailureFunction);
     * //Make the call with needed arguments. Pass null as the callback argument.
     * MedlanesSDK.checkCoupon(null, "<coupon code>");
     * @example
     * //Callback style
     * //Define the callback function
     * var callback = function(response){
     *   var parsedResp = MedlanesSDK.helpers.tryParseJson(response);
     *   if(!parsedResp || !parsedResp.success){
     *     //An error has occurred. 
     *   }else{
     *     //Work with the response.
     *   }
     * }
     * 
     * //Pass the callback function as the first argument.
     * MedlanesSDK.checkCoupon(callback, "<coupon code>");
     * @param {apiCallback} callback - The callback that handles the response
     * @param {string} couponCode - Code of coupon to check.
     * @fires MedlanesSDK#checkCouponSuccess
     * @fires MedlanesSDK#checkCouponFailure
     * @returns Doesn't return anything.
     */
    checkCoupon: function(callback, couponCode){
      var request = {action:"validate_coupon",
                      parameters:{}};
      var validatedData = M_validators.validate({coupon_code:couponCode});
      if(validatedData.errors.length>0){
        var retValue = JSON.stringify({success:false, errors:validatedData.errors});
        invokeEvent("checkCouponFailure", retValue);
        M_helpers.invokeCallback(callback, retValue);
        return;
      }
      request.parameters = validatedData.data;

      M_ajax.wrapCall(callback, request, "checkCoupon");
    },

    /**
     * Call which adds the question.
     * Current API call:
     * {action:"question_add",
        auth:{token:"18472_gI1Sp1fi0BhMigOqTuvj"},
        parameters:{"funnel":"ja2",
        "company":"askadoctor",
        "text":msg,
        "state":"stored",
        "message_properties":[{"name":"tried","value":tried,"type":9, "datatype":"text"}]}}
     *
     * @param {apiCallback} callback - The callback that handles the response
     * @param {string} message - Message of the question.
     * @param {string} [couponCode] - Coupon code.
     * @param {string} [triedMessage] - "What have you tried?" optional message.
     * @fires MedlanesSDK#addQuestionSuccess
     * @fires MedlanesSDK#addQuestionFailure
     * @returns Doesn't return anything.
     */
    addQuestion: function(callback, message, couponCode, triedMessage){
      //TODO: write example
      var reqData = {
        action:"question_add",
        auth:{},
        parameters:{
          state:"stored",
          funnel:M.getParam("funnel"),
          company:M.getParam("company"),
          message_properties:[]
        }
      },
        errors=[];

      if(M.getToken()){
        reqData.auth.token = M.getToken();
      }else{
        errors.push(M_err.ETOKENISNOTDEFINED);
      }

      if(!M_validators.isntEmptyString(message)){
        var retValue = JSON.stringify({success:false, errors:errors.concat([M_err.EQUESTIONMESSAGEEMPTY])});
        invokeEvent("addQuestionFailure", retValue);
        M_helpers.invokeCallback(callback, retValue);
      }else{
        reqData.parameters.text = message;
      }

      if(M_validators.isntEmptyString(triedMessage)){
        reqData.parameters.message_properties.push({
          name:"tried",
          value:triedMessage,
          type:9,
          datatype:"text"
        });
      }

      var onCouponCheck = function (response){
        response = M_helpers.tryParseJson(response);
        if(response && response.success){
          //TODO: dont add the question if coupon is there, but invalid
          reqData.parameters.coupon = couponCode;
          if(couponCode==="ptest"){
            M.setParam("couponPrice", response.price);
          }
        }
        M.on("addQuestionSuccess", M.sendAdditionalData);
        M_ajax.wrapCall(callback, reqData, "addQuestion");
      };

      M.checkCoupon(onCouponCheck, couponCode);
      // (check if coupon is valid. if not - return an error, fire Failure event).
      // http://askadoctor.today/optional.php
      //TODO: implement optional : name , gender , what have you tried, age , coupon.
    },

    /**
     * Call which, after the question is added, sends the additional marketing data.
     * Is called by addQuestion API call. Shouldn't be called directly.
     * Current API call:
     * {"action":"additional_data","auth":{"token":token}, "parameters":{"idTicket":ticket,"data":[{"funnelCountry":funnelcountry},{"funnelLanguage":funnellanguage},{"realCountry":rcountry},{"realLanguage":userLang},{"realCity":rcity},{"device":device},{"specialization":specialization},{"funnel":funnel},{"page":page}]}}
     *
     * @param {string} addQuestionResponse - JSON response, returned by the addQuestion call.
     * @param {string} [couponCode] - Coupon code.
     * @param {string} [triedMessage] - "What have you tried?" optional message.
     * @fires MedlanesSDK#sendAdditionalDataSuccess
     * @fires MedlanesSDK#sendAdditionalDataFailure
     * @todo Any checks on whether the init function was called?
     * @returns Doesn't return anything.
     */
    sendAdditionalData: function(addQuestionResponse){
      M.off("addQuestionSuccess", M.sendAdditionalData);
      var parsedResponse = M_helpers.tryParseJson(addQuestionResponse);
      if(!parsedResponse){
        invokeEvent("sendAdditionalDataFailure", JSON.stringify({success:false, errors:[M_err.EJSONINVALID]})); //TODO: was 'true'
      }
      M.setParam('ticketId', parsedResponse.result.firstMessage.idTicket);
      var reqData = {
        action:"additional_data",
        auth:{
          token:M.getToken()
        },
        parameters:{
          idTicket:parsedResponse.result.firstMessage.idTicket,
          data:[
            {
              funnelCountry:M.getParam("countryId")
            },
            {
              funnelLanguage:M.getParam("languageId")
            },
            {
              realCountry:M.getParam("realCountry")
            },
            {
              realLanguage:M.getParam("realLanguage")
            },
            {
              realCity:M.getParam("realCity")
            },
            {
              device:M.getParam("device")
            },
            {
              specialization:M.getParam("specialization")
            },{
              funnel:M.getParam("funnel")
            },{
              page:M.getParam("page")
            }
          ]
        }
      };

      M.ajax.wrapCall(null, reqData, "sendAdditionalData");
    },

    /**
     * API call, that adds a message to the certain ticket.
     * http://screencast.com/t/zhnrpJVFc4r
     * Current API call:
     * {"action":"message_add","auth":{"token":"9214_26hf5hWVlHszyuLsW6u0"},"parameters":{"text":"asgaggsa","idTicket":"14144"}}
     *
     * @example
     * //Events style
     * //Define event listener functions
     * var onSuccessFunction = function(data){
     *   //Call was successful. Work with data.
     * }
     *
     * var onFailureFunction = function(data){
     *   //Call was unsuccessful. Handle the errors.
     * }
     * //Subscribe to both Success and Failure events with corresponding functions.
     * MedlanesSDK.on("addMessageSuccess", onSuccessFunction);
     * MedlanesSDK.on("addMessageFailure", onFailureFunction);
     * //Make the call with needed arguments. Pass null as the callback argument.
     * MedlanesSDK.addMessage(null, "<message text>", "<question ticket ID>");
     * @example
     * //Callback style
     * //Define the callback function
     * var callback = function(response){
     *   var parsedResp = MedlanesSDK.helpers.tryParseJson(response);
     *   if(!parsedResp || !parsedResp.success){
     *     //An error has occurred. 
     *   }else{
     *     //Work with the response.
     *   }
     * }
     * 
     * //Pass the callback function as the first argument.
     * MedlanesSDK.addMessage(callback, "<message text>", "<question ticket ID>");
     * @param {apiCallback} callback - The callback that handles the response.
     * @param {string} text - Message text to add.
     * @param {string} ticketId - Ticket ID of question, to which the message should be added.
     * @fires MedlanesSDK#addMessageSuccess
     * @fires MedlanesSDK#addMessageFailure
     * @returns Doesn't return anything.
     */
    addMessage: function(callback, text, ticketId){
      var request = {
          action:"message_add",
          auth:{},
          parameters:{}
        },
        dataToValidate = {},
        errors = [];
      dataToValidate.text = text;
      dataToValidate.id = ticketId;

      if(M.getToken()){
        request.auth.token = M.getToken();
      }else{
        errors.push(M_err.ETOKENISNOTDEFINED);
      }

      var validatedData = M_validators.validate(dataToValidate);
      if(validatedData.errors.length>0 || errors.length>0){
        var retValue = JSON.stringify({success:false, errors:validatedData.errors.concat(errors)});
        invokeEvent("addMessageFailure", retValue);
        M_helpers.invokeCallback(callback, retValue);
        return;
      }
      request.parameters.idTicket = validatedData.data.id;
      request.parameters.text = validatedData.data.text;

      M_ajax.wrapCall(callback, request, "addMessage");

    },

    /**
     * API call to unsubscribe user's mail.
     * Current API call:
     * {"action":"unsubscribe_me","auth":{"token":"2_n1rAz4Or2GHsCSGjF5Sf"}}
     *
     * @example
     * //Events style
     * //Define event listener functions
     * var onSuccessFunction = function(data){
     *   //Call was successful. Work with data.
     * }
     *
     * var onFailureFunction = function(data){
     *   //Call was unsuccessful. Handle the errors.
     * }
     * //Subscribe to both Success and Failure events with corresponding functions.
     * MedlanesSDK.on("unsubscribeMailSuccess", onSuccessFunction);
     * MedlanesSDK.on("unsubscribeMailFailure", onFailureFunction);
     * //Make the call without arguments(you may as well make the call with null, since the callback is the only argument).
     * MedlanesSDK.unsubscribeMail();
     * @example
     * //Callback style
     * //Define the callback function
     * var callback = function(response){
     *   var parsedResp = MedlanesSDK.helpers.tryParseJson(response);
     *   if(!parsedResp || !parsedResp.success){
     *     //An error has occurred. 
     *   }else{
     *     //Work with the response.
     *   }
     * }
     * 
     * //Pass the callback function as the first argument.
     * MedlanesSDK.unsubscribeMail(callback);
     * @param {apiCallback} callback - The callback that handles the response.
     * @fires MedlanesSDK#unsubscribeMailSuccess
     * @fires MedlanesSDK#unsubscribeMailFailure
     * @returns Doesn't return anything.
     */
    unsubscribeMail: function(callback){
      var request = {
        action:"unsubscribe_me",
        auth:{}
      }
      if(M.getToken()){
        request.auth.token = M.getToken();
      }else{
        var retValue = JSON.stringify({success:false, errors:[M_err.ETOKENISNOTDEFINED]});
        invokeEvent("unsubscribeMailFailure", retValue);
        M_helpers.invokeCallback(callback, retValue);
        return;
      }

      M_ajax.wrapCall(callback, request, "unsubscribeMail");
    },

    /** 
     * Gives certain details about the user. 
     * Must be logged in for this call.
     * @todo To be changed soon.
     * Current API call:
     * {"action":"me","auth":{"token":"2_n1rAz4Or2GHsCSGjF5Sf"}}
     *
     * @example
     * //Events style
     * //Define event listener functions
     * var onSuccessFunction = function(data){
     *   //Call was successful. Work with data.
     * }
     *
     * var onFailureFunction = function(data){
     *   //Call was unsuccessful. Handle the errors.
     * }
     * //Subscribe to both Success and Failure events with corresponding functions.
     * MedlanesSDK.on("getUserInfoSuccess", onSuccessFunction);
     * MedlanesSDK.on("getUserInfoFailure", onFailureFunction);
     * //Make the call without arguments(you may as well make the call with null, since the callback is the only argument).
     * MedlanesSDK.getUserInfo();
     * @example
     * //Callback style
     * //Define the callback function
     * var callback = function(response){
     *   var parsedResp = MedlanesSDK.helpers.tryParseJson(response);
     *   if(!parsedResp || !parsedResp.success){
     *     //An error has occurred. 
     *   }else{
     *     //Work with the response.
     *   }
     * }
     * 
     * //Pass the callback function as the first argument.
     * MedlanesSDK.getUserInfo(callback);
     * @param {apiCallback} callback - The callback that handles the response.
     * @fires MedlanesSDK#getUserInfoSuccess
     * @fires MedlanesSDK#getUserInfoFailure
     * @returns Doesn't return anything.
     */
    getUserInfo: function(callback){
      var request = {
        action:"me",
        auth:{}
      }
      if(M.getToken()){
        request.auth.token = M.getToken();
      }else{
        var retValue = JSON.stringify({success:false, errors:[M_err.ETOKENISNOTDEFINED]});
        invokeEvent("getUserInfoFailure", retValue);
        M_helpers.invokeCallback(callback, retValue);
        return;
      }

      M_ajax.wrapCall(callback, request, "getUserInfo");
    },

    /**
     * Update person's settings - password, firstname, lastname.
     * Must be logged in for this call.
     * Current API call:
     * {"action":"user_update","auth":{"token":"9214_26hf5hWVlHszyuLsW6u0"},"parameters":{"password":"","firstname":"Danielaa","lastname":"Atanasovski"}}
     *
     * @example
     * //Events style
     * //Define event listener functions
     * var onSuccessFunction = function(data){
     *   //Call was successful. Work with data.
     * }
     *
     * var onFailureFunction = function(data){
     *   //Call was unsuccessful. Handle the errors.
     * }
     * //Subscribe to both Success and Failure events with corresponding functions.
     * MedlanesSDK.on("updateUserInfoSuccess", onSuccessFunction);
     * MedlanesSDK.on("updateUserInfoFailure", onFailureFunction);
     * //Make the call with needed arguments. Pass null as the callback argument.
     * //If you need to omit one or more optional string arguments, pass empty strings instead.
     * MedlanesSDK.updateUserInfo(null, "<password>", "<first name>", "<last name>");
     * @example
     * //Callback style
     * //Define the callback function
     * var callback = function(response){
     *   var parsedResp = MedlanesSDK.helpers.tryParseJson(response);
     *   if(!parsedResp || !parsedResp.success){
     *     //An error has occurred. 
     *   }else{
     *     //Work with the response.
     *   }
     * }
     * 
     * //Pass the callback function as the first argument.
     * //If you need to omit one or more optional string arguments, pass empty strings instead.
     * MedlanesSDK.updateUserInfo(callback, "<password>", "<first name>", "<last name>");
     * @param {apiCallback} callback - The callback that handles the response
     * @param {string} password - Password to update.
     * @param {string} [firstname] - Firstname to update.
     * @param {string} [lastname] - Lastname to update.
     * @fires MedlanesSDK#updateUserInfoSuccess
     * @fires MedlanesSDK#updateUserInfoFailure
     * @returns Doesn't return anything.
     */
    updateUserInfo: function(callback, password, firstname, lastname){
      var request = {
          action:"user_update",
          auth:{},
          parameters:{}
        },
        dataToValidate = {},
        errors = [];
      dataToValidate.password = password;
      dataToValidate.firstname = firstname;
      dataToValidate.lastname = lastname;

      if(M.getToken()){
        request.auth.token = M.getToken();
      }else{
        errors.push(M_err.ETOKENISNOTDEFINED);
      }

      var validatedData = M_validators.validate(dataToValidate);
      if(validatedData.errors.length>0 || errors.length>0){
        var retValue = JSON.stringify({success:false, errors:validatedData.errors.concat(errors)});
        invokeEvent("updateUserInfoFailure", retValue);
        M_helpers.invokeCallback(callback, retValue);
        return;
      }
      request.parameters = validatedData.data;

      M_ajax.wrapCall(callback, request, "updateUserInfo");
    },

    /**
     * Shows full details of a ticketId that is requested.
     * Must be logged in for this call.
     * http://screencast.com/t/uurvRVuA
     * Current API call:
     * {"action":"question_details","auth":{"token":"2_n1rAz4Or2GHsCSGjF5Sf"},"parameters":{"id":17}}
     *
     * @example
     * //Events style
     * //Define event listener functions
     * var onSuccessFunction = function(data){
     *   //Call was successful. Work with data.
     * }
     *
     * var onFailureFunction = function(data){
     *   //Call was unsuccessful. Handle the errors.
     * }
     * //Subscribe to both Success and Failure events with corresponding functions.
     * MedlanesSDK.on("getQuestionDetailsSuccess", onSuccessFunction);
     * MedlanesSDK.on("getQuestionDetailsFailure", onFailureFunction);
     * //Make the call with needed arguments. Pass null as the callback argument.
     * MedlanesSDK.getQuestionDetails(null, "<question ticket ID>");
     * @example
     * //Callback style
     * //Define the callback function
     * var callback = function(response){
     *   var parsedResp = MedlanesSDK.helpers.tryParseJson(response);
     *   if(!parsedResp || !parsedResp.success){
     *     //An error has occurred. 
     *   }else{
     *     //Work with the response.
     *   }
     * }
     * 
     * //Pass the callback function as the first argument.
     * MedlanesSDK.getQuestionDetails(callback, "<question ticket ID>");
     * @param {apiCallback} callback - The callback that handles the response.
     * @param {string} ticketId - Ticket ID of a question.
     * @fires MedlanesSDK#getQuestionDetailsSuccess
     * @fires MedlanesSDK#getQuestionDetailsFailure
     * @returns Doesn't return anything.
     */
    getQuestionDetails: function(callback, ticketId){
      var request = {
        action:"question_details",
        auth:{},
        parameters:{}
      },
        dataToValidate = {},
        errors = [];
      dataToValidate.id = ticketId;

      if(M.getToken()){
        request.auth.token = M.getToken();
      }else{
        errors.push(M_err.ETOKENISNOTDEFINED);
      }

      var validatedData = M_validators.validate(dataToValidate);
      if(validatedData.errors.length>0 || errors.length>0){
        var retValue = JSON.stringify({success:false, errors:validatedData.errors.concat(errors)});
        invokeEvent("getQuestionDetailsFailure", retValue);
        M_helpers.invokeCallback(callback, retValue);
        return;
      }
      request.parameters = validatedData.data;

      M_ajax.wrapCall(callback, request, "getQuestionDetails");
    },

    /**
     * Deletes a certain message from the user panel.
     * Must be logged in for this call.
     * Current API call:
     * {"action":"message_delete","auth":{"token":"2_n1rAz4Or2GHsCSGjF5Sf"},"parameters":{"idMessage":5}}
     *
     * @example
     * //Events style
     * //Define event listener functions
     * var onSuccessFunction = function(data){
     *   //Call was successful. Work with data.
     * }
     *
     * var onFailureFunction = function(data){
     *   //Call was unsuccessful. Handle the errors.
     * }
     * //Subscribe to both Success and Failure events with corresponding functions.
     * MedlanesSDK.on("deleteMessageSuccess", onSuccessFunction);
     * MedlanesSDK.on("deleteMessageFailure", onFailureFunction);
     * //Make the call with needed arguments. Pass null as the callback argument.
     * MedlanesSDK.deleteMessage(null, "<message ID>");
     * @example
     * //Callback style
     * //Define the callback function
     * var callback = function(response){
     *   var parsedResp = MedlanesSDK.helpers.tryParseJson(response);
     *   if(!parsedResp || !parsedResp.success){
     *     //An error has occurred. 
     *   }else{
     *     //Work with the response.
     *   }
     * }
     * 
     * //Pass the callback function as the first argument.
     * MedlanesSDK.deleteMessage(callback, "<message ID>");
     * @param {apiCallback} callback - The callback that handles the response.
     * @param {string} messageId - ID of message to delete.
     * @fires MedlanesSDK#deleteMessageSuccess
     * @fires MedlanesSDK#deleteMessageFailure
     * @returns Doesn't return anything.
     */
    deleteMessage: function(callback, messageId){
      var request = {
        action:"message_delete",
        auth:{},
        parameters:{}
      },
        errors = [];
      
      if(M.getToken()){
        request.auth.token = M.getToken();
      }else{
        errors.push(M_err.ETOKENISNOTDEFINED);
      }

      var validatedData = M_validators.validate({idMessage:messageId});
      if(validatedData.errors.length>0 || errors.length>0){
        var retValue = JSON.stringify({success:false, errors:validatedData.errors.concat(errors)});
        invokeEvent("deleteMessageFailure", retValue);
        M_helpers.invokeCallback(callback, retValue);
        return;
      }
      request.parameters = validatedData.data;

      M_ajax.wrapCall(callback, request, "deleteMessage");
    }

// {"action":"login","parameters":{"login_code":"abcd1234"}} -- add login_code as 1 more param to the login action 
// There is a retargeting process going with emailing people and giving them link to automaticly login with this oncetime login code in their panel. 
// http://screencast.com/t/yYlG89ipQ5CQ
  };

  /** Validators namespace will be here
   * @namespace
   * @alias MedlanesSDK.validators
   */
  var M_validators = M.validators = {

    /**
     * validate - General validate function.
     *
     * @param  {array} parameters Key-value array of parameters to validate.
     * @returns {Object} result - The result of parameters validation.
     * @returns {Object} result.data - The valid parameters as key-value.
     * @returns {array} result.errors - The validation errors array.
     */
    validate: function(parameters){
      var result = {data:{}, errors:[]},
      config = {
        email:{
          layers:[
          //TODO: kickbox API
            {
              validationFunction: this.isValidMail,
              errorCode: M_err.EMAILINVALID
            }
          ]
        },
        password:{
          layers:[
            {
              validationFunction: this.isValidPassword,
              // errorCode: M_err.EPASSWORDEMPTY
            }
          ]
        },
        firstname:{
          layers:[
            {
              validationFunction: this.isntEmptyString
            }
          ]
        },
        lastname:{
          layers:[
            {
              validationFunction: this.isntEmptyString
            }
          ]
        },
        age:{
          layers:[
            {
              validationFunction: this.isntEmptyString
            }
          ]
        },
        emailTemplate:{
          layers:[
            {
              validationFunction: this.isntEmptyString,
              errorCode: M_err.EMAILTEMPLATEINVALID
            }
          ]
        },
        funnel:{
          layers:[
            {
              validationFunction: this.isntEmptyString,
              errorCode: M_err.EFUNNELINVALID
            }
          ]
        },
        company:{
          layers:[
            { 
              validationFunction: this.isntEmptyString,
              errorCode: M_err.ECOMPANYINVALID
            }
          ]
        },
        realCity:{
          layers:[
            {
              validationFunction: this.isntEmptyString,
              errorCode: M_err.EREALCITYINVALID
            }
          ]
        },
        realCountry:{
          layers:[
            {
              validationFunction: this.isntEmptyString,
              errorCode: M_err.EREALCOUNTRYINVALID
            }
          ]
        },
        realLanguage:{
          layers:[
            {
              validationFunction: this.isntEmptyString,
              errorCode: M_err.EREALLANGUAGEINVALID
            }
          ]
        },
        currency:{
          layers:[
            {
              validationFunction: this.isntEmptyString,
              errorCode: M_err.ECURRENCYINVALID
            }
          ]
        },
        id:{
          layers:[
            {
              validationFunction: this.isntEmptyString,
              errorCode: M_err.ETICKETIDINVALID
            }
          ]
        },
        amount:{
          layers:[
            {
              validationFunction: this.isValidSum,
              errorCode: M_err.EAMOUNTINVALID,
              customValue: parseInt
            }
          ]
        },
        coupon_code:{
          layers:[
            {
              validationFunction: this.isValidCouponCode,
              errorCode: M_err.ECOUPONCODEINVALID
            }
          ]
        },
        gender:{
          layers:[
            {
              validationFunction: this.isValidGender
            }
          ],
          customValue: M_helpers.boolToGender
        },
        countryId:{
          layers:[
            {
              validationFunction: this.isNumber,
              errorCode: M_err.ECOUNTRYIDINVALID
            }
          ]
        },
        languageId:{
          layers:[
            {
              validationFunction: this.isNumber,
              errorCode: M_err.ELANGUAGEIDINVALID
            }
          ]
        },
        device:{
          layers:[
            {
              validationFunction:this.isntEmptyString,
              errorCode: M_err.EDEVICEINVALID
            }
          ]
        },
        page:{
          layers:[
            {
              validationFunction:this.isntEmptyString,
              errorCode:M_err.EPAGEINVALID
            }
          ]
        },
        specialization:{
          layers:[
            {
              validationFunction:this.isntEmptyString,
              errorCode:M_err.ESPECIALIZATIONEMPTY
            }
          ]
        },
        text:{
          layers:[
            {
              validationFunction:this.isntEmptyString,
              errorCode:M_err.EMESSAGETOADDISEMPTY
            }
          ]
        },
        idMessage:{
          layers:[
            {
              validationFunction:this.isntEmptyString,
              errorCode:M_err.EMESSAGEIDISEMPTY
            }
          ]
        }
      };

      for (var param in parameters) {
        if (parameters.hasOwnProperty(param) && typeof config[param].layers!=='undefined') {
          var paramIsValid = true;
          for(var i = 0; i < config[param].layers.length; i++){
            var layer = config[param].layers[i];
            if(typeof layer.validationFunction === "function" && !layer.validationFunction(parameters[param])){
              if(typeof layer.errorCode !== "undefined"){
                result.errors.push(layer.errorCode);
              }
              paramIsValid = false;
              break;
            }
          }
          if(paramIsValid){
            if(typeof config[param].customValue === "function"){
              result.data[param] = config[param].customValue(parameters[param]);
            }else{
              result.data[param] = parameters[param];
            }
          }
        }
      }

      return result;
    },

    /**
     * isValidMail - E-mail validator. Currently just checks whether the mail isn't empty\undefined.
     *
     * @param  {string} mail E-mail adress to validate.
     * @return {boolean} True if 'mail' param isn't null\undefined\empty string. False otherwise.
     */
    isValidMail: function(mail){
      if(!M_validators.isntEmptyString(mail)){
        return false;
      }else{
        var pattern = /^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/i;
        if(mail.match(pattern)){
          return true;
        }else{
          return false;
        }
      }
    },

    /**
     * isValidPassword - Password validator.
     * @todo Validate password properly
     *
     * @param  {string} password Password to validate.
     * @return {boolean} True if 'password' param isn't null\undefined\empty string. False otherwise.
     */
    isValidPassword: function(password){
      return M_validators.isntEmptyString(password);
    },

    /**
     * isntEmptyString - Common param validator.
     * @todo Write proper validators when the algorithms are stated.
     *
     * @param  {string} string String to validate.
     * @return {boolean} True if 'string' param isn't null\undefined\empty string. False otherwise.
     */
    isntEmptyString: function(string){
      if(!string || typeof string !== "string"){
        return false;
      }
      return true;
    },


    /**
     * isValidGender - Gender validator.
     *
     * @param  {boolean} gender Gender to validate.
     * @return {boolean} True if gender parameter is boolean. False otherwise.
     */
    isValidGender: function(gender){
      return(typeof(gender) == 'boolean');
    },

    /**
     * isValidSum - Currency sum validator. Checks that sum is a number and that it is greater than zero.
     *
     * @param  {*} sum - Sum to validate, a number or a string, containing a number.
     * @return {boolean} True if sum is greater than zero. False otherwise.
     */
    isValidSum: function(sum){
      pSum = parseInt(sum);
      if(!isNaN(pSum)){
        return (pSum > 0);
      }else{
        return false;
      }
      
    },

    /**
     * isValidCouponCode - Coupon code front-end validator. Checks that couponCode is a string that is longer than four characters.
     *
     * @param  {number} sum Sum to validate.
     * @return {boolean} True if sum is a number greater than zero. False otherwise.
     */
    isValidCouponCode: function(couponCode){
      if(!M_validators.isntEmptyString(couponCode)){
        return false;
      }
      return (couponCode.length>4);
    },

    /**
     * isNumber - Temporary validator for location\country IDs.
     *
     * @param  {number} number Number to validate
     * @return {boolean} True if "number" param is a number.
     */
    isNumber: function(number){
      return(typeof(number) == 'number');
    },

    /**
     * checkByKickbox - Async e-mail validator. Makes a call to kickbox API.
     *
     * @param  {string} mail E-mail adress to validate.
     * @return {boolean} False if "result" string of response equals to "invalid". True otherwise.
     */
    checkByKickbox: function(mail){
      //var kickboxApiKey = "56fedc4f9e032abf7d1db7033ed1c85eb8188546bb00577460692af10debe567";
      // https://api.kickbox.io/v1/verify?email=bill.lumbergh@gamil.com&apikey=6c87cd93c90583441bf35a0c233af90ce35efadf0a56affd6e42626cbcc7b49
    }
  };

  /** Helpers namespace will be here
   * @namespace
   * @alias MedlanesSDK.helpers
   */
  var M_helpers = M.helpers = {

    /**
     * boolToGender - Converts boolean value to gender string.
     *
     * @param  {boolean} isMale Boolean value to convert
     * @return {string} Returns "m" if argument is true, false otherwise.
     */
    boolToGender: function(isMale){
      if(isMale===true){
        return "m";
      }
      if(isMale===false){
        return "f";
      }
      return "";
    },

    /**
     * isFunction - Checks whether the passed argument is a function
     *
     * @param  {any} arg Argument to check
     * @return {boolean} True if arg is a function, False otherwise.
     */
    isFunction: function(arg){
      return typeof arg === "function";
    },

    /**
     * isDefined - Checks whether the passed argument is a function
     *
     * @param  {any} arg Argument to check
     * @return {boolean} True if arg isn't undefined, False otherwise.
     */
    isDefined: function(arg){
      return typeof arg !== "undefined";
    },

    /**
     * invokeCallback - Wrapper for a callback. If callback is a function - calls it whth the rest of arguments, otherwise doesn't do anything.
     *
     * @param  {function} callback Callback to invoke
     * @param  {any} anyArgs Any amount of arguments to pass to the callback.
     * @return Doesn't return anything
     */
    invokeCallback: function(callback){
      if(M_helpers.isFunction(callback)){
        callback.apply(null, Array.prototype.slice.call(arguments, 1));
      }
    },

    /**
     * tryParseJson - Wrapper for JSON.parse, that catches exceptions and invalid strings that raise no exceptions, but are not a valid JSON.
     *
     * @param  {string} jsonString - JSON to parse.
     * @return {*} retVal - Parsed JSON object if jsonString is valid JSON that raises no exceptionsm, false otherwise.
     */
    tryParseJson: function(jsonString){
      try {
        var o = JSON.parse(jsonString);
        if (o && typeof o === "object" && o !== null) {
            return o;
        }
      }
      catch (e) {
        //TODO: for debug purposes, to be removed later.
        console.log(e);
      }
      return false;
    }
  };

  /** AJAX namespace will be here
   * @namespace
   * @alias MedlanesSDK.ajax
   */
  var M_ajax = M.ajax = {
    /** Url for API calls
     * @todo Make private and add setter for config purposes
     */
    apiUrl : "https://api.medlanes.com/index.php?/api_1/call/json",

    /**
     * Makes the AJAX call to the api.
     *
     * @param  {object} params An object with the request parameters.
     * @return {object} JQuery AJAX promise.
     */
    call : function(params){
      //For debug purposes
      invokeEvent("ajaxCall", JSON.stringify(params));
      return $.ajax({
        type: "POST",
        url:  this.apiUrl,
        data: {request:JSON.stringify(params)}
      });
    },


    /**
     * wrapCall - AJAX call wrapper. Fires all needed events and calls the callback if provided.
     *
     * @param  {function} callback - Callback. Set to null if not needed.
     * @param  {Object} dataToSend - Request data to send via AJAX as JSON string.
     * @param  {string} eventName - Name of an action to associate with events(e.g. <eventName>Success/<eventName>Failure).
     * @return Doesn't return anything.
     */
    wrapCall : function(callback, dataToSend, eventName){
      if(!M_validators.isntEmptyString(eventName)){
        return;
      }
      M_ajax.call(dataToSend).then(function(data) {
        // for debug purposes
        invokeEvent("ajaxResponse", eventName, data);
        var dp = M_helpers.tryParseJson(data);
        if(!dp){
          var retValue = JSON.stringify({success:false,errors:[M_err.EJSONINVALID]});
          invokeEvent(eventName+"Failure", retValue);
          M_helpers.invokeCallback(callback, retValue);
        }
        if(dp.success===true){
          if(M_helpers.isDefined(dp.result) && typeof dp.result.token === "string"){
            M.setToken(dp.result.token);
          }
          if(M_helpers.isDefined(dp.token) && typeof dp.token === "string"){
            M.setToken(dp.token);
          }
          invokeEvent(eventName+"Success", data);
        }else{
          invokeEvent(eventName+"Failure", data);
        }
        M_helpers.invokeCallback(callback, data);
      }).fail(function(jqXHR, textStatus) {
        var retValue = JSON.stringify({success:false,errors:[M_err.EAJAXERROR]});
        invokeEvent(eventName+"Failure", retValue);
        M_helpers.invokeCallback(callback, retValue);
      });
    }
  };

  /** Errors namespace will be here
   * @namespace
   * @alias MedlanesSDK.errors
   */
  var M_err = M.errors = {
    //DONE: map errors for documentation.
    /**
     * AJAX call failed.
     * @default 0
     */
    EAJAXERROR: 0,

    /**
     * Mail is empty or invalid.
     * @default 1
     */
    EMAILINVALID: 1,

    /**
     * Password is empty.
     * @default 2
     */
    EPASSWORDEMPTY: 2,

    /**
     * CountryID is invalid.
     * @default 3
     */
    ECOUNTRYIDINVALID: 3,

    /**
     * LanguageID is empty or invalid.
     * @default 4
     */
    ELANGUAGEIDINVALID: 4,

    /**
     * Email template is empty or invalid.
     * @default 5
     */
    EMAILTEMPLATEINVALID: 5,

    /**
     * Funnel is empty or invalid.
     * @default 6
     */
    EFUNNELINVALID: 6,

    /**
     * Company is empty or invalid.
     * @default 7
     */
    ECOMPANYINVALID: 7,

    /**
     * Real city is empty or invalid.
     * @default 8
     */
    EREALCITYINVALID: 8,

    /**
     * Real country is empty or invalid.
     * @default 9
     */
    EREALCOUNTRYINVALID: 9,

    /**
     * Real language is empty or invalid.
     * @default 10
     */
    EREALLANGUAGEINVALID: 10,

    /**
     * Currency is empty or invalid.
     * @default 11
     */
    ECURRENCYINVALID: 11,

    /**
     * Ticket ID is empty or invalid.
     * @default 12
     */
    ETICKETIDINVALID: 12,

    /**
     * Amount of currency is invalid or less\equal to zero.
     * @default 13
     */
    EAMOUNTINVALID: 13,

    /**
     * Coupon code is empty or invalid.
     * @default 14
     */
    ECOUPONCODEINVALID: 14,

    /**
     * Question message is empty.
     * @default 15
     */
    EQUESTIONMESSAGEEMPTY: 15,

    /**
     * One or both parameters objects for init function are undefined.
     * @default 16
     */
    EINITPARAMETERSMISSING: 16,

    /**
     * Device is empty or invalid.
     * @default 17
     */
    EDEVICEINVALID: 17,

    /**
     * Page is empty or invalid.
     * @default 18
     */
    EPAGEINVALID:18,

    /**
     * Specialization string is empty.
     * @default 19
     */
    ESPECIALIZATIONEMPTY: 19,

    /**
     * Message to add is empty or undefined.
     * @default 20
     */
    EMESSAGETOADDISEMPTY: 20,

    /**
     * User's auth token is not defined
     * @default 21
     */
    ETOKENISNOTDEFINED: 21,

    /**
     * idMessage is empty or undefined.
     * @default 22
     */
    EMESSAGEIDISEMPTY: 22,

    /**
     * JSON string is invalid.
     * @default 23
     */
    EJSONINVALID: 23

  };

  var M_geoip = M.geoip = false;
  if(typeof geoip2 !== "undefined"){
    M.geoip = geoip2;
  }

  return M;
}());
