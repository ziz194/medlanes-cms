	
  function isNumberKey(evt)
  {
     var charCode = (evt.which) ? evt.which : event.keyCode
     if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

     return true;
  }

	$(document).ready(function(){
			$("#coupon").keyup(function(){
		    	var coupon = $(this).val();		    	
    			var request = {"action":"validate_coupon","parameters":{"coupon_code":coupon}};
    			var data = {"request":JSON.stringify(request)};
			    $.ajax({
			          type: "POST",
			          url:  "https://api.medlanes.com/index.php?/api_1/call/json",      
			          data: data,
			          dataType: 'json',
			          async: false,
			          success: function(data) {
			          	if(data.success == true){
			          		$('#coupon').css('color','green');
			          		var coupon = $('#coupon').val();
			          		localStorage.setItem('coupon', coupon);

			          	}else{
			          		$('#coupon').css('color','red');
			          		localStorage.removeItem('coupon', coupon);
			          	}			          	
			          },
			          failure: function(data) {
			             console.log(' failed to connect');
			          },
			           error: function(data) {
			      }
			    });
			});
	});
function getdetails(){
	$('.forward').attr('onclick','z');
	$('.continue').attr('onclick','z');
	$('.skip').attr('onclick','z');
	var name = $('#name').val();
	var age = $('#age').val();
	var gender = $('#gender').text();
	var email = localStorage.getItem("email");
	var msg = localStorage.getItem("msg");
	var medication = $('#medication').val();
	var illness = $('#illness').val();
	var sincewhen = $('#sincewhen').val();

	if(sincewhen == 'Select'){
		sincewhen == '';
	}
switch(gender) {
    case "Select":
        gender == '';
        break;
    case "Male":
        gender = "m";
        break;
    case "Female":
        gender = "f";
        break;
    case "Auswählen":
        gender = "";
        break;
    case "Männlich":
        gender = "m";
        break;
    case "Weiblich":
        gender = "f";
        break;
    default:
       
        gender == '';
}



	var pretoken = localStorage.getItem("token");
	if(pretoken === null){
		  var dataload = {"action":"user_dummy"};
		  var stringifydataload = {"request":JSON.stringify(dataload)};

		  $.ajax({
		    type: "POST",
		    url:  "https://api.medlanes.com/index.php?/api_1/call/json",
		    data: stringifydataload,
		    success: function(stringifydataload) {
		    var object = jQuery.parseJSON( stringifydataload );
		    var gettoken = object.result.token;           
		    localStorage.setItem('token', gettoken);
		   	getlang = 0;
		    var usersignup = {"action":"user_signup","auth":{"token":gettoken},"parameters":{"age":age,"gender":gender,"funnel":"m10","email_template":"signup_simple","company":"medlanes","firstname":name,"email":email,"idCountry":1,"idLanguage":1}};  
		    var requestusersignup = {"request":JSON.stringify(usersignup)};
		     $.ajax({
		           type: "POST",
		           url:  "https://api.medlanes.com/index.php?/api_1/call/json",
		           data: requestusersignup,
		               success: function(requestusersignup) {
		                 var success =JSON.parse(requestusersignup);
		                 var checkfalse = success.success;
		                     if(checkfalse == false){
		                       var emailsign2 = $('#emailsign2').val();
		                       $('#email').val(emailsign2);
		                     }else{
								var coupon = $('#coupon').val();
						      var params = 
						      {"funnel":"m10",
						       "company":"medlanes",
						        "text":msg,
						        "state":"stored",
						        "message_properties":
						      		[{"name":"MarketingPlatform","value":"web.medlanes.com","type":9, "datatype":"text"},
						      		 {"name":"firstoccurance","value":sincewhen,"datatype":"text","type":8},
					         		],
					         		 "userproperties":[{"type":4,"name":medication},{"type":6,"name":illness}]
					     		};
						
							    if(coupon != null && coupon != ""){
								        params.coupon = coupon;
							    } 
							    var request = {"action":"question_add","auth":{"token":gettoken},"parameters":params}
							    var data = {"request":JSON.stringify(request)};
							    $.ajax({
							          type: "POST",
							          url:  "https://api.medlanes.com/index.php?/api_1/call/json",      
							          data: data,
							          dataType: 'json',
							          async: false,
							          success: function(data) {
							            console.log(data);
							            var getmsgidfromaddmsg = data;
							            var specmsgid = getmsgidfromaddmsg.result.firstMessage.idTicket;
							            console.log(getmsgidfromaddmsg.result);
							            var idMessage = getmsgidfromaddmsg.result.firstMessage.idMessage;
							            localStorage.setItem('specmsgid', specmsgid);
							            var coupon = localStorage.getItem('coupon');
							            additionalData();
							            if(coupon === null){
							            	location.replace("payment.php");
							            }else{
							            	location.replace("thankyou.php");
							            }
							            $('.forward').attr('onclick','getdetails()');
							            
							          },
							          failure: function(data) {
							             console.log(' failed to connect');
							          },
							           error: function(data) {
							      }
							    });
		                     }         
		                   },

		               failure: function(requestusersignup) {
		                  console.log(' failed to connect');
		               }
		       });
		      }
		    });
	}else{
		var gettoken = localStorage.getItem('token');
		var coupon = $('#coupon').val();
		var params = {"funnel":"m10","company":"medlanes","text":msg,"state":"stored","message_properties":[{"name":"MarketingPlatform","value":"web.medlanes.com.JA","type":9, "datatype":"text"}]}
	    if(coupon != null && coupon != ""){
		        params.coupon = coupon;
	    } 
	    var request = {"action":"question_add","auth":{"token":gettoken},"parameters":params}
	    var data = {"request":JSON.stringify(request)};
	    console.log(JSON.stringify(request))
	    $.ajax({
	          type: "POST",
	          url:  "https://api.medlanes.com/index.php?/api_1/call/json",      
	          data: data,
	          dataType: 'json',
	          async: false,
	          success: function(data) {
	            console.log(data);
	            var getmsgidfromaddmsg = data;
	            var specmsgid = getmsgidfromaddmsg.result.firstMessage.idTicket;
	            console.log(getmsgidfromaddmsg.result);
	            var idMessage = getmsgidfromaddmsg.result.firstMessage.idMessage;
	            localStorage.setItem('specmsgid', specmsgid);
	            var coupon = localStorage.getItem('coupon');
	            additionalData();
	            if(coupon === null){
	            	location.replace("payment.php");
	            }else{
	            	location.replace("thankyou.php");
	            }
	            $('.forward').attr('onclick','getdetails()');
	          },
	          failure: function(data) {
	             console.log(' failed to connect');
	          },
	           error: function(data) {
	      }
	    });
	}






}
  function isNumberKey(evt)
  {
     var charCode = (evt.which) ? evt.which : event.keyCode
     if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

     return true;
  }

	
			$(function() {
				var i;
				$("select").selectmenu();			

				$('.inputFile').fileupload({
					url: 'include/upload.php',
					sequentialUploads: true,
					recalculateProgress: false,				
					add: function (e, data) {
						if(($('.file-preview-image').length + data.originalFiles.length) > 10){							
							$('.file-error').text('The upload is limited to 10 Images!');
							$('.file-preview, .file-error').fadeIn();
							return false;
						}
						
						if (!(/(\.|\/)(gif|jpe?g|png)$/i).test(data.files[0].name)) {											
							$('.file-error').text('Invalid file type "'+data.files[0].name+'"');
							$('.file-preview, .file-error').fadeIn();
							return false;
						}					
						
						i=1;	
						$('.progress').fadeIn();
						$('.file-error').fadeOut();						
						$('.progressFile').text('Uploading Image 1/'+data.originalFiles.length).fadeIn();
						data.submit();									
					},
					submit: function (e, data) {
						data.formData = {num: data.originalFiles.indexOf(data.files[0])+1};
					},
					done: function (e, data) {
						$('.file-preview').fadeIn();						
						$('.file-preview-thumbnails').append('<div class="file-preview-frame"><img class="file-preview-image" src="uploads/'+data.files[0].name+'" alt="'+data.files[0].name+'"></div>');
						$('.progressFile').text('Uploading Image '+(i++)+'/'+data.originalFiles.length);
					},					
					stop: function (e, data) {						
						$('.progressFile, .progress').fadeOut();					
					},
					progressall: function (e, data) {						
						var progress = parseInt(data.loaded / data.total * 100, 10);
						$('.progress .progress-bar').css('width',progress + '%');
						$('.progress .progress-bar').text(progress + '%');
					}
				});
				
				$('.file-preview .close').click(function() {
					$.ajax({
						type: "POST",
						url: "include/remove.php",
						data: 'length='+$('.file-preview-image').length,
						success: function(){						
							$('.file-preview').fadeOut();
							$('.file-preview-thumbnails').html('');
						}						
					});
				});			
				
				$('.uploadFile').click(function() {
					$('.inputFile').click();
					return false;
				});	
			});


