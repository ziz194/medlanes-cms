Stripe.setPublishableKey('pk_live_cfqu65XhUJBaXhpRXNcKK2SQ');


function stripeResponseHandler(status, response) {

    if (response.error) {


        $('.error').remove();
        console.log(response.error.message);
        $('.helper').after('<p style="float:left;width:100%;" class="error">' + response.error.message + '</p>');
    } else {
        localStorage.setItem("stripe", response.id);
        var email = localStorage.getItem("email");

        var amount = localStorage.getItem("amount");

        var token = localStorage.getItem("token");
        var ticket = localStorage.getItem("specmsgid");
        var stripe = response.id;
        $('#stripe').val(stripe);
        $('#email').val(email);
        $('#amount').val(amount);
        $('#token').val(token);
        $('#ticket').val(ticket);
        proceed();
    }
}

function proceed() {
    var form = $("#wrform");
    form.submit();

}

function paymentvalidation() {
    // var price = localStorage.setItem($('.amount').text());

    var prices = $('.amount').text();
    localStorage.setItem('amount', prices);
    $('#removeclick').attr('onclick', 'z');
    var validateCardNumber = $.payment.validateCardNumber($('#cn').val());
    var validateCardCVC = $.payment.validateCardCVC($('#sc').val());
    var amount = localStorage.getItem(".amount");
    $('.error').remove();
    if (!validateCardNumber) {
        $('#cn').after('<p class="error">Bitte geben Sie eine gültige Kreditkartennummer ein.</p>');
        $('#removeclick').attr('onclick', 'paymentvalidation()').unbind('click');
    }
    var str = $('#month').text()
    var red = str.slice(0, 3);
    red = parseInt(red);
    if (red == "Monat" || $('#year').text() == "Jahr") {
        $('.year').after('<p class="error">Bitte geben Sie ein Ablaufdatum an.</p>');
        $('#removeclick').attr('onclick', 'paymentvalidation()').unbind('click');
    }
    if (!validateCardCVC) {
        $('#sc').after('<p class="error">Bitte geben Sie einen gültigen Sicherheitscode ein.</p>');
        $('#removeclick').attr('onclick', 'paymentvalidation()').unbind('click');
    }
    if ($('.error').length > 0) {
        $('#removeclick').attr('onclick', 'paymentvalidation()').unbind('click');
        return false;
    }

    Stripe.createToken({
        number: $('#cn').val(),
        cvc: $('#sc').val(),
        exp_month: red,
        exp_year: $('#year').text()
    }, amount, stripeResponseHandler);
}



function DropDown(el) {
    this.dd = el;
    this.placeholder = this.dd.children('span');
    this.opts = this.dd.find('ul.dropdown > li');
    this.val = '';
    this.index = -1;
    this.initEvents();
}
DropDown.prototype = {
    initEvents: function() {
        var obj = this;

        obj.dd.on('click', function(event) {
            $(this).toggleClass('active');
            return false;
        });

        obj.opts.on('click', function() {
            var opt = $(this);
            obj.val = opt.text();
            obj.index = opt.index();
            obj.placeholder.text(obj.val);
        });
    },
    getValue: function() {
        return this.val;
    },
    getIndex: function() {
        return this.index;
    }
}

function doacheck() {



}


$(document).ready(function() {
    $(".block,.text p").click(function() {

        if ($('.payment-checked').hasClass('checked')) {
            $('.payment-checked').css('opacity', '0');
            $('.payment-checked').removeClass('checked');
            $('#tac').val('notchecked');
        } else {
            $('.payment-checked').css('opacity', '1');
            $('.payment-checked').addClass('checked');
            $('#tac').val('checked');
        }

    });




    $("#leftslide").noUiSlider({
        behaviour: 'tap-drag',
        direction: 'rtl',
        decimals: 0,
        start: [2],
        snap: true,
        orientation: "vertical",
        range: {
            'min': [1],
            '50%': [2],
            'max': [3]
        }
    });

    $("#rightslide").noUiSlider({
        behaviour: 'tap-drag',
        direction: 'rtl',
        decimals: 0,
        start: [2],
        snap: true,
        orientation: "vertical",
        range: {
            'min': [1],
            '50%': [2],
            'max': [3]
        }
    });

    $('#rightslide').on('slide', function() {
        check();
    });
    $('#leftslide').on('slide', function() {
        check();
    });


});
var level = ['High', 'Medium', 'Low'];

function check() {
    var x = $('#leftslide').val();
    var y = $('#rightslide').val();

    var x = parseInt(x);
    var y = parseInt(y);

    change(Math.floor(x + y));
}



$(function() {


    $("#range-one").slider({
        orientation: "vertical",
        range: "min",
        value: 2,
        min: 1,
        max: 3,
        step: 1,
        slide: function(event, ui) {
            $('#amounta').text(ui.value);
            $(".urgency").val(level[ui.value - 1]);
            check();
        }
    });

    $("#range-two").slider({
        orientation: "vertical",
        range: "min",
        value: 2,
        min: 1,
        max: 3,
        step: 1,
        slide: function(event, ui) {
            $('#amountb').text(ui.value);
            $(".detail").val(level[ui.value - 1]);
            check();
        }
    });

    $('.paypalForm').click(function() {
        var custom = $('.custom').val();
        custom += '&ti=' + localStorage.getItem('specmsgid') + '&to=' + localStorage.getItem('token') + '&price=' + $('.amount').text();
        $('.custom').val(custom);
    });
});

function moveit(val, slider) {
    console.log(val);
    val = val;
    console.log(slider);
    if (slider == 'left') {
        $("#leftslide").val(val);
        check();
    } else {
        $("#rightslide").val(val);
        check();
    }

}
$(function() {

    var dd = new DropDown($('#dd'));
    var year = new DropDown($('.year'));
    var month = new DropDown($('.month'));

    $(document).click(function() {
        // all dropdowns
        $('.wrapper-dropdown-3, .sincewhen, .month, .year').removeClass('active');
    });

});
$(document).ready(function() {

    $('.paymentoption .dropdown a').click(function() {


        var option = $(this).text()

        if (option == 'PayPal') {
            $('#continuePaypal').show();
            $('.continue').hide();
            $('.hideit').hide();
            $('.showit').show();
            $('#bankbtn').hide();
        }
        if (option == 'Kreditkarte') {
            $('#continuePaypal').hide();
            $('.continue').show();
            $('.hideit').show();
            $('.showit').hide();
            $('#bankbtn').hide();
        }
        if (option == "SOFORT") {
            $('#continuePaypal').hide();
            $('.continue').hide();
            $('.hideit').hide();
            $('.showit').hide();
            $('#bankbtn').show();
        }
    });






    $('#bankbtn').click(function() {

        var prices = $('.amount').text();
        localStorage.setItem('amount', prices);

        var emailPln = localStorage.getItem("email");
        var amountPln = localStorage.getItem("amount");
        var tokenPln = localStorage.getItem("token");
        var ticketPln = localStorage.getItem("specmsgid");
        var currencyPln = 'EUR';
        var geo_ipPln = localStorage.getItem("geo_ip");
        console.log(amountPln);
        $.ajax({
            type: "POST",
            url: "php/paylane-pp.php",
            dataType: "html",
            data: {
                email: emailPln,
                amount: amountPln,
                token: tokenPln,
                ticket: ticketPln,
                currency: currencyPln,
                geo_ip: geo_ipPln,
            },
            success: function(data) {
                console.log(data);
                data = JSON.parse(data);
                if (data.success == true) {
                    var url = data.redirect_url;

                    // Internet Explorer 8 and lower fix
                    if (navigator.userAgent.match(/MSIE\s(?!9.0)/)) {
                        var referLink = document.createElement("a");
                        referLink.href = url;
                        document.body.appendChild(referLink);
                        referLink.click();
                    } else {
                        // All other browsers
                        window.location.replace(url);
                    }
                }
            },
            error: function(xhr, error) {
                var jsonResponse = JSON.parse(xhr.responseText);

                console.log('Error : ' + jsonResponse.message);

            }

        });


    });



});