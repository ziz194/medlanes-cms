$(document).ready(function(){

	$("#careerslide").responsiveSlides({    
	speed: 1500
	});

	var stickyNavTop = $('.sub-menu').offset().top -100;  
	var stickyNav = function(){  
		var scrollTop = $(window).scrollTop();
		if (scrollTop > stickyNavTop) {   
		    $('.sub-menu').addClass('follow-me-sm'); 
		} else {  
		    $('.sub-menu').removeClass('follow-me-sm');  
		}  
	};  
	stickyNav(); 
	$(window).scroll(function() {
	stickyNav();
	});
	$('#vision').click(function(e){
		$('html, body').animate({
		scrollTop: $("#visiongo").offset().top -185
		}, 500);          
	});  
	$('#meet').click(function(e){
		$('html, body').animate({
		scrollTop: $("#meetgo").offset().top -140
		}, 500);          
	});  
	$('#open').click(function(e){
		$('html, body').animate({
		scrollTop: $("#opengo").offset().top -140
		}, 500);          
	});  
	$('#pics').click(function(e){
		$('html, body').animate({
		scrollTop: $("#picsgo").offset().top -160
		}, 500);          
	});  
});