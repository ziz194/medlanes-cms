(function( $ ){

	$.fn.ajaxPacker = function( options ) {  

		var settings = $.extend( {/*'show_images' : true, 'images_container': '.image_list'*/}, options);
		var pe = this;
		var form_data = new FormData();

		pe.find(':input').each(function(){

			switch(this.nodeName.toLowerCase()){

				case 'input':
					if($(this).attr('type') == 'file'){

						//console.log($(this));
						//console.log(this.files);
						len = this.files.length;

						for(var i = 0 ; i < len; i++ ){

							file = this.files[i];
							form_data.append("files[]", file);
						}

					}else if($(this).attr('type') == 'checkbox'){

						var cb = $(this).is(':checked') ? 1 :0;
						form_data.append($(this).attr('name'), cb);

					}else if($(this).attr('type') == 'radio'){

						if($(this).is(':checked')){
							form_data.append($(this).attr('name'),  $(this).val());
						}						

					}else{

						form_data.append($(this).attr('name'), $(this).val());

					}
				break;

				case 'select':
					form_data.append($(this).attr('name'), $(this).children('option:selected').val());
				break;

				case 'textarea':
					form_data.append($(this).attr('name'), $(this).val());
				break;
			}
		});

		return form_data;

	};
})( jQuery );
