<footer>
<div class="fullend">
	<div class="fwrap">
		<ul>
			<li><a href="index.php?lang=de">Home</a></li>
			<li><a href="service?lang=de">Unser Service</a></li>
			<li><a href="doctors?lang=de">Ärzte</a></li>
			<li><a href="faq?lang=de">FAQ</a></li>
			<li><a href="contact?lang=de">Kontakt</a></li>
		</ul>
		<ul>
			<li><a href="about?lang=de">Über Medmedo</a></li>
			<li><a href="career?lang=de">Karriere</a></li>
		</ul>
		<ul>
			<li><a href="terms?lang=de">AGBs</a></li>
			<li><a href="privacy?lang=de">Datenschutz</a></li>
			<li><a href="imprint?lang=de">Impressum</a></li>
		</ul>
		<div class="one">
			<p>Unterstützer <br> und Partner:</p>			<img style="margin-top:10px;" src="images/footer/footerimages.png" alt="partners"/>
		</div>
	</div>
</div>
	<div class="fwrap">
		<div class="bot">
			<div class="payline">
				<p>Payments securely <br> processed by:</p>
				<img src="images/payline.png" alt="#">
			</div>
		</div>
		<div class="bot">
			<p><p class="copyrights">Copyright © Medlanes GmbH 2014 Alle Rechte vorbehalten</p></p>
		</div>		
	</div>
</footer>
<style>
	.payline{
		margin: 0px auto;
		width: 262px;
		margin-bottom: 20px;
	}
	.payline p{
		font-family: 'Roboto';
		font-weight: 300;
		font-size: 14px;
		float: left;
		color: #ababab;
		width: auto !important;
		text-align: right !important;
		margin-top: 9px;
		margin-right: 20px;
		line-height: 18px;
	}
</style>
 <script type="text/javascript">

 var mobile = $( window ).width();

 if(mobile > "480"){


	<?php 
	if (isset($_GET["lang"]) && !empty($_GET["lang"])) {
		$getparam = $_GET['lang'];
			switch ($getparam) {
			    case "en":
			        echo "	
(function(d, src, c) { var t=d.scripts[d.scripts.length - 1],s=d.createElement('script');s.id='la_x2s6df8d';s.async=true;s.src=src;s.onload=s.onreadystatechange=function(){var rs=this.readyState;if(rs&&(rs!='complete')&&(rs!='loaded')){return;}c(this);};t.parentElement.insertBefore(s,t.nextSibling);})(document,
'//medlanes.ladesk.com/scripts/track.js',
function(e){ LiveAgent.createButton('98f3b020', e); });
					";
			        break;
			    case "de":
			        echo "	
(function(d, src, c) { var t=d.scripts[d.scripts.length - 1],s=d.createElement('script');s.id='la_x2s6df8d';s.async=true;s.src=src;s.onload=s.onreadystatechange=function(){var rs=this.readyState;if(rs&&(rs!='complete')&&(rs!='loaded')){return;}c(this);};t.parentElement.insertBefore(s,t.nextSibling);})(document,
'//medlanes.ladesk.com/scripts/track.js',
function(e){ LiveAgent.createButton('457ea174', e); });			        	
					";
			        break;
			}
		 
	}else{  
    		        echo "
(function(d, src, c) { var t=d.scripts[d.scripts.length - 1],s=d.createElement('script');s.id='la_x2s6df8d';s.async=true;s.src=src;s.onload=s.onreadystatechange=function(){var rs=this.readyState;if(rs&&(rs!='complete')&&(rs!='loaded')){return;}c(this);};t.parentElement.insertBefore(s,t.nextSibling);})(document,
'//medlanes.ladesk.com/scripts/track.js',
function(e){ LiveAgent.createButton('457ea174', e); });		   	
					";
	}
	?>
}else{
	 $(document).ready(function(){

 	$('#swaponmobb').text('Qualifizierte Informationen & Antworten von führenden Fachärzten');
 });
}



// </script>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-T3S4HG"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-T3S4HG');</script>
<!-- End Google Tag Manager -->