<?php
/* 
------------------
Language: English
------------------
*/

$page = basename($_SERVER['PHP_SELF']); 
$class1="";
$class2="";
$class3="";
$class4="";


if($page == 'about-us.php'){
		$class1="selected";
}else if ($page == 'our-doctors.php'){
		$class2="selected";
}else if ($page == 'faq.php'){
		$class3="selected";
}else if ($page == 'contact.php'){
		$class4="selected";
}



$lang = array();



$lang['LOGO_HOME'] = "http://medlanes.com?lang=en";

$lang['PAGE_TITLE'] = 'Medlanes - qualified medical consultations - mobile & online';
$lang['HEADER_TITLE'] = 'My website header title';
$lang['SITE_NAME'] = 'My Website';
$lang['SLOGAN'] = 'My slogan here';
$lang['HEADING'] = 'Heading';

// Menu----------------------
$lang['MENU_TEST'] = '<a href="welcome.php?lang=en">Start now</a>';
$lang['MENU_HOME'] = '<a href="index.php?lang=en">Start now</a>';
$lang['MENU_ABOUT_US'] = "<a href=about-us.php?lang=en class=".$class1." \>About us</a>";
$lang['MENU_OUR_DOCTORS'] = "<a href=our-doctors.php?lang=en class=".$class2.">Our Doctors</a>";
$lang['MENU_FAQ'] = "<a href=faq.php?lang=en class=".$class3.">FAQ</a>";
$lang['MENU_CONTACT'] = "<a href=contact.php?lang=en class=".$class4.">Contact</a>";
$lang['MENU_TERMS'] = '<a href="terms.php?lang=en">Terms & Conditions</a>';
$lang['MENU_PRESS'] = '<a href="press.php?lang=en">PR Contact</a>';
$lang['MENU_MEDIA'] = '<a href="media.php?lang=en">In the Media</a>';
$lang['MENU_JOBS'] = '<a href="jobs.php?lang=en">Jobs</a>';
$lang['MENU_HIRING'] = '<a href="jobs.php?lang=en">Join Medlanes</a>';
$lang['MENU_DOCTORPORTAL'] = '<a href="doctorportal.php?lang=en">For Doctors</a>';

// Footer-------------------
$lang['MENU_FOOTER_START_ONLINE'] = '<a href="#">- COMING SOON - <br />Start online </a>';
$lang['MENU_FOOTER_ANDROID'] = '<a href="#">- COMING SOON - <br />Download Android </a>';
$lang['MENU_FOOTER_IOS'] = '<a href="#">- COMING SOON - <br />Download iOS </a>';

$lang['MENU_FOOTER_ABOUT'] = '<a href="about-us.php?lang=en">About Medlanes </a>';
$lang['FOOTER_CLIENT_TERMS'] = '<a href="terms.php?lang=en">Client Terms of Service</a>';
$lang['FOOTER_PROVIDER_TERMS'] = '<a href="terms.php?lang=en">Provider Terms of Service</a>';
$lang['FOOTER_PRIVACY_STATEMENT'] = '<a href="terms.php?lang=en">Privacy Statement</a>';
$lang['FOOTER_DATA_SECURITY'] = '<a href="terms.php?lang=en">Data Security</a>';
$lang['FOOTER_IMPRINT'] = '<a href="impressum.php?lang=en">Imprint</a>';

$lang['FOOTER_press_room'] = 'Press Room';
$lang['FOOTER_jobs'] = 'Jobs';
$lang['FOOTER_hiring'] = 'Join Medlanes';
$lang['FOOTER_copyright'] = 'Copyright © Medlanes GmbH  2014  All Rights Reserved';


// Home Page--------------------

$lang['HOME_PAGE_banner-sliderone_content_testpage']="<h1>Got a medical problem?</h1>
				<h2>Direct advice by qualified doctors
					for a £29 fee </h2>
				<a href='signup.php'> Get advice now</a>";
				
				
$lang['HOME_PAGE_banner-sliderone_content']="<h1>Got a medical problem?</h1>
				<h2>Direct advice by qualified doctors
					for a £29 fee </h2>
				<a href='#'> Coming soon</a>";
	
$lang['HOME_PAGE_banner-slidertwo_content']="<h1>Medical advice&nbsp;<br>&nbsp;always available&nbsp;<br></h1>
				<h2>24/7 wherever&nbsp;<br>&nbsp; you might be&nbsp;</h2>
				<a href='#'> Coming soon</a>";
	
	
	$lang['HOME_PAGE_banner-sliderthree_content']="<h1>Medical advice &nbsp;<br>&nbsp;always available&nbsp;<br></h1>
				<h2>24/7 wherever&nbsp;<br>&nbsp; you might be&nbsp;</h2>
				<a href='#'> Coming soon</a>";			

				

$lang['HOME_PAGE_l-one']="Fully certified";
$lang['HOME_PAGE_l-two']="256-bit SSL";


$lang['HOME_PAGE_Concept']="Concept seen on:";


$lang['HOME_PAGE_widtitle1']="Why Medlanes";
$lang['HOME_PAGE_widcont1']="Getting medical advice at night, on the weekend or on vacation is difficult. Medlanes provides mobile and online advice directly from qualified doctors whenever access to healthcare is a hassle or expensive.";

$lang['HOME_PAGE_widtitle2']="How it works";
$lang['HOME_PAGE_widcont2']="Use the Medlanes app or webpage to ask your medical questions, we will match you with the right family doctor or specialist - no matter where you are and around the clock.";

$lang['HOME_PAGE_widtitle3']="Your advantage";
$lang['HOME_PAGE_widcont3']="With Medlanes you don't need to wait for appointments or rely on low-quality information on the internet. Instead, your questions are answered by a leading expert without you having to leave the house.";

$lang['HOME_PAGE_bluebox1_text1']="Support";
$lang['HOME_PAGE_bluebox1_text2']="0800 - 765 43 43";
$lang['HOME_PAGE_bluebox1_text3']="toll free from landlines";
$lang['HOME_PAGE_bluebox1_text4']="E-Mail";

$lang['HOME_PAGE_bluebox2_text1']="Certified & tested";
$lang['HOME_PAGE_bluebox2_text2']="We're dedicated to quality and reliability";
$lang['HOME_PAGE_bluebox2_text3']="<a href='about-us.php?lang=en'>About us</a>";

$lang['HOME_PAGE_bluebox3_text1']="Full-circle medicine";
$lang['HOME_PAGE_bluebox3_text2']="Generalists & Specialists at Medlanes";
$lang['HOME_PAGE_bluebox3_text3']="<a href='our-doctors.php?lang=en'>Our doctors</a>";


// About us Page--------------------
$lang['ABOUT_US_welcome']="Welcome to Medlanes";

$lang['ABOUT_US_BANNER_title1']="Leading experts from all specialities.";
$lang['ABOUT_US_BANNER_title2']="We work with the best to provide the best.";


$lang['ABOUT_US_tab-title']="With Medlanes you...";

$lang['ABOUT_US_tab-1']="Wait less";
$lang['ABOUT_US_tab-1_content']="Don't wait for days and days to get a doctor's appointment! With Medlanes medical advice is available 24 hours a day, 7 days a week. ";


$lang['ABOUT_US_tab-2']="Pay less";
$lang['ABOUT_US_tab-2_content']="Healthcare is extremely expensive, not only in direct costs but also due to opportunity costs. Having to take time off just to wait in the waiting room at the doctor's office is quite a hassle. With Medlanes medicine is affordable for everyone, and requires almost no time on your end!";

$lang['ABOUT_US_tab-3']="Stress less";
$lang['ABOUT_US_tab-3_content']="Getting medical advice with Medlanes is extremely convenient! Don't stress finding the time to make an appointment or schedule weeks in advance to fit in your schedule. Medlanes is available anytime and from anywhere - on your smartphone and on the web.";

$lang['ABOUT_US_tab-4']="Worry less";
$lang['ABOUT_US_tab-4_content']="Don't worry about you or your family's health. Medlanes works with experienced and specialized doctors to give you peace of mind. With less worries you'll be happier and you'll always know when it's really important to see a doctor to not miss getting treatment in time.";


$lang['ABOUT_US_someFact-title']="We make healthcare more efficient";


$lang['ABOUT_US_someFact-fact1']="8 min.";
$lang['ABOUT_US_someFact-fact1-cont']="Average time span a doctor has for a visit.";

$lang['ABOUT_US_someFact-fact2']="14 days";
$lang['ABOUT_US_someFact-fact2-cont']="Average waiting time to get a doctor's appointment.";

$lang['ABOUT_US_someFact-fact3']="3 hours";
$lang['ABOUT_US_someFact-fact3-cont']="Time a doctor's visit takes on average, including getting there.";

$lang['ABOUT_US_someFact-fact4']="80%";
$lang['ABOUT_US_someFact-fact4-cont']="Percentage of visits to the doctor's office that could be done online in certain specialities.";

$lang['ABOUT_US_someFact-fact5']="50%";
$lang['ABOUT_US_someFact-fact5-cont']="Amount of treatments that could be improved if patients got continuous advice.";


$lang['ABOUT_US_benefits-title']="Why, Where, When and How";

$lang['ABOUT_US_benefits-msg']="Get medical advice WHEN and WHERE you need it! Medlanes combines the convenience of an online service with the quality and reliability of a doctor's visit. Simply download the Medlanes app or use the web version to get help with you or your family's health - anytime, anywhere! In a nutshell: post a question, if needed add an image and answer a few primary questions, Medlanes will match you with the right doctor who will answer in no time!";



$lang['ABOUT_US_benefits-benifit1_title']="During vacation";
$lang['ABOUT_US_benefits-benifit1_content']="It's difficult to find a doctor that speaks your language when you're abroad - use Medlanes for a first opinion.";

$lang['ABOUT_US_benefits-benifit2_title']="At work";
$lang['ABOUT_US_benefits-benifit2_content']="Talking time off work to see a doctor during the day can be a tough decision. Get online advice without hassle.";

$lang['ABOUT_US_benefits-benifit3_title']="At night";
$lang['ABOUT_US_benefits-benifit3_content']="For all non-emergency questions it's almost impossible to get advice after 6pm - not so with Medlanes.";

$lang['ABOUT_US_benefits-benifit4_title']="On the weekend";
$lang['ABOUT_US_benefits-benifit4_content']="Finding a doctor on the weekend is very difficult and you wait a long time - not so with Medlanes.";

$lang['ABOUT_US_benefits-benifit5_title']="For convenience";
$lang['ABOUT_US_benefits-benifit5_content']="Waiting to get an appointment, waiting again in the waiting room - there's no lost time when using Medlanes.";

$lang['ABOUT_US_benefits-benifit6_title']="Long distance";
$lang['ABOUT_US_benefits-benifit6_content']="Don't settle for the nearest doctor, get advice from the right specialist from your home!";

$lang['ABOUT_US_welcome_cont']="Getting good medical advice is very difficult and a lot of hassle. You wait weeks to get an appointment, wait again in the waiting room and then the doctor has just 5 minutes for you. And on the weekends, at night or during vacation you might not even get professional help if you urgently need it.<br />
 Medlanes changes all that! With Medlanes you get help and advice whenever you need it. With years of profound expertise in IT and medicine we provide a medical service that allows patients to reach general and specialists fast, safe and convenient from everywhere and around the clock.";

$lang['ABOUT_US_title1']="Fast and convenient….";

$lang['ABOUT_US_content1']="Get medical advice faster and more conveniet than ever before! Simply ask your question through the Medlanes app or webpage, we'll connect you with the right doctor no matter where you are or what time it is. We'll guide you the process so that the doctors get all the information they need to give meaningful advive and get back to you in no time!";

$lang['ABOUT_US_title2']="Safe and confidential….";

$lang['ABOUT_US_content2']="All of our doctors are licensed physicians with many years of professional experience in their respective fields. All doctors adhere to a strict ethical code and absolute doctor-patient confidentiality.";








// DOCTORPORTAL Page--------------------
$lang['DOCTORPORTAL_welcome']="Welcome to Medlanes";

$lang['DOCTORPORTAL_welcome_cont']="Medlanes will launch soon. Doctors interested in working with Medlanes should contact doctor@medlanes.com";

$lang['DOCTORPORTAL_title1']="Your advantages";

$lang['DOCTORPORTAL_content1']="This page is currently being constructed. More information will be added very soon.";

$lang['DOCTORPORTAL_title2']="This is how it works";

$lang['DOCTORPORTAL_content2']="This page is currently being constructed. More information will be added very soon.";







// MEDIA Page--------------------
$lang['MEDIA_welcome']="Medlanes in the media";

$lang['MEDIA_welcome_cont']="Read about Medlanes in national and international press. Excerpts from non-english publications are not translated.";

$lang['MEDIA_title1']="Frankfurter Allgemeine Zeitung // 08. Nov 2013";

$lang['MEDIA_content1']="[ Medlanes ] setzt auf die enge Verzahnung von Medizin und Technik. Auf diesem Milliardenmarkt mischen Konzerne wie IBM, SAP, Microsoft mit. Microsoft hat aus 360 Bewerbern aus ganz Europa neun Unternehmen ausgewählt, die sie für besonders aussichtsreich halten. Medlanes ist eines davon.";


$lang['MEDIA_title2']="Tagesumschau // 18. März 2014";

$lang['MEDIA_content2']="Ein Arzt für die Hosentasche? Kein Problem mit der neuen Seite, auf der man folgendes erfährt: Holen Sie sich den Rat eines Arztes – schnell,bequem und kostengünstig. Medlanes schickt Ihnen eine Konsultation eines Arztes direkt auf ihr Handy. Egal wo Sie sind oder welche Beschwerden Sie haben, Medlanes ist für Sie da.";









// Our Doctors Page--------------------


$lang['OUR_DOCTORS_BANNER_title1']="Get certainty about your health now.";
$lang['OUR_DOCTORS_BANNER_title2']="Convenient advice by expert doctors.";


$lang['OUR_DOCTORS_title1']="Our Doctors";
$lang['OUR_DOCTORS_cont1']="Medlanes allows you to see your doctor from anywhere in the world, at any time, day or night. Medlanes only works with fully licensed and qualified doctors. Your &quot;online/mobile doctor&quot; will be matched to your specific query and will be an expert in the field.";

$lang['OUR_DOCTORS_title2']="We're currently building our base";
$lang['OUR_DOCTORS_cont2']="Medlanes only works with fully licensed and qualified doctors. Your &quot;online/mobile doctor&quot; will be matched to your specific query and will be an expert in the field. Medlanes is designed to help you with  medical queries, advice and dealing with chronic diseases. ";


$lang['DOCTORS-TYPE_title1']="General physician";
$lang['DOCTORS-TYPE_content1']="Most questions are best answered by general physician, they are your guide through medicine! ";


$lang['DOCTORS-TYPE_title2']="Pediatrics";
$lang['DOCTORS-TYPE_content2']="It's important to keep up to date with your child's health! Our pediatric doctors help you with advice any time you need it.";

$lang['DOCTORS-TYPE_title3']="Cardiology, Gynecology, Dermatology, Orthopedics, ...";
$lang['DOCTORS-TYPE_content3']="Medlanes also covers multiple other specialities, each supported by a leading expert in the fields. Go ahead and ask any question you like, we will direct it to the right doctor.";

$lang['DOCTORS-SPOTLIGHT_title']="Doctor spotlight";

$lang['DOCTORS-SPOTLIGHT_Doctor-name1']="Lars Omera, M.D.";
$lang['DOCTORS-SPOTLIGHT_Doctor-type1']="Cardiology";

$lang['DOCTORS-SPOTLIGHT_Doctor-name2']="Thomas Pers, M.D., P.hD.";
$lang['DOCTORS-SPOTLIGHT_Doctor-type2']="Family medicine / Internal";

$lang['DOCTORS-SPOTLIGHT_Doctor-name3']="Ane Wertheim, M.D.";
$lang['DOCTORS-SPOTLIGHT_Doctor-type3']="Pediatrics";

$lang['DOCTORS-SPOTLIGHT_content']="From time to time we will feature a few of our doctors to get you to know the specialists that work with us to help you! More soon.";




// PRESS Page--------------------

$lang['PRESS_title1']="Contact for press requests";
$lang['PRESS_cont1']="Please direct press requests at press@medlanes.com. We are more than happy to provide press kits.";




// JOBS Page--------------------

$lang['JOBS_title1']="Open positions at Medlanes";
$lang['JOBS_cont1']="Detailed job postings will be published in May 2014. We currently have a few open positions, please feel free to send us your unsolicited application.<br />
<br />
We currently have open positions for interns as well. Please apply for positions in marketing and business development. ";

$lang['JOBS_heading']="Open positions at Medlanes";
$lang['JOBS_heading2']="We are always looking for motivated individuals who want to support our Team.";
$lang['JOBS_subheading']="Find here the open positions:";
$lang['JOBS_list1']="Marketing & Sales Intern (m/f)";
$lang['JOBS_list2']="Technical Research & Development Intern (m/f)";
$lang['JOBS_list3']="Internship in Healthcare Startup in Berlin";
$lang['JOBS_list4']="Executive Assistant Intern (m/f)";


// Terms Page--------------------

$lang['TERMS_title']="Terms &amp; Conditions";
$lang['TERMS_cont']="
			<br />
			WE EXCLUSIVELY PROVIDE GENERAL MEDICAL INFORMATION. IN NO WAY CAN THE SERVICE BE USED FOR DIAGNOSIS, TREATMENT OR ANY INDIVIDUAL DECISION.<br />
			<br />
				<h1>1. Acceptance of this service's Terms and Conditions</h1>
				<p>By using this service you are deemed to understand, consent to and agree to be legally bound by these Terms and Conditions. If you do not understand and agree to these terms and conditions you should not use this service. </p>
				<br>
				<h1>2. Terminology</h1>
				<p> a. The term 'Medlanes' is the trading name of Medlanes GmbH</li>
				<p> b. The term 'the Medlanes website' means the URL www.medlanes.com and associated pages.
				<p> c. The term 'doctor' means a doctor working at or associated with Medlanes.
				<p> d. The term 'law' shall be construed as a reference to any law (including common or customary law), statute, judgment, regulation, directive, bye law, order, ordinance or any other legislative measure of any government, supranational, local government, statutory or regulatory body or court, in each case having the force of law.
				<p> e. The term 'Terms and Conditions' comprises of paragraphs 1 to 16 and includes the Privacy Policy and the Disclaimer.
				<br>
				<h1>3. Responsibility of our doctors for their patients </h1>
				<p>With an answer our doctors take exactly the same personal responsibility for each patient as they would if they were seeing a patient face to face. The online nature of this service in no way diminishes the professional obligations that our doctors owe to a patient. Similarly, the existence of Medlanes GmbH as the legal entity behind the Medlanes service in no way seeks to diminish the professional obligations that our doctors owe to a patient.</p>
				<br>
				<h1>4. The importance of providing true, timely and complete information</h1>
				<p>You agree that all information that you provide to our doctors will be true and complete to the best of your knowledge and that you will not purposefully omit to provide information that could reasonably be judged to be potentially relevant to our doctors in providing the service. You should not register or complete questionnaires on behalf of anyone other than yourself.</p>
				<br>
				<h1>5. Scope of services provided and contract</h1>
				<p>Our doctors provide general medical information for specific medical conditions only. Our doctors do not offer medical advice or prescriptions for medical conditions not related to the specific services we provide. In no case can the Medlanes office replace a face to face meeting with a doctor. Because Medlanes is not a pharmacy we do not dispense or deliver any medicines.</p>
				<br>
				<p>5.1 Medlanes – What we do</p>
				 <p> a.We provide general medical information based on online questionnaires you complete, telephone conversations, secure messaging and photo assessments.
				 <p> a.We do not offer any type of diagnosis, treatment of advice in medical emergencies.
				<p>5.2 Our website is only intended for use by people resident in the United Kingdom (the “Serviced Countries”). We do not accept orders from individuals outside those countries.</p>
				<p>5.3 How you should use our Services: By placing an order through our website, you confirm that:</p>
				<p> a.you are legally capable of entering into binding contracts; and
				<p> b.you are at least 18 years old;
				<p> c.you are resident in one of the Serviced Countries; and
				<p> d.you are accessing our website from that country.
				<p>5.4 You must use our website and Services with care and in compliance with the below:</p>
				<p> a.The provision of Services to you is conditional on you completing all consultation questionnaires contained on our website truthfully and honestly. You must reveal and disclose all relevant information truthfully to the best of your knowledge.
				<p> b.We cannot and are not liable for any damages which results from a failure of you to follow advice given on the website or from your failure to pass on information given on the website to your regular healthcare provider. You accept that the advice given on the website does not replace your regular healthcare provider. You must tell your regular healthcare provider about any information we supply.
				<p> c.All the information contained on our website is in English. You are solely responsible for ensuring that you understand the questions in the questionnaires you complete on our website. You must speak to your regular health care provider if you do not understand a question or are unsure how you should answer certain questions or you do not fully understand the advice or information given to you on our website.
				<p>5.5 No contract is formed between you and Medlanes until you are informed in a separate eMail that especially states the formation of the contract. Before that time you can cancel your request at any time. Medlanes is allowed to reject any request without giving a reason.  For contractual purposes, you agree to electronic means of communication and you acknowledge that all contracts, notices, information and other communications that we provide to you electronically comply with any legal requirement that such communications be in writing. This condition does not affect your statutory rights. </p>
				<br>
				<h1>6. Prescriptions written by our doctors</h1>
				<p>Our doctors do not offer any prescriptions.</p>
				<br>
				<h1>7. Informing your existing doctor about any information that is provided</h1>
				<p>You agree that if our doctors provide any information for you you and you want us to inform your existing doctor, you will provide our doctors with the full and correct contact details of your existing doctor. Our doctors will always encourage you to let us inform your existing doctor.</p>
				<br>
				<h1>8. Informed consent</h1>
				<p>You agree that by requesting information you formally give your informed consent for our doctors to provide information</p>
				<br>
				<h1>9. Your Medlanes account</h1>
				<p>a. You agree that if provided you will not share your username and password with anyone. You acknowledge that you will take all reasonable steps to ensure that a third party does not gain access to your account.</p>
				<p>b. You agree that you will take all reasonable steps to ensure that any device that you use to set up or access your account is suitably protected from potential hazards by firewalls, anti-virus software and other such security applications.</p>
				<p>c. You agree that you will not create more than one Medlanes account.</p>
				<p>d. You agree that if when creating your account you provide an email address or contact telephone number, you consent for us to contact you by email and telephone if and when the need arises.</p>
				<br>
				<h1>10. Cancellations and refunds</h1>
				<p>If charged, Medlanes offers a full refund on all request that were not answered.</p>
				<br>
				<h1>11. Privacy Policy and patient confidential data</h1>
				<p>Medlanes has a legal obligation to protect all patient confidential data. We are not allowed to share patient confidential data with third parties, including other doctors, who are unconnected to our service without your explicit consent. To provide this service to patients it is necessary that both our doctors and our non-medical staff working at have access to  patient confidential data. It is also necessary that for the purpose of maintaining our IT systems to provide access to patient data to our IT contractors. If required by law, for the purpose of financial audit Medlanes may be required to divulge very limited patient information to our financial auditors and wherever possible information used for the purpose of financial audit is anonymised. All access to patient information is kept to the minimum required to allow the safe and effective delivery of our services. You have a legal right to obtain any information that we hold about you.</p>
				<br>
				<h1>12. Vulnerabilities of electronic communications and post</h1>
				<p>The online nature of the Medlanes service necessarily relies upon using electronic forms of communication, post, telephone and SMS. There are inherent vulnerabilities in using such forms of communication and we are therefore unable to give a guarantee that a third party unrelated to our service will not intercept communications.</p>
				<br>
				<h1>13. Disclaimer</h1>
				<p>a. To the fullest extent permitted by applicable law, under no circumstances will Medlanes or our doctors be liable for any of the following losses or damage (whether such losses were foreseen, foreseeable, known or otherwise): (a) loss of emotional well-being including, but not limited to, any embarrassment caused (b) loss of data; (c) loss of revenue or anticipated profits; (d) loss of business; (e) loss of opportunity; (f) loss of goodwill or injury to reputation; (g) losses suffered by third parties; or (h) any indirect, consequential, special or exemplary damages arising from the use of the Medlanes service regardless of the form of action. Nothing in this clause is intended to exclude or limit Medlanes Doctor’s liability for death or personal injury caused by its negligence or fraud.</p>
				<p>b. You agree that to the fullest extent permitted by applicable law, Medlanes makes no representations or warranties with respect to any treatment, information, advice or action relied on or followed by any person using the Medlanes  service.</p>
				<p>c. Medlanes does not warrant that the Medlanes websites or this service in general will be either available without interruption or error-free or that the website, server, hardware and software that support the Medlanes service are free from viruses or something that may interfere with the normal operations of your systems. Medlanes will not be liable for any loss or damage resulting from the transmission of data as may occur during communications with Medlanes.</p>
				<p>d. To the fullest extent permitted by applicable law, Medlanes disclaims any liability resulting from the provision of services by third party providers. Liability shall rest with the appropriate third party provider and shall not under any circumstance be deemed to rest with Medlanes.</p>
				<br>
				<h1>14. Exclusive jurisdiction of the courts</h1>
				<p>If any of these terms and conditions should be determined to be illegal, invalid or otherwise unenforceable by reasons of the laws of any state or country in which these terms and conditions are intended to be effective, then to the extent and within the jurisdiction which that term or condition is illegal, invalid or unenforceable, it shall be severed and deleted from the agreement and the remaining terms and conditions shall survive, remain in full force and effect and continue to be binding and enforceable. The above terms and conditions shall be governed by and construed in accordance with local law and you irrevocably submit to its exclusive jurisdiction.</p>
				<br>
				<h1>15. Change to Terms and Conditions</h1>
				<p>Medlanes reserves the right to change these terms and conditions at any time by posting changes online. You are responsible for regularly reviewing information posted online to obtain timely notice of such changes. Your continued use of the website and the entire Medlanes service after changes are posted constitutes your acceptance of these terms and conditions as modified by the posted changes.</p>
			  ";


// Contact Page--------------------

$lang['contact_title1']="Our team is at your service";

$lang['contact_cont1']="At Medlanes Support is of the highest importance. We are available around the clock and are more than happy to help you with any question that you might have. Please get in contact with us, we'll get back to you shortly.";

$lang['contact_title2']="Phone and eMail-Support";

$lang['contact_cont2']="Over 90% of requests are answered in less that 24h.";


$lang['contact_blue_box1_text1']="Phone";
$lang['contact_blue_box1_text2']="0800 - 765 43 43";
$lang['contact_blue_box1_text3']="Toll free - available 24/7 ";
$lang['contact_blue_box1_text4']="Request a call back";
$lang['contact_blue_box1_text5']="E-Mail";

$lang['contact_blue_box2_text1']="eMail-Support";
$lang['contact_blue_box2_text2']="info@medlanes.com";
$lang['contact_blue_box2_text3']="Direct answer from 8AM - 6PM";

$lang['contact_form_title']="Contact form";
$lang['contact_form_cont']="You can also send us a message using the form below.";

$lang['contact_form_label_surname']="First name";
$lang['contact_form_label_name']="Last name";
$lang['contact_form_label_email']="eMail";
$lang['contact_form_label_msg']="Message";

$lang['contact_completemsg']="<li class=msg>Thank you!</li>";
$lang['contact_errmsg']="<li class=errorMsg>Error!</li>";


// FAQ Page--------------------

$lang['FAQ_main_title']="Frequently Asked Questions";
$lang['FAQ_main_cont']="We strive to provide the best customer service possible. Here are a few of the most commonly asked questions. For everything else please contact us directly. ";

$lang['FAQ_Tab-1']="General";
$lang['FAQ_Tab-2']="Medical";
$lang['FAQ_Tab-3']="Security";
$lang['FAQ_Tab-4']="Payment";

$lang['FAQ_points_title1']="1) Who We Are?";
$lang['FAQ_points_cont1']="Medlanes is a telehealth company located in Berlin, Germany. It was founded by the two experienced professionals, Emil Kendziorra, M.D. and Erik Stoffregen, who wanted to improve the quality of, and access to healthcare. When Emil Kendziorra graduated from medical school in Goettingen, he quickly realized that the future of healthcare will be shaped online. Emil and Erik believe that only online healthcare can lower the cost, increase accessibility and reduce the barriers separating the doctors from their patients. That is why they created Medlanes.";


$lang['FAQ_points_title2']="1) What makes Medlanes unique?";
$lang['FAQ_points_cont2']="Medlanes brings medicine online. Medlanes is available 24/7 and on all devices.";


$lang['FAQ_points_title3']="2) Can I contact a doctor from abroad?";
$lang['FAQ_points_cont3']="Of course, Medlanes is available from anywhere in the world! Just remember that it might be cheaper to use the local WiFi connection compared the the mobile network. ";





$lang['FAQ_points2_title1']="1) What questions can be asked?";
$lang['FAQ_points2_cont1']="You can asked any non-emergency questions using Medlanes. In the case of emergency contact 911 right away!";


$lang['FAQ_points2_title2']="2) Is my doctor seeing patients online?";
$lang['FAQ_points2_cont2']="The amount of doctors seeing patients online increases daily. If your doctor does not yet see patients online give us their contact details and we will send them information about how to join our online practice. Feel free to ask them if they would be interested to offer an online service for their patients.";


$lang['FAQ_points2_title3']="3) What situations is Medlanes build for?";
$lang['FAQ_points2_cont3']="
Let us give you a few examples.<br />

- I don’t feel well and would like to see a doctor, but I'm at work and don't have time. <br />
- My doctor’s office is closed or on vacation and I don’t want to go to a hospital.<br />
- I recently moved and don’t know any doctors in my area.<br />
- I would like to get a second opinion.<br />
- I got a rather simple question and don’t want to wait for hours in the waiting room of my doctor. <br />
- I’m on vacation and would like to talk to a doctor who speaks my language.<br />
- It is Sunday and my child has a fever. I need convenient advice.<br />
";









$lang['FAQ_points3_title1']="1) Is it secure?";
$lang['FAQ_points3_cont1']="Yes, Medlanes uses top level security and encryption to make sure that all your data and medical history files are stored privately and securely";


$lang['FAQ_points3_title2']="2) What are my information used for?";
$lang['FAQ_points3_cont2']="Your personal information is only used to provide advice from a doctor. Your data is not used for anything else";











$lang['FAQ_points4_title1']="1) How much does the service cost? Is it covered by insurance?";
$lang['FAQ_points4_cont1']="
You can download and use the Medlanes App for free. The cost of a doctor consultation is €/£/$29.Whether or not the service is covered by your healthcare insurance depends on your individual plan.
";


$lang['FAQ_points4_title2']="2) What if I’m unhappy with my doctor or the service?";
$lang['FAQ_points4_cont2']="
Your feedback is very important to us! If you’re not totally satisfied, then please contact our customer support team at support@medlanes.com. We're more than happy to help you.
";










































// Impressum Page--------------------

$lang['IMPRESSUM_main_title']="Imprint / Impressum";

$lang['IMPRESSUM_main_content']="Hinweise gem. § 6 des Teledienstegesetzes (TDG) in der Fassung des Gesetzes über rechtliche Rahmenbedingungen für den elektronischen Geschäftsverkehr vom 20.12.2001.";

$lang['IMPRESSUM_address']="Medlanes GmbH i.G. <br />
							Stresemannstraße 66A<br />
							10963 Berlin<br />
							<br />
							Fon +49 (30) 577 004 20<br />
							eMail: info@medlanes.com<br />
							<br />
							Geschäftsführer:: Emil Kendziorra, Erik Stoffregen";

$lang['IMPRESSUM_cont_title1']="Internetredaktion/Autor";

$lang['IMPRESSUM_cont_content1']="(Inhaltlich Verantwortlicher gem. § 10 Abs. 3 MDStV und gem.§ 6 MDStV [Staatsvertrag über Mediendienste]:<br />						<br />Emil Kendziorra";

$lang['IMPRESSUM_cont_title2']="Haftungshinweis:";

$lang['IMPRESSUM_cont_content2']="Alle Texte und Bilder sind urheberrechtlich geschützt. Die Veröffentlichung, Übernahme oder Nutzung von Texten, Bildern oder anderen Daten bedürfen der schriftlichen Zustimmung des Herausgebers – eine Ausnahme besteht bei Presseinformationen und den dort veröffentlichten Bildern. Die inhaltliche Verantwortung erfolgt durch die jeweils die Autoren. Trotz aller Bemühungen um möglichst korrekte Darstellung und Prüfung von Sachverhalten sind Irrtümer oder Interpretationsfehler möglich.<br />
							<br />
							Hiermit distanziert sich der Herausgeber ausdrücklich von allen Inhalten aller gelinkten Seiten auf der Homepage eierfabrik.de und macht sich ihren Inhalt nicht zu Eigen! Diese Erklärung gilt für alle auf dieser Homepage angebrachten Links. (Urteil zur “Haftung für Links” am Landgericht Hamburg vom 12. Mai 1998, AZ: 312 O 85/98)<br />
							<br />
							Der Herausgeber nimmt den Schutz Ihrer personenbezogenen Daten sehr ernst. Wir verarbeiten personenbezogene Daten, die beim Besuch auf unserer Webseiten erhoben werden, unter Beachtung der geltenden datenschutzrechtlichen Bestimmungen. Ihre Daten werden von uns weder veröffentlicht, noch unberechtigt an Dritte weitergegeben..";

$lang['IMPRESSUM_cont_title3']="Disclaimer";

$lang['IMPRESSUM_cont_content3']="Diese Website wurde mit größtmöglicher Sorgfalt erstellt. Die Medlanes GmbH i.G. übernimmt jedoch keine Garantie für die Vollständigkeit, Richtigkeit und Aktualität der enthaltenen Informationen. 
							
							Jegliche Haftung für Schäden, die direkt oder indirekt aus der Benutzung dieser Website entstehen, wird ausgeschlossen. Dies gilt auch für Links, auf die diese Website direkt oder indirekt verweist. Die Solidmedia hat keinen Einfluss auf die Gestaltung und die Inhalte der von uns gelinkten Seiten. Deshalb distanzieren wir uns hiermit ausdrücklich von allen Inhalten aller gelinkten Seiten fremder Anbieter. <br />
							<br />
							Bei Falschinformationen oder Fehlern bitten wir Sie, uns dies mitzuteilen. <br />
							<br />
							Diese Website benutzt Google Analytics, einen Webanalysedienst der Google Inc. („Google“). Google Analytics verwendet sog. „Cookies“, Textdateien, die auf Ihrem Computer gespeichert werden und die eine Analyse der Benutzung der Website durch Sie ermöglichen. Die durch den Cookie erzeugten Informationen über Ihre Benutzung dieser Website (einschließlich Ihrer IP-Adresse) wird an einen Server von Google in den USA übertragen und dort gespeichert. Google wird diese Informationen benutzen, um Ihre Nutzung der Website auszuwerten, um Reports über die Websiteaktivitäten für die Websitebetreiber zusammenzustellen und um weitere mit der Websitenutzung und der Internetnutzung verbundene Dienstleistungen zu erbringen. Auch wird Google diese Informationen gegebenenfalls an Dritte übertragen, sofern dies gesetzlich vorgeschrieben oder soweit Dritte diese Daten im Auftrag von Google verarbeiten. Google wird in keinem Fall Ihre IP-Adresse mit anderen Daten von Google in Verbindung bringen. Sie können die Installation der Cookies durch eine entsprechende Einstellung Ihrer Browser Software verhindern; wir weisen Sie jedoch darauf hin, dass Sie in diesem Fall gegebenenfalls nicht sämtliche Funktionen dieser Website vollumfänglich nutzen können. Durch die Nutzung dieser Website erklären Sie sich mit der Bearbeitung der über Sie erhobenen Daten durch Google in der zuvor beschriebenen Art und Weise und zu dem zuvor benannten Zweck einverstanden.";




?>
