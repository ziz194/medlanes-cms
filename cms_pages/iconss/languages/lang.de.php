<?php
/* 
------------------
Language: German
------------------
*/


$lang = array();


$lang['LOGO_HOME'] = "http://medlanes.com?lang=de";


$lang['PAGE_TITLE'] = 'Medlanes - Ihr Doktor in der Hosentasche - mobile & online';
$lang['HEADER_TITLE'] = 'My website header title';
$lang['SITE_NAME'] = 'My Website';
$lang['SLOGAN'] = 'My slogan here';
$lang['HEADING'] = 'Heading';

// Menu----------------------
$lang['MENU_TEST'] = '<a href="welcome.php?lang=de">Hier starten</a>';
$lang['MENU_HOME'] = '<a href="index.php?lang=de">Hier starten</a>';
$lang['LOGO_HOME'] = 'index.php?lang=de';
$lang['MENU_ABOUT_US'] = '<a href="about-us.php?lang=de"\>Über Medlanes</a>';
$lang['MENU_OUR_DOCTORS'] = '<a href="our-doctors.php?lang=de">Unsere Ärzte</a>';
$lang['MENU_FAQ'] = '<a href="faq.php?lang=de">FAQ</a>';
$lang['MENU_CONTACT'] = '<a href="contact.php?lang=de">Kontakt</a>';

$lang['MENU_TERMS'] = '<a href="terms.php?lang=de">AGB</a>';
$lang['MENU_PRESS'] = '<a href="press.php?lang=de">Presse</a>';
$lang['MENU_JOBS'] = '<a href="jobs.php?lang=de">Jobs</a>';
$lang['MENU_HIRING'] = '<a href="jobs.php?lang=de">Jetzt bewerben</a>';
$lang['MENU_MEDIA'] = '<a href="media.php?lang=de">In der Presse</a>';
$lang['MENU_DOCTORPORTAL'] = '<a href="doctorportal.php?lang=de">Für Ärzte</a>';


// Footer-------------------
$lang['MENU_FOOTER_START_ONLINE'] = '<a href="#">- In Kürze - <br />Start online </a>';
$lang['MENU_FOOTER_ANDROID'] = '<a href="#">- In Kürze - <br />Download Android </a>';
$lang['MENU_FOOTER_IOS'] = '<a href="#">- In Kürze - <br />Download iOS </a>';

$lang['MENU_FOOTER_ABOUT'] = '<a href="about-us.php?lang=de">Über Medlanes </a>';
$lang['FOOTER_CLIENT_TERMS'] = '<a href="terms.php?lang=de">AGB Kunden</a>';
$lang['FOOTER_PROVIDER_TERMS'] = '<a href="terms.php?lang=de">AGB Ärzte</a>';
$lang['FOOTER_PRIVACY_STATEMENT'] = '<a href="terms.php?lang=de">Datenschutzbestimmungen</a>';
$lang['FOOTER_DATA_SECURITY'] = '<a href="terms.php?lang=de">Datensicherheit</a>';
$lang['FOOTER_IMPRINT'] = '<a href="impressum.php?lang=de">Impressum</a>';

$lang['FOOTER_press_room'] = 'Presse';
$lang['FOOTER_jobs'] = 'Jobs';
$lang['FOOTER_hiring'] = 'Bei Medlanes bewerben';
$lang['FOOTER_copyright'] = 'Copyright © Medlanes GmbH  2014  Alle Rechte vorbehalten';


// Home Page--------------------

$lang['HOME_PAGE_banner-sliderone_content']="<h1>Ihr Arzt in der Hosentasche</h1>
				<h2>Rat von Spezialisten für einmalig €29,-</h2>
				<a href='#'> Bald verfügbar</a>";
	

$lang['HOME_PAGE_banner-slidertwo_content']="<h1>Medizinischer Rat&nbsp;<br>&nbsp; immer verfügbar&nbsp;<br></h1>
				<h2>24/7 egal&nbsp;<br>&nbsp; wo Sie gerade sind&nbsp;</h2>
				<a href='#'> Bald verfügbar</a>";
				

				
				
				
$lang['HOME_PAGE_banner-sliderthree_content']="<h1>Medizinischer Rat&nbsp;<br>&nbsp; immer verfügbar&nbsp;<br></h1>
				<h2>24/7 egal&nbsp;<br>&nbsp; wo Sie gerade sind&nbsp;</h2>
				<a href='#'> Bald verfügbar</a>";
				
				
$lang['HOME_PAGE_l-one']="Zertifiziert";
$lang['HOME_PAGE_l-two']="256-bit SSL";




$lang['HOME_PAGE_Concept']="Konzept bekannt aus:";
$lang['HOME_PAGE_widtitle1']="Warum Medlanes";
$lang['HOME_PAGE_widcont1']="Vertrauenswürdige Antworten zu medizinischen Problemen zu finden ist schwer. Vor allem wenn man im Urlaub ist, an Wochenende oder einfach wenig Zeit hat. Mit Medlanes haben Sie Ihren Doktor auf dem Smartphone.";

$lang['HOME_PAGE_widtitle2']="Kurzvorstellung";
$lang['HOME_PAGE_widcont2']="Beschreiben Sie online oder mit der App einfach kurz Ihre Symptome oder Ihre Frage - Medlanes findet den richtigen Arzt für Sie, der Ihnen umgehend antwortet.";

$lang['HOME_PAGE_widtitle3']="Überall verfügbar";
$lang['HOME_PAGE_widcont3']="Medlanes ist immer und überall erreichbar! Im Urlaub, in der Nacht, am Wochenende, auf Ihrem Handy und online. Testen Sie es.";

$lang['HOME_PAGE_bluebox1_text1']="Support";
$lang['HOME_PAGE_bluebox1_text2']="0800 - 765 43 43";
$lang['HOME_PAGE_bluebox1_text3']="gebührenfrei aus dem dt. Festnetz";
$lang['HOME_PAGE_bluebox1_text4']="E-Mail";

$lang['HOME_PAGE_bluebox2_text1']="Zertifiziert & getestet";
$lang['HOME_PAGE_bluebox2_text2']="Qualität und Verlässlichkeit verschrieben.";
$lang['HOME_PAGE_bluebox2_text3']="<a href='about-us.php?lang=de'>Über uns</a>";

$lang['HOME_PAGE_bluebox3_text1']="Umfangreiche Medizin";
$lang['HOME_PAGE_bluebox3_text2']="Hausärzte & Spezialisten bei Medlanes";
$lang['HOME_PAGE_bluebox3_text3']="<a href='our-doctors.php?lang=de'>Unsere Ärzte</a>";





// About us Page--------------------
$lang['ABOUT_US_welcome']="Willkommen bei Medlanes";

$lang['ABOUT_US_BANNER_title1']="Führende Experten aller Fachgebiete";
$lang['ABOUT_US_BANNER_title2']="Die Besten für den besten Service";


$lang['ABOUT_US_tab-title']="Mit Medlanes, weniger...";

$lang['ABOUT_US_tab-1']="Warten";
$lang['ABOUT_US_tab-1_content']="Oft wartet man viele Tage, oder sogar Wochen auf einen Termin bei einem Arzt. Und dann warten Sie meistens nochmal im Wartezimmer der Praxis. Medlanes ist 24 Stunden am Tag, 7 Tage die Woche verfügbar!";


$lang['ABOUT_US_tab-2']="Zahlen";
$lang['ABOUT_US_tab-2_content']="Auch in Deutschland ist Medizin teuer. Für den Staat, den Arbeitgeben und über die Steuern auch für Sie! Nicht nur die direkten Kosten sind relativ, auch die Opportunitätskosten sich wichtig - z.B. wenn Sie auf der Arbeit frei nehmen müssen um einen Termin wahr zu nehmen. Holen Sie sich einen erste kostengünstige Einschätzung über Medlanes!";

$lang['ABOUT_US_tab-3']="Stress";
$lang['ABOUT_US_tab-3_content']="Qualifizierten Rat zu bekommen ist mit viel Stress verbunden! Im Internet finden Sie nur Halbwissen und müssen oft lange suchen um den richtigen Arzt zu finden. Medlanes steht Ihnen immer zur Verfügung! Rund um die Uhr und egal wo Sie gerade sind - auf Ihrem Smartphone und im Web.";

$lang['ABOUT_US_tab-4']="Sorgen";
$lang['ABOUT_US_tab-4_content']="Machen Sie sich keine Sorgen über Ihre Gesundheit! Fragen Sie lieber direkt nach und haben Sie Gewissheit, das Richtige zu tun! Auch wenn es sich um die Gesundheit Ihrer Familie handelt. Mit weniger Sorgen leben Sie gesünder und werden auch weniger krank! ";



$lang['ABOUT_US_someFact-title']="Das Gesundheitssystem hat viele Probleme";


$lang['ABOUT_US_someFact-fact1']="8 min.";
$lang['ABOUT_US_someFact-fact1-cont']="Durchschnittliche Zeit, die ein Arzt für Sie hat";

$lang['ABOUT_US_someFact-fact2']="14 Tage";
$lang['ABOUT_US_someFact-fact2-cont']="Solange warten Sie im Durchschnitt auf einen Termin";

$lang['ABOUT_US_someFact-fact3']="3h";
$lang['ABOUT_US_someFact-fact3-cont']="Die Zeit, die Sie sich für einen Termin inkl. Anreise nehmen müssen";

$lang['ABOUT_US_someFact-fact4']="80%";
$lang['ABOUT_US_someFact-fact4-cont']="Anteil der Fragen, die man in manchen Fachgebieten auch online beantworten kann";

$lang['ABOUT_US_someFact-fact5']="50%";
$lang['ABOUT_US_someFact-fact5-cont']="Anteil der Patienten, die von besserem Rat profitieren würden";


$lang['ABOUT_US_benefits-title']="Warum, Wie, Wo...";

$lang['ABOUT_US_benefits-msg']="Medlanes ist immer und überall für Sie verfügbar. Wir kombinieren die Einfachheit einer Onlinesuche mit der Qualität und Verlässlichkeit eines Besuches beim Arzt. Laden Sie sich die Medlanes app ganz einfach herunter oder nutzen Sie die Web-Version. Sie stellen innerhalb von Minuten Ihre Fragen, laden ein Foto hoch (wenn nötig) und beantworten einige Rückfragen, Medlanes findet den richtigen Arzt für Sie und dieser antwortet in Kürze!";



$lang['ABOUT_US_benefits-benifit1_title']="Im Urlaub";
$lang['ABOUT_US_benefits-benifit1_content']="Im Ausland ist es oft schwierig einen Arzt zu finden der gut Englisch oder Deutsch spricht. Nutzen Sie Medlanes!";

$lang['ABOUT_US_benefits-benifit2_title']="Auf der Arbeit";
$lang['ABOUT_US_benefits-benifit2_content']="Auf der Arbeit Zeit frei nehmen ist teilweise mit Stress verbunden, holen Sie sich einen erste Einschätzung online.";

$lang['ABOUT_US_benefits-benifit3_title']="In der Nacht";
$lang['ABOUT_US_benefits-benifit3_content']="Medlanes ist auch in der Nacht verfügbar, stellen Sie einfach Ihre Fragen und ein Arzt antwortet umgehend.";

$lang['ABOUT_US_benefits-benifit4_title']="Am Wochenende";
$lang['ABOUT_US_benefits-benifit4_content']="Praktisch alle Ärzte haben am Wochenende geschlossen! Medlanes beantwortet Ihre Fragen 24/7.";

$lang['ABOUT_US_benefits-benifit5_title']="Wenn's einfach sein muss";
$lang['ABOUT_US_benefits-benifit5_content']="Nutzen Sie Ihr Smartphone oder Ihren PC statt für Antworten lange zu warten.";

$lang['ABOUT_US_benefits-benifit6_title']="Bei langen Entfernungen";
$lang['ABOUT_US_benefits-benifit6_content']="Vor allem auf dem Land ist der Arzt oft weit entfernt - für einfache Fragen einfach Medlanes nutzen";



$lang['ABOUT_US_welcome_cont']="Vertraueswürdige Antworten zu medizinischen Problem zu finden ist schwer. Vorallem wenn man im Urlaub ist, am Wochenende oder einfach wenig Zeit hat. Mit Medlanes haben Sie Ihren Doktor auf dem Smartphone - und zwar egal wo Sie sind oder wieviel Uhr es ist. Mit der Medlanes App oder über die Webseite erhalten Sie jederzeit qualifizierte Informationen zu medizinischen Fragen. Gegründet von Emil Kendziorra (einem Arzt) und Erik Stoffregen, greift Medlanes auf jahrelange Expertise aus IT und Medizin zurück und ermöglicht Patienten Zugang zu ärztlichem Rat - schnell, sicher und bequem von überall auf der Welt zu erreichen!
";

$lang['ABOUT_US_title1']="Ihre Gesundheit im Blick - Einfach und schnell.";

$lang['ABOUT_US_content1']="Beschreiben Sie online oder mit der App einfach kurz Ihre Symptome oder Ihre Frage - Medlanes findet den richtigen Arzt für Sie, der Ihnen umgehend antwortet. Sie erhalten medizinischen Rat am Wochenende, im Urlaub, in der Nacht, während der Arbeit und zu jeder anderen Zeit und Ort.";

$lang['ABOUT_US_title2']="Sicher und vertraulich per Smartphone oder Web";

$lang['ABOUT_US_content2']="Alle Ärzte haben langjährige Erfahrungen in Ihren Spezialgebieten und sind natürlich an die ärztliche Schweigepflicht gebunden. Darüber hinaus werden alle Daten ausschließlich auf deutschen Servern gespeicht und Sie sine jederzeit Herr über Ihre Daten.";



// DOCTORPORTAL Page--------------------
$lang['DOCTORPORTAL_welcome']="Ärzte bei Medlanes";

$lang['DOCTORPORTAL_welcome_cont']="Medlanes geht in Kürze online. Ärzte, die Medlanes nutzen möchten schreiben bitte an arzt@medlanes.com";

$lang['DOCTORPORTAL_title1']="Ihre Vorteile";

$lang['DOCTORPORTAL_content1']="Diese Seite befinden sich momentan im Aufbau. Weitere Informationen sind in Kürze verfügbar.";

$lang['DOCTORPORTAL_title2']="So funktioniert Medlanes für Ärzte";

$lang['DOCTORPORTAL_content2']="Diese Seite befinden sich momentan im Aufbau. Weitere Informationen sind in Kürze verfügbar.";







// MEDIA Page--------------------
$lang['MEDIA_welcome']="Medlanes in der Presse";

$lang['MEDIA_welcome_cont']="Lesen Sie in der nationalen und internationale Presse über Medlanes.";

$lang['MEDIA_title1']="Frankfurter Allgemeine Zeitung // 08. Nov 2013";

$lang['MEDIA_content1']="[ Medlanes ] setzt auf die enge Verzahnung von Medizin und Technik. Auf diesem Milliardenmarkt mischen Konzerne wie IBM, SAP, Microsoft mit. Microsoft hat aus 360 Bewerbern aus ganz Europa neun Unternehmen ausgewählt, die sie für besonders aussichtsreich halten. Medlanes ist eines davon.";


$lang['MEDIA_title2']="Tagesumschau // 18. März 2014";

$lang['MEDIA_content2']="Ein Arzt für die Hosentasche? Kein Problem mit der neuen Seite, auf der man folgendes erfährt: Holen Sie sich den Rat eines Arztes – schnell,bequem und kostengünstig. Medlanes schickt Ihnen eine Konsultation eines Arztes direkt auf ihr Handy. Egal wo Sie sind oder welche Beschwerden Sie haben, Medlanes ist für Sie da.";








// Our Doctors Page--------------------

$lang['OUR_DOCTORS_BANNER_title1']="Gewissheit über Ihre Gesundheit.";
$lang['OUR_DOCTORS_BANNER_title2']="Bequeme Beratung durch unsere Ärzte";


$lang['OUR_DOCTORS_title1']="Unsere Ärzte sind für Sie da.";
$lang['OUR_DOCTORS_cont1']="Medlanes arbeitet ausschließlich mit qualifizierten und lizenzierten Ärzten zusammen. Zusätzlich werden all unsere Partnerärzte einer umfassenden Qualitäts- und Sicherheitsprüfung unterzogen. Mit Hilfe von sorgfältig entwickelten Algorithmen, verbinden wir Sie mit Ihrem persönlichen Doktor.";



$lang['DOCTORS-TYPE_title1']="Allgemeinmediziner / Innere Medizin";
$lang['DOCTORS-TYPE_content1']="Die meisten Fragen werden am Besten von einem Allgemeinmediziner beantwortet. Unsere Ärzte beantworten Ihre Fragen und beraten Sie gerne.";


$lang['DOCTORS-TYPE_title2']="Kinderarzt";
$lang['DOCTORS-TYPE_content2']="Es ist wichtig, dass Sie über die Gesundheit Ihrer Kinder informiert sind. Fragen zu Ihren Kindern beantworten unsere Kinderärzte wann immer Sie es benötigen.";

$lang['DOCTORS-TYPE_title3']="Kardiologie, Gynäkologie, Dermatologie, Orthopäde, ...";
$lang['DOCTORS-TYPE_content3']="Unsere Spezialisten beantworten Ihre Fragen zu einer Vielzahl von Fachgebieten. Stellen Sie ganz einfach Ihre Fragen, wir finden den richtigen Arzt für Sie.";


$lang['DOCTORS-SPOTLIGHT_title']="Ärzte im Fokus";

$lang['DOCTORS-SPOTLIGHT_Doctor-name1']="Dr. med. Lars Omera";
$lang['DOCTORS-SPOTLIGHT_Doctor-type1']="Kardiologie";

$lang['DOCTORS-SPOTLIGHT_Doctor-name2']="Dr. med. Thomas Pers";
$lang['DOCTORS-SPOTLIGHT_Doctor-type2']="Allgemeinmediziner";

$lang['DOCTORS-SPOTLIGHT_Doctor-name3']="Dr. med. Ane Wertheim";
$lang['DOCTORS-SPOTLIGHT_Doctor-type3']="Kinderärztin";

$lang['DOCTORS-SPOTLIGHT_content']="Damit Sie unsere Ärzte kennenlernen stellen wir Ihnen von Zeit zu Zeit einige der Ärzte aus verschiedenen Fachgebieten vor. Mehr in Kürze.";



// PRESS Page--------------------

$lang['PRESS_title1']="Presse";
$lang['PRESS_cont1']="Presseanfragen bitte an presse@medlanes.com richten. Wir stellen Ihnen gerne Pressekits zur Verfügung.";




// JOBS Page--------------------

$lang['JOBS_title1']="Offene Stellen bei Medlanes";
$lang['JOBS_cont1']="Detailierte Stellenbeschreibungen werden im Mai 2014 veröffentlich. Momentan haben wir einige offene Positionen. Wir freuen uns auf Initiativbewerbungen aus allen Bereichen. <br />
<br />
Aktuell haben wir u.A. auch offene Positionen für Praktikanten in den Bereichen Marketing und Business development. ";

$lang['JOBS_heading']="Offene Stellen bei Medlanes";
$lang['JOBS_heading2']="Wir sind immer auf der Suche nach neuen Talenten um unser Team zu verstärken.";
$lang['JOBS_subheading']="Hier findest du unsere aktuellen offenen Positionen:";
$lang['JOBS_list1']="Marketing & Sales Intern (m/f)";
$lang['JOBS_list2']="Technical Research & Development Intern (m/f)";
$lang['JOBS_list3']="Internship in Healthcare Startup in Berlin";
$lang['JOBS_list4']="Executive Assistant Intern (m/f)";

// Terms Page--------------------

$lang['TERMS_title']="Allgemeine Geschäftsbedingungen / Terms &amp; Conditions";
$lang['TERMS_cont']="
			§ 1 Allgemeines
				(1) Diese Allgemeinen Geschäftsbedingungen gelten für sämtliche Angebote der Medlanes GmbH (nachfolgend Medlanes), die von den Nutzern über die Internetseite www.medlanes.com oder andere Applikationen von Medlanes bezogen werden können.
				(2) Abweichende Vereinbarungen bedürfen der Schriftform. <br />
				<br />
				§ 2 Leistungsumfang
				(1) Medlanes bietet Gesundheitsinformationen und informative Dienstleistungen auf der Internetseite www.medlanes.com und über andere Applikationen an. Diese werden anhand von Daten erstellt, die der Nutzer Medlanes überlässt. Das Angebot von Medlanes wird durch approbierte Ärzte unterstützt und der Dialog mit dem Nutzer erfolgt durch den Datenaustausch über die Onlineplattform Medlanes. Diese Informationen stellen grundsätzlich keine Diagnose oder Therapie sondern ausschließlich eine Unterstützung der Informationsfindung oder Internetrecherche dar. Für eine medizinische Untersuchung oder Behandlung ist ein Arzt vor Ort oder in dringenden Fällen eine Notfallambulanz aufzusuchen.
				(2) Die von Medlanes angebotenen Leistungen ersetzen nicht einen traditionellen Arztbesuch. Suchen Sie einen Arzt vor Ort auf oder in dringenden Fällen eine Notfallambulanz. <br />
				<br />
				§ 3 Vertragsschluss 
				(1) Das Vertragsverhältnis über die Nutzung der Leistungen von Medlanes kommt erst durch die Zustimmung dieser AGBs zustande.
				(2) Die Nutzung der Leistungen von Medlanes, insbesondere die Zusendung eines Sachverhaltes (Fall) gilt im Zweifel als Zustimmung.
				(3) Eine Anmeldung oder Nutzung der Dienstleistungen bei Medlanes ist nur möglich, sofern der Nutzer sämtliche in der Anmeldung abgefragten Daten wahrheitsgemäß und vollständig angegeben hat. Der Nutzer muss mindestens 18 Jahre alt und unbeschränkt geschäftsfähig sein und darf nicht unter Betreuung stehen, sofern Medlanes eine gesetzliche Vertretung nicht aktiv nachgewiesen wird. Es besteht kein Rechtsanspruch auf die Inanspruchnahme der Leistungen von Medlanes. Medlanes ist berechtigt, die Anmeldung ohne Angabe von Gründen zu verweigern. <br />
				<br />
				§ 4 Widerruf
				(1) Die Nutzer können die Vertragserklärungen innerhalb von 14 Tagen ohne Angabe von Gründen in Textform (z. B. Brief, Fax, E-Mail) widerrufen. Die Frist beginnt nach Erhalt dieser Belehrung in Textform, jedoch nicht vor Vertragsschluss und auch nicht vor Erfüllung der Informationspflichten gemäß Artikel 246 § 2 in Verbindung mit § 1 Abs. 1 und 2 EGBGB sowie unserer Pflichten gemäß § 312g Abs. 1 Satz 1 BGB in Verbindung mit Artikel 246 § 3 EGBGB. Zur Wahrung der Widerrufsfrist ist die rechtzeitige Absendung des Widerrufs. 
				(2) Der Widerruf ist zu richten an: Medlanes GmbH, vertreten durch die Geschäftsführer Emil Kendziorra und Erik Stoffregen, Stresemannstraße 66, 10963 Berlin, eMail: info(at)medlanes.com
				(3) Im Falle eines wirksamen Widerrufs sind die beiderseits empfangenen Leistungen zurückzugewähren und ggf. gezogene Nutzungen (z. B. Zinsen) herauszugeben. Kann der Nutzer dem Anbieter die empfangenen Leistungen sowie Nutzungen (z.B. Gebrauchsvorteile) nicht oder teilweise nicht oder nur in verschlechtertem Zustand zurückgewähren bzw. herausgeben, muss der Nutzer Wertersatz leisten. Dies kann dazu führen, dass der Nutzer die vertraglichen Zahlungsverpflichtungen für den Zeitraum bis zum Widerruf gleichwohl erfüllen muss. Verpflichtungen zur Erstattung von Zahlungen müssen innerhalb von 30 Tagen erfüllt werden. Die Frist beginnt für den Nutzer mit der Absendung der Widerruferklärung, für Medlanes mit deren Empfang.
				(4) Das Widerrufsrecht erlischt vorzeitig, wenn der Vertrag von beiden Seiten auf ausdrücklichen Wunsch des Nutzers vollständig erfüllt ist. <br />
				<br />
				§ 5 Vergütung und Zahlungsbedingungen
				(1) Alle auf der Internetseite www.medlanes.com angegebenen Preise verstehen sich einschließlich der ggf. anfallenden gesetzlichen Mehrwertsteuer.

				(2) Die Vergütung wird auf das erste Anfordern von Medlanes fällig, nachdem die von Medlanes bereitgestellten Leistungen in Anspruch genommen wurden. Medlanes behält sich vor nur nach Leistung eines Vorschusses tätig zu werden. Der Vorschuss wird vor Leistungserbringung fällig. Nimmt der der Nutzer die Leistung nicht in Anspruch oder ist die Leistung in anderer Weise gestört, erstattet Medlanes den Vorschuss zurück.
				(3) Medlanes akzeptiert folgende Zahlungsweisen: Kreditkarte, Lastschrift, PayPal. <br />
				<br />
				§ 6 Nutzungsrechte
				(1) Dem Nutzer stehen ausschließlich die nach diesen Allgemeinen Geschäftsbedingungen eingeräumten Rechte an dem Internetangebot zu. 
				(2) Die über die Online-Plattform von Medlanes und die weiteren Applikationen veröffentlichten Inhalte, Informationen, Bilder, Datenbanken und Softwareprogramme sind grundsätzlich urheberrechtlich geschützt und in der Regel Eigentum oder lizensiert von Medlanes.
				(3) Die Inhalte auf der Online-Plattform dürfen nur für persönliche und nicht für kommerzielle Zwecke genutzt oder vervielfältigt werden. Eine Weitergabe der Inhalte ist ohne ausdrückliche Zustimmung von Medlanes untersagt. <br />
				<br />
				§ 7 Nutzerdaten
				(1) Medlanes erhebt und nutzt für die Abwicklung der zwischen dem Nutzer und Medlanes geschlossenen Verträge Daten des Nutzers. Ohne die Einwilligung des Nutzers werden keine Daten erhoben, verarbeitet oder genutzt. Die mit Einwilligung erhobenen Daten werden nur verarbeitet oder genutzt soweit dies für die Abwicklung des Vertragsverhältnisses und zur Leistungserfüllung erforderlich ist.
				(2) Der Nutzer hat jederzeit und im Rahmen der Verfügbarkeit der Online-Plattform und sonstiger Applikationen von Medlanes die Möglichkeit, die von ihm gespeicherten Daten im Online-Profil abzurufen, zu ändern oder zu löschen. Dies gilt jedoch nicht für diejenigen Daten, für die eine gesetzliche Aufbewahrungspflicht besteht. Dies sind insbesondere die vom Nutzer über die Online-Plattform übermittelten Daten.
				(3) Im Übrigen wird auf die unter der Schaltfläche „Datenschutz“ abrufbare Datenschutzerklärung verwiesen. <br />
				<br />
				§ 8 Verfügbarkeit
				(1) Das Leistungsangebot steht in der Regel 24 Stunden am Tag zur Verfügung. Hiervon ausgenommen sind die Zeiten, in denen Datensicherungsarbeiten vorgenommen und Systemwartungs- oder Programmpflegearbeiten am System oder der Datenbank durchgeführt werden. Medlanes wird die hieraus entstehenden möglichen Störungen möglichst gering halten. 
				(2) Medlanes schließt jegliche Haftung wegen technischer oder sonstiger Störungen aus. <br />
				<br />
				§ 9 Haftung
				(1) Die von Medlanes bereitgestellten Inhalte und Leistungen werden sorgfältig nach dem jeweiligen Stand der Wissenschaft und des medizinischen Wissens erstellt. Medlanes übernimmt jedoch keine Gewähr für die Richtigkeit, Vollständigkeit, Korrektheit, Aktualität, Qualität oder Fehlerfreiheit der bereitgestellten Inhalte oder ihre Eignung für bestimmte Zwecke. Medlanes übernimmt keine Gewähr für die Richtigkeit der einzelnen Informationen. Es obliegt dem jeweiligen Nutzer, die Richtigkeit zu überprüfen oder einen Arzten aufzusuchen. Es wird insoweit auf § 2 der Allgemeinen Geschäftsbedingungen verwiesen.
				(2) Eine Haftung von Medlanes auf Schadensersatz, insbesondere wegen Verzugs, Nichterfüllung, Schlechterfüllung oder unerlaubter Handlung besteht nur bei Verletzung wesentlicher Vertragspflichten, auf deren Erfüllung in besonderem Maße vertraut werden durfte. Im Übrigen ist eine Haftung von Medlanes ausgeschlossen, es sei denn, es bestehen zwingende gesetzliche Regelungen. Der Haftungsausschluss gilt nicht für Vorsatz und grobe Fahrlässigkeit. 
				(3) Medlanes haftet nur für vorhersehbare Schäden. Die Haftung für mittelbare Schäden, insbesondere Mangelfolgeschäden, unvorhersehbare Schäden oder untypische Schäden sowie entgangenen Gewinn ist ausgeschlossen. Gleiches gilt für die Folgen von Arbeitskämpfen, zufälligen Schäden und höherer Gewalt.
				(4) Die vorstehenden Haftungsbeschränkungen gelten für sämtliche vertragliche und nichtvertragliche Ansprüche.
				(5) Zur Zahlungsabwicklung verwendet Medlanes externe Dienstleister woe PayPal und außerdem greift Medlanes auf die folgenden Drittanbieter zurück und verweist unter Freizeichnung von jeder Verantwortlichkeit und Haftung auf die entsprechenden AGBs:
				- Google <br />
				<br />
				§ 10 Pflichten des Nutzers
				(1) Der Nutzer darf das Leistungsangebot nur sachgerecht nutzen. Er wird insbesondere seinen Benutzernamen und das Passwort für den Zugang geheim halten, nicht weitergeben, keine Kenntnisnahme dulden oder ermöglichen und die erforderlichen Maßnahmen zur Gewährleistung der Vertraulichkeit ergreifen und bei einem Missbrauch oder Verlust dieser Angaben oder einem entsprechenden Verdacht dies dem Unternehmen anzuzeigen.
				(2) Der Nutzer ist verpflichtet Angaben, die für die Bereitstellung der individuellen ärztlischen Leistungen benötigt werden korrekt, umfassend und wahrheitsgemäß anzugeben. <br />
				<br />
				§ 11 Sperrung des Zugangs / Kündigung
				(1) Medlanes behält sich vor, bei Verdacht einer missbräuchlichen Nutzung oder wesentlichen Vertragsverletzung diesen Vorgängen nachzugehen, entsprechende Vorkehrungen zu treffen und bei einem begründeten Verdacht den Zugang des Nutzers zu sperren. Sollte der Verdacht ausgeräumt werden können, wird die Sperrung wieder aufgehoben, andernfalls steht Medlanes ein außerordentliches Kündigungsrecht zu.
				(2) Jeder Partei steht das Recht zur Kündigung aus wichtigem Grund zu. Die Kündigung bedarf der Textform (z.B. Email). Mit Wirksamwerden der Kündigung wird der Zugang des Nutzers zum Leistungsangebot von Medlanes gesperrt. <br />
				<br />
				§ 12 Änderungen
				(1) Medlanes hat das Recht, die AGB jederzeit gegenüber den Nutzern mit Wirkung für die Zukunft zu ändern. 
				(2) Eine beabsichtigte Änderung wird den Nutzern, die sich registriert haben, per E-Mail an die letzte dem Anbieter mitgeteilte Adresse mitgeteilt. Die jeweilige Änderung wird wirksam, wenn der jeweilige Nutzer ihr nicht innerhalb von zwei Wochen nach Absendung der E-Mail widerspricht. Für die Einhaltung der Zwei-Wochen-Frist ist die rechtzeitige Absendung des Widerspruchs maßgeblich. 
				(3) Widerspricht der Nutzer der Änderung innerhalb der Zwei-Wochenfrist, ist Medlanes berechtigt, das Vertragsverhältnis insgesamt außerordentlich fristlos zu beenden, ohne dass dem Nutzer hieraus irgendwelche Ansprüche gegen Medlanes erwachsen. Wird das Vertragsverhältnis nach dem wirksamen Widerspruch des Nutzers fortgesetzt, behalten die bisherigen AGB ihre Gültigkeit. <br />
				<br />
				§ 13 Schlussbestimmungen
				(1) Es gilt deutsches Recht.
				(2) Sollten einzelne Bestimmungen dieser AGB einschließlich dieser Bestimmung ganz oder teilweise unwirksam sein oder werden, bleibt die Wirksamkeit der übrigen Regelungen unberührt. Anstelle der unwirksamen oder fehlenden Bestimmungen treten die jeweiligen gesetzlichen Regelungen. <br />
				<br />
				Datenschutzbestimmungen <br />
				<br />
				<br />
				<br />
				§ 1 Erhebung personenbezogener Daten 
				Um die nach § 2 AGB vereinbarten Leistungen erbringen zu können, erhebt, verarbeitet und nutzt die 
				Medlanes GmbH i.G.
				vertreten durch die Geschäftsführer Emil Kendziorra und Erik Stoffregen
				Stressemannstraße 66, 10963 Berlin
				Handelsregisternummer: beantragt
				eMail: info[at]medlanes.com
				– nachfolgend ”Medlanes” –
				die von dem Nutzer angegebenen Daten, sowie die IP-Adresse von dessen Endgerät (nachfolgend „personenbezogene Daten”). <br />
				<br />
				§ 2 Zweck der Erhebung, Verarbeitung und Nutzung 
				(1) Die personenbezogenen Daten erhebt, verarbeitet und nutzt Medlanes ausschließlich zu dem Zweck, die mit dem Nutzer im Sinne des § 2 AGB vereinbarten Leistungen zu erbringen.
				(2) Die personenbezogenen Daten werden ausschließlich grundsätzlich auf Servern in Deutschland gespeichert und verarbeitet, die den Deutschen Datenschutzbestimmungen unterliegen. Die personenbezogenen Daten werden darüber hinaus nicht an Dritte übermittelt.
				(3) Medlanes setzt „Cookies” zu dem Zweck ein, die Berechtigung des Nutzers zum Abruf der gewünschten Leistungen zu verifizieren und zur Analyse des Nutzungsverhaltens, um die Optimierung der Webseite im Sinne der Nutzer zu ermöglichen. <br />
				<br />
				§ 3 Schutz der Privatsphäre 
				Medlanes verpflichtet sich, die Privatsphäre des Nutzers zu schützen und versichert, die personenbezogenen Daten im Einklang mit dem Bundesdatenschutzgesetz und dem Telemediengesetz zu erheben, zu verarbeiten und zu nutzen und ausschließlich für die Erfüllung der unter § 2 definierten Zwecke zu verarbeiten und zu nutzen. Medlanes wird seine Mitarbeiter – insbesondere die angeschlossenen Ärzte - entsprechend verpflichten. <br />
				<br />
				§ 4 Rechte des Nutzers 
				(1) Der Nutzer hat bezüglich der personenbezogenen Daten die durch das Bundesdatenschutzgesetz gewährleisteten Rechte auf Auskunft, Berichtigung und Löschung. Diese Rechte kann der Nutzer per Post oder eMail direkt an Medlanes.. 
				(2) Der Nutzer ist berechtigt, die Leistungen von Medlanes ohne Angaben zu persönlichen Daten anonym zu beziehen. In diesem Fall kann er nur seine E-Mail-Adresse und anstelle seines Namens ein Pseudonym angeben. <br />
				<br />
				§ 5 Einwilligung 
				Sofern der Nutzer in die Bedingungen dieser Erklärung einwilligt, erklärt er sich mit der geregelten Nutzung der personenbezogenen Daten einverstanden. Die Einwilligung oder ihre Verweigerung erfolgt durch Klicken oder Nichtklicken auf das entsprechende Feld nach dem Hinweis auf diese Bestimmungen. Die erteilte oder verweigerte Einwilligung wird protokolliert; ihr Inhalt ist für den Nutzer jederzeit unter auf Anfrage abrufbar. Der Nutzer hat das Recht, jederzeit seine Einwilligung zu widerrufen und damit sein Konto mit allen Daten aufzulösen und zu löschen. <br />
				<br />
				§ 6 Zahlungsabwicklung/Drittanbieter  
				(1) Sofern der Nutzer zur Zahlungsabwicklung den Service von PayPal in Anspruch nimmt, wird der Nutzer per Hyperlink auf die Website von www.paypal.de weitergeleitet. Medlanes weist darauf hin, dass hier weitere personenbezogene Daten des Nutzers erhoben, verarbeitet und genutzt werden. Medlanes trifft insofern keine Verantwortlichkeit; es gelten ausschließlich die Nutzungs- und Datenschutzbestimmungen von PayPal (Europe) S.à r.l. et Cie, S.C.A..
				(2) Außerdem greift Medlanes auf die Leistungen der folgenden weiteren Drittanbieter zu und verweist freibleibend auf die entpsrechenden Nutzungs- und Datenschutzbestimmungen:
				- Google
			";


// Contact Page--------------------

$lang['contact_title1']="Unser Team steht für Sie bereit";

$lang['contact_cont1']="Medlanes schreibt Support groß. Wir stehen Ihnen jederzeit mit Rat und Tat zur Seite, egal wie wir Ihnen helfen können. Kontaktieren Sie uns - wir freuen uns auf Ihre Nachricht.";

$lang['contact_title2']="Telefonischer und eMail-Support";

$lang['contact_cont2']="In über 90% der Anfragen antworten wir innerhalb von 24h.";


$lang['contact_blue_box1_text1']="Kundensupport";
$lang['contact_blue_box1_text2']="0800 - 765 43 43";
$lang['contact_blue_box1_text3']="gebührenfrei 24 Stunden an 365 Tagen im Jahr";
$lang['contact_blue_box1_text4']="Rückrufservice";
$lang['contact_blue_box1_text5']="E-Mail";

$lang['contact_blue_box2_text1']="eMail-Support";
$lang['contact_blue_box2_text2']="info@medlanes.com";
$lang['contact_blue_box2_text3']="8:00 bis 18:00 Uhr";

$lang['contact_form_title']="Kontaktformular";
$lang['contact_form_cont']="Schreiben Sie uns! Wir freuen uns auch über Feedback und Anregungen.";

$lang['contact_form_label_surname']="Vorname";
$lang['contact_form_label_name']="Name";
$lang['contact_form_label_email']="eMail";
$lang['contact_form_label_msg']="Anfrage";

$lang['contact_completemsg']="<li class=msg>Danke für ihre Anfrage</li>";
$lang['contact_errmsg']="<li class=errorMsg>Error!</li>";


// FAQ Page--------------------

$lang['FAQ_main_title']="Häufige Fragen";
$lang['FAQ_main_cont']="Guter Kundenservice steht bei Medlanes im Vordergrund. Hier finden Sie Antworten auf die häufigsten Fragen. Für alles Weitere kontaktieren Sie uns einfach direkt.";


$lang['FAQ_Tab-1']="Allgemein";
$lang['FAQ_Tab-2']="Medizin";
$lang['FAQ_Tab-3']="Sicherheit";
$lang['FAQ_Tab-4']="Bezahlung";

$lang['FAQ_points_title1']="1) Wer sind wir?";
$lang['FAQ_points_cont1']="Die Medlanes GmbH ist eine Telemhealth Firma mit Sitz in Berlin. Die Firma wurde von dem Arzt Emil Kendziorra und Erik Stoffregen gegründet um Medizin online zu bringen. Emil and Erik sind davon überzeugt, dass die Zukunft der Medizin online gestaltet wird. Medlanes hat das Ziel die Verfügbarkeit und Einfachheit von qualifiziertem medizinischen Rat zu verbessern und Patienten gesünder zu machen.";

$lang['FAQ_points_title2']="1) Was macht Medlanes besonders?";
$lang['FAQ_points_cont2']="Medlanes bildet einen großen Teil der Medizin online ab. Sie erhalten Rat zu einer Vielzahl medizinischer Fachbereiche und Zugang zu einem großen Netzwerk an hochqualifizierten Ärzten.";


$lang['FAQ_points_title3']="2) Kann ich Medlanes auch im Ausland nutzen?";
$lang['FAQ_points_cont3']="Natürlich, Medlanes ist international verfügbar! Nutzen Sie den Service ganz einfach wie gewohnt, über WLAN oder das mobile Netz (beachten Sie die Roaminggebühren Ihres Netzanbieters).";







$lang['FAQ_points2_title1']="1) Welche Fragen kann ich stellen?";
$lang['FAQ_points2_cont1']="Sie können jede Frage an unser Ärzte-Team stellen, handelt es sich jedoch um eine Notfall-Situation sollte umgehend ein Notarzt kontaktiert werden. ";


$lang['FAQ_points2_title2']="2) Ist mein bisheriger Arzt online erreichbar?";
$lang['FAQ_points2_cont2']="Unser Ärztenetzwerk wächst ständig. Fragen Sie Ihren Arzt gerne, ob er Medlanes kennt und nutzen möchte um Ihnen besseren Zugang zu qualifizierten Rat zu bieten. Wir schicken gerne Informationen an interessierte Ärzte.";


$lang['FAQ_points2_title3']="3) Für welche Fragen ist Medlanes konzipiert?";
$lang['FAQ_points2_cont3']="
Hier finden Sie einige Beispiele für typische Anwendungsfälle<br />

- Ich fühle mich nicht gut und habe keine Zeit einen Arzt aufzusuchen.<br />
- Mein Hausarzt hat geschlossen oder ist im Urlaub und ich habe eine Frage. <br />
- Ich bin gerade umgezogen und suche einen Arzt.<br />
- Ich hätte gerne eine Zweitmeinung.<br />
- Ich suche eine Antwort auf eine einfache Frage, möchte aber nicht lange auf einen Termin warten. <br />
- Ich bin im Urlaub und würde gerne einen Arzt kontaktieren, der meine Sprache spricht.<br />
- Es ist Wochenende und mein Kind hat Fieber. Soll ich ins Krankenhaus fahren?<br />
";












$lang['FAQ_points3_title1']="1) Sind meine Daten sicher?";
$lang['FAQ_points3_cont1']="Ja, Medlanes nutzt ausschließlich Software, die den höchsten Sicherheitsstandards entspricht. Alle Ihre Daten werden auf deutschen Servern gespeichert und können von Ihnen vollständig gelöscht werden.";


$lang['FAQ_points3_title2']="2) Für was werden meine Informationen genutzt?";
$lang['FAQ_points3_cont2']="Ihre Angaben werden ausschließlich zweckgebunden für die Beantwortung durch den Arzt genutzt. Medlanes benutzt Ihre Daten für keinerlei andere Zwecke.";











$lang['FAQ_points4_title1']="1) Was kostet die Nutzung von Medlanes? Übernimmt meine Versicherung die Kosten?";
$lang['FAQ_points4_cont1']="
Die Medlanes App können Sie kostenlos herunterladen und nutzen. Wenn Sie einen Arzt ein Frage stellen kostet dies einmalig €29. Die anfallenden Kosten werden dabei in den meisten Fällen nicht von den Krankenkassen übernommen.
";


$lang['FAQ_points4_title2']="2) Was passiert wenn ich mit dem Service unzufrienden bin?";
$lang['FAQ_points4_cont2']="
Ihre Meinung ist uns sehr wichtig! Wenn Sie nicht vollständig zufrieden sein sollten, bitten wir Sie unser Supportteam (support@medlanes.com) zu kontaktieren. Wir helfen Ihnen gerne weiter und finden eine Lösung.
";









// Impressum Page--------------------

$lang['IMPRESSUM_main_title']="Imprint / Impressum";

$lang['IMPRESSUM_main_content']="Hinweise gem. § 6 des Teledienstegesetzes (TDG) in der Fassung des Gesetzes über rechtliche Rahmenbedingungen für den elektronischen Geschäftsverkehr vom 20.12.2001.";

$lang['IMPRESSUM_address']="Medlanes GmbH i.G. <br />
							Stresemannstraße 66A<br />
							10963 Berlin<br />
							<br />
							Fon +49 (30) 577 004 20<br />
							eMail: info@medlanes.com<br />
							<br />
							Geschäftsführer:: Emil Kendziorra, Erik Stoffregen";

$lang['IMPRESSUM_cont_title1']="Internetredaktion/Autor";

$lang['IMPRESSUM_cont_content1']="(Inhaltlich Verantwortlicher gem. § 10 Abs. 3 MDStV und gem.§ 6 MDStV [Staatsvertrag über Mediendienste]:<br />						<br />Emil Kendziorra";

$lang['IMPRESSUM_cont_title2']="Haftungshinweis:";

$lang['IMPRESSUM_cont_content2']="Alle Texte und Bilder sind urheberrechtlich geschützt. Die Veröffentlichung, Übernahme oder Nutzung von Texten, Bildern oder anderen Daten bedürfen der schriftlichen Zustimmung des Herausgebers – eine Ausnahme besteht bei Presseinformationen und den dort veröffentlichten Bildern. Die inhaltliche Verantwortung erfolgt durch die jeweils die Autoren. Trotz aller Bemühungen um möglichst korrekte Darstellung und Prüfung von Sachverhalten sind Irrtümer oder Interpretationsfehler möglich.<br />
							<br />
							Hiermit distanziert sich der Herausgeber ausdrücklich von allen Inhalten aller gelinkten Seiten auf der Homepage eierfabrik.de und macht sich ihren Inhalt nicht zu Eigen! Diese Erklärung gilt für alle auf dieser Homepage angebrachten Links. (Urteil zur “Haftung für Links” am Landgericht Hamburg vom 12. Mai 1998, AZ: 312 O 85/98)<br />
							<br />
							Der Herausgeber nimmt den Schutz Ihrer personenbezogenen Daten sehr ernst. Wir verarbeiten personenbezogene Daten, die beim Besuch auf unserer Webseiten erhoben werden, unter Beachtung der geltenden datenschutzrechtlichen Bestimmungen. Ihre Daten werden von uns weder veröffentlicht, noch unberechtigt an Dritte weitergegeben..";

$lang['IMPRESSUM_cont_title3']="Disclaimer";

$lang['IMPRESSUM_cont_content3']="Diese Website wurde mit größtmöglicher Sorgfalt erstellt. Die Medlanes GmbH i.G. übernimmt jedoch keine Garantie für die Vollständigkeit, Richtigkeit und Aktualität der enthaltenen Informationen. 
							
							Jegliche Haftung für Schäden, die direkt oder indirekt aus der Benutzung dieser Website entstehen, wird ausgeschlossen. Dies gilt auch für Links, auf die diese Website direkt oder indirekt verweist. Die Solidmedia hat keinen Einfluss auf die Gestaltung und die Inhalte der von uns gelinkten Seiten. Deshalb distanzieren wir uns hiermit ausdrücklich von allen Inhalten aller gelinkten Seiten fremder Anbieter. <br />
							<br />
							Bei Falschinformationen oder Fehlern bitten wir Sie, uns dies mitzuteilen. <br />
							<br />
							Diese Website benutzt Google Analytics, einen Webanalysedienst der Google Inc. („Google“). Google Analytics verwendet sog. „Cookies“, Textdateien, die auf Ihrem Computer gespeichert werden und die eine Analyse der Benutzung der Website durch Sie ermöglichen. Die durch den Cookie erzeugten Informationen über Ihre Benutzung dieser Website (einschließlich Ihrer IP-Adresse) wird an einen Server von Google in den USA übertragen und dort gespeichert. Google wird diese Informationen benutzen, um Ihre Nutzung der Website auszuwerten, um Reports über die Websiteaktivitäten für die Websitebetreiber zusammenzustellen und um weitere mit der Websitenutzung und der Internetnutzung verbundene Dienstleistungen zu erbringen. Auch wird Google diese Informationen gegebenenfalls an Dritte übertragen, sofern dies gesetzlich vorgeschrieben oder soweit Dritte diese Daten im Auftrag von Google verarbeiten. Google wird in keinem Fall Ihre IP-Adresse mit anderen Daten von Google in Verbindung bringen. Sie können die Installation der Cookies durch eine entsprechende Einstellung Ihrer Browser Software verhindern; wir weisen Sie jedoch darauf hin, dass Sie in diesem Fall gegebenenfalls nicht sämtliche Funktionen dieser Website vollumfänglich nutzen können. Durch die Nutzung dieser Website erklären Sie sich mit der Bearbeitung der über Sie erhobenen Daten durch Google in der zuvor beschriebenen Art und Weise und zu dem zuvor benannten Zweck einverstanden.";




?>
