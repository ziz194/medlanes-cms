<!DOCTYPE html>
<?php 
    include_once 'functions.php';
	if($_POST){		
		
		require_once 'mandrill-api-php/src/Mandrill.php';	
		include_once 'db/connect.php';
		
		try{		
			$sth = $db->prepare("INSERT INTO questions(`text`,`email`) VALUES(?,?)");				 
			$sth->execute(array($_POST['question'],$_POST['email']));

			
			$mandrill = new Mandrill('7uxpzYG1lEQapNmfgXB4wA');
			$async = true;
			$template_name = 'Medlanes Green';
			$template_content = array(
				array(
					'name' => 'headline',
					'content' => $lang['SIGNUPEMAIL_HEADLINE']),
				array(
					"name" => "main",
					"content" => $lang['SIGNUPEMAIL_MAIN']),
				array(
					"name" => "signature",
					"content" => $lang['SIGNUPEMAIL_SIGNATURE']),
				array(
					"name" => "footer",
					"content" => $lang['SIGNUPEMAIL_FOOTER'])
				);
			$message = array(
				'subject' => $lang['SIGNUPEMAIL_SUBJECT'],
				'from_email' => 'service@medlanes.com',
				'from_name' => 'Medlanes',
				'to' => array(
					array(
						'email' => $_POST['email'],
					)
				),
				'track_opens' => true,
				'track_clicks' => true,
				'tags' => array('f2'),
			);
			
			$mandrill->messages->sendTemplate($template_name, $template_content, $message, $async);		
		}
		catch (Exception $e){
		  die('Erreur : ' . $e->getMessage());
		}
	}
?>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
	<link rel="icon" type="image/png" href="images/favicon.ico" sizes="16x16">
	<title><?php echo $lang['TITLE_PAYMENT']; ?></title>
	<title>Medlanes</title>
	
	
	<!-- css -->
	<link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="css/global.css">

	<script src="js/jquery.js" /></script>
	<script src="js/migrate.js" /></script>
	<!-- css -->
	<link rel="stylesheet" href="css/payment.css">
	<link rel="stylesheet" href="css/sss.css">
	<link rel="stylesheet" href="css/jquery.css">
	<link rel="stylesheet" href="css/noslider.css">
	
	<!-- scripts -->
	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
	<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
	<script src="js/sss.min.js"></script>
	<script src="js/noslider.js"></script>
	<?php 
	if (isset($_GET["lang"]) && !empty($_GET["lang"])) {
		$getparam = $_GET['lang'];
			switch ($getparam) {
			    case "en":
			        print('	
			        	<script src="js/switchpaymentEN.js"></script>
			        	<script src="js/switchtextsEN.js"></script>
					');
			        break;
			    case "de":
			        print('	
			        	<link rel="stylesheet" href="css/german.css">
			        	<script src="js/switchpaymentDE.js"></script>
			        	<script src="js/switchtextsDE.js"></script>
					');
			        break;
			}
		 
	}else{  
    		        print('
			        	<script src="js/switchpaymentDE.js"></script>
			        	<script src="js/switchtextsDE.js"></script>
					');
	}
	?>
	<script>
		function doacheck(){
			console.log('clicking');
			console.log($('#tac').val());
			if($('#tac').val() == 'notchecked'){
				$('.agree p').css('color',"red");
			}else{
				$('.paypalForm').submit();
			}
		}
	$(document).ready(function(){
		$( ".block,.text p" ).click(function() {

			if($('.payment-checked').hasClass('checked')){
				$('.payment-checked').css('opacity','0');
				$('.payment-checked').removeClass('checked');
				$('#tac').val('notchecked');
			}else{
			  $('.payment-checked').css('opacity','1');
			  $('.payment-checked').addClass('checked');
			  $('#tac').val('checked');				
			}

		});



$("#leftslide").noUiSlider({
	behaviour: 'tap-drag',
	direction: 'rtl',
	decimals: 0,
	start: [ 2 ],
	snap : true,
	orientation: "vertical",
	range: {
		'min': [ 1 ],		
		'50%': [ 2 ],
		'max': [ 3 ]
	}
});

$("#rightslide").noUiSlider({
	behaviour: 'tap-drag',
	direction: 'rtl',
	decimals: 0,
	start: [ 2 ],
	snap : true,
	orientation: "vertical",
	range: {
		'min': [ 1 ],		
		'50%': [ 2 ],
		'max': [ 3 ]
	}
});

$('#rightslide').on('slide', function(){
	check();	
});
$('#leftslide').on('slide', function(){
	check();	
});


	});
	var level = ['High','Medium','Low'];
	
	function check(){
		var x = $('#leftslide').val();
		var y = $('#rightslide').val();

		var x = parseInt(x);
		var y = parseInt(y);
		swapurg(x);
		swapdets(y);
		change(Math.floor(x+y));
	}
	

	
	$(function() {
		$('.slider').sss({
			slideShow : true, // Set to false to prevent SSS from automatically animating.
			startOn : 0, // Slide to display first. Uses array notation (0 = first slide).
			transition : 400, // Length (in milliseconds) of the fade transition.
			speed : 3500, // Slideshow speed in milliseconds.
			showNav : true // Set to false to hide navigation arrows.
		});
			
	    $( "#range-one" ).slider({
			orientation: "vertical",
			range: "min",
			value:2,
			min: 1,
			max: 3,
			step: 1,
			slide: function( event, ui ) {	        
				$('#amounta').text(ui.value);
				$(".urgency").val(level[ui.value-1]);
				check();
			}
	    });

	    $( "#range-two" ).slider({
			orientation: "vertical",
			range: "min",
			value:2,
			min: 1,
			max: 3,
			step: 1,
			slide: function( event, ui ) {
				$('#amountb').text(ui.value);			
				$(".detail").val(level[ui.value-1]);
				check();
			}
	    });

		$('.paypalForm').click(function() {
			var custom = $('.custom').val();			
			custom += 'funnel=f2' + '&ti='+ localStorage.getItem('specmsgid')  +'&to=' + localStorage.getItem('token') + '&price='+$('.price').val()+'&urgency='+$('.urgency').val()+'&detail='+$('.detail').val();
			$('.custom').val(custom);
		});	
	});
	function moveit(val,slider){
		console.log(val);
		val=val;
		console.log(slider);
		if(slider == 'left'){
			$("#leftslide").val(val);
			check();
		}else{
			$("#rightslide").val(val);
			check();
		}

	}

	</script>
</head>
<body>

<?php include "header.php" ?>	

	<div class="thanks">
		<div class="three-step">
			<div class="mini">
				<img src="images/thankyou/check.png" alt="#">
				<div style="background:#ffffff" class="one">
					<img src="images/thankyou/getsymptom.png" alt="#">
					<p><?php echo $lang['3step_1']; ?></p>
					<img class="arrow" src="images/thankyou/arrow.png" alt="#"/>
				</div>
				<div style="background:#f8f8f8" class="one">
					<img src="images/thankyou/getpay.png" alt="#">
					<p><?php echo $lang['3step_2']; ?></p>
					<img class="arrow" src="images/thankyou/arrow.png" alt="#"/>
				</div>
				<div class="one">
					<img src="images/thankyou/getadvice.png" alt="#">
					<p><?php echo $lang['3step_3']; ?></p>
					<img class="arrow" src="images/thankyou/arrow.png" alt="#"/>
				</div>
			</div>			
		</div><!-- end 3steps -->
	</div><!-- end thankyou -->
	<div class="sub-wrap">
		<div class="left">
			<div class="counters">
				<p  id="amounta">1</p>
				<p id="amountb">1</p>
				<p class="amount"></p>
				<p id="leftam">2</p>
				<p id="rightam">2</p>
			</div>
			<h1><?php echo $lang['PAYMENT_NEW1']; ?></h1>
			<p><?php echo $lang['PAYMENT_NEW2']; ?></p>
			<div class="one">
				<div class="two">
					<p class="pad-this-on-ger"><?php echo $lang['PAYMENT_CHOOSEPANEL']; ?></p>
					<div class="testslide" id="leftslide"></div>
					<div class="text">
						<p onclick="moveit('3','left')"><?php echo $lang['PAYMENT_CHOOSEPANEL4']; ?></p>
						<p onclick="moveit('2','left')"><?php echo $lang['PAYMENT_CHOOSEPANEL3']; ?></p>
						<p onclick="moveit('1','left')"><?php echo $lang['PAYMENT_CHOOSEPANEL2']; ?></p>
					</div>
					<span id="value-span"></span>
				</div>
				<div class="two">
					<p><?php echo $lang['PAYMENT_CHOOSEPANEL1']; ?></p>
					<div class="testslide" id="rightslide"></div>
					<div class="text">
						<p onclick="moveit('3','right')"><?php echo $lang['PAYMENT_CHOOSEPANEL4']; ?></p>
						<p onclick="moveit('2','right')"><?php echo $lang['PAYMENT_CHOOSEPANEL3']; ?></p>
						<p onclick="moveit('1','right')"><?php echo $lang['PAYMENT_CHOOSEPANEL2']; ?></p>
					</div>
				</div>
				<p class="fourhund" style="width:400px;line-height:22px;float:left;"><?php echo $lang['PAYMENT_NEW3']; ?> <?php echo $lang['pricess']; ?><span class="amount">34</span></p>
			</div>
			<div class="agree">
			<input id="tac" type="hidden" value="notchecked">
				<div class="block">
		            <img class="payment-checked" src="images/payment/circle-check.png" alt="#"/>
		            <img src="images/payment/circle-pay.png" alt="#"/>						
				</div>
				<div class="text">
					<p><?php echo $lang['PAYMENT_AGREEMENT1']; ?> <?php echo $lang['PAYMENT_AGREEMENT2']; ?> <?php echo $lang['PAYMENT_AGREEMENT3']; ?> <?php echo $lang['PAYMENT_AGREEMENT4']; ?></p>
				</div>
			</div>
			<div class="dep">
				<form class="paypalForm" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top"> 
					<input type="hidden" class="price" value="34" />
					<input type="hidden" class="urgency" value="Medium" />
					<input type="hidden" class="detail" value="Medium" />
					<input type="hidden" name="cmd" value="_s-xclick">
					<input type="hidden" id="codePaypal" name="hosted_button_id" value="V26EPGG4G6G2G">
					<input type="hidden" class="custom" name="custom" value="">
					<div class="button btn" onclick="doacheck()"></div>
					<img  alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
				</form> 
				<!-- <a href="#"><img src="images/payment/paypal.jpg" alt="#"></a> -->
				<div class="one curmarg">
					<img class="wetrust" src="images/global/trust.png" alt="#"/>
				</div>

		</div>
	</div><!-- end left -->
		<div class="rightside">
			<div class="one beg">
				<img src="images/payment/clock.png" alt="#"/>
				<h2><?php echo $lang['PAYMENT_NEW4']; ?></h2>
			</div>
			<div class="doc">
				<img src="images/payment/doc.png" alt="#"/>
				<div class="r">
					<div class="one">
						<img src="images/payment/health.png" alt="#"/>
						<h2><b><?php echo $lang['PAYMENT_NEW5']; ?></b></h2>
						<p><?php echo $lang['PAYMENT_NEW6']; ?></p>
					</div>
					<div class="one">
						<img src="images/payment/verified.png" alt="#"/>
						<h2><b><?php echo $lang['PAYMENT_NEW7']; ?></b></h2>
					</div>
					<div class="one">
						<p><?php echo $lang['PAYMENT_NEW8']; ?></p>
						<img src="images/payment/stars.png" alt="#"/>
						<p><?php echo $lang['PAYMENT_NEW9']; ?></p>
					</div>

				</div>
				<p style="line-height:22px;padding-top:2px;float:left"><?php echo $lang['PAYMENT_NEW10']; ?></p>
			</div>
		</div>
		<div class="concept">
			<p><?php echo $lang['INDEX_7']; ?></p>
			<?php 
			if (isset($_GET["lang"]) && !empty($_GET["lang"])) {
				$getparam = $_GET['lang'];
					switch ($getparam) {
					    case "en":
					        print('	
					        	<img class="bigconcept" src="images/global/concept.png" alt="#"/>
					        	<img class="mobconcept" src="images/global/c10.png" alt="#"/>
							');
					        break;
					    case "de":
					        print('	
					        	<img class="bigconcept" src="images/global/conceptde.png" alt="#"/>
					        	<img class="mobconcept" src="images/global/conceptgermob.png" alt="#"/>
							');
					        break;
					}
				 
			}else{  
		    		        print('
					        	<img class="bigconcept" src="images/global/conceptde.png" alt="#"/>
					        	<img class="mobconcept" src="images/global/conceptgermob.png" alt="#"/>
							');
			}
			?>
		</div>	
	</div>

<?php include "footer.php" ?>	
</body>
</html>
