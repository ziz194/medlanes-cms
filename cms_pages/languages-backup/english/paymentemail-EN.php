<?php 

$lang['PAYMENTEMAIL_SUBJECT'] = "Important: Your doctor's consultation with Medlanes.";
$lang['PAYMENTEMAIL_HEADLINE'] = "Thank you for using Medlanes";
$lang['PAYMENTEMAIL_MAIN'] = "Hello, we received your question <br><br>Now we will match you with one of our qualified doctors and send you an answer as soon as possible. In the unexpected case that we are not be able to find a suitable doctor for your question, then we will refund your deposit within 24 business hours. ";
$lang['PAYMENTEMAIL_SIGNATURE'] = "In good health, <br>The Medlanes Team";
$lang['PAYMENTEMAIL_FOOTER'] = "Have a question or want to say hello? Just hit reply!";

?>