<?php 

$lang['FAQ_'] = "";

// FAQ HEADING

$lang['FAQ_HEADING'] = "Frequently Asked Questions";

// FAQ SUBS
$lang['FAQ_SUB1'] = "Frequently Asked Questions";
$lang['FAQ_SUB2'] = "We strive to provide the best customer service possible. Here are a few of the most commonly asked questions. For everything else please contact us directly.";

// FAQ TABS

$lang['FAQ_TABS_HEADING1'] = "Medical";
$lang['FAQ_TABS_TEXT1_1'] = "<p><b>1) What questions can be asked?</b></p>
						<p>You can asked any non-emergency questions using Medlanes. In the case of emergency contact 911 right away!</p>";
$lang['FAQ_TABS_TEXT1_2'] = "<p><b>2) Is my doctor seeing patients online?</b></p>
						<p>The amount of doctors seeing patients online increases daily. If your doctor does not yet see patients online give us their contact details and we will send them information about how to join our online practice. Feel free to ask them if they would be interested to offer an online service for their patients.</p>";
$lang['FAQ_TABS_TEXT1_3'] = "<p><b>3) What situations is Medlanes build for?</b></p>
						<p>Let us give you a few examples.</p>
						<p>- I don’t feel well and would like to see a doctor, but I'm at work and don't have time. </p>
						<p>- My doctor’s office is closed or on vacation and I don’t want to go to a hospital.</p>
						<p>- I recently moved and don’t know any doctors in my area.</p>
						<p>- I would like to get a second opinion.</p>
						<p>- I got a rather simple question and don’t want to wait for hours in the waiting room of my doctor. </p>
						<p>- I’m on vacation and would like to talk to a doctor who speaks my language.</p>
						<p>- It is Sunday and my child has a fever. I need convenient advice.</p>";

$lang['FAQ_TABS_HEADING2'] = "Security";
$lang['FAQ_TABS_TEXT2_1'] = "<p><b>1) Is it secure?</b></p>
			        	<p>Yes, Medlanes uses top level security and encryption to make sure that all your data and medical history files are stored privately and securely</p>";
$lang['FAQ_TABS_TEXT2_2'] = "<p><b>2) What are my information used for?</b></p>
			        	<p>Your personal information is only used to provide advice from a doctor. Your data is not used for anything else</p>";

$lang['FAQ_TABS_HEADING3'] = "Payment";
$lang['FAQ_TABS_TEXT3_1'] = "<p><b>1) How much does the service cost? Is it covered by insurance?</b></p>
			        	<p>You can download and use the Medlanes App for free. The cost of a doctor consultation is $29.Whether or not the service is covered by your healthcare insurance depends on your individual plan.</p>";
$lang['FAQ_TABS_TEXT3_2'] = "<p><b>2) What if I’m unhappy with my doctor or the service?</b></p>
			        	<p>Your feedback is very important to us! If you’re not totally satisfied, then please contact our customer support team at support@medlanes.com. We're more than happy to help you.</p>";

$lang['FAQ_TABS_HEADING4'] = "General";
$lang['FAQ_TABS_TEXT4_1'] = "<p><b>1) What makes Medlanes unique?</b></p>
			        	<p>Medlanes brings medicine online. Medlanes is available 24/7 and on all devices.</p>";
$lang['FAQ_TABS_TEXT4_2'] = "<p><b>2) Can I contact a doctor from abroad?</b></p>
			        	<p>Of course, Medlanes is available from anywhere in the world! Just remember that it might be cheaper to use the local WiFi connection compared the the mobile network.</p>";



?>