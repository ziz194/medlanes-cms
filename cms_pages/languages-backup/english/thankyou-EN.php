<?php 

$lang['THANKYOU_'] = "";


// HEADING

$lang['THANKYOU_HEADING1'] = "Thank you";
$lang['THANKYOU_HEADING2'] = "You don’t need to do anything else! One of our doctors will get back to you soon.";

// LEFT SIDE IF YOU HAVE ANY QUESTIONS

$lang['THANKYOU_FAQ1'] = "If you have any questions...";
$lang['THANKYOU_FAQ2'] = "Our customer support is always there for you! Feel free to contact us any time.";


// RIGHT SIDE LEARN MORE

$lang['THANKYOU_LEARN_HEADING1'] = "Would you like to learn more?";
$lang['THANKYOU_LEARN_HEADING2'] = "Start here and discover what Medlanes offers and how we can help you with your questions.";
$lang['THANKYOU_LEARN_LINK'] = "Our Service";

$lang['THANKS_LINK_TO_SERV'] = '<a href="service?lang=en">Read more about Medlanes</a>';
?>