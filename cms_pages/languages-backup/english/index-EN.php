<?php 

// PAGE TITLES

$lang['TITLE_IMPRINT'] = "Medlanes | Qualified medical information - Expert opinions - Second opinions | Imprint";
$lang['TITLE_INDEX'] = "Medlanes | Qualified medical information - Expert opinions - Second opinions | Home";
$lang['TITLE_PAYMENT'] = "Medlanes | Qualified medical information - Expert opinions - Second opinions | Payment";
$lang['TITLE_PRIVACY'] = "Medlanes | Qualified medical information - Expert opinions - Second opinions | Privacy Statement";
$lang['TITLE_TERMS'] = "Medlanes | Qualified medical information - Expert opinions - Second opinions | Terms of Service";
$lang['TITLE_CONTACT'] = "Medlanes | Qualified medical information - Expert opinions - Second opinions | Contact";
$lang['TITLE_THANKYOU'] = "Medlanes | Qualified medical information - Expert opinions - Second opinions | Thank you";
$lang['TITLE_SERVICE'] = "Medlanes | Qualified medical information - Expert opinions - Second opinions | Thank you";
$lang['TITLE_FAQ'] = "Medlanes | Qualified medical information - Expert opinions - Second opinions | Thank you";
$lang['TITLE_DOCTOR'] = "Medlanes | Qualified medical information - Expert opinions - Second opinions | Thank you";


$lang['HEADER_ADQ'] = 'add-question.php?lang=de';


// HEADER TEXT


$lang['INDEX_H'] = 'Qualified medical <br> Information & Assessment <br> by leading Specialists.';
$lang['INDEX_H1'] = 'Every 9 seconds a question is answered. <br> We assure 100% satisfaction guarantee.';





$lang['HEADER_1'] = "Validated medical information from medical experts!";
$lang['HEADER_2'] = "Our support team is ready to help <b>0800 / 765 43 43</b>";


$lang['HEADER_LINK1'] = '<a href="service.php?lang=en">Our Service</a>';
$lang['HEADER_LINK2'] = '<a href="#">Diseases<img style="margin-left:10px;" src="images/triangle.png" alt="#"/></a>';
$lang['HEADER_LINK3'] = '<a href="doctors.php?lang=en">Doctors</a>';
$lang['HEADER_LINK4'] = '<a href="faq.php?lang=en">FAQ</a>';
$lang['HEADER_LINK5'] = '<a href="contact.php?lang=en">Support</a>';
/*FOOTER LINKS*/

$lang['FOOTER1'] = '<a href="index.php?lang=en">Medlanes Mainpage</a>';
$lang['FOOTER2'] = '<a href="imprint.php?lang=en">About Medlanes / Imprint</a>';
$lang['FOOTER3'] = '<a href="contact.php?lang=en">Support</a>';
$lang['FOOTER4'] = '<a href="terms.php?lang=en"> Terms & Conditions</a>';
$lang['FOOTER5'] = '<a href="privacy.php?lang=en"> Privacy Statement</a>';

$lang['FOOTER8'] = '<a href="#"> PR Contact</a>';
$lang['FOOTER9'] = '<a href="#"> Careers</a>';
$lang['FOOTER10'] = '<p>Supporters <br> and Partners</p>';
$lang['FOOTER11'] = '&copy; Medlanes GmbH - Alle Rechte vorbehalten.';

$lang['INDEX_1'] = "Quick answers by leading specialists";
$lang['INDEX_2'] = "Our experts answer your medical questions. A new question is answered every <b>9 seconds</b>. We provide a<b>100% satisfaction guarantee.</b>";
$lang['INDEX_3'] = "Your question to our experts";
$lang['INDEX_4'] = "Your eMail*";
$lang['INDEX_5'] = "Get an answer now";
$lang['INDEX_6'] = "*Your data is encrypted and will not be disclosed.";
$lang['INDEX_7'] = "Concept seen on:";
$lang['INDEX_8'] = "In the news";
$lang['INDEX_9'] = "Medlanes have seen a spike in traffic lately. Medlanes’ traffic increased by 36% over the last month and was visited by almost 400,000 people.";
$lang['INDEX_10'] = "Today, 10/8/2013";
$lang['INDEX_11'] = "Our customers about us";
$lang['INDEX_12'] = "“This app has improved my life vastly. I've had more questions than answers for a while now. It's unreasonable to call my doctor every 5 minutes.";
$lang['INDEX_13'] = "Xenia, 27 <br> Berlin";
$lang['INDEX_14'] = "Our doctors";
$lang['INDEX_15'] = "Dr. med. K. Hamann";
$lang['INDEX_16'] = "General Physician 12 years of practice.";
$lang['INDEX_17'] = "Rating: 4.9 / 5";
$lang['INDEX_18'] = "";
$lang['INDEX_19'] = "";


$lang['FOOTER_link1'] = '<a href="index.php?lang=en">Home</a>';
$lang['FOOTER_link2'] = '<a href="service.php?lang=en">Our Service</a>';
$lang['FOOTER_link3'] = '<a href="doctors.php?lang=en">Doctors</a>';
$lang['FOOTER_link4'] = '<a href="faq.php?lang=en">FAQ</a>';
$lang['FOOTER_link5'] = '<a href="contact.php?lang=en">Contact</a>';
// 2nd menu
$lang['FOOTER_link6'] = '<a href="about.php?lang=en">About Medlanes</a>';
$lang['FOOTER_link7'] = '<a href="career.php?lang=en">Careers</a>';
// 3rd menu
$lang['FOOTER_link8'] = '<a href="terms.php?lang=en">Terms of Service</a>';
$lang['FOOTER_link9'] = '<a href="privacy.php?lang=en">Privacy Statement</a>';
$lang['FOOTER_link10'] = '<a href="imprint.php?lang=en">Imprint</a>';


?>