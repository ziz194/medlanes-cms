<?php 


$lang['aq-1'] = 'Get instant answers to medical questions';
$lang['aq-2'] = 'Pinpoint the problems';
$lang['aq-3'] = 'front';
$lang['aq-4'] = 'back';
$lang['aq-5'] = 'Your symptoms';
$lang['aq-6'] = 'More informations';
$lang['aq-7'] = 'Additional information';
$lang['aq-8'] = 'Since when do you experience the symptoms.';
$lang['aq-9'] = 'Select';
$lang['aq-10'] = 'Acute';
$lang['aq-11'] = 'Today';
$lang['aq-12'] = 'This week';
$lang['aq-13'] = 'This month';
$lang['aq-chronic'] = 'Chronic';
$lang['aq-yq'] = 'Your question';
$lang['aq-experience'] = 'Did you experience any trauma / injury?';
$lang['aq-14'] = 'yes';
$lang['aq-15'] = 'no';
$lang['aq-16'] = 'Please list medications you are currently taking';
$lang['aq-17'] = 'Anything noteworthy in you medical history?(e.g. high blood pressure, COPD, Asthma, etc)';
$lang['aq-18'] = 'Additional files';
$lang['aq-19'] = 'Attach Files';
$lang['aq-20'] = 'The perfect qualified doctor will be assigned to your question within hours.';
$lang['aq-21'] = 'Confirm';
$lang['aq-add'] = 'ADD';

// HEADING

$lang['aq-bp-1'] = 'Head';
$lang['aq-bp-2'] = 'Right arm';
$lang['aq-bp-3'] = 'Right chest';
$lang['aq-bp-4'] = 'Left chest';
$lang['aq-bp-5'] = 'Left arm';
$lang['aq-bp-6'] = 'Right stomach';
$lang['aq-bp-7'] = 'Left stomach';
$lang['aq-bp-8'] = 'Right hand';
$lang['aq-bp-9'] = 'Public area';
$lang['aq-bp-10'] = 'Left hand';
$lang['aq-bp-11'] = 'Right thigh';
$lang['aq-bp-12'] = 'Left thigh';
$lang['aq-bp-13'] = 'Right lower leg';
$lang['aq-bp-14'] = 'Left lower leg';
$lang['aq-bp-15'] = 'Right foot';
$lang['aq-bp-16'] = 'Left foot';

?>

