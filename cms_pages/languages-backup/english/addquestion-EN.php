<?php 

$lang['ADDQUESTION_'] = "";


$lang['EMAIL_HEAD'] = "Your eMail address - we will send the answer to that address.";
$lang['emailsign'] = "Incorrect email adress";
$lang['emailsign2'] = "eMail already used";

$lang['ADDQUESTION_1'] = "Ask your question here, a doctor is waiting to answer you.";
$lang['ADDQUESTION_2'] = "Select affected body part(s)";
$lang['ADDQUESTION_3'] = "Pinpoint where the problem is. You can select multiple locations.";
$lang['ADDQUESTION_4'] = "front";
$lang['ADDQUESTION_5'] = "back";
$lang['ADDQUESTION_6'] = "Add your symptoms";
$lang['ADDQUESTION_7'] = "Please list your symptomes (e.g. Headache, Stomach pain, Skin rash, ...)";
$lang['ADDQUESTION_8'] = "Attach relevant pictures";
$lang['ADDQUESTION_9'] = "E.g. images, test results, ...";
$lang['ADDQUESTION_10'] = "We need some additional information";
$lang['ADDQUESTION_11'] = "When did the symptoms start?";
$lang['ADDQUESTION_12'] = "Did you experience a trauma or injury?";
$lang['ADDQUESTION_13'] = "Please list all relevant medication that you are taking ";
$lang['ADDQUESTION_14'] = "Do you have a specific question?";
$lang['ADDQUESTION_15'] = "Continue to final step.";

$lang['ADDQUESTION_yes'] = "Yes";
$lang['ADDQUESTION_no'] = "No";
$lang['ADDQUESTION_add'] = "Add";
$lang['ADDQUESTION_upload'] = "Upload file";
$lang['ADDQUESTION_LPANEL'] = "Perfect! You already have an account";
$lang['ADDQUESTION_LPANEL1'] = "Please enter your eMail and password and submit your question. The password was sent to you by eMail with your first question.";

$lang['ADDQUESTION_LPANEL2'] = 'Password:';
$lang['ADDQUESTION_LPANEL3'] = 'Type your Password';
$lang['ADDQUESTION_LPANEL4'] = 'Type your eMail';


$lang['aq-bp-33'] = 'Right upper back';
$lang['aq-bp-44'] = 'Left upper back';

$lang['aq-bp-66'] = 'Right lower back';
$lang['aq-bp-77'] = 'Left lower back';

$lang['aq-bp-1'] = 'Head';
$lang['aq-bp-2'] = 'Right arm';
$lang['aq-bp-3'] = 'Right chest';
$lang['aq-bp-4'] = 'Left chest';
$lang['aq-bp-5'] = 'Left arm';
$lang['aq-bp-6'] = 'Right stomach';
$lang['aq-bp-7'] = 'Left stomach';
$lang['aq-bp-8'] = 'Right hand';
$lang['aq-bp-9'] = 'Pubic area';
$lang['aq-bp-10'] = 'Left hand';
$lang['aq-bp-11'] = 'Right thigh';
$lang['aq-bp-12'] = 'Left thigh';
$lang['aq-bp-13'] = 'Right lower leg';
$lang['aq-bp-14'] = 'Left lower leg';
$lang['aq-bp-15'] = 'Right foot';
$lang['aq-bp-16'] = 'Left foot';
$lang['aq-coupon'] = 'Please enter your <b>coupon</b> here';


$lang['aq-1'] = 'Get instant answers to medical questions';
$lang['aq-2'] = 'Pinpoint the problems';
$lang['aq-3'] = 'front';
$lang['aq-4'] = 'back';
$lang['aq-5'] = 'Your symptoms';
$lang['aq-6'] = 'More informations';
$lang['aq-7'] = 'Additional information';
$lang['aq-8'] = 'Since when do you experience the symptoms.';
$lang['aq-9'] = 'Select';
$lang['aq-10'] = 'Acute';
$lang['aq-11'] = 'Today';
$lang['aq-12'] = 'This week';
$lang['aq-13'] = 'This month';
$lang['aq-chronic'] = 'Chronic';
$lang['aq-yq'] = 'Your question';
$lang['aq-experience'] = 'Did you experience any trauma / injury?';
$lang['aq-14'] = 'Yes';
$lang['aq-15'] = 'No';
$lang['aq-16'] = 'Please list medications you are currently taking';
$lang['aq-17'] = 'Anything noteworthy in you medical history?(e.g. high blood pressure, COPD, Asthma, etc)';
$lang['aq-18'] = 'Additional files';
$lang['aq-19'] = 'Attach Files';
$lang['aq-20'] = 'The perfect qualified doctor will be assigned to your question within hours.';
$lang['aq-21'] = 'Confirm';
$lang['aq-add'] = 'ADD';
?>
