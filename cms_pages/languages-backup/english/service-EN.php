<?php 

$lang['SERVICE_'] = "";


/*SERVICE MAIN HEADING*/
$lang['SERVICE_MAINHEADING'] = "A doctor in your pocket,  <br> wherever you are!";

// SERVICE SUB MENU

$lang['SERVICE_SUBMENU1'] = "How it Works";
$lang['SERVICE_SUBMENU2'] = "Why use Medlanes";
$lang['SERVICE_SUBMENU3'] = "Use Cases";
$lang['SERVICE_SUBMENU4'] = "Price & Use Cases";
// HOW

$lang['SERVICE_HOW1'] = "How medlanes Works";
$lang['SERVICE_HOW2'] = "With Medlanes, you receive a diagnosis from a qualified, board-certified general practitioner or specialist within 24 hours. There are just three simple steps to get started! You submit your questions and symptoms through our web form, choose one of the suggested doctors from your state, and pay for your doctor visit. The doctor you have selected will evaluate all of the data you have provided, and respond to your question within 24 hours. Any follow up questions that you may have will be answered within that timeline as well!";


$lang['SERVICE_HOW_HEAD1'] = "Describe your Symptoms";
$lang['SERVICE_HOW_TEXT1'] = "First, describe your symptoms through our simple questionnaire available online with our mobile App. At this time, you will also have the opportunity to upload any documentation or images relative to your condition. After that is completed, you can ask your doctor the specific questions you have.";

$lang['SERVICE_HOW_HEAD2'] = "Choose your doctor & pay";
$lang['SERVICE_HOW_TEXT2'] = "Choose one of the suggested doctors from your area and pay your consultation fee of just $29. If our doctors are not able to help you, you will be refunded within 24 hours of all costs in full with our money back guarantee.";

$lang['SERVICE_HOW_HEAD3'] = "Qualified medical advice";
$lang['SERVICE_HOW_TEXT3'] = "A qualified, board-certified doctor will answer your questions and diagnose your symptoms typically within an hour! Now it is possible to get an expert opinion of your condition with the push of a button!";


// WHY

$lang['SERVICE_WHY_HEADING'] = "Why use Medlanes";

$lang['SERVICE_WHY_HEAD1'] = "8 min.";
$lang['SERVICE_WHY_TEXT1'] = "Average time span a doctor has for a visit.";

$lang['SERVICE_WHY_HEAD2'] = "14 days";
$lang['SERVICE_WHY_TEXT2'] = "Average waiting time to get a doctor's appointment.";

$lang['SERVICE_WHY_HEAD3'] = "3 hours";
$lang['SERVICE_WHY_TEXT3'] = "Time a doctor's visit takes on average, including getting there.";

$lang['SERVICE_WHY_HEAD4'] = "80%";
$lang['SERVICE_WHY_TEXT4'] = "Percentage of visits to the doctor's office that could be done online";

$lang['SERVICE_WHY_HEAD5'] = "50%";
$lang['SERVICE_WHY_TEXT5'] = "Amount of treatments that could be improved if patients got continuous advice.";


/*SERVICE TABS*/
$lang['SERVICE_TABS_HEAD'] = "With medlanes you...";

$lang['SERVICE_TABS_MAIN1'] = "Wait Less";
$lang['SERVICE_TABS_TEXT1'] = "Never wait again to get an appointment with your doctor! Your Medlanes doctor is avaiable to provide you with medical advice 24 hours a day, 7 days a week.";

$lang['SERVICE_TABS_MAIN2'] = "Pay Less";
$lang['SERVICE_TABS_TEXT2'] = "Healthcare is extremely expensive. Whether you have insurance or not, healthcare costs are exorbitant! Not only is paying for the doctor expensive, but you are having to take valuable time out of your busy day just to wait in the waiting room at the doctor's office. With Medlanes, medicine is affordable for everyone, and it is on your time!";

$lang['SERVICE_TABS_MAIN3'] = "Stress Less";
$lang['SERVICE_TABS_TEXT3'] = "Getting medical advice with Medlanes is extremely convenient! No more stress finding the time to make an appointment or schedule weeks in advance to fit in your schedule. No more worries on vacations! No more worries in the middle of the night! Medlanes is available anytime and from anywhere - on your smartphone and on the web!";

$lang['SERVICE_TABS_MAIN4'] = "Worry Less";
$lang['SERVICE_TABS_TEXT4'] = "Don't worry about you or your family's health. Medlanes has an extensive network of doctors of both general practitioners and specialists to give you peace of mind. No more worries! Your doctor is waiting on you, as opposed to you waiting on them!";

/*ALWAYS HERE*/

$lang['SERVICE_ALWAYS_HEADING1'] = "Always here for you, Wherever you may be!";
$lang['SERVICE_ALWAYS_HEADING2'] = "Get medical advice WHEN and WHERE you need it! Medlanes combines the convenience of an online service with the quality and reliability of a visit to your doctor! Simply download the Medlanes App or use the Medlanes Website to get answers to medical questions you may have - anytime, anywhere! Answer the questionnaire, upload documents relevant to your condition, submit your questions and Medlanes takes care of the rest! Our group of doctors is truly here to make your life easier!";

$lang['SERVICE_ALWAYS_HEAD1'] = "During Vacation";
$lang['SERVICE_ALWAYS_TEXT1'] = "It's difficult to find a doctor that speaks your language when you are travelling abroad, or in an unfamiliar area - use Medlanes to get answers to your questions!";

$lang['SERVICE_ALWAYS_HEAD2'] = "At Work";
$lang['SERVICE_ALWAYS_TEXT2'] = "Talking time off work to see a doctor during the day can be a tough decision. Medlanes eliminates the need to waste your valuable time at the doctor's office!";

$lang['SERVICE_ALWAYS_HEAD3'] = "At Night";
$lang['SERVICE_ALWAYS_TEXT3'] = "For all non-emergency questions it's almost impossible to get advice after 6pm - not with Medlanes!";

$lang['SERVICE_ALWAYS_HEAD4'] = "On the Weekend";
$lang['SERVICE_ALWAYS_TEXT4'] = "Finding a doctor on the weekend without having to wait at an Urgent Care or the ER is very difficult - with Medlanes, you never will wait again!";

$lang['SERVICE_ALWAYS_HEAD5'] = "For Convenience";
$lang['SERVICE_ALWAYS_TEXT5'] = "Waiting to get an appointment, the dreaded waiting room - there's no lost time when using Medlanes.";

$lang['SERVICE_ALWAYS_HEAD6'] = "Long Distance";
$lang['SERVICE_ALWAYS_TEXT6'] = "Don't settle for an under-qualified doctor because he is close, get advice from the right specialist from your home!";


// PRICE

$lang['SERVICE_PRICE_HEADING1'] = "Price &amp; use Cases";
$lang['SERVICE_PRICE_HEADING2'] = "Our mission at Medlanes is to make high quality healthcare affordable to all! Healthcare is extremely expensive, not only paying your doctor, but the time you waste actually going through the process of seeing your doctor. With Medlanes, medicine is affordable for everyone, and requires almost no time on your end! Your doctor is always at your fingertips!";

$lang['SERVICE_PRICE_1'] = "Qualified Answers";
$lang['SERVICE_PRICE_2'] = "Consultation/Diagnosis per Email includes up to 3 follow up questions by our board-certified United States based general practitioners or specialists.";
$lang['SERVICE_PRICE_3'] = "$29";

// USECASES REPLICATED FROM INDEX ONES

?>