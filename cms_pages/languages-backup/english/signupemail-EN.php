<?php 

$lang['SIGNUPEMAIL_SUBJECT'] = "Welcome to Medlanes!";
$lang['SIGNUPEMAIL_HEADLINE'] = "Thank you for signing up at Medlanes!";
$lang['SIGNUPEMAIL_MAIN'] = "Hello there and welcome to Medlanes! <br><br>Medlanes is the convenient, fast and time-saving way to get expert opinions from the best doctors around the clock. If you have paid the doctor's fee at the end of the checkout one of our doctors will get back to you soon. Otherwise, we will contact you once our new low-cost service is available.";
$lang['SIGNUPEMAIL_SIGNATURE'] = "In good health, <br>The Medlanes Team";
$lang['SIGNUPEMAIL_FOOTER'] = "Have a question or want to say hello? Just hit reply!";

?>