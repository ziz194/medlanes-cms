<?php 

$lang['DOCTORS_'] = "";


// HEADING
$lang['DOCTORS_HEADING'] = "Doctors of every speciality available around the clock.";

$lang['DOCTORS_HEADING2'] = "Doctor Network and Expert Advisers";
$lang['DOCTORS_HEADING3'] = "In partnership with our expert doctors, Medlanes has brought together the best and brightest medical practitioners from every specialty to better serve you";

// DOCTORS PANEL

$lang['DOCTORS_NAME1'] = "Dr. med. Jessica Männel";
$lang['DOCTORS_SPECIALIZATION1'] = "Family Medicine Specialist";

$lang['DOCTORS_NAME2'] = "Dr. med. Wolf Siepen";
$lang['DOCTORS_SPECIALIZATION2'] = "Orthopedics and Trauma Specialist";

$lang['DOCTORS_NAME3'] = "Dr. med. Kathrin Hamann";
$lang['DOCTORS_SPECIALIZATION3'] = "Family Medicine Specialist";

$lang['DOCTORS_NAME4'] = "Dr. med. Norbert Scheufele";
$lang['DOCTORS_SPECIALIZATION4'] = "Gynaecology and Obstetrics Specialist";

$lang['DOCTORS_NAME5'] = "Dr.rer.nat.Uta-Maria Weigel";
$lang['DOCTORS_SPECIALIZATION5'] = "Naturopathic Specialist";

$lang['DOCTORS_NAME6'] = "Christian Welsch";
$lang['DOCTORS_SPECIALIZATION6'] = "ENT Specialist";

$lang['DOCTORS_NAME7'] = "Stefan Paffrath";
$lang['DOCTORS_SPECIALIZATION7'] = "Psychologist";

$lang['DOCTORS_NAME8'] = "Beate Broermann";
$lang['DOCTORS_SPECIALIZATION8'] = "Psychologist";

$lang['DOCTORS_NAME9'] = "Dr. med. Joachim Nowak";
$lang['DOCTORS_SPECIALIZATION9'] = "Orthopedic Specialist";

$lang['DOCTORS_NAME10'] = "Dr. med. Ive Schaaf";
$lang['DOCTORS_SPECIALIZATION10'] = "Phlebology Specialist";


// OUR AREA OF EXPERTY PANEL

$lang['DOCTORS_EXPERTISE1'] = "Our Areas of Expertise";
$lang['DOCTORS_EXPERTISE2'] = "All doctors who work with Medlanes are carefully selected. Our doctors have years of experience in their field and are specially trained for online advice.";

$lang['DOCTORS_EXPERTISE_SUBHEAD1'] = "Family Medicine";
$lang['DOCTORS_EXPERTISE_SUBTEXT1'] = "Most questions are best answered by general physician, they are your guide through medicine!";

$lang['DOCTORS_EXPERTISE_SUBHEAD2'] = "Pediatrics";
$lang['DOCTORS_EXPERTISE_SUBTEXT2'] = "It's important to keep up to date with your child's health! Our pediatric doctors help you with advice any time you need it.";

$lang['DOCTORS_EXPERTISE_SUBHEAD3'] = "Cardiology";
$lang['DOCTORS_EXPERTISE_SUBTEXT3'] = "Lower the risk of a heart disease! Our cardiologists can diagnose heart rhythm disturbances and determine best ways how to manage your cardiac disease.";

$lang['DOCTORS_EXPERTISE_SUBHEAD4'] = "Gynecology";
$lang['DOCTORS_EXPERTISE_SUBTEXT4'] = "In Gynecology a trusting relationship between patient and physician is important. Our gynecologists do their best to ensure that you are comfortable.";

$lang['DOCTORS_EXPERTISE_SUBHEAD5'] = "Dermatology";
$lang['DOCTORS_EXPERTISE_SUBTEXT5'] = "Up to 90% of all skin problems can be successfully diagnosed using medical photo analysis. Our dermatologists evaluate your skin problems and give treatment advice.";

$lang['DOCTORS_EXPERTISE_SUBHEAD6'] = "Orthopedics";
$lang['DOCTORS_EXPERTISE_SUBTEXT6'] = "Anytime there is a suspected traumatic or repetitive motion injury to a bone, joint or nerve one of our orthopedic doctors is your best choice for treatment.";

$lang['DOCTORS_EXPERTISE_SUBHEAD7'] = "Gastroenterology";
$lang['DOCTORS_EXPERTISE_SUBTEXT7'] = "When you first notice troublesome signs or symptoms in your digestive system, such as constipation, diarrhea or abdominal pain, you might ask one of our gastroenterologists for qualified medical advice.";

$lang['DOCTORS_EXPERTISE_SUBHEAD8'] = "Psychotherapy";
$lang['DOCTORS_EXPERTISE_SUBTEXT8'] = "Mental stress is often only noticeable through physical illnesses. Do not hesitate to ask our qualified psychotherapists for advice.";
?>
