<?php 

$lang['PAYMENTEMAIL_SUBJECT'] = "Wichtig: Ihre Frage an Medlanes";
$lang['PAYMENTEMAIL_HEADLINE'] = "Vielen Dank, dass Sie unseren Service nutzen";
$lang['PAYMENTEMAIL_MAIN'] = "Wir haben Ihre Frage erhalten! <br><br> Wir verbinden Sie nun mit einem zuständigen Arzt oder Experten und werden Ihre Frage so schnell wie möglich beantworten. <br><br>Sollte jedoch wider Erwarten kein Arzt zur Beantwortung Ihrer Frage verfügbar sein, erstatten wir umgehend Ihre Zahlung.";
$lang['PAYMENTEMAIL_SIGNATURE'] = "Herzliche Grüße, <br>Das Medlanes Team";
$lang['PAYMENTEMAIL_FOOTER'] = "Haben Sie Fragen oder Anregungen? Antworten Sie einfach auf diese E-Mail!";

?>