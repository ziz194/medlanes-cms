<?php 
$lang['SIGNUPEMAIL_SUBJECT'] = "Willkommen bei Medlanes!";
$lang['SIGNUPEMAIL_HEADLINE'] = "Danke für Ihr Interesse an Medlanes.";
$lang['SIGNUPEMAIL_MAIN'] = "Hallo und willkommen bei Medlanes! <br><br>Medlanes ist ein bequemer, schneller und kostengünstiger Service, der Ihnen Expertenmeinungen von hochqualifizierten Ärzten zu allen medizinischen Fragen bietet - überall und rund um die Uhr. <br><br>Nach Zahlung einer geringen Servicegebühr wird sich einer unserer Experten in Kürze bei Ihnen melden. <br> Wir informieren Sie gerne, sobald unser günstiger Einsteigerservice für Sie verfügbar ist.";
$lang['SIGNUPEMAIL_SIGNATURE'] = "Herzliche Grüße, <br>Das Medlanes Team";
$lang['SIGNUPEMAIL_FOOTER'] = "Haben Sie Fragen oder Anregungen? Antworten Sie einfach auf diese E-Mail!";
?>