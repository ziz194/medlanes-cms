<?php 

$lang['ABOUT_'] = "";

// HEADING
$lang['ABOUT_HEADING'] = "<h1><b>Wir bringen Medizin online!</b>Und machen es bezahlbar <br> für jedermann.</h1>";

// THREE POINTS

$lang['ABOUT_SUBHEADING1'] = "Das Unternehmen";
$lang['ABOUT_SUBTEXT1'] = "Die Medlanes GmbH ist ein Unternehmen mit Sitz in Berlin, das sich auf Telemedizin und medizinische Expertenmeinung spezialisiert hat. Seit unserer Gründung in 2013, haben wir den herkömmlichen Arztbesuch revolutioniert. Für unsere Innovationen im Gesundheitswesen wurden wir bereits mit mehreren Förderpreisen, unteranderem von Microsoft und der Bayer AG, ausgezeichnet.  Unsere Ärzte sind jederzeit verfügbar und stellen die Gesundheit unserer Patienten immer in den Vordergrund. Keine langen Wege mehr zur Arztpraxis oder wochenlanges Warten auf einen Termnin bei kleinen Beschwerden. Unsere Ärzte sind immer für Sie da und beantworten Ihre Fragen innerhalb einer Stunde.";

$lang['ABOUT_SUBHEADING2'] = "Unsere Vision";
$lang['ABOUT_SUBTEXT2'] = "Medlanes wurde aus einem einfachen Grund gegründet. Wir möchten die bestmögliche Gesundheitsversorgung ermöglichen und vermeiden dabei die Nachteile eines üblichen Arztbesuches. Wir bieten sofortigen Zugang zur medizinischen Expertenmeinung, überall, jederzeit und zu einem erschwinglichen Preis. Wir glauben fest daran, dass der Zugang zur bestmöglichen gesundheitlichen Versorgung nicht durch Versicherungsmitgliedschaften, finanzielles Budget, Standort oder mangelnde Zeit limitiert werden sollte. Medlanes revolutioniert das Gesundheitswesen und ermöglicht eine Gesundheitsversorgung ohne Grenzen.";

$lang['ABOUT_SUBHEADING3'] = "Unsere Lösung";
$lang['ABOUT_SUBTEXT3'] = "Wir sind stolz Ihnen die Medlanes App zu präsentieren! Die Medlanes App ermöglicht es, Ihren Arzt ständig dabei zu haben. Wir sind für Sie verfügbar, egal wo Sie sich befinden und wann immer Sie einen ärztlichen Rat benötigen. Wir stehen Ihnen mit unserem einfachen, schnellen und günstigen Service für medizinische Fragen rund um die Uhr zur Verfügung!";
?>

