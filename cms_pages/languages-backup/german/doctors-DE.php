<?php 

$lang['DOCTORS_'] = "";


// HEADING
$lang['DOCTORS_HEADING'] = "Ärzte jeder Fachrichtung verfügbar rund um die Uhr.";

$lang['DOCTORS_HEADING2'] = "Partnerärzte und Ärztebeirat";
$lang['DOCTORS_HEADING3'] = "Wir entwickeln Medlanes zusammen mit Ärzten aus allen Fachrichtungen. Wir setzen auf das Fachwissen und die Erfahrung unserer Ärzte, daher wird unser Ärzteteam stetig erweitert.";

// DOCTORS PANEL

$lang['DOCTORS_NAME1'] = "Dr. med. Jessica Männel";
$lang['DOCTORS_SPECIALIZATION1'] = "Fachärztin für Allgemeinmedizin";

$lang['DOCTORS_NAME2'] = "Dr. med. Wolf Siepen";
$lang['DOCTORS_SPECIALIZATION2'] = "Facharzt für orth. Chirugie und Traumatologie";

$lang['DOCTORS_NAME3'] = "Dr. med. Kathrin Hamann";
$lang['DOCTORS_SPECIALIZATION3'] = "Fachärztin für Allgemeinmedizin";

$lang['DOCTORS_NAME4'] = "Dr. med. Norbert Scheufele";
$lang['DOCTORS_SPECIALIZATION4'] = "Facharzt für Frauenheilkunde und Geburtshilfe";

$lang['DOCTORS_NAME5'] = "Dr. rer. nat. Uta-Maria Weigel";
$lang['DOCTORS_SPECIALIZATION5'] = "Heilpraktikerin";

$lang['DOCTORS_NAME6'] = "Christian Welsch";
$lang['DOCTORS_SPECIALIZATION6'] = "Facharzt für HNO-Heilkunde";

$lang['DOCTORS_NAME7'] = "Stefan Paffrath";
$lang['DOCTORS_SPECIALIZATION7'] = "Dipl. Psychologe";

$lang['DOCTORS_NAME8'] = "Beate Broermann";
$lang['DOCTORS_SPECIALIZATION8'] = "Dipl. Psychologin";

$lang['DOCTORS_NAME9'] = "Dr. med. Joachim Nowak";
$lang['DOCTORS_SPECIALIZATION9'] = "Facharzt für Orthopädie";

$lang['DOCTORS_NAME10'] = "Dr. med. Ive Schaaf";
$lang['DOCTORS_SPECIALIZATION10'] = "Fachärztin für Phlebologie";


// OUR AREA OF EXPERTY PANEL

$lang['DOCTORS_EXPERTISE1'] = "Die besten Ärzte für Sie";
$lang['DOCTORS_EXPERTISE2'] = "Alle Ärzte, die mit Medlanes zusammenarbeiten, sind sorgfältig ausgewählt. Unsere Ärzte haben jahrelange Erfahrung in Ihrem Spezialgebiet und werden speziell für die Beratung geschult.";

$lang['DOCTORS_EXPERTISE_SUBHEAD1'] = "Allgemeinmedizin";
$lang['DOCTORS_EXPERTISE_SUBTEXT1'] = "Die meisten Fragen können von einem Allgemeinmediziner beantwortet werden. Unsere Fachärzte für Allgemeinmedizin beraten Sie gerne!";

$lang['DOCTORS_EXPERTISE_SUBHEAD2'] = "Kinder- und Jugendmedizin";
$lang['DOCTORS_EXPERTISE_SUBTEXT2'] = "Es ist wichtig, bei Gesundheitsfragen bezüglich Ihrer Kinder auf dem neusten Stand zu sein! Unsere Kinderärzte helfen Ihnen gerne weiter.";

$lang['DOCTORS_EXPERTISE_SUBHEAD3'] = "Herz und Kreislauf Erkrankungen";
$lang['DOCTORS_EXPERTISE_SUBTEXT3'] = "Verringern Sie das Risiko einer Herzkrankheit! Lassen Sie sich von unseren Kardiologen zum Thema Herz und Kreislauf beraten!";

$lang['DOCTORS_EXPERTISE_SUBHEAD4'] = "Gynäkologie und Geburtshilfe";
$lang['DOCTORS_EXPERTISE_SUBTEXT4'] = "In der Gynäkologie ist eine vertrauensvolle Beziehung zwischen Patient und Arzt sehr wichtig. Unsere Gynäkologen tun ihr Bestes, um sicherzustellen, dass Sie sich wohl fühlen und Ihre Fragen schnellstmöglich beantwortet werden.";

$lang['DOCTORS_EXPERTISE_SUBHEAD5'] = "Dermatologie";
$lang['DOCTORS_EXPERTISE_SUBTEXT5'] = "Bis zu 90% aller Hautprobleme können mittels medizinischer Fotoanalyse erfolgreich diagnostiziert werden. Unsere Dermatologen bewerten Ihre Hautprobleme und beraten Sie gerne.";

$lang['DOCTORS_EXPERTISE_SUBHEAD6'] = "Orthopädie";
$lang['DOCTORS_EXPERTISE_SUBTEXT6'] = "Immer dann, wenn ein Verdacht auf eine Verletzung von Knochen, Gelenken oder Nerven vorliegt, ist einer unserer Orthopäden die beste Wahl für qualifizierte medizinische Informationen.";

$lang['DOCTORS_EXPERTISE_SUBHEAD7'] = "Gastroenterologie";
$lang['DOCTORS_EXPERTISE_SUBTEXT7'] = "Wenn Sie Beschwerden in Ihrem Verdauungssystem wie Verstopfung, Durchfall oder Bauchschmerzen haben, kontaktieren Sie einen unserer Gastroenterologen für eine qualifizierte medizinische Expertenmeinung.";

$lang['DOCTORS_EXPERTISE_SUBHEAD8'] = "Psychotherapie";
$lang['DOCTORS_EXPERTISE_SUBTEXT8'] = "Krankheiten sind oft ein Hilfeschrei der Seele. Seelische Belastungen werden manchmal erst durch körperliche Beschwerden bemerkbar. Scheuen Sie sich nicht davor, unsere qualifizierten Psychotherapeuten um Rat zu fragen.";
?>
