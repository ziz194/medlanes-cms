
<?php 



// HEADER TEXT

$lang['HEADER_1'] = "Geprüfte medizinische Informationen von Fachärzten für Sie!";
$lang['HEADER_2'] = "Unser Support steht Ihnen zur Seite: <b>0800 / 765 43 43</b>";

$lang['HEADER_LINK1'] = '<a href="service.php?lang=de">Unser Service</a>';
$lang['HEADER_LINK2'] = '<a href="#">Krankheiten<img style="margin-left:10px;" src="images/triangle.png" alt="#"/></a>';
$lang['HEADER_LINK3'] = '<a href="doctors.php?lang=de">Ärzte</a>';
$lang['HEADER_LINK4'] = '<a href="faq.php?lang=de">FAQ</a>';
$lang['HEADER_LINK5'] = '<a href="contact.php?lang=de">Support</a>';

$lang['HEADER_ADQ'] = 'add-question.php?lang=de';
 
// PAGE TITLES



$lang['INDEX_H'] = 'Qualifizierte medizinische <br> Informationen & Einschätzungen <br> von führenden Fachärzten';
$lang['INDEX_H1'] = 'Alle 9 Sekunden wird eine Fragen beantwortet. <br> Seine Sie sich sicher mit 100% Zufriedenheitsgarantie.';




$lang['TITLE_IMPRINT'] = "Medlanes | Qualifizierte medizinische Informationen - Expertenmeinungen - Zweitmeinungen | Impressum";
$lang['TITLE_INDEX'] = "Medlanes | Qualifizierte medizinische Informationen - Expertenmeinungen - Zweitmeinungen | Home";
$lang['TITLE_PAYMENT'] = "Medlanes | Qualifizierte medizinische Informationen - Expertenmeinungen - Zweitmeinungen | Bezahlung";
$lang['TITLE_PRIVACY'] = "Medlanes | Qualifizierte medizinische Informationen - Expertenmeinungen - Zweitmeinungen | Datenschutz";
$lang['TITLE_TERMS'] = "Medlanes | Qualifizierte medizinische Informationen - Expertenmeinungen - Zweitmeinungen | Allgemeine Geschäftsbedingungen";
$lang['TITLE_CONTACT'] = "Medlanes | Qualifizierte medizinische Informationen - Expertenmeinungen - Zweitmeinungen | Kontakt";
$lang['TITLE_THANKYOU'] = "Medlanes | Qualifizierte medizinische Informationen - Expertenmeinungen - Zweitmeinungen | Danke";
$lang['TITLE_ADD_QUESTION'] = "Medlanes | Qualifizierte medizinische Informationen - Expertenmeinungen - Zweitmeinungen | Danke";
$lang['TITLE_SERVICE'] = "Medlanes | Qualifizierte medizinische Informationen - Expertenmeinungen - Zweitmeinungen | Danke";
$lang['TITLE_FAQ'] = "Medlanes | Qualifizierte medizinische Informationen - Expertenmeinungen - Zweitmeinungen | Danke";
$lang['TITLE_DOCTOR'] = "Medlanes | Qualifizierte medizinische Informationen - Expertenmeinungen - Zweitmeinungen | Danke";

/*FOOTER LINKS*/

$lang['FOOTER1'] = '<a href="index.php?lang=de">Medlanes Mainpage</a>';
$lang['FOOTER2'] = '<a href="imprint.php?lang=de">Über Medlanes / Impressum</a>';
$lang['FOOTER3'] = '<a href="contact.php?lang=de">Support</a>';
$lang['FOOTER4'] = '<a href="terms.php?lang=de"> Allgemeine Geschäftsbedingungen</a>';
$lang['FOOTER5'] = '<a href="privacy.php?lang=de"> Datenschutz</a>';

$lang['FOOTER8'] = '<a href="#"> Presse</a>';
$lang['FOOTER9'] = '<a href="#"> Karriere</a>';
$lang['FOOTER10'] = '<p>Unterstützer <br> und Partner:</p>';
$lang['FOOTER11'] = '&copy; Medlanes GmbH - Alle Rechte vorbehalten.';


$lang['INDEX_1'] = "Schnelle Antworten von führenden Fachärzten";
$lang['INDEX_2'] = "Unsere Experten beantworten Ihre Fragen. Alle <b>9 Sekunden</b> wird eine neue Frage beantwortet. Wir bieten Ihnen eine <b>100% Zufriedenheitsgarantie.</b>";
$lang['INDEX_3'] = "Ihre Frage an unsere Fachärzte";
$lang['INDEX_4'] = "Ihre E-Mail-Adresse*";
$lang['INDEX_5'] = "Antwort jetzt erhalten";
$lang['INDEX_6'] = "*Ihre Daten sind verschlüsselt und werden nicht weitergegeben.";
$lang['INDEX_7'] = "Konzept behannt aus:";
$lang['INDEX_8'] = "In den Nachrichten";
$lang['INDEX_9'] = "[...] 83% der befragten Regierungen unterstützt die Nutzung von Mobiltelefonen für Gesundheitsdieste.";
$lang['INDEX_10'] = "World Health Organisation";
$lang['INDEX_11'] = "Unsere Kunden über uns";
$lang['INDEX_12'] = "“Die App erleichtert meinen Alltag erheblich. Im Internet habe ich schon 1-2x falsche Informationen bekommen und es macht wenig Sinn meinen Arzt jedesmal anzurufen.“";
$lang['INDEX_13'] = "Xenia, 27 <br> Berlin";
$lang['INDEX_14'] = "Unsere Ärzte";
$lang['INDEX_15'] = "Dr. med. K. Hamann";
$lang['INDEX_16'] = "Allgemeinärztin mit 12 Jahren Praxiserfahrung.";
$lang['INDEX_17'] = "Bewertung 4.9 / 5";
$lang['INDEX_18'] = "";
$lang['INDEX_19'] = "";

$lang['FOOTER_link1'] = '<a href="index.php?lang=de">Home</a>';
$lang['FOOTER_link2'] = '<a href="service.php?lang=de">Unser Service</a>';
$lang['FOOTER_link3'] = '<a href="doctors.php?lang=de">Ärzte</a>';
$lang['FOOTER_link4'] = '<a href="faq.php?lang=de">FAQ</a>';
$lang['FOOTER_link5'] = '<a href="contact.php?lang=de">Kontakt</a>';
// 2nd menu
$lang['FOOTER_link6'] = '<a href="about.php?lang=de">Über Medlanes</a>';
$lang['FOOTER_link7'] = '<a href="career.php?lang=de">Karriere</a>';
// 3rd menu
$lang['FOOTER_link8'] = '<a href="terms.php?lang=de">AGBs</a>';
$lang['FOOTER_link9'] = '<a href="privacy.php?lang=de">Datenschutz</a>';
$lang['FOOTER_link10'] = '<a href="imprint.php?lang=de">Impressum</a>';
?>