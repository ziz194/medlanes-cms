<?php 

$lang['THANKYOU_'] = "";


// HEADING

$lang['THANKYOU_HEADING1'] = "Danke";
$lang['THANKYOU_HEADING2'] = "Sie müssen nichts weiter tun! Einer unserer Ärzte wird sich in Kürze bei Ihnen melden.";

// LEFT SIDE IF YOU HAVE ANY QUESTIONS

$lang['THANKYOU_FAQ1'] = "Wenn Sie weitere Fragen haben...";
$lang['THANKYOU_FAQ2'] = "Unser Kundendienst ist jederzeit für Sie da! Kontaktieren Sie uns.";


// RIGHT SIDE LEARN MORE

$lang['THANKYOU_LEARN_HEADING1'] = "Möchten Sie mehr erfahren?";
$lang['THANKYOU_LEARN_HEADING2'] = "Hier erfahren Sie mehr über den Service von Medlanes und wie wir Ihnen weiterhelfen können.";
$lang['THANKYOU_LEARN_LINK'] = "Unser Service";

$lang['THANKS_LINK_TO_SERV'] = '<a href="service?lang=de">Lesen Sie hier mehr</a>';
?>