<?php 
$lang['HEADER_ALLERGIE'] = "Allergien";
$lang['HEADER_SLEEP_DISORDER'] = "Schlafstörungen";
$lang['HEADER_COLD'] = "Erkältung, Husten, Grippe";
$lang['HEADER_DIABETES'] = "Diabetes";
$lang['HEADER_DIGESTIVE'] = "Verdauungsstörungen";


$lang['HEADER_EYE'] = "Augenerkrankungen";
$lang['HEADER_EAR'] = "Ohrenschmerzen";
$lang['HEADER_MENTAL'] = "Psychische Erkrankungen";
$lang['HEADER_SEXUAL'] = "Sexualität";
$lang['HEADER_DIET'] = "Ernährung";
$lang['HEADER_INFECTION'] = "Infektionskrankheiten";
$lang['HEADER_PAIN'] = "Schmerzen";
$lang['HEADER_SKIN'] = "Haare, Haut und Nägel" ;

$lang['HEADER_ARITHIS'] = "Arthritis" ;
$lang['HEADER_COPD'] = "Asthma und COPD" ;
$lang['HEADER_CHILDREN'] = "Kinderkrankheiten" ;
$lang['HEADER_ENDOCRINE'] = "Hormonelle Störungen" ;
$lang['HEADER_INJURIES'] = "Verletzungen" ;
$lang['HEADER_MEN'] = "Männerkrankheiten" ;
$lang['HEADER_ORAL'] = "Zahnerkrankungen" ;
$lang['HEADER_RESPIRATORY'] = "Atemwegserkrankungen" ;
$lang['HEADER_STD'] = "Geschlechtskrankheiten" ;
$lang['HEADER_WOMEN'] = "Frauenkrankheiten" ;


// Conditions--------------------

$lang['FEVER_CONTENT1'] ="Beschreibung ";
$lang['FEVER_CONTENT2'] ="Anzeichen &amp; Symptome";
$lang['FEVER_CONTENT3'] ="Ursachen";
$lang['FEVER_CONTENT4'] ="Diagnose";
$lang['FEVER_CONTENT5'] ="Behandlung";
$lang['FEVER_CONTENT6'] ="Komplikationen";







//Tests Template 

$lang['TEST_CONTENT1'] ="Beschreibung" ;
$lang['TEST_CONTENT2'] ="Testzwecke" ;
$lang['TEST_CONTENT3'] ="Normalbereiche";
$lang['TEST_CONTENT4'] ="Ergebnisse";


//Medication_name Template 
$lang['MED_NAME_CONTENT1'] ="Übersicht" ;
$lang['MED_NAME_CONTENT2'] ="Wichtige Informationen" ;




//Medication_type Template 


$lang['MED_TYPE_CONTENT1'] ="Übersicht" ;
$lang['MED_TYPE_CONTENT2'] ="Beispiele" ;
$lang['MED_TYPE_CONTENT3'] ="Anwendungen";


//Side bar 


$lang['SEXUAL_SIDEBAR_BUTTON'] ="Fragen Sie jetzt einen Arzt";




$lang['ASK'] = 'Fragen Sie einen Arzt' ;
$lang['DSBOX1_DOCTOR_img'] = '<img src="./images/docs/Doctor1.png" id="doc_img" alt="Sidebar arzt bild"/>';
$lang['DSBOX1_DOCTOR_txt'] = 'Dr. Thomas Penning';
$lang['DSBOX1_SP_img'] = '<img src="./images/family_medicine.png" id="specialty_img" width="12px" height="12px">';
$lang['DSBOX1_SP_txt'] = 'Allgemeinmedizin';
$lang['DSBOX1_EXP_h'] = 'Erfahrungen';
$lang['DSBOX1_EXP_txt'] = '23 years of practice';
$lang['DSBOX1_RATING_h'] = 'Rating';
$lang['DSBOX1_RATING_txt'] = '4.9 (147 Ratings)';




 ?>