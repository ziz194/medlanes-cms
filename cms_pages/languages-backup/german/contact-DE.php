<?php 

$lang['CONTACT_'] = "Kontakt";

// CONTACT MAIN HEADING
$lang['CONTACT_HEADING'] = "Fragen? <br> Kontaktieren Sie uns noch heute! ";

// CONTACT EXLANATION

$lang['CONTACT_EXPLAIN1'] = "Unser Team steht für Sie bereit";
$lang['CONTACT_EXPLAIN2'] = "Medlanes schreibt Kundenbetreuung groß. Wir stehen Ihnen jederzeit mit Rat und Tat zur Seite! Kontaktieren Sie uns - wir freuen uns auf Ihre Nachricht.";

$lang['CONTACT_EXPLAIN2'] = "Telefon und E-Mail Support";
$lang['CONTACT_EXPLAIN2'] = "Über 90% der Anfragen beantworten wir innerhalb von 24 Stunden.";


// PHONE BOX
$lang['CONTACT_PHONE1'] = "Telefon";
$lang['CONTACT_PHONE2'] = "0800 - 765 43 43";
$lang['CONTACT_PHONE3'] = "gebührenfrei 24 Stunden an 365 Tagen im Jahr";
$lang['CONTACT_PHONE4'] = "Rückrufservice";
$lang['CONTACT_PHONE5'] = "E-Mail";
// EMAIL BOX
$lang['CONTACT_EMAIL1'] = "E-Mail-Support";
$lang['CONTACT_EMAIL2'] = "info@medlanes.com";
$lang['CONTACT_EMAIL3'] = "8:00 bis 18:00 Uhr";

// CONTACT FORM
$lang['CONTACT_FORM_HEADING1'] = "Kontaktformular";
$lang['CONTACT_FORM_HEADING2'] = "Schreiben Sie uns! Wir freuen uns über Ihr Feedback und Ihre Anregungen.";

$lang['CONTACT_FORM_NAME'] = "Vorname";
$lang['CONTACT_FORM_LASTNAME'] = "Name";
$lang['CONTACT_FORM_EMAIL'] = "E-Mail";
$lang['CONTACT_FORM_MESSAGE'] = "Anfrage";
$lang['CONTACT_FORM_SEND'] = "Senden";

?>