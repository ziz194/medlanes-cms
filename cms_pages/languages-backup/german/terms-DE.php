<?php 

$lang['FULL_TERMS'] = 'Allgemeine Geschäftsbedingungen';

// TERMS HEADING

$lang['TERMS_HEADING1'] = 'Allgemeine Geschäftsbedingungen';
$lang['TERMS_HEADING2'] = '<b>Stand: Jan 2015</b>';

// TERMS PARAG


$lang['TERMS_1'] ='
				<p><b>§ 1 Allgemeines</b></p>		
				<p>1.	Die Nutzung des Leistungsangebotes von Medlanes richtet sich nach den vorliegenden Allgemeinen Geschäftsbedingungen. Abweichende Vereinbarungen bedürfen der Schriftform</p>	
				<div class="two" style="width:100%;float:left;margin-top:20px;margin-bottom:20px;">
					<p>Medlanes GmbH, eingetragen im Handelsregister des Amtsgerichts Charlottenburg (Berlin) unter der Registernummer HRB159023B, </p>
					<p>vertreten durch den Geschäftsführer Dr. Emil Kendziorra, </p>
					<p>Oranienstr. 185, 10999 Berlin</p>
					<p>Telefon: +49 (0)30 577 004 20</p>
					<p>Telefax: +49 (0)6151 62749 799 </p>
					<p>E-Mail: info(at)medlanes.com </p>
					<p>Umsatzsteuer-Identifikationsnummer gemäß § 27a Umsatzsteuergesetz: DE295684112</p>
';
$lang['TERMS_2'] ='
				<p><b>§ 2 Leistungsumfang</b></p>
				<p>1.	Medlanes ist eine mobile und online verfügbare Gesundheitsplattform, die den Nutzern die Möglichkeit einer schnellen ärztlichen Information über das Internet verschafft. Der Nutzer kann über die Plattform unter anderem Symptome, Medikamenteneinnahmen, Gesund-heitsangaben, Fotos oder allgemeine Fragen einstellen und online sein Informationsinteresse weiterleiten. Der Nutzer erhält wahlweise eine automatisch erstellte, eine ärztlich überprüfte oder eine ärztlich erstellte Information zurück. </p>
				<p>2.	Medlanes bietet über die auf der Internetseite www.medlanes.com hinausgehende Leistungsbeschreibung keine medizinischen Leistungen an. Die Nutzung von Medlanes kann einen traditionellen Arztbesuch nicht ersetzen. Suchen Sie einen Arzt vor Ort oder in dringenden Fällen eine Notfallambulanz auf. Für die medizinische Versorgung über das Internet gelten besondere berufsrechtliche Bestimmungen und Einschränkungen. Das Leistungsangebot ist daher auf die aktuellen rechtlichen Möglichkeiten begrenzt. Besondere Hinweise bei der Leistungsbeschreibung und Leistungserbringung sind verbindlich.</p>
';
$lang['TERMS_3'] ='
				<p><b>§ 3 Registrierung</b></p>
				<p>1.	Die Nutzung von Medlanes erfordert die Einrichtung eines nutzerspezifischen Accounts, für den eine Registrierung erforderlich ist. Die Registrierung setzt voraus, dass der Nutzer sämtliche in der Anmeldemaske abgefragten Daten wahrheitsgemäß und vollständig an Medlanes übermittelt. Bei der Registrierung muss der Nutzer mindestens 18 Jahre alt und unbeschränkt geschäftsfähig sein. </p>
				<p>2.	Nach der Registrierung erhält der Nutzer eine E-Mail mit einem Bestätigungslink an eine von ihm während der Registrierung angegebene E-Mail-Adresse. Dieser Link muss zur Freischaltung des Accounts angeklickt werden. Wenn der Account nicht binnen zwei Wochen freigeschaltet wird, wird der Registrierungsvorgang verworfen und die eingegebenen Daten werden gelöscht. Mit der Freischaltung des nutzerspezifischen Accounts kommt eine Nutzungsvereinbarung nach Maßgabe der Allgemeinen Geschäftsbedingungen zustande.</p>
				<p>3.	Es besteht kein Rechtsanspruch auf die Inanspruchnahme der Leistungen von Medlanes. Medlanes ist berechtigt, die Registrierung ohne Angabe von Gründen zu verweigern.</p>
';
$lang['TERMS_4'] ='
				<p><b>§ 4 Widerrufsbelehrung </b></p>
				<p>Widerrufsrecht:</p>
				<p>Sie haben das Recht, binnen vierzehn Tagen ohne Angabe von Gründen diesen Vertrag zu widerrufen.</p>
				<p>Die Widerrufsfrist beträgt vierzehn Tage ab dem Tag des Vertragsabschlusses.</p>
				<p>Um Ihr Widerrufsrecht auszuüben, müssen Sie uns </p>
				<div class="two" style="width:100%;float:left;margin-top:20px;margin-bottom:20px;">
					<p>Medlanes GmbH, vertreten durch den Geschäftsführer Dr. Emil Kendziorra, </p>
					<p>Oranienstr. 185, 10999 Berlin</p>
					<p>Telefon: +49 (0)30 577 004 20</p>
					<p>Telefax: +49 (0)6151 62749 799 </p>
					<p>E-Mail: info(at)medlanes.com </p>
				</div>
				<p>mittels einer eindeutigen Erklärung (z.B. ein mit der Post versandter Brief, Telefax oder E-Mail) über Ihren Entschluss, diesen Vertrag zu widerrufen, informieren.</p>
				<p>Zur Wahrung der Widerrufsfrist reicht es aus, dass Sie die Mitteilung über die Ausübung des Widerrufsrechts vor Ablauf der Widerrufsfrist absenden.</p>
				<p>Folgen des Widerrufs:</p>
				<p>Wenn Sie diesen Vertrag widerrufen, haben wir Ihnen alle Zahlungen, die wir von Ihnen erhalten haben, unverzüglich und spätestens binnen vierzehn Tagen ab dem Tag zurückzuzahlen, an dem die Mitteilung über Ihren Widerruf dieses Vertrags bei uns eingegangen ist. Für diese Rückzahlung verwenden wir dasselbe Zahlungsmittel, das Sie bei der ursprünglichen Transaktion eingesetzt haben, es sei denn, mit Ihnen wurde ausdrücklich etwas anderes vereinbart; in keinem Fall werden Ihnen wegen dieser Rückzahlung Entgelte berechnet. </p>
				<p>Haben Sie verlangt, dass die Dienstleistungen während der Widerrufsfrist beginnen sollen, so haben Sie uns seinen angemessenen Betrag zu zahlen, der dem Anteil der bis zu dem Zeitpunkt, zu dem Sie uns von der Ausübung des Widerrufsrechts hinsichtlich dieses Vertrages unterrichten, bereits erbrachten Dienstleistungen im Vergleich zum Gesamtumfang der im Vertrag vorgesehenen Dienstleistungen entspricht.</p>
				<p>- Ende der Widerrufsbelehrung-</p>
';
$lang['TERMS_5'] ='
				<p><b>§ 5 Informationspflichten</b></p>
				<p>1.	Ein außergerichtliches Beschwerde- oder Rechtsbehelfsverfahren, dem Medlanes unterworfen ist, gibt es nicht.</p>
				<p>2.	Alle weiteren Informationen zu Medlanes, dem Angebot und der Abwicklung kann aus den Darstellungen auf www.medlanes.com entnommen werden.</p>
				<p>3.	Bezüglich der technischen Schritte zum Vertragsschluss ist § 3 AGB zu beachten. </p>
				<p>4.	Der Nutzer kann den Vertragstext abspeichern, indem er durch die Funktion seines Browsers "Speichern unter" die betreffende Internetseite auf seinem Computer sichert. Durch die Druckfunktion seines Browsers hat er zudem die Möglichkeit, den Vertragstext auszudrucken. Medlanes selbst speichert die Vertragstexte und macht dem Nutzer diese auf Wunsch per Email oder per Post zugänglich. </p>
				<p>5.	Seine Eingaben kann der Nutzer während der Registrierung jederzeit korrigieren, indem er den Button "Zurück" im Browser wählt und dann die entsprechende Änderung vornimmt. Durch Schließen des Webbrowsers kann der Nutzer die Registrierung jederzeit abbrechen. </p>
				<p>6.	Die für den Vertragsabschluss zur Verfügung stehende Sprache ist ausschließlich Deutsch. </p>
				<p>7.	Wir haben uns keinem besonderen Verhaltenskodex (Regelwerk) unterworfen.</p>
';
$lang['TERMS_6'] ='
				<p><b>§ 6 Nutzungerechte</b></p>
				<p>1.	Dem Nutzer stehen ausschließlich die nach diesen Allgemeinen Geschäftsbedingungen eingeräumten Rechte an dem Internetangebot zu. </p>
				<p>2.	Die über die Onlineplattform von Medlanes veröffentlichten Inhalte, Informationen, Bilder, Videos, Datenbanken sind grundsätzlich urheberrechtlich geschützt und in der Regel Eigentum oder lizensiert von Medlanes.</p>
				<p>3.	Die Inhalte auf der Onlineplattform dürfen nur für persönliche und nicht für kommerzielle Zwecke genutzt oder vervielfältigt werden. Eine Weitergabe der Inhalte ist ohne ausdrückliche Zustimmung von Medlanes untersagt.</p>
';
$lang['TERMS_7'] ='
				<p><b>§ 7 Nutzerdaten</b></p>
				<p>1.	Medlanes erhebt und nutzt für die Abwicklung der zwischen dem Nutzer und Medlanes geschlossenen Verträge die angegebenen Daten des Nutzers. Ohne die Einwilligung des Nutzers werden keine Daten erhoben, verarbeitet, weitergegeben oder genutzt. Die mit Einwilligung erhobenen Daten werden nur verarbeitet oder genutzt soweit dies für die Abwicklung des Vertragsverhältnisses und zur Leistungserfüllung erforderlich ist. </p>
				<p>2.	Der Nutzer hat jederzeit und im Rahmen der Verfügbarkeit der Onlineplattform die Möglichkeit, die von ihm gespeicherten Daten in seinem Account abzurufen, zu ändern oder zu löschen.</p>
				<p>3.	Im Übrigen wird auf die unter der Schaltfläche „Datenschutz“ abrufbare Datenschutzerklärung verwiesen.</p>
';
$lang['TERMS_8'] ='
				<p><b>§ 8 Verfügbarkeit</b></p>
				<p>1.	Das Leistungsangebot steht in der Regel 24 Stunden am Tag zur Verfügung. Hiervon ausgenommen sind die Zeiten, in denen Datensicherungsarbeiten vorgenommen und Systemwartungs- oder Programmpflegearbeiten am System oder der Datenbank durchgeführt werden. Medlanes wird die hieraus entstehenden möglichen Störungen möglichst gering halten. </p>
';
$lang['TERMS_9'] ='
				<p><b>§ 9 Haftung</b></p>
				<p>1.	Eine Haftung von Medlanes auf Schadensersatz, insbesondere wegen Verzugs, Nichterfüllung, Schlechterfüllung oder unerlaubter Handlung besteht nur bei Verletzung wesentlicher Vertragspflichten, auf deren Erfüllung in besonderem Maße vertraut werden durfte. Im Übrigen ist eine Haftung von Medlanes ausgeschlossen, es sei denn, es bestehen zwingende gesetzliche Regelungen. Der Haftungsausschluss gilt nicht für Vorsatz und grobe Fahrlässigkeit. </p>
				<p>2.	Medlanes haftet nur für vorhersehbare Schäden. Die Haftung für mittelbare Schäden, insbesondere Mangelfolgeschäden, unvorhersehbare Schäden oder untypische Schäden sowie entgangenen Gewinn ist ausgeschlossen. Gleiches gilt für die Folgen von Arbeitskämpfen, zufälligen Schäden und höherer Gewalt.</p>
				<p>3.	Die vorstehenden Haftungsbeschränkungen gelten für sämtliche vertraglichen und nichtvertraglichen Ansprüche.</p>
';
$lang['TERMS_10'] ='
				<p><b>§ 10 Pflichten des Nutzers</b></p>
				<p>1.	Der Nutzer darf das Leistungsangebot nur sachgerecht nutzen. Er wird insbesondere seinen Benutzernamen und das Passwort für den Zugang geheim halten, nicht weitergeben, keine Kenntnisnahme dulden oder ermöglichen und die erforderlichen Maßnahmen zur Gewährleistung der Vertraulichkeit ergreifen und bei einem Missbrauch oder Verlust dieser Angaben oder einem entsprechenden Verdacht dies dem Unternehmen anzuzeigen.</p>
				<p>2.	Der Nutzer ist verpflichtet Angaben, die für die Bereitstellung der Leistungen benötigt werden korrekt, umfassend und wahrheitsgemäß anzugeben. </p>
';
$lang['TERMS_11'] ='
				<p><b>§ 11 Sperrung des Zugangs / Kündigung</b></p>
				<p>1.	Medlanes behält sich vor, bei Verdacht einer missbräuchlichen Nutzung oder wesentlichen Vertragsverletzung diesen Vorgängen nachzugehen, entsprechende Vorkehrungen zu treffen und bei einem begründeten Verdacht den Zugang des Nutzers zu sperren. Sollte der Verdacht ausgeräumt werden können, wird die Sperrung wieder aufgehoben, andernfalls steht Medlanes ein außerordentliches Kündigungsrecht zu.</p>
				<p>2.	Jeder Partei steht das Recht zur Kündigung aus wichtigem Grund zu. Die Kündigung bedarf der Textform (z.B. Email). Mit Wirksamwerden der Kündigung wird der Zugang des Nutzers zur Online-Plattform von Medlanes gesperrt.</p>
';
$lang['TERMS_12'] ='
				<p><b>§ 12 Änderungen</b></p>
				<p>1.	Medlanes hat das Recht, die allgemeinen Geschäftsbedingungen jederzeit gegenüber den Nutzern mit Wirkung für die Zukunft zu ändern. </p>
				<p>2.	Eine beabsichtigte Änderung wird den Nutzern, die sich registriert haben, per E-Mail an die letzte Medlanes überlassene Email-Adresse mitgeteilt. Die jeweilige Änderung wird wirksam, wenn der jeweilige Nutzer ihr nicht innerhalb von zwei Wochen nach Absendung der E-Mail widerspricht. Für die Einhaltung der Zwei-Wochen-Frist ist die rechtzeitige Absendung des Widerspruchs maßgeblich. </p>
				<p>3.	Widerspricht der Nutzer der Änderung innerhalb der Zwei-Wochenfrist, ist Medlanes berechtigt, das Vertragsverhältnis insgesamt außerordentlich fristlos zu beenden, ohne dass dem Nutzer hieraus irgendwelche Ansprüche gegen Medlanes erwachsen. Wird das Vertragsverhältnis nach dem wirksamen Widerspruch des Nutzers fortgesetzt, behalten die bisherigen allgemeinen Geschäftsbedingungen ihre Gültigkeit.</p>
';
$lang['TERMS_13'] = '
				<p><b>§ 13 Schlussbestimmungen</b></p>
				<p>Sollten einzelne Bestimmungen dieser allgemeinen Geschäftsbedingungen einschließlich dieser Bestimmung ganz oder teilweise unwirksam sein oder werden, bleibt die Wirksamkeit der übrigen Regelungen unberührt. Anstelle der unwirksamen oder fehlenden Bestimmungen treten die jeweiligen gesetzlichen Regelungen.</p>
';



?>