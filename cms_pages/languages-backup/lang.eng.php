<?php
/* 
------------------
Language: English
------------------
*/

$page = basename($_SERVER['PHP_SELF']); 



$lang = array();

include 'english/index-EN.php';
include 'english/payment-EN.php';
include 'english/paymentemail-EN.php';
include 'english/signupemail-EN.php';
include 'english/header-footer-EN.php';
include 'english/thankyou-EN.php';

include 'english/privacy-EN.php';
include 'english/terms-EN.php';
include 'english/imprint-EN.php';
include 'english/contact-EN.php';
include 'english/about-EN.php';
include 'english/doctors-EN.php';
include 'english/faq-EN.php';
include 'english/service-EN.php';
include 'english/addquestion-EN.php';
include 'english/career-EN.php';
include 'english/langde.php';

?>
