<?php 	

$lang['php_echo'] = 'Text';
$lang['BXSLIDER'] = "<ul class='staticlink'>";
$lang['HEADER_1'] = 'Our support team is here for you 24/7!';
$lang['HEADER_2'] = '<b>(800) 413-7290</b> or <b>service@medlanes.com</b>';
$lang['INDEX_1'] = 'Do not let PTSD control your life.';
$lang['INDEX_2'] = 'Our expert psychologists and psychiatrists are online now and here to help. <br>Submit your question and feel better today!<br><b>Your satisfaction is 100% guaranteed.</b>';
$lang['TESTIMONIALS_0'] = 'We Beat PTSD';
$lang['TESTIMONIALS_1'] = '<b>Taking Life Back.</b> <br>When I got back from the Middle East, I was struggling with the symptoms of PTSD. I could not get an appointment with the VA, so I turned to Medlanes. Best decision ever! The PTSD treatment I am getting is second to none.';
$lang['TESTIMONIALS_2'] = 'Margaret Patton, 49<br>Atlanta, GA';
$lang['TESTIMONIALS_3'] = '<b>Overcoming Loss</b> <br>Recently I lost my wife, and have been struggling with PTSD. Dr. Sandrini really cares, and my post traumatic stress disorder treatment is fully online. Thanks Doc!';
$lang['TESTIMONIALS_4'] = 'April Papst, 28<br>Atlanta, GA';
$lang['TESTIMONIALS_5'] = '<b>Transcending Trauma</b> <br> I always thought PTSD was something soldiers suffered from when they were deployed. Thanks to Dr. Brown, I have finally found the perfect PTSD support. It has truly turned my life around.';
$lang['TESTIMONIALS_6'] = 'Reece Bush, 31<br>Cambridge, MA';
$lang['TESTIMONIALS_7'] = '<b>PTSD Questions Answered</b> <br>Having access to an online psychologist has been the greatest thing to happen to me in s long time. I can now get my PTSD questions from the comfort of my own home. Thank you, Medlanes';
$lang['TESTIMONIALS_8'] = 'Emma Lehmann, 38<br>Lombard, IL';
$lang['TESTIMONIALS_9'] = '<b>Stop Struggling</b> <br>This is my advice to you. Take the next step, and ask your question. Start your treatment of PTSD today and take your life back. This is by far the most affordable post traumatic stress disorder treatment available';
$lang['TESTIMONIALS_10'] = 'Patrick Dias, 43<br>Peoria, IL';
$lang['TESTIMONIALS_11'] = '<b>Hero of War</b> <br>Friends were not the only thing I left behind in Iraq. I feel like I left my mind there as well. Reluctant, I reached out to Dr. C for help and the difference has been night and day. The feelings of anxiety are gone, and I am finally moving forward with my life.';
$lang['TESTIMONIALS_12'] = 'Brodie Biermann, 29<br>Tempe, AZ';
$lang['TESTIMONIALS_13'] = '<b>Moving On</b> <br>I had a rough upbringing and never could shake the feelings of what happened. Little did I know, I suffered from a form of PTSD. Seeking PTSD support online was a turning point in my life. Thank you to the Medlanes team for being so helpful.';
$lang['TESTIMONIALS_14'] = 'Amelie De Luca, 51<br>Los Angeles, CA';
$lang['TESTIMONIALS_15'] = '<b>Online Psychiatrist</b> <br>The medications I had received for post traumatic stress disorder were causing a lot of weird side effects. Thanks to Dr. Cushing, I found that my PTSD treatment was interacting with a supplement I was taking. Thanks again!';
$lang['TESTIMONIALS_16'] = 'Jett Fierro, 30<br>Atlanta, GA';
$lang['DOCTORS_0'] = 'Meet our Providers';
$lang['DOCTORS_1'] = 'Agatha Andrews, MD';
$lang['DOCTORS_2'] = 'Board Certified Psychiatrist <br> 2,873+ People Helped';
$lang['DOCTORS_3'] = '4.85 / 5<br>I have over 15 years of experience treating PTSD and conditions that are related to and/or cause PTSD. I have been specially trained to treat PTSD online!';
$lang['DOCTORS_4'] = 'Heather Johnson, Ph.D.';
$lang['DOCTORS_5'] = 'Board Certified Psychotherapist <br> 1,611 People Helped';
$lang['DOCTORS_6'] = '4.92 / 5<br>I have treated well over 10,000 cases of PTSD and related conditions since starting practice. I am an expert in the treatment of PTSD!';
$lang['DOCTORS_7'] = 'Thomas Sandrini, MD';
$lang['DOCTORS_8'] = 'Diplomat of Psychiatry <br> 918 People Helped';
$lang['DOCTORS_9'] = '4.71 / 5<br>PTSD does not have to control you. My patients have overcome their issues with PTSD with specialized PTSD treatments that have been custom tailored to meet their specific needs.';
$lang['DOCTORS_10'] = 'Erwin Williams, MD, MBA';
$lang['DOCTORS_11'] = 'Board Certified Psychiatrist <br> 873+ People Helped';
$lang['DOCTORS_12'] = '4.8 / 5<br>Online psychiatry and online psychology are both very new concepts and I am excited to be on the ground floor of revolutionizing the treatment of PTSD online and through digital methods.';
$lang['DOCTORS_13'] = 'Jason Evans, Ph.D., MS';
$lang['DOCTORS_14'] = 'Certified Couples Therapist <br> 1,920 Couples Helped';
$lang['DOCTORS_15'] = '4.81 / 5<br>I have been doing online therapy for about 3 years now, and can with with a wide range of issues involving PTSD. I am here to help!';
$lang['DOCTORS_16'] = 'Wilma Forney, MD';
$lang['DOCTORS_17'] = 'Board Certified Psychiatrist <br> 1,001 People Helped';
$lang['DOCTORS_18'] = '4.78 / 5<br>I am so excited to be a part of a revolutionary online doctor platform like Medlanes. We are here for you 24/7!';
$lang['PAYMENT_NEW4'] = 'Your expert is waiting for your question!';
$lang['PAYMENT_NEW5'] = 'Agatha Andrews, MD';
$lang['PAYMENT_NEW6'] = 'Board Certified Psychiatrist <br> 2,873+ People Helped';
$lang['PAYMENT_DOC_IMG'] = '<img src="images/payment/doc-an.jpg" width="180" height="180" alt="#"/>';
$lang['DEP_CC'] = 'Credit Card';
$lang['DEP_PP'] = 'PayPal';
$lang['DEP_PP_TEXT'] = 'You will be redirected to PayPal to complete your payment.';
$lang['THANKYOU_6'] = 'service@medlanes.com';
$lang['THANKYOU_7'] = '(800) 413-7290';
$lang['PRESS_0'] = 'Online Mental Health Support!';
$lang['PRESS_1'] = 'Our doctors have been extensively trained to treat mental health conditions online! We can also help with:';
$lang['PRESS_2'] = '<ul><li><h2>Depression Support</h2></li><li><h2>Fears and Phobias</h2></li><li><h2>Behavioral Issues</h2></li><li><h2>Psychiatric Medication Questions</h2></li><li><h2>Panic Attacks</h2></li></ul>';



?>