<?php 	

$lang['php_echo'] = 'Text';
$lang['BXSLIDER'] = "<ul class='staticlink'>";
$lang['HEADER_1'] = 'Our support team is here for you 24/7!';
$lang['HEADER_2'] = '<b>(800) 413-7290</b> or <b>service@medlanes.com</b>';
$lang['INDEX_1'] = 'Questions about Pleurisy?';
$lang['INDEX_2'] = 'Our pulmonologists are online now and ready to help you. Submit your question about Pleurisy and feel better today!<b> Your satisfaction is 100% guaranteed.</b>';
$lang['TESTIMONIALS_0'] = 'What Our Patients Are Saying';
$lang['TESTIMONIALS_1'] = '<b>Quick answers here</b> <br>My sister has a bad pleurisy a year ago, so when I had the first symptoms I got very scared. I wanted to see a doctor quickly and was so happy to get an immediate response from my doctor at Medlanes. ';
$lang['TESTIMONIALS_2'] = 'Maria Davis, 47<br>Syracuse, IN';
$lang['TESTIMONIALS_3'] = '<b>Very competent help!</b> <br>I wanted to find out what pleurisy is and was so thankful to get some quick answers from Medlanes! Meanwhile my whole family use it regularly and everyone is convinced by the quick and reliable service.';
$lang['TESTIMONIALS_4'] = 'Demi Krimmer, 41<br>Portland, OR';
$lang['TESTIMONIALS_5'] = '<b>Medlanes - I like!</b> <br>My first symptoms were sharp pain during breathing and a dry cough. When I started getting fever and chills I got in touch with my Doctor at Medlanes. I was surprised at how quickly she found out that I must have a pleurisy and in no time I was treated. ';
$lang['TESTIMONIALS_6'] = 'Carter Cattaneo, 45<br>Kansas City, MO';
$lang['TESTIMONIALS_7'] = '<b>The best Doctors I have seen in a while!</b> <br>I was suffering from terrible side pains in my chest. I waited it out but then my symptoms got so bad that I could not wait for an appointment. Thanks to Medlanes I did not have to wait and got help the same day.';
$lang['TESTIMONIALS_8'] = 'Charlotte Jimínez, 44<br>Lufkin, TX';
$lang['TESTIMONIALS_9'] = '<b>Good and Fast Advice</b> <br>I have two small children and when I started getting a really bad cough and fever I was worried I had something contagious. With the help of my doctor at Medlanes I found out I had a pleurisy. I got quick treatment and was relieved to know my kids were safe. ';
$lang['TESTIMONIALS_10'] = 'Reece Tearle, 49<br>Grand Rapids, MI';
$lang['TESTIMONIALS_11'] = '<b>Thank you Medlanes</b> <br>I was worried that I had some sort of serious lung inflammation and since it was the weekend I could not get hold of my GP. I was so greatful that the Doctor at Medlanes was available and could help me out.';
$lang['TESTIMONIALS_12'] = 'Levi Kruger, 46<br>Maben, MS';
$lang['TESTIMONIALS_13'] = '<b>Global medcal help</b> <br>A friend was diagnosed with a pleurisy when she was on vacation in Africa. I was so worried because I did not know if pleurisy is life threatening. So i contacted a doctor from Medlanes and got quick answers that I could forward to her. ';
$lang['TESTIMONIALS_14'] = 'Sophia Rentería, 41<br>Salt Lake City, UT';
$lang['TESTIMONIALS_15'] = '<b>Modern healthcare at its best!</b> <br>I was very sceptical if a Doctor could really make a diagnosis online, but Medlanes has proven to be absolutely reliable and helpful. I had a pleurisy some months ago and was absolutely satisfied with the service they offered. ';
$lang['TESTIMONIALS_16'] = 'Frank Toscani, 49<br>Cincinnati, OH';
$lang['DOCTORS_0'] = 'Meet Our Experts';
$lang['DOCTORS_1'] = 'Celine Karlsson, MD';
$lang['DOCTORS_2'] = 'Board Certified Pulmonologist <br> 1,903 People Helped';
$lang['DOCTORS_3'] = '4.81 / 5<br>If it is not treated quickly pleurisy can lead to scarring. With Medlanes to time is lost and help can be given immediately!';
$lang['DOCTORS_4'] = 'Leslie Tomaszewski, MD';
$lang['DOCTORS_5'] = 'Board Certified Pulmonologist <br> 2,869 People Helped';
$lang['DOCTORS_6'] = '4.91 / 5<br>I know how tedious it is to wait for an appointment when you are worried about about your health! That is why Medlanes is the perfect solution! Here you get medial help when you need it!';
$lang['DOCTORS_7'] = 'Arthur Forester, MD';
$lang['DOCTORS_8'] = 'Board Certified Pulmonologist <br> 1,480 People Helped';
$lang['DOCTORS_9'] = '4.86 / 5<br>With my meanwhile 20 years of experience as a pulmonologist I suppose I am somewhat of an old-timer in the medical world. That is why I am so glad to be part of this wonderful project that finally puts medicine back on a modern track.  ';
$lang['DOCTORS_10'] = 'Alexander Hayward, MD';
$lang['DOCTORS_11'] = 'Board Certified Pulmonologist <br> 1,689 People Helped';
$lang['DOCTORS_12'] = '4.78 / 5<br>Many patients that are so limited by their symptoms that they cannot leave the house. Patients with wet pleurisy for example have severe breathing problems. A quick online chat helps me distinguish between a serious emergency that needs immediate treatment or a small problem that can be waited out. ';
$lang['DOCTORS_13'] = 'Larry Bavin, MD';
$lang['DOCTORS_14'] = 'Board Certified Pulmonologist <br> 1,527 People Helped';
$lang['DOCTORS_15'] = '4.95 / 5<br>A pleuritis without cough can be hard to diagnose, even for an experienced doctor. In the online chat however, I can really talk things through with the patient and make sure that I am not missing a piece of the puzzle. ';
$lang['DOCTORS_16'] = 'Yasmin Barros, MD';
$lang['DOCTORS_17'] = 'Board Certified Pulmonologist <br> 1,829 People Helped';
$lang['DOCTORS_18'] = '4.89 / 5<br>Lung diseases such as pleurisy or bronchitis can have many signs and symptoms. Patients need good medical advice, which they can get at any time of day at Medlanes. ';
$lang['PAYMENT_NEW4'] = 'Your expert is waiting for your question!';
$lang['PAYMENT_NEW5'] = 'Celine Karlsson, MD';
$lang['PAYMENT_NEW6'] = 'Board Certified Pulmonologist <br> 1,903 People Helped';
$lang['PAYMENT_DOC_IMG'] = '<img src="images/payment/doc-an.jpg" width="180" height="180" alt="#"/>';
$lang['DEP_CC'] = 'Credit Card';
$lang['DEP_PP'] = 'PayPal';
$lang['DEP_PP_TEXT'] = 'You will be redirected to PayPal to complete your payment.';
$lang['THANKYOU_6'] = 'service@medlanes.com';
$lang['THANKYOU_7'] = '(800) 413-7290';
$lang['PRESS_0'] = 'Ask Your Online Pulmonologist!';
$lang['PRESS_1'] = 'Our doctors have been extensively trained to treat pulmonary health conditions online! We can also help with:';
$lang['PRESS_2'] = '<ul><li><h2>COPD</h2></li><li><h2>Asthma</h2></li><li><h2>Bronchitis</h2></li><li><h2>Pulmonary Medication Questions</h2></li><li><h2>Emphysema</h2></li></ul>';


?>