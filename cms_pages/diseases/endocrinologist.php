<?php 	

$lang['BXSLIDER'] = "<ul class='staticlink'>";
$lang['HEADER_1'] = 'Our support team is here for you 24/7!';
$lang['HEADER_2'] = '<b>(800) 413-7290</b> or <b>service@medlanes.com</b>';
$lang['INDEX_1'] = 'Ask An Endocrinologist Now';
$lang['INDEX_2'] = 'Expert Endocrinologists are available now to take your questions. First consultations or second opinions, simply ask your question to get started <br><b>Your satisfaction is 100% guaranteed. </b>';
$lang['TESTIMONIALS_0'] = 'What Our Patients Are Saying';
$lang['TESTIMONIALS_1'] = '<b>Wonderful</b> <br>My online endocrinologist helped me with all of my questions after I was diagnosed with type 2 diabetes. Thank you!';
$lang['TESTIMONIALS_2'] = 'Dorothy Tindal, 44<br>South Boston, MA';
$lang['TESTIMONIALS_3'] = '<b>Best Endocrinologists</b> <br>I worked with Dr. Nguyen, he was wonderful. He helped me figure out the reason for my adverse reaction to levothyroxine.';
$lang['TESTIMONIALS_4'] = 'Zara Diederich, 44<br>Spokane Valley, WA';
$lang['TESTIMONIALS_5'] = '<b>Endocrinology Experts</b> <br>Having diabetes, I always have a lot of questions to ask. I am so happy that Medlanes has such a wonderful online endocrinologist platform. Lifesaver!';
$lang['TESTIMONIALS_6'] = 'Fergus Baker, 43<br>South Boston, MA';
$lang['TESTIMONIALS_7'] = '<b>Thank You</b> <br>My daughter was diagnosed with Hashimoto\'s Thyroiditis and her GP did not answer her questions. A certified endocrinologist not only answered her questions, but gave her peace of mind.';
$lang['TESTIMONIALS_8'] = 'Karissa Allen, 42<br>Cleveland, OH';
$lang['TESTIMONIALS_9'] = '<b>Trusted Endocrinologists</b> <br>I am glad I found this wonderful network of online endocrinologists that I can trust with all of my health issues. Thank you so, so much.';
$lang['TESTIMONIALS_10'] = 'Ali Myers, 40<br>Boise, ID';
$lang['TESTIMONIALS_11'] = '<b>Thanks!</b> <br>Simply awesome! My expert online endocrinologist has guided me through so much regarding my recent hypothyroidism diagnosis. Thank you for the clarity.';
$lang['TESTIMONIALS_12'] = 'Codi Köhler, 47<br>Malone, TX';
$lang['TESTIMONIALS_13'] = '<b>Simply the Best</b> <br>I was a little skeptical at first, but the advice from my online endocrinologist gave me the best follow up advice. He made up for the lack of explanation from my office based endocrinologist.';
$lang['TESTIMONIALS_14'] = 'Katie Correia, 38<br>Bowling Green, KY';
$lang['TESTIMONIALS_15'] = '<b>Problem Solved</b> <br>I always thought I had restless leg syndrome. My endocrinologist recommended I had my thyroid checked. Sure enough, hyperthyroidism. Thank you for finally helping me!';
$lang['TESTIMONIALS_16'] = 'Leon Genovese, 38<br>Portland, PA';
$lang['DOCTORS_0'] = 'Meet Our Experts';
$lang['DOCTORS_1'] = 'Sarah Adams, MD';
$lang['DOCTORS_2'] = 'Board Certified Endocrinologist<br> 1,215 People Helped';
$lang['DOCTORS_3'] = '4.81 / 5<br>Often times, endocrine system disorders are extremely complex and hard to find the right support. We are here for you!';
$lang['DOCTORS_4'] = 'Faye Goldstein, MD';
$lang['DOCTORS_5'] = 'Board Certified Endocrinologist<br> 1,869 People Helped';
$lang['DOCTORS_6'] = '4.91 / 5<br>Having access to an endocrinologist online will benefit virtually anyone suffering from some form of systemic disorder. I am happy to be a part of it.';
$lang['DOCTORS_7'] = 'Mike Hunt, MD';
$lang['DOCTORS_8'] = 'Certified in Endocrinology<br> 1,480 People Helped';
$lang['DOCTORS_9'] = '4.86 / 5<br>Bringing endocrinology online has always been a goal of mine, and I am here to assist in any way I can.';
$lang['DOCTORS_10'] = 'Kevin Nguyen, MD';
$lang['DOCTORS_11'] = 'Certified in Endocrinology<br> 1,689 People Helped';
$lang['DOCTORS_12'] = '4.78 / 5<br>Online endocrinologist? Yes! Welcome to the future of medicine. I am happy to assist you, and waiting for your questions.';
$lang['DOCTORS_13'] = 'Donald Masterson, MD';
$lang['DOCTORS_14'] = 'Board Certified Endocrinologist<br> 1,527 People Helped';
$lang['DOCTORS_15'] = '4.95 / 5<br>I specialize in disorders of the thyroid and diabetes. Ask your questions today!';
$lang['DOCTORS_16'] = 'Tella Moore, MD';
$lang['DOCTORS_17'] = 'Board Certified Endocrinologist<br> 1,829 People Helped';
$lang['DOCTORS_18'] = '4.89 / 5<br>I am happy to answer all of your endocrinology questions online now!';
$lang['PAYMENT_NEW4'] = 'Your expert is waiting for your question!';
$lang['PAYMENT_NEW5'] = 'Sarah Adams, MD';
$lang['PAYMENT_NEW6'] = 'Board Certified Endocrinologist<br> 1,215 People Helped';
$lang['PAYMENT_DOC_IMG'] = '<img src="images/payment/doc-an.jpg" width="180" height="180" alt="#"/>';
$lang['DEP_CC'] = 'Credit Card';
$lang['DEP_PP'] = 'PayPal';
$lang['DEP_PP_TEXT'] = 'You will be redirected to PayPal to complete your payment.';
$lang['THANKYOU_6'] = 'service@medlanes.com';
$lang['THANKYOU_7'] = '(800) 413-7290';
$lang['PRESS_0'] = 'Endocrinologists At Your Service!';
$lang['PRESS_1'] = 'Our superior endocrinologists have been extensively trained to support a wide variety of conditions online. We are also able to provide advice for:';
$lang['PRESS_2'] = '<ul><li><h2>Hypothyroidism</h2></li><li><h2>Low Testosterone</h2></li><li><h2>Diabetes</h2></li><li><h2>Thyroiditis</h2></li><li><h2>Prediabetes</h2></li><li><h2>Hormone Replacement Therapy</h2></li><li><h2>and More!</h2></li></ul>';




?>