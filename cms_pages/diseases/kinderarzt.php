<?php 	

$lang['BXSLIDER'] = "<ul class='staticlink'>";
$lang['HEADER_1'] = 'Unser Kundensupport ist für Sie da!';
$lang['HEADER_2'] = '<b>0800 / 765 43 43</b> or <b>service@medlanes.com</b>';
$lang['INDEX_1'] = 'Fragen Sie einen Kinderarzt';
$lang['INDEX_2'] = 'Unsere Kinderärzte beantworten Ihre Fragen rund um das Thema Kinder und Jugendliche.<b> Wir garantieren 100% Kundenzufriedenheit!</b>';
$lang['TESTIMONIALS_0'] = 'Unsere Kunden über uns';
$lang['TESTIMONIALS_1'] = '<b>Kein Krankenhaus.</b> <br>Vielen Dank an meinen Online-Kinderarzt. Wir haben uns die nächtliche Fahrt ins Krankenhaus gespart. Danke Medlanes!';
$lang['TESTIMONIALS_2'] = 'J. Meier, 43 <br> Lemgo';
$lang['TESTIMONIALS_3'] = '<b>Einfach super.</b> <br>Mein Sohn hatte einen schrecklichen Hautausschlag und hat das Kratzen nicht aufgehört. Ein schneller Kontakt mit meinem Kinderarzt online und uns wurde geholfen.';
$lang['TESTIMONIALS_4'] = 'P. Tegers, 30 <br> Sassnitz';
$lang['TESTIMONIALS_5'] = '<b>Kinderarzt online.</b> <br>Wir sind vor kurzem Eltern geworden und jedes Fieber und jeder Husten macht uns Sorgen. Dank Medlanes ist unser Kinderarzt immer schnell zu erreichen!';
$lang['TESTIMONIALS_6'] = 'E. Kleinert, 38 <br> Gießen';
$lang['TESTIMONIALS_7'] = '<b>Wunderbar.</b> <br>Der Zugang zu einem Kinderarzt online ist sehr bequem. Medlanes als junge Mutter an der Seite zu haben, ist goldwert.';
$lang['TESTIMONIALS_8'] = 'F. Esser, 43 <br> Coswig';
$lang['TESTIMONIALS_9'] = '<b>Mein Kinderarzt.</b> <br>Er ist wunderbar! Ich pflege den Kontakt zu meinem Online-Kinderarzt mittlerweile seit über einem Jahr. Medlanes macht den Kontakt mit meinem Kinderarzt einfach und erschwinglich.';
$lang['TESTIMONIALS_10'] = 'G. Ostermann, 41 <br> Erwitte';
$lang['TESTIMONIALS_11'] = '<b>Onlineärzte</b> <br>Ehrlich gesagt dachte ich, einen Arzt online zu fragen, ist nicht möglich. Der Online-Kinderarzt bei Medlanes hat mir das Gegenteil bewiesen. Danke für alles!';
$lang['TESTIMONIALS_12'] = 'S. Piening, 31<br> Miesbach';
$lang['TESTIMONIALS_13'] = '<b>Ohreninfektion war einmal!</b> <br>Wir haben über eine Woche lang versucht, die Ohreninfektionen meiner Tochter loszuwerden. Dank Medlanes konnten wir feststellen, dass sie allergisch auf Daunenfedern war.';
$lang['TESTIMONIALS_14'] = 'A. Leiser, 49<br> Münster';
$lang['TESTIMONIALS_15'] = '<b>Lebensretter</b> <br>Mein Sohn war Bettnässer. Ich sprach mit meinem Online-Kinderarzt und bekam den besten Rat. Keine nächtliche "Unfälle" mehr!';
$lang['TESTIMONIALS_16'] = 'T. Kramer, 28<br> Herten';
$lang['DOCTORS_0'] = 'Unsere Ärzte';
$lang['DOCTORS_1'] = 'Dr. S. Wiedemann';
$lang['DOCTORS_2'] = 'Fachärztin für Kinder- und Jugendmedizin <br> 17 Jahre Praxiserfahrung';
$lang['DOCTORS_3'] = '4.95 / 5<br><br>Ich habe die Möglichkeit, vielen Kindern über diese Plattform zu helfen. Ich bin so froh, dass ich auch online Fragen beantworten kann.';
$lang['DOCTORS_4'] = 'Dr. R. Lauterbach';
$lang['DOCTORS_5'] = 'Fachärztin für Kinder- und Jugendmedizin <br> 16 Jahre Praxiserfahrung';
$lang['DOCTORS_6'] = '4.82 / 5<br><br>Probleme, die das Kind betreffen, sind immer beängstigend. Der Zugang zu Online-Kinderärzten nimmt vielen Eltern die Angst. Fragen Sie uns noch heute!';
$lang['DOCTORS_7'] = 'Dr. J. Maiwert';
$lang['DOCTORS_8'] = 'Facharzt für Kinder- und Jugendmedizin <br> 14 Jahre Praxiserfahrung';
$lang['DOCTORS_9'] = '4.81 / 5<br><br>Ich haben das Gefühl, dass ich bereits mehr kleinen Patienten online geholfen habe, als in meiner 14-jährigen Praxistätigkeit.';
$lang['DOCTORS_10'] = 'Dr. L. Krau';
$lang['DOCTORS_11'] = 'Facharzt für Kinder- und Jugendmedizin <br> 11 Jahre Praxiserfahrung';
$lang['DOCTORS_12'] = '4.88 / 5<br><br>Ich bin hier, um die Antworten auf alle Ihre Fragen zu pädiatrischen Themen zu geben! Fragen Sie mich jetzt!';
$lang['DOCTORS_13'] = 'Dr. T. Jägers';
$lang['DOCTORS_14'] = 'Facharzt für Kinder- und Jugendmedizin  <br> 18 Jahre Praxiserfahrung';
$lang['DOCTORS_15'] = '4.86 / 5<br><br>Ich bin speziell als Online-Kinderarzt geschult und freue mich darauf, auch Ihnen und Ihrer Familie zu helfen.';
$lang['DOCTORS_16'] = 'Dr. K. Riessmer';
$lang['DOCTORS_17'] = 'Fachärztin für Kinder- und Jugendmedizin  <br> 15 Jahre Praxiserfahrung';
$lang['DOCTORS_18'] = '4.79 / 5<br><br>Ich bin stolz als Kinderarzt auch online zur Verfügung zu stehen, 24 Stunden am Tag, 7 Tage die Woche!';
$lang['PAYMENT_NEW4'] = 'Ein Facharzt wartet auf Ihre Frage';
$lang['PAYMENT_NEW5'] = 'Dr. S. Wiedemann';
$lang['PAYMENT_NEW6'] = 'Fachärztin für Kinder- und Jugendmedizin <br> 17 Jahre Praxiserfahrung';
$lang['PAYMENT_DOC_IMG'] = '<img src="images/payment/doc-an.jpg" width="180" height="180" alt="#"/>';
$lang['DEP_CC'] = 'Kreditkarte';
$lang['DEP_PP'] = 'PayPal';
$lang['DEP_PP_TEXT'] = 'Zur Bezahlung werden Sie zur PayPal Webseite weitergeleitet.';
$lang['THANKYOU_6'] = 'service@medlanes.com';
$lang['THANKYOU_7'] = '0800 / 765 43 43';
$lang['PRESS_0'] = 'Onlinehilfe bei Kindererkrankungen ';
$lang['PRESS_1'] = 'Unsere Kinderärzte sind in der Onlineberatung geschult. Wir helfen bei:';
$lang['PRESS_2'] = '<ul><li><h2>Windpocken<h2></li><li><h2>Fieber</h2></li><li><h2>Ohrinfektionen</h2></li><li><h2>Allergien</h2></li></ul>';



?>