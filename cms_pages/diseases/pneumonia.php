<?php 	

$lang['php_echo'] = 'Text';
$lang['BXSLIDER'] = "<ul class='staticlink'>";
$lang['HEADER_1'] = 'Our support team is here for you 24/7!';
$lang['HEADER_2'] = '<b>(800) 413-7290</b> or <b>service@medlanes.com</b>';
$lang['INDEX_1'] = 'Online Pneumonia help';
$lang['INDEX_2'] = 'Our pulmonologists are online now and ready to help you. Submit your question about Pneumonia and feel better today!<b> Your satisfaction is 100% guaranteed.</b>';
$lang['TESTIMONIALS_0'] = 'What Our Patients Are Saying';
$lang['TESTIMONIALS_1'] = '<b>Quick and Simple</b> <br>When taking care of my mother, who had severe pneumonia, I did not know if I was at risk of getting the same thing. One quick consultation with my doctor from Medlanes and I knew about the ways to protect myself.';
$lang['TESTIMONIALS_2'] = 'Natalie Barbosa, 39<br>S Boston, MA';
$lang['TESTIMONIALS_3'] = '<b>Medicine Online! About time!</b> <br>When I fell ill with pneumonia even walking up and down the stairs was a huge struggle. Having the option to ask a specialist online really made things so much easier. ';
$lang['TESTIMONIALS_4'] = 'Summer Rossi, 44<br>Washington, VA';
$lang['TESTIMONIALS_5'] = '<b>Great benefits</b> <br>I never thought that an online doctor could be so much help. But when I fell sick on the weekend and no doctor was available I noticed the benefits of the service. Keep up the good work Medlanes!';
$lang['TESTIMONIALS_6'] = 'Abdullah Russell, 38<br>Wilmington, MA';
$lang['TESTIMONIALS_7'] = '<b>Professional pulmonologists Online</b> <br>Im sure that everyone working fulltime knows the struggles of getting appointment with your doctor. I really found Medlanes to be a huge relief and I am greatful for all the people putting so much work into this great service.  ';
$lang['TESTIMONIALS_8'] = 'Harriet Black, 47<br>Summerville, SC';
$lang['TESTIMONIALS_9'] = '<b>Big Thanks to Medlanes</b> <br>My doctor at Medlanes was such great help when I fell sick with pneumonia. I got the help I needed right away and felt I could ask any question and get qualified answers.';
$lang['TESTIMONIALS_10'] = 'Keir Cavalcanti, 40<br>Middleburg, FL';
$lang['TESTIMONIALS_11'] = '<b>Great medical service</b> <br>When I had pneumonia even walking and talking cost me so much energy. I was so relieved to find quick answers and help from Medlanes and do not want to go without this great service anymore. ';
$lang['TESTIMONIALS_12'] = 'Ryan Alexander, 43<br>Phoenix, AZ';
$lang['TESTIMONIALS_13'] = '<b>Compliments to the Medlanes Team</b> <br>I am so thankful that I came across this great service. When you are really ill you do not have the energy to run from one doctors apointment to the next. I had pneumonia and my symptoms were pretty insane so Medlanes really saved me a lot of struggle. ';
$lang['TESTIMONIALS_14'] = 'Nicole Nunan, 44<br>Sarasota, FL';
$lang['TESTIMONIALS_15'] = '<b>Get your Questions answered now</b> <br>A friend of mine fell sick with a very rare type of pneumonia. I was so worried for her, but knowing the doctors at Medlanes were taking care of her and continuously answering her questions day and night made me feel much better. ';
$lang['TESTIMONIALS_16'] = 'Ayaan Belle, 39<br>Rochelle, TX';
$lang['DOCTORS_0'] = 'Meet Our Experts';
$lang['DOCTORS_1'] = 'Celine Karlsson, MD';
$lang['DOCTORS_2'] = 'Board Certified Pulmonologist <br> 1,903 People Helped';
$lang['DOCTORS_3'] = '4.81 / 5<br>Patients with typical forms of pneumonia are contagious and should not be leaving the house more than absolutely necessary. Medlanes offers a plattform for them to get their medical questions answered at home by professionals such as myself. ';
$lang['DOCTORS_4'] = 'Leslie Tomaszewski, MD';
$lang['DOCTORS_5'] = 'Board Certified Pulmonologist <br> 2,869 People Helped';
$lang['DOCTORS_6'] = '4.91 / 5<br>We live in a world that is changing and developing ever so quickly. Medlanes is on the forefront of finally bringing medicine to the place it should be - the 21st century!';
$lang['DOCTORS_7'] = 'Arthur Forester, MD';
$lang['DOCTORS_8'] = 'Board Certified Pulmonologist <br> 1,480 People Helped';
$lang['DOCTORS_9'] = '4.86 / 5<br>I am happy to see that Medlanes is getting more and more successful. We need people to put in the effort and help our health system develop in a more modern direction. I am proud to be a part of it. ';
$lang['DOCTORS_10'] = 'Alexander Hayward, MD';
$lang['DOCTORS_11'] = 'Board Certified Pulmonologist <br> 1,689 People Helped';
$lang['DOCTORS_12'] = '4.78 / 5<br>As a pulmonologist I am confronted with so many difficult cases everyday. I am greatfu to be part of this service that helps me work so much more efficiently and treat at least 30% more patients than I would ususally be capable of. ';
$lang['DOCTORS_13'] = 'Larry Bavin, MD';
$lang['DOCTORS_14'] = 'Board Certified Pulmonologist <br> 1,527 People Helped';
$lang['DOCTORS_15'] = '4.95 / 5<br>Lung infections like vial pneumonia should not be taken lightly. I have been working as a pulmonologist for nearly 20 years now and know that the quicker people get the right help, the better. ';
$lang['DOCTORS_16'] = 'Yasmin Barros, MD';
$lang['DOCTORS_17'] = 'Board Certified Pulmonologist <br> 1,829 People Helped';
$lang['DOCTORS_18'] = '4.89 / 5<br>I can only advice everyone who is suffering from symptoms like short breath, fever and chest pain to seak help at Medlanes. You will get quick and professional answers from experienced doctors and may be able to avoid a simple cold from turning into a serious pneumonia or the like. ';
$lang['PAYMENT_NEW4'] = 'Your expert is waiting for your question!';
$lang['PAYMENT_NEW5'] = 'Celine Karlsson, MD';
$lang['PAYMENT_NEW6'] = 'Board Certified Pulmonologist <br> 1,903 People Helped';
$lang['PAYMENT_DOC_IMG'] = '<img src="images/payment/doc-an.jpg" width="180" height="180" alt="#"/>';
$lang['DEP_CC'] = 'Credit Card';
$lang['DEP_PP'] = 'PayPal';
$lang['DEP_PP_TEXT'] = 'You will be redirected to PayPal to complete your payment.';
$lang['THANKYOU_6'] = 'service@medlanes.com';
$lang['THANKYOU_7'] = '(800) 413-7290';
$lang['PRESS_0'] = 'Ask Your Online Pulmonologist!';
$lang['PRESS_1'] = 'Our doctors have been extensively trained to treat pulmonary health conditions online! We can also help with:';
$lang['PRESS_2'] = '<ul><li><h2>COPD</h2></li><li><h2>Asthma</h2></li><li><h2>Bronchitis</h2></li><li><h2>Pulmonary Medication Questions</h2></li><li><h2>Emphysema</h2></li></ul>';


?>