<?php 	

$lang['BXSLIDER'] = "<ul class='staticlink'>";
$lang['HEADER_1'] = 'Our support team is here for you 24/7!';
$lang['HEADER_2'] = '<b>(800) 413-7290</b> or <b>service@medlanes.com</b>';
$lang['INDEX_1'] = 'Your Anxiety ends here.';
$lang['INDEX_2'] = 'Our expert psychologists and psychiatrists are online now and here to help. <br>Stop suffering from anxiety. Submit your question and feel better today!<br><b>Your satisfaction is 100% guaranteed.</b>';
$lang['TESTIMONIALS_0'] = 'We Beat Anxiety!';
$lang['TESTIMONIALS_1'] = '<b>I Conquered Social Anxiety.</b> <br> I\'ve suffered from anxiety for countless years, and finally, thanks to Medlanes, can say I am anxiety free! Their friendly online doctor platform gave me the ability to speak to my psychiatrist online from my home!';
$lang['TESTIMONIALS_2'] = 'Sophie Clack, 44 <br> Huntington Beach, CA';
$lang['TESTIMONIALS_3'] = '<b>I beat Generalized Anxiety Disorder.</b> <br>I have always had to deal with my anxiety disorder, and never could get the support I needed from talking to a therapist once a week. Now, I can talk about my anxiety to my therapist online!';
$lang['TESTIMONIALS_4'] = 'Janice Watkins, 27 <br> Birmingham, AL';
$lang['TESTIMONIALS_5'] = '<b>Panic Disorder No More!</b> <br> My panic disorder was causing me issues daily, and I was having a lot of issues finding any joy in life. I can talk to my psychiatrist online now! Just knowing that online therapy exists give me peace of mind.';
$lang['TESTIMONIALS_6'] = 'Ralph Scottie, 37 <br> Jacksonville, FL';
$lang['TESTIMONIALS_7'] = '<b>No more anxiety attacks!</b> <br> I was having a rough time with anxiety attacks and panic issues. I have been working with my doctor online, and have found the perfect treatment for anxiety attacks. I cannot thank Dr. Andrews enough!';
$lang['TESTIMONIALS_8'] = 'Melissa Fillmore, 32 <br> Skokie, IL';
$lang['TESTIMONIALS_9'] = '<b>Axe your Anxiety</b> <br>I was lucky enough to find a wonderful anxiety treatment online. If it helps, me it will help anyone! I highly recommend the online mental health treatments provided from Medlanes!';
$lang['TESTIMONIALS_10'] = 'Jackson Morrow, 55 <br> New York, NY';
$lang['TESTIMONIALS_11'] = '<b>Ask A Psychiatrist Online</b> <br>Seeking anxiety help online has by far changed the way I deal with treating my anxiety. Medlanes has been a complete game-changer in my life and offers awesome ways to treat anxiety disorders online.';
$lang['TESTIMONIALS_12'] = 'Howard Little, 29<br> Landover, MD';
$lang['TESTIMONIALS_13'] = '<b>OCD Support I needed</b> <br>I finally found the Obsessive Compulsive Disorder support I have been searching for in an easy online interface. I finally have the help I need for my treatment of OCD. Thank you to Dr. Sandrini and the friendly support team with Medlanes!';
$lang['TESTIMONIALS_14'] = 'Ashley Cox, 51<br> Galveston, TX';
$lang['TESTIMONIALS_15'] = '<b>I vanquished PTSD</b> <br>Coming home from Iraq recently, I was dealing with symptoms of post traumatic stress disorder. I have been seeing my therapist online for about 4 weeks now, and have finally gotten the help I have so desperately seeked. Thank you Medlanes!';
$lang['TESTIMONIALS_16'] = 'Chad Bush, 22<br> Irvine, CA';
$lang['DOCTORS_0'] = 'Meet our Providers';
$lang['DOCTORS_1'] = 'Agatha Andrews, MD';
$lang['DOCTORS_2'] = 'Board Certified Psychiatrist <br> 2,873+ People Helped';
$lang['DOCTORS_3'] = '4.85 / 5<br><br>I have over 15 years of experience treating anxiety and conditions that are related to and/or cause anxiety. I have been specially trained to treat anxiety online!';
$lang['DOCTORS_4'] = 'Heather Johnson, Ph.D.';
$lang['DOCTORS_5'] = 'Board Certified Psychotherapist <br> 1,611 People Helped';
$lang['DOCTORS_6'] = '4.92 / 5<br><br>I have treated well over 10,000 cases of anxiety and related conditions since starting practice. I am an expert in the treatment of anxiety!';
$lang['DOCTORS_7'] = 'Thomas Sandrini, MD';
$lang['DOCTORS_8'] = 'Diplomat of Psychiatry <br> 918 People Helped';
$lang['DOCTORS_9'] = '4.71 / 5<br><br>Anxiety does not have to control you. My patients have overcome their issues with anxiety with specialized anxiety treatments that have been custom tailored to meet their specific needs.';
$lang['DOCTORS_10'] = 'Erwin Williams, MD, MBA';
$lang['DOCTORS_11'] = 'Board Certified Psychiatrist <br> 873+ People Helped';
$lang['DOCTORS_12'] = '4.8 / 5<br><br>Online psychiatry and online psychology are both very new concepts and I am excited to be on the ground floor of revolutionizing the treatment of anxiety online and through digital methods.';
$lang['DOCTORS_13'] = 'Jason Evans, Ph.D., MS';
$lang['DOCTORS_14'] = 'Certified Couples Therapist <br> 1,920 Couples Helped';
$lang['DOCTORS_15'] = '4.81 / 5<br><br>I have been doing online therapy for about 3 years now, and can with with a wide range of issues involving anxiety. I am here to help';
$lang['DOCTORS_16'] = 'Wilma Forney, MD';
$lang['DOCTORS_17'] = 'Board Certified Psychiatrist <br> 1,001 People Helped';
$lang['DOCTORS_18'] = '4.78 / 5<br><br>I am so excited to be a part of a revolutionary online doctor platform like Medlanes. We are here for you 24/7!';
$lang['PAYMENT_NEW4'] = 'Your expert is waiting for your question!';
$lang['PAYMENT_NEW5'] = 'Agatha Andrews, MD';
$lang['PAYMENT_NEW6'] = 'Board Certified Psychiatrist <br> 2,873+ People Helped';
$lang['PAYMENT_DOC_IMG'] = '<img src="images/payment/doc-an.jpg" width="180" height="180" alt="#"/>';
$lang['DEP_CC'] = 'Credit Card';
$lang['DEP_PP'] = 'PayPal';
$lang['DEP_PP_TEXT'] = 'You will be redirected to PayPal to complete your payment.';
$lang['THANKYOU_6'] = 'service@medlanes.com';
$lang['THANKYOU_7'] = '(800) 413-7290';
$lang['PRESS_0'] = 'Online Mental Health Support!';
$lang['PRESS_1'] = 'Our doctors have been extensively trained to treat mental health conditions online! We can also help with:';
$lang['PRESS_2'] = '<ul><li><h2>Depression Support</h2></li><li><h2>Fears and Phobias</h2></li><li><h2>Behavioral Issues</h2></li><li><h2>Psychiatric Medication Questions</h2></li><li><h2>Panic Attacks</h2></li></ul>'; 



 ?>