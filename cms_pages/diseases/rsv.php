<?php 	

$lang['php_echo'] = 'Text';
$lang['BXSLIDER'] = "<ul class='staticlink'>";
$lang['HEADER_1'] = 'Our support team is here for you 24/7!';
$lang['HEADER_2'] = '<b>(800) 413-7290</b> or <b>service@medlanes.com</b>';
$lang['INDEX_1'] = 'We help you tackle Respiratory Syncytial Virus (RSV)';
$lang['INDEX_2'] = 'Our pediatricians are online now and ready to help you. Submit your question about Respiratory Syncytial Virus (RSV) and feel better today!<b> Your satisfaction is 100% guaranteed.</b>';
$lang['TESTIMONIALS_0'] = 'What Our Patients Are Saying';
$lang['TESTIMONIALS_1'] = '<b>Thank you!</b> <br>Withouth the help of Medlanes I would not have been diagnosed with syncytial virus and would not have had proper treatment. I am so very grateful.';
$lang['TESTIMONIALS_2'] = 'Sara Fiorentini, 38<br>Maple Shade, NJ';
$lang['TESTIMONIALS_3'] = '<b>RSV be gone</b> <br>I did not know what does rsv stand for and my online doctor explained that it was the respiratory syncytial virus. Medlanes provided great doctor consultations and I will make sure to use their services in the future.';
$lang['TESTIMONIALS_4'] = 'Alisha Engel, 42<br>Durham, NC';
$lang['TESTIMONIALS_5'] = '<b>Best consultation ever</b> <br>When my child fell ill with respiratory virus I felt so lost and helpless. Only an online doctor from Medlanes has managed to reassure me and guide me through the treatment process. Thank you.';
$lang['TESTIMONIALS_6'] = 'Anthony Meier, 46<br>Columbus, OH';
$lang['TESTIMONIALS_7'] = '<b>Medlanes provides best service</b> <br>I did not know what is rsv and how to cope with it. If you feel similar, use Medlanes. It is a great online health service platfrom!';
$lang['TESTIMONIALS_8'] = 'Piper Fuchs, 43<br>Chicago, IL';
$lang['TESTIMONIALS_9'] = '<b>I will use Medlanes again</b> <br>Respiratory syncytial virus is so hard to endure. As a mother, I felt horrible when my daughter fell ill but once again, Medlanes was there for me. ';
$lang['TESTIMONIALS_10'] = 'Samuel Camden, 49<br>Waltham, MA';
$lang['TESTIMONIALS_11'] = '<b>Happy mom</b> <br>I just became a mom and my infant had rsv. It was scary and terrifying, but not only my online doctor helped my daughter, but helped me with my struggles. I am so at ease now!';
$lang['TESTIMONIALS_12'] = 'Richard Findlay, 39<br>Buffalo Grove, IL';
$lang['TESTIMONIALS_13'] = '<b>Adult witn an rsv</b> <br>I was so surprised when I got diagnosed with an rsv, but it appears that rsv in adults is common. I learned that from my consultation with medlanes online doctor. I also received a great treatment plan.';
$lang['TESTIMONIALS_14'] = 'Katherine Smith, 49<br>Jupiter, FL';
$lang['TESTIMONIALS_15'] = '<b>Medlanes helped my family</b> <br>Respiratory virus in adults is so common, so I expected to fall sick once I knew my son was ill. It was Medlanes services that surprised me. It was quick, inexpensive and reliable.';
$lang['TESTIMONIALS_16'] = 'Bailey Cotto, 47<br>Louisville, MS';
$lang['DOCTORS_0'] = 'Meet Our Experts';
$lang['DOCTORS_1'] = 'Allison Watters, MD';
$lang['DOCTORS_2'] = 'Board Certified Pediatrician<br> 1,903 People Helped';
$lang['DOCTORS_3'] = '4.81 / 5<br>Many people don\'t know what is rsv in infants and I am here to help them. As an online pediatrician I can cater to people\'s needs in an easy and fast way.';
$lang['DOCTORS_4'] = 'Libby Ryder, MD';
$lang['DOCTORS_5'] = 'Board Certified Pediatrician<br> 2,869 People Helped';
$lang['DOCTORS_6'] = '4.91 / 5<br>I have been an online pediatrician for a while now and it never ceases to amaze me how great telehealth is. I manage to help people from across the world. ';
$lang['DOCTORS_7'] = 'Jack McKay, MD';
$lang['DOCTORS_8'] = 'Board Certified Pediatrician<br> 1,480 People Helped';
$lang['DOCTORS_9'] = '4.86 / 5<br>I have been practising medicine for 20 years now and if you need help, I am your person. Contact me at Medlanes 24/7.';
$lang['DOCTORS_10'] = 'Kelvin Lee, MD';
$lang['DOCTORS_11'] = 'Certified in Pediatrics<br> 1,689 People Helped';
$lang['DOCTORS_12'] = '4.78 / 5<br>RSV treatment is often complicated and Medlanes intends to make it easy for you. Contact me if you suffer from RSV and receive the treatment you deserve.';
$lang['DOCTORS_13'] = 'Jayce Timpani, MD';
$lang['DOCTORS_14'] = 'Board Certified - Pediatrics<br> 1,527 People Helped';
$lang['DOCTORS_15'] = '4.95 / 5<br>RSV in toddlers is often hard on the parents. As a pediatrician I intend to help both the children and their parents. You can contact me 24/7 at Medlanes.';
$lang['DOCTORS_16'] = 'Kelly Robbins, MD';
$lang['DOCTORS_17'] = 'Board Certified Pediatrician<br> 1,829 People Helped';
$lang['DOCTORS_18'] = '4.89 / 5<br>Respiratory Syncytial Virus is a serious condition that requires medical attention. Medlanes provides that so contact me any time for a consultation.';
$lang['PAYMENT_NEW4'] = 'Your expert is waiting for your question!';
$lang['PAYMENT_NEW5'] = 'Allison Watters, MD';
$lang['PAYMENT_NEW6'] = 'Board Certified Pediatrician<br> 1,903 People Helped';
$lang['PAYMENT_DOC_IMG'] = '<img src="images/payment/doc-an.jpg" width="180" height="180" alt="#"/>';
$lang['DEP_CC'] = 'Credit Card';
$lang['DEP_PP'] = 'PayPal';
$lang['DEP_PP_TEXT'] = 'You will be redirected to PayPal to complete your payment.';
$lang['THANKYOU_6'] = 'service@medlanes.com';
$lang['THANKYOU_7'] = '(800) 413-7290';
$lang['PRESS_0'] = 'Ask Your Online Pediatrician!';
$lang['PRESS_1'] = 'Our expert pediatricians have been extensively trained to support a wide variety of conditions online. We are here for you and your child. Our specialties include:';
$lang['PRESS_2'] = '<ul><li><h2>Rash Analysis</h2></li><li><h2>Mononucleosis</h2></li><li><h2>ADHD</h2></li><li><h2>Fevers</h2></li><li><h2>Ear Infections</h2></li><li><h2>Croup</h2></li><li><h2>and More!</h2></li></ul>';


?>