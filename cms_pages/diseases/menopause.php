<?php 	

$lang['php_echo'] = 'Text';
$lang['BXSLIDER'] = "<ul class='staticlink'>";
$lang['HEADER_1'] = 'Our support team is here for you 24/7!';
$lang['HEADER_2'] = '<b>(800) 413-7290</b> or <b>service@medlanes.com</b>';
$lang['INDEX_1'] = 'Problems with menopause? Ask a question.';
$lang['INDEX_2'] = 'Our gynecologists are online now and ready to help you. Submit your question about Menopause and feel better today!<b> Your satisfaction is 100% guaranteed.</b>';
$lang['TESTIMONIALS_0'] = 'What Our Patients Are Saying';
$lang['TESTIMONIALS_1'] = '<b>Thank you for being there</b> <br>Thanks to Medlanes, I was able to talk to a serious gynecologist without having to wait weeks for an appointment. She was very helpful and explained me all the signs of menopause. She even gave me great advice on how to alleviate some of the symptoms that may develop.';
$lang['TESTIMONIALS_2'] = 'Lily Pritchard, 40<br>Newport, RI';
$lang['TESTIMONIALS_3'] = '<b>A very useful and simple service</b> <br>A very useful and simple service';
$lang['TESTIMONIALS_4'] = 'Brooke Posada, 48<br>Jonesboro, GA';
$lang['TESTIMONIALS_5'] = '<b>A very useful and simple service</b> <br>My wife recently started to show some signs of perimenopause. Thanks to Medlanes, I was able to talk with a nice online gynecologist who explained me what menopause actually is. He also told me what I could do to help my wife in this transitionnal period.';
$lang['TESTIMONIALS_6'] = 'Corey Alves, 38<br>Mentone, CA';
$lang['TESTIMONIALS_7'] = '<b>Thank you for being there</b> <br>My mood changes constantly, I have missed a few periods and also started to sleep poorly. I was very worried about my health until I talked to an online gynecologist at Medlanes. She really took the time to listen to me and explained all the body changes that occur during perimenopause.';
$lang['TESTIMONIALS_8'] = 'Tia McGregor, 46<br>Sabattus, ME';
$lang['TESTIMONIALS_9'] = '<b>Thanks to the online gynecologists at Medlanes I was finally able to learn more about menopause. Now I get to help my loving wife through this challenging transition and know what to do to help her relieve some of her worst symptoms.</b> <br>Thanks to the online gynecologists at Medlanes I was finally able to learn more about menopause. Now I get to help my loving wife through this challenging transition and know what to do to help her relieve some of her worst symptoms.';
$lang['TESTIMONIALS_10'] = 'Nate Jaeger, 38<br>Moanalua, HI';
$lang['TESTIMONIALS_11'] = '<b>An awesome platform!</b> <br>My girlfriend recently entered the perimenopause period. Thanks to Medlanes, we were able to talk to a certified online gynecologist, who gave us great advice. She even told us how to treat perimenopause mood swings as my girlfriend had been feeling depressed lately. Now everything is good!';
$lang['TESTIMONIALS_12'] = 'Logan Boni, 44<br>Detroit, MI';
$lang['TESTIMONIALS_13'] = '<b>Medlanes is simply wonderful</b> <br>I have been asking myself so many questions about menopause and sleep disorders lately, as I am constantly exhausted. It was nice to be able to talk about it with a certified online gynecologist. He really put me at ease and addressed all my concerns. I will finally get a good night\'s sleep tonight!';
$lang['TESTIMONIALS_14'] = 'Sienna Whittington, 38<br>Orlando, FL';
$lang['TESTIMONIALS_15'] = '<b>Here for you 24/7</b> <br>The other day, my girlfriend and I were wondering about sexual arousal after menopause. Thanks to the webplatform, we were able to ask an online gynecologist about it. Those doctors at Medlanes are really there for you, when you need them the most!';
$lang['TESTIMONIALS_16'] = 'Zachary Camden, 38<br>Washington, DC';
$lang['DOCTORS_0'] = 'Meet Our Experts';
$lang['DOCTORS_1'] = 'Madison Leal, MD';
$lang['DOCTORS_2'] = 'Board Certified Gynecologist<br> 2,811 People Helped';
$lang['DOCTORS_3'] = '4.89 / 5<br>Working from a web platform such as Medlanes is amazing! I get to help my older patient deal with menopause and can make sure they undersand women\'s health.';
$lang['DOCTORS_4'] = 'Callie Ross, MD';
$lang['DOCTORS_5'] = 'Board Certified OBGYN <br> 1,760 People Helped';
$lang['DOCTORS_6'] = '4.88 / 5<br>I have been a gynecologist for over 15 years now and have helped many women. Thanks to Medlanes, I get to take on even more patients. I can make sure you too will get the best care possible.';
$lang['DOCTORS_7'] = 'Owen Tyers, MD';
$lang['DOCTORS_8'] = 'Board Certified OBGYN<br> 908 People Helped';
$lang['DOCTORS_9'] = '4.94 / 5<br>Working with Medlanes is truly wonderful. It gives me the opportunity to help more women every day. Considering I am a man, some patients find it easier to use a webplatform to talk about their problems.';
$lang['DOCTORS_10'] = 'Alvin Lee, MD, MPH';
$lang['DOCTORS_11'] = 'Board Certified OBGYN <br> 1,401 People Helped';
$lang['DOCTORS_12'] = '4.8 / 5<br>You are wondering about the symptoms of perimenopause at age 40? You came to the right place! As an certified online gynecologist, it\'s my duty to help you and make sure you know everything about women\'s health.';
$lang['DOCTORS_13'] = 'Rasin Sem, MD, MPH';
$lang['DOCTORS_14'] = 'Board Certified OBGYN <br> 1,407 People Helped';
$lang['DOCTORS_15'] = '4.81 / 5<br>It\'s perfectly normal to develop the first symptoms of menopause in your 40. And if you have other questions on the subject, do not hesitate to ask! Thanks to Medlanes, I am here for you 24/7.';
$lang['DOCTORS_16'] = 'Molly Wearing, MD';
$lang['DOCTORS_17'] = 'Board Certified OBGYN <br> 1,556 People Helped';
$lang['DOCTORS_18'] = '4.95 / 5<br>If you want to know more about the signs of perimenopause and what to do to relieve your symptoms, then ask your questions right away. As an online gynecologist, I will do my best to help you.';
$lang['PAYMENT_NEW4'] = 'Your expert is waiting for your question!';
$lang['PAYMENT_NEW5'] = 'Madison Leal, MD';
$lang['PAYMENT_NEW6'] = 'Board Certified Gynecologist<br> 2,811 People Helped';
$lang['PAYMENT_DOC_IMG'] = '<img src="images/payment/doc-an.jpg" width="180" height="180" alt="#"/>';
$lang['DEP_CC'] = 'Credit Card';
$lang['DEP_PP'] = 'PayPal';
$lang['DEP_PP_TEXT'] = 'You will be redirected to PayPal to complete your payment.';
$lang['THANKYOU_6'] = 'service@medlanes.com';
$lang['THANKYOU_7'] = '(800) 413-7290';
$lang['PRESS_0'] = 'Ask Your Online Gynecologist!';
$lang['PRESS_1'] = 'Our doctors have been extensively trained to advise on a wide range of gynecological issues online. We are happy to help with:';
$lang['PRESS_2'] = '<ul><li><h2>Menstrual Cycle Problems</h2></li><li><h2>Birth Control Questions</h2></li><li><h2>Menopause</h2></li><li><h2>Conception</h2></li><li><h2>Endometriosis</h2></li></ul>';



?>