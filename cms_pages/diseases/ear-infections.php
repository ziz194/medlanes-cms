<?php 	

$lang['php_echo'] = 'Text';
$lang['BXSLIDER'] = "<ul class='staticlink'>";
$lang['HEADER_1'] = 'Our support team is here for you 24/7!';
$lang['HEADER_2'] = '<b>(800) 413-7290</b> or <b>service@medlanes.com</b>';
$lang['INDEX_1'] = 'Help your child fight off an Ear Infection';
$lang['INDEX_2'] = 'Our pediatricians are online now and ready to help you. Submit your question about Ear Infections and feel better today!<b> Your satisfaction is 100% guaranteed.</b>';
$lang['TESTIMONIALS_0'] = 'What Our Patients Are Saying';
$lang['TESTIMONIALS_1'] = '<b>Great pediatrician</b> <br>My online pediatrician provided me with great treatment plan for my ear infection and the pain it causes. Medlanes is great!';
$lang['TESTIMONIALS_2'] = 'Alice Knibbs, 46<br>San Diego, CA';
$lang['TESTIMONIALS_3'] = '<b>Awesome service</b> <br>Otitis media is a very common problem in my children. It causes them pain, difficulty sleeping and headaches. Medlanes has managed to provide help and relief for us though.';
$lang['TESTIMONIALS_4'] = 'Ardella Atkinson, 41<br>Phoenix, AZ';
$lang['TESTIMONIALS_5'] = '<b>Pleasantly surprised</b> <br>I was skeptical using online pediatrician help, but Medlanes has proved me wrong. It was one of the best medical consultations I ever received. My ear infection is gone!';
$lang['TESTIMONIALS_6'] = 'Arran Wright, 44<br>Presque Isle, ME';
$lang['TESTIMONIALS_7'] = '<b>Thank you</b> <br>My son suffered from a middle ear infection until an online Medlanes doctor gave us a consultation. We got so many useful tips on home remedies and now my son is pain free!';
$lang['TESTIMONIALS_8'] = 'Morgan Richardson, 46<br>Los Angeles, CA';
$lang['TESTIMONIALS_9'] = '<b>Reliable service</b> <br>I did not know that ear infections in children are caused by bacterial infections. That is, I did not know it until I consulted an online pediatrician. Now I am aware how to treat such conditions. Thank you Medlanes!';
$lang['TESTIMONIALS_10'] = 'Levi Ferrer, 46<br>Queens, NY';
$lang['TESTIMONIALS_11'] = '<b>Finally healthy</b> <br>It was the third time in the past months that my daughter had ear infection and inflammation. I was so desperate for good medical opinion and Medlanes provided me with exactly that. Thank you!';
$lang['TESTIMONIALS_12'] = 'Ethan Pugliesi, 39<br>Plantation, FL';
$lang['TESTIMONIALS_13'] = '<b>I am so happy</b> <br>My online pediatrician taught me how to fight viral infections. I am very pleased with the service I had been provided with.';
$lang['TESTIMONIALS_14'] = 'Chelsea Wirth, 44<br>Lancaster, PA';
$lang['TESTIMONIALS_15'] = '<b>Awesome doctors</b> <br>I suffer from acute otitis media and Medlanes doctors have been the only people that could provide me proper care and support. Thank you!';
$lang['TESTIMONIALS_16'] = 'Coby Trevisani, 43<br>Greenville, SC';
$lang['DOCTORS_0'] = 'Meet Our Experts';
$lang['DOCTORS_1'] = 'Allison Watters, MD';
$lang['DOCTORS_2'] = 'Board Certified Pediatrician<br> 1,903 People Helped';
$lang['DOCTORS_3'] = '4.81 / 5<br>Now you can consult a doctor online! Just use Medlanes.';
$lang['DOCTORS_4'] = 'Libby Ryder, MD';
$lang['DOCTORS_5'] = 'Board Certified Pediatrician<br> 2,869 People Helped';
$lang['DOCTORS_6'] = '4.91 / 5<br>Medlanes has brought pediatrics online. Now you can reach a doctor 24/7';
$lang['DOCTORS_7'] = 'Jack McKay, MD';
$lang['DOCTORS_8'] = 'Board Certified Pediatrician<br> 1,480 People Helped';
$lang['DOCTORS_9'] = '4.86 / 5<br>If you require help, consult me vie Medlanes 24/7. I am a board-certified pediatrician, trained for online services.';
$lang['DOCTORS_10'] = 'Kelvin Lee, MD';
$lang['DOCTORS_11'] = 'Certified in Pediatrics<br> 1,689 People Helped';
$lang['DOCTORS_12'] = '4.78 / 5<br>I am thankful to Medlanes because as a doctor I can reach patients from any part of the world. Medlanes has revolutionized medicine.';
$lang['DOCTORS_13'] = 'Jayce Timpani, MD';
$lang['DOCTORS_14'] = 'Board Certified - Pediatrics<br> 1,527 People Helped';
$lang['DOCTORS_15'] = '4.95 / 5<br>Medlanes is a great online platform that provides best health care services out there. Contact us!';
$lang['DOCTORS_16'] = 'Kelly Robbins, MD';
$lang['DOCTORS_17'] = 'Board Certified Pediatrician<br> 1,829 People Helped';
$lang['DOCTORS_18'] = '4.89 / 5<br>Being an online pediatrician has allowed me to help so many people. If you suffer, I can help you too!';
$lang['PAYMENT_NEW4'] = 'Your expert is waiting for your question!';
$lang['PAYMENT_NEW5'] = 'Allison Watters, MD';
$lang['PAYMENT_NEW6'] = 'Board Certified Pediatrician<br> 1,903 People Helped';
$lang['PAYMENT_DOC_IMG'] = '<img src="images/payment/doc-an.jpg" width="180" height="180" alt="#"/>';
$lang['DEP_CC'] = 'Credit Card';
$lang['DEP_PP'] = 'PayPal';
$lang['DEP_PP_TEXT'] = 'You will be redirected to PayPal to complete your payment.';
$lang['THANKYOU_6'] = 'service@medlanes.com';
$lang['THANKYOU_7'] = '(800) 413-7290';
$lang['PRESS_0'] = 'Ask Your Online Pediatrician!';
$lang['PRESS_1'] = 'Our expert pediatricians have been extensively trained to support a wide variety of conditions online. We are here for you and your child. Our specialties include:';
$lang['PRESS_2'] = '<ul><li><h2>Rash Analysis</h2></li><li><h2>Mononucleosis</h2></li><li><h2>ADHD</h2></li><li><h2>Fevers</h2></li><li><h2>Ear Infections</h2></li><li><h2>Croup</h2></li><li><h2>and More!</h2></li></ul>';


?>