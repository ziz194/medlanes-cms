<?php
$lang['php_echo'] = 'Text';
$lang['BXSLIDER'] = "<ul class='staticlink'>";
$lang['HEADER_1'] = 'Our support team is here for you 24/7!';
$lang['HEADER_2'] = '<b>(800) 413-7290</b> or <b>service@medlanes.com</b>';
$lang['INDEX_1'] = 'Professional ECG Interpretation help';
$lang['INDEX_2'] = 'Our cardiologists are online now and ready to help you. Submit your question about ECG Interpretation and feel better today!<b> Your satisfaction is 100% guaranteed.</b>';
$lang['TESTIMONIALS_0'] = 'What Our Patients Are Saying';
$lang['TESTIMONIALS_1'] = '<b>Best out there</b> <br>I had an a consultation with an online cardiologist and I was surprised that he had the time to explain properly what ECG interpretation is. I am very impressed with Medlanes!';
$lang['TESTIMONIALS_2'] = 'Lovie Barros, 43<br>Newark, NJ';
$lang['TESTIMONIALS_3'] = '<b>What an amazing service.</b> <br>I am amazed at the possibility Medlanes online platfrom brings to  healthcare. One consultation and I was reffered to have an electrocardiogram. ';
$lang['TESTIMONIALS_4'] = 'Diane Waymire, 31<br>Fremont, CA';
$lang['TESTIMONIALS_5'] = '<b>Big help</b> <br>My online cardiologist from Medlanes has helped me to interpret the results of my electrocardiogram. Very cool, I was impressed that they provide their online services even for little tasks like that.';
$lang['TESTIMONIALS_6'] = 'Muhammad Christie, 39<br>Midland, TX';
$lang['TESTIMONIALS_7'] = '<b>Thank you Medlanes</b> <br>I was really cohnfused with my results, so an online ECG interpretation from Medlanes really helped me out.';
$lang['TESTIMONIALS_8'] = 'Claire Neudorf, 33<br>Trenton, NJ';
$lang['TESTIMONIALS_9'] = '<b>Thank you</b> <br>If you need a second opinion, like for ECG interpretations, Medlanes doctors is what you need. Best online health services ever!';
$lang['TESTIMONIALS_10'] = 'Nicholas Rubio, 40<br>Poughkeepsie, NY';
$lang['TESTIMONIALS_11'] = '<b>Great interpretation</b> <br>I recently had an electrocardogram and did not know what it meant. Online cardiologist at Medlanes provided me with a detailed explanation of my ECG. What a great help, I did not even have to leave my home for that!';
$lang['TESTIMONIALS_12'] = 'Riley Valenzuela, 27<br>Forsyth, GA';
$lang['TESTIMONIALS_13'] = '<b>Exactly what I needed</b> <br>I got my ECG results and being able to upload files is a great feature that Medalnes offers. Without it, I would not have been able to get a medical explanation. Online doctors at medlanes are the best!';
$lang['TESTIMONIALS_14'] = 'Lilly Friedman, 45<br>Winchester, VA';
$lang['TESTIMONIALS_15'] = '<b>Impressive service</b> <br>I recevied an ECG interpretation online via Medlanes. What a quick advice and it was exactly what I needed.';
$lang['TESTIMONIALS_16'] = 'Zain Cardona, 27<br>Albuquerque, NM';
$lang['DOCTORS_0'] = 'Meet Our Experts';
$lang['DOCTORS_1'] = 'Jamie Mahon, MD';
$lang['DOCTORS_2'] = 'Board Certified Cardiologist <br> 2,741 People Helped';
$lang['DOCTORS_3'] = '4.87 / 5<br>It has never been easier to provide people with help and support they need!';
$lang['DOCTORS_4'] = 'Rose Tynes, MD';
$lang['DOCTORS_5'] = 'Board Certified Cardiologist  <br> 1,763 People Helped';
$lang['DOCTORS_6'] = '4.83 / 5<br>Online cardiologist, sounds unreal, but it is possible. I can help you, just contact me via Medlanes.';
$lang['DOCTORS_7'] = 'Jason Santos, MD';
$lang['DOCTORS_8'] = 'Board Certified Cardiologist  <br> 997 People Helped';
$lang['DOCTORS_9'] = '4.78 / 5<br>Heart conditions are extremely serious and require medical attention. Medlanes can provide the care you need!';
$lang['DOCTORS_10'] = 'Archie Rutt, MD';
$lang['DOCTORS_11'] = 'Board Certified Cardiologist  <br> 1,964 People Helped';
$lang['DOCTORS_12'] = '4.77 / 5<br>Online cardiologist at your service. Available 24/7.';
$lang['DOCTORS_13'] = 'John Lundberg, MD';
$lang['DOCTORS_14'] = 'Board Certified Cardiologist  <br> 1,270 People Helped';
$lang['DOCTORS_15'] = '4.96 / 5<br>Medlanes is the platform that makes healthcare affordable. I am proud to be part of it.';
$lang['DOCTORS_16'] = 'Indiana Denison, MD';
$lang['DOCTORS_17'] = 'Board Certified Cardiologist  <br> 2,475 People Helped';
$lang['DOCTORS_18'] = '4.79 / 5 <br>I am so excited to be a part of a revolutionary online doctor platform like Medlanes. We are here for you 24/7!Cardiology is online now! Reach a doctor from your home via Medlanes.';
$lang['PAYMENT_NEW4'] = 'Your expert is waiting for your question!';
$lang['PAYMENT_NEW5'] = 'Jamie Mahon, MD';
$lang['PAYMENT_NEW6'] = 'Board Certified Cardiologist <br> 2,741 People Helped';
$lang['PAYMENT_DOC_IMG'] = '<img src="images/payment/doc-an.jpg" width="180" height="180" alt="#"/>';
$lang['DEP_CC'] = 'Credit Card';
$lang['DEP_PP'] = 'PayPal';
$lang['DEP_PP_TEXT'] = 'You will be redirected to PayPal to complete your payment.';
$lang['THANKYOU_6'] = 'service@medlanes.com';
$lang['THANKYOU_7'] = '(800) 413-7290';
$lang['PRESS_0'] = 'Ask Your Online Cardiologist!';
$lang['PRESS_1'] = 'Our doctors have been extensively trained to treat heart health conditions online! We can also help with:';
$lang['PRESS_2'] = '<ul><li><h2>High Blood Pressure</h2></li><li><h2>Stroke</h2></li><li><h2>Heart Failure</h2></li><li><h2>Heart Health Medication Questions</h2></li><li><h2>Heart Attacks</h2></li></ul>';
?>