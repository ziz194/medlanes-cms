<?php 	

$lang['php_echo'] = 'Text';
$lang['BXSLIDER'] = "<ul class='staticlink'>";
$lang['HEADER_1'] = 'Our support team is here for you 24/7!';
$lang['HEADER_2'] = '<b>(800) 413-7290</b> or <b>service@medlanes.com</b>';
$lang['INDEX_1'] = 'Ask A Cardiologist Now';
$lang['INDEX_2'] = 'Our expert cardiologists are online now and here to help. <br>Simply submit your question to get started.<br><b>Your satisfaction is 100% guaranteed.</b>';
$lang['TESTIMONIALS_0'] = 'What Our Customers Are Saying';
$lang['TESTIMONIALS_1'] = '<b>I regained control.</b> <br>I have had a lot of trouble with my heart in the past. I finally found the perfect heart problems help with Medlanes. The online cardiologists have been great and their service is second to none!';
$lang['TESTIMONIALS_2'] = 'Amber Jung, 42<br>Euclid, OH';
$lang['TESTIMONIALS_3'] = '<b>Kiss Heart Failure Goodbye.</b> <br> I have always noticed certain symptoms of heart failure, but chose to ignore them. Medlanes offers me the chance to talk to a cardiologist online and thanks to them, I have found the perfect heart health support!';
$lang['TESTIMONIALS_4'] = 'Natasha Knaggs, 32<br>Prairie Grove, AR';
$lang['TESTIMONIALS_5'] = '<b>Best Heart Trouble Help!</b> <br>Finally! That is all I have to say. With so many heart problems treatments available, I did not know which one to choose until Dr. Johnson recommended the best treatment option for me. I can finally do the things that I love doing again. Thank you so much! ';
$lang['TESTIMONIALS_6'] = 'Kieran Hall, 40<br>Winchester, KY';
$lang['TESTIMONIALS_7'] = '<b>Finally Feeling Right!</b> <br> Heart issues run in my family. I was not even aware that it was genetic prior to speaking with my cardiologist online through Medlanes. I have finally gotten the answers and support I need to treat my heart issue. Thanks!';
$lang['TESTIMONIALS_8'] = 'Holly Heredia, 33<br>Concord, CA';
$lang['TESTIMONIALS_9'] = '<b>Fewer Heart Issues</b> <br>I was recommended by a friend to try Medlanes who had a lot of success with her heart problems treatment online from Medlanes. I have been working with Dr. Evans ever since. I finally feel like I have balance in my life again.';
$lang['TESTIMONIALS_10'] = 'Gerard Dellucci, 56<br>Brooklyn, NY';
$lang['TESTIMONIALS_11'] = '<b>Best Online Cardiologist</b> <br>Talking with Dr. Forney has been such a blessing. I finally have the tools I need in place to support myself and improve my heart health. I cannot thank Medlanes enough. What a wonderful online cardiologist platform.';
$lang['TESTIMONIALS_12'] = 'Will Beneventi, 30<br>Watertown, SD';
$lang['TESTIMONIALS_13'] = '<b>Ask A Cardiologist Online</b> <br>I will be honest, I thought it was not possible to ask a cardiologist question online to my doctor. After using the service a few times, I can get heart help online 24 hours a day virtually. From anywhere as well. Thank you to Medlanes!';
$lang['TESTIMONIALS_14'] = 'Madeleine Kaestner, 52<br>Akron, OH';
$lang['TESTIMONIALS_15'] = '<b>Heart Issues Medication Help</b> <br>I started taking beta blockers a few weeks ago and was having weird side effects. I had all my medication questions answered, and realized that the side effects were completely normal. This put my mind at ease. Thank you everyone!';
$lang['TESTIMONIALS_16'] = 'Marley Larsen, 24<br>Phoenix, AZ';
$lang['DOCTORS_0'] = 'Meet our Providers';
$lang['DOCTORS_1'] = 'Jamie Mahon, MD';
$lang['DOCTORS_2'] = 'Board Certified Cardiologist <br> 2,741 People Helped';
$lang['DOCTORS_3'] = '4.87 / 5<br>I have over 15 years of experience treating heart problems and conditions that are related to and/or cause heart issues . I have been specially trained to treat heart problems online!';
$lang['DOCTORS_4'] = 'Rose Tynes, MD';
$lang['DOCTORS_5'] = 'Board Certified Cardiologist  <br> 1,763 People Helped';
$lang['DOCTORS_6'] = '4.83 / 5<br> I have treated well over 10,000 cases of heart issues  and related conditions since starting practice. I am an expert in the treatment of heart problems!';
$lang['DOCTORS_7'] = 'Jason Santos, MD';
$lang['DOCTORS_8'] = 'Board Certified Cardiologist  <br> 997 People Helped';
$lang['DOCTORS_9'] = '4.78 / 5<br>Heart problems do not have to control you. My patients have overcome their issues with heart health with specialized heart health treatments that have been custom tailored to meet their specific needs.';
$lang['DOCTORS_10'] = 'Archie Rutt, MD';
$lang['DOCTORS_11'] = 'Board Certified Cardiologist  <br> 1,964 People Helped';
$lang['DOCTORS_12'] = '4.77 / 5<br> Online cardiology is a very new concept and I am excited to be on the ground floor of revolutionizing the treatment of heart issues online and through digital methods.';
$lang['DOCTORS_13'] = 'John Lundberg, MD';
$lang['DOCTORS_14'] = 'Board Certified Cardiologist  <br> 1,270 People Helped';
$lang['DOCTORS_15'] = '4.96 / 5<br>I have been doing online treatment for about 3 years now, and can with with a wide range of issues involving heart health problems. I am here to help!';
$lang['DOCTORS_16'] = 'Indiana Denison, MD';
$lang['DOCTORS_17'] = 'Board Certified Cardiologist  <br> 2,475 People Helped';
$lang['DOCTORS_18'] = '4.79 / 5 <br>I am so excited to be a part of a revolutionary online doctor platform like Medlanes. We are here for you 24/7!';
$lang['PAYMENT_NEW4'] = 'Your expert is waiting for your question!';
$lang['PAYMENT_NEW5'] = 'Jamie Mahon, MD';
$lang['PAYMENT_NEW6'] = 'Board Certified Cardiologist <br> 2,741 People Helped';
$lang['PAYMENT_DOC_IMG'] = '<img src="images/payment/doc-an.jpg" width="180" height="180" alt="#"/>';
$lang['DEP_CC'] = 'Credit Card';
$lang['DEP_PP'] = 'PayPal';
$lang['DEP_PP_TEXT'] = 'You will be redirected to PayPal to complete your payment.';
$lang['THANKYOU_6'] = 'service@medlanes.com';
$lang['THANKYOU_7'] = '(800) 413-7290';
$lang['PRESS_0'] = 'Online Heart Health Support!';
$lang['PRESS_1'] = 'Our doctors have been extensively trained to treat heart health conditions online! We can also help with:';
$lang['PRESS_2'] = '<ul><li><h2>High Blood Pressure</h2></li><li><h2>Stroke</h2></li><li><h2>Heart Failure</h2></li><li><h2>Heart Health Medication Questions</h2></li><li><h2>Heart Attacks</h2></li></ul>';



?>