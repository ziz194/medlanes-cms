<?php 	

$lang['php_echo'] = 'Text';
$lang['BXSLIDER'] = "<ul class='staticlink'>";
$lang['HEADER_1'] = 'Our support team is here for you 24/7!';
$lang['HEADER_2'] = '<b>(800) 413-7290</b> or <b>service@medlanes.com</b>';
$lang['INDEX_1'] = 'Questions about Gestational Diabetes?';
$lang['INDEX_2'] = 'Our gynecologists are online now and ready to help you. Submit your question about Gestational Diabetes and feel better today!<b> Your satisfaction is 100% guaranteed.</b>';
$lang['TESTIMONIALS_0'] = 'What Our Patients Are Saying';
$lang['TESTIMONIALS_1'] = '<b>I am so thankful!</b> <br>Ever since I have been diagnosed with gestational diabetes, I have been worried about my future baby\'s health. I had so many questions. Thanks to Medlanes, I talked to an online gynecologist who answered me right away. He listen to all my doubts and gave me great advice to control my blood sugar level.';
$lang['TESTIMONIALS_2'] = 'Jasmine Myers, 44<br>Harrisburg, PA';
$lang['TESTIMONIALS_3'] = '<b>Put your mind at ease</b> <br>Talking to a gynecologist at Medlanes allowed me to learn more about the symptoms of gestational diabetes. Now I know how to take care of myself and my little baby. I do not need to worry anymore because I know the online doctor will be here for me again if I have more questions';
$lang['TESTIMONIALS_4'] = 'Felicia Cardoso, 49<br>Philadelphia, PA';
$lang['TESTIMONIALS_5'] = '<b>Thank you Medlanes!</b> <br>Thank you Medlanes!';
$lang['TESTIMONIALS_6'] = 'Anthony Vela, 44<br>Kailua, HI';
$lang['TESTIMONIALS_7'] = '<b>Always here for me</b> <br>When I found out I was pregnant, I was extremely worried about my baby\'s health! I really did not want her to suffer from prenatal diabetes. The online gynecologist at Medlanes was amazing! He really put me at ease and made sure I knew how I could control my blood sugar level.';
$lang['TESTIMONIALS_8'] = 'Melissa Crawford, 41<br>Madison, WI';
$lang['TESTIMONIALS_9'] = '<b>Good job Medlanes!</b> <br>My wife is pregnant with our third child. When we found out that she had diabetes, we could not wait to talk to a doctor about it. Our online gynecologist at Medlanes was so helpful! He explained us the causes of gestational diabetes and made sure my wife knew how to manage her blood sugar level.';
$lang['TESTIMONIALS_10'] = 'Ethan Ferguson, 44<br>Casper, WY';
$lang['TESTIMONIALS_11'] = '<b>Serious answers 24/7</b> <br>Thanks to Medlanes, I was able to talk to an online gynecologist. He explained me and my girlfriend all the risk factors for gestational diabetes and how to prevent the condition. Now we know how to keep our future daughter healthy!';
$lang['TESTIMONIALS_12'] = 'Jacob Davidson, 45<br>Washington, MD';
$lang['TESTIMONIALS_13'] = '<b>Simple, easy and quick</b> <br>My 17 year old daughter is expecting her first child. I was worried she may suffer from high blood sugar during her pregnancy. The online gynecologist at Medlanes really put my mind at ease and even gave me some tips to make sure my grandchild would be safe and my daughter could control her blood sugar level.';
$lang['TESTIMONIALS_14'] = 'Madison Powell, 44<br>Grambling, LA';
$lang['TESTIMONIALS_15'] = '<b>Medlanes changed my life!</b> <br>I was so excited when I found out I was going to be a dad. Then my girlfriend told me she was suffering from gestational diabetes mellitus. Talking with an online gynecologist has really helped us both deal with the bad news. She addressed our concerns and promised to be there 24/7, if we needed her!';
$lang['TESTIMONIALS_16'] = 'Alan Smith, 43<br>Philadelphia, PA';
$lang['DOCTORS_0'] = 'Meet Our Experts';
$lang['DOCTORS_1'] = 'Madison Leal, MD';
$lang['DOCTORS_2'] = 'Board Certified Gynecologist<br> 2,811 People Helped';
$lang['DOCTORS_3'] = '4.89 / 5<br>You have questions about the tests for gestational diabetes? You definitely came to the right place! As a trained online gynecologist, it\'s my duty to provide you with all the information you need!';
$lang['DOCTORS_4'] = 'Callie Ross, MD';
$lang['DOCTORS_5'] = 'Board Certified OBGYN <br> 1,760 People Helped';
$lang['DOCTORS_6'] = '4.88 / 5<br>It truly is an honor for me to work with Medlanes! Thanks to their platform I am able to help even more patients. I always do my best to answer all of your questions about women\'s health.';
$lang['DOCTORS_7'] = 'Owen Tyers, MD';
$lang['DOCTORS_8'] = 'Board Certified OBGYN<br> 908 People Helped';
$lang['DOCTORS_9'] = '4.94 / 5<br>As a certified online gynecologist I am here for you when you need me! do not hesitate to contact me, I will tell you everthing there is to know about the signs and symptoms of gestational diabetes';
$lang['DOCTORS_10'] = 'Alvin Lee, MD, MPH';
$lang['DOCTORS_11'] = 'Board Certified OBGYN <br> 1,401 People Helped';
$lang['DOCTORS_12'] = '4.8 / 5<br>As a certified online gynecologist I am here for you when you need me! do not hesitate to contact me, I will tell you everthing there is to know about the signs and symptoms of gestational diabetes';
$lang['DOCTORS_13'] = 'Rasin Sem, MD, MPH';
$lang['DOCTORS_14'] = 'Board Certified OBGYN <br> 1,407 People Helped';
$lang['DOCTORS_15'] = '4.81 / 5<br>As future parents, it is tough to learn that your baby might be suffering from prenatal diabetes. But do not worry, as an experienced gynecologist, I will do my best to support you! ';
$lang['DOCTORS_16'] = 'Molly Wearing, MD';
$lang['DOCTORS_17'] = 'Board Certified OBGYN <br> 1,556 People Helped';
$lang['DOCTORS_18'] = '4.95 / 5<br>You want to know what causes gestational diabetes? Thanks to Medlanes, I am available 24/7 and can answer all of your questions. I am a trained online gynecologist with lots of experience.';
$lang['PAYMENT_NEW4'] = 'Your expert is waiting for your question!';
$lang['PAYMENT_NEW5'] = 'Madison Leal, MD';
$lang['PAYMENT_NEW6'] = 'Board Certified Gynecologist<br> 2,811 People Helped';
$lang['PAYMENT_DOC_IMG'] = '<img src="images/payment/doc-an.jpg" width="180" height="180" alt="#"/>';
$lang['DEP_CC'] = 'Credit Card';
$lang['DEP_PP'] = 'PayPal';
$lang['DEP_PP_TEXT'] = 'You will be redirected to PayPal to complete your payment.';
$lang['THANKYOU_6'] = 'service@medlanes.com';
$lang['THANKYOU_7'] = '(800) 413-7290';
$lang['PRESS_0'] = 'Ask Your Online Gynecologist!';
$lang['PRESS_1'] = 'Our doctors have been extensively trained to advise on a wide range of gynecological issues online. We are happy to help with:';
$lang['PRESS_2'] = '<ul><li><h2>Menstrual Cycle Problems</h2></li><li><h2>Birth Control Questions</h2></li><li><h2>Menopause</h2></li><li><h2>Conception</h2></li><li><h2>Endometriosis</h2></li></ul>';



?>