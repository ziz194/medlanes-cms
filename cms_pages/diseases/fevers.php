<?php 	

$lang['php_echo'] = 'Text';
$lang['BXSLIDER'] = "<ul class='staticlink'>";
$lang['HEADER_1'] = 'Our support team is here for you 24/7!';
$lang['HEADER_2'] = '<b>(800) 413-7290</b> or <b>service@medlanes.com</b>';
$lang['INDEX_1'] = 'Ask about Fevers';
$lang['INDEX_2'] = 'Our pediatricians are online now and ready to help you. Submit your question about Fever and feel better today!<b> Your satisfaction is 100% guaranteed.</b>';
$lang['TESTIMONIALS_0'] = 'What Our Patients Are Saying';
$lang['TESTIMONIALS_1'] = '<b>Great service</b> <br>My daughter has had high fever for quite some time. Nothing I tried worked so I canted Medlanes online pediatrician for a consultation. It was the best help I ever received!';
$lang['TESTIMONIALS_2'] = 'Helene Endrizzi, 42<br>Louisville, KY';
$lang['TESTIMONIALS_3'] = '<b>Medlanes rules</b> <br>My toddler had a high fever and I felt so lost. It was only help from Medlanes that made my baby and me feel better.';
$lang['TESTIMONIALS_4'] = 'Helene Pisani, 49<br>Panama City, FL';
$lang['TESTIMONIALS_5'] = '<b>Medlanes saved us</b> <br>After a family vacation my husband came home with dengue fever. It seemed that the treatment was not working, so I consulted Medlanes. It was such quick and comforting service. I could not be any happier!';
$lang['TESTIMONIALS_6'] = 'Tyson Lee, 49<br>Lafayette, LA';
$lang['TESTIMONIALS_7'] = '<b>Very reliable</b> <br>I had high temperature and could not leave home, because it made me feel so bad. Luckily, I could consult an online doctor at Medlanes and he gave the right tips how to battle my fever.';
$lang['TESTIMONIALS_8'] = 'Amber Costa, 41<br>Burbank, CA';
$lang['TESTIMONIALS_9'] = '<b>Peace of mind</b> <br>Fever in children is common, but each time it requires different approach. That is why I always turn to Medlanes online pediatrician for help!';
$lang['TESTIMONIALS_10'] = 'Harvey Montero, 46<br>Salt Lake City, UT';
$lang['TESTIMONIALS_11'] = '<b>I am so grateful</b> <br>My son was suffering from low fever and his body temperature was different. As as mother, I was very worried, but my online doctor reassured me about the condition and provided great treatment steps.';
$lang['TESTIMONIALS_12'] = 'Jordan Cunha, 39<br>Corpus Christi, TX';
$lang['TESTIMONIALS_13'] = '<b>Medical experts online</b> <br>When I had fever and I did not suspect that there would be an underlying cause to it. Only after talking to a Medlanes pediatrician I realised that I had an infection and got the right treatment for it.';
$lang['TESTIMONIALS_14'] = 'Catherine Busch, 46<br>Luther, OK';
$lang['TESTIMONIALS_15'] = '<b>Great online help</b> <br>I had the flu and with that came adult fever. It did not seem serious, but I consulted an online doctor anyway. It was the right decision, because Medlanes provided me with the best information imaginable.';
$lang['TESTIMONIALS_16'] = 'Jayden Abrego, 43<br>Kiowa, OK';
$lang['DOCTORS_0'] = 'Meet Our Experts';
$lang['DOCTORS_1'] = 'Allison Watters, MD';
$lang['DOCTORS_2'] = 'Board Certified Pediatrician<br> 1,903 People Helped';
$lang['DOCTORS_3'] = '4.81 / 5<br>Fever is often scary. I am here to provide help and support.';
$lang['DOCTORS_4'] = 'Libby Ryder, MD';
$lang['DOCTORS_5'] = 'Board Certified Pediatrician<br> 2,869 People Helped';
$lang['DOCTORS_6'] = '4.91 / 5<br>Online medicine is possible and Medlanes is here to make it happen. I am glad to be a part of it!';
$lang['DOCTORS_7'] = 'Jack McKay, MD';
$lang['DOCTORS_8'] = 'Board Certified Pediatrician<br> 1,480 People Helped';
$lang['DOCTORS_9'] = '4.86 / 5<br>Online pediatrician? Yes, I am here and I am ready to help you 24/7. Reach me via Medlanes';
$lang['DOCTORS_10'] = 'Kelvin Lee, MD';
$lang['DOCTORS_11'] = 'Certified in Pediatrics<br> 1,689 People Helped';
$lang['DOCTORS_12'] = '4.78 / 5<br>I am board-certified online doctor and I am more than happy to help you!';
$lang['DOCTORS_13'] = 'Jayce Timpani, MD';
$lang['DOCTORS_14'] = 'Board Certified - Pediatrics<br> 1,527 People Helped';
$lang['DOCTORS_15'] = '4.95 / 5<br>Medlanes allows me to help people all over the world. I can help you too. If you have fever or the flu, contact me.';
$lang['DOCTORS_16'] = 'Kelly Robbins, MD';
$lang['DOCTORS_17'] = 'Board Certified Pediatrician<br> 1,829 People Helped';
$lang['DOCTORS_18'] = '4.89 / 5<br>Any health issues, even as little as fever require attention. Contact me via Medlanes today.';
$lang['PAYMENT_NEW4'] = 'Your expert is waiting for your question!';
$lang['PAYMENT_NEW5'] = 'Allison Watters, MD';
$lang['PAYMENT_NEW6'] = 'Board Certified Pediatrician<br> 1,903 People Helped';
$lang['PAYMENT_DOC_IMG'] = '<img src="images/payment/doc-an.jpg" width="180" height="180" alt="#"/>';
$lang['DEP_CC'] = 'Credit Card';
$lang['DEP_PP'] = 'PayPal';
$lang['DEP_PP_TEXT'] = 'You will be redirected to PayPal to complete your payment.';
$lang['THANKYOU_6'] = 'service@medlanes.com';
$lang['THANKYOU_7'] = '(800) 413-7290';
$lang['PRESS_0'] = 'Ask Your Online Pediatrician!';
$lang['PRESS_1'] = 'Our expert pediatricians have been extensively trained to support a wide variety of conditions online. We are here for you and your child. Our specialties include:';
$lang['PRESS_2'] = '<ul><li><h2>Rash Analysis</h2></li><li><h2>Mononucleosis</h2></li><li><h2>ADHD</h2></li><li><h2>Fevers</h2></li><li><h2>Ear Infections</h2></li><li><h2>Croup</h2></li><li><h2>and More!</h2></li></ul>';


?>