<?php 	

$lang['BXSLIDER'] = "<ul class='staticlink'>";
$lang['HEADER_1'] = 'Our support team is here for you 24/7!';
$lang['HEADER_2'] = '<b>(800) 413-7290</b> or <b>service@medlanes.com</b>';
$lang['INDEX_1'] = 'Ask A Pediatrician Now';
$lang['INDEX_2'] = 'Expert pediatricians are online now an available for your questions. <br>Put your mind at ease, ask your question now. <br><b>Your satisfaction is 100% guaranteed.</b>';
$lang['TESTIMONIALS_0'] = 'What Our Patients Are Saying';
$lang['TESTIMONIALS_1'] = '<b>Skipped the ER</b> <br>Thanks on my online pediatrician, I skipped an unnecessary trip to the ER at 4 in the morning. Thank you, Medlanes!';
$lang['TESTIMONIALS_2'] = 'Jill Melo, 41<br>Wolverhampton';
$lang['TESTIMONIALS_3'] = '<b>Simply Amazing</b> <br>My son had a horrible rash and would not stop scratching. One quick moment with my online pediatrician, and we had the solution!';
$lang['TESTIMONIALS_4'] = 'Tayla Trout, 39<br>Barnsley';
$lang['TESTIMONIALS_5'] = '<b>Online Pediatrician</b> <br>We have a newborn, and every little fever and cough is nerve wracking. Thanks to Medlanes, I have my pediatrician on call when I need him!';
$lang['TESTIMONIALS_6'] = 'Christopher De Luca, 39<br>Stoke-on-Trent';
$lang['TESTIMONIALS_7'] = '<b>Wonderful</b> <br>Having access to a pediatrician online has been very comforting to me. As a new parent, having Medlanes on my side has been a blessing in disguise.';
$lang['TESTIMONIALS_8'] = 'Erin Ferrari, 43<br>North Lanarkshire';
$lang['TESTIMONIALS_9'] = '<b>My Pediatrician</b> <br>Is wonderful! I have worked with Dr. Keller for over a year now almost. Medlanes has made consulting my pediatrician online easy and affordable.';
$lang['TESTIMONIALS_10'] = 'Robert Geils, 41<br>South Gloucestershire';
$lang['TESTIMONIALS_11'] = '<b>Online Doctors</b> <br>Honestly, I thought seeing a doctor online was extremely unrealistic. My online pediatrician with Medlanes has proved me otherwise. Thank you for everything!';
$lang['TESTIMONIALS_12'] = 'Cillian Souza, 49<br>North East Lincolnshire';
$lang['TESTIMONIALS_13'] = '<b>Zero Ear Infections</b> <br>My pediatrician and I were working for over a week trying to help my daughter with ear infections. Thanks to her, we found out she was allergic to down feathers. No more ear infections!';
$lang['TESTIMONIALS_14'] = 'Lilian Alves, 40<br>Derby';
$lang['TESTIMONIALS_15'] = '<b>Life Saver</b> <br>My son was having some issues wetting the bed. I spoke with my online pediatrician, and came up with the best solution thus far. No more night-time accidents!';
$lang['TESTIMONIALS_16'] = 'Aaron Trevisani, 40<br>South Somerset';
$lang['DOCTORS_0'] = 'Meet Our Experts';
$lang['DOCTORS_1'] = 'Allison Watters, MD';
$lang['DOCTORS_2'] = 'Board Certified Pediatrician<br> 1,903 People Helped';
$lang['DOCTORS_3'] = '4.81 / 5<br>I have the chance to help so many more children via this platform. I am so happy I can provide my services online.';
$lang['DOCTORS_4'] = 'Libby Ryder, MD';
$lang['DOCTORS_5'] = 'Board Certified Pediatrician<br> 2,869 People Helped';
$lang['DOCTORS_6'] = '4.91 / 5<br>Issues involving your child are always scary. Access to online pediatricians take away so much anxiety. Consult us today!';
$lang['DOCTORS_7'] = 'Jack McKay, MD';
$lang['DOCTORS_8'] = 'Board Certified Pediatrician<br> 1,480 People Helped';
$lang['DOCTORS_9'] = '4.86 / 5<br>I feel like I have help more of my little patients online than in my 17 years of actual practice. ';
$lang['DOCTORS_10'] = 'Kelvin Lee, MD';
$lang['DOCTORS_11'] = 'Certified in Pediatrics<br> 1,689 People Helped';
$lang['DOCTORS_12'] = '4.78 / 5<br>I am here to provide answers to all of your pediatric questions! Ask me now!';
$lang['DOCTORS_13'] = 'Jayce Timpani, MD';
$lang['DOCTORS_14'] = 'Board Certified - Pediatrics<br> 1,527 People Helped';
$lang['DOCTORS_15'] = '4.95 / 5<br>I am specially trained as an online pediatrician, and look forward to assisting you with anything you may need.';
$lang['DOCTORS_16'] = 'Kelly Robbins, MD';
$lang['DOCTORS_17'] = 'Board Certified Pediatrician<br> 1,829 People Helped';
$lang['DOCTORS_18'] = '4.89 / 5<br>I am proud of offer all of my pediatrician services online, 24 hours a day, 7 days a week!';
$lang['PAYMENT_NEW4'] = 'Your expert is waiting for your question!';
$lang['PAYMENT_NEW5'] = 'Allison Watters, MD';
$lang['PAYMENT_NEW6'] = 'Board Certified Pediatrician<br> 1,903 People Helped';
$lang['PAYMENT_DOC_IMG'] = '<img src="images/payment/doc-an.jpg" width="180" height="180" alt="#"/>';
$lang['DEP_CC'] = 'Credit Card';
$lang['DEP_PP'] = 'PayPal';
$lang['DEP_PP_TEXT'] = 'You will be redirected to PayPal to complete your payment.';
$lang['THANKYOU_6'] = 'service@medlanes.com';
$lang['THANKYOU_7'] = '(800) 413-7290';
$lang['PRESS_0'] = 'Online Pediatricians at your Service';
$lang['PRESS_1'] = 'Our expert pediatricians have been extensively trained to support a wide variety of conditions online. We are here for you and your child. Our specialties include:';
$lang['PRESS_2'] = '<ul><li><h2>Rash Analysis</h2></li><li><h2>Mononucleosis</h2></li><li><h2>ADHD</h2></li><li><h2>Fevers</h2></li><li><h2>Ear Infections</h2></li><li><h2>Croup</h2></li><li><h2>and More!</h2></li></ul>';



?>