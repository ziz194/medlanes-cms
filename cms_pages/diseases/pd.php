<?php 	

$lang['BXSLIDER'] = "<ul class='staticlink'>";
$lang['HEADER_1'] = 'Our support team is here for you 24/7!';
$lang['HEADER_2'] = '<b>(800) 413-7290</b> or <b>service@medlanes.com</b>';
$lang['INDEX_1'] = 'Panic Disorder ends here.';
$lang['INDEX_2'] = 'Our expert psychologists and psychiatrists are online now and here to help. <br>Stop suffering from panic attacks. Submit your question and feel better today!<br><b>Your satisfaction is 100% guaranteed.</b>';
$lang['TESTIMONIALS_0'] = 'We Beat Panic Disorder';
$lang['TESTIMONIALS_1'] = '<b>No More Panic</b> <br>I have never been able to find an effective treatment for panic disorder until I started working with Dr. Johnson. I finally have an effective way to treat my panic attacks, and she is always available for me.';
$lang['TESTIMONIALS_2'] = 'Mary Watkins, 54 <br> Salt Lake City, UT';
$lang['TESTIMONIALS_3'] = '<b>Goodbye Panic Disorder</b> <br>I have struggled with panic attacks my whole life, to the point where I was losing a lot of the joy I had for living. Thanks to Medlanes, I found the perfect panic disorder treatment online, and feel a world better.';
$lang['TESTIMONIALS_4'] = 'Kelly Reynolds, 28 <br> Ann Arbor, MI';
$lang['TESTIMONIALS_5'] = '<b>Controlling My Panic</b> <br>I finally have an effective method to treat my panic disorder from home, and access to my psychologist online from the comfort of my own home. Thank you Dr. E and to the Medlanes Support Team!';
$lang['TESTIMONIALS_6'] = 'Jakub Jankowski, 35 <br> St. Louis, MO';
$lang['TESTIMONIALS_7'] = '<b>Understand Panic Disorder</b> <br>Before speaking to Dr. Johnson, I was scared to having panic attacks because I did not understand the symptoms of panic disorder or the science behind it. Just knowing how they work gives me peace of mind.';
$lang['TESTIMONIALS_8'] = 'Jane Griffin, 32 <br> Charleston, WV';
$lang['TESTIMONIALS_9'] = '<b>Medication Confusion</b> <br>I was given medication for panic disorder treatment, but my normal doctor did not explain in a method I understood. I now understand everything about my medication, and how to treat panic disorder.';
$lang['TESTIMONIALS_10'] = 'Bob Larkin <br> South Bend, IN';
$lang['TESTIMONIALS_11'] = '<b>Overcoming Agoraphobia</b> <br>The thought of just having a panic attack deterred me from doing things I love. Now that I have found the perfect way to treat panic disorder, I am slowly getting my life back to normal. Thank you Dr. A!';
$lang['TESTIMONIALS_12'] = 'Theo Rossi, 29 <br>Tulsa, OK';
$lang['TESTIMONIALS_13'] = '<b>Panic Attack Treatment</b> <br>I did not think I would ever find an effective panic disorder treatment online, but then came Medlanes. I finally have the panic attack support I have wanted for so long, but never got. Thank you to everyone at Medlanes.';
$lang['TESTIMONIALS_14'] = 'Tammy Swanson, 54<br> Portland, OR';
$lang['TESTIMONIALS_15'] = '<b>Online Psychiatrist</b> <br>The medications I had received for my panic attack treatment were causing me lots of side effects. Thanks to the support of Medlanes, I have switched my medication and have a whole new way of panic attack treatment.';
$lang['TESTIMONIALS_16'] = 'Woody James-White, 25<br> Reno, NV';
$lang['DOCTORS_0'] = 'Meet our Providers';
$lang['DOCTORS_1'] = 'Agatha Andrews, MD';
$lang['DOCTORS_2'] = 'Board Certified Psychiatrist <br> 2,873+ People Helped';
$lang['DOCTORS_3'] = '4.85 / 5<br><br>I have over 15 years of experience treating panic disorder and conditions that are related to and/or cause anxiety. I have been specially trained to treat panic disorder online!';
$lang['DOCTORS_4'] = 'Heather Johnson, Ph.D.';
$lang['DOCTORS_5'] = 'Board Certified Psychotherapist <br> 1,611 People Helped';
$lang['DOCTORS_6'] = '4.92 / 5<br><br>I have treated well over 10,000 cases of panic disorder and related conditions since starting practice. I am an expert in the treatment of panic disorder!';
$lang['DOCTORS_7'] = 'Thomas Sandrini, MD';
$lang['DOCTORS_8'] = 'Diplomat of Psychiatry <br> 918 People Helped';
$lang['DOCTORS_9'] = '4.71 / 5<br><br>Panic attacks do not have to control you. My patients have overcome their issues with panic disorder with specialized panic attacks treatments that have been custom tailored to meet their specific needs.';
$lang['DOCTORS_10'] = 'Erwin Williams, MD, MBA';
$lang['DOCTORS_11'] = 'Board Certified Psychiatrist <br> 873+ People Helped';
$lang['DOCTORS_12'] = '4.8 / 5<br><br>Online psychiatry and online psychology are both very new concepts and I am excited to be on the ground floor of revolutionizing the treatment of panic disorder online and through digital methods.';
$lang['DOCTORS_13'] = 'Jason Evans, Ph.D., MS';
$lang['DOCTORS_14'] = 'Certified Couples Therapist <br> 1,920 Couples Helped';
$lang['DOCTORS_15'] = '4.81 / 5<br><br>I have been doing online therapy for about 3 years now, and can with with a wide range of issues involving panic disorder. I am here to help!';
$lang['DOCTORS_16'] = 'Wilma Forney, MD';
$lang['DOCTORS_17'] = 'Board Certified Psychiatrist <br> 1,001 People Helped';
$lang['DOCTORS_18'] = '4.78 / 5<br><br>I am so excited to be a part of a revolutionary online doctor platform like Medlanes. We are here for you 24/7!';
$lang['PAYMENT_NEW4'] = 'Your expert is waiting for your question!';
$lang['PAYMENT_NEW5'] = 'Agatha Andrews, MD';
$lang['PAYMENT_NEW6'] = 'Board Certified Psychiatrist <br> 2,873+ People Helped';
$lang['PAYMENT_DOC_IMG'] = '<img src="images/payment/doc-an.jpg" width="180" height="180" alt="#"/>';
$lang['DEP_CC'] = 'Credit Card';
$lang['DEP_PP'] = 'PayPal';
$lang['DEP_PP_TEXT'] = 'You will be redirected to PayPal to complete your payment.';
$lang['THANKYOU_6'] = 'service@medlanes.com';
$lang['THANKYOU_7'] = '(800) 413-7290';
$lang['PRESS_0'] = 'Online Mental Health Support!';
$lang['PRESS_1'] = 'Our doctors have been extensively trained to treat mental health conditions online! We can also help with:';
$lang['PRESS_2'] = '<ul><li>Depression Support</li><li>Fears and Phobias</li><li>Behavioral Issues</li><li>Psychiatric Medication Questions</li><li>Anxiety</li><li>Panic Attacks</li</ul>';



?>