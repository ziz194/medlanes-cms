<?php 	

$lang['php_echo'] = 'Text';
$lang['BXSLIDER'] = "<ul class='staticlink'>";
$lang['HEADER_1'] = 'Our support team is here for you 24/7!';
$lang['HEADER_2'] = '<b>(800) 413-7290</b> or <b>service@medlanes.com</b>';
$lang['INDEX_1'] = 'Allergy in child? We can help you.';
$lang['INDEX_2'] = 'Our pediatricians are online now and ready to help you. Submit your question about Allergies and feel better today!<b> Your satisfaction is 100% guaranteed.</b>';
$lang['TESTIMONIALS_0'] = 'What Our Patients Are Saying';
$lang['TESTIMONIALS_1'] = '<b>Simply the best!</b> <br>Each spring hay fever hits me and I do not know how to deal with it. Then, I heard about Medlanes and gave it a shot and guess what, my hay fever is under control.';
$lang['TESTIMONIALS_2'] = 'Isla Solórzano, 48<br>California, CA';
$lang['TESTIMONIALS_3'] = '<b>Love it!</b> <br>I was healthy all my online and only recently I developed a shellfish allergy. I did not know what to do, but my online pediatrician provided me with perfect help. Thank you Medlanes!';
$lang['TESTIMONIALS_4'] = 'Rosa Mashman, 40<br>Jacksonville, FL';
$lang['TESTIMONIALS_5'] = '<b>Great support</b> <br>My children suffer from different allergies and it is hard on us. I am grateful to Medlanes for being there for me!';
$lang['TESTIMONIALS_6'] = 'Elliot Hoy, 43<br>Lomita, CA';
$lang['TESTIMONIALS_7'] = '<b>Thank you</b> <br>I have had atopic dermatitis as long as I remember. It is a real struggle, but my online pediatrician has provided me with useful information about home remedies. My condition is under control now!';
$lang['TESTIMONIALS_8'] = 'Laura Jung, 42<br>Boston, MA';
$lang['TESTIMONIALS_9'] = '<b>I am happy</b> <br>Online pediatrician helped me to get my eczema under control. I am very impressed with Medlanes services.';
$lang['TESTIMONIALS_10'] = 'Anthony Rothstein, 42<br>Tompkinsville, KY';
$lang['TESTIMONIALS_11'] = '<b>Great service</b> <br>I was having some really bad symptoms of allergies, like hives and wheezing, Medlanes provided me with the right treatment plan and now I feel a lot better! Thank you!';
$lang['TESTIMONIALS_12'] = 'Adam Gould, 42<br>Springfield, MA';
$lang['TESTIMONIALS_13'] = '<b>Impressive medical response</b> <br>Skip the visit to a doctor\'s office. Use medlanes instead, my online pediatrician provided me with a great consultation! My allergies are under control now!';
$lang['TESTIMONIALS_14'] = 'Jade Iadanza, 44<br>Cheshire, CT';
$lang['TESTIMONIALS_15'] = '<b>Savers!</b> <br>I have always wanted to be a chef, but I suffer from food allergies. Medlanes saved my dream, because it taught me how to deal with my condition. Thank you!';
$lang['TESTIMONIALS_16'] = 'Jeffrey Grunwald, 44<br>Botkins, OH';
$lang['DOCTORS_0'] = 'Meet Our Experts';
$lang['DOCTORS_1'] = 'Allison Watters, MD';
$lang['DOCTORS_2'] = 'Board Certified Pediatrician<br> 1,903 People Helped';
$lang['DOCTORS_3'] = '4.81 / 5<br>Medlanes and I have brought pediatrics online. If you need help, we will be there for you!';
$lang['DOCTORS_4'] = 'Libby Ryder, MD';
$lang['DOCTORS_5'] = 'Board Certified Pediatrician<br> 2,869 People Helped';
$lang['DOCTORS_6'] = '4.91 / 5<br>Online pediatrics is the future. If you or your child has fallen sick, contact me via Medlanes.';
$lang['DOCTORS_7'] = 'Jack McKay, MD';
$lang['DOCTORS_8'] = 'Board Certified Pediatrician<br> 1,480 People Helped';
$lang['DOCTORS_9'] = '4.86 / 5<br>I am happy to provide my expert pediatric opinion on any issues you might have. Contact me via Medlanes.';
$lang['DOCTORS_10'] = 'Kelvin Lee, MD';
$lang['DOCTORS_11'] = 'Certified in Pediatrics<br> 1,689 People Helped';
$lang['DOCTORS_12'] = '4.78 / 5<br>Medlanes is a great online platform for quick doctor\'s consultations. Contact us!';
$lang['DOCTORS_13'] = 'Jayce Timpani, MD';
$lang['DOCTORS_14'] = 'Board Certified - Pediatrics<br> 1,527 People Helped';
$lang['DOCTORS_15'] = '4.95 / 5<br>Working with Medlanes has allowed me to help more people that I would have from office visits. Use an online doctor today!';
$lang['DOCTORS_16'] = 'Kelly Robbins, MD';
$lang['DOCTORS_17'] = 'Board Certified Pediatrician<br> 1,829 People Helped';
$lang['DOCTORS_18'] = '4.89 / 5<br>We made telehealth happen! Medlanes is the answer to any health related issues you might have!';
$lang['PAYMENT_NEW4'] = 'Your expert is waiting for your question!';
$lang['PAYMENT_NEW5'] = 'Allison Watters, MD';
$lang['PAYMENT_NEW6'] = 'Board Certified Pediatrician<br> 1,903 People Helped';
$lang['PAYMENT_DOC_IMG'] = '<img src="images/payment/doc-an.jpg" width="180" height="180" alt="#"/>';
$lang['DEP_CC'] = 'Credit Card';
$lang['DEP_PP'] = 'PayPal';
$lang['DEP_PP_TEXT'] = 'You will be redirected to PayPal to complete your payment.';
$lang['THANKYOU_6'] = 'service@medlanes.com';
$lang['THANKYOU_7'] = '(800) 413-7290';
$lang['PRESS_0'] = 'Ask Your Online Pediatrician!';
$lang['PRESS_1'] = 'Our expert pediatricians have been extensively trained to support a wide variety of conditions online. We are here for you and your child. Our specialties include:';
$lang['PRESS_2'] = '<ul><li><h2>Rash Analysis</h2></li><li><h2>Mononucleosis</h2></li><li><h2>ADHD</h2></li><li><h2>Fevers</h2></li><li><h2>Ear Infections</h2></li><li><h2>Croup</h2></li><li><h2>and More!</h2></li></ul>';


?>