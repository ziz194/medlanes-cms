<?php 	

$lang['php_echo'] = 'Text';
$lang['BXSLIDER'] = "<ul class='staticlink'>";
$lang['HEADER_1'] = 'Our support team is here for you 24/7!';
$lang['HEADER_2'] = '<b>(800) 413-7290</b> or <b>service@medlanes.com</b>';
$lang['INDEX_1'] = 'Questions about Bronchitis?';
$lang['INDEX_2'] = 'Our pulmonologists are online now and ready to help you. Submit your question about Bronchitis and feel better today!<b> Your satisfaction is 100% guaranteed.</b>';
$lang['TESTIMONIALS_0'] = 'What Our Patients Are Saying';
$lang['TESTIMONIALS_1'] = '<b>Thankful!</b> <br>My grandfather died of a lung infection many years ago. I knew about bronchitis and did not know if bronchitis can lead to pneumonia. My Doctor at Medlanes was able to calm me down, advise me on the right treatment and in no time I was back on my feet again. ';
$lang['TESTIMONIALS_2'] = 'Lillian Johnstone, 48<br>Casper, WY';
$lang['TESTIMONIALS_3'] = '<b>Breathing freely again. </b> <br>I was feeling terrible and had no energy left to leave the house for days. My online doctor I gave me the information I needed and was able to get the right treatment for my actue bronchitis. ';
$lang['TESTIMONIALS_4'] = 'Barbara Barros, 44<br>Tulsa, OK';
$lang['TESTIMONIALS_5'] = '<b>Finally quick medical help</b> <br>I was not sure when to do against my breathing difficulties. In no time my online doctor got back to me and informed me that I had a bronchitis and needed antibiotics treatment.';
$lang['TESTIMONIALS_6'] = 'Xavier Scholz, 48<br>Birmingham, AL';
$lang['TESTIMONIALS_7'] = '<b>Real information instead of just medication</b> <br>Chronic bronchitis is a huge hastle. I was so thankful that the doctor at Medlanes was there for me to give me a full information package on what I should avoid and the treatment I needed. ';
$lang['TESTIMONIALS_8'] = 'Poppy Rizzo, 49<br>Grand Rapids, MI';
$lang['TESTIMONIALS_9'] = '<b></b> <br>After months of suffering from intense cough, I finally got some quick answers from my online doctor. He really took the time to define and explain what chronic bronchitis is and how it is treated.  ';
$lang['TESTIMONIALS_10'] = 'Gavin Hynes, 46<br>Colorado Springs, CO';
$lang['TESTIMONIALS_11'] = '<b>No more waiting rooms</b> <br>If you know what acute bronchitis is, you know that it is extremely tedious. I was so greatful that I did not have to carry myself to the doctors office, but could get advice on my treatment directly online.';
$lang['TESTIMONIALS_12'] = 'Ray Esposito, 38<br>Jonesboro, AR';
$lang['TESTIMONIALS_13'] = '<b>I can sing again!</b> <br>As a singer I was very scared that I had something seriously wrong with me that could affect my voice permanently.I told my online Doctor about my symptoms he told me I have a bronchitis. Only a few days after I began my treatment I was able to sing again like before.';
$lang['TESTIMONIALS_14'] = 'Caitlin Padovano, 39<br>Monroe, LA';
$lang['TESTIMONIALS_15'] = '<b>Physicians who care</b> <br>My husband had a cough and chest pain for days. He was so busy with work, that he refused to go see a doctor. We sat down one evening and asked the doctor at Medlanes. He was able to help us immediatly!  ';
$lang['TESTIMONIALS_16'] = 'Henry Lorenzo, 39<br>Marquette, MI';
$lang['DOCTORS_0'] = 'Meet Our Experts';
$lang['DOCTORS_1'] = 'Celine Karlsson, MD';
$lang['DOCTORS_2'] = 'Board Certified Pulmonologist <br> 1,903 People Helped';
$lang['DOCTORS_3'] = '4.81 / 5<br>Pateints with chest pain and dry cough do not want to wait for weeks before they get an appointment. Online I can give them advice immediately without waisting precious time.  ';
$lang['DOCTORS_4'] = 'Leslie Tomaszewski, MD';
$lang['DOCTORS_5'] = 'Board Certified Pulmonologist <br> 2,869 People Helped';
$lang['DOCTORS_6'] = '4.91 / 5<br>Medlanes has offered me a great opportunity to be there for patients outside the rigid strucutures of the hospital. It gives me a lot of freedom to help people when they need it.';
$lang['DOCTORS_7'] = 'Arthur Forester, MD';
$lang['DOCTORS_8'] = 'Board Certified Pulmonologist <br> 1,480 People Helped';
$lang['DOCTORS_9'] = '4.86 / 5<br>I have been looking for an opportunity like Medlanes for a long time. It really feels wonderful to be part of this great project and bring medicine directly home to the patient.';
$lang['DOCTORS_10'] = 'Alexander Hayward, MD';
$lang['DOCTORS_11'] = 'Board Certified Pulmonologist <br> 1,689 People Helped';
$lang['DOCTORS_12'] = '4.78 / 5<br>The symptoms of bronchitis can be difficult to interpret. Good medcal practice takes time. Working as an online Doctor I am not stressed to run from one patient to another.';
$lang['DOCTORS_13'] = 'Larry Bavin, MD';
$lang['DOCTORS_14'] = 'Board Certified Pulmonologist <br> 1,527 People Helped';
$lang['DOCTORS_15'] = '4.95 / 5<br>If left untreated for too long, even a normal cough can turn into a serious bronchitis. Ask us now, before things get uncomfortable. ';
$lang['DOCTORS_16'] = 'Yasmin Barros, MD';
$lang['DOCTORS_17'] = 'Board Certified Pulmonologist <br> 1,829 People Helped';
$lang['DOCTORS_18'] = '4.89 / 5<br>Patients often are not aware of the causes of bronchitis. It is important to keep them aware of what they can do to protect themselves. I can give them this information quickly online.  ';
$lang['PAYMENT_NEW4'] = 'Your expert is waiting for your question!';
$lang['PAYMENT_NEW5'] = 'Celine Karlsson, MD';
$lang['PAYMENT_NEW6'] = 'Board Certified Pulmonologist <br> 1,903 People Helped';
$lang['PAYMENT_DOC_IMG'] = '<img src="images/payment/doc-an.jpg" width="180" height="180" alt="#"/>';
$lang['DEP_CC'] = 'Credit Card';
$lang['DEP_PP'] = 'PayPal';
$lang['DEP_PP_TEXT'] = 'You will be redirected to PayPal to complete your payment.';
$lang['THANKYOU_6'] = 'service@medlanes.com';
$lang['THANKYOU_7'] = '(800) 413-7290';
$lang['PRESS_0'] = 'Ask Your Online Pulmonologist!';
$lang['PRESS_1'] = 'Our doctors have been extensively trained to treat pulmonary health conditions online! We can also help with:';
$lang['PRESS_2'] = '<ul><li><h2>COPD</h2></li><li><h2>Asthma</h2></li><li><h2>Bronchitis</h2></li><li><h2>Pulmonary Medication Questions</h2></li><li><h2>Emphysema</h2></li></ul>';


?>