<?php 	

$lang['php_echo'] = 'Text';
$lang['BXSLIDER'] = "<ul class='staticlink'>";
$lang['HEADER_1'] = 'Our support team is here for you 24/7!';
$lang['HEADER_2'] = '<b>(800) 413-7290</b> or <b>service@medlanes.com</b>';
$lang['INDEX_1'] = 'Ask A Pulmonologist Now';
$lang['INDEX_2'] = 'Our expert pulmonologists are online now and here to help. <br>Stop suffering from pulmonary conditions. Submit your question and feel better today!<br><b>Your satisfaction is 100% guaranteed.</b>';
$lang['TESTIMONIALS_0'] = 'We Are Customers Are Saying';
$lang['TESTIMONIALS_1'] = '<b>I Control My Pulmonary Condition</b> <br>I have had a lot of trouble with my pulmonary condition in the past. I finally found the perfect pulmonary disease treatment online with Medlanes. The doctors have been great and their service is second to none!';
$lang['TESTIMONIALS_2'] = 'Sallie Quiñones, 45<br>Blackpool';
$lang['TESTIMONIALS_3'] = '<b>Kiss Pulmonary Symptoms Goodbye</b> <br>I have always shown symptoms of a pulmonary condition, but never wanted to talk to a doctor face to face. Medlanes is the best online pulmonologist platform around and thanks to them, have found the perfect pulmonary health support!';
$lang['TESTIMONIALS_4'] = 'Barbara Norris, 24<br>Middlesbrough';
$lang['TESTIMONIALS_5'] = '<b>Best Pulmonary Health Help!</b> <br>Finally! That is all I have to say. Treatments for pulmonary conditions are confusing and hard to understand. Dr. Johnson has found the perfect pulmonary disease treatment, and I am finally feeling like a normal human being again. Thank you so much!';
$lang['TESTIMONIALS_6'] = 'George Beneventi, 34<br>Poole';
$lang['TESTIMONIALS_7'] = '<b>Finally Feeling Right!</b> <br>Pulmonary conditions run in my family. I was not even aware that it was genetic prior to speaking with my pulmonologist online through Medlanes. I have finally gotten the answers and support I need to treat my pulmonary problem. Thanks!';
$lang['TESTIMONIALS_8'] = 'Alana Fleming, 33<br>Preston';
$lang['TESTIMONIALS_9'] = '<b>No More Pulmonary Problems</b> <br>I was recommended by a friend to try Medlanes who had a lot of success with her pulmonary condition treatment online from Medlanes. I have been working with Dr. Evans ever since. I finally feel like I regained control of my pulmonary disease.';
$lang['TESTIMONIALS_10'] = 'Jonathan Chapman, 54<br>Gateshead';
$lang['TESTIMONIALS_11'] = '<b>Best Online Pulmonologist</b> <br>Talking with Dr. Forney has been such a blessing. I finally have the tools I need in place to support myself and control my pulmonary condition. I cannot thank Medlanes enough. What a wonderful online pulmonologist platform.';
$lang['TESTIMONIALS_12'] = 'George Ostermann, 29<br>Barnsley';
$lang['TESTIMONIALS_13'] = '<b>Ask A Pulmonologist Online</b> <br>I will be honest, I thought it was not possible to ask a pulmonologist question online to my doctor. After using the service a few times, I can get pulmonology help online 24 hours a day virtually. From anywhere as well. Thank you to Medlanes!';
$lang['TESTIMONIALS_14'] = 'Opal Logan, 52<br>Liverpool';
$lang['TESTIMONIALS_15'] = '<b>Help With Pulmonary Problems Medication</b> <br>I started taking levalbuterol a few weeks ago and was having weird side effects. I had all my medication questions answered, and realized that the side effects were completely normal. This put my mind at ease. Thank you everyone!';
$lang['TESTIMONIALS_16'] = 'Evan Sotelo, 21<br>Birmingham';
$lang['DOCTORS_0'] = 'Meet our Providers';
$lang['DOCTORS_1'] = 'Celine Karlsson, MD';
$lang['DOCTORS_2'] = 'Board Certified Pulmonologist <br> 1,903 People Helped';
$lang['DOCTORS_3'] = '4.81 / 5<br>I have over 15 years of experience treating pulmonary problems and conditions that are related to and/or cause pulmonary symptoms. I have been specially trained to treat pulmonary diseases online!';
$lang['DOCTORS_4'] = 'Leslie Tomaszewski, MD';
$lang['DOCTORS_5'] = 'Board Certified Pulmonologist <br> 2,869 People Helped';
$lang['DOCTORS_6'] = '4.91 / 5<br>I have treated well over 10,000 cases of pulmonary issues  and related conditions since starting practice. I am an expert in the treatment of pulmonary conditions!';
$lang['DOCTORS_7'] = 'Arthur Forester, MD';
$lang['DOCTORS_8'] = 'Board Certified Pulmonologist <br> 1,480 People Helped';
$lang['DOCTORS_9'] = '4.86 / 5<br>Pulmonary conditions do not have to control you. My patients have overcome their issues with their pulmonary health problems with specialized pulmonary conditions treatments.';
$lang['DOCTORS_10'] = 'Alexander Hayward, MD';
$lang['DOCTORS_11'] = 'Board Certified Pulmonologist <br> 1,689 People Helped';
$lang['DOCTORS_12'] = '4.78 / 5<br>Online pulmonologists is a very new concept and I am excited to be on the ground floor of revolutionizing the treatment of pulmonary diseases online and through digital methods.';
$lang['DOCTORS_13'] = 'Larry Bavin, MD';
$lang['DOCTORS_14'] = 'Board Certified Pulmonologist <br> 1,527 People Helped';
$lang['DOCTORS_15'] = '4.95 / 5<br>I have been giving online advice for about 3 years now, and can with with a wide range of issues involving pulmonology. I am here to help!';
$lang['DOCTORS_16'] = 'Yasmin Barros, MD';
$lang['DOCTORS_17'] = 'Board Certified Pulmonologist <br> 1,829 People Helped';
$lang['DOCTORS_18'] = '4.89 / 5<br>I am so excited to be a part of a revolutionary online doctor platform like Medlanes. We are here for you 24/7!';
$lang['PAYMENT_NEW4'] = 'Your expert is waiting for your question!';
$lang['PAYMENT_NEW5'] = 'Celine Karlsson, MD';
$lang['PAYMENT_NEW6'] = 'Board Certified Pulmonologist <br> 1,903 People Helped';
$lang['PAYMENT_DOC_IMG'] = '<img src="images/payment/doc-an.jpg" width="180" height="180" alt="#"/>';
$lang['DEP_CC'] = 'Credit Card';
$lang['DEP_PP'] = 'PayPal';
$lang['DEP_PP_TEXT'] = 'You will be redirected to PayPal to complete your payment.';
$lang['THANKYOU_6'] = 'service@medlanes.com';
$lang['THANKYOU_7'] = '(800) 413-7290';
$lang['PRESS_0'] = 'Online Pulmonary Health Support!';
$lang['PRESS_1'] = 'Our doctors have been extensively trained to treat pulmonary health conditions online! We can also help with:';
$lang['PRESS_2'] = '<ul><li><h2>COPD</h2></li><li><h2>Asthma</h2></li><li><h2>Bronchitis</h2></li><li><h2>Pulmonary Medication Questions</h2></li><li><h2>Emphysema</h2></li></ul>';


?>