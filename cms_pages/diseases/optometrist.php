<?php 	

$lang['BXSLIDER'] = "<ul class='staticlink'>";
$lang['HEADER_1'] = 'Our support team is here for you 24/7!';
$lang['HEADER_2'] = '<b>(800) 413-7290</b> or <b>service@medlanes.com</b>';
$lang['INDEX_1'] = 'Ask An Optometrist Now';
$lang['INDEX_2'] = 'Expert optometrists are here to support you on any conditions involving the eyes. <br> <b>Your satisfaction is 100% guaranteed.</b>';
$lang['TESTIMONIALS_0'] = 'What Our Patients Are Saying';
$lang['TESTIMONIALS_1'] = '<b>See the Light</b> <br>Having access to my optometrist online is such an amazing innovation. I am finally able to rest easy about my recent glaucoma diagnosis.';
$lang['TESTIMONIALS_2'] = 'Teresa Hoffmann, 39<br>Santa Ana, CA';
$lang['TESTIMONIALS_3'] = '<b>Online Optometrist</b> <br>I was recently diagnosed with astigmatism, and was having a lot of trouble getting my contacts lenses to fit. My online optometrist walked me through the whole process!';
$lang['TESTIMONIALS_4'] = 'Sandra Dean, 46<br>Chicago, IL';
$lang['TESTIMONIALS_5'] = '<b>Thank you!</b> <br>My son came home with pink eye. Thanks to the expert optometrists with Medlanes, I was able to get an accurate diagnosis and prevent him from spreading it around.';
$lang['TESTIMONIALS_6'] = 'Scott Russell, 47<br>Norcross, GA';
$lang['TESTIMONIALS_7'] = '<b>Eureka!</b> <br>My son was having trouble in school. My online optometrist suggested that he may need glasses. Sure enough, this fully corrected all the troubles he was having.';
$lang['TESTIMONIALS_8'] = 'Jennifer Dawson, 45<br>Boca Raton, FL';
$lang['TESTIMONIALS_9'] = '<b>Online Eye Doctor</b> <br>I did not know I could work with my eye doctor online. This is saved me so much time. No more doctors appointments.';
$lang['TESTIMONIALS_10'] = 'Sebastian Calabresi, 42<br>Birmingham, MI';
$lang['TESTIMONIALS_11'] = '<b>Bye Bye Sty</b> <br>I had a nasty sty that was causing lot of pain. After a quick consultation with my optometrist online, I was given the remedies to clear it right up!';
$lang['TESTIMONIALS_12'] = 'Jamie Schultz, 41<br>Elmhurst, IL';
$lang['TESTIMONIALS_13'] = '<b>Lifesaver!</b> <br>When putting in my contact, I scratched my cornea and was in immense pain. I talked to my optometrist online, and got the support I needed within minutes.';
$lang['TESTIMONIALS_14'] = 'Barbara Costa, 42<br>New York, NY';
$lang['TESTIMONIALS_15'] = '<b>The Best</b> <br>This service is simply the best. I am so thankful that I have access to my optometrist online 24 hours a day, 7 days a week from the comfort of my home.';
$lang['TESTIMONIALS_16'] = 'Dominic Wymer, 48<br>Saint Louis, MO';
$lang['DOCTORS_0'] = 'Meet Our Experts';
$lang['DOCTORS_1'] = 'Janice Geller, MD';
$lang['DOCTORS_2'] = 'Board Certified Optometrist<br> 1,903 People Helped';
$lang['DOCTORS_3'] = '4.81 / 5<br>I have been specially trained to treat a wide range of eye conditions online. I am ready to help!';
$lang['DOCTORS_4'] = 'Kelly Tomascak, MD';
$lang['DOCTORS_5'] = 'Board Certified Optometric<br> 2,869 People Helped';
$lang['DOCTORS_6'] = '4.91 / 5<br>I am proud to offer my services as an online optometrist. I have over 6 years treating patients eye conditions online!';
$lang['DOCTORS_7'] = 'Jacob Shields, MD';
$lang['DOCTORS_8'] = 'Board Certified Optometrist <br> 1,480 People Helped';
$lang['DOCTORS_9'] = '4.86 / 5<br>I have been an optometrist for over 17 years, and currently accepting new patients online.';
$lang['DOCTORS_10'] = 'Jonas Heller, MD';
$lang['DOCTORS_11'] = 'Certified in Optometry <br> 1,689 People Helped';
$lang['DOCTORS_12'] = '4.78 / 5<br>I am ready to help treat any eye issues online! Ask me now!';
$lang['DOCTORS_13'] = 'Taylor Haller, MD';
$lang['DOCTORS_14'] = 'Board Certified Optometrist<br> 1,527 People Helped';
$lang['DOCTORS_15'] = '4.95 / 5<br>Let me help you see the light! I am specially trained in online optometry and ready to assist.';
$lang['DOCTORS_16'] = 'Julia Sykes-Hill, MD';
$lang['DOCTORS_17'] = 'Board Certified Optometrist<br> 1,829 People Helped';
$lang['DOCTORS_18'] = '4.89 / 5<br>Working as a eye doctor online has been extremely rewarding to me. I am happy to reach so many people.';
$lang['PAYMENT_NEW4'] = 'Your expert is waiting for your question!';
$lang['PAYMENT_NEW5'] = 'Janice Geller, MD';
$lang['PAYMENT_NEW6'] = 'Board Certified Optometrist<br> 1,903 People Helped';
$lang['PAYMENT_DOC_IMG'] = '<img src="images/payment/doc-an.jpg" width="180" height="180" alt="#"/>';
$lang['DEP_CC'] = 'Credit Card';
$lang['DEP_PP'] = 'PayPal';
$lang['DEP_PP_TEXT'] = 'You will be redirected to PayPal to complete your payment.';
$lang['THANKYOU_6'] = 'service@medlanes.com';
$lang['THANKYOU_7'] = '(800) 413-7290';
$lang['PRESS_0'] = 'Ask Your Online Eye Doctor!';
$lang['PRESS_1'] = 'Our expert optometrists have been extensively trained to treat a wide variety of eye conditions online! We are able to help with:';
$lang['PRESS_2'] = '<ul><li><h2>Glaucoma</h2></li><li><h2>Astigmatism</h2></li><li><h2>Cataracts</h2></li><li><h2>Lasik Questions</h2></li><li><h2>Pink Eye</h2></li><li><h2>Stys</h2></li><li><h2>Eye Allergies</h2></li></ul>';



?>