<?php 	

$lang['BXSLIDER'] = "<ul class='staticlink'>";
$lang['HEADER_1'] = 'Our support team is here for you 24/7!';
$lang['HEADER_2'] = '<b>(800) 413-7290</b> or <b>service@medlanes.com</b>';
$lang['INDEX_1'] = 'Your Depression ends here.';
$lang['INDEX_2'] = 'Our expert psychologists and psychiatrists are online now and here to help. <br>Stop suffering from depression. Submit your question and feel better today!<br><b>Your satisfaction is 100% guaranteed.</b>';
$lang['TESTIMONIALS_0'] = 'We Beat Depression';
$lang['TESTIMONIALS_1'] = '<b>I Beat Seasonal Affective Disorder.</b> <br>I have had a lot of trouble with SAD in the past. I finally found the perfect seasonal affective disorder treatment online with Medlanes. The doctors have been great and their service is second to none!';
$lang['TESTIMONIALS_2'] = 'Janet Watson, 43 <br> Atlanta, GA';
$lang['TESTIMONIALS_3'] = '<b>Kiss Depression Goodbye.</b> <br>I have always shown symptoms of depression, but never wanted to talk to a doctor face to face. Medlanes is the best online psychologist platform around and thanks to them, have found the perfect depression support!';
$lang['TESTIMONIALS_4'] = 'Betty Wylie, 29 <br> Irving, TX';
$lang['TESTIMONIALS_5'] = '<b>Best Depression Help!</b> <br>Finally! That is all I have to say. Treatments for depression are confusing and hard to understand. Dr. Johnson has found the perfect depression treatment, and I am finally feeling like a normal human being again. Thank you so much!';
$lang['TESTIMONIALS_6'] = 'Jack Boles, 37 <br> Tacoma, WA';
$lang['TESTIMONIALS_7'] = '<b>Finally Feeling Right!</b> <br>Manic depression runs in my family. I was not even aware that it was genetic prior to speaking with my psychiatrist online through Medlanes. I have finally gotten the answers and support I need to treat my depression. Thanks!';
$lang['TESTIMONIALS_8'] = 'Tameka Jones, 39<br> Detroit, MI';
$lang['TESTIMONIALS_9'] = '<b>No More Depression</b> <br>I was recommended by a friend to try Medlanes who had a lot of success with her depression treatment online from Medlanes. I have been working with Dr. Evans ever since. I finally feel like I have beat depression for good.';
$lang['TESTIMONIALS_10'] = 'Clay Teller, 59 <br> Philadelphia, PA';
$lang['TESTIMONIALS_11'] = '<b>Best Online Psychiatrist</b> <br>Talking with Dr. Forney has been such a blessing. I finally have the tools I need in place to support myself and fight off my feelings of depression. I cannot thank Medlanes enough. What a wonderful online psychologist platform.';
$lang['TESTIMONIALS_12'] = 'Shane Carter, 27<br> Chicago, IL';
$lang['TESTIMONIALS_13'] = '<b>Ask A Psychiatrist Online</b> <br>I will be honest, I thought it was not possible to ask a psychiatrist question online to my doctor. After using the service a few times, I can get mental help online 24 hours a day virtually. From anywhere as well. Thank you to Medlanes!';
$lang['TESTIMONIALS_14'] = 'Christine Roseman, 51<br> Lexington, KY';
$lang['TESTIMONIALS_15'] = '<b>Depression Medication Help</b> <br>I started taking an antidepressant a few weeks ago and was having weird side effects. I had all my medication questions answered, and realized that the side effects were completely normal. This put my mind at ease. Thank you everyone!';
$lang['TESTIMONIALS_16'] = 'Kyle Holmes, 22<br> Cheyenne, WY';
$lang['DOCTORS_0'] = 'Meet our Providers';
$lang['DOCTORS_1'] = 'Agatha Andrews, MD';
$lang['DOCTORS_2'] = 'Board Certified Psychiatrist <br> 2,873+ People Helped';
$lang['DOCTORS_3'] = '4.85 / 5<br><br>I have over 15 years of experience treating depression and conditions that are related to and/or cause depression . I have been specially trained to treat depression online!';
$lang['DOCTORS_4'] = 'Heather Johnson, Ph.D.';
$lang['DOCTORS_5'] = 'Board Certified Psychotherapist <br> 1,611 People Helped';
$lang['DOCTORS_6'] = '4.92 / 5<br><br>I have treated well over 10,000 cases of depression  and related conditions since starting practice. I am an expert in the treatment of depression!';
$lang['DOCTORS_7'] = 'Thomas Sandrini, MD';
$lang['DOCTORS_8'] = 'Diplomat of Psychiatry <br> 918 People Helped';
$lang['DOCTORS_9'] = '4.71 / 5<br><br>Depression does not have to control you. My patients have overcome their issues with depression with specialized depression treatments that have been custom tailored to meet their specific needs.';
$lang['DOCTORS_10'] = 'Erwin Williams, MD, MBA';
$lang['DOCTORS_11'] = 'Board Certified Psychiatrist <br> 873+ People Helped';
$lang['DOCTORS_12'] = '4.8 / 5<br><br>Online psychiatry and online psychology are both very new concepts and I am excited to be on the ground floor of revolutionizing the treatment of depression online and through digital methods.';
$lang['DOCTORS_13'] = 'Jason Evans, Ph.D., MS';
$lang['DOCTORS_14'] = 'Certified Couples Therapist <br> 1,920 Couples Helped';
$lang['DOCTORS_15'] = '4.81 / 5<br><br>I have been doing online therapy for about 3 years now, and can with with a wide range of issues involving depression. I am here to help!';
$lang['DOCTORS_16'] = 'Wilma Forney, MD';
$lang['DOCTORS_17'] = 'Board Certified Psychiatrist <br> 1,001 People Helped';
$lang['DOCTORS_18'] = '4.78 / 5<br><br>I am so excited to be a part of a revolutionary online doctor platform like Medlanes. We are here for you 24/7!';
$lang['PAYMENT_NEW4'] = 'Your expert is waiting for your question!';
$lang['PAYMENT_NEW5'] = 'Agatha Andrews, MD';
$lang['PAYMENT_NEW6'] = 'Board Certified Psychiatrist <br> 2,873+ People Helped';
$lang['PAYMENT_DOC_IMG'] = '<img src="images/payment/doc-an.jpg" width="180" height="180" alt="#"/>';
$lang['DEP_CC'] = 'Credit Card';
$lang['DEP_PP'] = 'PayPal';
$lang['DEP_PP_TEXT'] = 'You will be redirected to PayPal to complete your payment.';
$lang['THANKYOU_6'] = 'service@medlanes.com';
$lang['THANKYOU_7'] = '(800) 413-7290';
$lang['PRESS_0'] = 'Online Mental Health Support!';
$lang['PRESS_1'] = 'Our doctors have been extensively trained to treat mental health conditions online! We can also help with:';
$lang['PRESS_2'] = '<ul><li>Anxiety</li><li>Fears and Phobias</li><li>Behavioral Issues</li><li>Psychiatric Medication Questions</li><li>Panic Attacks</li></ul>';;



 ?>