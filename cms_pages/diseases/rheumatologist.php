<?php 	

$lang['BXSLIDER'] = "<ul class='staticlink'>";
$lang['HEADER_1'] = 'Our support team is here for you 24/7!';
$lang['HEADER_2'] = '<b>(800) 413-7290</b> or <b>service@medlanes.com</b>';
$lang['INDEX_1'] = 'Ask A Rheumatologist Now';
$lang['INDEX_2'] = 'Expert Rheumatologists are available now to take your questions. <br>Skip the doctors office, simply ask your question to get started. <br><b>Your satisfaction is 100% guaranteed. </b>';
$lang['TESTIMONIALS_0'] = 'What Our Patients Are Saying';
$lang['TESTIMONIALS_1'] = '<b>Pain Free</b> <br>My online rheumatologist recommended a few amazing home remedies that has allowed me to rid myself of my arthritic pains. Thanks, Medlanes!';
$lang['TESTIMONIALS_2'] = 'Maddison Hay, 46<br>Sabattus, ME';
$lang['TESTIMONIALS_3'] = '<b>Skip The Office</b> <br>Online rheumatologists can do virtually anything that a traditional office based rheumatologist can. Skip the visit, go with Medlanes!';
$lang['TESTIMONIALS_4'] = 'Evie Meagher, 40<br>Pittsburgh, PA';
$lang['TESTIMONIALS_5'] = '<b>Rheumatology Experts</b> <br>I have personally spoken to 3 online rheumatologists, and each one of them has provided me with individual attention and compassion.';
$lang['TESTIMONIALS_6'] = 'Charlie Charles, 49<br>Westbrook, ME';
$lang['TESTIMONIALS_7'] = '<b>Thank You</b> <br>My mother was having hip issues, and her GP was unable to figure out what was going on. One quick consultation with my online rheumatologist, and she is feeling better!';
$lang['TESTIMONIALS_8'] = 'Carrie Silva, 42<br>Caulfield, MO';
$lang['TESTIMONIALS_9'] = '<b>Trusted Rheumatologists</b> <br>I have always struggled with arthritis in my hands. I have received some of the best advice from my online rheumatologist. Thank you, Dr, W!';
$lang['TESTIMONIALS_10'] = 'John McIntyre, 42<br>Fremont, CA';
$lang['TESTIMONIALS_11'] = '<b>Awesome</b> <br>Simply awesome! My expert online rheumatologist has guided me through so much regarding my recent RA diagnosis. I am so thankful.';
$lang['TESTIMONIALS_12'] = 'Cairn Branch, 49<br>Unalaska, AK';
$lang['TESTIMONIALS_13'] = '<b>Online Rheumatologist</b> <br>I was a little skeptical at first, but the advice from my online rheumatologist has trumped anything my actual rheumatologist as said!';
$lang['TESTIMONIALS_14'] = 'Verona Clark, 44<br>Atlanta, MO';
$lang['TESTIMONIALS_15'] = '<b>Many Thanks</b> <br>I was having some really negative side effects from my steroid therapy. My rheumatologist walked me through how to counteract these side effects, all online! Thanks!';
$lang['TESTIMONIALS_16'] = 'Joshua Broinowski, 38<br>Cambridge, MA';
$lang['DOCTORS_0'] = 'Meet Our Experts';
$lang['DOCTORS_1'] = 'Jayne Winters, MD';
$lang['DOCTORS_2'] = 'Board Certified Rheumatologist<br> 1,775 People Helped';
$lang['DOCTORS_3'] = '4.81 / 5<br>I hope I can take the pain out of your arthritis. Consult with me today!';
$lang['DOCTORS_4'] = 'Kimberly Rivers, MD';
$lang['DOCTORS_5'] = 'Board Certified Rheumatologist<br> 1,869 People Helped';
$lang['DOCTORS_6'] = '4.91 / 5<br>I am proud to offer my services as an online rheumatologist. I have over 6 years treating all arthritis type conditions online!';
$lang['DOCTORS_7'] = 'Travis Vernon, MD';
$lang['DOCTORS_8'] = 'Certified Rheumatologist<br> 1,480 People Helped';
$lang['DOCTORS_9'] = '4.86 / 5<br>Bringing rheumatology online has helped thousands of patients who suffer from RA. Be one of them!';
$lang['DOCTORS_10'] = 'Perry Chang, MD';
$lang['DOCTORS_11'] = 'Certified in Rheumatology<br> 1,689 People Helped';
$lang['DOCTORS_12'] = '4.78 / 5<br>Get out of pain today. Ask me how!';
$lang['DOCTORS_13'] = 'Walter Price, MD';
$lang['DOCTORS_14'] = 'Board Certified - Rheumatology<br> 1,527 People Helped';
$lang['DOCTORS_15'] = '4.95 / 5<br>Arthritis does not have to control your life. I am glad to assist with arthritis and related conditions online!';
$lang['DOCTORS_16'] = 'Jackie Parks, MD';
$lang['DOCTORS_17'] = 'Board Certified Rheumatologist<br> 1,829 People Helped';
$lang['DOCTORS_18'] = '4.89 / 5<br>We brought rheumatology online. I am happy I have the ability to reach my patients on their time, not on mine!';
$lang['PAYMENT_NEW4'] = 'Your expert is waiting for your question!';
$lang['PAYMENT_NEW5'] = 'Jayne Winters, MD';
$lang['PAYMENT_NEW6'] = 'Board Certified Rheumatologist<br> 1,775 People Helped';
$lang['PAYMENT_DOC_IMG'] = '<img src="images/payment/doc-an.jpg" width="180" height="180" alt="#"/>';
$lang['DEP_CC'] = 'Credit Card';
$lang['DEP_PP'] = 'PayPal';
$lang['DEP_PP_TEXT'] = 'You will be redirected to PayPal to complete your payment.';
$lang['THANKYOU_6'] = 'service@medlanes.com';
$lang['THANKYOU_7'] = '(800) 413-7290';
$lang['PRESS_0'] = 'Rheumatologists Advice From Home!';
$lang['PRESS_1'] = 'Our expert rheumatologists have been extensively trained to support a wide variety of conditions online. We are also able to provide advice for:';
$lang['PRESS_2'] = '<ul><li><h2>Arthritis</h2></li><li><h2>Rheumatoid Arthritis</h2></li><li><h2>Gout</h2></li><li><h2>Osteoporosis</h2></li><li><h2>Lupus</h2></li><li><h2>and More!</h2></li></ul>';



?>