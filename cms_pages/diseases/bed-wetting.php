<?php 	

$lang['php_echo'] = 'Text';
$lang['BXSLIDER'] = "<ul class='staticlink'>";
$lang['HEADER_1'] = 'Our support team is here for you 24/7!';
$lang['HEADER_2'] = '<b>(800) 413-7290</b> or <b>service@medlanes.com</b>';
$lang['INDEX_1'] = 'No more Bed Wetting!';
$lang['INDEX_2'] = 'Our pediatricians are online now and ready to help you. Submit your question about Bed Wetting and feel better today!<b> Your satisfaction is 100% guaranteed.</b>';
$lang['TESTIMONIALS_0'] = 'What Our Patients Are Saying';
$lang['TESTIMONIALS_1'] = '<b>Problem free household</b> <br>Enuresis has been a problem at my house, because it kept recurring and made my child self-conscious. I went to many places for help, but only Medlanes proved to work. Enuresis is no longer a problem.';
$lang['TESTIMONIALS_2'] = 'Charlie Preston, 49<br>Toledo, OH';
$lang['TESTIMONIALS_3'] = '<b>No more soggy sheets</b> <br>I have been dealing with soggy sheets and pajamas because my son was bed wetting. I was at loss and only Medlanes managed to explain me the reason for bed wettting and possible treatments. Thank you!';
$lang['TESTIMONIALS_4'] = 'Dixie Onio, 48<br>San Diego, CA';
$lang['TESTIMONIALS_5'] = '<b>Quick treatment options</b> <br>My child had nocturnal enuresis and Medlanes helped me to deal with it. It provided me with information about bladder training and moisture alarms. Now the problem is gone!';
$lang['TESTIMONIALS_6'] = 'Chad Weaver, 39<br>Little Rock, AR';
$lang['TESTIMONIALS_7'] = '<b>Great help!</b> <br>Bedwetting is hard to deal with, but Medlanes showed me the right approach to it. What a great online service!';
$lang['TESTIMONIALS_8'] = 'Scarlett Piazza, 49<br>Detroit, MI';
$lang['TESTIMONIALS_9'] = '<b>Help from Medlanes</b> <br>My oldest son has been having problems with wetting the bed and we were at loss. An online pediatrician found the solution though and it has been a year with no bedwetting accidents!';
$lang['TESTIMONIALS_10'] = 'Jay Swift, 45<br>Sacramento, CA';
$lang['TESTIMONIALS_11'] = '<b>Medlanes rose to the occasion</b> <br>I was worried that I was dealing with something extremely serious, but Medlanes gave me a perfect enuresis definition and I learned how to tackle it!';
$lang['TESTIMONIALS_12'] = 'John Loya, 44<br>Buffalo, NY';
$lang['TESTIMONIALS_13'] = '<b>Amazing service</b> <br>My boy was so worried about his bed wetting, so I had a pediatrician from Medlanes define enuresis for him. The consultation was great, because he also gave us different treatment options.';
$lang['TESTIMONIALS_14'] = 'Anna Sunners, 45<br>Keaau, HI';
$lang['TESTIMONIALS_15'] = '<b>A battle won</b> <br>We have tried so many treatments for our little problem and nothing worked. Then, a medlanes pediatrician suggested us bedwetting medication and poof, bedwetting has not recurred.';
$lang['TESTIMONIALS_16'] = 'Harrison Hardess, 41<br>Port Orange, FL';
$lang['DOCTORS_0'] = 'Meet Our Experts';
$lang['DOCTORS_1'] = 'Allison Watters, MD';
$lang['DOCTORS_2'] = 'Board Certified Pediatrician<br> 1,903 People Helped';
$lang['DOCTORS_3'] = '4.81 / 5<br>Bed wetting at any age, especially bed wetting at age 5 is common and normal. If it does cause any issue and you need help, contact me at Medlanes. ';
$lang['DOCTORS_4'] = 'Libby Ryder, MD';
$lang['DOCTORS_5'] = 'Board Certified Pediatrician<br> 2,869 People Helped';
$lang['DOCTORS_6'] = '4.91 / 5<br>I am a board-certified pediatrician ready to help you with any issues you might have. Just contact me at Medlanes any time!';
$lang['DOCTORS_7'] = 'Jack McKay, MD';
$lang['DOCTORS_8'] = 'Board Certified Pediatrician<br> 1,480 People Helped';
$lang['DOCTORS_9'] = '4.86 / 5<br>Healthcare should not be difficult or confusing. Medlanes is a revolutionary platform that proves that. Being an online pediatrician I have dealt with more health cases than possible to count.';
$lang['DOCTORS_10'] = 'Kelvin Lee, MD';
$lang['DOCTORS_11'] = 'Certified in Pediatrics<br> 1,689 People Helped';
$lang['DOCTORS_12'] = '4.78 / 5<br>Many people are unaware what is nocturnal enuresis. If you need more information, have a consultation with me at Medlanes. I am here to help.';
$lang['DOCTORS_13'] = 'Jayce Timpani, MD';
$lang['DOCTORS_14'] = 'Board Certified - Pediatrics<br> 1,527 People Helped';
$lang['DOCTORS_15'] = '4.95 / 5<br>Bedwetting in children is completely normal, but requires a lot of patience and understanding. If you require any advice, do not hesitate to contact me.';
$lang['DOCTORS_16'] = 'Kelly Robbins, MD';
$lang['DOCTORS_17'] = 'Board Certified Pediatrician<br> 1,829 People Helped';
$lang['DOCTORS_18'] = '4.89 / 5<br>If you require bedwetting treatment, you are at the right place! I or any other skilled pediatricians from Medlanes team will do our best to help you.';
$lang['PAYMENT_NEW4'] = 'Your expert is waiting for your question!';
$lang['PAYMENT_NEW5'] = 'Allison Watters, MD';
$lang['PAYMENT_NEW6'] = 'Board Certified Pediatrician<br> 1,903 People Helped';
$lang['PAYMENT_DOC_IMG'] = '<img src="images/payment/doc-an.jpg" width="180" height="180" alt="#"/>';
$lang['DEP_CC'] = 'Credit Card';
$lang['DEP_PP'] = 'PayPal';
$lang['DEP_PP_TEXT'] = 'You will be redirected to PayPal to complete your payment.';
$lang['THANKYOU_6'] = 'service@medlanes.com';
$lang['THANKYOU_7'] = '(800) 413-7290';
$lang['PRESS_0'] = 'Ask Your Online Pediatrician!';
$lang['PRESS_1'] = 'Our expert pediatricians have been extensively trained to support a wide variety of conditions online. We are here for you and your child. Our specialties include:';
$lang['PRESS_2'] = '<ul><li><h2>Rash Analysis</h2></li><li><h2>Mononucleosis</h2></li><li><h2>ADHD</h2></li><li><h2>Fevers</h2></li><li><h2>Ear Infections</h2></li><li><h2>Croup</h2></li><li><h2>and More!</h2></li></ul>';


?>