<?php 	

$lang['php_echo'] = 'Text';
$lang['BXSLIDER'] = "<ul class='staticlink'>";
$lang['HEADER_1'] = 'Our support team is here for you 24/7!';
$lang['HEADER_2'] = '<b>(800) 413-7290</b> or <b>service@medlanes.com</b>';
$lang['INDEX_1'] = 'Ask An Orthopedist Now';
$lang['INDEX_2'] = 'Our expert orthopedists are online now and here to help. <br>Stop suffering from orthopedic conditions. Submit your question and feel better today!<br><b>Your satisfaction is 100% guaranteed.</b>';
$lang['TESTIMONIALS_0'] = 'We help with orthopedic problems';
$lang['TESTIMONIALS_1'] = '<b>I gained control of my orthopedic problem</b> <br>I have had a lot of trouble with orthopedic disorders in the past. I finally found the perfect orthopedic treatment online with Medlanes. The doctors have been great and their service is second to none!';
$lang['TESTIMONIALS_2'] = 'Imogen Fleming, 49<br>New York, NY';
$lang['TESTIMONIALS_3'] = '<b>Kiss Orthopedic Problems Goodbye</b> <br>I have always shown symptoms of orthopedic issues, but never had the time to talk to a doctor face to face. Medlanes is the best online orthopedic platform around and thanks to them, I have found the perfect support for my orthopedic problem!';
$lang['TESTIMONIALS_4'] = 'Eleanor Schneider, 29<br>Daytona Beach, FL';
$lang['TESTIMONIALS_5'] = '<b>Best Orthopedics Help!</b> <br>Finally! That is all I have to say. Treatments for orthopedic problems are confusing and hard to understand. Dr. Johnson has found the perfect orthopedic issues treatment, and I am finally feeling like a normal human being again. Thank you so much!';
$lang['TESTIMONIALS_6'] = 'Campbell Shaw, 38<br>Fort Lee, NJ';
$lang['TESTIMONIALS_7'] = '<b>Finally Feeling Right!</b> <br>Orthopedic problems run in my family. I was not even aware that it was genetic prior to speaking with my orthopedist online through Medlanes. I have finally gotten the answers and support I need to treat my orthopedic problem. Thanks!';
$lang['TESTIMONIALS_8'] = 'Isobel Bishop, 37<br>Greenville, SC';
$lang['TESTIMONIALS_9'] = '<b>No More Orthopedic Problems</b> <br>I was recommended by a friend to try Medlanes who had a lot of success with her orthopedic problem treatment online from Medlanes. I have been working with Dr. Evans ever since. I finally feel like I have beat my orthopedic problem for good.';
$lang['TESTIMONIALS_10'] = 'Samuel Barry, 41<br>Salt Lake City, UT';
$lang['TESTIMONIALS_11'] = '<b>Best Online Orthopedist</b> <br>Talking with Dr. Forney has been such a blessing. I finally have the tools I need in place to support myself and control my orthopedic problem. I cannot thank Medlanes enough. What a wonderful online orthopedic platform.';
$lang['TESTIMONIALS_12'] = 'Toby Le Rennetel, 26<br>Somerville, NJ';
$lang['TESTIMONIALS_13'] = '<b>Ask An Orthopedist Online</b> <br>I will be honest, I thought it was not possible to ask an orthopedic question online about my orthopedic condition to my doctor. After using the service a few times, I can get orthopedic help online 24 hours a day virtually. From anywhere as well. Thank you to Medlanes!';
$lang['TESTIMONIALS_14'] = 'Carlita Ross, 45<br>Cincinnati, OH';
$lang['TESTIMONIALS_15'] = '<b>Orthopedic Condition Medication Help</b> <br>I started taking an meloxicam a few weeks ago and was having weird side effects. I had all my medication questions answered, and realized that the side effects were completely normal. This put my mind at ease. Thank you everyone!';
$lang['TESTIMONIALS_16'] = 'Evan Hamilton, 30<br>Albany, NY';
$lang['DOCTORS_0'] = 'Meet our Providers';
$lang['DOCTORS_1'] = 'Carol Reed, MD';
$lang['DOCTORS_2'] = 'Board Certified Orthopedist <br> 2,441 People Helped';
$lang['DOCTORS_3'] = '4.91 / 5<br>I have over 15 years of experience treating orthopedic conditions. I have been specially trained to treat orthopedic problems online!';
$lang['DOCTORS_4'] = 'Donna James, MD';
$lang['DOCTORS_5'] = 'Board Certified Orthopedist <br> 2,220 People Helped';
$lang['DOCTORS_6'] = '4.81 / 5<br>I have treated well over 10,000 cases of orthopedic problems  and related conditions since starting practice. I am an expert in the treatment of orthopedic conditions!';
$lang['DOCTORS_7'] = 'Eden Lynch, MD';
$lang['DOCTORS_8'] = 'Board Certified Orthopedist <br> 921 People Helped';
$lang['DOCTORS_9'] = '4.88 / 5<br>Orthopedic conditions do not have to control you. My patients have overcome their orthopedic issues with specialized orthopedic treatments that have been custom tailored to meet their specific needs.';
$lang['DOCTORS_10'] = 'Angus Hooley, MD';
$lang['DOCTORS_11'] = 'Board Certified Orthopedist <br> 2,048 People Helped';
$lang['DOCTORS_12'] = '4.91 / 5<br>Online orthopedics is a very new concept and I am excited to be on the ground floor of revolutionizing the treatment of orthopedic conditions online and through digital methods.';
$lang['DOCTORS_13'] = 'Jason Evans, MD';
$lang['DOCTORS_14'] = 'Board Certified Orthopedist <br> 2,573 People Helped';
$lang['DOCTORS_15'] = '4.93 / 5<br>I have been giving online medical advice for about 3 years now, and can with with a wide range of issues involving orthopedic conditions. I am here to help!';
$lang['DOCTORS_16'] = 'Leslie Thompson, MD';
$lang['DOCTORS_17'] = 'Board Certified Orthopedist <br> 1,903 People Helped';
$lang['DOCTORS_18'] = '4.81 / 5<br>I am so excited to be a part of a revolutionary online doctor platform like Medlanes. We are here for you 24/7!';
$lang['PAYMENT_NEW4'] = 'Your expert is waiting for your question!';
$lang['PAYMENT_NEW5'] = 'Carol Reed, MD';
$lang['PAYMENT_NEW6'] = 'Board Certified Orthopedist <br> 2,441 People Helped';
$lang['PAYMENT_DOC_IMG'] = '<img src="images/payment/doc-an.jpg" width="180" height="180" alt="#"/>';
$lang['DEP_CC'] = 'Credit Card';
$lang['DEP_PP'] = 'PayPal';
$lang['DEP_PP_TEXT'] = 'You will be redirected to PayPal to complete your payment.';
$lang['THANKYOU_6'] = 'service@medlanes.com';
$lang['THANKYOU_7'] = '(800) 413-7290';
$lang['PRESS_0'] = 'Online Orthopedic Health Support!';
$lang['PRESS_1'] = 'Our doctors have been extensively trained to treat orthopedic health conditions online! We can also help with:';
$lang['PRESS_2'] = '<ul><li><h2>ACL Injuries</h2></li><li><h2>Hip Bursitis</h2></li><li><h2>Plantar Fascitis</h2></li><li><h2>Medications for Orthopedic Conditions Questions</h2></li><li><h2>Tennis Elbow</h2></li></ul>';


?>