<?php 	

$lang['php_echo'] = 'Text';
$lang['BXSLIDER'] = "<ul class='staticlink'>";
$lang['HEADER_1'] = 'Our support team is here for you 24/7!';
$lang['HEADER_2'] = '<b>(800) 413-7290</b> or <b>service@medlanes.com</b>';
$lang['INDEX_1'] = 'Questions about Pregnancy?';
$lang['INDEX_2'] = 'Our gynecologists are online now and ready to help you. Submit your question about Pregnancy and feel better today!<b> Your satisfaction is 100% guaranteed.</b>';
$lang['TESTIMONIALS_0'] = 'What Our Patients Are Saying';
$lang['TESTIMONIALS_1'] = '<b>My husband and I are finally expecting our first child. Talking about the pregnancy with a trained online gynecologist was important to me. Since I am not so young anymore, I had many concerns. The doctor at Medlanes was there for me. She really took the time to answer all my questions.</b> <br>My husband and I are finally expecting our first child. Talking about the pregnancy with a trained online gynecologist was important to me. Since I am not so young anymore, I had many concerns. The doctor at Medlanes was there for me. She really took the time to answer all my questions.';
$lang['TESTIMONIALS_2'] = 'Amy Santos, 40<br>York';
$lang['TESTIMONIALS_3'] = '<b>Such a great service</b> <br>I am a future mom! Thanks to Medlanes I was able to talk to an online gynecologist, who told me which foods to avoid during my pregnancy and what was safe to eat. Actually he addressed all my concerns and was very helpful. I will certainly contact him again when I have more questions.';
$lang['TESTIMONIALS_4'] = 'Elizabeth Ostermann, 46<br>Charnwood';
$lang['TESTIMONIALS_5'] = '<b>A big step in modern healthcare</b> <br>I can not believe we are finally going to have a kid! Thanks to Medlanes and their online gynecologists, I now know all the symptoms that may appear during early pregnancy: diarrhea, nausea, constipation, vomiting... I also know what to do to help my wife through the tough times.';
$lang['TESTIMONIALS_6'] = 'William Piazza, 43<br>Dundee';
$lang['TESTIMONIALS_7'] = '<b>A big step in modern healthcare</b> <br>I was trying to find week by week pictures of pregnancy when I discovered Medlanes webplatform. The online gynecologist who anwered me was very helpful and comforting. Now I do not have to worry about the health of my future baby anymore: we are both in good hands!';
$lang['TESTIMONIALS_8'] = 'Christine Marchant, 47<br>Falkirk';
$lang['TESTIMONIALS_9'] = '<b>Always there when you need it!</b> <br>As a single father myself, I was at a loss when my daughter told me she was pregnant. So we talked to an online gynecologist. She really took the time to answer our questions and explained my daughters what are the signs of pregnancy diabetes. This way she will be able to get treated as soon as possible.';
$lang['TESTIMONIALS_10'] = 'Evan Gaytan, 49<br>Renfrewshire';
$lang['TESTIMONIALS_11'] = '<b>An awesome service</b> <br>I found out about a month ago that I was going to be a father. But my wife has been freaking out because she has uterine fibroids and she was scared it will affect her pregnancy. So we talked to an online gynecologist. Thanks to Medlanes, all our doubts have vanished!';
$lang['TESTIMONIALS_12'] = 'Charles Barrera, 43<br>North Tyneside';
$lang['TESTIMONIALS_13'] = '<b>Very helpful!</b> <br>I just found out that my 16 year old daughter is pregnant. Thanks to Medlanes, we were able to talk to an online gynecologist. She made sure my daughter knew what to eat during the first trimester and answered all our questions in no time.';
$lang['TESTIMONIALS_14'] = 'Ashley Silva, 40<br>Bedford';
$lang['TESTIMONIALS_15'] = '<b>Medlanes changed my life</b> <br>I recently found out that my girlfriend was pregnant. I must admit I am scared! Is the baby going to be healthy? Talking to an online gynecologist at Medlanes, and knowing that he\'s here for me whenever I have questions, is awesome! It totally put my mind at ease!';
$lang['TESTIMONIALS_16'] = 'Alfie Sousa, 39<br>Tendring';
$lang['DOCTORS_0'] = 'Meet Our Experts';
$lang['DOCTORS_1'] = 'Madison Leal, MD';
$lang['DOCTORS_2'] = 'Board Certified Gynecologist<br> 2,811 People Helped';
$lang['DOCTORS_3'] = '4.89 / 5<br>Having low blood sugar in pregnancy is something that concerns many future mother. If you wish to learn more about it and how to prevent the condition, do not hesitate to ask me!';
$lang['DOCTORS_4'] = 'Callie Ross, MD';
$lang['DOCTORS_5'] = 'Board Certified OBGYN <br> 1,760 People Helped';
$lang['DOCTORS_6'] = '4.88 / 5<br>I truly love my job as a gynecologist. Thanks to Medlanes, I get to help even more women every day. I feel like I can make a difference in modern healthcare. ';
$lang['DOCTORS_7'] = 'Owen Tyers, MD';
$lang['DOCTORS_8'] = 'Board Certified OBGYN<br> 908 People Helped';
$lang['DOCTORS_9'] = '4.94 / 5<br>I have been a gynecologist for 20 years now. It is such an amazing feeling to be able to help new patients. Thanks to Medlanes, I am now here for you 24/7!';
$lang['DOCTORS_10'] = 'Alvin Lee, MD, MPH';
$lang['DOCTORS_11'] = 'Board Certified OBGYN <br> 1,401 People Helped';
$lang['DOCTORS_12'] = '4.8 / 5<br>You are in your sixth month of pregnancy and would like to make sure everything is ok? You have any questions? Thanks to Medlanes, I am here for you!';
$lang['DOCTORS_13'] = 'Rasin Sem, MD, MPH';
$lang['DOCTORS_14'] = 'Board Certified OBGYN <br> 1,407 People Helped';
$lang['DOCTORS_15'] = '4.81 / 5<br>Many patients wonder about the impacts of ketones in urine during their pregnancy. If you also want some more information about it, simply ask me via Medlanes\' online platform!';
$lang['DOCTORS_16'] = 'Molly Wearing, MD';
$lang['DOCTORS_17'] = 'Board Certified OBGYN <br> 1,556 People Helped';
$lang['DOCTORS_18'] = '4.95 / 5<br>You should know diabetes insipidus rarely occurs in pregnancy. As a trained online gynecologist at Medlanes, I will do my best to answer all your other questions.';
$lang['PAYMENT_NEW4'] = 'Your expert is waiting for your question!';
$lang['PAYMENT_NEW5'] = 'Madison Leal, MD';
$lang['PAYMENT_NEW6'] = 'Board Certified Gynecologist<br> 2,811 People Helped';
$lang['PAYMENT_DOC_IMG'] = '<img src="images/payment/doc-an.jpg" width="180" height="180" alt="#"/>';
$lang['DEP_CC'] = 'Credit Card';
$lang['DEP_PP'] = 'PayPal';
$lang['DEP_PP_TEXT'] = 'You will be redirected to PayPal to complete your payment.';
$lang['THANKYOU_6'] = 'service@medlanes.com';
$lang['THANKYOU_7'] = '(800) 413-7290';
$lang['PRESS_0'] = 'Ask Your Online Gynecologist!';
$lang['PRESS_1'] = 'Our doctors have been extensively trained to advise on a wide range of gynecological issues online. We are happy to help with:';
$lang['PRESS_2'] = '<ul><li><h2>Menstrual Cycle Problems</h2></li><li><h2>Birth Control Questions</h2></li><li><h2>Menopause</h2></li><li><h2>Conception</h2></li><li><h2>Endometriosis</h2></li></ul>';



?>