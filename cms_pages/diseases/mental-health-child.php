<?php 	

$lang['php_echo'] = 'Text';
$lang['BXSLIDER'] = "<ul class='staticlink'>";
$lang['HEADER_1'] = 'Our support team is here for you 24/7!';
$lang['HEADER_2'] = '<b>(800) 413-7290</b> or <b>service@medlanes.com</b>';
$lang['INDEX_1'] = 'Child mental health problems? We help.';
$lang['INDEX_2'] = 'Our pediatricians are online now and ready to help you. Submit your question about Croup and feel better today!<b> Your satisfaction is 100% guaranteed.</b>';
$lang['TESTIMONIALS_0'] = 'What Our Patients Are Saying';
$lang['TESTIMONIALS_1'] = '<b>Keep up the good work</b> <br>Thank you Medlanes! I probably never would have been able to detect the first signs of mental illness in my daughter without the help of your online doctors. That pediatrician really took the time to answer my question and address all my concerns!';
$lang['TESTIMONIALS_2'] = 'Rose Boehm, 47<br>Cave Creek, AZ';
$lang['TESTIMONIALS_3'] = '<b>Getting serious help online</b> <br>Thanks to online doctors at Medlanes I could have my son tested for mental disorders. As a young mother, it is tough to know that your kid suffers from a mental disorder! The pediatrician was here for me and answered all my questions!';
$lang['TESTIMONIALS_4'] = 'Abbey Urner, 41<br>Fort Washington, MD';
$lang['TESTIMONIALS_5'] = '<b>Good support for worried parents</b> <br>it is really tough to deal with mental health issues. Especially when your youngest daughter is affected! Medlanes provided great support for me and my wife. Our online pediatrician reall took the time to listen to us.';
$lang['TESTIMONIALS_6'] = 'Rocco Cumbrae-Stewart, 42<br>Raleigh, NC';
$lang['TESTIMONIALS_7'] = '<b>Quick answers when you need them</b> <br>It is very scary to find out that your son\'s behavioral health may be at risk. Good thing that Medlanes was here for us 24/7! My husband and I can just ask our online pediatrician anytime a new question arises. And we know he\'ll be here for us!';
$lang['TESTIMONIALS_8'] = 'Megan Trentino, 47<br>North Billerica, MA';
$lang['TESTIMONIALS_9'] = '<b>An amazing web platform</b> <br>I\'m so thankful for Medlanes and the support they provided me with when I found out my baby girl may be suffering from a mental illness. I will definitely have her tested and monitored regularly when she grows up. And I know I can count on my online pediatrician whenever I have doubts.';
$lang['TESTIMONIALS_10'] = 'Elliot Walker, 38<br>Wichita, KS';
$lang['TESTIMONIALS_11'] = '<b>Always here for you!</b> <br>Mental disorders are scary, especially when they affect your kids! So it nice to know that online pediatricians are just a click away whenever you need someone to confort you and address your concerns. I am really glad I discovered Medlanes when I did!';
$lang['TESTIMONIALS_12'] = 'Reece Gough, 44<br>Lexington, KY';
$lang['TESTIMONIALS_13'] = '<b>Great doctors available 24/7</b> <br>Ever since we had our child\'s mental stability tested last month, my husband and I had so many questions. Thanks to Medlanes and their 24/7 service, we can get our answers right away without having to make an appointment. It is very nice and we are thankful for it all!';
$lang['TESTIMONIALS_14'] = 'Brooke Allan, 45<br>Ropesville, TX';
$lang['TESTIMONIALS_15'] = '<b>Thank you Medlanes!</b> <br>What can you do when your kid is diagnosed with a psychiatric illness? Turn to Medlanes, they really are here for you! Our online pediatrician was truly amazing. Not only did she listen to us, but she also helped us cope with the bad news.';
$lang['TESTIMONIALS_16'] = 'Leo Williams, 49<br>Springfield, VA';
$lang['DOCTORS_0'] = 'Meet Our Experts';
$lang['DOCTORS_1'] = 'Allison Watters, MD';
$lang['DOCTORS_2'] = 'Board Certified Pediatrician<br> 1,903 People Helped';
$lang['DOCTORS_3'] = '4.81 / 5<br>Living with mental illness is not easy. Being an online pediatrician allows me to help more and more children every day and make sure their families are here to support them. Thank you Medlanes for the platform!';
$lang['DOCTORS_4'] = 'Libby Ryder, MD';
$lang['DOCTORS_5'] = 'Board Certified Pediatrician<br> 2,869 People Helped';
$lang['DOCTORS_6'] = '4.91 / 5<br>It is such a pleasure to work with an online platform like Medlanes. Their service really is improving modern health care as it allows doctors like me to treat so many more patients every day.';
$lang['DOCTORS_7'] = 'Jack McKay, MD';
$lang['DOCTORS_8'] = 'Board Certified Pediatrician<br> 1,480 People Helped';
$lang['DOCTORS_9'] = '4.86 / 5<br>After 17 years of practice as a pediatrician, Medlanes provided me with a great tool to help even more young patients. it is very exciting to be part of such a project!';
$lang['DOCTORS_10'] = 'Kelvin Lee, MD';
$lang['DOCTORS_11'] = 'Certified in Pediatrics<br> 1,689 People Helped';
$lang['DOCTORS_12'] = '4.78 / 5<br>Thanks to Medlanes, I can make sure that parents get all the mental health information they need to treat their kids. If you too have a question, never hesitate to ask! I am here for you 24/7.';
$lang['DOCTORS_13'] = 'Jayce Timpani, MD';
$lang['DOCTORS_14'] = 'Board Certified - Pediatrics<br> 1,527 People Helped';
$lang['DOCTORS_15'] = '4.95 / 5<br>As an online pediatrician I am always here for my patients and their parents. So if you need to discuss treatment for mental illness or any other issue, just ask me via Medlanes!';
$lang['DOCTORS_16'] = 'Kelly Robbins, MD';
$lang['DOCTORS_17'] = 'Board Certified Pediatrician<br> 1,829 People Helped';
$lang['DOCTORS_18'] = '4.89 / 5<br>Thanks to the online platform at Medlanes, I can help more children every day and make sure they get the best mental therapy. Will your kid be one of them?';
$lang['PAYMENT_NEW4'] = 'Your expert is waiting for your question!';
$lang['PAYMENT_NEW5'] = 'Allison Watters, MD';
$lang['PAYMENT_NEW6'] = 'Board Certified Pediatrician<br> 1,903 People Helped';
$lang['PAYMENT_DOC_IMG'] = '<img src="images/payment/doc-an.jpg" width="180" height="180" alt="#"/>';
$lang['DEP_CC'] = 'Credit Card';
$lang['DEP_PP'] = 'PayPal';
$lang['DEP_PP_TEXT'] = 'You will be redirected to PayPal to complete your payment.';
$lang['THANKYOU_6'] = 'service@medlanes.com';
$lang['THANKYOU_7'] = '(800) 413-7290';
$lang['PRESS_0'] = 'Ask Your Online Pediatrician!';
$lang['PRESS_1'] = 'Our expert pediatricians have been extensively trained to support a wide variety of conditions online. We are here for you and your child. Our specialties include:';
$lang['PRESS_2'] = '<ul><li><h2>Rash Analysis</h2></li><li><h2>Mononucleosis</h2></li><li><h2>ADHD</h2></li><li><h2>Fevers</h2></li><li><h2>Ear Infections</h2></li><li><h2>Croup</h2></li><li><h2>and More!</h2></li></ul>';


?>