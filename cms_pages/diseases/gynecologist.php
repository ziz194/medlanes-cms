<?php 	

$lang['php_echo'] = 'Text';
$lang['BXSLIDER'] = "<ul class='staticlink'>";
$lang['HEADER_1'] = 'Our support team is here for you 24/7!';
$lang['HEADER_2'] = '<b>(800) 413-7290</b> or <b>service@medlanes.com</b>';
$lang['INDEX_1'] = 'Ask A Gynecologist Now';
$lang['INDEX_2'] = 'Our expert Gynecologists are online now and here to help. <br>Simply submit your question to get started.<br><b>Your satisfaction is 100% guaranteed.</b>';
$lang['TESTIMONIALS_0'] = 'What our Customers are Saying';
$lang['TESTIMONIALS_1'] = '<b>Great Help!</b> <br>Any questions related to gynecology are embarrassing, and sometimes I do not feel comfortable enough to talk face to face with my doctor. I am so thankful I am able to ask a gynecologist questions online via Medlanes!';
$lang['TESTIMONIALS_2'] = 'Staci Thompson, 45<br>Jupiter, FL';
$lang['TESTIMONIALS_3'] = '<b>Awesome!</b> <br>This service is awesome. I am so thankful I am able to contact my OBGYN online, and get answers to all of my questions. The online interface is super easy, cheap, and the result is amazing!';
$lang['TESTIMONIALS_4'] = 'Elizabeth Huston, 25<br>Dothan, AL';
$lang['TESTIMONIALS_5'] = '<b>We are Pregnant!</b> <br>For a year, my wife and I were having trouble trying to conceive. As a last option, we reached out to a gynecologist at Medlanes. Dr. Andrews was kind, helpful, and without a doubt gave us the advice we needed to finally conceive!                         ';
$lang['TESTIMONIALS_6'] = 'Caleb Stewart, 36<br>Albany, NY';
$lang['TESTIMONIALS_7'] = '<b>Thanks!</b> <br>I was having trouble finding solutions to my mood swings. The gynecologist I spoke with had all of the answers I needed, and I am finally at the right dose of my birth control. Thank you, Dr. Williams.';
$lang['TESTIMONIALS_8'] = 'Claire Sargood, 36<br>Seymour, MO';
$lang['TESTIMONIALS_9'] = '<b>Mood Swings</b> <br>My fiance was having a lot of trouble managing her emotions. I felt so bad for her, it was a roller coaster struggle. I reached out to the OBGYNs with Medlanes, and they gave me sound advice on how we can keep her feeling normal!';
$lang['TESTIMONIALS_10'] = 'Gabriel Siede, 42<br>Los Angeles, CA';
$lang['TESTIMONIALS_11'] = '<b>Whoops!</b> <br>I actually found this site by accident, thank god I did. My wife was experiencing pain, and was put on antibiotics. I did not know antibiotics causes birth control to fail. Thank goodness we figured that out before anything happened!';
$lang['TESTIMONIALS_12'] = 'Jan Mitchell, 25<br>Elmhurst, IL';
$lang['TESTIMONIALS_13'] = '<b>Online Gynecologists Rock!</b> <br>Going to the gynecologists grosses me out. I am so thankful I can avoid the stirrups and speak and ask a gynecologist online all of my questions. Thanks Medlanes and Dr. E.';
$lang['TESTIMONIALS_14'] = 'Camille Domínguez, 49<br>Keene, NH';
$lang['TESTIMONIALS_15'] = '<b>Support!</b> <br>We recently lost our baby, and reached out to Medlanes when my wife was having odd symptoms. The online gynecologists were compassionate, helpful, and took great care of us.';
$lang['TESTIMONIALS_16'] = 'Lennox Duggan, 27<br>Los Angeles, CA';
$lang['DOCTORS_0'] = 'Meet our Providers';
$lang['DOCTORS_1'] = 'Madison Leal, MD';
$lang['DOCTORS_2'] = 'Board Certified Gynecologist<br> 2,811 People Helped';
$lang['DOCTORS_3'] = '4.89 / 5<br>I have over 15 years of experience with online gynecologist visits. I am here for you!';
$lang['DOCTORS_4'] = 'Cindy Dyken, MD';
$lang['DOCTORS_5'] = 'Board Certified OBGYN <br> 1,760 People Helped';
$lang['DOCTORS_6'] = '4.88 / 5<br>I have treated well over 10,000 OBGYN patients online and I am now accepting new patients.';
$lang['DOCTORS_7'] = 'Owen Tyers, MD';
$lang['DOCTORS_8'] = 'Board Certified OBGYN<br> 908 People Helped';
$lang['DOCTORS_9'] = '4.94 / 5<br>Gynecologists are no fun. This unique platform allows us to take the awkwardness out of a visit to the OBGYN!';
$lang['DOCTORS_10'] = 'Alvin Lee, MD, MPH';
$lang['DOCTORS_11'] = 'Board Certified OBGYN <br> 1,401 People Helped';
$lang['DOCTORS_12'] = '4.8 / 5<br>I specialize in medication issues regarding birth control. I am here to help!';
$lang['DOCTORS_13'] = 'Rasin Sem, MD, MPH';
$lang['DOCTORS_14'] = 'Board Certified OBGYN <br> 1,407 People Helped';
$lang['DOCTORS_15'] = '4.81 / 5<br>I am able to reach so many lives via the ask a gynecologist platform, and helping you wonderful people start families is my passion!';
$lang['DOCTORS_16'] = 'Molly Wearing, MD';
$lang['DOCTORS_17'] = 'Board Certified OBGYN <br> 1,556 People Helped';
$lang['DOCTORS_18'] = '4.95 / 5<br>I am so excited to be a part of a revolutionary online doctor platform like Medlanes. We are here for you 24/7!';
$lang['PAYMENT_NEW4'] = 'Your expert is waiting for your question!';
$lang['PAYMENT_NEW5'] = 'Madison Leal, MD';
$lang['PAYMENT_NEW6'] = 'Board Certified Gynecologist<br> 2,811 People Helped';
$lang['PAYMENT_DOC_IMG'] = '<img src="images/payment/doc-an.jpg" width="180" height="180" alt="#"/>';
$lang['DEP_CC'] = 'Credit Card';
$lang['DEP_PP'] = 'PayPal';
$lang['DEP_PP_TEXT'] = 'You will be redirected to PayPal to complete your payment.';
$lang['THANKYOU_6'] = 'service@medlanes.com';
$lang['THANKYOU_7'] = '(800) 413-7290';
$lang['PRESS_0'] = 'Ask A Gynecologist Now!';
$lang['PRESS_1'] = 'Our doctors have been extensively trained to advise on a wide range of gynecological issues online. We are happy to help with:';
$lang['PRESS_2'] = '<ul><li><h2>Menstrual Cycle Problems</h2></li><li><h2>Birth Control Questions</h2></li><li><h2>Menopause</h2></li><li><h2>Conception</h2></li><li><h2>Endometriosis</h2></li></ul>';



?>