<?php 	

$lang['BXSLIDER'] = "<ul class='staticlink'>";
$lang['HEADER_1'] = 'Our support team is here for you 24/7!';
$lang['HEADER_2'] = '<b>(800) 413-7290</b> or <b>service@medlanes.com</b>';
$lang['INDEX_1'] = 'Ask A Pharmacist Now';
$lang['INDEX_2'] = 'Expert Pharmacists are available 24/7 to answer your medication related questions! Private, anonymous, and from the comfort of your own home. <br> Simply ask your question to get started.<b> Your satisfaction is 100% guaranteed. </b>';
$lang['TESTIMONIALS_0'] = 'What Our Patients Are Saying';
$lang['TESTIMONIALS_1'] = '<b>Online Pharmacists</b> <br>Best invention ever. I have had questions that I wanted to ask in private, but never had the opportunity to. Thanks to Medlanes, I can ask completely anonymously from home.';
$lang['TESTIMONIALS_2'] = 'Natalie Fernandez, 39<br>Grand Rapids, MI';
$lang['TESTIMONIALS_3'] = '<b>Lifesaver!</b> <br>I was having an adverse reaction to my antidepressant. Thanks to my online pharmacist, I figured out the the grapefruit just I was drinking was causing an interaction.';
$lang['TESTIMONIALS_4'] = 'Sue Miller, 39<br>Pittsburgh, PA';
$lang['TESTIMONIALS_5'] = '<b>Compassionate Pharmacists</b> <br>Having diabetes, I always have a lot of questions to ask. I am so happy that Medlanes has such a wonderful online endocrinologist platform. Lifesaver!';
$lang['TESTIMONIALS_6'] = 'Christopher Fierro, 38<br>West Sacramento, CA';
$lang['TESTIMONIALS_7'] = '<b>Super!</b> <br>Hats off to Dr. Lindemann - She saved me a trip to the store. The ability to contact pharmacists online from the comfort of my own home is wonderful!';
$lang['TESTIMONIALS_8'] = 'Sophia Sanderson, 42<br>Moline, IL';
$lang['TESTIMONIALS_9'] = '<b>Pharmacology Experts!</b> <br>So happy that I have this awesome network of online pharmacists here for me. They genuinely care, and work very hard to answer all of your questions.';
$lang['TESTIMONIALS_10'] = 'Vernon Neumann, 45<br>Philadelphia, PA';
$lang['TESTIMONIALS_11'] = '<b>Awesome</b> <br>Dr. Xander was awesome. He had the answers to all of my questions regarding my hypertension medication. Even advised me of supplements I can take in conjunction. Thank you!';
$lang['TESTIMONIALS_12'] = 'Mackenzie Henderson, 38<br>Floral Park, NY';
$lang['TESTIMONIALS_13'] = '<b>Thanks!</b> <br>The online pharmacists at Medlanes truly are professionals. They are very friendly and go up and beyond to make sure you are well taken care of.';
$lang['TESTIMONIALS_14'] = 'Ella Zapata, 49<br>Shreveport, LA';
$lang['TESTIMONIALS_15'] = '<b>Money Saver</b> <br>I cannot believe how cheap it is to ask questions to an expert pharmacist online. This service blew me away. I really cannot thank you all enough for everything.';
$lang['TESTIMONIALS_16'] = 'Finlay Glockner, 48<br>Buffalo Grove, IL';
$lang['DOCTORS_0'] = 'Meet Our Experts';
$lang['DOCTORS_1'] = 'Helen Lindemann, Pharm.D.';
$lang['DOCTORS_2'] = 'Board Certified Pharmacist<br> 1,411 People Helped';
$lang['DOCTORS_3'] = '4.81 / 5<br>Medications are complicated. Consult a pharmacist online now to clear up any questions you may have!';
$lang['DOCTORS_4'] = 'Ashleigh Roberts, Pharm.D.';
$lang['DOCTORS_5'] = 'Board Certified Pharmacist<br> 1,869 People Helped';
$lang['DOCTORS_6'] = '4.91 / 5<br>Night and Day, I am ready to assist you with all of your medication questions.';
$lang['DOCTORS_7'] = 'Scott Jovanovski, Pharm.D.';
$lang['DOCTORS_8'] = 'Certified in Pharmacology<br> 1,480 People Helped';
$lang['DOCTORS_9'] = '4.86 / 5<br>Many times, pharmacists at brick and mortar locations are simply unable to take the time to explain your medications properly. I am here for just that.';
$lang['DOCTORS_10'] = 'Robert Blake, Pharm.D.';
$lang['DOCTORS_11'] = 'Certified in Pharmacology<br> 1,689 People Helped';
$lang['DOCTORS_12'] = '4.78 / 5<br>I am ready to assist you with all of your medication related questions. Ask me now!';
$lang['DOCTORS_13'] = 'Dave Holmes III, Pharm.D.';
$lang['DOCTORS_14'] = 'Board Certified Pharmacist<br> 1,527 People Helped';
$lang['DOCTORS_15'] = '4.95 / 5<br>I specialize in psychiatric medications including SSRIs, Stimulants, Antidepressants, and more. I am available now.';
$lang['DOCTORS_16'] = 'Janice Peake, Pharm.D.';
$lang['DOCTORS_17'] = 'Board Certified Pharmacist<br> 1,829 People Helped';
$lang['DOCTORS_18'] = '4.89 / 5<br>Having access to pharmacists online is wonderful for late night and after hours help. I am ready to assist you!';
$lang['PAYMENT_NEW4'] = 'Your expert is waiting for your question!';
$lang['PAYMENT_NEW5'] = 'Helen Lindemann, Pharm.D.';
$lang['PAYMENT_NEW6'] = 'Board Certified Pharmacist<br> 1,411 People Helped';
$lang['PAYMENT_DOC_IMG'] = '<img src="images/payment/doc-an.jpg" width="180" height="180" alt="#"/>';
$lang['DEP_CC'] = 'Credit Card';
$lang['DEP_PP'] = 'PayPal';
$lang['DEP_PP_TEXT'] = 'You will be redirected to PayPal to complete your payment.';
$lang['THANKYOU_6'] = 'service@medlanes.com';
$lang['THANKYOU_7'] = '(800) 413-7290';
$lang['PRESS_0'] = 'Pharmacists At Your Service 24/7!';
$lang['PRESS_1'] = 'Our certified pharmacists have been extensively trained to educated on every medication currently available. We specialize in:';
$lang['PRESS_2'] = '<ul><li><h2>Heart Medication</h2></li><li><h2>Psychiatric Medication</h2></li><li><h2>Cholesterol Medication</h2></li><li><h2>Sexual Enhancement</h2></li><li><h2>Diet Pills</h2></li><li><h2>Vitamins & Supplements</h2></li><li><h2>and More!</h2></li></ul>';



?>