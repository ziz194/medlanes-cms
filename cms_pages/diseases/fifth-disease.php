<?php 	

$lang['BXSLIDER'] = "<ul class='staticlink'>";
$lang['HEADER_1'] = 'Our support team is here for you 24/7!';
$lang['HEADER_2'] = '<b>(800) 413-7290</b> or <b>service@medlanes.com</b>';
$lang['INDEX_1'] = 'Is it Fifth Disease?';
$lang['INDEX_2'] = 'Our pediatricians are online now and ready to help you. Submit your question about Fifth Disease and feel better today! <b> Your satisfaction is 100% guaranteed. </b>';
$lang['TESTIMONIALS_0'] = 'What Our Patients Are Saying';
$lang['TESTIMONIALS_1'] = '<b>Best online service</b> <br>Fifth disease in children is common and my child got infected at kindergarten. So once again I turned to Medlanes for help and once again - I am extremely pleased with the help I received.';
$lang['TESTIMONIALS_2'] = 'Victoria Watson, 40<br>Urbandale, IA';
$lang['TESTIMONIALS_3'] = '<b>Great and reliable information</b> <br>I had no idea what fifth disease was until my son got sick and my online doctor explained what was wrong. Thank you Medlanes!';
$lang['TESTIMONIALS_4'] = 'Evie Kaestner, 48<br>Greensboro, NC';
$lang['TESTIMONIALS_5'] = '<b>I am cured!</b> <br>I always thgouht that 5th disease is a childhood disease, so I was surprised when my doctor at Medlanes explained me that what I was suffering from was exactly that. Thank you Medlanes!';
$lang['TESTIMONIALS_6'] = 'Bryce Pridham, 38<br>Beverly Hills, CA';
$lang['TESTIMONIALS_7'] = '<b>Happy household</b> <br>I was terrified to know that my 5 years old daughter had parvovirus, but my online doctor reassured me that it was as serious as it sounds. I got a great treatment plan and now both I and my daughter feel great!';
$lang['TESTIMONIALS_8'] = 'Hannah Sykes, 46<br>Baltimore, MD';
$lang['TESTIMONIALS_9'] = '<b>Count me in !</b> <br>I am a single mother, I juggle two job, so I am so happy for online service like Medlanes. Both of my babies got children\'s fifth disease and I had no time to go to a clinic. An online doctor was a great solution!';
$lang['TESTIMONIALS_10'] = 'Harrison Meister, 42<br>South Burlington, VT';
$lang['TESTIMONIALS_11'] = '<b>Fifth disease- Never again!</b> <br>I never realized how common is fifth disease, but I was worried when my son kept getting sick. Thankfully, I decided Medlanes online doctor and now I know how to prevent the condition from ever happening again!';
$lang['TESTIMONIALS_12'] = 'Scott Jaeger, 49<br>Ontario, CA';
$lang['TESTIMONIALS_13'] = '<b>Groovy service!</b> <br>I looked at fifth disease in children pictures online but I still was not sure if my daughter was sick, so I decided to consult an online doctor. Best decision I have ever made!';
$lang['TESTIMONIALS_14'] = 'Alice Azevedo, 47<br>Daniel Island, SC';
$lang['TESTIMONIALS_15'] = '<b>100% Satisfaction</b> <br>I am a teacher and kids in my class started getting sick with parvo. I contacted Medlanes for information, so now I, the kids and their parents are at ease. It was a great service!';
$lang['TESTIMONIALS_16'] = 'Luke Hackett, 38<br>Bolton, MA';
$lang['DOCTORS_0'] = 'Meet Our Experts';
$lang['DOCTORS_1'] = 'Allison Watters, MD';
$lang['DOCTORS_2'] = 'Board Certified Pediatrician<br> 1,903 People Helped';
$lang['DOCTORS_3'] = '4.81 / 5<br>Many people do not now that parvo is contagious, so if you are unsure about your condition it is best to consult a doctor. With Medlanes, I am available 24/7';
$lang['DOCTORS_4'] = 'Libby Ryder, MD';
$lang['DOCTORS_5'] = 'Board Certified Pediatrician<br> 2,869 People Helped';
$lang['DOCTORS_6'] = '4.91 / 5<br>I have been working as an online doctor for a while now and I am very impressed with Medlanes online platform. It makes helping people so much easier!';
$lang['DOCTORS_7'] = 'Jack McKay, MD';
$lang['DOCTORS_8'] = 'Board Certified Pediatrician<br> 1,480 People Helped';
$lang['DOCTORS_9'] = '4.86 / 5<br>I am a board certified pediatrician. I have been working with Medlanes from the start and its pleases me to say, that we are making a change in modern health care.';
$lang['DOCTORS_10'] = 'Kelvin Lee, MD';
$lang['DOCTORS_11'] = 'Certified in Pediatrics<br> 1,689 People Helped';
$lang['DOCTORS_12'] = '4.78 / 5<br>Even babies can get fifths disease and as a parent you should contact a pediatrician. I have been practising for over 20 years aned I am ready to help your family too!';
$lang['DOCTORS_13'] = 'Jayce Timpani, MD';
$lang['DOCTORS_14'] = 'Board Certified - Pediatrics<br> 1,527 People Helped';
$lang['DOCTORS_15'] = '4.95 / 5<br>Parvovirus in humans is very common. Please contact us at Medlanes if you see any symptoms of the condition. We are an online health service platform.';
$lang['DOCTORS_16'] = 'Kelly Robbins, MD';
$lang['DOCTORS_17'] = 'Board Certified Pediatrician<br> 1,829 People Helped';
$lang['DOCTORS_18'] = '4.89 / 5<br>Fifth disease in adult is just as common as in children. I have been practising medicine for many years now and I can help you if you fall sick. Do not hesitate to contact me at Medlanes.';
$lang['PAYMENT_NEW4'] = 'Your expert is waiting for your question!';
$lang['PAYMENT_NEW5'] = 'Allison Watters, MD';
$lang['PAYMENT_NEW6'] = 'Board Certified Pediatrician<br> 1,903 People Helped';
$lang['PAYMENT_DOC_IMG'] = '<img src="images/payment/doc-an.jpg" width="180" height="180" alt="#"/>';
$lang['DEP_CC'] = 'Credit Card';
$lang['DEP_PP'] = 'PayPal';
$lang['DEP_PP_TEXT'] = 'You will be redirected to PayPal to complete your payment.';
$lang['THANKYOU_6'] = 'service@medlanes.com';
$lang['THANKYOU_7'] = '(800) 413-7290';
$lang['PRESS_0'] = 'Ask Your Online Pediatrician!';
$lang['PRESS_1'] = 'Our expert pediatricians have been extensively trained to support a wide variety of conditions online. We are here for you and your child. Our specialties include:';
$lang['PRESS_2'] = '<ul><li><h2>Rash Analysis</h2></li><li><h2>Mononucleosis</h2></li><li><h2>ADHD</h2></li><li><h2>Fevers</h2></li><li><h2>Ear Infections</h2></li><li><h2>Croup</h2></li><li><h2>and More!</h2></li></ul>';



?>