<?php 	

$lang['BXSLIDER'] = "<ul class='staticlink'>";
$lang['HEADER_1'] = 'Our support team is here for you 24/7!';
$lang['HEADER_2'] = '<b>(800) 413-7290</b> or <b>service@medlanes.com</b>';
$lang['INDEX_1'] = 'Suffering from Chicken Pox?';
$lang['INDEX_2'] = 'Our pediatricians are online now and ready to help you. Submit your question about Chicken Pox and feel better today! <b>Your satisfaction is 100% guaranteed.</b>';
$lang['TESTIMONIALS_0'] = 'What Our Patients Are Saying';
$lang['TESTIMONIALS_1'] = '<b>I beat Chicken Pox!</b> <br>Thanks to online doctors at Medlanes I was able to learn all about incubation period for chicken pox. It calmed me down and made my daughter happy, as she could skip school!';
$lang['TESTIMONIALS_2'] = 'Zoe Knaggs, 39<br>Bohemia, NY';
$lang['TESTIMONIALS_3'] = '<b>Chicken pox symptoms dissapeared!</b> <br>Thanks to Medlanes I was able to learn about the varicella virus and how to deal with it. My online doctor helped me to overcome chicken pox!';
$lang['TESTIMONIALS_4'] = 'Willa Milano, 43<br>Austin, TX';
$lang['TESTIMONIALS_5'] = '<b>Chicken pox virus gone!</b> <br>I looked at early chicken pox pictures online when I was not sure what I was suffering from, but doctors at Medlanes managed to assure me and provide me with a correct treatment plan!';
$lang['TESTIMONIALS_6'] = 'Zachary Hay, 46<br>New York, NY';
$lang['TESTIMONIALS_7'] = '<b>Bye chicken pox rash!</b> <br>When my daughter fell sick I had no idea what to do. It is thanks to Medlanes that I learned about chicken pox symptoms in kids and was able to look after my daughter accordingly.';
$lang['TESTIMONIALS_8'] = 'Melissa Santillán, 45<br>Bloomington, IN';
$lang['TESTIMONIALS_9'] = '<b>Chicken pox defetead. Again.</b> <br>I was unaware that you can get chicken pox twice, so I am very grateful to Medlanes for helping me out.';
$lang['TESTIMONIALS_10'] = 'Harry Flynn, 47<br>Cincinnati, OH';
$lang['TESTIMONIALS_11'] = '<b>Chicken pox stages information</b> <br>Online doctors at Medlanes taught me all there is to know about chicken pox prevention and it\'s different stages,';
$lang['TESTIMONIALS_12'] = 'Toby Jones, 48<br>Chicago, IL';
$lang['TESTIMONIALS_13'] = '<b>I conquered chicken pox!</b> <br>Chicken pox disease hit me in my 30s. I am very grateful to doctor at Medlanes for helping me get better.';
$lang['TESTIMONIALS_14'] = 'Isobel Hoffmann, 46<br>Wilmington, MA';
$lang['TESTIMONIALS_15'] = '<b>Quick chicken pox treatment!</b> <br>Dealing with chicken pox shingles has been the worst! Fortunately, Medlanes provided me with useful advice about how to deal with chicken pox symptoms and stages.';
$lang['TESTIMONIALS_16'] = 'Ewan Nangle, 38<br>Bowling Green, KY';
$lang['DOCTORS_0'] = 'Meet Our Experts';
$lang['DOCTORS_1'] = 'Allison Watters, MD';
$lang['DOCTORS_2'] = 'Board Certified Pediatrician<br> 1,903 People Helped';
$lang['DOCTORS_3'] = '4.81 / 5<br>I have been a pediatrician for a long time now and working from an online platfrom allows me to reach more patients!';
$lang['DOCTORS_4'] = 'Libby Ryder, MD';
$lang['DOCTORS_5'] = 'Board Certified Pediatrician<br> 2,869 People Helped';
$lang['DOCTORS_6'] = '4.91 / 5<br>Many people don\'t know how does chicken pox start. I am here to help them find out!';
$lang['DOCTORS_7'] = 'Jack McKay, MD';
$lang['DOCTORS_8'] = 'Board Certified Pediatrician<br> 1,480 People Helped';
$lang['DOCTORS_9'] = '4.86 / 5<br>It\'s okay if you don\'t know when chicken pox is contagious. I am here to provide you with all necessary information!';
$lang['DOCTORS_10'] = 'Kelvin Lee, MD';
$lang['DOCTORS_11'] = 'Certified in Pediatrics<br> 1,689 People Helped';
$lang['DOCTORS_12'] = '4.78 / 5<br>Chicken pox virus is extremely serious. As a pediatrician its my duty to help affected people and provide them with treatment options.';
$lang['DOCTORS_13'] = 'Jayce Timpani, MD';
$lang['DOCTORS_14'] = 'Board Certified - Pediatrics<br> 1,527 People Helped';
$lang['DOCTORS_15'] = '4.95 / 5<br>Chicken pox is extremely contagious, so helping people via Medlanes online platform is a great way to stop the spreading of chicken pox!';
$lang['DOCTORS_16'] = 'Kelly Robbins, MD';
$lang['DOCTORS_17'] = 'Board Certified Pediatrician<br> 1,829 People Helped';
$lang['DOCTORS_18'] = '4.89 / 5<br>Being a doctor online has made chicken pox prevention easier and faster! I am thankful to Medlanes for the opportunity to make a change. ';
$lang['PAYMENT_NEW4'] = 'Your expert is waiting for your question!';
$lang['PAYMENT_NEW5'] = 'Allison Watters, MD';
$lang['PAYMENT_NEW6'] = 'Board Certified Pediatrician<br> 1,903 People Helped';
$lang['PAYMENT_DOC_IMG'] = '<img src="images/payment/doc-an.jpg" width="180" height="180" alt="#"/>';
$lang['DEP_CC'] = 'Credit Card';
$lang['DEP_PP'] = 'PayPal';
$lang['DEP_PP_TEXT'] = 'You will be redirected to PayPal to complete your payment.';
$lang['THANKYOU_6'] = 'service@medlanes.com';
$lang['THANKYOU_7'] = '(800) 413-7290';
$lang['PRESS_0'] = 'Ask Your Online Pediatrician!';
$lang['PRESS_1'] = 'Our expert pediatricians have been extensively trained to support a wide variety of conditions online. We are here for you and your child. Our specialties include:';
$lang['PRESS_2'] = '<ul><li><h2>Rash Analysis</h2></li><li><h2>Mononucleosis</h2></li><li><h2>ADHD</h2></li><li><h2>Fevers</h2></li><li><h2>Ear Infections</h2></li><li><h2>Croup</h2></li><li><h2>and More!</h2></li></ul>';



?>