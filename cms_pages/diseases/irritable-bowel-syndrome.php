<?php 	

$lang['php_echo'] = 'Text';
$lang['BXSLIDER'] = "<ul class='staticlink'>";
$lang['HEADER_1'] = 'Our support team is here for you 24/7!';
$lang['HEADER_2'] = '<b>(800) 413-7290</b> or <b>service@medlanes.com</b>';
$lang['INDEX_1'] = 'Problems with Irritable Bowel?';
$lang['INDEX_2'] = 'Our gastroenterologists are online now and ready to help you. Submit your question about Irritable Bowel Syndrome and feel better today!<b> Your satisfaction is 100% guaranteed.</b>';
$lang['TESTIMONIALS_0'] = 'What Our Patients Are Saying';
$lang['TESTIMONIALS_1'] = '<b>Great help</b> <br>I have been diagnosed with irritable bowel syndrome over a year ago. It has been very difficult on me, so I decided to consult with an online gastroenterologist. Thank you Medlanes!';
$lang['TESTIMONIALS_2'] = 'Estella Esquivel, 47<br>Phoenix, AZ';
$lang['TESTIMONIALS_3'] = '<b>Awesome service</b> <br>Mucous Colitis is a very serious condition. I am glad I used Medlanes online healthcare services for help. I can have my condition under control now.';
$lang['TESTIMONIALS_4'] = 'Faith Segovia, 46<br>Buffalo, NY';
$lang['TESTIMONIALS_5'] = '<b>Amazing information</b> <br>I have been constipated for two weeks and then an online gastroenterologist suggested that I might have spastic colon. I got tested and he was right, so I received immediate treatment. Thanks Medlanes!';
$lang['TESTIMONIALS_6'] = 'Leigh Roth, 43<br>Beacon, NY';
$lang['TESTIMONIALS_7'] = '<b>Happy couple</b> <br>My husband has been suffering from IBS and we felt helpless upon we stumbled on Medlanes. We received useful information about spastic colon treatment from an online gastroenterologist. We are so happy!';
$lang['TESTIMONIALS_8'] = 'Katie Palerma, 44<br>Vivian, SD';
$lang['TESTIMONIALS_9'] = '<b>Family history</b> <br>Irritable bowel syndrome is running in my family, so I was wondering what were my chances of developing it. An online doctor from Medlanes provided me with all necessary information.';
$lang['TESTIMONIALS_10'] = 'Tristan Alves, 44<br>Waukesha, WI';
$lang['TESTIMONIALS_11'] = '<b>Fast and reliable</b> <br>Mucous colitis caused me a lot of abdominal pain, gas and diarrhea until I got a consultation with an online gastroenterologist. Thanks Medlanes!';
$lang['TESTIMONIALS_12'] = 'Joe Hanger, 44<br>Wood Dale, IL';
$lang['TESTIMONIALS_13'] = '<b>Revolution at hand</b> <br>Medlanes revolutionizes healthcare. I realized that when I received a consultation with an online gastroenterologist. Thank you Medlanes!';
$lang['TESTIMONIALS_14'] = 'Laura Jamieson, 42<br>Gilbert, AZ';
$lang['TESTIMONIALS_15'] = '<b>I am pain free now</b> <br>Medlanes helped me to cope with my IBS. I feel better now than I did in a long while. 100% satisfaction.';
$lang['TESTIMONIALS_16'] = 'Taj , 46<br>Salt Lake City, UT';
$lang['DOCTORS_0'] = 'Meet Our Experts';
$lang['DOCTORS_1'] = 'Emma Priestley, MD';
$lang['DOCTORS_2'] = 'Board Certified Gastroenterologist <br> 2,475 People Helped';
$lang['DOCTORS_3'] = '4.98 / 5<br>Medlanes is an online healthcare platform that allows me to treat people from any part of the world. Contact us!';
$lang['DOCTORS_4'] = 'Sara Piazza, MD';
$lang['DOCTORS_5'] = 'Board Certified Gastroenterologist <br> 1,996 People Helped';
$lang['DOCTORS_6'] = '4.83 / 5<br>If you need an expert gastroenterologist opinion, you can now reach one online. Just use Medlanes!';
$lang['DOCTORS_7'] = 'Lucas Henn, MD';
$lang['DOCTORS_8'] = 'Diplomat of Gastroenterology <br> 2,754 People Helped';
$lang['DOCTORS_9'] = '4.92 / 5<br>As an online doctor its my duty to help anyone in need. Medlanes makes it easy for patients to reach their doctors, so feel free to contact me.';
$lang['DOCTORS_10'] = 'Anthony Oberg, MD';
$lang['DOCTORS_11'] = 'Board Certified Gastroenterologist <br> 1,636 People Helped';
$lang['DOCTORS_12'] = '4.88 / 5<br>You can reach a doctor from the comforts of your own home now. Just use Medlanes!';
$lang['DOCTORS_13'] = 'Jeffrey Richter, MD';
$lang['DOCTORS_14'] = 'Certified Gastroenterologist <br> 972 People Helped';
$lang['DOCTORS_15'] = '4.93 / 5<br>It has never been easier seeing a gastroenterologist, because now you can do it online thanks to Medlanes.';
$lang['DOCTORS_16'] = 'Jessica Hensley, MD';
$lang['DOCTORS_17'] = 'Board Certified Gastroenterologist <br> 2,441 People Helped';
$lang['DOCTORS_18'] = '4.96 / 5<br>Online gastroenterologist at your service. If you have any questions about IBS or GERD, contact me via Medlanes.';
$lang['PAYMENT_NEW4'] = 'Your expert is waiting for your question!';
$lang['PAYMENT_NEW5'] = 'Emma Priestley, MD';
$lang['PAYMENT_NEW6'] = 'Board Certified Gastroenterologist <br> 2,475 People Helped';
$lang['PAYMENT_DOC_IMG'] = '<img src="images/payment/doc-an.jpg" width="180" height="180" alt="#"/>';
$lang['DEP_CC'] = 'Credit Card';
$lang['DEP_PP'] = 'PayPal';
$lang['DEP_PP_TEXT'] = 'You will be redirected to PayPal to complete your payment.';
$lang['THANKYOU_6'] = 'service@medlanes.com';
$lang['THANKYOU_7'] = '(800) 413-7290';
$lang['PRESS_0'] = 'Ask Your Online Gastroenterologist!';
$lang['PRESS_1'] = 'Our doctors have been extensively trained to treat gastroenterological health conditions online! We can also help with:';
$lang['PRESS_2'] = '<ul><li><h2>Irritable Bowel Syndrome</h2></li><li><h2>Constipation</h2></li><li><h2>Colon Polyps</h2></li><li><h2>Gastroenterological Medication Questions</h2></li><li><h2>Colitis</h2></li></ul>';


?>