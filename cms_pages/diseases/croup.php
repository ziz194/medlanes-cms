<?php 	

$lang['BXSLIDER'] = "<ul class='staticlink'>";
$lang['HEADER_1'] = 'Our support team is here for you 24/7!';
$lang['HEADER_2'] = '<b>(800) 413-7290</b> or <b>service@medlanes.com</b>';
$lang['INDEX_1'] = 'Questions about Croup?';
$lang['INDEX_2'] = 'Our pediatricians are online now and ready to help you. Submit your question about Croup and put your mind at ease!<b> Your satisfaction is 100% guaranteed</b>';
$lang['TESTIMONIALS_0'] = 'What Our Patients Are Saying';
$lang['TESTIMONIALS_1'] = '<b>Quick croup treatment!</b> <br>I was so worried when I noticed barking cough in my toddler. Medlanes managed to provide me relief by providing quick and affordable doctor\'s consultation.';
$lang['TESTIMONIALS_2'] = 'Lara Kaufmann, 41<br>Providence, RI';
$lang['TESTIMONIALS_3'] = '<b>Barking cough gone.</b> <br>My doctor online provided me with information with croup cough treatment and my child is cough free now! I am extremely pleased with Medlanes services.';
$lang['TESTIMONIALS_4'] = 'Scarlett Zelaya, 45<br>Silver Lake, KS';
$lang['TESTIMONIALS_5'] = '<b>My kids had croup</b> <br>I had no idea what to do for croup. Lucky me, I asked Medlanes for support and I could not be any happier!';
$lang['TESTIMONIALS_6'] = 'Samuel Trentini, 43<br>Norcross, GA';
$lang['TESTIMONIALS_7'] = '<b>Viral croup got us</b> <br>My son was suffering from croupy cough for week and only doctors at Medlanes were able to help him. It was super fast and reliable online service.';
$lang['TESTIMONIALS_8'] = 'Jill Marín, 48<br>Brownsville, TX';
$lang['TESTIMONIALS_9'] = '<b>Online doctor for croup</b> <br>My child had a loud, seal bark cough and I was desperate to make him feel better. Thankfully, I can contact Medlanes at any time of the day and was able to consult a doctor immediately!';
$lang['TESTIMONIALS_10'] = 'Alfie Fitch, 45<br>Commerce, GA';
$lang['TESTIMONIALS_11'] = '<b>Got rid of croup!</b> <br>I knew that croup in children is common, but I would always rather consult a doctor for treatment options. Doctors at Medlanes provided me with exactly what I was looking for!';
$lang['TESTIMONIALS_12'] = 'James Mata, 40<br>Seattle, WA';
$lang['TESTIMONIALS_13'] = '<b>What is croup cough?</b> <br>I am a recent mom and I was in distress when my toddler had croup. My online doctor managed to help me out and now both I and my baby are stress free!';
$lang['TESTIMONIALS_14'] = 'Lilian Cameron, 45<br>Mclean, VA';
$lang['TESTIMONIALS_15'] = '<b>Quick online croup treatment!</b> <br>My daughter started coughing unstoppably at night and I had no idea that we were dealing with spasmodic croup. Thank you Medlanes for providing me with information and treatment options. I will use your services in the future too!';
$lang['TESTIMONIALS_16'] = 'Charles Correia, 42<br>Phila, PA';
$lang['DOCTORS_0'] = 'Meet Our Experts';
$lang['DOCTORS_1'] = 'Allison Watters, MD';
$lang['DOCTORS_2'] = 'Board Certified Pediatrician<br> 1,903 People Helped';
$lang['DOCTORS_3'] = '4.81 / 5<br>Many parents don\'t know what croup sounds like and as a result can\'t tell how to treat their children. I am here to help all those parents and provide them with information.';
$lang['DOCTORS_4'] = 'Libby Ryder, MD';
$lang['DOCTORS_5'] = 'Board Certified Pediatrician<br> 2,869 People Helped';
$lang['DOCTORS_6'] = '4.91 / 5<br>Being an online doctor I can teach my patients how to treat croup at any time of the day. It makes me very happy that telemedicine makes providing support so easy!';
$lang['DOCTORS_7'] = 'Jack McKay, MD';
$lang['DOCTORS_8'] = 'Board Certified Pediatrician<br> 1,480 People Helped';
$lang['DOCTORS_9'] = '4.86 / 5<br>Spasmodic croup is sudden and severe. I am a practising pediatrician for 15 years now and I am more than happy to consult you!';
$lang['DOCTORS_10'] = 'Kelvin Lee, MD';
$lang['DOCTORS_11'] = 'Certified in Pediatrics<br> 1,689 People Helped';
$lang['DOCTORS_12'] = '4.78 / 5<br>Croup does not always require serious medication like steroids. To learn more about croup treatments, contact me at Medlanes. I have dealt online with over 10,000 pediatric conditions.';
$lang['DOCTORS_13'] = 'Jayce Timpani, MD';
$lang['DOCTORS_14'] = 'Board Certified - Pediatrics<br> 1,527 People Helped';
$lang['DOCTORS_15'] = '4.95 / 5<br>If you don\'t know what to do for croup, contact me. I am a skilled pediatrician and I am ready to help you through revolutionary Medlanes online platform.';
$lang['DOCTORS_16'] = 'Kelly Robbins, MD';
$lang['DOCTORS_17'] = 'Board Certified Pediatrician<br> 1,829 People Helped';
$lang['DOCTORS_18'] = '4.89 / 5<br>As an online doctor I have dealt with many viral croup cases. Contact me at Medlanes today in order to beat croup!';
$lang['PAYMENT_NEW4'] = 'Your expert is waiting for your question!';
$lang['PAYMENT_NEW5'] = 'Allison Watters, MD';
$lang['PAYMENT_NEW6'] = 'Board Certified Pediatrician<br> 1,903 People Helped';
$lang['PAYMENT_DOC_IMG'] = '<img src="images/payment/doc-an.jpg" width="180" height="180" alt="#"/>';
$lang['DEP_CC'] = 'Credit Card';
$lang['DEP_PP'] = 'PayPal';
$lang['DEP_PP_TEXT'] = 'You will be redirected to PayPal to complete your payment.';
$lang['THANKYOU_6'] = 'service@medlanes.com';
$lang['THANKYOU_7'] = '(800) 413-7290';
$lang['PRESS_0'] = 'Ask Your Online Pediatrician!';
$lang['PRESS_1'] = 'Our expert pediatricians have been extensively trained to support a wide variety of conditions online. We are here for you and your child. Our specialties include:';
$lang['PRESS_2'] = '<ul><li><h2>Rash Analysis</h2></li><li><h2>Mononucleosis</h2></li><li><h2>ADHD</h2></li><li><h2>Fevers</h2></li><li><h2>Ear Infections</h2></li><li><h2>Croup</h2></li><li><h2>and More!</h2></li></ul>';



?>