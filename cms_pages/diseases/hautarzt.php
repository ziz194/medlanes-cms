<?php 	

$lang['BXSLIDER'] = "<ul class='staticlink'>";
$lang['HEADER_1'] = 'Unser Kundensupport ist für Sie da!';
$lang['HEADER_2'] = '<b>0800 / 765 43 43</b> or <b>service@medlanes.com</b>';
$lang['INDEX_1'] = 'Fragen Sie einen Hautarzt';
$lang['INDEX_2'] = 'Unsere Hautärzte beantworten Ihre Fragen rund um das Thema Hautkrankheiten.<br><b>Wir garantieren 100% Kundenzufriedenheit!</b>';
$lang['TESTIMONIALS_0'] = 'Unsere Kunden über uns';
$lang['TESTIMONIALS_1'] = '<b>Akne war gestern.</b> <br>Dank der Online-Dermatologen habe ich meine Akne in den Griff bekommen und mein Selbstvertrauen zurück gewonnen.';
$lang['TESTIMONIALS_2'] = 'J. Weichsel, 42 <br> Iphofen';
$lang['TESTIMONIALS_3'] = '<b>Wunderbar!</b> <br>Ich habe lange Zeit mit Rosazea gekämpft. Nachdem ich einen Dermatologen online über Medlanes gefragt habe, konnte ich eine ganz individuelle Therapie beginnen, die mir sehr geholfen hat.';
$lang['TESTIMONIALS_4'] = 'B. Berger, 31 <br> Alsdorf';
$lang['TESTIMONIALS_5'] = '<b>Neurodermitis</b> <br>Mein Sohn leidet seit Geburt an Neurodermitis. Dermatologen online zu befragen ist ein erstaunlicher Service und hat uns sehr geholfen, die Symptome zu lindern. Wir sind alle so dankbar, danke!';
$lang['TESTIMONIALS_6'] = 'J. Bach, 37 <br> Trier';
$lang['TESTIMONIALS_7'] = '<b>Verdächtiges Muttermal</b> <br>Mein Freund fand ein Muttermal auf meinem Rücken. Ich konnte nicht länger auf einen Termin bei einem lokalen Hautarzt warten. Dank Medlanes bekam ich die Antworten auf meine Fragen sofort.';
$lang['TESTIMONIALS_8'] = 'T. Johanns, 39 <br> Dormagen';
$lang['TESTIMONIALS_9'] = '<b>Danke!</b> <br>Ich hatte Probleme mit Hautflecken. Es war mir peinlich zum Arzt zu gehen. Dann habe ich mich an Medlanes gewendet. Vielen Dank, Ihre Dermatologen sind wunderbar und sehr hilfsbereit!';
$lang['TESTIMONIALS_10'] = 'C. Teller, 45 <br> Potsdam';
$lang['TESTIMONIALS_11'] = '<b>Super Service</b> <br>Ich sprach mit einem Hautarzt online über die eingewachsene Haare. Der Hautarzt gab mir gute Ratschläge, schnell und einfach zu verstehen.';
$lang['TESTIMONIALS_12'] = 'S. Cramer, 27<br> Calau';
$lang['TESTIMONIALS_13'] = '<b>Akne Medikation</b> <br>Es hat sich herausgestellt, dass meine Akne-Medikamente Leberprobleme verursacht haben. Dank Medlanes konnte ich auf eine Alternative umsteigen.';
$lang['TESTIMONIALS_14'] = 'C. Rössner, 51<br> Langenhagen';
$lang['TESTIMONIALS_15'] = '<b>Frühblüher</b> <br>Ich bin allergisch auf Frühblüher. Dank der Hilfe durch den Dermatologen, habe ich die Symptome im Griff.';
$lang['TESTIMONIALS_16'] = 'K. Heitmann, 29<br> Ostfildern';
$lang['DOCTORS_0'] = 'Unsere Ärzte';
$lang['DOCTORS_1'] = 'Dr. A. Ackermann';
$lang['DOCTORS_2'] = 'Fachärztin für Dermatologie <br> 16 Jahre Praxiserfahrung';
$lang['DOCTORS_3'] = '4.89 / 5<br><br>Ich habe mehr als 16 Jahre Erfahrung in der Behandlung von Hautkrankheiten und bin hier, um zu helfen.';
$lang['DOCTORS_4'] = 'Dr. H. Jericke';
$lang['DOCTORS_5'] = 'Fachärztin für Dermatologie <br> 17 Jahre Praxiserfahrung';
$lang['DOCTORS_6'] = '4.87 / 5<br><br>Ich helfe bei dermatologischen Fragen online seit über 5 Jahren! Ich freue mich auf Ihre Fragen.';
$lang['DOCTORS_7'] = 'Dr. T. Schauff';
$lang['DOCTORS_8'] = 'Facharzt für Dermatologie <br> 15 Jahre Praxiserfahrung';
$lang['DOCTORS_9'] = '4.83 / 5<br><br>Ich möchte, dass sich die Patienten wieder wohl fühlen in Ihrer Haut. Zögern Sie nicht und stellen Sie Ihre Frage noch heute!';
$lang['DOCTORS_10'] = 'Dr. E. Wedemann';
$lang['DOCTORS_11'] = 'Facharzt für Dermatologie <br> 13 Jahre Praxiserfahrung';
$lang['DOCTORS_12'] = '4.91 / 5<br><br>Die Online Dermatologie ist ein sehr neues Konzept, und ich freue mich, mit dabei zu sein!';
$lang['DOCTORS_13'] = 'Dr. J. Eigner';
$lang['DOCTORS_14'] = 'Facharzt für Dermatologie <br> 19 Jahre Praxiserfahrung';
$lang['DOCTORS_15'] = '4.86 / 5<br><br>Viele Betroffene wissen nicht, dass Allergien schwerwiegende Folgen haben können. Klären Sie Ihre Fragen schnell!';
$lang['DOCTORS_16'] = 'Dr. W. Faber';
$lang['DOCTORS_17'] = 'Fachärztin für Dermatologie <br> 16 Jahre Praxiserfahrung';
$lang['DOCTORS_18'] = '4.83 / 5<br><br>Ich freue mich, Teil einer revolutionären Onlinedermatologen-Plattform wie Medlanes zu sein. Wir sind für Sie da!';
$lang['PAYMENT_NEW4'] = 'Ein Facharzt wartet auf Ihre Frage';
$lang['PAYMENT_NEW5'] = 'Dr. A. Ackermann';
$lang['PAYMENT_NEW6'] = 'Fachärztin für Dermatologie<br> 16 Jahre Praxiserfahrung';
$lang['PAYMENT_DOC_IMG'] = '<img src="images/payment/doc-an.jpg" width="180" height="180" alt="#"/>';
$lang['DEP_CC'] = 'Kreditkarte';
$lang['DEP_PP'] = 'PayPal';
$lang['DEP_PP_TEXT'] = 'Zur Bezahlung werden Sie zur PayPal Webseite weitergeleitet.';
$lang['THANKYOU_6'] = 'service@medlanes.com';
$lang['THANKYOU_7'] = '0800 / 765 43 43';
$lang['PRESS_0'] = 'Onlinehilfe bei Hautkrankheiten';
$lang['PRESS_1'] = 'Unsere Hautärzte sind in der Onlineberatung geschult. Wir helfen bei:';
$lang['PRESS_2'] = '<ul><li><h2>Akne<h2></li><li><h2>Hautausschlag</h2></li><li><h2>Neurodermitis</h2></li><li><h2>Allergien</h2></li></ul>';



?>