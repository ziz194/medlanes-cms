<?php 	

$lang['BXSLIDER'] = "<ul class='staticlink'>";
$lang['HEADER_1'] = 'Our support team is here for you 24/7!';
$lang['HEADER_2'] = '<b>(800) 413-7290</b> or <b>service@medlanes.com</b>';
$lang['INDEX_1'] = 'Ask A Neurologist Now';
$lang['INDEX_2'] = 'Expert Neurologists are available now to take your questions.<br>First consultations or second opinions, simply ask your question to get started.<br><b>Your satisfaction is 100% guaranteed. </b>';
$lang['TESTIMONIALS_0'] = 'What Our Patients Are Saying';
$lang['TESTIMONIALS_1'] = '<b>Peace of Mind</b> <br>My online neurologist recommended a few amazing home remedies that has allowed me to rid myself of my shooting pains in my legs. Thanks, Medlanes!';
$lang['TESTIMONIALS_2'] = 'Eleanor Piazza, 47<br>Little Rock, AR';
$lang['TESTIMONIALS_3'] = '<b>Online Neurologists</b> <br>Online neurologists can do virtually anything that a traditional office based neurologist can. Skip the visit, go with Medlanes!';
$lang['TESTIMONIALS_4'] = 'Crystal Ferreira, 43<br>Milwaukee, WI';
$lang['TESTIMONIALS_5'] = '<b>Neurology Experts</b> <br>I have personally spoken to 3 online neurologists in search of a second opinion, and each one of them has provided me with individual attention and compassion.';
$lang['TESTIMONIALS_6'] = 'Cody Tomholt, 38<br>Canton, MA';
$lang['TESTIMONIALS_7'] = '<b>Amazing</b> <br>My mother was developing a tremor, and her GP was unable to figure out what was going on. One quick consultation with my online neurologist led us to the proper diagnosis. Thanks!';
$lang['TESTIMONIALS_8'] = 'Madeline Huber, 44<br>New Orleans, LA';
$lang['TESTIMONIALS_9'] = '<b>Trusted Neurologists</b> <br>I have always struggled with shooting pain from my neck to elbow. I have received some of the best advice from my online neurologist. Turns out I have a herniated disc. Thank you, Dr, W!';
$lang['TESTIMONIALS_10'] = 'Angus Schmitz, 39<br>Detroit, MI';
$lang['TESTIMONIALS_11'] = '<b>Thank You</b> <br>Simply awesome! My expert online neurologist has guided me through so much regarding my recent MS diagnosis. I can sleep at night again. I am so thankful.';
$lang['TESTIMONIALS_12'] = 'Seth Giordano, 45<br>Jackson, MS';
$lang['TESTIMONIALS_13'] = '<b>Simply the Best</b> <br>I was a little skeptical at first, but the advice from my online neurologist gave me the best follow up advice. He made up for the lack of explanation from my office based neurologist.';
$lang['TESTIMONIALS_14'] = 'Holly Stable, 49<br>Marion, IL';
$lang['TESTIMONIALS_15'] = '<b>Many Thanks</b> <br>I was having some really negative side effects from recurring migraines. One quick consultation, and my neurologist provided me with useful remedies online. Many thanks!';
$lang['TESTIMONIALS_16'] = 'Tony Rodríguez, 49<br>Walnut Creek, CA';
$lang['DOCTORS_0'] = 'Meet Our Experts';
$lang['DOCTORS_1'] = 'Monica Gilbert, MD';
$lang['DOCTORS_2'] = 'Board Certified Neurologist<br> 3,815 People Helped';
$lang['DOCTORS_3'] = '4.81 / 5<br>Neurological problems can be scary to deal with. I am here to help any way I can!';
$lang['DOCTORS_4'] = 'Mackenzie Zimmer, MD';
$lang['DOCTORS_5'] = 'Board Certified Neurologist<br> 1,869 People Helped';
$lang['DOCTORS_6'] = '4.91 / 5<br>Online neurologists are the wave of the future, and I am proud of be part of it!';
$lang['DOCTORS_7'] = 'Jason Barstow, MD';
$lang['DOCTORS_8'] = 'Certified in Neurology<br> 1,480 People Helped';
$lang['DOCTORS_9'] = '4.86 / 5<br>Bringing neurology online has helped thousands of patients who are in need of second opinions.';
$lang['DOCTORS_10'] = 'James Wang, MD';
$lang['DOCTORS_11'] = 'Certified in Neurology<br> 1,689 People Helped';
$lang['DOCTORS_12'] = '4.78 / 5<br>Online neurologist? Yes! Welcome to the future of medicine. I am happy to assist you, and waiting for your questions.';
$lang['DOCTORS_13'] = 'Zack Bolton, MD';
$lang['DOCTORS_14'] = 'Board Certified - Neurology<br> 1,527 People Helped';
$lang['DOCTORS_15'] = '4.95 / 5<br>I am happy to help and advise on any neurological issues you may have.';
$lang['DOCTORS_16'] = 'Kendra Monty, MD';
$lang['DOCTORS_17'] = 'Board Certified Neurologist<br> 1,829 People Helped';
$lang['DOCTORS_18'] = '4.89 / 5<br>We brought neurology online. I am happy I have the ability to reach my patients on their time, not on mine!';
$lang['PAYMENT_NEW4'] = 'Your expert is waiting for your question!';
$lang['PAYMENT_NEW5'] = 'Monica Gilbert, MD';
$lang['PAYMENT_NEW6'] = 'Board Certified Neurologist<br> 3,815 People Helped';
$lang['PAYMENT_DOC_IMG'] = '<img src="images/payment/doc-an.jpg" width="180" height="180" alt="#"/>';
$lang['DEP_CC'] = 'Credit Card';
$lang['DEP_PP'] = 'PayPal';
$lang['DEP_PP_TEXT'] = 'You will be redirected to PayPal to complete your payment.';
$lang['THANKYOU_6'] = 'service@medlanes.com';
$lang['THANKYOU_7'] = '(800) 413-7290';
$lang['PRESS_0'] = 'Expert Neurologists At Your Service!';
$lang['PRESS_1'] = 'Our expert neurologists have been extensively trained to support a wide variety of conditions online. We are also able to provide advice for:';
$lang['PRESS_2'] = '<ul><li><h2>Migraines</h2></li><li><h2>Vertigo</h2></li><li><h2>Pinched Nerves</h2></li><li><h2>Headaches</h2></li><li><h2>Sleep Apnea</h2></li><li><h2>Fibromyalgia</h2></li><li><h2>and More!</h2></li></ul>';



?>