<?php 	

$lang['php_echo'] = 'Text';
$lang['BXSLIDER'] = "<ul class='staticlink'>";
$lang['HEADER_1'] = 'Our support team is here for you 24/7!';
$lang['HEADER_2'] = '<b>(800) 413-7290</b> or <b>service@medlanes.com</b>';
$lang['INDEX_1'] = 'Your Upper Respiratory Infection ends here.';
$lang['INDEX_2'] = 'Our pulmonologists are online now and ready to help you. Submit your question about Upper Respiratory Infection and feel better today!<b> Your satisfaction is 100% guaranteed.</b>';
$lang['TESTIMONIALS_0'] = 'What Our Patients Are Saying';
$lang['TESTIMONIALS_1'] = '<b>The best thing out there</b> <br>My children have respiratory infections regularly. With three children I can not run to the doctors office every time. So for me Medlanes is the perfect solution to get quick answers from a specialist. ';
$lang['TESTIMONIALS_2'] = 'Charlotte Cardoso, 43<br>Baltimore, MD';
$lang['TESTIMONIALS_3'] = '<b>Thumbs up</b> <br>I have always had repiratory problems and I did not think that online doctor could offer me the help I needed. Medlanes convinced me that I was wrong. Top quality care!';
$lang['TESTIMONIALS_4'] = 'Megan Nunan, 48<br>Jonesboro, GA';
$lang['TESTIMONIALS_5'] = '<b>Great customer service</b> <br>Me and my husband use the Medlane service regularly. My husband had a serious lung infection a while back and it was wonderful to be taken such great care of. ';
$lang['TESTIMONIALS_6'] = 'Toby Berg, 40<br>Los Angeles, CA';
$lang['TESTIMONIALS_7'] = '<b>Life savers!</b> <br>I am generally not a fan of going to the doctors. So Medlanes is a great solution for me. When I began having serious symptoms last winter, the doctor at Medlanes told me i probably have a dangerous viral infection and should go to the ER. That was a wise decision in the end. I probably would have waited it out until things got even worse.';
$lang['TESTIMONIALS_8'] = 'Tammi Burgess, 45<br>Cleveland, GA';
$lang['TESTIMONIALS_9'] = '<b>Online triage</b> <br>I had severe symptoms and was sure that I had a terrible respiratory infection. My pulmonologist at Medlanes told me to wait a few days before taking further steps and he was spot on. One day later things were already looking much better already and I saved myself and the doctors in the ER a lot of time and energy.';
$lang['TESTIMONIALS_10'] = 'Alexander Mondragón, 49<br>Bettie, TX';
$lang['TESTIMONIALS_11'] = '<b>My first choice</b> <br>I had a viral infection and got first-class help from the pulmolnoogist at Medlanes. Ever since then it is the first place I go for advice.';
$lang['TESTIMONIALS_12'] = 'Owen Segura, 49<br>Baltimore, MD';
$lang['TESTIMONIALS_13'] = '<b>Happy customer</b> <br>At my age bronchial infections are no joke. I need quick advice to be able to make the right choices and not put myself in unnecessary danger. At the same time I do not want to run to the Doctors office for every little flu. That is why I consider Medlanes to be a perfect solution!';
$lang['TESTIMONIALS_14'] = 'Hollie Perkins, 42<br>Seven Springs, PA';
$lang['TESTIMONIALS_15'] = '<b>Absolutely superb! </b> <br>As a mother of 3 kids I want to be informed about certain health risks my kids are exposed to. When there was a contagious bacterial infection spreading in my childs school I asked the doctor at Medlanes for help and got just the information I needed. ';
$lang['TESTIMONIALS_16'] = 'Kalvin Iadanza, 46<br>Teterboro, NJ';
$lang['DOCTORS_0'] = 'Meet Our Experts';
$lang['DOCTORS_1'] = 'Celine Karlsson, MD';
$lang['DOCTORS_2'] = 'Board Certified Pulmonologist <br> 1,903 People Helped';
$lang['DOCTORS_3'] = '4.81 / 5<br>I am a specialist on upper respiratory infection. I joined the Medlanes team a few months ago and so far it has been a great experience leaving me with healthy and happy patients. ';
$lang['DOCTORS_4'] = 'Leslie Tomaszewski, MD';
$lang['DOCTORS_5'] = 'Board Certified Pulmonologist <br> 2,869 People Helped';
$lang['DOCTORS_6'] = '4.91 / 5<br>I was introduced to Medlanes by a fellow physician and was immediately convinced to join the team. Its about time that health care joins the new century. ';
$lang['DOCTORS_7'] = 'Arthur Forester, MD';
$lang['DOCTORS_8'] = 'Board Certified Pulmonologist <br> 1,480 People Helped';
$lang['DOCTORS_9'] = '4.86 / 5<br>I see so many collegues that are frustrated with the direction that health care is going. I think online services such as Medlanes will make a real difference and improve the situation for both physicians and patients. ';
$lang['DOCTORS_10'] = 'Alexander Hayward, MD';
$lang['DOCTORS_11'] = 'Board Certified Pulmonologist <br> 1,689 People Helped';
$lang['DOCTORS_12'] = '4.78 / 5<br>Especially in the winter months the ER is full to the brim with patients suffering from upper respiratory infections. As an online doctor I can take some weight of their shoulders and filter out patients with minor problems that can be dealt with online.';
$lang['DOCTORS_13'] = 'Larry Bavin, MD';
$lang['DOCTORS_14'] = 'Board Certified Pulmonologist <br> 1,527 People Helped';
$lang['DOCTORS_15'] = '4.95 / 5<br>I have treated patients with respiratory infections for many years now. My experience with pleurisy, bronchitis, pneumonia patients and the like, help me to read the signs and symptoms quickly and give patients the advice they need.';
$lang['DOCTORS_16'] = 'Yasmin Barros, MD';
$lang['DOCTORS_17'] = 'Board Certified Pulmonologist <br> 1,829 People Helped';
$lang['DOCTORS_18'] = '4.89 / 5<br>Chest pain can drive patients into abosolute panic. In many cases it is nothing serious and can be dealt with very quickly. It is very gratifying to know that what I am doing at Medlanes is saving my collegues working at ER from lot of excess work. ';
$lang['PAYMENT_NEW4'] = 'Your expert is waiting for your question!';
$lang['PAYMENT_NEW5'] = 'Celine Karlsson, MD';
$lang['PAYMENT_NEW6'] = 'Board Certified Pulmonologist <br> 1,903 People Helped';
$lang['PAYMENT_DOC_IMG'] = '<img src="images/payment/doc-an.jpg" width="180" height="180" alt="#"/>';
$lang['DEP_CC'] = 'Credit Card';
$lang['DEP_PP'] = 'PayPal';
$lang['DEP_PP_TEXT'] = 'You will be redirected to PayPal to complete your payment.';
$lang['THANKYOU_6'] = 'service@medlanes.com';
$lang['THANKYOU_7'] = '(800) 413-7290';
$lang['PRESS_0'] = 'Ask Your Online Pulmonologist!';
$lang['PRESS_1'] = 'Our doctors have been extensively trained to treat pulmonary health conditions online! We can also help with:';
$lang['PRESS_2'] = '<ul><li><h2>COPD</h2></li><li><h2>Asthma</h2></li><li><h2>Bronchitis</h2></li><li><h2>Pulmonary Medication Questions</h2></li><li><h2>Emphysema</h2></li></ul>';


?>