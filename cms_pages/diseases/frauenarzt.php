<?php 	

$lang['BXSLIDER'] = "<ul class='staticlink'>";
$lang['HEADER_1'] = 'Unser Kundensupport ist für Sie da!';
$lang['HEADER_2'] = '<b>0800 / 765 43 43</b> or <b>service@medlanes.com</b>';
$lang['INDEX_1'] = 'Fragen Sie einen Frauenarzt';
$lang['INDEX_2'] = 'Unsere Frauenärzte beantworten Ihre Fragen rund um das Thema Frauengesundheit.<br><b>Wir garantieren 100% Kundenzufriedenheit!</b>';
$lang['TESTIMONIALS_0'] = 'Unsere Kunden über uns';
$lang['TESTIMONIALS_1'] = '<b>Ganz Anonym.</b> <br>Es ist mir ein bisschen unangenehm bei gynäkologischen Fragen einem Arzt direkt gegenüber zu stehen. Mit Medlanes kann ich einen Frauenarzt online fragen - ganz anonym.';
$lang['TESTIMONIALS_2'] = 'J. Borchert, 27 <br> Monschau';
$lang['TESTIMONIALS_3'] = '<b>Einfach und bequem.</b> <br>Ich hatte eine kurze Frage zu Verhütungsmethoden. Vom Frauenarzt haben ich die Info bequem online bekommen.';
$lang['TESTIMONIALS_4'] = 'A. Petersmann, 29 <br> Bad Oldesloe';
$lang['TESTIMONIALS_5'] = '<b>Es hat geklappt!</b> <br>Seit über einem Jahr versuchen mein Mann und ich schon schwanger zu werden. Mit den Tipps vom Online Frauenarzt hat es endlich geklappt.';
$lang['TESTIMONIALS_6'] = 'K. Eichwald, 32 <br> Neckarsulm';
$lang['TESTIMONIALS_7'] = '<b>Beschwerden in den Wechseljahren.</b> <br>Die Schlafstörungen und Hitzewallungen haben mich wahnsinnig gemacht. Vom Frauenarzt habe ich online wertvolle Infos bekommen, um die Beschwerden in den Griff zu bekommen.';
$lang['TESTIMONIALS_8'] = 'C. Schweiger, 51<br> Werl';
$lang['TESTIMONIALS_9'] = '<b>Stimmungsschwankungen</b> <br>Meine Freundin leidet oft unter Stimmungsschwankungen während ihrer Tage. Die Online Ärzte von Medlanes haben mir Tipps gegeben, um meine Freundin zu unterstützen.';
$lang['TESTIMONIALS_10'] = 'O. Welker, 25 <br> Freising';
$lang['TESTIMONIALS_11'] = '<b>Zum Glück.</b> <br>Ich bin per Zufall auf Medlanes gestoßen. Bis dahin wusste ich nicht, dass Antibiotika und die Pille sich nicht vertragen. Zum Glück ist bis jetzt alles gut gegangen.';
$lang['TESTIMONIALS_12'] = 'P. Gellershagen, 22<br> Porta Westfalica';
$lang['TESTIMONIALS_13'] = '<b>Mein Frauenarzt online.</b> <br>Ich habe endlich einen Frauenarzt gefunden, der mich versteht. Bei kleinen Fragen kann ich mich jederzeit an ihn wenden.';
$lang['TESTIMONIALS_14'] = 'V. Thomalla, 38<br> Laubach';
$lang['TESTIMONIALS_15'] = '<b>Depressionen in der Schwangerschaft.</b> <br>Ich bin sehr früh Mutter geworden. Die frühe Phase meiner Schwangerschaft war nicht einfach. Dank Medlanes bin ich jetzt eine sehr glückliche Mutter.';
$lang['TESTIMONIALS_16'] = 'K. Hollmann, 22<br> Jena';
$lang['DOCTORS_0'] = 'Unsere Ärzte';
$lang['DOCTORS_1'] = 'Dr. P. Wirsing';
$lang['DOCTORS_2'] = 'Fachärztin für Frauenheilkunde und Geburtshilfe <br> 18 Jahre Praxiserfahrung';
$lang['DOCTORS_3'] = '4.85 / 5<br><br>Die Beantwortung der Fragen online ist einfach und unkompliziert. Ich kann vielen Patienten helfen, ohne mich im Praxisalltag einzuschränken.';
$lang['DOCTORS_4'] = 'Dr. S. Berger';
$lang['DOCTORS_5'] = 'Fachärztin für Frauenheilkunde und Geburtshilfe <br> 15 Jahre Praxiserfahrung';
$lang['DOCTORS_6'] = '4.92 / 5<br><br>Ich habe eine ausführliche Schulung bekommen, um online Fragen zu beantworten. Ich freue mich, Teil dieses Projektes zu sein.';
$lang['DOCTORS_7'] = 'Dr. A. Seilbach';
$lang['DOCTORS_8'] = 'Fachärztin für Frauenheilkunde und Geburtshilfe <br> 16 Jahre Praxiserfahrung';
$lang['DOCTORS_9'] = '4.71 / 5<br><br>Mich erreichen vor allem Fragen von jungen Patientinnen, denen es unangenehm ist, zum Arzt zu gehen. Die Aufklärung ist jedoch enorm wichtig. ';
$lang['DOCTORS_10'] = 'Dr. S. Perlhans';
$lang['DOCTORS_11'] = 'Fachärztin für Frauenheilkunde und Geburtshilfe <br> 9 Jahre Praxiserfahrung';
$lang['DOCTORS_12'] = '4.8 / 5<br><br>Ich bin sehr technik-affin. Ich halte die Onlinebetreuung für eine wunderbahre Möglichkeit, Patienten zu versorgen und dabei in kurzer Zeit viel dazu zu lernen.';
$lang['DOCTORS_13'] = 'Dr. H. Wölscher';
$lang['DOCTORS_14'] = 'Facharzt für Frauenheilkunde und Geburtshilfe  <br> 19 Jahre Praxiserfahrung';
$lang['DOCTORS_15'] = '4.81 / 5<br><br>Ich habe bereits über 1000 Fragen zu gynäkologischen Themen online beantwortet und freue mich über jede neue Frage.';
$lang['DOCTORS_16'] = 'Dr. L. Dieckmer';
$lang['DOCTORS_17'] = 'Fachärztin für Frauenheilkunde und Geburtshilfe  <br> 16 Jahre Praxiserfahrung';
$lang['DOCTORS_18'] = '4.78 / 5<br><br>Ich habe ein breites Wissen, wenn es um das Thema Frauengesundheit geht und möchte durch meine Expertise Medlanes unterstützen.';
$lang['PAYMENT_NEW4'] = 'Ein Facharzt wartet auf Ihre Frage';
$lang['PAYMENT_NEW5'] = 'Dr. P. Wirsing';
$lang['PAYMENT_NEW6'] = 'Fachärztin für Frauenheilkunde und Geburtshilfe <br> 18 Jahre Praxiserfahrung';
$lang['PAYMENT_DOC_IMG'] = '<img src="images/payment/doc-an.jpg" width="180" height="180" alt="#"/>';
$lang['DEP_CC'] = 'Kreditkarte';
$lang['DEP_PP'] = 'PayPal';
$lang['DEP_PP_TEXT'] = 'Zur Bezahlung werden Sie zur PayPal Webseite weitergeleitet.';
$lang['THANKYOU_6'] = 'service@medlanes.com';
$lang['THANKYOU_7'] = '0800 / 765 43 43';
$lang['PRESS_0'] = 'Onlinehilfe bei Frauenerkrankungen ';
$lang['PRESS_1'] = 'Unsere Frauenärzte sind in der Onlineberatung geschult. Wir helfen bei:';
$lang['PRESS_2'] = '<ul><li><h2>Schwangerschaft</h2></li><li><h2>Brustkrebs</h2></li><li><h2>Wechseljahre</h2></li><li><h2>Regelschmerzen</h2></li></ul>';




?>