<?php 	

$lang['php_echo'] = 'Text';
$lang['BXSLIDER'] = "<ul class='staticlink'>";
$lang['HEADER_1'] = 'Our support team is here for you 24/7!';
$lang['HEADER_2'] = '<b>(800) 413-7290</b> or <b>service@medlanes.com</b>';
$lang['INDEX_1'] = 'Ask A Surgeon Now';
$lang['INDEX_2'] = 'Our expert surgeons are online now and here to help. <br>Stop suffering from unclarity. Submit your question and feel better today!<br><b>Your satisfaction is 100% guaranteed.</b>';
$lang['TESTIMONIALS_0'] = 'We Provide Clarity';
$lang['TESTIMONIALS_1'] = '<b>I Made A Better Decision</b> <br>My Doctor recommended a surgical procedure that I was sure I needed. Then I found Medlanes and got a second opinion online and realized that surgery is not the only option. The online doctors have been great and their service is second to none!';
$lang['TESTIMONIALS_2'] = 'Imogen Traugott, 39<br>North Wilkesboro, NC';
$lang['TESTIMONIALS_3'] = '<b>Know The Risks</b> <br> Surgery risks are common during such procedures, but I needed to know every possible outcome before undertaking the surgical measure. Medlanes is the best online surgeon platform around and thanks to them, I have found the perfect surgery support!';
$lang['TESTIMONIALS_4'] = 'Lauren Nussbaum, 33<br>South Bend, IN';
$lang['TESTIMONIALS_5'] = '<b>Best Surgery Help!</b> <br>Finally! That is all I have to say. I have seen various Doctors who recommended an invasive surgery, but Dr. Johnson has found a less invasive surgery option for me. Thank you so much!';
$lang['TESTIMONIALS_6'] = 'Toby Moretti, 40<br>West Allis, WI';
$lang['TESTIMONIALS_7'] = '<b>Finally Feeling Right!</b> <br>After my surgery, my recovery took longer than it should have. As I was not able to leave my bed easily; Medlanes really helped me with surgery complications online!I have finally gotten the answers and support I need to recover fast. Thanks!';
$lang['TESTIMONIALS_8'] = 'Phoebe Joyce, 33<br>San Francisco, CA';
$lang['TESTIMONIALS_9'] = '<b>No More Weird Symptoms</b> <br>I was recommended by a friend to try Medlanes who had a lot of success with her surgery question online from Medlanes. I have been working with Dr. Evans ever since. I finally feel like I have beat the symptoms assiciated with post-surgery care for good.';
$lang['TESTIMONIALS_10'] = 'Jesse McDonald, 55<br>San Francisco, CA';
$lang['TESTIMONIALS_11'] = '<b>Best Online Surgeon</b> <br>Talking with Dr. Forney has been such a blessing. Before I went into surgery, I needed information about surgery preperation online and cannot thank Medlanes enough for being there for me day and night. What a wonderful online surgeon platform.';
$lang['TESTIMONIALS_12'] = 'Joshua Herrmann, 25<br>Richardson, TX';
$lang['TESTIMONIALS_13'] = '<b>Ask A Surgeon Online</b> <br>I will be honest, I thought it was not possible to ask a surgeon question online to my doctor. After using the service a few times, I can get surgery help online 24 hours a day virtually. From anywhere as well. Thank you to Medlanes!';
$lang['TESTIMONIALS_14'] = 'Linda Santos, 50<br>El Paso, TX';
$lang['TESTIMONIALS_15'] = '<b>Surgery Medication Help</b> <br> Before proceeding with my surgical procedure, I had various questions about anesthesia recommendations online. The online Doctor at Medlanes advised me and answered all my medical questions. This put my mind at ease. Thank you everyone!';
$lang['TESTIMONIALS_16'] = 'Richard Wulf, 20<br>Frederick, MD';
$lang['DOCTORS_0'] = 'Meet our Providers';
$lang['DOCTORS_1'] = 'Lilian Dill, MD';
$lang['DOCTORS_2'] = 'Board Certified Surgeon <br> 1,270 People helped';
$lang['DOCTORS_3'] = '4.73 / 5<br>I have over 15 years of experience answering surgery questions and symptoms related to surgery. I have been specially trained to answer questions about surgical procedures online!';
$lang['DOCTORS_4'] = 'Karen Young, MD';
$lang['DOCTORS_5'] = 'Board Certified Surgeon <br> 1,186 People helped';
$lang['DOCTORS_6'] = '4.86 / 5<br>I have treated well over 10,000 cases of questions about surgery and related symptoms since starting practice. I am an expert in the field of online surgery tips! ';
$lang['DOCTORS_7'] = 'Walter Goodwin, MD';
$lang['DOCTORS_8'] = 'Diplomat of Surgery <br> 1,861 People helped';
$lang['DOCTORS_9'] = '4.98 / 5<br>Surgery does not have to be frightening. My patients felt relieved with personalized surgery options online that have been custom tailored to meet their specific needs.';
$lang['DOCTORS_10'] = 'Arthur Burt, MD';
$lang['DOCTORS_11'] = 'Board Certified Surgeon <br> 1,101 People helped';
$lang['DOCTORS_12'] = '4.85 / 5<br>Online surgery help is a very new concept and I am excited to be on the ground floor of revolutionizing the medical advice online and through digital methods.';
$lang['DOCTORS_13'] = 'Jason Evans, MD';
$lang['DOCTORS_14'] = 'Certified Surgeon <br> 855 People helped';
$lang['DOCTORS_15'] = '4.93 / 5<br>I have been giving online medical advice for about 3 years now, and can provide expert opinioins on a wide range of issues involving surgery. I am here to help!';
$lang['DOCTORS_16'] = 'Audrey Cunha, MD';
$lang['DOCTORS_17'] = 'Board Certified Surgeon <br> 1,576 People helped';
$lang['DOCTORS_18'] = '4.79 / 5<br>I am so excited to be a part of a revolutionary online doctor platform like Medlanes. We are here for you 24/7!';
$lang['PAYMENT_NEW4'] = 'Your expert is waiting for your question!';
$lang['PAYMENT_NEW5'] = 'Lilian Dill, MD';
$lang['PAYMENT_NEW6'] = 'Board Certified Surgeon <br> 1,270 People helped';
$lang['PAYMENT_DOC_IMG'] = '<img src="images/payment/doc-an.jpg" width="180" height="180" alt="#"/>';
$lang['DEP_CC'] = 'Credit Card';
$lang['DEP_PP'] = 'PayPal';
$lang['DEP_PP_TEXT'] = 'You will be redirected to PayPal to complete your payment.';
$lang['THANKYOU_6'] = 'service@medlanes.com';
$lang['THANKYOU_7'] = '(800) 413-7290';
$lang['PRESS_0'] = 'Online Surgery Support!';
$lang['PRESS_1'] = 'Our doctors have been extensively trained to answer surgery questions online! We can also help with:';
$lang['PRESS_2'] = '<ul><li><h2>Vasectomy</h2></li><li><h2>Mastectomy</h2></li><li><h2>Skin Biopsy</h2></li><li><h2>Surgery Medication Questions</h2></li><li><h2>Osteotomy</h2></li></ul>';


?>