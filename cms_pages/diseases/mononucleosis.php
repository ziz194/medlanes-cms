<?php 	

$lang['php_echo'] = 'Text';
$lang['BXSLIDER'] = "<ul class='staticlink'>";
$lang['HEADER_1'] = 'Our support team is here for you 24/7!';
$lang['HEADER_2'] = '<b>(800) 413-7290</b> or <b>service@medlanes.com</b>';
$lang['INDEX_1'] = 'Questions about Mononucleosis?';
$lang['INDEX_2'] = 'Our pediatricians are online now and ready to help you. Submit your question about Mononucleosis and feel better today!<b> Your satisfaction is 100% guaranteed.</b>';
$lang['TESTIMONIALS_0'] = 'What Our Patients Are Saying';
$lang['TESTIMONIALS_1'] = '<b>Such quick service</b> <br>When I noticed mononucleosis symptoms, the first thing I did was consult my online doctor at Medlanes. He never fails to help me!';
$lang['TESTIMONIALS_2'] = 'Savannah Ocasio, 38<br>Springfield, MO';
$lang['TESTIMONIALS_3'] = '<b>Support I needed</b> <br>I never though I would get mono disease in my forties, but here I was. I am so glad I could find help at Medlanes. My doctor was suppotive and great!';
$lang['TESTIMONIALS_4'] = 'Charlotte Scott, 48<br>O Fallon, MO';
$lang['TESTIMONIALS_5'] = '<b>Unlucky me</b> <br>When I kissed \'Tom\' I had no idea he had infectious mononucleosis. Thankfully, Medlanes gave me useful tips on how to battle it and now I feel great. I stopped seeing Tom though.';
$lang['TESTIMONIALS_6'] = 'Seth Boyle, 38<br>Bayonne, NJ';
$lang['TESTIMONIALS_7'] = '<b>Teacher I never had</b> <br>I did not know that mono is contagious. Thank you Medlanes for teaching me!';
$lang['TESTIMONIALS_8'] = 'Joyce Jamieson, 45<br>Aurora, IL';
$lang['TESTIMONIALS_9'] = '<b>I feel great again!</b> <br>I was wondering how long does mono last, because I have been battling it for 3 weeks. I asked Medlanes and not only I got an answer, I got a doctor\'s consultation and treatment information. Great service!';
$lang['TESTIMONIALS_10'] = 'Matthew Builder, 48<br>Madisonville, KY';
$lang['TESTIMONIALS_11'] = '<b>The most adequate service</b> <br>It has always been a mystery to me how do you get mono, until I got it. I felt so bad I could not leave home. I am so happy that Medlanes exist and I could talk with a doctor just by using my laptop. Thank you, I feel so much better now.';
$lang['TESTIMONIALS_12'] = 'Jeffrey Alcalá, 47<br>Cumberland, MD';
$lang['TESTIMONIALS_13'] = '<b>Fun times</b> <br>Freshmen year has been so much fun until the kissing disease got me. I was too embarrased to face anyone about it, so Medlanes has been a great solution. Even more, it was best doctor I have ever spoken to!';
$lang['TESTIMONIALS_14'] = 'Taylah Frost, 40<br>Gate, OK';
$lang['TESTIMONIALS_15'] = '<b>Revolutionary platform</b> <br>Symptoms of mononucleosis are so hard to deal with. Without the help of my online doctor at Medlanes I would have managed to go through mononucleosis!';
$lang['TESTIMONIALS_16'] = 'Archie Kelly, 48<br>Waynesboro, VA';
$lang['DOCTORS_0'] = 'Meet Our Experts';
$lang['DOCTORS_1'] = 'Allison Watters, MD';
$lang['DOCTORS_2'] = 'Board Certified Pediatrician<br> 1,903 People Helped';
$lang['DOCTORS_3'] = '4.81 / 5<br>Did you know that mononucleosis is contagious? If you would like to find out more, contact me at Medlanes. I am available 24/7';
$lang['DOCTORS_4'] = 'Libby Ryder, MD';
$lang['DOCTORS_5'] = 'Board Certified Pediatrician<br> 2,869 People Helped';
$lang['DOCTORS_6'] = '4.91 / 5<br>Working via an online platform has allowed me to be the best pediatrician I can be, simply because I can help so many more people. I can help you too!';
$lang['DOCTORS_7'] = 'Jack McKay, MD';
$lang['DOCTORS_8'] = 'Board Certified Pediatrician<br> 1,480 People Helped';
$lang['DOCTORS_9'] = '4.86 / 5<br>It has been my lifelong goal to be a pediatrician and provide help to those in need. Medlanes is a great way to achieve that. With us, health care is faster, cheaper and more reliable!';
$lang['DOCTORS_10'] = 'Kelvin Lee, MD';
$lang['DOCTORS_11'] = 'Certified in Pediatrics<br> 1,689 People Helped';
$lang['DOCTORS_12'] = '4.78 / 5<br>I am treating mononucleosis cases every day and I could help you too. Reach me at Medlanes any time for expert medical opinion.';
$lang['DOCTORS_13'] = 'Jayce Timpani, MD';
$lang['DOCTORS_14'] = 'Board Certified - Pediatrics<br> 1,527 People Helped';
$lang['DOCTORS_15'] = '4.95 / 5<br>Mono symptoms are hard to deal with and require a lot of patience. As a skilled pediatrician I can provide help and information how to alleviate said symptoms. Contact me today.';
$lang['DOCTORS_16'] = 'Kelly Robbins, MD';
$lang['DOCTORS_17'] = 'Board Certified Pediatrician<br> 1,829 People Helped';
$lang['DOCTORS_18'] = '4.89 / 5<br>Not many people are aware how serious are the symptoms of mono. I am ready to provide you with information and treatments that you need.';
$lang['PAYMENT_NEW4'] = 'Your expert is waiting for your question!';
$lang['PAYMENT_NEW5'] = 'Allison Watters, MD';
$lang['PAYMENT_NEW6'] = 'Board Certified Pediatrician<br> 1,903 People Helped';
$lang['PAYMENT_DOC_IMG'] = '<img src="images/payment/doc-an.jpg" width="180" height="180" alt="#"/>';
$lang['DEP_CC'] = 'Credit Card';
$lang['DEP_PP'] = 'PayPal';
$lang['DEP_PP_TEXT'] = 'You will be redirected to PayPal to complete your payment.';
$lang['THANKYOU_6'] = 'service@medlanes.com';
$lang['THANKYOU_7'] = '(800) 413-7290';
$lang['PRESS_0'] = 'Ask Your Online Pediatrician!';
$lang['PRESS_1'] = 'Our expert pediatricians have been extensively trained to support a wide variety of conditions online. We are here for you and your child. Our specialties include:';
$lang['PRESS_2'] = '<ul><li><h2>Rash Analysis</h2></li><li><h2>Mononucleosis</h2></li><li><h2>ADHD</h2></li><li><h2>Fevers</h2></li><li><h2>Ear Infections</h2></li><li><h2>Croup</h2></li><li><h2>and More!</h2></li></ul>';


?>