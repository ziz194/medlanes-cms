<?php 	

$lang['php_echo'] = 'Text';
$lang['BXSLIDER'] = "<ul class='staticlink'>";
$lang['HEADER_1'] = 'Our support team is here for you 24/7!';
$lang['HEADER_2'] = '<b>(800) 413-7290</b> or <b>service@medlanes.com</b>';
$lang['INDEX_1'] = 'Ask a Urologist Now';
$lang['INDEX_2'] = 'Our expert urologists are online now and here to help. Submit your question and feel better today!<b> Your satisfaction is 100% guaranteed.</b>';
$lang['TESTIMONIALS_0'] = 'What Our Patients Say';
$lang['TESTIMONIALS_1'] = '<b>I Beat My Urological Problem.</b> <br>I have had a lot of trouble with my urological problem in the past. I finally found the perfect urological disease treatment online with Medlanes. The doctors have been great and their service is second to none!';
$lang['TESTIMONIALS_2'] = 'Janet Watson, 43 <br>Derby';
$lang['TESTIMONIALS_3'] = '<b>Kiss Your Urological Problem Goodbye.</b> <br>I have always shown symptoms of a urological condition, but never wanted to talk to a doctor face to face. Medlanes is the best online urologist platform around and thanks to them, I have found the perfect urological health support!';
$lang['TESTIMONIALS_4'] = 'Betty Wylie, 29 <br>Mansfield';
$lang['TESTIMONIALS_5'] = '<b>Best Help For Urological Problems!</b> <br>Finally! That is all I have to say. Treatments for urological diseases are confusing and hard to understand. Dr. Johnson has found the perfect urological condition treatment for me, and I am finally feeling like a normal human being again. Thank you so much!';
$lang['TESTIMONIALS_6'] = 'Jack Boles, 37 <br>Alford';
$lang['TESTIMONIALS_7'] = '<b>Finally Feeling Right!</b> <br>Urological conditions run in my family. I was not even aware that it was genetic prior to speaking with my urologist online through Medlanes. I have finally gotten the answers and support I need to treat my urological problem. Thanks!';
$lang['TESTIMONIALS_8'] = 'Tameka Jones, 39<br>Grimsby';
$lang['TESTIMONIALS_9'] = '<b>No More Urological Problems</b> <br>I was recommended by a friend to try Medlanes who had a lot of success with her treatment for urology conditions online from Medlanes. I have been working with Dr. Evans ever since. I finally feel like I have beat my urological disease for good.';
$lang['TESTIMONIALS_10'] = 'Clay Teller, 59 <br>Leeds';
$lang['TESTIMONIALS_11'] = '<b>Best Online Urologist</b> <br>Talking with Dr. Forney has been such a blessing. I finally have the tools I need in place to support myself and control of my urological symptoms. I cannot thank Medlanes enough. What a wonderful online urologist platform.';
$lang['TESTIMONIALS_12'] = 'Shane Carter, 27<br>Middlesbrough';
$lang['TESTIMONIALS_13'] = '<b>Ask An Urologist Online</b> <br>I will be honest, I thought it was not possible to ask an urologist question online to my doctor. After using the service a few times, I can get urological help online 24 hours a day virtually. From anywhere as well. Thank you to Medlanes!';
$lang['TESTIMONIALS_14'] = 'Christine Roseman, 51<br>Cockermouth';
$lang['TESTIMONIALS_15'] = '<b>Urology Medication Help</b> <br>I started taking an pyridium a few weeks ago and was having weird side effects. I had all my medication questions answered, and realized that the side effects were completely normal. This put my mind at ease. Thank you everyone!';
$lang['TESTIMONIALS_16'] = 'Kyle Holmes, 22<br>Whitehaven';
$lang['DOCTORS_0'] = 'Meet our Providers';
$lang['DOCTORS_1'] = 'Helen Eckhoff, MD';
$lang['DOCTORS_2'] = 'Board Certified Urologist <br> 2,100+ People Helped';
$lang['DOCTORS_3'] = '4.85 / 5<br>I have over 15 years of experience treating urological conditions and problems that are related to and/or cause urological symptoms. I have been specially trained to treat urology diseases online!';
$lang['DOCTORS_4'] = 'Renee Booker, MD';
$lang['DOCTORS_5'] = 'Board Certified Urologist <br> 1,611 People Helped';
$lang['DOCTORS_6'] = '4.99 / 5<br>I have treated well over 10,000 cases of urological conditions  and related diseases since starting practice. I am an expert in the treatment of urological symptoms!';
$lang['DOCTORS_7'] = 'Mathew Mullins, MD';
$lang['DOCTORS_8'] = 'Board Certified Urologist <br> 870 People Helped';
$lang['DOCTORS_9'] = '4.76 / 5<br>Urological diseases do not have to control you. My patients have overcome their issues with urological conditions with specialized urology treatments that have been custom tailored to meet their specific needs.';
$lang['DOCTORS_10'] = 'Sebastian Kruglow, MD';
$lang['DOCTORS_11'] = 'Board Certified Urologist <br> 1,123 People Helped';
$lang['DOCTORS_12'] = '4.81 / 5<br>Online urologists is a very new concept and I am excited to be on the ground floor of revolutionizing the treatment of urological diseases online and through digital methods.';
$lang['DOCTORS_13'] = 'Albert Lowe, MD';
$lang['DOCTORS_14'] = 'Board Certified Urologist <br> 1,432 People Helped';
$lang['DOCTORS_15'] = '4.86 / 5<br>I have been giving online advice for about 3 years now, and can with with a wide range of issues involving urological conditions. I am here to help!';
$lang['DOCTORS_16'] = 'Elizabeth Ferrera, MD';
$lang['DOCTORS_17'] = 'Board Certified Urologist <br> 1,376 People Helped';
$lang['DOCTORS_18'] = '4.83 / 5<br>I am so excited to be a part of a revolutionary online doctor platform like Medlanes. We are here for you 24/7!';
$lang['PAYMENT_NEW4'] = 'Your expert is waiting for your question!';
$lang['PAYMENT_NEW5'] = 'Helen Eckhoff, MD';
$lang['PAYMENT_NEW6'] = 'Board Certified Urologist <br> 2,100+ People Helped';
$lang['PAYMENT_DOC_IMG'] = '<img src="images/payment/doc-an.jpg" width="180" height="180" alt="#"/>';
$lang['DEP_CC'] = 'Credit Card';
$lang['DEP_PP'] = 'PayPal';
$lang['DEP_PP_TEXT'] = 'You will be redirected to PayPal to complete your payment.';
$lang['THANKYOU_6'] = 'service@medlanes.com';
$lang['THANKYOU_7'] = '(800) 413-7290';
$lang['PRESS_0'] = 'Online Urological Health Support!';
$lang['PRESS_1'] = 'Our doctors have been extensively trained to treat urological health conditions online! We can also help with:';
$lang['PRESS_2'] = '<ul><li>Urinary Tract Infection</li><li>Kidney Stones</li><li>Urinary Incontinence</li><li>Urological Medication Questions</li><li>Erectile Dysfunction</li></ul>';


?>