<?php 	

$lang['php_echo'] = 'Text';
$lang['BXSLIDER'] = "<ul class='staticlink'>";
$lang['HEADER_1'] = 'Our support team is here for you 24/7!';
$lang['HEADER_2'] = '<b>(800) 413-7290</b> or <b>service@medlanes.com</b>';
$lang['INDEX_1'] = 'Tackle Whooping Cough with us.';
$lang['INDEX_2'] = 'Our pediatricians are online now and ready to help you. Submit your question about Whooping Cough and feel better today!<b> Your satisfaction is 100% guaranteed.</b>';
$lang['TESTIMONIALS_0'] = 'What Our Patients Are Saying';
$lang['TESTIMONIALS_1'] = '<b>Quick response</b> <br>Whooping cough was so tough on my daughter, thankfully Medlanes gave me useful information on how to cope with it. We are whooping cough free now!';
$lang['TESTIMONIALS_2'] = 'Nancy Genovese, 39<br>Hays, KS';
$lang['TESTIMONIALS_3'] = '<b>Sick children</b> <br>My children came home with pertussis one day and I had no idea how to cope with it. And then, Medlanes provided me with all the help I needed.';
$lang['TESTIMONIALS_4'] = 'Juanita Alexander, 49<br>Booneville, MS';
$lang['TESTIMONIALS_5'] = '<b>Just what I needed</b> <br>I have been battling with whooping cough for weeks when I heard about Medlanes. I decided to give it a try and I could not be happier! I am cough free now.';
$lang['TESTIMONIALS_6'] = 'Jack Trevisani, 41<br>Erie, PA';
$lang['TESTIMONIALS_7'] = '<b>Happy customer</b> <br>It is not the first time I used Medlanes services and once again, I am 100% satisfied. My online doctor provided me with the best pertussis treatment.';
$lang['TESTIMONIALS_8'] = 'Melissa Bryan, 46<br>Dayton, OH';
$lang['TESTIMONIALS_9'] = '<b>Healthy and happy</b> <br>I found out that whooping cough is contagious when I fell ill straight after my daughter did. My symptoms were worse so I decided to consult an online doctor and I got better a week afterwards!';
$lang['TESTIMONIALS_10'] = 'Cameron Gerber, 44<br>Hampton, GA';
$lang['TESTIMONIALS_11'] = '<b>I am loving Medlanes</b> <br>It is not easy to find treatment for whooping cough but Medlanes provided me with exactly what I was looking for. Thank you so much! I am recommending you to all of my friends and family!';
$lang['TESTIMONIALS_12'] = 'Levi Brown, 49<br>Racine, WI';
$lang['TESTIMONIALS_13'] = '<b>Healthy baby</b> <br>I never dealt with whooping cough in infants so when my baby got sick I did not know what to do. An online doctor from Medlanes helped me though and now my baby is growing happy and strong.';
$lang['TESTIMONIALS_14'] = 'Amelie Bryan, 49<br>Lubbock, TX';
$lang['TESTIMONIALS_15'] = '<b>Blessing from the skies</b> <br>I was so sick that I started throwing up from coughing and it was really scary. I could not even leave my home, but I was blessed with great internet connection and Medlanes. I got help immediately and I got better instantly!';
$lang['TESTIMONIALS_16'] = 'Caleb Currey, 46<br>Philadelphia, PA';
$lang['DOCTORS_0'] = 'Meet Our Experts';
$lang['DOCTORS_1'] = 'Allison Watters, MD';
$lang['DOCTORS_2'] = 'Board Certified Pediatrician<br> 1,903 People Helped';
$lang['DOCTORS_3'] = '4.81 / 5<br>Many patients are unaware of signs of whooping cough. It is my duty to inform them and help them if they deal with something unknown. Contact me!';
$lang['DOCTORS_4'] = 'Libby Ryder, MD';
$lang['DOCTORS_5'] = 'Board Certified Pediatrician<br> 2,869 People Helped';
$lang['DOCTORS_6'] = '4.91 / 5<br>I am a practising pediatrician and provide health advise for anyone who needs it. Medlanes allows me to help people around the globe. I coud help you too!';
$lang['DOCTORS_7'] = 'Jack McKay, MD';
$lang['DOCTORS_8'] = 'Board Certified Pediatrician<br> 1,480 People Helped';
$lang['DOCTORS_9'] = '4.86 / 5<br>If you need medical advice or pediatric help, contact me at Medlanes. I have been practising pediatrics for a long time and I am sure I can help you out.';
$lang['DOCTORS_10'] = 'Kelvin Lee, MD';
$lang['DOCTORS_11'] = 'Certified in Pediatrics<br> 1,689 People Helped';
$lang['DOCTORS_12'] = '4.78 / 5<br>People with whooping cough should seek medical advice. This is what Medlanes is for! We are available 24/7.';
$lang['DOCTORS_13'] = 'Jayce Timpani, MD';
$lang['DOCTORS_14'] = 'Board Certified - Pediatrics<br> 1,527 People Helped';
$lang['DOCTORS_15'] = '4.95 / 5<br>Who would have thought that it would be possible diagnosing whooping cough online. If you are unsure what is your condition, contact me for advise!';
$lang['DOCTORS_16'] = 'Kelly Robbins, MD';
$lang['DOCTORS_17'] = 'Board Certified Pediatrician<br> 1,829 People Helped';
$lang['DOCTORS_18'] = '4.89 / 5<br>Hard coughing, sore throat, fever and nasal congestion are signs of whooping cough. If you notice these symptoms, contact me at Medlanes.';
$lang['PAYMENT_NEW4'] = 'Your expert is waiting for your question!';
$lang['PAYMENT_NEW5'] = 'Allison Watters, MD';
$lang['PAYMENT_NEW6'] = 'Board Certified Pediatrician<br> 1,903 People Helped';
$lang['PAYMENT_DOC_IMG'] = '<img src="images/payment/doc-an.jpg" width="180" height="180" alt="#"/>';
$lang['DEP_CC'] = 'Credit Card';
$lang['DEP_PP'] = 'PayPal';
$lang['DEP_PP_TEXT'] = 'You will be redirected to PayPal to complete your payment.';
$lang['THANKYOU_6'] = 'service@medlanes.com';
$lang['THANKYOU_7'] = '(800) 413-7290';
$lang['PRESS_0'] = 'Ask Your Online Pediatrician!';
$lang['PRESS_1'] = 'Our expert pediatricians have been extensively trained to support a wide variety of conditions online. We are here for you and your child. Our specialties include:';
$lang['PRESS_2'] = '<ul><li><h2>Rash Analysis</h2></li><li><h2>Mononucleosis</h2></li><li><h2>ADHD</h2></li><li><h2>Fevers</h2></li><li><h2>Ear Infections</h2></li><li><h2>Croup</h2></li><li><h2>and More!</h2></li></ul>';


?>