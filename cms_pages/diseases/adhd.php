<?php 	

$lang['php_echo'] = 'Text';
$lang['BXSLIDER'] = "<ul class='staticlink'>";
$lang['HEADER_1'] = 'Our support team is here for you 24/7!';
$lang['HEADER_2'] = '<b>(800) 413-7290</b> or <b>service@medlanes.com</b>';
$lang['INDEX_1'] = 'Dealing with a hyperactive child? Let us help you.';
$lang['INDEX_2'] = 'Our pediatricians are online now and ready to help you. Submit your question about ADHD and feel better today!<b> Your satisfaction is 100% guaranteed.</b>';
$lang['TESTIMONIALS_0'] = 'What Our Patients Are Saying';
$lang['TESTIMONIALS_1'] = '<b>You are not alone</b> <br>It was a difficult time for the whole family when my son was diagnosed with ADHD. We were so grateful to have our doctor from Medlanes at our side.';
$lang['TESTIMONIALS_2'] = 'Jessica Ferreira, 38<br>San Antonio, TX';
$lang['TESTIMONIALS_3'] = '<b>Great help</b> <br>Having a child suffering from Attention Deficit Hyperactivity Disorder, is not easy for a parent. But Medlanes online pediatrician has got my back!';
$lang['TESTIMONIALS_4'] = 'Esther Bruce, 46<br>Midkiff, TX';
$lang['TESTIMONIALS_5'] = '<b>Trustworthy service</b> <br>I did not know what was wrong with me and then my online pediatrician offered me an adhd test. Now, that I have been diagnosed I know how to deal with my condition.';
$lang['TESTIMONIALS_6'] = 'Zachary Zavala, 43<br>Standing Stone, PA';
$lang['TESTIMONIALS_7'] = '<b>A pleasant surprise</b> <br>I was not aware that there is an adhd online test, but when I consulted my online doctor at Medlanes, he managed to prove me wrong and help me at the same time.';
$lang['TESTIMONIALS_8'] = 'Nicole Gordon, 43<br>Buffalo, NY';
$lang['TESTIMONIALS_9'] = '<b>Best out there</b> <br>I did not know what is add disorder, but Medlanes provided me great heap of information and I feel so much better now. Thank you!';
$lang['TESTIMONIALS_10'] = 'Izaak González, 39<br>Asheville, NC';
$lang['TESTIMONIALS_11'] = '<b>I am in love</b> <br>I contacted Medlanes online pediatrician for add diagnosis test and I was provided one. Such quick and reliable service!';
$lang['TESTIMONIALS_12'] = 'Todd Gocher, 38<br>Newark, NJ';
$lang['TESTIMONIALS_13'] = '<b>I will come back</b> <br>I found an add online test with the help of Medlanes. Great service! I will make sure to use it in the future too.';
$lang['TESTIMONIALS_14'] = 'Tahlia Cruz, 46<br>Patchogue, NY';
$lang['TESTIMONIALS_15'] = '<b>Revolutionary service!</b> <br>I did not know whether I had add or adhd so I contacted Medlanes. They provided me with information and help that I was looking for.';
$lang['TESTIMONIALS_16'] = 'Jude Lehrer, 45<br>Lima, OH';
$lang['DOCTORS_0'] = 'Meet Our Experts';
$lang['DOCTORS_1'] = 'Allison Watters, MD';
$lang['DOCTORS_2'] = 'Board Certified Pediatrician<br> 1,903 People Helped';
$lang['DOCTORS_3'] = '4.81 / 5<br>Many people contact me for an add test and if you need one, contact me too! Just reach to Medlanes.';
$lang['DOCTORS_4'] = 'Libby Ryder, MD';
$lang['DOCTORS_5'] = 'Board Certified Pediatrician<br> 2,869 People Helped';
$lang['DOCTORS_6'] = '4.91 / 5<br>Being an online pediatrician has allowed me to help people that can not afford a visit to the doctor\'s office. I can help you too!';
$lang['DOCTORS_7'] = 'Jack McKay, MD';
$lang['DOCTORS_8'] = 'Board Certified Pediatrician<br> 1,480 People Helped';
$lang['DOCTORS_9'] = '4.86 / 5<br>I have been practising online pediatrics for a while now and Medlanes is the best platform for that. Contact me today if you need help.';
$lang['DOCTORS_10'] = 'Kelvin Lee, MD';
$lang['DOCTORS_11'] = 'Certified in Pediatrics<br> 1,689 People Helped';
$lang['DOCTORS_12'] = '4.78 / 5<br>Parents often do not know if their children test positive for add. I am here to provide help.';
$lang['DOCTORS_13'] = 'Jayce Timpani, MD';
$lang['DOCTORS_14'] = 'Board Certified - Pediatrics<br> 1,527 People Helped';
$lang['DOCTORS_15'] = '4.95 / 5<br>If you wish to learn more about add symptoms, contact me or other online pediatricians at Medlanes.';
$lang['DOCTORS_16'] = 'Kelly Robbins, MD';
$lang['DOCTORS_17'] = 'Board Certified Pediatrician<br> 1,829 People Helped';
$lang['DOCTORS_18'] = '4.89 / 5<br>If you are unsure whether is is add or adhd I can be the person to help you. Contact Medlanes today!';
$lang['PAYMENT_NEW4'] = 'Your expert is waiting for your question!';
$lang['PAYMENT_NEW5'] = 'Allison Watters, MD';
$lang['PAYMENT_NEW6'] = 'Board Certified Pediatrician<br> 1,903 People Helped';
$lang['PAYMENT_DOC_IMG'] = '<img src="images/payment/doc-an.jpg" width="180" height="180" alt="#"/>';
$lang['DEP_CC'] = 'Credit Card';
$lang['DEP_PP'] = 'PayPal';
$lang['DEP_PP_TEXT'] = 'You will be redirected to PayPal to complete your payment.';
$lang['THANKYOU_6'] = 'service@medlanes.com';
$lang['THANKYOU_7'] = '(800) 413-7290';
$lang['PRESS_0'] = 'Ask Your Online Pediatrician!';
$lang['PRESS_1'] = 'Our expert pediatricians have been extensively trained to support a wide variety of conditions online. We are here for you and your child. Our specialties include:';
$lang['PRESS_2'] = '<ul><li><h2>Rash Analysis</h2></li><li><h2>Mononucleosis</h2></li><li><h2>ADHD</h2></li><li><h2>Fevers</h2></li><li><h2>Ear Infections</h2></li><li><h2>Croup</h2></li><li><h2>and More!</h2></li></ul>';


?>