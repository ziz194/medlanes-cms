<?php 	

$lang['php_echo'] = 'Text';
$lang['BXSLIDER'] = "<ul class='staticlink'>";
$lang['HEADER_1'] = 'Our support team is here for you 24/7!';
$lang['HEADER_2'] = '<b>(800) 413-7290</b> or <b>service@medlanes.com</b>';
$lang['INDEX_1'] = 'Ask A Gastroenterologist Now';
$lang['INDEX_2'] = 'Our expert gastroenterologists are online now and here to help. Submit your question and feel better today!<b> Your satisfaction is 100% guaranteed.</b>';
$lang['TESTIMONIALS_0'] = 'What our Customers are Saying';
$lang['TESTIMONIALS_1'] = '<b>Thanks!</b> <br>I have had a lot of trouble with gastroenterological disorders in the past. I finally found the perfect gastroenterological disorder treatment online with Medlanes. The doctors have been great and their service is second to none!';
$lang['TESTIMONIALS_2'] = 'Francis Haro, 40<br>Salisbury';
$lang['TESTIMONIALS_3'] = '<b>Perfect Online Service</b> <br>I have always shown symptoms of gastroenterological problems, but was too embarrassed to talk to a doctor face to face . Medlanes is the best online gastroenterologist platform around and thanks to them, I have found the perfect gastroenterological support!';
$lang['TESTIMONIALS_4'] = 'Jane McKenzie, 24<br>Gwynedd';
$lang['TESTIMONIALS_5'] = '<b>Best Online Gastroenterologist</b> <br>Treatments for gastroenterologic conditions are confusing and hard to understand. Dr. Johnson has found the perfect gastroenterology condition treatment, and I am finally feeling like a normal human being again. Thank you so much!';
$lang['TESTIMONIALS_6'] = 'William Funnell, 35<br>Sevenoaks';
$lang['TESTIMONIALS_7'] = '<b>Finally Feeling Right!</b> <br>Gastroenterological disorders run in my family. I was not even aware that it was genetic prior to speaking with my gastroenterologist online through Medlanes. Thanks!';
$lang['TESTIMONIALS_8'] = 'Sofia Bush, 32<br>London';
$lang['TESTIMONIALS_9'] = '<b>Thank you, Medlanes</b> <br>I was recommended by a friend to try Medlanes who had a lot of success with her gastroenterological problem treatment online from Medlanes. I have been working with Dr. Evans ever since. I finally feel like I have beat my gastroenterological problems for good.';
$lang['TESTIMONIALS_10'] = 'Luke Thompson, 40<br>Torbay';
$lang['TESTIMONIALS_11'] = '<b>Online Gastroenterologist</b> <br>Talking with Dr. Forney has been such a blessing. I finally have the tools I need in place to support myself and regain control of my gastroenterological problems. I cannot thank Medlanes enough. What a wonderful online gastroenterologist platform.';
$lang['TESTIMONIALS_12'] = 'Fred Lomeli, 30<br>Eastleigh';
$lang['TESTIMONIALS_13'] = '<b>Ask A Gastroenterologist Online</b> <br>I will be honest, I thought it was not possible to ask a gastroenterologist question online to my doctor. After using the service a few times, I can get gastroenterological help online 24 hours a day virtually. Thank you to Medlanes!';
$lang['TESTIMONIALS_14'] = 'Isabella Gourgaud, 50<br>Erewash';
$lang['TESTIMONIALS_15'] = '<b>Gastroenterological Disorders Medication Help</b> <br>I started taking nexium a few weeks ago and was having weird side effects. I had all my medication questions answered, and realized that the side effects were completely normal. This put my mind at ease. Thank you everyone!';
$lang['TESTIMONIALS_16'] = 'Leo Duncan, 26<br>Cambridge';
$lang['DOCTORS_0'] = 'Meet our Providers';
$lang['DOCTORS_1'] = 'Emma Priestley, MD';
$lang['DOCTORS_2'] = 'Gastroenterologist <br> 2,475 People Helped';
$lang['DOCTORS_3'] = '4.98 / 5<br>I have over 15 years of experience treating gastroenterological disorders and conditions that are related to and/or cause gastroenterological problems.';
$lang['DOCTORS_4'] = 'Sara Piazza, MD';
$lang['DOCTORS_5'] = 'Gastroenterologist <br> 1,996 People Helped';
$lang['DOCTORS_6'] = '4.83 / 5<br>I have treated well over 10,000 cases of gastroenterological problems and related conditions since starting practice. I am an expert online gastroenterologist!';
$lang['DOCTORS_7'] = 'Lucas Henn, MD';
$lang['DOCTORS_8'] = 'Diplomat of Gastroenterology <br> 2,754 People Helped';
$lang['DOCTORS_9'] = '4.92 / 5<br>Gastroenterological problems do not have to control you. My patients have overcome their issues with gastroenterological disorders!';
$lang['DOCTORS_10'] = 'Anthony Oberg, MD';
$lang['DOCTORS_11'] = 'Gastroenterologist <br> 1,636 People Helped';
$lang['DOCTORS_12'] = '4.88 / 5<br>Online gastroenterology is a very new concept and I am excited to be on the ground floor of revolutionizing the treatment of gastroenterological problems online and through digital methods.';
$lang['DOCTORS_13'] = 'Jeffrey Richter, MD';
$lang['DOCTORS_14'] = 'Gastroenterologist <br> 972 People Helped';
$lang['DOCTORS_15'] = '4.93 / 5<br>I have been doing online treatment for about 3 years now, and can with with a wide range of issues involving gastroenterological issues. I am here to help!';
$lang['DOCTORS_16'] = 'Jessica Hensley, MD';
$lang['DOCTORS_17'] = 'Gastroenterologist <br> 2,441 People Helped';
$lang['DOCTORS_18'] = '4.96 / 5<br>I am so excited to be a part of a revolutionary online doctor platform like Medlanes. We are here for you 24/7!';
$lang['PAYMENT_NEW4'] = 'Your expert is waiting for your question!';
$lang['PAYMENT_NEW5'] = 'Emma Priestley, MD';
$lang['PAYMENT_NEW6'] = 'Board Certified Gastroenterologist <br> 2,475 People Helped';
$lang['PAYMENT_DOC_IMG'] = '4.98 / 5<br>I have over 15 years of experience treating gastroenterological disorders and conditions that are related to and/or cause gastroenterological problems. I have been specially trained to treat gastroenterological problems online!';
$lang['DEP_CC'] = 'Credit Card';
$lang['DEP_PP'] = 'PayPal';
$lang['DEP_PP_TEXT'] = 'You will be redirected to PayPal to complete your payment.';
$lang['THANKYOU_6'] = 'service@medlanes.com';
$lang['THANKYOU_7'] = '(800) 413-7290';
$lang['PRESS_0'] = 'Online Gastroenterology Health Support!';
$lang['PRESS_1'] = 'Our doctors have been extensively trained to treat gastroenterological health conditions online! We can also help with:';
$lang['PRESS_2'] = '<ul><li><h2>Irritable Bowel Syndrome</h2></li><li><h2>Constipation</h2></li><li><h2>Colon Polyps</h2></li><li><h2>Gastroenterological Medication Questions</h2></li><li><h2>Colitis</h2></li></ul>';



?>