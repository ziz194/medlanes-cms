<?php 	

$lang['BXSLIDER'] = "<ul class='staticlink'>";
$lang['HEADER_1'] = 'Our support team is here for you 24/7!';
$lang['HEADER_2'] = '<b>(800) 413-7290</b> or <b>service@medlanes.com</b>';
$lang['INDEX_1'] = 'Ask A Dermatologist Now';
$lang['INDEX_2'] = 'Our expert dermatologists are online now and here to help. <br>Simply submit your question to get started.<br><b>Your satisfaction is 100% guaranteed.</b>';
$lang['TESTIMONIALS_0'] = 'What our Customers are Saying';
$lang['TESTIMONIALS_1'] = '<b>Acne Be Gone!</b> <br>Thanks to this wonderful online dermatologist platform, I was able to get the tips I needed to clear up my acne issues, and get my self confidence back. Thank you, Medlanes!';
$lang['TESTIMONIALS_2'] = 'Janet Watson, 43 <br> Surrey';
$lang['TESTIMONIALS_3'] = '<b>Wonderful!</b> <br>I have struggled with rosacea most of my life. It is embarrassing, and hard for me to go out in public. I asked a dermatologist online through Medlanes, and was given a customized treatment plan that has helped so much!';
$lang['TESTIMONIALS_4'] = 'Betty Wylie, 29 <br> Leeds';
$lang['TESTIMONIALS_5'] = '<b>Eczema</b> <br>My son has struggled with eczema his entire life. Ask a dermatologist is an amazing service. Working with doctor Forney, we found out that he was allergic to the detergent we were using. We are all so grateful, thanks!';
$lang['TESTIMONIALS_6'] = 'Jack Boles, 37 <br> Cockfosters';
$lang['TESTIMONIALS_7'] = '<b>Questionable Mole</b> <br>My friend found an odd mole on my back. I could not get an appointment with a local dermatologist for over 3 months. Thanks to Medlanes, I uploaded a picture, and got the answer to my questions immediately.';
$lang['TESTIMONIALS_8'] = 'Tameka Jones, 39<br> Worcester';
$lang['TESTIMONIALS_9'] = '<b>Thanks!</b> <br>I have had problems with skin tags. I was too embarrassed to go to the doctor, but got all of my treatment options laid out for me using Medlanes. Thank you, your dermatologists are wonderful and helpful!';
$lang['TESTIMONIALS_10'] = 'Clay Teller, 59 <br> Hereford';
$lang['TESTIMONIALS_11'] = '<b>Good Service</b> <br>I spoke with a dermatologist about the ingrown hairs I kept getting. My dermatologist gave me great advice, was very quick, and the ingrown hair problem has finally stopped!';
$lang['TESTIMONIALS_12'] = 'Shane Carter, 27<br> Studley';
$lang['TESTIMONIALS_13'] = '<b>Acne Medication</b> <br>As an adult, having acne is not fun. I was taking accutane, and started having liver problems. Thanks to ask a dermatologist, I was told that a bad effect of accutane is liver problems. I had no idea.';
$lang['TESTIMONIALS_14'] = 'Christine Roseman, 51<br> Brixworth';
$lang['TESTIMONIALS_15'] = '<b>Late Bloomer</b> <br>I am a late bloomer, and having just graduated college, was having trouble finding a job due to my frequent breakouts. Thanks to Dr. Andrews, I am finally seeing improvement in my complexion.';
$lang['TESTIMONIALS_16'] = 'Kyle Holmes, 22<br> Haverhill';
$lang['DOCTORS_0'] = 'Meet our Providers';
$lang['DOCTORS_1'] = 'Agatha Andrews, MD';
$lang['DOCTORS_2'] = 'Board Certified Dermatologist <br> 2,873+ People Helped';
$lang['DOCTORS_3'] = '4.85 / 5<br><br>I have over 25 years of experience treating dermatological issues and am here to help.';
$lang['DOCTORS_4'] = 'Heather Johnson, Ph.D.';
$lang['DOCTORS_5'] = 'Board Certified Dermatologist <br> 1,611 People Helped';
$lang['DOCTORS_6'] = '4.92 / 5<br><br>I have been treating acne online for over 5 years now! I welcome your questions.';
$lang['DOCTORS_7'] = 'Thomas Sandrini, MD';
$lang['DOCTORS_8'] = 'Diplomat of Dermatology <br> 918 People Helped';
$lang['DOCTORS_9'] = '4.71 / 5<br><br>I love helping people feel comfortable in their own skin. Do not hesitate, ask today!';
$lang['DOCTORS_10'] = 'Erwin Williams, MD, MBA';
$lang['DOCTORS_11'] = 'Board Certified in Dermatology <br> 873+ People Helped';
$lang['DOCTORS_12'] = '4.8 / 5<br><br>Online dermatology is a very new concept and I am excited to work with you all!';
$lang['DOCTORS_13'] = 'Jason Evans, Ph.D., MS';
$lang['DOCTORS_14'] = 'Doctor of Immunology <br> 1,920 Couples Helped';
$lang['DOCTORS_15'] = '4.81 / 5<br><br>Many people do not realize that allergies cause many skin conditions. I hope to help you soon!';
$lang['DOCTORS_16'] = 'Wilma Forney, MD';
$lang['DOCTORS_17'] = 'Board Certified Dermatologist<br> 1,001 People Helped';
$lang['DOCTORS_18'] = '4.78 / 5<br><br>I am so excited to be a part of a revolutionary online dermatologist platform like Medlanes. We are here for you 24/7!';
$lang['PAYMENT_NEW4'] = 'Your expert is waiting for your question!';
$lang['PAYMENT_NEW5'] = 'Agatha Andrews, MD';
$lang['PAYMENT_NEW6'] = 'Board Certified Dermatologist<br> 2,873+ People Helped';
$lang['PAYMENT_DOC_IMG'] = '<img src="images/payment/doc-an.jpg" width="180" height="180" alt="#"/>';
$lang['DEP_CC'] = 'Credit Card';
$lang['DEP_PP'] = 'PayPal';
$lang['DEP_PP_TEXT'] = 'You will be redirected to PayPal to complete your payment.';
$lang['THANKYOU_6'] = 'service@medlanes.com';
$lang['THANKYOU_7'] = '(800) 413-7290';
$lang['PRESS_0'] = 'Ask A Dermatologist';
$lang['PRESS_1'] = 'Our doctors have been extensively trained to treat and advise on a wide range of skin conditions including:';
$lang['PRESS_2'] = '<ul><li>Acne</li><li>Eczema and Dermatitis</li><li>Insect Bites</li><li>Scar Treatments</li><li>Mole Analysis</li><li>Rosacea</li><li>and more!</li></ul>';



?>