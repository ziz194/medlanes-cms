<?php 	

$lang['BXSLIDER'] = "<ul class='staticlink'>";
$lang['HEADER_1'] = 'Unser Kundensupport ist für Sie da!';
$lang['HEADER_2'] = '<b>0800 / 765 43 43</b> or <b>service@medlanes.com</b>';
$lang['INDEX_1'] = 'Fragen Sie einen Urologen';
$lang['INDEX_2'] = 'Unsere Urologen beantworten Ihre Fragen rund um das Thema Urologie.<br><b>Wir garantieren 100% Kundenzufriedenheit!</b>';
$lang['TESTIMONIALS_0'] = 'Unsere Kunden über uns';
$lang['TESTIMONIALS_1'] = '<b>Urologisches Problem</b> <br>Ich hatte eine Menge Ärger mit meinem urologischen Problem. Über Medlanes habe ich eine hilfreiche Antwort online erhalten. Der Urologe war großartig und der Service ist toll!';
$lang['TESTIMONIALS_2'] = 'A. Junker, 42 <br> Walsrode';
$lang['TESTIMONIALS_3'] = '<b>Urologie anonym</b> <br>Ich leide schon länger an einer urologischen Erkrankung, aber wollte nie das Problem bei einem Arztbesuch ansprechen. Mit Medlanes habe ich schnelle anonyme Hilfe erhalten.';
$lang['TESTIMONIALS_4'] = 'I. Westphal, 31 <br> Falkensee';
$lang['TESTIMONIALS_5'] = '<b>Schnelle Hilfe bei urologischen Fragen</b> <br>Behandlungensempfehlungen bei urologischen Erkrankungen sind verwirrend und schwer zu verstehen. Dank dem Online-Urologen bin ich bestens informiert. Danke!';
$lang['TESTIMONIALS_6'] = 'B. Jaspers, 37 <br> Taunusstein';
$lang['TESTIMONIALS_7'] = '<b>Endlich fühle ich mich gut!</b> <br>Urologische Erkrankungen treten häufig bei uns in der Familie auf. Vor dem Kontakt mit dem Urologen online wusste ich nicht, dass die Gene eine Rolle spielen. Jetzt ist mir Vieles klarer.';
$lang['TESTIMONIALS_8'] = 'J. Taggert, 39 <br> Datteln';
$lang['TESTIMONIALS_9'] = '<b>Urologie einfach</b> <br>Ein Freund hat mir Medlanes empfohlen. Der Kontakt zu einem Urologen online war schnell und unkompliziert. Meine urologische Erkrankung habe ich jetzt im Griff.';
$lang['TESTIMONIALS_10'] = 'T. Eibwand, 45 <br> Potsdam';
$lang['TESTIMONIALS_11'] = '<b>Toller Onlineservice!</b> <br>Im Gespräch dem Online-Urologen war eine große Hilfe. Endlich kann ich auf meine Symptome reagieren. Was für eine wunderbare Onlineplattform rund um das Thema Urologie. Danke!';
$lang['TESTIMONIALS_12'] = 'C. Sorglos, 27<br> Hameln';
$lang['TESTIMONIALS_13'] = '<b>Einen Urologen online fragen.</b> <br>Ich will ehrlich sein. Ich dachte, einen Arzt online zu fragen, ist keine Hilfe. Jetzt nutze ich Medlanes nicht nur bei urologischen Fragen. Eine tolle Idee!';
$lang['TESTIMONIALS_14'] = 'G. Hinrichs, 51<br> Montabaur';
$lang['TESTIMONIALS_15'] = '<b>Hilfe bei Medikamentenfragen</b> <br>Es traten seltsame Nebenwirkungen auf, als ich meine Medikamente umgestellt habe. Der Online-Urologe hat mir bestätigt, dass diese Nebenwirkungen völlig normal sind und mir meine Sorgen komplett genommen.';
$lang['TESTIMONIALS_16'] = 'H. Kant, 29<br> Wuppertal';
$lang['DOCTORS_0'] = 'Unsere Ärzte';
$lang['DOCTORS_1'] = 'Dr. H. Elsner';
$lang['DOCTORS_2'] = 'Fachärztin für Urologie <br> 17 Jahre Praxiserfahrung';
$lang['DOCTORS_3'] = '4.94 / 5<br><br>Ich habe mehr als 17 Jahre Erfahrung in der Behandlung von Hautkrankheiten und bin hier, um zu helfen.';
$lang['DOCTORS_4'] = 'Dr. R. Biermann';
$lang['DOCTORS_5'] = 'Fachärztin für Urologie <br> 18 Jahre Praxiserfahrung';
$lang['DOCTORS_6'] = '4.89 / 5<br><br>Ich helfe bei urologischen Fragen online seit über 5 Jahren! Ich freue mich auf Ihre Fragen.';
$lang['DOCTORS_7'] = 'Dr. M. Mertes';
$lang['DOCTORS_8'] = 'Facharzt für Urologie <br> 16 Jahre Praxiserfahrung';
$lang['DOCTORS_9'] = '4.87 / 5<br><br>Ich stehe meinen Patienten gerne auch online bei urologischen Problemen zur Seite. Dieser Service ist sehr schnell und unkompliziert.';
$lang['DOCTORS_10'] = 'Dr. S. Kästner';
$lang['DOCTORS_11'] = 'Facharzt für Urologie <br> 14 Jahre Praxiserfahrung';
$lang['DOCTORS_12'] = '4.92 / 5<br><br>Die Online Urologie ist ein sehr neues Konzept, und ich freue mich, mit dabei zu sein!';
$lang['DOCTORS_13'] = 'Dr. A. Leber';
$lang['DOCTORS_14'] = 'Facharzt für Urologie <br> 18 Jahre Praxiserfahrung';
$lang['DOCTORS_15'] = '4.88 / 5<br><br>Viele urologische Erkrankungen sind unangenehm anzusprechen. Ich bin online für Sie da, ganz anonym!';
$lang['DOCTORS_16'] = 'Dr. F. Elbers';
$lang['DOCTORS_17'] = 'Fachärztin für Urologie <br> 17 Jahre Praxiserfahrung';
$lang['DOCTORS_18'] = '4.84 / 5<br><br>Ich freue mich, Teil einer revolutionären Onlineurologen-Plattform wie Medlanes zu sein. Wir sind für Sie da!';
$lang['PAYMENT_NEW4'] = 'Ein Facharzt wartet auf Ihre Frage';
$lang['PAYMENT_NEW5'] = 'Dr. H. Elsner';
$lang['PAYMENT_NEW6'] = 'Fachärztin für Urologie<br> 17 Jahre Praxiserfahrung';
$lang['PAYMENT_DOC_IMG'] = '<img src="images/payment/doc-an.jpg" width="180" height="180" alt="#"/>';
$lang['DEP_CC'] = 'Kreditkarte';
$lang['DEP_PP'] = 'PayPal';
$lang['DEP_PP_TEXT'] = 'Zur Bezahlung werden Sie zur PayPal Webseite weitergeleitet.';
$lang['THANKYOU_6'] = 'service@medlanes.com';
$lang['THANKYOU_7'] = '0800 / 765 43 43';
$lang['PRESS_0'] = 'Onlinehilfe bei urologischen Fragen';
$lang['PRESS_1'] = 'Unsere Urologen sind in der Onlineberatung geschult. Wir helfen bei:';
$lang['PRESS_2'] = '<ul><li><h2>Inkontinenz<h2></li><li><h2>Blasenentzündung</h2></li><li><h2>Impotenz</h2></li><li><h2>Harnwegsinfekten</h2></li></ul>';



?>