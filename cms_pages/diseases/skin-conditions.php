<?php 	

$lang['php_echo'] = 'Text';
$lang['BXSLIDER'] = "<ul class='staticlink'>";
$lang['HEADER_1'] = 'Our support team is here for you 24/7!';
$lang['HEADER_2'] = '<b>(800) 413-7290</b> or <b>service@medlanes.com</b>';
$lang['INDEX_1'] = 'No more problems with Skin Conditions.';
$lang['INDEX_2'] = 'Our pediatricians are online now and ready to help you. Submit your question about Skin Conditions and feel better today!<b> Your satisfaction is 100% guaranteed.</b>';
$lang['TESTIMONIALS_0'] = 'What Our Patients Are Saying';
$lang['TESTIMONIALS_1'] = '<b>Best answers online</b> <br>My kid has been suffering from several skin diseases over the last few years. Thanks to my online pediatrician at Medlanes I was able to understand to condition better. Now I know what to do to help my son and how I can prevent the disease from occuring.';
$lang['TESTIMONIALS_2'] = 'Tia Laver, 40<br>Philadelphia, PA';
$lang['TESTIMONIALS_3'] = '<b>Bye skin conditions</b> <br>My daughter\'s skin is very sensitive. Thanks to Medlanes I don\'t have to make an appointment every time a new skin condition develops. I can just ask all my questions to an online pediatrician and treat her right away.';
$lang['TESTIMONIALS_4'] = 'Molly Muir, 40<br>Charlotte, NC';
$lang['TESTIMONIALS_5'] = '<b>Get your answers online</b> <br>My kid came back from school with a rash on her face today. I immediately contacted an online pediatrician and was able to get her treated right away! Thank you Medlanes for your amazing service!';
$lang['TESTIMONIALS_6'] = 'Samuel Botello, 45<br>Damascus, MD';
$lang['TESTIMONIALS_7'] = '<b>Get a clear answer with Medlanes</b> <br>There are so many pictures of hives and other skin conditions on the internet. I was desperate to find one that looked like what my kids have so I could figure out a way to treat them. And then I found Medlanes! The online pediatrician was very helpful and took the time to address all my concerns.';
$lang['TESTIMONIALS_8'] = 'Alicia Crawford, 47<br>Fort Washington, PA';
$lang['TESTIMONIALS_9'] = '<b>Doctors available 24/7</b> <br>Thanks to Medlanes and their certified online pediatricians, dealing with skin disorders is so easy. I don\'t have to worry about my kids anymore! I can get all the answers I need 24/7.';
$lang['TESTIMONIALS_10'] = 'Hugo Mata, 48<br>Brentwood, NY';
$lang['TESTIMONIALS_11'] = '<b>Such an awesome service!</b> <br>My youngest daughter was complaining about an armpit rash this morning. Thanks to Medlanes this is no longer an issue! The online pediatrician took the time to help me find the best treatment for my kid!';
$lang['TESTIMONIALS_12'] = 'Mason Goncalves, 45<br>Edna, OK';
$lang['TESTIMONIALS_13'] = '<b>Thank you Medlanes</b> <br>Skin problems are unfortunately very common in our family. My kids always suffer from one skin condition or another. Thanks to the online pediatrician I know what to do to relieve their symptoms and prevent the conditions from recurring too often.';
$lang['TESTIMONIALS_14'] = 'Alexandra Plante, 42<br>Birmingham, AL';
$lang['TESTIMONIALS_15'] = '<b>Great advice from certified doctors</b> <br>It\'s not rare for my son to have a rash on his chest. Talking with an online pediatrician really helped me understand what causes this skin condition. I should now be able to relieve my kid\'s symptoms and make sure he doesn\'t get affected again.';
$lang['TESTIMONIALS_16'] = 'Angus Chavarría, 43<br>Seattle, WA';
$lang['DOCTORS_0'] = 'Meet Our Experts';
$lang['DOCTORS_1'] = 'Allison Watters, MD';
$lang['DOCTORS_2'] = 'Board Certified Pediatrician<br> 1,903 People Helped';
$lang['DOCTORS_3'] = '4.81 / 5<br>I really love my job as an online pediatrician. Thanks to Medlanes I get to help even more children with skin diseases, making sure their parents know the difference between psoriasis vs eczema.';
$lang['DOCTORS_4'] = 'Libby Ryder, MD';
$lang['DOCTORS_5'] = 'Board Certified Pediatrician<br> 2,869 People Helped';
$lang['DOCTORS_6'] = '4.91 / 5<br>I became a pediatrician 6 years ago to help children and make them feel better when they\'re sick. If you have any question regarding the health of your kids, just ask away: I\'ll always do my best to answer you!';
$lang['DOCTORS_7'] = 'Jack McKay, MD';
$lang['DOCTORS_8'] = 'Board Certified Pediatrician<br> 1,480 People Helped';
$lang['DOCTORS_9'] = '4.86 / 5<br>Working from an online platform like Medlanes is amazing. I\'m able to treat many young patients and prevent their parents from worrying too much. So if you have a question, know that I\'ll do my best to help you!';
$lang['DOCTORS_10'] = 'Kelvin Lee, MD';
$lang['DOCTORS_11'] = 'Certified in Pediatrics<br> 1,689 People Helped';
$lang['DOCTORS_12'] = '4.78 / 5<br>There are so many possible skin conditions, rosacea is just one of them. As an online pediatrician it\'s my job to make sure parents know how to differenciate the diseases so they can get the best treatment for their children.';
$lang['DOCTORS_13'] = 'Jayce Timpani, MD';
$lang['DOCTORS_14'] = 'Board Certified - Pediatrics<br> 1,527 People Helped';
$lang['DOCTORS_15'] = '4.95 / 5<br>Children are often affected by chronic skin conditions. Thanks to Medlanes, I can make you will always get the best advice on how to treat your kids. I am available 24/7.';
$lang['DOCTORS_16'] = 'Kelly Robbins, MD';
$lang['DOCTORS_17'] = 'Board Certified Pediatrician<br> 1,829 People Helped';
$lang['DOCTORS_18'] = '4.89 / 5<br>I am a certified online pediatrician and know everything about skin conditions in children. So don\'t hesitate to ask your question at Medlanes : it\'ll be my pleasure to help you!';
$lang['PAYMENT_NEW4'] = 'Your expert is waiting for your question!';
$lang['PAYMENT_NEW5'] = 'Allison Watters, MD';
$lang['PAYMENT_NEW6'] = 'Board Certified Pediatrician<br> 1,903 People Helped';
$lang['PAYMENT_DOC_IMG'] = '<img src="images/payment/doc-an.jpg" width="180" height="180" alt="#"/>';
$lang['DEP_CC'] = 'Credit Card';
$lang['DEP_PP'] = 'PayPal';
$lang['DEP_PP_TEXT'] = 'You will be redirected to PayPal to complete your payment.';
$lang['THANKYOU_6'] = 'service@medlanes.com';
$lang['THANKYOU_7'] = '(800) 413-7290';
$lang['PRESS_0'] = 'Ask Your Online Pediatrician!';
$lang['PRESS_1'] = 'Our expert pediatricians have been extensively trained to support a wide variety of conditions online. We are here for you and your child. Our specialties include:';
$lang['PRESS_2'] = '<ul><li><h2>Rash Analysis</h2></li><li><h2>Mononucleosis</h2></li><li><h2>ADHD</h2></li><li><h2>Fevers</h2></li><li><h2>Ear Infections</h2></li><li><h2>Croup</h2></li><li><h2>and More!</h2></li></ul>';


?>