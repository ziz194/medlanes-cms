<?php 	

$lang['BXSLIDER'] = "<ul class='staticlink'>";
$lang['HEADER_1'] = 'Our support team is here for you 24/7!';
$lang['HEADER_2'] = '<b>(800) 413-7290</b> or <b>service@medlanes.com</b>';
$lang['INDEX_1'] = 'Social Anxiety ends here.';
$lang['INDEX_2'] = 'Our expert psychologists and psychiatrists are online now and here to help. <br>Stop suffering from social anxiety. Submit your question and feel better today!<br><b>Your satisfaction is 100% guaranteed.</b>';
$lang['TESTIMONIALS_0'] = 'We Beat Social Anxiety';
$lang['TESTIMONIALS_1'] = '<b>Feeling Good Again</b> <br>I have always struggled with social anxiety. Thanks to Medlanes, I finally have found an effective social anxiety treatment online. I am able to leave the house again, and couldnt be happier.';
$lang['TESTIMONIALS_2'] = 'Tammy Patino, 54 <br> Kill Devil Hills, NC';
$lang['TESTIMONIALS_3'] = '<b>Social Butterfly</b> <br>Finally I have found the social anxiety support I have searched for! Dr. Andrews has excellent methods to treat social anxiety, all on a convienient online platform! ';
$lang['TESTIMONIALS_4'] = 'Sally Reuter, 22 <br> Hershey, PA';
$lang['TESTIMONIALS_5'] = '<b>Life of the Party</b> <br>I am finally thriving in social situations thanks to the social anxiety support I have gotten from Medlanes. Their online social anxiety treatment methods have wroked wonders for me. Thank you, Medlanes!';
$lang['TESTIMONIALS_6'] = 'Rupert Stevens, 34 <br> New Brunswick, NJ';
$lang['TESTIMONIALS_7'] = '<b>Finally Myself</b> <br>I was never one for public places, but I am finally living my life the way I want thanks to the social anxiety help I have gotten from Dr. Evans and the support of the entire Medlanes team.';
$lang['TESTIMONIALS_8'] = 'Eva Clemmons, 25 <br> Savannah, GA';
$lang['TESTIMONIALS_9'] = '<b>On Track!</b> <br>I have always confined myself in social situations, but am learning to be myself thanks to the excellent social anxiety treatment options I have worked out with my online psychiatrist. I am finally doing what I love, and not worrying.';
$lang['TESTIMONIALS_10'] = 'Chaz Fenster, 52 <br> Mobile, AL';
$lang['TESTIMONIALS_11'] = '<b>Thank You Dr. A</b> <br>Doctor Andrews has given me social anxiety treatment options that without Medlanes, would not have access to. This is a very inexpensive way to get the best online psychological help possible!';
$lang['TESTIMONIALS_12'] = 'Charlie Hamman, 29 <br>Watkins Glen, VA';
$lang['TESTIMONIALS_13'] = '<b>Social Anxiety Support</b> <br>Newly diagnosed, I have had so many questions about social anxiety and it has been difficult to figure out. Now, I have all of my social anxiety questions answered from the comfort of my own home.';
$lang['TESTIMONIALS_14'] = 'Raylen Gray, 49<br> San Fransisco, CA';
$lang['TESTIMONIALS_15'] = '<b>Online Psychiatrist</b> <br>The medications I had received for my social anxiety treatments were causing me lots of side effects. Thanks to the support of Medlanes, I have switched my medication and have a great social anxiety support network!';
$lang['TESTIMONIALS_16'] = 'James Gates, 27<br> Bridgeport, ME';
$lang['DOCTORS_0'] = 'Meet our Providers';
$lang['DOCTORS_1'] = 'Agatha Andrews, MD';
$lang['DOCTORS_2'] = 'Board Certified Psychiatrist <br> 2,873+ People Helped';
$lang['DOCTORS_3'] = '4.85 / 5<br><br>I have over 15 years of experience treating social anxiety, and specialize in the treatment of social anxiety online!';
$lang['DOCTORS_4'] = 'Heather Johnson, Ph.D.';
$lang['DOCTORS_5'] = 'Board Certified Psychotherapist <br> 1,611 People Helped';
$lang['DOCTORS_6'] = '4.92 / 5<br><br>I have treated well over 10,000 cases of social anxiety, and am looking forward to providing you the support you need!';
$lang['DOCTORS_7'] = 'Thomas Sandrini, MD';
$lang['DOCTORS_8'] = 'Diplomat of Psychiatry <br> 918 People Helped';
$lang['DOCTORS_9'] = '4.71 / 5<br><br>Overcome social anxiety. We are here to help, and have the social anxiety support you are searching for.';
$lang['DOCTORS_10'] = 'Erwin Williams, MD, MBA';
$lang['DOCTORS_11'] = 'Board Certified Psychiatrist <br> 873+ People Helped';
$lang['DOCTORS_12'] = '4.8 / 5<br><br>Online psychiatry and online psychology are both very new concepts and I am excited to be on the ground floor of revolutionizing the treatment of social anxiety online!';
$lang['DOCTORS_13'] = 'Jason Evans, Ph.D., MS';
$lang['DOCTORS_14'] = 'Certified Couples Therapist <br> 1,920 Couples Helped';
$lang['DOCTORS_15'] = '4.81 / 5<br><br>I have been doing online therapy for about 3 years now, and can with with a wide range of issues involving treating social anxiety.';
$lang['DOCTORS_16'] = 'Wilma Forney, MD';
$lang['DOCTORS_17'] = 'Board Certified Psychiatrist <br> 1,001 People Helped';
$lang['DOCTORS_18'] = '4.78 / 5<br><br>I am so excited to be a part of a revolutionary online doctor platform like Medlanes. We are here for you 24/7!';
$lang['PAYMENT_NEW4'] = 'Your expert is waiting for your question!';
$lang['PAYMENT_NEW5'] = 'Agatha Andrews, MD';
$lang['PAYMENT_NEW6'] = 'Board Certified Psychiatrist <br> 2,873+ People Helped';
$lang['PAYMENT_DOC_IMG'] = '<img src="images/payment/doc-an.jpg" width="180" height="180" alt="#"/>';
$lang['DEP_CC'] = 'Credit Card';
$lang['DEP_PP'] = 'PayPal';
$lang['DEP_PP_TEXT'] = 'You will be redirected to PayPal to complete your payment.';
$lang['THANKYOU_6'] = 'service@medlanes.com';
$lang['THANKYOU_7'] = '(800) 413-7290';
$lang['PRESS_0'] = 'Online Mental Health Support!';
$lang['PRESS_1'] = 'Our doctors have been extensively trained to treat mental health conditions online! We can also help with:';
$lang['PRESS_2'] = '<ul><li>Depression Support</li><li>Fears and Phobias</li><li>PTSD</li><li>Psychiatric Medication Questions</li><li>Anxiety</li></ul>';



?>