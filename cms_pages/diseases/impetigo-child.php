<?php 	

$lang['php_echo'] = 'Text';
$lang['BXSLIDER'] = "<ul class='staticlink'>";
$lang['HEADER_1'] = 'Our support team is here for you 24/7!';
$lang['HEADER_2'] = '<b>(800) 413-7290</b> or <b>service@medlanes.com</b>';
$lang['INDEX_1'] = 'We help you manage Impetigo';
$lang['INDEX_2'] = 'Our pediatricians are online now and ready to help you. Submit your question about Impetigo and feel better today!<b> Your satisfaction is 100% guaranteed.</b>';
$lang['TESTIMONIALS_0'] = 'What Our Patients Are Saying';
$lang['TESTIMONIALS_1'] = '<b>Thank you Medlanes!</b> <br>My baby has been suffering from impetigo and it made her very restless. My online doctor at Medlanes managed to help us though and impetigo is no longer a problem.';
$lang['TESTIMONIALS_2'] = 'Bianca Gray, 48<br>Monterey, TN';
$lang['TESTIMONIALS_3'] = '<b>Red sores disappeared!</b> <br>Impetigo signs and symptoms are so hard to deal with. I felt lost but Medlanes provided me with the right action course. Thank you!';
$lang['TESTIMONIALS_4'] = 'Katie Brown, 40<br>Los Angeles, CA';
$lang['TESTIMONIALS_5'] = '<b>My little boys</b> <br>Impetigo in children is so common. I have 3 boys and each has suffered from impetigo. I would not have managed without the help of Medlanes.';
$lang['TESTIMONIALS_6'] = 'Robert Beneventi, 42<br>Akron, OH';
$lang['TESTIMONIALS_7'] = '<b>A quick cure!</b> <br>My boy got sick and I did not know what the red sores could be. An online doctor at Medlanes gave me impetigo description and from then on I knew what to do.';
$lang['TESTIMONIALS_8'] = 'Chelsea Davidson, 45<br>Fort Collins, CO';
$lang['TESTIMONIALS_9'] = '<b>Quick treatment</b> <br>If not for Medlanes, I would not know how to treat impetigo. Thank you for such brilliant service!';
$lang['TESTIMONIALS_10'] = 'Sol Noble, 38<br>Bridgeview, IL';
$lang['TESTIMONIALS_11'] = '<b>Best information online</b> <br>I did not know how long is impetigo contagious, so I turned to Medlanes for help. Not only my doctor gave me useful information, he also gave me a treatment plan. Thank you.';
$lang['TESTIMONIALS_12'] = 'Riley Fruehauf, 49<br>San Diego, CA';
$lang['TESTIMONIALS_13'] = '<b>Best service I ever had</b> <br>I have been battling impetigo for the longest time, because I did not know that it could be treated with impetigo antibiotics. Thankfully, my online doctor at Medalnes informed me and now my family can be impetigo free!';
$lang['TESTIMONIALS_14'] = 'Claudia Walker, 42<br>Dallas, TX';
$lang['TESTIMONIALS_15'] = '<b>Medlanes is the best</b> <br>My baby suffered from a serious impetigo infection. If not for Medlanes, that nightmare would have never ended!';
$lang['TESTIMONIALS_16'] = 'Thomas Kitson, 49<br>Hickory Hills, IL';
$lang['DOCTORS_0'] = 'Meet Our Experts';
$lang['DOCTORS_1'] = 'Allison Watters, MD';
$lang['DOCTORS_2'] = 'Board Certified Pediatrician<br> 1,903 People Helped';
$lang['DOCTORS_3'] = '4.81 / 5<br>Impetigo is a serious condition and people should be aware of impetigo causes. As an online doctor it is very easy for me to help you out, so please do not hesitate to contact Medlanes.';
$lang['DOCTORS_4'] = 'Libby Ryder, MD';
$lang['DOCTORS_5'] = 'Board Certified Pediatrician<br> 2,869 People Helped';
$lang['DOCTORS_6'] = '4.91 / 5<br>Being a pediatrician I deal wih many health cases. Being an online doctor allows me to reach out to more people and it is Medlanes that we should be grateful for.';
$lang['DOCTORS_7'] = 'Jack McKay, MD';
$lang['DOCTORS_8'] = 'Board Certified Pediatrician<br> 1,480 People Helped';
$lang['DOCTORS_9'] = '4.86 / 5<br>I have been practising pediatrics for many years now but it has never been as easy as it is with telehealth. Contact me today for a consultation!';
$lang['DOCTORS_10'] = 'Kelvin Lee, MD';
$lang['DOCTORS_11'] = 'Certified in Pediatrics<br> 1,689 People Helped';
$lang['DOCTORS_12'] = '4.78 / 5<br>If you do not know how to treat impetigo, have a consulatation with me at Medlanes. I have treated many impetigo cases and I can help you too!';
$lang['DOCTORS_13'] = 'Jayce Timpani, MD';
$lang['DOCTORS_14'] = 'Board Certified - Pediatrics<br> 1,527 People Helped';
$lang['DOCTORS_15'] = '4.95 / 5<br>Symptoms of impetigo may cause a lot of discomfort. Contact Medlanes and I or other skilled pediatricians will let you know what to do!';
$lang['DOCTORS_16'] = 'Kelly Robbins, MD';
$lang['DOCTORS_17'] = 'Board Certified Pediatrician<br> 1,829 People Helped';
$lang['DOCTORS_18'] = '4.89 / 5<br>Impetigo rash is hard to deal with. That is why I am here to help you with that. As a pediatrician I have dealt with many impetigo cases and know what to do.';
$lang['PAYMENT_NEW4'] = 'Your expert is waiting for your question!';
$lang['PAYMENT_NEW5'] = 'Allison Watters, MD';
$lang['PAYMENT_NEW6'] = 'Board Certified Pediatrician<br> 1,903 People Helped';
$lang['PAYMENT_DOC_IMG'] = '<img src="images/payment/doc-an.jpg" width="180" height="180" alt="#"/>';
$lang['DEP_CC'] = 'Credit Card';
$lang['DEP_PP'] = 'PayPal';
$lang['DEP_PP_TEXT'] = 'You will be redirected to PayPal to complete your payment.';
$lang['THANKYOU_6'] = 'service@medlanes.com';
$lang['THANKYOU_7'] = '(800) 413-7290';
$lang['PRESS_0'] = 'Ask Your Online Pediatrician!';
$lang['PRESS_1'] = 'Our expert pediatricians have been extensively trained to support a wide variety of conditions online. We are here for you and your child. Our specialties include:';
$lang['PRESS_2'] = '<ul><li><h2>Rash Analysis</h2></li><li><h2>Mononucleosis</h2></li><li><h2>ADHD</h2></li><li><h2>Fevers</h2></li><li><h2>Ear Infections</h2></li><li><h2>Croup</h2></li><li><h2>and More!</h2></li></ul>';


?>