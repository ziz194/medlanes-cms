<nav class="top-bar" data-topbar role="navigation">
  <ul class="title-area">
    <li class="name">
      <h1><a href="http://www.medlanes.com"><img src='<?php echo $url ?>cms/cms_pages/img/logo.png'></a></h1>
    </li>
     <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
    <li class="toggle-topbar menu-icon"><a href="#"><span></span></a></li>
  </ul>

  <section class="top-bar-section">
    <!-- Right Nav Section -->
    <ul class="right">
       <li><a href="http://www.medlanes.com/our-service.php">OUR SERVICE</a></li>
     <li><a href="http://www.medlanes.com/doctors">DOCTORS</a></li>
     <li><a href="http://www.medlanes.com/faq">FAQ</a></li>
     <li><a href="http://www.medlanes.com/support.php">SUPPORT</a></li>
    </ul>


  </section>
</nav>