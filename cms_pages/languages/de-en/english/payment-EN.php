<?php 

$lang['PAYMENT_'] = "Payment";

$lang['PAYMENT_HEADING'] = "Choose urgency and level of detail";
$lang['PAYMENT_SUBHEADING'] = "Choose how fast you need the information and how detailed it should be.";


// CHOOSE URGENCY PANEL
$lang['PAYMENT_CHOOSEPANEL'] = "Urgency";

$lang['PAYMENT_CHOOSEPANEL1'] = "Level of Detail";
$lang['PAYMENT_CHOOSEPANEL2'] = "Low";
$lang['PAYMENT_CHOOSEPANEL3'] = "Medium";
$lang['PAYMENT_CHOOSEPANEL4'] = "High";
$lang['PAYMENT_CHOOSEPANEL5'] = "Amount you will pay IF satisfied with the answer: 
";


// DEPOSIT FEE

$lang['PAYMENT_DEPOSIT1'] = "Deposit doctor’s fee (100% satisfaction garantee)";
$lang['PAYMENT_DEPOSIT2'] = "and get your answer in real-time now.";
$lang['PAYMENT_DEPOSIT22'] = "Place your deposit of";
$lang['PAYMENT_DEPOSIT3'] = "Deposit by Paypal";
$lang['PAYMENT_DEPOSIT4'] = "Your information is secure.";
$lang['PAYMENT_DEPOSIT5'] = "By clicking “Continue” you indicate that you agree to the terms of service and are 18 years old or older.";
$lang['PAYMENT_DEPOSIT6'] = "Continue to final step.";


// SIDEBAR 

$lang['PAYMENT_SIDEHEAD1'] = "Doctor Network and Expert Advisers";
$lang['PAYMENT_SIDEHEAD2'] = "One of our highly qualified doctors will reply shortly.";


$lang['PAYMENT_SIDE_DOC1HEAD1'] = "Dr. med. Kathrin Hamann";
$lang['PAYMENT_SIDE_DOC1HEAD2'] = "Family Medicine Specialist";
$lang['PAYMENT_SIDE_DOC1TEXT3'] = "<p>- 12 years practice experience</p>";

$lang['PAYMENT_SIDE_DOC2HEAD1'] = "Dr. med. Wolf Siepen";
$lang['PAYMENT_SIDE_DOC2HEAD2'] = "Orthopedics and Trauma Specialist";
$lang['PAYMENT_SIDE_DOC2TEXT3'] = "<p>- 12 years practice experience</p>";

$lang['PAYMENT_SIDE_DOC3HEAD1'] = "Dr. med. Norbert Scheufele";
$lang['PAYMENT_SIDE_DOC3HEAD2'] = "Gynaecology and Obstetrics Specialist";
$lang['PAYMENT_SIDE_DOC3TEXT3'] = "<p>- 20 years practice experience</p>";
$lang['pricess'] = '€';


$lang['PAYMENT_AGREEMENT1'] = 'I’ve read and agree to the';
$lang['PAYMENT_AGREEMENT2'] = '<a href="privacy.php?lang=de" target="_blank" >privacy statement</a> and the';
$lang['PAYMENT_AGREEMENT3'] = '<a href="terms.php?lang=de" target="_blank" >terms conditions</a>';
$lang['PAYMENT_AGREEMENT4'] = 'and I am 18 years or older.';


$lang['PAYMENT_NEW1'] = "Confirm Your Appointment";
$lang['PAYMENT_NEW2'] = "Adjust the sliders below to specify how detailed, and how quickly you need your question answered. Remember, your satisfaction is 100% guaranteed.";
$lang['PAYMENT_NEW3'] = 'Price you will pay if you\'re satisfied with the answer: € ';
$lang['PAYMENT_NEW4'] = "A specialist is ready for you";
$lang['PAYMENT_NEW5'] = "Dr. med Anna Bergmann";
$lang['PAYMENT_NEW6'] = "Board Certified Physician <br> 11 years of experience";
$lang['PAYMENT_NEW7'] = "VERIFIED EXPERT";
$lang['PAYMENT_NEW8'] = "Rating";
$lang['PAYMENT_NEW9'] = "4.9 / 5 Stars";
$lang['PAYMENT_NEW10'] = "<b>Money-Back Guarantee</b><br>Your satisfaction is 100% guaranteed! You only pay if you are fully satisfied with our service. ";
$lang['PAYMENT_DOC_IMG'] = '<img src="images/payment/doc.png" alt="#"/>';

$lang['3step_1'] = "Describe your symptoms";
$lang['3step_2'] = "Pay Consultation fee";
$lang['3step_3'] = "Get Qualified advice";

$lang['DEP_SELECT'] = 'Select';
$lang['DEP_METHOD'] = 'Deposit Method';
$lang['DEP_CC'] = 'Credit Card';
$lang['DEP_PP'] = 'PayPal';
$lang['DEP_PP_TEXT'] = 'You will be taken to PayPal to complete your payment.';
$lang['DEP_CC_NR'] = 'Card Number';
$lang['DEP_CC_EXP'] = 'Expiration Date';
$lang['DEP_CC_MONTH'] = '<span id="month">Month</span>
						<ul class="dropdown">
							<li><a href="#">01 - Jan</a></li>
							<li><a href="#">02 - Feb</a></li>
							<li><a href="#">03 - Mar</a></li>
							<li><a href="#">04 - Apr</a></li>
							<li><a href="#">05 - May</a></li>
							<li><a href="#">06 - Jun</a></li>
							<li><a href="#">07 - Jul</a></li>
							<li><a href="#">08 - Aug</a></li>
							<li><a href="#">09 - Sep</a></li>
							<li><a href="#">10 - Oct</a></li>
							<li><a href="#">11 - Nov</a></li>
							<li><a href="#">12 - Dec</a></li>							
						</ul>';
$lang['DEP_CC_YEAR'] = '<span id="year">Year</span>
						<ul class="dropdown">
							<li><a href="#">2015</a></li>							
							<li><a href="#">2016</a></li>							
							<li><a href="#">2017</a></li>							
							<li><a href="#">2018</a></li>							
							<li><a href="#">2019</a></li>							
							<li><a href="#">2020</a></li>							
							<li><a href="#">2021</a></li>							
							<li><a href="#">2022</a></li>							
							<li><a href="#">2023</a></li>							
							<li><a href="#">2024</a></li>							
						</ul>';
$lang['DEP_CC_CVC'] = 'Security Code';
$lang['DEP_SECURE'] = 'Your information is secure.';
$lang['DEP_PRIVACY'] = 'Privacy & Security';

$lang['PAY_CONTINUE'] = 'Continue';
$lang['PAYMENT_DOC_IMG'] = '<img src="images/payment/doc-an.jpg" width="180" height="180" alt="#"/>';


?>