<?php 

$lang['ADDQUESTION_'] = "";


$lang['EMAIL_HEAD'] = "Your eMail address - Your answer will be sent here when it is ready.";
$lang['emailsign'] = "Incorrect email address";
$lang['emailsign2'] = "Your eMail address is already in use";

$lang['ADDQUESTION_1'] = "Submit your questions here. A doctor is waiting to answer you!";
$lang['ADDQUESTION_2'] = "Select affected body part(s)";
$lang['ADDQUESTION_3'] = "Pinpoint where the problem is. You can select multiple locations.";
$lang['ADDQUESTION_4'] = "Front";
$lang['ADDQUESTION_5'] = "Back";
$lang['ADDQUESTION_6'] = "Add your Symptoms";
$lang['ADDQUESTION_7'] = "Please list your symptoms (e.g. Headache, Stomach pain, Skin rash, ...)";
$lang['ADDQUESTION_8'] = "Attach and Relevant Files";
$lang['ADDQUESTION_9'] = "E.g. images, test results, ...";
$lang['ADDQUESTION_10'] = "Additional information";
$lang['ADDQUESTION_11'] = "When did the symptoms start?";
$lang['ADDQUESTION_12'] = "Did you experience a trauma or injury?";
$lang['ADDQUESTION_13'] = "Please list all medication and supplements you are currently taking";
$lang['ADDQUESTION_14'] = "Your Question";
$lang['ADDQUESTION_15'] = "Continue to Final Step.";

$lang['ADDQUESTION_yes'] = "Yes";
$lang['ADDQUESTION_no'] = "No";
$lang['ADDQUESTION_add'] = "Add";
$lang['ADDQUESTION_upload'] = "Upload file";


$lang['aq-bp-33'] = 'Right Upper Back';
$lang['aq-bp-44'] = 'Left Upper Back';

$lang['aq-bp-66'] = 'Right Lower Back';
$lang['aq-bp-77'] = 'Left Lower Back';

$lang['aq-bp-1'] = 'Head';
$lang['aq-bp-2'] = 'Right Arm';
$lang['aq-bp-3'] = 'Right Chest';
$lang['aq-bp-4'] = 'Left Chest';
$lang['aq-bp-5'] = 'Left Arm';
$lang['aq-bp-6'] = 'Right Stomach';
$lang['aq-bp-7'] = 'Left Stomach';
$lang['aq-bp-8'] = 'Right Hand';
$lang['aq-bp-9'] = 'Pubic Area';
$lang['aq-bp-10'] = 'Left Hand';
$lang['aq-bp-11'] = 'Right Thigh';
$lang['aq-bp-12'] = 'Left Thigh';
$lang['aq-bp-13'] = 'Right Lower Leg';
$lang['aq-bp-14'] = 'Left Lower Leg';
$lang['aq-bp-15'] = 'Right Foot';
$lang['aq-bp-16'] = 'Left Foot';
$lang['aq-coupon'] = 'Please enter your <b>coupon code</b> here';


$lang['aq-1'] = 'Get instant answers to medical questions';
$lang['aq-2'] = 'Pinpoint the location of your problem. You can select multiple locations.';
$lang['aq-3'] = 'Front';
$lang['aq-4'] = 'Back';
$lang['aq-5'] = 'Your Symptoms';
$lang['aq-6'] = 'Attach any Relevant Pictures';
$lang['aq-7'] = 'Additional Information';
$lang['aq-8'] = 'When did your symptoms start?';
$lang['aq-9'] = 'Select';
$lang['aq-10'] = 'Acute';
$lang['aq-11'] = 'Today';
$lang['aq-12'] = 'This Week';
$lang['aq-13'] = 'This Month';
$lang['aq-chronic'] = 'Chronic';
$lang['aq-yq'] = 'Your Question';
$lang['aq-experience'] = 'Did you experience any trauma or injury?';
$lang['aq-14'] = 'Yes';
$lang['aq-15'] = 'No';
$lang['aq-16'] = 'Please list all medications and supplements you are currently taking';
$lang['aq-17'] = 'Anything noteworthy in you medical history? (e.g. high blood pressure, COPD, Asthma, etc)';
$lang['aq-18'] = 'Additional Files';
$lang['aq-19'] = 'Attach Files';
$lang['aq-20'] = 'Your question will be answered by the perfect doctor within the next few hours. We will keep you posted.';
$lang['aq-21'] = 'Confirm';
$lang['aq-add'] = 'Add';



$lang['ADDQUESTION_LPANEL'] = 'Perfect! You already have an account. Please log in.';
$lang['ADDQUESTION_LPANEL1'] = 'You have received your credentials by email with your first question';
$lang['ADDQUESTION_LPANEL2'] = 'Password';
$lang['ADDQUESTION_LPANEL4'] = 'Add';
?>
