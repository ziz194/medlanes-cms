<?php 

$lang['CAREER_'] = "";

// HEADING 

$lang['CAREER_HEADING'] = "We revolutionize Medicine";


// SUB MENU

$lang['CAREER_SUBMENU_LINK1'] = "Visiton & Values";
$lang['CAREER_SUBMENU_LINK2'] = "Meet the Team";
$lang['CAREER_SUBMENU_LINK3'] = "Open positions";
$lang['CAREER_SUBMENU_LINK4'] = "Pics & Location";


// SUBHEADING CAREER

$lang['CAREER_SUBHEADING1'] = "We are Medlanes &amp; you can be too!";
$lang['CAREER_SUBHEADING2'] = "Here at Medlanes we, want to revolutionize healthcare by providing accessible, affordable, and reliable medical advice online. We want to give everyone instant access to medicine, anywhere, anytime by combining the convenience of the web and mobile world with the quality of a certified doctor’s visit.";
$lang['CAREER_SUBHEADING3'] = "To accomplish this ground-breaking plan, our international team works hand in hand. We all have different backgrounds, but we bear the same goal: bringing a change to the healthcare world! We are committed to learning and to success.Each day represents a new, exciting challenge for us, with diverse projects and a wide range of responsibilities!";
$lang['CAREER_SUBHEADING4'] = "If you also want to be part of our small, fast-paced, and collaborative team then join us in Berlin! We are taking off, and we are looking for new excited Medlaners! Check out our open position and apply now. We're looking forward to getting to know you.";


// VISION

$lang['CAREER_VISION_1'] = "Our vision is to bring medicine online and these are our core values";

$lang['CAREER_VISION_2'] = "Impact!";
$lang['CAREER_VISION_3'] = "We go for Impact!";
$lang['CAREER_VISION_4'] = "We want to revolutionize";

$lang['CAREER_VISION_5'] = "Results";
$lang['CAREER_VISION_6'] = "We focus on results in";
$lang['CAREER_VISION_7'] = "everything we do";

$lang['CAREER_VISION_8'] = "Collaboration";
$lang['CAREER_VISION_9'] = "We are a team and";
$lang['CAREER_VISION_10'] = "We work as a team";

$lang['CAREER_VISION_11'] = "Feedback & Improvement";
$lang['CAREER_VISION_12'] = "We value honest feedback and";
$lang['CAREER_VISION_13'] = "improvement everywhere";

// PERKS

$lang['CAREER_PERKS'] = "The Perks";

$lang['CAREER_PERKS_TEXT1'] = "Each Monday we kick off the week with a delicious breakfast and some stories from the weekend!";

$lang['CAREER_PERKS_TEXT2'] = "M for...multicultural! Medlanes is an international team in the multicultural neighborhood of Berlin Kreuzberg.";

$lang['CAREER_PERKS_TEXT3'] = " Wanna be our MVP? Each month Medlanes recognizes the best performers with a special prize!";

$lang['CAREER_PERKS_TEXT4'] = "Friday Funday! We finish every week with a cold beer and some fun games! The last Friday of the month is cultural themed night, including themed food, games, and music!";

$lang['CAREER_PERKS_TEXT5'] = "Medlanes makes sure each member of the team has the chance to grow with the company through constant challenges and prompt feedback!";

$lang['CAREER_PERKS_TEXT6'] = "First time in Berlin? No worries! There is a welcome package waiting for you with all relevant information…and some nice surprises!";

$lang['CAREER_PERKS_TEXT7'] = "We don’t lose the chance to celebrate: birthdays never go unseen! And we're always up for celebrating holidays in fun ways...Halloween costume contest, Secret Santa anyone? ";

$lang['CAREER_PERKS_TEXT8'] = "A constructive evaluation sheet from the founders will help you understand your strengths, weaknesses, achievements and improvements.";


// EMPLOYE STORIES
$lang['CAREER_EMPLOYE'] = "Employe Stories";
// EMPLOYE 1
$lang['CAREER_EMPLOYE_TEXT1'] = "“Working at a startup like Medlanes gives me the freedom to try new things and explore my own potential. I have the opportunity to develop exciting projects while learning with a team of international and skilled individuals.“ ";
$lang['CAREER_EMPLOYE_NAME1'] = "STEPHEN, MARKETING";
// EMPLOYE 2
$lang['CAREER_EMPLOYE_TEXT2'] = "“Doing HR at a startup is very exciting! I have the responsibility of finding the great talent to build the best possible team. I enjoy meeting diverse people and working with such a multicultural team. “ ";
$lang['CAREER_EMPLOYE_NAME2'] = "ALYSSA, HR";
// EMPLOYE 3
$lang['CAREER_EMPLOYE_TEXT3'] = "“I have enjoyed every day at Medlanes so far, every day is different and challenging. The hardworking and motivated environment pushes me to do my best but we also know how to have fun. “ ";
$lang['CAREER_EMPLOYE_NAME3'] = "JAKUB, TECH DEVELOPMENT";



// MEDLANES IMAGES PART HEADING

$lang['CAREER_PICTURES_HEADING'] = "Work at the heart of Berlin's startup culture";
?>
