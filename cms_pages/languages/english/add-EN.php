<?php 


$lang['aq-1'] = 'Get instant answers to medical questions';
$lang['aq-2'] = 'Pinpoint the location of your problem. You can select multiple locations.';
$lang['aq-3'] = 'Front';
$lang['aq-4'] = 'Back';
$lang['aq-5'] = 'Your Symptoms';
$lang['aq-6'] = 'Attach any Relevant Pictures';
$lang['aq-7'] = 'Additional Information';
$lang['aq-8'] = 'When did your symptoms start?';
$lang['aq-9'] = 'Select';
$lang['aq-10'] = 'Acute';
$lang['aq-11'] = 'Today';
$lang['aq-12'] = 'This Week';
$lang['aq-13'] = 'This Month';
$lang['aq-chronic'] = 'Chronic';
$lang['aq-yq'] = 'Your Question';
$lang['aq-experience'] = 'Did you experience any trauma or injury?';
$lang['aq-14'] = 'Yes';
$lang['aq-15'] = 'No';
$lang['aq-16'] = 'Please list all medications and supplements you are currently taking';
$lang['aq-17'] = 'Anything noteworthy in you medical history? (e.g. high blood pressure, COPD, Asthma, etc)';
$lang['aq-18'] = 'Additional Files';
$lang['aq-19'] = 'Attach Files';
$lang['aq-20'] = 'Your question will be answered by the perfect doctor within the next few hours. We will keep you posted.';
$lang['aq-21'] = 'Confirm';
$lang['aq-add'] = 'Add';

// HEADING

$lang['aq-bp-1'] = 'Head';
$lang['aq-bp-2'] = 'Right Arm';
$lang['aq-bp-3'] = 'Right Chest';
$lang['aq-bp-4'] = 'Left Chest';
$lang['aq-bp-5'] = 'Left Arm';
$lang['aq-bp-6'] = 'Right Stomach';
$lang['aq-bp-7'] = 'Left Stomach';
$lang['aq-bp-8'] = 'Right Hand';
$lang['aq-bp-9'] = 'Public Area';
$lang['aq-bp-10'] = 'Left Hand';
$lang['aq-bp-11'] = 'Right Thigh';
$lang['aq-bp-12'] = 'Left Thigh';
$lang['aq-bp-13'] = 'Right Lower Leg';
$lang['aq-bp-14'] = 'Left Lower Leg';
$lang['aq-bp-15'] = 'Right Foot';
$lang['aq-bp-16'] = 'Left Foot';

?>

