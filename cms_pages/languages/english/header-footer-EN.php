<?php 

// PAGE TITLES

$lang['TITLE_INDEX'] = 'Medlanes | Qualified medical information - Expert options - second opinions';
$lang['TITLE_PAYMENT'] = 'Medlanes | Qualified medical information - Expert options - second opinions';
$lang['TITLE_SERVICE'] = 'Medlanes | Qualified medical information - Expert options - second opinions';
$lang['TITLE_TERMS'] = 'Medlanes | Qualified medical information - Expert options - second opinions';
$lang['TITLE_PRIVACY'] = 'Medlanes | Qualified medical information - Expert options - second opinions';
$lang['TITLE_ABOUT'] = 'Medlanes | Qualified medical information - Expert options - second opinions';
$lang['TITLE_CONTACT'] = 'Medlanes | Qualified medical information - Expert options - second opinions';
$lang['TITLE_CAREER'] = 'Medlanes | Qualified medical information - Expert options - second opinions';
$lang['TITLE_ADD_QUESTION'] = 'Medlanes | Qualified medical information - Expert options - second opinions';
$lang['TITLE_THANKYOU'] = 'Medlanes | Qualified medical information - Expert options - second opinions';
$lang['TITLE_DOCTOR'] = 'Medlanes | Qualified medical information - Expert options - second opinions';
$lang['TITLE_FAQ'] = 'Medlanes | Qualified medical information - Expert options - second opinions';

// 3 step process

$lang['3step_1'] = 'Describe your symptoms';
$lang['3step_2'] = 'Pay consultation fee';
$lang['3step_3'] = 'Get qualified advice';

// HEADER MENU LINKS

$lang['link1'] = '<a href="service?lang=en">Our Service</a>';
$lang['link2'] = '<a href="doctors?lang=en">Doctors</a>';
$lang['link3'] = '<a href="faq?lang=en">FAQ</a>';
$lang['link4'] = '<a href="contact?lang=en">Contact</a>';
$lang['LOGOLINK'] = 'index.php?lang=en';
// HEADER MENU LINKS END HERE


/*FOOTER MENU LINKS START*/

// 1st menu
$lang['FOOTER_link1'] = '<a href="index.php?lang=en">Home</a>';
$lang['FOOTER_link2'] = '<a href="service?lang=en">Our Service</a>';
$lang['FOOTER_link3'] = '<a href="doctors?lang=en">Doctors</a>';
$lang['FOOTER_link4'] = '<a href="faq?lang=en">FAQ</a>';
$lang['FOOTER_link5'] = '<a href="contact?lang=en">Contact</a>';
// 2nd menu
$lang['FOOTER_link6'] = '<a href="about?lang=en">About Medlanes</a>';
$lang['FOOTER_link7'] = '<a href="career?lang=en">Careers</a>';
// 3rd menu
$lang['FOOTER_link8'] = '<a href="terms?lang=en">Terms of Service</a>';
$lang['FOOTER_link9'] = '<a href="privacy?lang=en">Privacy Statement</a>';
$lang['FOOTER_link10'] = '<a href="imprint?lang=en">Imprint</a>';

$lang['FOOTER_SUPPORTEDBY'] = '<p>Supported by:</p>';

$lang['FOOTER_COPYRIGHTS'] = '<p class="copyrights">Copyright © Medlanes GmbH 2014 All Rights Reserved</p>';

/*FOOTER MENU LINKS END*/
?>