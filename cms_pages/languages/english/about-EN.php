<?php 

$lang['ABOUT_'] = "";

// HEADING
$lang['ABOUT_HEADING'] = "<h1><b>We bring medicine online!</b>And make healthcare <br> affordable for everyone.</h1>";

// THREE POINTS

$lang['ABOUT_SUBHEADING1'] = "The Company";
$lang['ABOUT_SUBTEXT1'] = "Medlanes GmbH is a company based in Berlin, Germany that specializes in telemedicine, and virtual doctor consultations. Since our inception in 2013, we have revolutionized the way you see your doctor! We have been awarded with several major grants for our innovations in healthcare from such names as Microsoft and Bayer Pharmaceuticals. Our doctors are available 24/7, and always fully dedicated to the wellness of our patients. No more trips to the doctor, no more hassle of waiting weeks to see a physician for a simple problem. Our doctors are always on call for you, and we complete most of our consultations within 1 hour.";

$lang['ABOUT_SUBHEADING2'] = "Our Vision";
$lang['ABOUT_SUBTEXT2'] = "Medlanes was founded for one basic reason. To provide the highest quality of healthcare to anyone in need, without the common restrictions of seeing a doctor. We give everyone in the world instant access to medicine, anywhere, anytime, and with a price that is affordable. We firmly believe that you shouldn't be denied the best quality of care because of insurance issues, financial hardships, geographical limitations or simply due to time restrictions. Medlanes will revolutionize healthcare, and we aim to change the world! We truly provide healthcare without borders.";

$lang['ABOUT_SUBHEADING3'] = "Our Solution";
$lang['ABOUT_SUBTEXT3'] = "We are proud to introduce the Medlanes App! With the Medlanes App, it is now possible to carry your doctor with you! We are available wherever you are, whenever you need us. We provide a fast, easy, and extremely affordable option to get medical advice from your doctor around the clock!";
?>