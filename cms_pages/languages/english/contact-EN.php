<?php 

$lang['CONTACT_'] = "Contact";

// CONTACT MAIN HEADING
$lang['CONTACT_HEADING'] = "Got questions? <br> Contact us today, we are happy to help! ";

// CONTACT EXLANATION

$lang['CONTACT_EXPLAIN1'] = "Medlanes Support is at your service";
$lang['CONTACT_EXPLAIN2'] = "Taking care of our patients is our highest priority. We are available around the clock and are more than happy to help you with any question that you might have. Please get in contact with us, we'll get back to you shortly.";

$lang['CONTACT_EXPLAIN2'] = "Phone and eMail-Support";
$lang['CONTACT_EXPLAIN2'] = "Over 90% of requests are answered in less that 24h.";


// PHONE BOX
$lang['CONTACT_PHONE1'] = "Phone";
$lang['CONTACT_PHONE2'] = "0800 - 765 43 43";
$lang['CONTACT_PHONE3'] = "Tollfree - available 09:00 - 21:00 Monday - Friday";
$lang['CONTACT_PHONE4'] = "Request a call back";
$lang['CONTACT_PHONE5'] = "Email";
// EMAIL BOX
$lang['CONTACT_EMAIL1'] = "Email Support";
$lang['CONTACT_EMAIL2'] = "service@medlanes.com";
$lang['CONTACT_EMAIL3'] = "Direct answer from 09:00 - 21:00";

// CONTACT FORM
$lang['CONTACT_FORM_HEADING1'] = "Contact Form";
$lang['CONTACT_FORM_HEADING2'] = "Send us a secured message using the form below.";

$lang['CONTACT_FORM_NAME'] = "First Name";
$lang['CONTACT_FORM_LASTNAME'] = "Last Name";
$lang['CONTACT_FORM_EMAIL'] = "Email";
$lang['CONTACT_FORM_MESSAGE'] = "Message";
$lang['CONTACT_FORM_SEND'] = "Send";

?>