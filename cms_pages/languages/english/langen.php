<?php 





$lang['HEADER_ALLERGIE'] = "Allergies";
$lang['HEADER_SLEEP_DISORDER'] = "Sleep Health";
$lang['HEADER_COLD'] = "Cold, Cough and Flu";
$lang['HEADER_DIABETES'] = "Diabetes";

$lang['HEADER_EYE'] = "Eye Health";
$lang['HEADER_EAR'] = "Ear Pain";
$lang['HEADER_MENTAL'] = "Mental Health";
$lang['HEADER_SEXUAL'] = "Sexual Health";
$lang['HEADER_DIET'] = "Diet and Weight Loss";
$lang['HEADER_DIGESTIVE'] = "Digestive Health";

$lang['HEADER_INFECTION'] = "Infections";
$lang['HEADER_PAIN'] = "Pain";
$lang['HEADER_ARITHIS'] = "Arthritis" ;
$lang['HEADER_COPD'] = "Asthma and COPD" ;
$lang['HEADER_CHILDREN'] = "Children's Health" ;
$lang['HEADER_ENDOCRINE'] = "Endocrine System Health" ;
$lang['HEADER_INJURIES'] = "Injuries" ;
$lang['HEADER_MEN'] = "Men's Health" ;
$lang['HEADER_ORAL'] = "Oral Health" ;
$lang['HEADER_RESPIRATORY'] = "Respiratory Health" ;
$lang['HEADER_STD'] = "Sexually Transmitted Diseases" ;
$lang['HEADER_WOMEN'] = "Women's Health" ;

$lang['HEADER_SKIN'] = "Hair, Skin and Nails" ;

//Condition TEmplate 
$lang['FEVER_CONTENT1'] ="Description";
$lang['FEVER_CONTENT2'] ="Signs &amp; Symptoms";
$lang['FEVER_CONTENT3'] ="Causes";
$lang['FEVER_CONTENT4'] ="Diagnosis ";
$lang['FEVER_CONTENT5'] ="Treatments";
$lang['FEVER_CONTENT6'] ="Complications";



//Tests Template 

$lang['TEST_CONTENT1'] ="Description" ;
$lang['TEST_CONTENT2'] ="Testing Purposes " ;
$lang['TEST_CONTENT3'] ="Normal Ranges";
$lang['TEST_CONTENT4'] ="What Results Indicate";


//Medication_name Template 
$lang['MED_NAME_CONTENT1'] ="Overview" ;
$lang['MED_NAME_CONTENT2'] ="Important information " ;




//Medication_type Template 


$lang['MED_TYPE_CONTENT1'] ="Overview" ;
$lang['MED_TYPE_CONTENT2'] ="Examples" ;
$lang['MED_TYPE_CONTENT3'] ="Uses";




//Portals
$lang['TOP'] ="Top Searched Conditions";
$lang['OVERVIEW'] ="Overview";
$lang['CONDITIONS'] ="Conditions A-Z";



//side bar 



$lang['SEXUAL_SIDEBAR_BUTTON'] ="Ask a doctor now ";




$lang[ 'ASK'] = 'Ask a doctor' ;
$lang['DSBOX1_DOCTOR_img'] = '<img src="./images/docs/Doctor1.png" id="doc_img" alt="Sidebar doctor image"/>';
$lang['DSBOX1_DOCTOR_txt'] = 'Dr. Thomas Penning';
$lang['DSBOX1_SP_img'] = '<img src="./images/family_medicine.png" id="specialty_img" width="12px" height="12px">';
$lang['DSBOX1_SP_txt'] = 'General Medecine';
$lang['DSBOX1_EXP_h'] = 'Experience';
$lang['DSBOX1_EXP_txt'] = '23 years of practice';
$lang['DSBOX1_RATING_h'] = 'Rating';
$lang['DSBOX1_RATING_txt'] = '4.9 (147 Ratings)';



$lang['DOC_HEADER'] ="Doctor Network and Expert Advisers";
$lang['DOC_TEXT'] ="In partnership with our expert doctors, Medlanes has brought together the best and brightest medical practitioners from every specialty to better serve you!";




$lang['DOC1_NAME'] ="Dr. med. Jessica Männel";
$lang['DOC1_SPEC'] ="Family Medicine Specialist";


$lang['DOC2_NAME'] ="Dr. med. Wolf Siepen";
$lang['DOC2_SPEC'] ="Orthopedics and Trauma Specialist";

$lang['DOC3_NAME'] ="Dr. med. Kathrin Hamann";
$lang['DOC3_SPEC'] ="Family Medicine Specialist";

$lang['DOC4_NAME'] ="Beate Broermann";
$lang['DOC4_SPEC'] ="Psychologist";

$lang['DOC5_NAME'] ="Christian Welsch";
$lang['DOC5_SPEC'] ="ENT Specialist";

$lang['DOC6_NAME'] ="Stefan Paffrath";
$lang['DOC6_SPEC'] ="Psychologist";


$lang['DOC7_NAME'] ="Dr. med. Ive Schaaf";
$lang['DOC7_SPEC'] ="Phlebology Specialist";




$lang['DOC8_NAME'] ="Dr. med. Norbert Scheufele";
$lang['DOC8_SPEC'] ="Gynaecology and Obstetrics Specialist";


$lang['DOC9_NAME'] ="Dr.rer.nat.Uta-Maria Weigel";
$lang['DOC9_SPEC'] ="Naturopathic Specialist";


$lang['DOC10_NAME'] ="Dr. med. Joachim Nowak";
$lang['DOC10_SPEC'] =" Orthopedic Specialist";

 ?>