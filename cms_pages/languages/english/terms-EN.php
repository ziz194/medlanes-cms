<?php 

$lang['FULL_TERMS'] = 'Terms and Conditions';

// TERMS HEADING

$lang['TERMS_HEADING1'] = 'Terms and Conditions';
$lang['TERMS_HEADING2'] = '<b>WE EXCLUSIVELY PROVIDE GENERAL MEDICAL INFORMATION. IN NO WAY CAN THE SERVICE BE USED FOR DIAGNOSIS, TREATMENT OR ANY INDIVIDUAL DECISION. THIS SERVICE CAN ONLY BE USED IN GERMANY AND IN GERMAN.</b>';

// TERMS PARAG


$lang['TERMS_1'] ='
				<p><b>§ 1 Overview</b></p>		
				<p>The use of the services offered by Medlanes GmbH is governed by the following terms and conditions. Differences of the agreement must be submitted writing. Medlanes GmbH, registered in the commercial register of the local court of Charlottenburg (Berlin, Germany) under registration number HRB159023B is represented by its managing directors Dr. Emil Kendziorra and Erik Stoffregen. Inquires can be directed to:</p>	
				<div class="two" style="width:100%;float:left;margin-top:20px;margin-bottom:20px;">
				
					<p>Via Mail: Oranienstr. 185, 10999 Berlin</p>
					<p>Telephone: +49 (0)30 577 004 20</p>
					<p>Fax: +49 (0)6151 62749 799</p>
					<p>Telefax: +49 (0)6151 62749 799 </p>
					<p>E-Mail: info(at)medlanes.com</p>
					<p>According to the §27 sales tax law, our registration number is: DE295684112</p></div>
';
$lang['TERMS_2'] ='
				<p><b>§ 2 Scope</b></p>
				<p> Medlanes is a mobile and online health platform that gives users the possibility to obtain medical information via the internet. The user is provided with the opportunity to submit via the platform their symptoms, medications, medical history disclosures, photos or general medical questions. Upon submission, the user will receive one of the following responses:</p>
				<p>a. An automatically generated system email stating that we are unable to address the specific problem</p>
				<p>b. An answer to their inquiry from a licensed physician.</p>
				<p>2. Features of the service provided by Medlanes that go beyond the website (www.medlanes.com) are not specified as medical services. The use of Medlanes cannot replace a traditional doctors visit. In any cases that require immediate assistance (e.g. medical emergency, suicidal feelings) must be treated as an emergency, and we are unable to assist in these cases. Special professional legal provisions and restrictions apply to medical care on the internet. The serves are therefore limited to the current legal options. Special instructions for the specification and performance are compulsory.</p>
';
$lang['TERMS_3'] ='
				<p><b>§ 3 Registration</b></p>
				<p>1. The use of Medlanes requires the establishment of a specific user account for which registration is required. The registration requires that the user shall provide all requested information in the registration template. At the time of registration, the user must be 18 years of age or older.</p>
				<p>2. When registration is completed, the user will receive an automated email containing a confirmation link sent to the email address that was specified during the registration process. This link must be clicked to activate the users account. If the account is not activated within two weeks of the registration process, it will be discarded and the user information will be deleted. Upon activation of the user-specific account, a legal agreement is established in accordance with the Terms and Conditions.
</p>
				<p>3. There is no legal entitlement to the benefits and services provided by Medlanes. Medlanes has the right to refuse registration with providing explanation.</p>
';
$lang['TERMS_4'] ='
				<p><b>§ 4 Conditions</b></p>
				<p>Withdrawal:</p>
				<p>You have the right to cancel service within fourteen (14) days without specifying a reason. The revocation period is fourteen (14) days from the date of the contract. To exercise this option, you must inform Medlanes GmbH, represented by Dr. Emil Kendziorra - Managing Director</p>
				
				<div class="two" style="width:100%;float:left;margin-top:20px;margin-bottom:20px;">
					<p>Oranienstr. 185, 10999 Berlin</p>
					<p>Telephone: +49 (0)30 577 004 20</p>
					<p>Fax: +49 (0)6151 62749 799</p>
					<p>E-Mail: info(at)medlanes.com</p>
				</div>
				<p>by means of a clear and concise statement (e.g. consigned by post mail, fax or email) of your decision to terminate this contract. To meet the cancellation deadline, it is sufficient that you send your communication concerning the exercise of the right of withdrawal before the withdrawal deadline.</p>
			
				<p>Effects of withdrawal:</p>
				<p>If you withdraw from this contract, Medlanes will provide you full repayment immediately and no longer than fourteen (14) days from the date on which the notice is received through your cancellation of this agreement. For the repayment method, we will refund the direct source of the original transaction, unless you expressly state otherwise; in this case, you will be charged fees for such repayment.</p>
				<p>If services have been rendered during the withdrawal period, you will be responsible for the complete costs of specified service up until the date on which notification is received of your right of cancellation in the respect of this contract. Service rendered compared to the total amount provided for in the contract.</p>
				<p>-      End of cancellation information</p>
';
$lang['TERMS_5'] ='
				<p><b>§ 5 Information Obligation</b></p>
				<p>1. Out of court complaint and redress procedures do not apply to Medlanes GmbH.
</p>
				<p>2. Further information about Medlanes, including our offerings and process can be seen exclusively via the illustrations on www.medlanes.com.</p>
				<p>3. All technical steps inclusive of this contract can be observed via section §3 of this Terms and Conditions agreement.</p>
				<p>4. The user is permitted to save the text of this Terms and Conditions agreement by selecting the “Save As” function of their browser. The user additionally is permitted to print the text of this Terms and Conditions agreement via the print function of their browser. Medlanes houses all Terms and Conditions texts, and are available to the user upon request submitted via email, or postal mail.
</p>
				<p>5. Any input by the user may be modified at any time by selecting the “Back” button in their browser, and then making any according corrections. Termination of the browser session will act as cancellation of the registration process. This can be done at any time.</p>
				<p>6. The conclusion of this contract is solely available in the German language.</p>
				<p>7. We have subjected us any specific Code of Conduct (policy).</p>
';
$lang['TERMS_6'] ='
				<p><b>§ 6 Rights of Use</b></p>
				<p>1. The user is solely awarded the rights granted under these Terms and Conditions.
</p>
				<p>2. The information published on the online and web app platform of Medlanes inclusive of all content, information, pictures, video and/or databases are subject to copyright and owned and/or licensed to Medlanes GmbH.</p>
				<p>3. The content available via Medlanes’ online and/or web app may be used or reproduced for personal and non-commercial purposes. Any use of property without expressed written consent from Medlanes GmbH is prohibited.</p>
';
$lang['TERMS_7'] ='
				<p><b>§ 7 User Data</b></p>
				<p>1. Medlanes is entitled to collect and use information submitted by the user in accordance with this agreement. Without expressed consent from the user, Medlanes GmbH is permitted to collect and process all user data. The data collected will be processed and used to any extent necessary for the execution of the contract and in accordance to service fulfillment.
</p>
				<p>2. At any given time, the user subject to the availability of the online platform has the ability to retrieve the data stored in his/her account and either modify, or delete said information.</p>
				<p>3. Using the “Privacy Statement” button via www.medlanes.com will link to full refrence in regards to the privacy policy.</p>
';
$lang['TERMS_8'] ='
				<p><b>§ 8 Availability</b></p>
				<p>Medlanes provided service is available 24 hours per day except in times of the following instances:
</p>
				<p>During backup operations</p>
				<p>During times of system maintenance</p>
				<p>or in times of program work to the system and/or database</p>
				<p>Medlanes will minimize any potential instances of disruption.</p>
';
$lang['TERMS_9'] ='
				<p><b> § 9 Liability</b></p>
				<p>1. Medlanes is not to be held liable for damages due to delay, non-performance, subpar performance or any wrongful act or an infringement of a right (other than under contract) leading to civil legal liability.
</p>
				<p>2. Medlanes is only liable for civil damages, but cannot be held liable for indirect damages including consequential damages, unforeseeable damages, atypical damages or loss of profits. Additionally, this applies to labor disputes, accidental damage and force majeure.
</p>
				<p>3. The foregoing limitation of liability shall apply to all contractual and non-contractual claims.
</p>
';
$lang['TERMS_10'] ='
				<p><b>§ 10 Obligations of the User</b></p>
				<p>1. The user is permitted to use the service provided by Medlanes GmbH properly. It is the responsibility of the user to record their personal information including username and password. Additionally, the user is responsible to ensure that personal information remains confidential.The user is responsible to notify Medlanes GmbH if there is a misuse, or a suspicion of misuse of this information.</p>
				<p>2. The user is obligated to provide all necessary information to use this serve. The information that is provided must be full, and truthful.</p>
';
$lang['TERMS_11'] ='
				<p><b>§ 11 Blocking Access / Termination</b></p>
				<p>1. Medlanes GmbH reserves the right to pursue litigation in the event of improper use or fundamental breach. Subsequently, Medlanes reserves the right to block a user’s access at a reasonable suspicion. If the suspicion is overcome, the block will be removed. It is at discretion of Medlanes GmbH.

</p>
				<p>2. Each party to this Agreement has the right to terminate if just cause is provided. The notice of termination must be submitted in writing (e.g. email). In an event of termination, the user must provide an effective date of termination, in which the user’s access to the Medlanes online platform is revoked.</p>
';
$lang['TERMS_12'] ='
				<p><b>§ 12 Modifications</b></p>
				<p>1. Medlanes has the right at any time to modify the terms and conditions towards users with effect for the future.</p>
				<p>2. If any modification is necessary to the Terms and Conditions, Medlanes is obligated to submit the updates via email to the last used email address of the user. The modifications take effect in fourteen (14) days after the email is originally sent if the user does not provide express rejection of the specified modification. </p>
				<p>3. If the user denies the modification made to the Terms and Agreement, Medlanes GmbH reserves the right to terminate the contract extraordinarily, and without notice or consent. If the contract continues, the previously agreed upon terms and conditions are still valid.
</p>
';
$lang['TERMS_13'] = '
				<p><b>§ 13 Payment</b></p>
				<p>Medlanes supports the following payment options. Certain options might not be available in all countries: Credit card, Debit card, SEPA, Paypal, Sofort, GiroPay, in select countries wire transfer.</p>
				<p>Payments are processed by PayLane sp. z o.o. which is located in Gdańsk at ul. Arkońska 6/A3, zip code: 80-387, company number: 0000227278.</p>
				<p>If you choose a subscription for Medlanes, periodic withdrawals are made to your selected payment method / account. By choosing a subscription based payment you agree to periodic withdrawals. If a free testing period is offered and chosen, payments might begin automatically after the testing period. This will be clearly stated on the page where the testing period is selected. You can inform Medlanes GmbH at any time, if you no longer wish to use the service. Please write an eMail to service(at)medlanes.com to do so. </p>
';


$lang['TERMS_14'] = '
				<p><b>§ 14 Conclusion</b></p>
				<p>If any provision of these terms and conditions, including this provision be wholly or partially invalid, the validity of the remaining provisions shall remain unaffected. Instead of the ineffective or missing conditions the respective legal regulations shall be valid.</p>
';

?>