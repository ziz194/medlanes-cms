<?php 

$lang['FAQ_'] = "";

// FAQ HEADING

$lang['FAQ_HEADING'] = "Frequently Asked Questions";

// FAQ SUBS
$lang['FAQ_SUB1'] = "Frequently Asked Questions";
$lang['FAQ_SUB2'] = "We strive to provide the best customer service possible. Here are a few of the most commonly asked questions. For everything else please contact us directly.";

// FAQ TABS

// Medical FAQs

$lang['FAQ_TABS_HEADING1'] = "Medical";

$lang['FAQ_TABS_TEXT1_1'] = "<p><b>1) What types of questions are you able to answer?</b></p>
						<p>We are able to answer any type of question as long as it is not in the case of a medical emergency. In the case of emergency, please seek interventional care immediately by contacting 911 right away!</p>";
						
$lang['FAQ_TABS_TEXT1_2'] = "<p><b>2) Is my doctor seeing patients online?</b></p>
						<p>The amount of doctors seeing patients online increases daily. If your doctor does not yet see patients online give us their contact details and we will send them information about how to join our online practice. Feel free to ask them if they would be interested to offer an online service for their patients.</p>";
						
$lang['FAQ_TABS_TEXT1_3'] = "<p><b>3) Which cases is it best to use Medlanes?</b></p>
						<p>Let us give you a few examples.</p>
						<p>- I don’t feel well and would like to see a doctor, but I'm at work and don't have time </p>
						<p>- On vacation and the local doctors don't speak my language</p>
						<p>- I recently moved and don’t know any doctors in my area</p>
						<p>- I would like to get a second opinion</p>
						<p>- I got a rather simple question and don’t want to wait for hours in the waiting room of my doctor</p>
						<p>- I have an embarrassing issue that I would prefer asking anonymously </p>
						<p>- It is Sunday and my child has a fever. I need convenient advice without spending hours in the Emergency Room </p>";
						
$lang['FAQ_TABS_TEXT1_4'] = "<p><b>1) Who are the doctors?</b></p>
						<p>Medlanes works with a group of board-certified medical practitioners from every speciality in order to provide the best service for the patients.</p>";					
						
$lang['FAQ_TABS_TEXT1_5'] = "<p><b>1) What is mHealth?</b></p>
						<p>mHealth is a simple and efficient way to gain medical advice either online or by mobile app. A person submits their personal information and medical question through Medlanes/' easy to use website then chooses their personal board certified doctor. The doctor then looks at the individual case and sends their professional medical advice back to the patient within the time period specified at checkout. Why wait to see the doctor ever again?</p>";			
						
$lang['FAQ_TABS_TEXT1_6'] = "<p><b>1) Can I make an appointment? How quickly will I be seen?</b></p>
						<p>With Medlanes, there is no need to make appointments in advance. Instead, Medlanes services can be used 24/7 from any part of the world. Any time, any place! The doctor you have selected will evaluate all of the data you have provided, and respond to your question as soon as possible. </p>";		
						
// Security FAQs				

$lang['FAQ_TABS_HEADING2'] = "Security";

$lang['FAQ_TABS_TEXT2_1'] = "<p><b>1) Is Medlanes a secure service?</b></p>
			        	<p>Yes! Protecting your privacy is the number one priority of every employee here at Medlanes. We use millitary grade 256 bit SSL security, and maintain healthcare information storage standards. </p>";
						
$lang['FAQ_TABS_TEXT2_2'] = "<p><b>2) What are my information used for?</b></p>
			        	<p>Your personal information is only used to provide advice from a doctor. Your data is not used for anything else.</p>";

$lang['FAQ_TABS_HEADING3'] = "Payment";
$lang['FAQ_TABS_TEXT3_1'] = "<p><b>1) How much does the service cost? Is it covered by insurance?</b></p>
			        	<p>You can download and use the Medlanes App for free. The cost of a doctor consultation is $29.Whether or not the service is covered by your healthcare insurance depends on your individual plan.</p>";
$lang['FAQ_TABS_TEXT3_2'] = "<p><b>2) What if I’m unhappy with my doctor or the service?</b></p>
			        	<p>Your feedback is very important to us! If you’re not totally satisfied, then please contact our customer support team at support@medlanes.com. We're more than happy to help you.</p>";

$lang['FAQ_TABS_HEADING4'] = "General";
$lang['FAQ_TABS_TEXT4_1'] = "<p><b>1) What makes Medlanes unique?</b></p>
			        	<p>Medlanes brings medicine online. Medlanes is available 24/7 and on all devices.</p>";
$lang['FAQ_TABS_TEXT4_2'] = "<p><b>2) Can I contact a doctor from abroad?</b></p>
			        	<p>Of course, Medlanes is available from anywhere in the world! Just remember that it might be cheaper to use the local WiFi connection compared the the mobile network.</p>";



?>