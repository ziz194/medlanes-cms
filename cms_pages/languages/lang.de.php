<?php
/* 
------------------
Language: German
------------------
*/


$lang = array();


// ADD QUESTION

include 'german/index-DE.php';
include 'german/payment-DE.php';
include 'german/paymentemail-DE.php';
include 'german/signupemail-DE.php';
include 'german/header-footer-DE.php';
include 'german/thankyou-DE.php';

include 'german/privacy-DE.php';
include 'german/terms-DE.php';
include 'german/imprint-DE.php';
include 'german/contact-DE.php';
include 'german/about-DE.php';
include 'german/doctors-DE.php';
include 'german/faq-DE.php';
include 'german/service-DE.php';
include 'german/addquestion-DE.php';
include 'german/career-DE.php';
include 'german/langde.php';
?>


