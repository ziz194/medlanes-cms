<?php 

$lang['THANKYOU_1'] = "Thank you for your question. What happens next?";
$lang['THANKYOU_2'] = "You are finished. You will receive the answer directly by email.";
$lang['THANKYOU_3'] = "If we are unable to pair you with a doctor, we will refund the cost of the consultation immediately.";
$lang['THANKYOU_4'] = "If you have any questions...";
$lang['THANKYOU_5'] = "Our customer support is always there for you! Feel free to contact us any time.";
$lang['THANKYOU_6'] = 'eMail: <b style="padding-left:10px">support@medlanes.com</b>';
$lang['THANKYOU_7'] = 'Tel.<b style="padding-left:28px">+49 30 577 004 20</b>';
$lang['THANKYOU_8'] = "";
$lang['THANKYOU_9'] = "";

?>