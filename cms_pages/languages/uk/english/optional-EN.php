<?php

$lang['OPT_HEADING'] = 'Optional Information';
$lang['OPT_SUBHEAD'] = 'Your doctor wants to answer you better. By providing additional information your answer will be more detailed and faster.';
$lang['OPT_SKIP'] = 'Skip this Step';
$lang['OPT_SKIP2'] = 'Skip';
$lang['OPT_NAME'] = 'Name:';
$lang['OPT_GENDER'] = 'Choose your gender:';
$lang['OPT_SELECT'] = 'Select';
$lang['OPT_MALE'] = 'Male';
$lang['OPT_FEMALE'] = 'Female';
$lang['OPT_AGE'] = 'Age';
$lang['OPT_PHOTOS'] = 'Please attach relevant photos';
$lang['OPT_FILES'] = 'Add Files';
$lang['OPT_MED_HISTORY'] = 'Relevant medical history (e.g. Allergies, High blood pressure, COPD, Asthma etc.)';
$lang['OPT_MEDICATION'] = 'Please list frequently taken medications:';
$lang['OPT_SYMPTOMS'] = 'Since when do the symptoms occur?';
$lang['OPT_SYMPTOMS_DD'] = '<span id="sincewhen">Select</span>
					<ul class="dropdown">
						<li><a href="#">Acute</a></li>
						<li><a href="#">Today</a></li>
						<li><a href="#">This Week</a></li>
						<li><a href="#">This Month</a></li>
						<li><a href="#">Chronic</a></li>
					</ul>';
$lang['OPT_CONTINUE'] = 'Continue';



$lang['PAYMENT_NEW1'] = "Select your options";
$lang['PAYMENT_NEW2'] = "One of our experts is ready to answer your questions. We provide <b>100% satisfaction guarantee</b>.";
$lang['PAYMENT_NEW3'] = 'Experts fee for <b id="detailsswap">highly detailed</b> and <b id="urgencyswap">fast</b> information:';
$lang['PAYMENT_NEW4'] = "A specialist is ready for you";
$lang['PAYMENT_NEW5'] = "Dr. med. J. Männel";
$lang['PAYMENT_NEW6'] = "General Physician 12 years of practice";
$lang['PAYMENT_NEW7'] = "VERIFIED EXPERT";
$lang['PAYMENT_NEW8'] = "Rating";
$lang['PAYMENT_NEW9'] = "<b>9,7 of 10</b> (Sanego)";
$lang['PAYMENT_NEW10'] = "<b>Money-back guarantee</b><br>Your satisfaction is 100% guaranteed, you only pay if you are fully satisfied. ";




?>