<?php 

$lang['PRIVACY_TITLE'] = 'Medlanes - Privacy Policy';

// PRIVACY HEADING

$lang['PRIVACY_HEADING1'] = '<h1>Privacy Policy</h1>';


// PRIVACY PARAG

$lang['PRIVACY_1'] = '
				<p><b>§ 1. Collection of Personal Data </b></p>
				<p>In order to provide the agreed upon services to the pursuant of  § 2 Term and Conditions e</p>
					<p class="center">Medlanes GmbH,  represented by its Managing Director Dr. Emil Kendziorra and  Erik Stoffregen, </p>
					<p class="center">Oranienstr. 185, 10999 Berlin</p>
					<p class="center">Telephone: +49 (0)30 577 004 20</p>
					<p class="center">Fax: +49 (0)6151 62749 799 </p>
					<p class="center">Email: info(at)medlanes.com </p>
					<p style="margin-top:20px">– Hereinafter ”Medlanes” –</p>
					<p>collects the data that is entered by the user, including the IP address of the terminal used (hereinafter “Personal Data”).</p>
';
$lang['PRIVACY_2'] = '
				<p><b>§ 2 Purpose of collection, processing and use </b></p>
				<p>(1)	The personal data that is used and collected by Medlanes solely for the purpose of providing the agreed upon service to the user within the conditions of § 2 Terms and Conditions.</p>
				<p>(2)	The personal data collected will be processed and stored on secured servers provided by Medlanes in Germany.</p>
				<p>(3)	Medlanes permits the user to keep access to personal information via the use of cookies that will be stored on the users computed, which facilitates the registration process. The user may refuse the use of cookies in their computer by changing the settings of their respective internet browser to either remove cookies, or disallow them completely. However, Medlanes advises that in this case an automatic registration and re-login without the access to the cookie file is not possible.</p>
';
$lang['PRIVACY_3'] = '
				<p><b>§ 3 Protection of Privacy</b></p>
				<p>Medlanes is fully committed to protecting the privacy of users and insures to collect data in accordance with the Federal Data Protection Act and the Telemedia Act, to process and use the information for the fulfillment of purposes defined in § 2 Terms and Conditions. Medlanes staff are wholly obligated to the protection of your data.</p>
';
$lang['PRIVACY_4'] = '
				<p><b>§ 4 Rights of the User</b></p>
				<p>The user in accordance to their Federal Privacy Act rights is entitled access to their personal data including the ability to rectify or delete specified data. Within the scope of these rights, the user can contact Medlanes directly to the administrator specified in  § 6 person mentioned exercise.</p>
';
$lang['PRIVACY_5'] = '
				<p><b>§ 5 Consent</b></p>
				<p>If the user agrees to the terms of this declaration, the user also agrees with the controlled use of personal data. The consent to opt in, via clicking or not clicking the corresponding field after the reference to these provisions. The approval granted or refused consent is recorded; and its content is available to the user at any time via (<a href="terms.php">Terms of Service</a>). At any time, the user has the right to revoke their consent and thereby dissolve their account, including the deletion of all personal data.</p>
';
$lang['PRIVACY_6'] = '
				<p><b>§ 6 Payment </b></p>
				<p>Transaction data, including personal data, can be transferred to PayLane Sp. z o.o. located in Gdańsk at Arkońska 6/A3, zip code: 80-387, company number: 0000227278, in order to process payments.</p>
';
$lang['PRIVACY_7'] = '
				<p><b>§ 7 Contact </b></p>
				<p>Contact for all privacy related questions and requests for this exercise of the rights described in § 4 is Erik Stoffregen</p>
';

?>