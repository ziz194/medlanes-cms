<?php 

$lang['CONTACT_'] = "Contact";

// CONTACT MAIN HEADING
$lang['CONTACT_HEADING'] = "Got questions? <br> Contact us today, we are happy to help! ";

// CONTACT EXLANATION

$lang['CONTACT_EXPLAIN1'] = "Our team is at your service";
$lang['CONTACT_EXPLAIN2'] = "At Medlanes Support is of the highest importance. We are available around the clock and are more than happy to help you with any question that you might have. Please get in contact with us, we'll get back to you shortly.";

$lang['CONTACT_EXPLAIN2'] = "Phone and eMail-Support";
$lang['CONTACT_EXPLAIN2'] = "Over 90% of requests are answered in less that 24h.";


// PHONE BOX
$lang['CONTACT_PHONE1'] = "Phone";
$lang['CONTACT_PHONE2'] = "0800 - 765 43 43";
$lang['CONTACT_PHONE3'] = "tolfree - available 24/7";
$lang['CONTACT_PHONE4'] = "Request a call back";
$lang['CONTACT_PHONE5'] = "Email";
// EMAIL BOX
$lang['CONTACT_EMAIL1'] = "Email support";
$lang['CONTACT_EMAIL2'] = "info@medlanes.com";
$lang['CONTACT_EMAIL3'] = "Direct answer from 8AM - 6PM";

// CONTACT FORM
$lang['CONTACT_FORM_HEADING1'] = "Contact form";
$lang['CONTACT_FORM_HEADING2'] = "You can also send us a mesage using the form below.";

$lang['CONTACT_FORM_NAME'] = "First Name";
$lang['CONTACT_FORM_LASTNAME'] = "Last Name";
$lang['CONTACT_FORM_EMAIL'] = "Email";
$lang['CONTACT_FORM_MESSAGE'] = "Message";
$lang['CONTACT_FORM_SEND'] = "Send";

?>