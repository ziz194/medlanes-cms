<?php
/* 
------------------
Language: English
------------------
*/

$page = basename($_SERVER['PHP_SELF']); 



$lang = array();

include 'english/index-EN.php';
include 'english/optional-EN.php';
include 'english/payment-EN.php';
include 'english/thankyou-EN.php';

include 'english/privacy-EN.php';
include 'english/terms-EN.php';
include 'english/contact-EN.php';

?>
