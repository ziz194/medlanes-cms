<?php 

$lang['CONTACT_'] = "Kontant";

// CONTACT MAIN HEADING
$lang['CONTACT_HEADING'] = "Maszy pytanie? <br> Skontaktuj się z nami, chętnie Ci pomożemy";

// CONTACT EXLANATION

$lang['CONTACT_EXPLAIN1'] = "Nasz zespół jest do twojej dyspozycji";
$lang['CONTACT_EXPLAIN2'] = "W Medlanes obsługa klienta jest naszym najwyższym priorytetem jesteśmy do dyspozycji całą dobę i chętnie ci pomożemy. Skontaktuj się z nami byśmy mogli ci pomóc";

$lang['CONTACT_EXPLAIN2'] = "Telefon oraz eMail";
$lang['CONTACT_EXPLAIN2'] = "Na ponad 90% pytań odpowiadamy w przeciągu 24h";


// PHONE BOX
$lang['CONTACT_PHONE1'] = "Telefon";
$lang['CONTACT_PHONE2'] = "0800 - 765 43 43";
$lang['CONTACT_PHONE3'] = "za darmo - dostępne całą dobę";
$lang['CONTACT_PHONE4'] = "Zamów telefon zwrotny";
$lang['CONTACT_PHONE5'] = "Email";
// EMAIL BOX
$lang['CONTACT_EMAIL1'] = "kontakt przez Email";
$lang['CONTACT_EMAIL2'] = "info@medlanes.com";
$lang['CONTACT_EMAIL3'] = "Natychmiastowa odpowiedź w godzinach 8-18";

// CONTACT FORM
$lang['CONTACT_FORM_HEADING1'] = "Formularz kontaktowy";
$lang['CONTACT_FORM_HEADING2'] = "Możesz również skontaktować sie z nami korzystając z poniższego formularza";

$lang['CONTACT_FORM_NAME'] = "Imię";
$lang['CONTACT_FORM_LASTNAME'] = "Nazwisko";
$lang['CONTACT_FORM_EMAIL'] = "Email";
$lang['CONTACT_FORM_MESSAGE'] = "Wiadomość";
$lang['CONTACT_FORM_SEND'] = "Wyślij";

?>