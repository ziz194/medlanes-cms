<?php

$lang['OPT_HEADING'] = 'Informacje opcjonalne';
$lang['OPT_SUBHEAD'] = 'Twój lekarz chce odpowiedzieć na twoje pytanie jak najlepiej. Podając dodatkowe informacje otrzymasz dokładniejszą i szybszą odpowiedź.';
$lang['OPT_SKIP'] = 'Pomiń';
$lang['OPT_SKIP2'] = 'Pomiń';
$lang['OPT_NAME'] = 'Imię:';
$lang['OPT_GENDER'] = 'Wybierz płeć:';
$lang['OPT_SELECT'] = 'Zaznacz';
$lang['OPT_MALE'] = 'Mężczyzna';
$lang['OPT_FEMALE'] = 'Kobieta';
$lang['OPT_AGE'] = 'Wiek';
$lang['OPT_PHOTOS'] = 'Zalącz istotne zdjęcia';
$lang['OPT_FILES'] = 'Zalącz pliki';
$lang['OPT_MED_HISTORY'] = 'Istotne informacje medyczne(np. Alergie, Podwyższone ciśnienie krwi, Przebyte choroby)';
$lang['OPT_MEDICATION'] = 'Wymień ostatnio zażywane lekarstwa';
$lang['OPT_SYMPTOMS'] = 'Od kiedy występują symptomy?';
$lang['OPT_SYMPTOMS_DD'] = '<span id="sincewhen">Wybierz</span>
					<ul class="dropdown">
						<li><a href="#">Nagły przypadek</a></li>
						<li><a href="#">Dzisiaj</a></li>
						<li><a href="#">Tydzień</a></li>
						<li><a href="#">Miesiąc</a></li>
						<li><a href="#">Przewlekłe</a></li>
					</ul>';
$lang['OPT_CONTINUE'] = 'Dalej';



$lang['PAYMENT_NEW1'] = "Zaznacz";
$lang['PAYMENT_NEW2'] = "Jeden z naszych ekspertów jest gotowy odpowiedzeć na twoje pytanie. Zapewniamy  <b>100% gwarancję satysfakcji</b>.";
$lang['PAYMENT_NEW3'] = 'Honorarium lekarza <b id="detailsswap">szczegółowa</b> i <b id="urgencyswap">szybka</b> odpowiedź:';
$lang['PAYMENT_NEW4'] = "Nasz specjalista jest gotowy ci pomóc";
$lang['PAYMENT_NEW5'] = "dr J. Männel";
$lang['PAYMENT_NEW6'] = "Medycyna ogólna 12 lat doświadczenia";
$lang['PAYMENT_NEW7'] = "Sprawdzony ekspert";
$lang['PAYMENT_NEW8'] = "Ocena";
$lang['PAYMENT_NEW9'] = "<b>9,7 of 10</b> (Sanego)";
$lang['PAYMENT_NEW10'] = "<b>Gwarancja zwrotu pieniędzy</b><br>Gwarantujemy 100% satysfakcji – płacisz tylko wtedy, kiedy jesteś zadowolony z usługi.";




?>