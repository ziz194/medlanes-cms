
<?php 

$lang['PRIVACY_TITLE'] = 'Medlanes - Polityka prywatności';

// PRIVACY HEADING

$lang['PRIVACY_HEADING1'] = '<h1>Polityka prywatności</h1>';


// PRIVACY PARAG

$lang['PRIVACY_1'] = '
				<p><b>§ 1. Gromadzenie danych personalnych </b></p>
				<p>W celu zapewnienia uzgodnionych usług na mocy §2 Ogólne Warunki e</p>
					<p class="center">Medlanes GmbH, reprezentowana przez dyrektora zarządzającego lek. med. Emil Kendziorra oraz  Erik Stoffregen, </p>
					<p class="center">Oranienstr. 185, 10999 Berlin</p>
					<p class="center">Telephone: +49 (0)30 577 004 20</p>
					<p class="center">Fax: +49 (0)6151 62749 799 </p>
					<p class="center">Email: info(at)medlanes.com </p>
					<p style="margin-top:20px">– dalej zwany ”Medlanes” –</p>
					<p>Następuje gromadzenie  danych wprowadzonych przez uzytkownika wlącznie z adresem IP (dalej zwane "danymi osobistymni").</p>
';						
$lang['PRIVACY_2'] = '
				<p><b>§ 2 Cel gromadzenia, przetwarzania i wykorzystywania danych osobowych </b></p>
				<p>(1)	Dane osobowe są przetwarzane i gromadzone wyłacznie na potrzeby świadczenia usługi zgodnie z warunkami zwartymi w § 2 "Ogólne warunki".</p>
				<p>(2)	Zgromadzone dane bedą przechowywane i przetwarzane na bezpiecznych serwerach firmy Medlanes w Niemczech.</p>
				<p>(3)	Medlanes korzysta z plików cookie przechowywanych na komputerze użytkownika w celu przeprowadzenia procesu rejestracji. Uzytkownik ma prawo zablokowania plików cookie we własnym zakresie  zmieniajac ustawienia swojej przeglądarki internetowej. W przypadku blokady lub usunięcia plików cookie Medlanes zastrzega że funkcjonalność strony może zostać ograniczona</p>                                                                                                                                                       


';
$lang['PRIVACY_3'] = '
				<p><b>§ 3 Ochrona prywatności</b></p>
				<p>Medlanes jest w pełni zaangażowane w ochronę prywatnosci użytkowników i zapewnia gromadzenie i przetwarzanie danych zgodnie z ustawą o ochronie danych osobowych wylącznie w celu świadczenie usług określonych  w  § 2 "Ogólne warunki".Pracownicy firmy Medlanes są całkowicie zobligowani do ochrony danych osobowych użytkowników.</p>
';					                 
$lang['PRIVACY_4'] = '
				<p><b>§ 4 Prawa użytkownika</b></p>
				<p>Użytkownik zgodnie z ustawą o danych osobowych  ma prawo dostępu do swoich danych oraz możliwość ich modyfikowania lub usuwania w dowolnym czasie. W zakresie tych praw użytkownik może skontaktować się z administratorem danych Medlanes określonym w §7 Kontakt.</p>
';
$lang['PRIVACY_5'] = '
				<p><b>§ 5 Zgoda</b></p>
				<p>Jeśli użytkownik zgadza się na warunki niniejszej umowy, użytkownik zgadza się również na  kontrolowane wykorzystanie danych osobowych.Zgoda, lub odmowa przyjęcia warunków umowy następuje poprzez zaznaczenie lub nie zaznaczenie  odpowiedniego pola. Zarówno zatwierdzenie jak i odmawa zgody są rejestrowane; Ich treść jest dostępna dla użytkownika w dowolnym momencie za pomocą (<a href="terms.php"> Regulamin</a>). W każdej chwili użytkownik ma prawo do cofnięcia zgody, a tym samym rozwiązaniu swojego konta, łącznie z usunięciem wszystkich danych osobowych</p>
';
$lang['PRIVACY_6'] = '
				<p><b>§ 6 Płatność </b></p>
				<p>W celu przetwarzania płatności  dane transakcji, w tym dane osobowe, mogą być przeniesione do PayLane Sp. z.o.o która mieści się w Gdańsku przy Arkońskiej 6 / A3, kod pocztowy: 80-387, NIP: 0000227278.</p>
';
$lang['PRIVACY_7'] = '
				<p><b>§ 7 Kontakt </b></p>
				<p>Osobą kontaktową w sprawie wszystkich pytań związanych z prywatnością jest Erik Stoffregen</p>
';

?>
