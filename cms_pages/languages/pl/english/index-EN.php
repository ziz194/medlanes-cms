<?php 

// PAGE TITLES

$lang['TITLE_IMPRINT'] = "Medlanes | Rzetelne informacje medyczne - Opinie ekspertów - Opinia drugiego lekarza | Dane firmy";
$lang['TITLE_INDEX'] = "Medlanes | Rzetelne informacje medyczne - Opinie ekspertów - Opinia drugiego lekarza| Home";
$lang['TITLE_PAYMENT'] = "Medlanes | Rzetelne informacje medyczne - Opinie ekspertów - Opinia drugiego lekarza| Płatność";
$lang['TITLE_PRIVACY'] = "Medlanes | Rzetelne informacje medyczne - Opinie ekspertów - Opinia drugiego lekarza | Polityka prywatności";
$lang['TITLE_TERMS'] = "Medlanes | Rzetelne informacje medyczne - Opinie ekspertów - Opinia drugiego lekarza | Warunki korzystania z serwisu";
$lang['TITLE_CONTACT'] = "Medlanes | Rzetelne informacje medyczne - Opinie ekspertów - Opinia drugiego lekarza | Kontakt";
$lang['TITLE_THANKYOU'] = "Medlanes | Rzetelne informacje medyczne - Opinie ekspertów - Opinia drugiego lekarza| Dziękujemy";


// HEADER TEXT

$lang['HEADER_1'] = "Sprawdzone informacje medyczne od lekarzy specjalistów";
$lang['HEADER_2'] = "Nasz zespół chętnie ci pomoże <b>service@medlanes.com</b>";

/*FOOTER LINKS*/

$lang['FOOTER1'] = '<a href="index.php?lang=en">Home</a>';
$lang['FOOTER3'] = '<a href="contact.php?lang=en">Strona główna</a>';
$lang['FOOTER4'] = '<a href="terms.php?lang=en">Wsparcie</a>';
$lang['FOOTER5'] = '<a href="privacy.php?lang=en">Ogólne warunki</a>';

$lang['FOOTER8'] = '<a href="#"> Kontakt PR</a>';
$lang['FOOTER9'] = '<a href="#"> Kariera</a>';
$lang['FOOTER10'] = '<p>Wspólnicy <br> i Partnerzy</p>';
$lang['FOOTER11'] = '&copy; Medlanes GmbH - Wszystkie prawa zastrzeżone.';

$lang['INDEX_1'] = "Szybka odpowiedź od najlepszych lekarzy";
$lang['INDEX_2'] = "Nasi eksperci odpowiedzą na twoje pytanie medyczne. Nowe pytanie co <b>9 sekund</b>. Zapewniamy <b>100% satysfakcję.</b>";
$lang['INDEX_3'] = "Twoje pytanie do naszych ekspertów";
$lang['INDEX_4'] = "Twój e-mail*";
$lang['INDEX_5'] = "Otrzymaj odpowiedź";
$lang['INDEX_6'] = "*Twoje dane są szyfrowane i nie bedą nikomu udostępnione";
$lang['INDEX_7'] = "Koncept znany z:";
$lang['INDEX_8'] = "W mediach";
$lang['INDEX_9'] = "Medlanes odnotowało gwałtowny skok w ilości użytkowników. Liczba użytkowników Medlanes zwiększyła sie o  36% w przeciągu ostatniego miesiąca, łącznie odwiedziło ją ponad 400,000 ludzi";
$lang['INDEX_10'] = "Today, 10/8/2013";
$lang['INDEX_11'] = "Klienci o nas";
$lang['INDEX_12'] = "“Aplikacja Medlanes znacząco poprawiła jakość mojego życia. Od jakiegoś czasu miałam coraz więcej wątpliwości co do mojego zdrowia, nie chciałam dzwonić do mojego lekarza co 5 minut";
$lang['INDEX_13'] = "Ksenia, 27 <br> Łódź";
$lang['INDEX_14'] = "Nasi lekarze";
$lang['INDEX_15'] = "dr K. Hamann";
$lang['INDEX_16'] = "Medycyna ogólna, 12 lat doświadczenia";
$lang['INDEX_17'] = "Ocena: 4.9 / 5";
$lang['INDEX_18'] = "";
$lang['INDEX_19'] = "";



// IN THE PRESS
$lang['PRESS_0'] = "O telemedycynie w mediach";
$lang['PRESS_1'] = "[...] rządy 83% wszystkich państw wspierają stosowanie telefonów komórkowych w usługach medycznych.";
$lang['PRESS_2'] = "Światowa Organizacja Zdrowia";
$lang['PRESS_3'] = "[...] ważne jest, żebyśmy wykorzystali wszelkie możliwe rozwiązania techniczne, żeby postęp w medycynie dotarł rzeczywiście do wszystkich pacjentów.";
$lang['PRESS_4'] = "Minister zdrowia Niemiec Hermann Gröhe w wywiadzie dla FAZ";
$lang['PRESS_5'] = "Telemedycyna [...] pomoże nam znacząco poszerzyć dostęp do usług medycznych.";
$lang['PRESS_6'] = "Sabine Bätzing-Lichtenthäler, Minister pracy RFN dla gazety Die Welt";
$lang['PRESS_7'] = "Dzięki możliwościom, które dają nam nowoczesne technologie informatyczne i komunikacyjne, będziemy mogli w przyszłości wzbogacić opiekę medyczną o nowe, wartościowe elementy.";
$lang['PRESS_8'] = "Prezes inicjatywy eHealth ";
$lang['PRESS_9'] = "Kontakt z lekarzem online: Samodzielne poszukiwanie odpowiedzi na problemy zdrowotne w internecie nie jest pozbawione niebezpieczeństw. Często rozsądniej jest poradzić się eksperta.";
$lang['PRESS_10'] = "Süddeutsche Zeitung";
$lang['PRESS_11'] = "Każdy lekarz udzielający informacji w sieci jest gruntownie sprawdzany.";
$lang['PRESS_12'] = "Wirtschaftswoche";

// TESTIMONIALS
$lang['TESTIMONIALS_0'] = "Klienci o nas";
$lang['TESTIMONIALS_1'] = "Jestem bardzo zadowolona z informacji, których udzielił mi lekarz. Odpowiedź była szybka i sensowna.";
$lang['TESTIMONIALS_2'] = "J. Górska, 34 lat  <br> Warszawa";
$lang['TESTIMONIALS_3'] = "Na początku miałem spore opory przed kontaktem z lekarzem przez internet. Ale odpowiedź od lekarza miło mnie rozczarowała.";
$lang['TESTIMONIALS_4'] = "T. Rydz, 31 lat <br> Brodnica";
$lang['TESTIMONIALS_5'] = "W grupie wsparcia poradzili zapytać lekarza w internecie. Z pomocą Medlanes w końcu udało się rzucić!";
$lang['TESTIMONIALS_6'] = "R. Tyrmand, 58 lat <br> Wrocław";
$lang['TESTIMONIALS_7'] = "Nie miałem pojęcia, co oznaczają moje wyniki z laboratorium. Lekarz online wszystko mi wyjaśnił.";
$lang['TESTIMONIALS_8'] = "M. Marcinkowski, 27 <br> Stargard";
$lang['TESTIMONIALS_9'] = "Córka obudziła się w środku nocy i dziwnie się zachowywała, więc potrzebna mi była szybka porada lekarska. Wtedy natknęłam się na Medlanes.";
$lang['TESTIMONIALS_10'] = "P. Bielik, 29 lat <br> Sopot";
$lang['TESTIMONIALS_11'] ="Po wizycie potrzebna mi była konsultacja z drugim lekarzem. Możliwość skierowania pytania do lekarza w internecie znacznie uprościła sprawę.";
$lang['TESTIMONIALS_12'] = "B. Winiak, 47 <br> Warszawa";

// DOCTORS
$lang['DOCTORS_0'] = "Nasi lekarze";
$lang['DOCTORS_1'] = "dr Jessica Männel";
$lang['DOCTORS_2'] = "Specjalista medycyny ogólnej <br> 12 lat doświadczenia";
$lang['DOCTORS_3'] = "Ocena 4.9 / 5";
$lang['DOCTORS_4'] = "dr Wolf Siepen";
$lang['DOCTORS_5'] = "Specjalista chirurgii i traumatologii <br> 12 lat doświadczenia";
$lang['DOCTORS_6'] = "Ocena 4.7 / 5";
$lang['DOCTORS_7'] = "dr Kathrin Hamann";
$lang['DOCTORS_8'] = "Specjalista medycyny ogólnej <br> 14 lat doświadczenia";
$lang['DOCTORS_9'] = "Ocena 4.9 / 5";
$lang['DOCTORS_10'] = "dr Norbert Scheufele";
$lang['DOCTORS_11'] = "Lekarz ginekolog <br> 20 lat doświadczenia";
$lang['DOCTORS_12'] = "Ocena 4.4 / 5";
$lang['DOCTORS_13'] = "Christian Welsch";
$lang['DOCTORS_14'] = "Lekarz laryngolog <br> 16 lat doświadczenia";
$lang['DOCTORS_15'] = "Ocena 4.8 / 5";
$lang['DOCTORS_16'] ="Stefan Paffrath";
$lang['DOCTORS_17'] = "Dyplomowany psycholog <br> 27 lat doświadczenia";
$lang['DOCTORS_18'] = "Ocena 4.8 / 5";


?>