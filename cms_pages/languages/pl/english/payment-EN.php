<?php 

$lang['PAYMENT_'] = "Płatność";

$lang['PAYMENT_HEADING'] = "Wybierz pilność i poziom szczegółowości odpowiedzi";
$lang['PAYMENT_SUBHEADING'] = "Wybierz jak szybko i jak dokładną chcesz otrzymać odpowiedź";


// CHOOSE URGENCY PANEL
$lang['PAYMENT_CHOOSEPANEL'] = "Pilność";

$lang['PAYMENT_CHOOSEPANEL1'] = "Szczegółowość";
$lang['PAYMENT_CHOOSEPANEL2'] = "Niska";
$lang['PAYMENT_CHOOSEPANEL3'] = "Średnia";
$lang['PAYMENT_CHOOSEPANEL4'] = "Wysoka";
$lang['PAYMENT_CHOOSEPANEL5'] = "Kwota jaką zapłacisz, jeśli będziesz usatysfacjonowany odpowiedzią: ";


// DEPOSIT FEE

$lang['PAYMENT_DEPOSIT1'] = "Wpłać honorarium lekarza (100% gwarancja satysfakcji)";
$lang['PAYMENT_DEPOSIT2'] = "otrzymaj odpowiedź natychmiast";
$lang['PAYMENT_DEPOSIT22'] = "Wpłać opłatę wynoszącą:";
$lang['PAYMENT_DEPOSIT3'] = "Zapłać korzystając z PayPal";
$lang['PAYMENT_DEPOSIT4'] = "Twoje informacje są bezpieczne";
$lang['PAYMENT_DEPOSIT5'] = "Klikając 'Dalej' zgadzasz sie na warunki korzystania z serwisu oraz potwierdzasz że masz więcej niz 18 lat";
$lang['PAYMENT_DEPOSIT6'] = "Dalej do ostatniego etapu";


// SIDEBAR 

$lang['PAYMENT_SIDEHEAD1'] = "Sieć lekarzy i ekspertów";
$lang['PAYMENT_SIDEHEAD2'] = "Jeden z naszych wysoko wykwalifikowanych lekarzy odpowie ci w krótce";


$lang['PAYMENT_SIDE_DOC1HEAD1'] = "dr n.med. Kathrin Hamann";
$lang['PAYMENT_SIDE_DOC1HEAD2'] = "Medycyna rodzinna";
$lang['PAYMENT_SIDE_DOC1TEXT3'] = "<p>- 9 lat doświadczenia</p>";

$lang['PAYMENT_SIDE_DOC2HEAD1'] = "Lek. med. Wolf Siepen";
$lang['PAYMENT_SIDE_DOC2HEAD2'] = "Lekarz ortopeda";
$lang['PAYMENT_SIDE_DOC2TEXT3'] = "<p>- 12 lat doświadczenia </p>";

$lang['PAYMENT_SIDE_DOC3HEAD1'] = "dr n.med. Norbert Scheufele";
$lang['PAYMENT_SIDE_DOC3HEAD2'] = "Ginekologia";
$lang['PAYMENT_SIDE_DOC3TEXT3'] = "<p>- 20 lat doświadczenia klinicznego</p>";
$lang['pricess'] = '$';


$lang['PAYMENT_AGREEMENT1'] = 'Przeczytałem i zgadzam się na';
$lang['PAYMENT_AGREEMENT2'] = '<a href="privacy.php?lang=de" target="_blank" >prywatność</a> oraz';
$lang['PAYMENT_AGREEMENT3'] = '<a href="terms.php?lang=de" target="_blank" >warunki</a>';
$lang['PAYMENT_AGREEMENT4'] = 'oraz mam przynajmniej 18 lat';


$lang['PAYMENT_NEW1'] = "Wybierz sposób płatności";
$lang['PAYMENT_NEW2'] = "Nasz ekspert jest gotów Ci pomóc. Zapewniamy  <b>100% gwarancję satysfakcji</b>.";
$lang['PAYMENT_NEW3'] = 'Cena jaką zapłacisz, jeśli będziesz zadowolony z odpowiedzi: PLN ';
$lang['PAYMENT_NEW4'] = "Specjalista jest gotowy odpowiedzieć na twoje pytanie";
$lang['PAYMENT_NEW5'] = "dr J. Männel";
$lang['PAYMENT_NEW6'] = "Lekarz rodzinny, 10 lat doświadczenia";
$lang['PAYMENT_NEW7'] = "SPRAWDZONY EKSPERT";
$lang['PAYMENT_NEW8'] = "Ocena";
$lang['PAYMENT_NEW9'] = "<b>4.85 / 5</b>";
$lang['PAYMENT_NEW10'] = "<b>Gwarancja zwrotu kosztów</b><br>Gwarantujemy 100% satysfakcji – płacisz tylko wtedy, kiedy jesteś zadowolony z usługi.";

$lang['3step_1'] = "Opisz swoje symptomy";
$lang['3step_2'] = "Zapłac honorarium";
$lang['3step_3'] = "Uzyskaj profesjonalną opinię";


$lang['DEP_SELECT'] = 'Wybierz';
$lang['DEP_METHOD'] = 'Metoda płatności';
$lang['DEP_CC'] = 'Karta Kredytowa';
$lang['DEP_PP'] = 'PayPal';
$lang['DEP_WT'] = 'SOFORT';
$lang['DEP_POL_P'] = 'Przelew bankowy';
$lang['DEP_PP_TEXT'] = 'Zostaniesz przeniesiony na stronę PayPal.';
$lang['DEP_CC_NR'] = 'Numer karty';
$lang['DEP_CC_EXP'] = 'Data ważności';
$lang['DEP_CC_MONTH'] = '<span id="month">Miesiąc</span>
						<ul class="dropdown">
							<li><a href="#" id="01">01 - Sty</a></li>
							<li><a href="#" id="02">02 - Lut</a></li>
							<li><a href="#" id="03">03 - Mar</a></li>
							<li><a href="#" id="04">04 - Kwi</a></li>
							<li><a href="#" id="05">05 - Maj</a></li>
							<li><a href="#" id="06">06 - Cze</a></li>
							<li><a href="#" id="07">07 - Lip</a></li>
							<li><a href="#" id="08">08 - Sie</a></li>
							<li><a href="#" id="09">09 - Wrz</a></li>
							<li><a href="#" id="10">10 - Paź</a></li>
							<li><a href="#" id="11">11 - Lis</a></li>
							<li><a href="#" id="12">12 - Gru</a></li>							
						</ul>';
$lang['DEP_CC_YEAR'] = '<span id="year">Rok</span>
						<ul class="dropdown">
							<li><a href="#">2015</a></li>							
							<li><a href="#">2016</a></li>							
							<li><a href="#">2017</a></li>							
							<li><a href="#">2018</a></li>							
							<li><a href="#">2019</a></li>							
							<li><a href="#">2020</a></li>							
							<li><a href="#">2021</a></li>							
							<li><a href="#">2022</a></li>							
							<li><a href="#">2023</a></li>							
							<li><a href="#">2024</a></li>							
						</ul>';
$lang['DEP_CC_CVC'] = 'Kod CVV2';
$lang['DEP_SECURE'] = 'Twoje dane są bezpieczne.';
$lang['DEP_PRIVACY'] = 'Prywatność i Bezpieczeństwo';
$lang['POLISH_PAYMENTS'] = '<p>Bank</p>
					<div id="dd" class="wrapper-dropdown-3 bank" tabindex="1">
					<span id="bank">Wybierz bank</span>
					<ul class="dropdown">
						<li><a href="#" id="AB">Alior Bank</a></li>
						<li><a href="#" id="AS">T-Mobile Usługi Bankowe</a></li>
						<li><a href="#" id="MU">Multibank</a></li>
						<li><a href="#" id="MT">mTransfer</a></li>
						<li><a href="#" id="IN">Inteligo</a></li>
						<li><a href="#" id="IP">iPKO</a></li>
						<li><a href="#" id="NO">Nordea</a></li>
						<li><a href="#" id="DB">Deutsche Bank</a></li>
						<li><a href="#" id="MI">Millennium</a></li>
						<li><a href="#" id="CA">Credit Agricole</a></li>
						<li><a href="#" id="PP">Poczta Polska</a></li>
						<li><a href="#" id="BP">Bank BPH</a></li>
						<li><a href="#" id="IB">Idea Bank</a></li>
						<li><a href="#" id="PO">Pekao S.A.</a></li>
						<li><a href="#" id="GB">Getin Bank</a></li>
						<li><a href="#" id="IG">ING Bank Śląski</a></li>
						<li><a href="#" id="WB">Bank Zachodni WBK</a></li>
						<li><a href="#" id="OH">Inny</a></li>
					   </ul>
					</div>';

$lang['PAY_CONTINUE'] = 'Dalej';

?>
