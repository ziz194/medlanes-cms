<?php 

$lang['THANKYOU_1'] = "Dziękujemy za Twoje pytanie. Jakie są następne kroki?";
$lang['THANKYOU_2'] = "Odpowiedź na maila otrzymasz prosto na podany adres e-mail.";
$lang['THANKYOU_3'] = "Jeśli nie będziemy w stanie połączyć Cię z naszym lekarzem - zwrócimy poniesione przez Ciebie koszty. Natychmiast.";
$lang['THANKYOU_4'] = "Jeśli masz jakieś pytania ...";
$lang['THANKYOU_5'] = "Nasza obsługa klienta jest zawsze do twojej dyspozycji.";
$lang['THANKYOU_6'] = 'e-mail: <b style="padding-left:10px">support@medlanes.com</b>';
$lang['THANKYOU_7'] = 'Tel.<b style="padding-left:28px">+49 30 577 004 20</b>';
$lang['THANKYOU_8'] = "";
$lang['THANKYOU_9'] = "";

?>