<?php 
$lang['TERMS_TITLE'] = 'Medlanes - Warunki korzystania z serwisu';

// TERMS HEADING

$lang['TERMS_HEADING1'] = '<h1>Regulamin</h1>';
$lang['TERMS_HEADING2'] = '<b>Firma Medlanes dostarcza wylącznie informacji medycznych. W żadnym wypadku usługa świadczona nie powinna być używana w celu diagnozy i podejmowania osobistych decyzji medycznych.</b>';

// TERMS PARAG


$lang['TERMS_1'] ='
				<p><b>§ 1 Overview</b></p>		
				<p>Korzystanie z usług oferowanych przez Medlanes GmbH podlega następującym warunkom. Wszystkie odstępstwa od umowy należy złożyć pisemnie. Firma Medlanes GmbH, zarejestrowana jest w rejestrze handlowym prowadzonym przez lokalny sąd Charlottenburg (Berlin, Niemcy) pod numerem rejestracyjnym HRB159023B jest reprezentowana przez jej dyrektorów lek. med. Emil Kendziorra i Erik Stoffregen.</p>	
			    <p >Zapytania można kierować na: </p>
					<p class="center">Via Mail: Oranienstr. 185, 10999 Berlin </p>
					<p class="center">Telephone: +49 (0)30 577 004 20</p>
					<p class="center">Fax: +49 (0)6151 62749 799</p>
					<p class="center">Email: info(at)medlanes.com </p>
				<p>Zgodnie z §27 prawa podatkowego, nasz numer rejestracyjny: DE295684112</p>
';
$lang['TERMS_2'] ='
				<p><b>§ 2 Zakres</b></p>
				<p>1. Medlanes jest mobilną i internetową platformą zdrowia, która daje użytkownikom możliwość uzyskania informacji medycznych przez internet. Użytkownik otrzymuje możliwość złożenia za pośrednictwem platformy zapytania w postaci objawów  choroby, zażywanych leków, historii medyczne, zdjęcia lub ogólnego pytania medycznego. Po złożeniu zapytania , użytkownik otrzyma jedną z następujących odpowiedzi:</p>
				        <p class="center">Automatycznie wygenerowany e-mail stwierdzający, że nie jesteśmy  w stanie odpowiedzieć na zadane pytanie</p>
				        <p class="center">Odpowiedź na zadane pytanie od lekarza medycyny</p>
				<p>2. Usługi świadczone przez Medlanes, które wykraczają poza zakres podany na stronie internetowej (<a href="index.php"> www.medlanes.com </a>) nie są określone jako usługi medyczne. Usługa firmy  Medlanes nie może zastąpić tradycyjnej wizyty  u lekarza. W przypadkach, które wymagają natychmiastowej pomocy medycznej(np. nagłe przypadki,myśli samobójcze itp.) należy traktować jako wyjątkowego i nie jesteśmy w stanie pomóc w tych przypadkach. Specjalne przepisy zawodowe i ograniczenia mają zastosowanie do opieki medycznej w Internecie. Dlatego świadczone usługi  są ograniczone do obecnych możliwości prawnych</p>
';
$lang['TERMS_3'] ='
				<p><b>§ 3 Registration</b></p>
				<p>1. Użytkowanie Medlanes wymaga ustanowienia  konta użytkownika, do czego wymagana jest rejestracja.W czasie rejestracji użytkownik winien wypełnic wszystkie wymagane informacje zawarte w szablonie rejestracyjnym. Użytkownik serwisu musi miec ukończone 18 lat by korzystać z Medlanes </p>
				<p>2. Po zakończeniu rejestracji, użytkownik otrzyma automatyczną wiadomość e-mail zawierającą link potwierdzający wysłany na adres e-mail, który został podany podczas rejestracji. Ten link należy kliknąć, aby aktywować konto użytkowników. Jeśli konto nie zostanie aktywowane w przeciągu dwóch tygodni od procesu rejestracji, rejestracja zostanie odrzucona i informacje użytkownika zostaną usunięte. Po aktywacji konta następuje zawarcie umowy prawnej zgodnie z  ogólnymi warunkami zawartmi w "Warunkach umowy" </p>
				<p>3. Medlanes nie jest prawnie zobligowane do wykonania wymienionych usług i ma prawo do nie przyjęcia zgłoszenia lub odmowy rejestracji użytkownika bez podania przyczyny.</p>
';
$lang['TERMS_4'] ='
				<p><b>§ 4 Warunki </b></p>
				<p>Odsąpienie od umowy:</p>
				<p>Masz prawo do anulowania usługi w ciągu czternastu (14) dni bez podania przyczyny.</p>

				<p>Aby skorzystać z tej opcji, należy poinformować</p>
					<p class="center">Medlanes GmbH, reprezentowanej przez lek.med. Emila Kendziorra - Dyrektor Zarządzający</p>
					<p class="center">Oranienstr. 185, 10999 Berlin</p>
					<p class="center">Telephone: +49 (0)30 577 004 20</p>
					<p class="center">Fax: +49 (0)6151 62749 799 </p>
				    <p class="center">Email: info(at)askadoctor.today </p>
				<p>Wysyłając jasną i zwięzła wiadomość zawierająca informacje o podjętej decyzji.</p>
				<p>Aby dotrzymać terminu rezygnacji, wystarczy, że wyślesz swoje informacja o skorzystaniu z prawa do odstąpienia od umowy przed upływem terminu odstąpienia od umowy.</p>
				<p>Skutki odstąpienia od umowy:</p>
				<p>W przypadku odstąpienia od niniejszej umowy, Medlanes w czasie nie dłuższym niż 14 (czternastu) dni od daty otrzymania oświadczenia o odstąpieniu od niniejszej umowy.
					Jeśli nie sprecyzowano inaczej w dokumencie odstąpienia od umowy Pierwotny koszt zostanie zwrócony w taki sam sposób w jaki dokonano płatnośći.
				 </p>
				<p>If services have been rendered during the withdrawal period, you will be responsible for the complete costs of specified service up until the date on which notification is received of your right of cancellation in the respect of this contract. Service rendered compared to the total amount provided for in the contract.</p>
				<p>- Koniec informacji  o odstąpieniu od umowy</p>
';
$lang['TERMS_5'] ='
				<p><b>§ 5 Obowiązek informacyjny</b></p>
				<p>1. Pozasądowej procedury wnoszenia skarg i dochodzenia roszczeń, nie mają zastosowania do Medlanes GmbH.</p>
				<p>2. Dalsze informacje na temat Medlanes, w tym naszych usług i procesu można zobaczyć wyłącznie na <a href="index.php"> www.medlanes.com</a>.</p>
				<p> 3. Wszystkie kroki tej umowy mogą być obserwowane przez sekcje §3 niniejszej umowy.</p>
				<p> 4. Użytkownik może zapisać tekst tej umowy o ogólnych warunkach świadczenia usług wybierając funkcję "Zapisz jako" w przeglądarce. Użytkownik dodatkowo może wydrukować tekst tej umowy za pomocą funkcji drukowania swojej przeglądarce. Medlanes domy wszystkich warunków i teksty, i są dostępne dla użytkownika, na wniosek przedłożony przez e-mail lub pocztą. </p>
				<p> 5. Wszelkie wprowadzone przez użytkownika informacje mogą być zmienione w każdej chwili, wybierając przycisk "Wstecz" w przeglądarce. Zakończenie sesji przeglądarki będzie działać jako anulowanie procesu rejestracji. Można to zrobić w dowolnym momencie </p>
				<p>6. Zawarcie tej umowy jest dostępna wyłącznie w języku niemieckim. </p>
				<p>7. Poddaliśmy nam żadnej konkretnej kodeks postępowania (polityki)</p>
';
$lang['TERMS_6'] ='
				<p><b> §6 Prawa użytkownika</b></p>
				<p>1. Użytkownikowi przyznane są prawa zawarte wylącznie  niniejszym Regulaminie.</p>
				<p>2. Informacje publikowane na stronie internetowej Medlanes oraz platformie mobilnej z uwzględnieniem wszystkich treści, informacji, zdjęć, wideo i / lub baz danych są przedmiotem prawa autorskiego i własności firmy Medlanes GmbH.</p>
				<p>3. Materiały dostępne za pośrednictwem aplikacji internetowej "Medlanes i / lub  strony internetowej  mogą być wykorzystywane lub reprodukowany w celach osobistych i niekomercyjnych. Jakiekolwiek inne wykorzystanie ich z  bez pisemnej zgody jest zabronione Medlanes GmbH.</p>
';
$lang['TERMS_7'] ='
				<p><b> § 7 Dane użytkownika</b></p>
				<p>1. Medlanes ma prawo zbierać i wykorzystywać informacje dostarczone przez użytkownika zgodnie z niniejszą umową. Bez wcześniejszej zgody użytkownika, Medlanes GmbH jest uprawniony do gromadzenia i przetwarzania wszystkich danych użytkownika. Zebrane dane będą przetwarzane i wykorzystywane w stopniu niezbędnym do realizacji usługi.</p>
				<p> 2. W każdej chwili, użytkownik ma zapewniony dostęp do  platformy internetowej  na ma możliwość pobierania danych przechowywanych na jego temat informacji jak i możliwość ich edycji bądź usunięcia</p>
				<p>3. Pełny Raport o prywatności dostępny jest  poprzez <a href="index.php"> www.medlanes.com </a></p>
';
$lang['TERMS_8'] ='
				<p><b>§ 8 Dostępność </b></p>
				<p>Usługa świadczona przez Medlanes dostępna jest  24 godziny na dobę, z wyjątkiem w czasach następujących przypadkach jest </p>
				        <p class="center">W trakcie operacji tworzenia kopii zapasowych</p>
				        <p class="center">W czasie konserwacji systemu </p>
				        <p class="center">lub w czasie pracy programu w systemie i / lub bazy danych</p>
				<p>Medlanes dokona wsszelkich starań by minimalizować czas występywania zakłóceń w działaniu serwisu</p>
';
$lang['TERMS_9'] ='
				<p><b>§ 9 odpowiedzialność</b></p>
				<p>1. Medlanes nie może być pociągnięty do odpowiedzialności za szkody wynikające z opóźnienia, niewykonania, poniżej normy wydajności lub jakiegokolwiek bezprawnego działania lub naruszenia prawa (innych niż w ramach umowy) prowadzącej do odpowiedzialności prawnej cywilnej.</p>
				<p>2. Medlanes odpowiada tylko za szkody cywilne, ale nie może być pociągnięty do odpowiedzialności za szkody pośrednie, w tym szkody następcze, nieprzewidziane szkody, nietypowych uszkodzeń lub utratę zysków. Dodatkowo zapis ten ma zastosowanie także do sporów pracowniczych, przypadkowym uszkodzeniem oraz siły wyższej. </p>
				<p>Powyższe ograniczenie odpowiedzialności ma zastosowanie do wszystkich umownych i pozaumownych roszczeń.</p>
';
$lang['TERMS_10'] ='
				<p><b> § 10 Obowiązki Użytkownika </b></p>
				<p>1. Użytkownik jest uprawniony do korzystania z usługi świadczonej przez Medlanes GmbH prawidlowo, zgodnie z ogólnie przyjętymi zasadami. Użytkownik ponosi odpowiedzialność podania swoich danych osobowych , w tym nazwę użytkownika i hasło. Na użytkowniku spoczywa odpowiedzialność podania prawdziwych danych </p>
				<p>2. Użytkownik jest zobowiązany do dostarczenia wszelkich niezbędnych informacji. Oraz zapewnić ze są one pełne i zgodne z prawdą.</p>
';
$lang['TERMS_11'] ='
				<p><b>§ 11 Blokowanie dostępu / Zerwanie umowy</b></p>
				<p>1. Medlanes GmbH zastrzega sobie prawo do prowadzenia postępowania sądowego w przypadku niewłaściwego użycia lub istotnego  naruszenie.Medlanes zastrzega sobie prawo do zablokowania dostępu użytkownika w przypadku podejrzanej aktywności</p>
				<p>Każda strona tej umowy ma prawo do  jej rozwiązania, jeśli jest ku temu przyczyna. Wypowiedzenie musi zostać złożone w formie pisemnej (np e-mail). W przypadku rozwiązania umowy, użytkownik musi określić datę zakończenia, w którym dostęp użytkownika do platformy internetowej Medlanes zostanie przerwany</p>
';
$lang['TERMS_12'] ='
				<p><b> § 12 Modyfikacje</b></p>
				<p> 1. Medlanes ma prawo w każdej chwili do zmiany warunków wobec użytkowników ze skutkiem na przyszłość.</p>
				<p>2. Jeśli konieczna jest zmiana regulaminu, Medlanes zobowiązany jest poinformowac o tym  za pośrednictwem poczty elektronicznej na adres e-mail ostatnio używanym przez użytkownika. Zmiany wchodzą w życie w czasie 14 dni od wysłania informacji i uznaje sie je za zaakceptowane w przypadku braku odrzucenia ich przez użytkownika</p>
				<p>3. Jeśli użytkownik nie zgadza sie na zmiany wprowadzone do warunków i umowy, Medlanes GmbH zastrzega sobie prawo do rozwiązania umowy  bez powiadomienia . Jeżeli umowa trwa, uzgodnione wcześniej na warunki są nadal ważne. </p>
';
$lang['TERMS_13'] = '
				<p><b>§ 13 Płatnośći</b></p>
				<p>Medlanes obsługuje następujące opcje płatności. Niektóre opcje mogą nie być dostępne we wszystkich krajach:. Karta kredytowa, karta debetowa, SEPA, Paypal, Sofort, PayPal, w wybranych przelewem kraje</p>
                <p>Płatności są przetwarzane przez PayLane sp. z o który znajduje się w Gdańsku przy ul. Arkońska 6 / A3, kod pocztowy: 80-387, numer firmy:. 0000227278</p>
                <p>W przypadku wybrania subskrypcji Medlanes, okresowe opłąty dokonywane są przy pomocy  wybranej metody płatności / rachunku. Wybierając płatności oparty na subskrypcji, wyrażasz zgodę na pobieranie okresowych wypłat. Jeżeli za darmo oferowany jest okres testowy. Płatności za wybraną subskrypcje mogą  rozpocząć się automatycznie po zakończeniu okresu testowego. Informacja ta bedzię podana w trakcie wybierania usługi. By przerwać subsrypcję konieczne jest wysłanie maila na adres e-mail serwisu medlanes </p>
';
$lang['TERMS_14'] = '
				<p><b>§ 14 Wnioski</b></p>
				<p>Jeśli jakiekolwiek postanowienie niniejszych warunków, lub  przepisu jest całkowicie lub częściowo nieważne, ważność pozostałych postanowień pozostają nienaruszone.</p>
';


?>