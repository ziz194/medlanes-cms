<?php
/* 
------------------
Language: German
------------------
*/


$lang = array();
$lang['langde'] = 'class="langde"';




/// new landing page
$lang['signup_nb'] = "5466403";


$lang['nl-1'] = "Bequem, einfach und schnell</br> Qualifizierter medizinischer Rat</br>immer und überall!";
$lang['nl-a'] = "Lange Wartezeiten nerven.</br> Medmedo ist rund um die Uhr verfügbar.";
$lang['nl-2'] = "Bald verfügbar";
$lang['nl-3'] = '30 Tage</p><br /><p class="bigit-now">GELD-ZURÜCK-<br /> GARANTIE';
$lang['nl-4'] = '256-bit SSL</p><br /><p class="bigit-now">SECURITY';
$lang['nl-5'] = 'Konzept aus:';
$lang['nl-6'] = 'So kann Medmedo genutzt werden';
$lang['nl-7'] = "<b>Hochqualifizierte Ärzte </b> beantworten Ihre Fragen.";
$lang['nl-8'] = 'Bequem';
$lang['nl-9'] = 'Einfach';
$lang['nl-10'] = 'Günstig';
$lang['nl-11'] = 'Zeitsparend';
$lang['nl-12'] = "Qualifizierten Rat zu bekommen ist mit viel Stress verbunden! Im Internet finden Sie nur Halbwissen und müssen oft lange suchen um den richtigen Arzt zu finden. Medmedo steht Ihnen immer zur Verfügung! Rund um die Uhr und egal wo Sie gerade sind - auf Ihrem Smartphone und im Web.";
$lang['nl-13'] = "Medmedo ist einfach, nutzerfreudlich";
$lang['nl-14'] = "Auch in Deutschland ist Medizin teuer. Für den Staat, den Arbeitgeben und über die Steuern auch für Sie! Nicht nur die direkten Kosten sind relativ, auch die Opportunitätskosten sich wichtig - z.B. wenn Sie auf der Arbeit frei nehmen müssen um einen Termin wahr zu nehmen. Holen Sie sich einen erste kostengünstige Einschätzung über Medmedo!";
$lang['nl-15'] = "Oft wartet man viele Tage, oder sogar Wochen auf einen Termin bei einem Arzt. Und dann warten Sie meistens nochmal im Wartezimmer der Praxis. Medmedo ist 24 Stunden am Tag, 7 Tage die Woche verfügbar!";


$lang['nl-person1'] = '<p>“Ich war überrascht, dass ich einfach professionellen Rat bekommen kann.”</p>';
$lang['nl-person2'] = '<p>“Medmedo ist absolut praktisch! Medizinischer Rat während ich auf der Arbeit bin, ist für mich unbezahlbar.”</p>';
$lang['nl-person3'] = '<p>“Ich habe relativ oft Fragen zu meiner Gesundheit, ich nutze Medmedo um nicht immer zu Arzt rennen zu müssen”</p>';
$lang['nl-person4'] = '<p>“Hilfe von Experte zu einem guten Preis ist großartig für mich und meine Familie.”</p>';
$lang['nl-person5'] = "<p>“Als Mutter habe ich nicht immer Zeit bei kleinen Fragen zum Arzt zu gehen und 1-2 Stunden zu warten! Mit Medmedo kriege ich zeitnah Rat.”</p>";
$lang['nl-person6'] = "<p>“Ohne das Haus verlassen zu müssen bekomme ich medizinische Fragen beantwortet.”</p>";
$lang['nl-person7'] = "<p>“Vorallem auf Reise macht Medmedo für micht Sinn!”</p>";
$lang['nl-person8'] = "<p>“Wenn ich meine Symptome im Internet recherchiere sind widersprüchliche Informationen oft ein Problem. Ich bevorzuge sichere Infos vom einem Arzt!”</p>";
$lang['nl-person9'] = "					<p>“Ich fühle mich bei gesundheitlichen Probleme und Fragen gerne sicher! Medmedo hilft mir hier! Ich empfehle es gerne!”</p>
					";
$lang['nl-person10'] = "<p>“Ein außergewöhnlich guter Service, mit dem ich mir 1-2x im Monat Beratzung zu Gesundheitsfragen meiner Familie hole.”</p>
					";
$lang['nl-person11'] = "<p>“Am Wochenende ein Arzt zu finden ist meistens schwierig. Per App ist es deutlich leichter!”</p>
					";
$lang['nl-person12'] = "<p>“Für die einfachen Fragen reicht mir eine kurze Antwort und wenn nötig gehe ich dann zum Arzt.”</p>
					";
$lang['nl-person13'] = "<p>“Auf Reisen ist nutze ich Medmedo um einfachen Kontakt zu einem deutschen Arzt zu halten.”</p>
					";
$lang['nl-person14'] = "<p>“Mir hilft es, dass ich mit Medmedo theoretisch jederzeit und schnell Zugang zu Ärzten habe, die mir Rat zur Zeit stehen.”</p>
					";
$lang['nl-person15'] = "<p>“Computer und Apps sind zwar nicht so mein Ding, aber Medmedo nutze ich für mich und meine Familien mittlerweile fast regelmäßig.”</p>
					";
$lang['nl-person16'] = "<p>“Wir lieben Medmedo! So muss das Gesundheitssystem heutzutage funktionieren.”</p>
					";




// ADD QUESTION
// 3 STEPS

$lang['STEP1'] = 'Beschreiben Sie Ihre Symptome';
$lang['STEP2'] = 'Arztauswahl & Bezahlung';
$lang['STEP3'] = 'Qualifizerter medizinischer Rat';


//CTA

$lang['LinktoCTA'] ="add-question";
$lang['CTA'] = 'Qualifizierter medizinischer Rat - Überall, Jederzeit, 24/7';
$lang['BUTTON'] = 'Ja, Ich möchte jetzt einen Arzt fragen!';

// ADD QUESTION

$lang['ASK_TITLE'] = 'Qualifizierter medizinischer Rat - Überall, Jederzeit, 24/7';
$lang['ASK_DESCRIPTION'] = 'Fragen Sie unsere Ärzte nach Ihren Symptomen';	

$lang['signupclass'] = 'WFItem5862903';
$lang['signupcode'] = '5862903';
$lang['signuplink'] = 'http://app.getresponse.com/view_webform.js?wid=5862903&mg_param1=1&u=lACb';

$lang['signupclassppc2'] = 'WFItem6331303';
$lang['signupcodeppc2'] = '6331303';
$lang['signuplinkppc2'] = 'http://app.getresponse.com/view_webform.js?wid=6331303&mg_param1=1&u=lACb';

$lang['aq-1'] = 'Fragen Sie unsere Ärzte nach Ihren Symptomen';
$lang['aq-2'] = 'Bestimmen Sie das Problem';
$lang['aq-select'] = 'Wählen Sie anhand des Modells die betroffenen Körperteile  aus';
$lang['aq-describe'] = 'Beschreiben Sie Ihre Symptome';
$lang['aq-list'] = 'Bitte geben Sie ihre Symptome an (z.B. Bauchschmerzen, Übelkeit, Kopfschmerzen, Hautausschlag,…)';
$lang['aq-photo1'] = 'Laden Sie relevante Fotos hoch';
$lang['aq-photo2'] = 'Foto anhängen';
$lang['aq-answer'] = 'Haben Sie spezielle Fragen an unsere Ärzte?';
$lang['aq-ph'] = 'Stellen Sie Ihre Frage(n) hier';              
$lang['aq-confirm'] = 'Bestätigung';
$lang['aq-process'] = 'Wir werden einen gegeigneten Arzt für Sie finden und bearbeiten Ihre Anfrage so schnell wie möglich.';
$lang['aq-3'] = 'Vorderseite';
$lang['aq-4'] = 'Rückseite';
$lang['aq-5'] = 'Ihre Symptome';
$lang['aq-6'] = 'Mehr Informationen';
$lang['aq-7'] = 'Zusätzliche Informationen';
$lang['aq-age'] = 'Wählen Sie Ihr Alter:';
$lang['aq-gender'] = 'Geschlecht:';
$lang['aq-male'] = 'Männlich';
$lang['aq-female'] = 'Weiblich';
$lang['aq-8'] = 'Seit wann haben Sie die Symptome?';
$lang['aq-9'] = 'Wählen';
$lang['aq-10'] = 'Akut';
$lang['aq-11'] = 'Heute';
$lang['aq-12'] = 'Diese Woche';
$lang['aq-13'] = 'Dieser Monat';
$lang['aq-chronic'] = 'Chronisch';
$lang['aq-yq'] = 'Ihre Frage';
$lang['aq-experience'] = 'Haben Sie Unfälle/Verletzungen erlitten?';
$lang['aq-14'] = 'Ja';
$lang['aq-15'] = 'Nein';
$lang['aq-16'] = 'Welche Medikamente nehmen Sie zurzeit ein?';
$lang['aq-17'] = 'Zusätzliche Informationen Ihrer Krankengeschichte (z.B. Allergien, hoher Blutdruck, COPD, Asthma, u.s.w.)';
$lang['aq-18'] = 'Zusätzliche Anhänge';
$lang['aq-19'] = 'Bild(er) hochladen';
$lang['aq-20'] = 'Der passende Arzt wird Ihnen innerhalb von wenigen Stunden zugewiesen.';
$lang['aq-21'] = 'Absenden';
$lang['aq-add'] = 'Hinzufügen';
$lang['aq-name'] = 'Ihr Name';
$lang['aq-email'] = 'Ihre Email';
$lang['aq-button'] = 'Ich möchte jetzt medizinischen Rat erhalten!';




// Add question end


// Body parts 

$lang['aq-bp-1'] = 'Kopf';
$lang['aq-bp-2'] = 'Arm (Rechts)';
$lang['aq-bp-3'] = 'Brust (Rechts)';
$lang['aq-bp-4'] = 'Brust (Links)';
$lang['aq-bp-5'] = 'Arm(Links)';
$lang['aq-bp-6'] = 'Bauch (Rechts)';
$lang['aq-bp-7'] = 'Bauch (Links)';
$lang['aq-bp-8'] = 'Hand(Rechts)';
$lang['aq-bp-9'] = 'Schambereich';
$lang['aq-bp-10'] = 'Hand(Links)';
$lang['aq-bp-11'] = 'Oberschenkel (Rechts)';
$lang['aq-bp-12'] = 'Oberschenkel (Links)';
$lang['aq-bp-13'] = 'Unterschenkel (Rechts)';
$lang['aq-bp-14'] = 'Unterschenkel (Links)';
$lang['aq-bp-15'] = 'Fuß (Rechts)';
$lang['aq-bp-16'] = 'Fuß (Links)';

$lang['aq-bp-33'] = 'Oberer Rücken (Links)';
$lang['aq-bp-44'] = 'Oberer Rücken (Rechts)';
$lang['aq-bp-66'] = 'Unterer Rücken (Rechts)';
$lang['aq-bp-77'] = 'Unterer Rücken (Links)';
$lang['aq-bp-99'] = 'Steiß';

// Body parts end


// sign up start
$lang['sup-class1'] ='style="width:95px !important;"';
$lang['sup-class2'] ='style="width:92px !important;"';
$lang['sup-form1'] ='Vorname';
$lang['sup-form2'] ='Nachname';
$lang['sup-form3'] ='eMail';
$lang['sup-form4'] ='Passwort';
$lang['sup-1'] = 'Erstellen Sie Ihren privaten Account';
$lang['sup-2'] = 'Alle Informationen und Fragen sind privat';
$lang['sup-3'] = 'Bitte geben Sie Ihren Vornamen an';
$lang['sup-4'] = 'Bitte geben Sie Ihren Nachnamen an';
$lang['sup-5'] = 'Bitte geben Sie Ihre eMail an';
$lang['sup-6'] = 'Bitte geben Sie Ihr Passwort an';
$lang['sup-7'] = 'Bitte geben Sie Ihr Alter an';
$lang['sup-8'] = 'Bitte Altersbereich wählen';
$lang['sup-9'] = 'Bitte wählen Sie Ihr Geschlecht aus';
$lang['sup-10'] = 'Männlich';
$lang['sup-11'] = 'Weiblich';
$lang['sup-12'] = 'Optional';
$lang['sup-13'] = 'You must accept the Terms and Conditions and the Privacy Policy.';
$lang['sup-14'] = 'Ich habe die <a class="fancybox fancybox.iframe" href="terms-and-conditions.html" >Allgemeinen Geschäftsbedingungen</a> gelesen und stimme zu.
';
$lang['sup-15'] = 'Ich habe die <a class="fancybox fancybox.iframe" href="privacy-statement.html">Datenschutzbestimmungen </a> gelesen und stimme zu.';
$lang['sup-16'] = 'Account erstellen / Frage speichern';


// sign up end


// top-banners start

$lang['tb-1'] = 'Beratung von qualifizierten Ärzten.';
$lang['tb-2'] = 'Von überall und rund um die Uhr Für pauschal 24€ 
';

// top-banners end

$lang['LOGO_HOME'] = "http://Medmedo.com/";
$lang['LOGO_HOME_TEST'] = "http://Medmedo.com/ask/index";
$lang['LOGO_HOME_TEST2'] = "http://Medmedo.com/ask/index1";

$lang['PAGE_TITLE'] = 'Medmedo - Ihr Doktor in der Hosentasche - mobile & online';
$lang['HEADER_TITLE'] = 'My website header title';
$lang['SITE_NAME'] = 'My Website';
$lang['SLOGAN'] = 'My slogan here';
$lang['HEADING'] = 'Heading';

// Header----------------------

$lang['HEADER_LOGO'] = '<a class="logo" href="http://medmedo.de/">Medmedo</a>';

$lang['HEADER_HOME'] = '<a href="index">Home</a>';
$lang['HEADER_SERVICE'] = "<a href=service>Unser Service</a>";
$lang['HEADER_DOCTORS'] = "<a href=aerzte>Ärzte</a>";
$lang['HEADER_ASK'] = '<a class="last" href="add-question">Arzt Fragen</a>';
$lang['HEADER_TREAT'] = "<a href='#'>Krankheiten ";


$lang['HEADER_ALLERGIE'] = "Allergien";
$lang['HEADER_SLEEP_DISORDER'] = "Schlafstörungen";
$lang['HEADER_COLD'] = "Erkältung, Husten, Grippe";
$lang['HEADER_DIABETES'] = "Diabetes";
$lang['HEADER_DIGESTIVE'] = "Verdauungsstörungen";


$lang['HEADER_EYE'] = "Augenerkrankungen";
$lang['HEADER_EAR'] = "Ohrenschmerzen";
$lang['HEADER_MENTAL'] = "Psychische Erkrankungen";
$lang['HEADER_SEXUAL'] = "Sexualität";
$lang['HEADER_DIET'] = "Ernährung";
$lang['HEADER_INFECTION'] = "Infektionskrankheiten";
$lang['HEADER_PAIN'] = "Schmerzen";
$lang['HEADER_SKIN'] = "Haare, Haut und Nägel" ;

$lang['HEADER_ARITHIS'] = "Arthritis" ;
$lang['HEADER_COPD'] = "Asthma und COPD" ;
$lang['HEADER_CHILDREN'] = "Kinderkrankheiten" ;
$lang['HEADER_ENDOCRINE'] = "Hormonelle Störungen" ;
$lang['HEADER_INJURIES'] = "Verletzungen" ;
$lang['HEADER_MEN'] = "Männerkrankheiten" ;
$lang['HEADER_ORAL'] = "Zahnerkrankungen" ;
$lang['HEADER_RESPIRATORY'] = "Atemwegserkrankungen" ;
$lang['HEADER_STD'] = "Geschlechtskrankheiten" ;
$lang['HEADER_WOMEN'] = "Frauenkrankheiten" ;


$lang['ASK'] =" Fragen Sie einen Arzt";



$lang['HEADER_FAQ'] = "<a href=faq>FAQ</a>";
$lang['HEADER_CONTACT'] = "<a href=kontakt>Kontakt</a>";

$lang['HEADER_SIGNUP'] = '<a class="last" href="add-question.php?lang=en">Sign Up</a>';


// Footer-------------------

$lang['FOOTER_HOME'] = '<a href="http://medmedo.de">Home</a>';
$lang['FOOTER_ABOUT'] = '<a href="ueber-medmedo">Über Medmedo </a>';
$lang['FOOTER_FAQ'] = "<a href=faq>FAQ</a>";
$lang['FOOTER_CONTACT'] = "<a href=kontakt>Kontakt</a>";
$lang['FOOTER_CAREER'] = '<a href="career">Karriere</a>';

$lang['FOOTER_SIGNUP'] = '<a class="last" href="add-question.php?lang=en">Sign up</a>';

$lang['FOOTER_ASK'] = '<a class="last" href="add-question">Arzt Fragen</a>';
$lang['FOOTER_DOCTORS'] = "<a href=aerzte>Ärzte</a>";
$lang['FOOTER_SERVICE'] = "<a href=service>Unser Service</a>";

$lang['FOOTER_TERMS'] = '<a href="agb">AGBs</a>';
$lang['FOOTER_PRIVACY'] = '<a href="agb">Datenschutzbestimmungen</a>';
$lang['FOOTER_SECURITY'] = '<a href="privacy">Datensicherheit</a>';




$lang['FOOTER_SUPPORT'] = 'Unterstützt von :';
$lang['FOOTER_COPYRIGHT'] = 'Copyright © Medlanes GmbH  2014  Alle Rechte vorbehalten';
$lang['FOOTER_IMPRINT'] = '<a href="impressum">Impressum</a>';

// Home Page--------------------

$lang['HOME_PAGE_TITLE'] = 'Qualifizierter medizinischer Rat - Überall, Jederzeit, 24/7';
$lang['HOME_PAGE_DESCRIPTION'] = 'Fragen Sie unsere Ärzte nach Ihren Symptomen';	

$lang['HOME_PAGE_banner-sliderone_content']="<h1>Ihr Arzt in der Hosentasche</h1>
				<h2>Rat von Spezialisten für einmalig €24,-</h2>
				<a href='#'> Bald verfügbar</a>";
	

$lang['HOME_PAGE_banner-slidertwo_content']="<h1>Medizinischer Rat&nbsp;<br>&nbsp; immer verfügbar&nbsp;<br></h1>
				<h2>24/7 egal&nbsp;<br>&nbsp; wo Sie gerade sind&nbsp;</h2>
				<a href='#'> Bald verfügbar</a>";
				

				
				
				
$lang['HOME_PAGE_banner-sliderthree_content']="<h1>Medizinischer Rat&nbsp;<br>&nbsp; immer verfügbar&nbsp;<br></h1>
				<h2>24/7 egal&nbsp;<br>&nbsp; wo Sie gerade sind&nbsp;</h2>
				<a href='#'> Bald verfügbar</a>";
				
$lang['SIGNUP']="Sign Up";
$lang['EMAIL_VALIDATION']="Email-Adresse ist ungültig";
$lang['EMAIL_PLACEHOLDER']="Mit Email starten";

				
$lang['HOME_PAGE_l-one']="Zertifiziert";
$lang['HOME_PAGE_l-two']="256-bit SSL";




$lang['HOME_PAGE_Concept']="Konzept bekannt aus:";
$lang['HOME_PAGE_widtitle1']="Warum Medmedo";
$lang['HOME_PAGE_widcont1']="Vertrauenswürdige Antworten zu medizinischen Problemen zu finden ist schwer. Vor allem wenn man im Urlaub ist, an Wochenende oder einfach wenig Zeit hat. Mit Medmedo haben Sie Ihren Doktor auf dem Smartphone.";

$lang['HOME_PAGE_widtitle2']="Kurzvorstellung";
$lang['HOME_PAGE_widcont2']="Beschreiben Sie online oder mit der App einfach kurz Ihre Symptome oder Ihre Frage - Medmedo findet den richtigen Arzt für Sie, der Ihnen umgehend antwortet.";

$lang['HOME_PAGE_widtitle3']="Überall verfügbar";
$lang['HOME_PAGE_widcont3']="Medmedo ist immer und überall erreichbar! Im Urlaub, in der Nacht, am Wochenende, auf Ihrem Handy und online. Testen Sie es.";

$lang['HOME_PAGE_bluebox1_text1']="Support";
$lang['HOME_PAGE_bluebox1_text2']="0800 - 765 43 43";
$lang['HOME_PAGE_bluebox1_text3']="gebührenfrei aus dem dt. Festnetz";
$lang['HOME_PAGE_bluebox1_text4']="E-Mail";

$lang['HOME_PAGE_bluebox2_text1']="Zertifiziert & getestet";
$lang['HOME_PAGE_bluebox2_text2']="Qualität und Verlässlichkeit verschrieben.";
$lang['HOME_PAGE_bluebox2_text3']="<a href='about-us'>Über uns</a>";

$lang['HOME_PAGE_bluebox3_text1']="Umfangreiche Medizin";
$lang['HOME_PAGE_bluebox3_text2']="Hausärzte & Spezialisten bei Medmedo";
$lang['HOME_PAGE_bluebox3_text3']="<a href='our-doctors'>Unsere Ärzte</a>";




// Our Service Page--------------------
$lang['SERVICE_TITLE'] = 'Qualifizierter medizinischer Rat - Überall, Jederzeit, 24/7';
$lang['SERVICE_DESCRIPTION'] = 'Fragen Sie unsere Ärzte nach Ihren Symptomen';	

$lang['SERVICE_HEADLINE']="Ein Arzt in der Tasche, überall und jederzeit!";

     // Menu--------------------
$lang['SERVICE_MENU_HOW']="So funktioniert's";
$lang['SERVICE_MENU_WHY']="Warum Medmedo?";
$lang['SERVICE_MENU_USECASES']="Nutzungsbeispiele";
$lang['SERVICE_MENU_PRICE']="Preis & Empfehlungen";

$lang['SERVICE_BUTTON1']="Erhalten Sie jetzt medizinischen Rat!";

     // How Medmedo works--------------------
$lang['SERVICE_HOW_HEADLINE']="So funktioniert Medmedo";
$lang['SERVICE_HOW_TEXT']="Mit Medmedo werden Sie von zertifizierten Allgemein- oder Fachärzten beraten. Erhalten Sie qualifizierten, medizinischen Rat innerhalb von 24 Stunden in nur drei einfachen Schritten. Senden Sie uns Ihre Symptome und Fragen über unser Webformular, wählen Sie einen der vorgeschlagenen Ärzte und bezahlen Sie bequem per PayPal. Unsere Ärzte werten Ihre Daten aus und senden Ihnen innerhalb von 24 Stunden eine Antwort. Ergeben sich daraus weitere Fragen, beantworten wir diese natürlich gerne.";

$lang['SERVICE_HOW_DESCRIBE_h']="Beschreiben Sie Ihre Symptome"; 
$lang['SERVICE_HOW_DESCRIBE_t']="Beschreiben Sie Ihre Symptome und stellen Sie Ihre Fragen einfach über unseren Online-Fragebogen oder über unsere mobile App. Sie haben auch die Möglichkeit, Bilder hochladen."; 

$lang['SERVICE_HOW_CHOOSE_h']="Doktorauswahl & Bezahlung"; 
$lang['SERVICE_HOW_CHOOSE_t']="Wählen Sie einen der vorgeschlagenen Ärzte aus Ihrer Region und bezahlen Sie die Gebühr von nur 24€. Konnte Ihnen unser Arzt nicht helfen, bekommen Sie Ihr Geld zurück."; 

$lang['SERVICE_HOW_ADVICE_h']="Qualifizierter medizinischer Rat"; 
$lang['SERVICE_HOW_ADVICE_t']="Ein qualifizierter und zertifizierter Arzt wird Ihre Fragen innerhalb von 24 Stunden beantworten. Holen Sie sich eine Expertenmeinung auf Knopfdruck!"; 


          // Why use Medmedo--------------------
$lang['SERVICE_tab-title']="Mit Medmedo, weniger...";

$lang['SERVICE_tab-1']="Warten";
$lang['SERVICE_tab-1_content']="Warten Sie nicht länger auf Ihren Arzttermin oder im Wartezimmer. Mit Medmedo erhalten Sie medizinische Beratung 24 Stunden am Tag, 7 Tage die Woche.";


$lang['SERVICE_tab-2']="Zahlen";
$lang['SERVICE_tab-2_content']="Durch eine qualifizierte, medizinische Beratung können indirekte Kosten entstehen. Mit Medmedo sparen Sie Sie sich unnötige Wege zum Arzt und verlieren keine Arbeitszeit.";

$lang['SERVICE_tab-3']="Stress";
$lang['SERVICE_tab-3_content']="Medizinische Beratung durch Medmedo ist bequem! Sparen Sie sich den Aufwand, Ihren Arzttermin Wochen im Voraus zu planen. Medmedo is überall und jederzeit verfügbar - auf Ihrem Smartphone oder im Web.";

$lang['SERVICE_tab-4']="Sorgen";
$lang['SERVICE_tab-4_content']="Machen Sie sich keine Sorgen mehr über Ihre Gesundheit! Wenden Sie sich direkt an unsere Ärzte - auch bei Gesundheitsfragen zu Ihrer Familie. Mit weniger Sorgen gesünder leben und weniger krank sein!";



$lang['SERVICE_someFact-title']="Warum Medmedo?";


$lang['SERVICE_someFact-fact1']="8 min.";
$lang['SERVICE_someFact-fact1-cont']="Durchschnittliche Zeit, die ein Arzt für Sie hat.";

$lang['SERVICE_someFact-fact2']="14 Tage";
$lang['SERVICE_someFact-fact2-cont']="So lange warten Sie im Durchschnitt auf einen Termin.";

$lang['SERVICE_someFact-fact3']="3 Std.";
$lang['SERVICE_someFact-fact3-cont']="Die Zeit, die Sie sich für einen Termin inkl. Anreise nehmen müssen.";

$lang['SERVICE_someFact-fact4']="80%";
$lang['SERVICE_someFact-fact4-cont']="Anteil der Fragen, die man in manchen Fachgebieten auch online beantworten kann.";

$lang['SERVICE_someFact-fact5']="50%";
$lang['SERVICE_someFact-fact5-cont']="Anteil der Patienten, die von einem besseren Rat profitieren würden.";

            // Use Cases--------------------
$lang['SERVICE_benefits-title']="Nutzungsbeispiele";

$lang['SERVICE_benefits-msg']="Medmedo ist immer und überall für Sie verfügbar. Wir kombinieren die Einfachheit einer Onlinesuche mit der Qualität und Verlässlichkeit eines Besuches beim Arzt. Laden Sie sich die Medmedo App ganz einfach herunter oder nutzen Sie die Webversion. Sie stellen innerhalb von Minuten Ihre Fragen, laden ein Foto hoch und beantworten einige Rückfragen. Medmedo findet den richtigen Arzt für Sie und dieser antwortet in Kürze!";
        
$lang['SERVICE_benefits-benifit1_title']="Im Urlaub";
$lang['SERVICE_benefits-benifit1_content']="Im Ausland ist es oft schwer einen Arzt zu finden, der gut englisch oder deutsch spricht. Nutzen Sie Medmedo!";

$lang['SERVICE_benefits-benifit2_title']="Bei der Arbeit";
$lang['SERVICE_benefits-benifit2_content']="Verlieren Sie keine kostbare Arbeitszeit und holen Sie sich einen ersten Rat online.";

$lang['SERVICE_benefits-benifit3_title']="In der Nacht";
$lang['SERVICE_benefits-benifit3_content']="Medmedo ist auch in der Nacht verfügbar. Stellen Sie einfach Ihre Fragen und ein Arzt antwortet umgehend.";

$lang['SERVICE_benefits-benifit4_title']="Am Wochenende";
$lang['SERVICE_benefits-benifit4_content']="Praktisch alle Ärzte haben am Wochenende geschlossen! Medmedo beantwortet Ihre Fragen 24/7.";

$lang['SERVICE_benefits-benifit5_title']="Wenn's einfach gehen muss";
$lang['SERVICE_benefits-benifit5_content']="Nutzen Sie Ihr Smartphone oder Ihren PC und warten Sie nicht lange auf Antworten.";

$lang['SERVICE_benefits-benifit6_title']="Bei langen Entfernungen";
$lang['SERVICE_benefits-benifit6_content']="Vor allem auf dem Land ist der Arzt oft schwer zu erreichen. Nutzen Sie Medmedo für einfache Fragen!";



$lang['SERVICE_welcome_cont']="Vertraueswürdige Antworten zu medizinischen Problemen zu finden, ist schwer. Vor allem im Urlaub, am Wochenende oder während der Arbeit. Mit Medmedo haben Sie Ihren Doktor auf dem Smartphone - egal wo Sie sind und zu welcher Uhrzeit. Mit der Medmedo App oder über die Webseite erhalten Sie jederzeit qualifizierte Informationen zu medizinischen Fragen. Gegründet von Emil Kendziorra (einem Arzt) und Erik Stoffregen, greift Medmedo auf jahrelange Expertise aus IT und Medizin zurück und ermöglicht Patienten Zugang zu ärztlichem Rat - schnell, sicher und bequem, von überall auf der Welt zu erreichen!
";

$lang['SERVICE_title1']="Ihre Gesundheit im Blick - Einfach und schnell.";

$lang['SERVICE_content1']="Beschreiben Sie online oder mit der App einfach kurz Ihre Symptome oder Ihre Frage - Medmedo findet den richtigen Arzt für Sie, der Ihnen umgehend antwortet. Sie erhalten medizinischen Rat am Wochenende, im Urlaub, in der Nacht, während der Arbeit und zu jeder anderen Zeit.";

$lang['SERVICE_title2']="Sicher und vertraulich per Smartphone oder Web";

$lang['SERVICE_content2']="Alle Ärzte haben langjährige Erfahrungen in Ihren Spezialgebieten und sind natürlich an die ärztliche Schweigepflicht gebunden. Darüber hinaus werden alle Daten ausschließlich auf deutschen Servern gespeichert. Ein Missbrauch Ihrer Daten ist daher ausgeschlossen.";

           // Price--------------------
$lang['SERVICE_PRICE_HEADLINE']="Preis & Empfehlungen";	
$lang['SERVICE_PRICE_TEXT']="Medmedo ist kostensparend! Durch eine qualifizierte, medizinische Beratung können indirekte Kosten entstehen. Mit Medmedo sparen Sie Sie sich unnötige Wege zum Arzt und verlieren keine Arbeitszeit. Medmedo macht Medizin für jedermann bezahlbar!";	   

$lang['SERVICE_PRICE_BOX_ADVICE']="Qualifizierte Beratung";	
$lang['SERVICE_PRICE_BOX_TEXT']="Konsultationen per Email inkl. bis zu drei weitere Fragen an unsere zertifizierten Allgemein- oder Fachärzte aus Deutschland.";	 
$lang['SERVICE_PRICE_BOX_PRICE']="€24";	 
      
		  // Testimonials--------------------
$lang['SERVICE_TESTIMONIALS_HEADLINE']="Empfehlungen";


// Doctors Page--------------------

$lang['DOCTORS_TITLE'] = 'Qualifizierter medizinischer Rat - Überall, Jederzeit, 24/7';
$lang['DOCTORS_DESCRIPTION'] = 'Fragen Sie unsere Ärzte nach Ihren Symptomen';	

$lang['DOCTORS_HEADLINE']="Ärzte jeder <br> Fachrichtung verfügbar rund um die Uhr.";

$lang['DOCTORS_title1']="Unsere Ärzte sind für Sie da.";
$lang['DOCTORS_cont1']="Medmedo arbeitet ausschließlich mit qualifizierten und lizensierten Ärzten zusammen. Zusätzlich werden all unsere Partnerärzte einer umfassenden Qualitäts- und Sicherheitsprüfung unterzogen. Mit Hilfe von sorgfältig entwickelten Algorithmen verbinden wir Sie mit Ihrem persönlichen Arzt.";

$lang['DOCTORS_title2']="We're currently building our base";
$lang['DOCTORS_cont2']="Medmedo only works with fully licensed and qualified doctors. Your &quot;online/mobile doctor&quot; will be matched to your specific query and will be an expert in the field. Medmedo is designed to help you with  medical queries, advice and dealing with chronic diseases. ";

$lang['DOCTORS_EXPERIENCE_HEADLINE']="Erfahrung";
$lang['DOCTORS_WHY_HEADLINE']="Warum Medmedo?";
$lang['DOCTORS_EDUCATION_HEADLINE']="Ausbildung";
$lang['DOCTORS_HIGHLIGHTS_HEADLINE']="Beruflicher Werdegang";
$lang['DOCTORS_CERTIFICATION_HEADLINE']="Zertifikate";
             //Doctor Slider -------------------------
             //Doctor 1
$lang['DOCTORS_NAME1']="Dr. Marion Nolte";
$lang['DOCTORS_FIELD1']="Pädiatrie";
$lang['DOCTORS_IMAGE1']="<img src=\"images/pediatrics.png\" width=\"45px\" height=\"45px\" alt=\"pediatrics\" />";
$lang['DOCTORS_PORTRAIT1']="<img src=\"images/docs/Doctor5.png\"/>";
$lang['DOCTORS_REGION1']="Offenbach";
$lang['DOCTORS_EXPERIENCE_TEXT1']="Krankenhaus (7 Jahre)";
$lang['DOCTORS_WHY_TEXT1']="Viele Eltern denken, sie liegen mit der Selbstdiagnose ihrer Kinder richtig. Die Selbstdiagnose kann jedoch irreführend und gefährlich sein, insbesondere bei Kleinkindern. Mit Medmedo bin ich jederzeit erreichbar, um eine zuverlässige und qualifizierte Auskunft zu geben.";
$lang['DOCTORS_EDUCATION_TEXT1']="Studium der Medizin, Johann Wolfgang Goethe Universität Frankfurt am Main</p>
          <p>Facharzt für Kinder- und Jugendmedizin, Landesärztekammer Hessen";
$lang['DOCTORS_HIGHLIGHTS_TEXT1']="Ärztin im Praktikum in Pädiatrischen Radiologie und Kinderchirurgie</p>
		  <p>Assistenzärztin mit Schwerpunkt Pädiatrische Kardiologie, Pädiatrische Intensivmedizin, Neonatologie";		  
$lang['DOCTORS_CERTIFICATION_TEXT1']="Fachkunde im Strahlenschutz für Ärzte, Notfalldiagnostik und Röntgenthorax</p>
          <p>Fachkunde Rettungsdienst</p>
          <p>Notfallmedizin";
		  
		     //Doctor 2
$lang['DOCTORS_NAME2']="Dr. David H. Mbia";
$lang['DOCTORS_FIELD2']="Diätetik";
$lang['DOCTORS_IMAGE2']="<img src=\"images/dietetics.png\" width=\"45px\" height=\"45px\" alt=\"dietetics\" />";
$lang['DOCTORS_PORTRAIT2']="<img src=\"images/docs/Doctor6.png\"/>";
$lang['DOCTORS_REGION2']="Solingen";
$lang['DOCTORS_EXPERIENCE_TEXT2']="Humanitäre Hilfe (3 Jahre)</p>
                                      <p>Privatpraxis (2 Jahre)</p>";
$lang['DOCTORS_WHY_TEXT2']="Ich bin ein Verfechter der Medizinethik und überzeugt von der Idee, qualifizierte medizinische Beratung für jedermann zugänglich zu machen.";
$lang['DOCTORS_EDUCATION_TEXT2']="Dualers Studium Diätetik Hochschule Fula, Universitätsklinikum Gießen</p>
          <p>Doktor der Dietätik und Ernährung, McGill University, School of Dietetics and Human Nutrition, Quebec, Kanada </p>";
$lang['DOCTORS_HIGHLIGHTS_TEXT2']="<p>Niedergelassener Facharzt für Ernährungsmedizin</p>
			<p>Ernährungsberater Deutsche Gesellschaft für Ernährung e.V.</p>";		  
$lang['DOCTORS_CERTIFICATION_TEXT2']="Zertifizierung des Verbands für Ernährung und Diätetik e. V. (VFED)</p>";
		  
		    //Doctor 3
$lang['DOCTORS_NAME3']="Dr. Christiane Funke";
$lang['DOCTORS_FIELD3']="Gastroenterologie";
$lang['DOCTORS_IMAGE3']="<img src=\"images/gastroenterology.png\" width=\"45px\" height=\"45px\" alt=\"gastroenterology\" />";
$lang['DOCTORS_PORTRAIT3']="<img src=\"images/docs/Doctor8.png\"/>";
$lang['DOCTORS_REGION3']="Chicago, IL";
$lang['DOCTORS_EXPERIENCE_TEXT3']="Krankenhaus (6 Jahre)</p>
                                      <p>Privatpraxis (10 Jahre)</p>";
$lang['DOCTORS_WHY_TEXT3']="Als Fachärztin weiß ich, wie mühsam es ist, kurzfristig einen Termin zu bekommen. Online-Konsultationen sind zeitsparend sowohl für Patienten als auch für uns Ärzte.";
$lang['DOCTORS_EDUCATION_TEXT3']="Studium der Medizin, Medizinischen Fakultät der CAU, Kiel</p>
          <p>Facharzt für Innere Medizin, 1. Medizinischen Klinik des Universitätsklinikums Schleswig-Holstein</p>";
$lang['DOCTORS_HIGHLIGHTS_TEXT3']="Klinische Tätigkeit (Gastroenterologie, Nephrologie und Intensivmedizin mit Kardiologie)</p>
                        <p>Niedergelassene Fachärztin für Innere Medizin mit den Schwerpunkten Gatroenterologie und Proktologie</p>";		  
$lang['DOCTORS_CERTIFICATION_TEXT3']="Weiterbildungsermächtigung für Innere Medizin, Gastroenterologie und Proktologie</p>";
		  
		  		    //Doctor 4
$lang['DOCTORS_NAME4']="Dr. Daniel Geyer";
$lang['DOCTORS_FIELD4']="Pädiatrie";
$lang['DOCTORS_IMAGE4']="<img src=\"images/pediatrics.png\" width=\"45px\" height=\"45px\" alt=\"pediatrics\" />";
$lang['DOCTORS_PORTRAIT4']="<img src=\"images/docs/Doctor7.png\"/>";
$lang['DOCTORS_REGION4']="Augsburg";
$lang['DOCTORS_EXPERIENCE_TEXT4']="Krankenhaus(8 Jahre)";
$lang['DOCTORS_WHY_TEXT4']="Patientenzustände sind verschieden und verändern sich ständig. Daher ist eine regelmäßige Überwachung unumgänglich. Medmedo macht dies möglich.";
$lang['DOCTORS_EDUCATION_TEXT4']="Studium der Medizin, Universität Leipzig, Leipzig</p>
          <p>Facharzt für Kinder- und Jugendmedizin, Universitätskinderklinik Leipzig, Leipzig</p>";
$lang['DOCTORS_HIGHLIGHTS_TEXT4']="Klinische Koordination des erweiterten sächsischen Neugeborenenscreenings</p>
                        <p>Subspezialisierung zum pädiatrischen Endokrinologen und Diabetologen</p>
						<p>Ausbildung am Institut für Laboratoriumsmedizin der Universität Leipzig zur Durchführung fachbezogener Labordiagnostik
</p>";		  
$lang['DOCTORS_CERTIFICATION_TEXT4']="Labormedizin</p>
          <p>Pädiatrische Ernährungsmedizin</p>";
		  
		  		   //Doctor 5
$lang['DOCTORS_NAME5']="Dr. Thomas Pfeiffer";
$lang['DOCTORS_FIELD5']="Hausarzt";
$lang['DOCTORS_IMAGE5']="<img src=\"images/family_medicine.png\" width=\"45px\" height=\"45px\" alt=\"family medicine\" />";
$lang['DOCTORS_PORTRAIT5']="<img src=\"images/docs/Doctor1.png\"/>";
$lang['DOCTORS_REGION5']="Dallas, TX";
$lang['DOCTORS_EXPERIENCE_TEXT5']="Krankenhaus (4 Jahre)</p>
                                      <p>Privatpraxis (17 years)";
$lang['DOCTORS_WHY_TEXT5']="Ich habe Medmedo gewählt, weil ich überzeugt bin, dass neue Technologien meinen medizinischen Alltag vereinfachen können. Mithilfe von Online-Konsultationen erhalte ich eine interdisziplinäre Übersicht von meinen Patienten, dies ermöglicht genauere Diagnosen.";
$lang['DOCTORS_EDUCATION_TEXT5']="Studium der Medizin, Heinrich-Heine-Universität zu Düsseldorf, Düsseldorf</p>
          <p>Facharzt für Innere Medizin, Ärztekammer Westfalen-Lippe</p>";
$lang['DOCTORS_HIGHLIGHTS_TEXT5']="Oberarzt der St. Mauritius Therapieklinik Meerbusch, Abteilung Geriatrie</p>
                                         <p>Niedergelassener Facharzt für Innere Medizin";		  
$lang['DOCTORS_CERTIFICATION_TEXT5']="Palliativmedizin</p>
                                         <p>Verkehrsmedizin</p>
										 <p>Naturheilverfahren</p>
										 <p>Chirotherapie</p>";
		  
		  		   //Doctor 6
$lang['DOCTORS_NAME6']="Dr. Gerhard Walther";
$lang['DOCTORS_FIELD6']="Dermatologie";
$lang['DOCTORS_IMAGE6']="<img src=\"images/dermatology.png\" width=\"45px\" height=\"45px\" alt=\"dermatology\" />";
$lang['DOCTORS_PORTRAIT6']="<img src=\"images/docs/Doctor4.png\"/>";
$lang['DOCTORS_REGION6']="Nürnberg";
$lang['DOCTORS_EXPERIENCE_TEXT6']="Forschung (8 Jahre)</p>
                                       <p>Krankenhaus (6 Jahre)</p>
                                      <p>Privatpraxis (21 Jahre)";
$lang['DOCTORS_WHY_TEXT6']="Ich bin nun mehr als 25 Jahre im Gesundheitswesen tätig. Medmedo bietet die einmalige Möglichkeit, meine Erfahrungen auf breitem Feld zu teilen. So kann ich behaupten, dass ich einen wertvollen Beitrag zur Gesellschaft leiste.";
$lang['DOCTORS_EDUCATION_TEXT6']="Studium der Medizin, Universität Erlangen-Nürnberg, Erlangen</p>
          <p>Facharzt für Dermatologie, Hautklinik des Universitätsklinikum Erlangen</p>";
$lang['DOCTORS_HIGHLIGHTS_TEXT6']="Oberarzt an der Universitäts-Hautklinik Erlangen</p>
                        <p>Niedergelassener Facharzt für Haut- und Venenerkrankungen</p>";	  
$lang['DOCTORS_CERTIFICATION_TEXT6']="Phlebologie</p>";
		  
		     //Doctor 7
$lang['DOCTORS_NAME7']="Dr. Elisabeth Böttcher";
$lang['DOCTORS_FIELD7']="Hausärztin";
$lang['DOCTORS_IMAGE7']="<img src=\"images/family_medicine.png\" width=\"45px\" height=\"45px\" alt=\"family medicine\" />";
$lang['DOCTORS_PORTRAIT7']="<img src=\"images/docs/Doctor2.png\"/>";
$lang['DOCTORS_REGION7']="Düren";
$lang['DOCTORS_EXPERIENCE_TEXT7']="Privatpraxis (16 Jahre)";
$lang['DOCTORS_WHY_TEXT7']="Mithilfe von Medmedo kann ich die Fragen jener Patienten beantworten, die ortsgebunden sind oder altersbedingt meine Praxis nicht mehr aufsuchen können.";
$lang['DOCTORS_EDUCATION_TEXT7']="Studium der Medizin, RWTH Aachen, Aachen</p>
          <p>Facharzt für Allgemeinmedizin, RWTH Aachen</p>";
$lang['DOCTORS_HIGHLIGHTS_TEXT7']="Assistenzärztin mit Schwerpunkt Innere Medizin, Geriatrie</p>
          <p>Niedergelassene Fachärztin für Allgemeinmedizin</p>";		  
$lang['DOCTORS_CERTIFICATION_TEXT7']="Akupunktur</p>
          <p>Palliativmedizin</p>";
		  
		      //Doctor 8
$lang['DOCTORS_NAME8']="Dr. Richard Krieger";
$lang['DOCTORS_FIELD8']="Hausarzt";
$lang['DOCTORS_IMAGE8']="<img src=\"images/family_medicine.png\" width=\"45px\" height=\"45px\" alt=\"family medicine\" />";
$lang['DOCTORS_PORTRAIT8']="<img src=\"images/docs/Doctor3.png\"/>";
$lang['DOCTORS_REGION8']="Wuppertal";
$lang['DOCTORS_EXPERIENCE_TEXT8']="Privatpraxis (8 Jahre)";
$lang['DOCTORS_WHY_TEXT8']="Viele meiner Patienten sind regelmäßig außerhalb Deutschlands unterwegs. Online-Konsultationen ermöglichen Ihnen, von überall denselben Arzt aufzusuchen.";
$lang['DOCTORS_EDUCATION_TEXT8']="Studium der Medizin, Medizinischen Hochschule Hannover, Hannover</p>
          <p>Facharzt für Innere Medizin, Bethesda Krankenhaus,  Wuppertal</p>";
$lang['DOCTORS_HIGHLIGHTS_TEXT8']="Assistenarzt mit Schwerpunkt Unfall-Chirurgie, Thorax-Chirurgie, Abdominal Chirurgie, Plastische Chirurgie</p>
           <p>Niedergelassener Facharzt für Allgemeinmedizin</p>";				  
$lang['DOCTORS_CERTIFICATION_TEXT8']="Reisemedizinische Gesundheitsberatung</p>";
		  
		  	      //Doctor 9
$lang['DOCTORS_NAME9']="Dr. Linda Hoffmann";
$lang['DOCTORS_FIELD9']="Gynäkologie";
$lang['DOCTORS_IMAGE9']="<img src=\"images/gynecology.png\" width=\"45px\" height=\"45px\" alt=\"gyeocology\" />";
$lang['DOCTORS_PORTRAIT9']="<img src=\"images/docs/Doctor10.png\"/>";
$lang['DOCTORS_REGION9']="Berlin";
$lang['DOCTORS_EXPERIENCE_TEXT9']="Privatpraxis(27 Jahre)";
$lang['DOCTORS_WHY_TEXT9']="Viele Patieten vermeiden den direkten ärztlichen Kontakt, wenn es um diskrete medizinische Fragen geht. Mit Medmedo ist eine anonyme medizinische Beratung möglich.";
$lang['DOCTORS_EDUCATION_TEXT9']="Studium der Medizin, Johann Wolfgang von Goethe Universität, Frankfurt am Main</p>
          <p>Fachärztin für Frauenheilkunde und Geburtshilfe, Krankenhaus Am Urban, Berlin</p>";
$lang['DOCTORS_HIGHLIGHTS_TEXT9']="Assistenzärztin mit Schwerpunkt Innere Medizin, Chirurgie, Gynäkologie und Geburtshilfe</p>
          <p>Niedergelassener Fachärztin für Gynäkologie</p>";	  
$lang['DOCTORS_CERTIFICATION_TEXT9']="Kinder- und Jugendgynäkologie</p>
          <p>Naturheilverfahren</p>";
		  
		  		  	      //Doctor 10
$lang['DOCTORS_NAME10']="Dr. Rose A. S.";
$lang['DOCTORS_FIELD10']="Pediatrics";
$lang['DOCTORS_IMAGE10']="<img src=\"images/pediatrics.png\" width=\"45px\" height=\"45px\" alt=\"pediatrics\" />";
$lang['DOCTORS_PORTRAIT10']="<img src=\"images/docs/Doctor5.png\"/>";
$lang['DOCTORS_REGION10']="Orlando";
$lang['DOCTORS_EXPERIENCE_TEXT10']="Hospital (8 years)";
$lang['DOCTORS_WHY_TEXT10']="Sometimes parents stay at home thinking their self-diagnosis for their child is right. Yet, self-diagnosis is both misleading and dangerous, especially for young children. With Medmedo, I am easily reachable when necessary, for a proper and reliable expertise.";
$lang['DOCTORS_EDUCATION_TEXT10']="Fellow - Cardiovascular Anesthesiology, Michael Reese Hospital</p>
          <p>Resident - Anesthesiology, Michael Reese Hospital</p>
          <p>MD, Loyola University &amp; Stritch School of Medicine</p>
          <p>MSEE - Electrical and Computer Engineering, University of Wisconsin,</p>
          <p>BSE - Biomedical Engineering, Duke University";
$lang['DOCTORS_HIGHLIGHTS_TEXT10']="Chair, Membership Committee, American College of Pediatrics, 2006-present";		  
$lang['DOCTORS_CERTIFICATION_TEXT10']="American Board of Pediatrics</p>
          <p>Diplomate, American Board of Psychiatry and Neurology</p>
          <p>Diplomate, American Board of Psychiatry and Neurology - General Psychiatry.";

		    	      //Doctor 11
$lang['DOCTORS_NAME11']="Dr. Rose A. S.";
$lang['DOCTORS_FIELD11']="Pediatrics";
$lang['DOCTORS_IMAGE11']="<img src=\"images/pediatrics.png\" width=\"45px\" height=\"45px\" alt=\"pediatrics\" />";
$lang['DOCTORS_PORTRAIT11']="<img src=\"images/docs/Doctor5.png\"/>";
$lang['DOCTORS_REGION11']="Orlando";
$lang['DOCTORS_EXPERIENCE_TEXT11']="Hospital (8 years)";
$lang['DOCTORS_WHY_TEXT11']="Sometimes parents stay at home thinking their self-diagnosis for their child is right. Yet, self-diagnosis is both misleading and dangerous, especially for young children. With Medmedo, I am easily reachable when necessary, for a proper and reliable expertise.";
$lang['DOCTORS_EDUCATION_TEXT11']="Fellow - Cardiovascular Anesthesiology, Michael Reese Hospital</p>
          <p>Resident - Anesthesiology, Michael Reese Hospital</p>
          <p>MD, Loyola University &amp; Stritch School of Medicine</p>
          <p>MSEE - Electrical and Computer Engineering, University of Wisconsin,</p>
          <p>BSE - Biomedical Engineering, Duke University";
$lang['DOCTORS_HIGHLIGHTS_TEXT11']="Chair, Membership Committee, American College of Pediatrics, 2006-present";		  
$lang['DOCTORS_CERTIFICATION_TEXT11']="American Board of Pediatrics</p>
          <p>Diplomate, American Board of Psychiatry and Neurology</p>
          <p>Diplomate, American Board of Psychiatry and Neurology - General Psychiatry.";
		  
		    	      //Doctor 12
$lang['DOCTORS_NAME12']="Dr. Rose A. S.";
$lang['DOCTORS_FIELD12']="Pediatrics";
$lang['DOCTORS_IMAGE12']="<img src=\"images/pediatrics.png\" width=\"45px\" height=\"45px\" alt=\"pediatrics\" />";
$lang['DOCTORS_PORTRAIT12']="<img src=\"images/docs/Doctor5.png\"/>";
$lang['DOCTORS_REGION12']="Orlando";
$lang['DOCTORS_EXPERIENCE_TEXT12']="Hospital (8 years)";
$lang['DOCTORS_WHY_TEXT12']="Sometimes parents stay at home thinking their self-diagnosis for their child is right. Yet, self-diagnosis is both misleading and dangerous, especially for young children. With Medmedo, I am easily reachable when necessary, for a proper and reliable expertise.";
$lang['DOCTORS_EDUCATION_TEXT12']="Fellow - Cardiovascular Anesthesiology, Michael Reese Hospital</p>
          <p>Resident - Anesthesiology, Michael Reese Hospital</p>
          <p>MD, Loyola University &amp; Stritch School of Medicine</p>
          <p>MSEE - Electrical and Computer Engineering, University of Wisconsin,</p>
          <p>BSE - Biomedical Engineering, Duke University";
$lang['DOCTORS_HIGHLIGHTS_TEXT12']="Chair, Membership Committee, American College of Pediatrics, 2006-present";		  
$lang['DOCTORS_CERTIFICATION_TEXT12']="American Board of Pediatrics</p>
          <p>Diplomate, American Board of Psychiatry and Neurology</p>
          <p>Diplomate, American Board of Psychiatry and Neurology - General Psychiatry.";
		  
		    	      //Doctor 13
$lang['DOCTORS_NAME13']="Dr. Rose A. S.";
$lang['DOCTORS_FIELD13']="Pediatrics";
$lang['DOCTORS_IMAGE13']="<img src=\"images/pediatrics.png\" width=\"45px\" height=\"45px\" alt=\"pediatrics\" />";
$lang['DOCTORS_PORTRAIT13']="<img src=\"images/docs/Doctor5.png\"/>";
$lang['DOCTORS_REGION13']="Orlando";
$lang['DOCTORS_EXPERIENCE_TEXT13']="Hospital (8 years)";
$lang['DOCTORS_WHY_TEXT13']="Sometimes parents stay at home thinking their self-diagnosis for their child is right. Yet, self-diagnosis is both misleading and dangerous, especially for young children. With Medmedo, I am easily reachable when necessary, for a proper and reliable expertise.";
$lang['DOCTORS_EDUCATION_TEXT13']="Fellow - Cardiovascular Anesthesiology, Michael Reese Hospital</p>
          <p>Resident - Anesthesiology, Michael Reese Hospital</p>
          <p>MD, Loyola University &amp; Stritch School of Medicine</p>
          <p>MSEE - Electrical and Computer Engineering, University of Wisconsin,</p>
          <p>BSE - Biomedical Engineering, Duke University";
$lang['DOCTORS_HIGHLIGHTS_TEXT13']="Chair, Membership Committee, American College of Pediatrics, 2006-present";		  
$lang['DOCTORS_CERTIFICATION_TEXT13']="American Board of Pediatrics</p>
          <p>Diplomate, American Board of Psychiatry and Neurology</p>
          <p>Diplomate, American Board of Psychiatry and Neurology - General Psychiatry.";
		  
                //Specialties -------------------	
$lang['DOCTORS_TYPE_HEADLINE']="Die besten Ärzte für Sie";
$lang['DOCTORS_TYPE_TEXT']="Alle Ärzte, die mit Medmedo zusammenarbeiten, sind sorgfältig ausgewählt. Unsere Ärzte haben jahrelange Erfahrung in Ihrem Spezialgebiet und werden speziell für die Beratung geschult.";

$lang['DOCTORS-TYPE_title1']="Allgemeinmediziner / Innere Medizin";
$lang['DOCTORS-TYPE_content1']="Die meisten Fragen können von einem Allgemeinmediziner beantwortet werden. Unsere Fachärzte für Allgemeinmedizin beraten Sie gerne.";

$lang['DOCTORS-TYPE_title2']="Kinderarzt";
$lang['DOCTORS-TYPE_content2']="Es ist wichtig, bei Gesundheitsfragen bzgl. Ihrer Kinder auf dem neusten Stand zu sein! Unsere Kinderärzte helfen Ihnen gerne weiter.";

$lang['DOCTORS-TYPE_title3']="Kardiologie";
$lang['DOCTORS-TYPE_content3']="Verringern Sie das Risiko einer Herzkrankheit! Unsere Kardiologen können Herzrhythmusstörungen erkennen und finden die bestmöglichen Wege, um Ihre Herzerkrankung zu behandeln.";

$lang['DOCTORS-TYPE_title4']="Gynäkologie";
$lang['DOCTORS-TYPE_content4']="In der Gynäkologie ist eine vertrauensvolle Beziehung zwischen Patient und Arzt sehr wichtig. Unsere Gynäkologen tun ihr Bestes, um sicherzustellen, dass Sie sich wohl fühlen.";

$lang['DOCTORS-TYPE_title5']="Dermatologie";
$lang['DOCTORS-TYPE_content5']="Bis zu 90% aller Hautprobleme können mittels medizinischer Fotoanalyse erfolgreich diagnostiziert werden. Unsere Dermatologen bewerten Ihre Hautprobleme und beraten Sie gerne.";

$lang['DOCTORS-TYPE_title6']="Orthopädie";
$lang['DOCTORS-TYPE_content6']="Immer dann, wenn ein Verdacht auf eine Verletzung von Knochen, Gelenken oder Nerven vorliegt, ist einer unserer orthopädischen Ärzte die beste Wahl für eine Beratung.";

$lang['DOCTORS-TYPE_title7']="Gastroenterologie";
$lang['DOCTORS-TYPE_content7']="Wenn Sie Beschwerden in Ihrem Verdauungssystem wie Verstopfung, Durchfall oder Bauchschmerzen haben, können Sie einen unserer Gastroenterologen für eine qualifizierte medizinische Beratung konsultieren.";



$lang['DOCTORS-TYPE_title8']="Psychotherapie";
$lang['DOCTORS-TYPE_content8']="Krankheiten sind oft ein Hilfeschrei der Seele. Seelische Belastungen werden manchmal erst durch körperliche Beschwerden bemerkbar. Scheuen Sie sich nicht davor, unsere qualifizierten Psychotherapeuten um Rat zu fragen.
";

$lang['DOCTORS-SPOTLIGHT_title']="Ärzte im Fokus";

$lang['DOCTORS-SPOTLIGHT_Doctor-name1']="Dr. med. Lars Omera";
$lang['DOCTORS-SPOTLIGHT_Doctor-type1']="Kardiologie";

$lang['DOCTORS-SPOTLIGHT_Doctor-name2']="Dr. med. Thomas Pers";
$lang['DOCTORS-SPOTLIGHT_Doctor-type2']="Allgemeinmediziner";

$lang['DOCTORS-SPOTLIGHT_Doctor-name3']="Dr. med. Ane Wertheim";
$lang['DOCTORS-SPOTLIGHT_Doctor-type3']="Kinderärztin";

$lang['DOCTORS-SPOTLIGHT_content']="Damit Sie unsere Ärzte kennenlernen stellen wir Ihnen von Zeit zu Zeit einige der Ärzte aus verschiedenen Fachgebieten vor. Mehr in Kürze.";

 // Privacy Page -------------------
$lang['PRIVACY_TITLE'] = 'Qualified Medical Advice - Anywhere, Anytime, 24/7';
$lang['PRIVACY_DESCRIPTION'] = 'Ask our doctors and describe your symptoms.';

// Terms Page --------------------
$lang['TERMS_TITLE'] = 'Qualified Medical Advice - Anywhere, Anytime, 24/7';
$lang['TERMS_DESCRIPTION'] = 'Ask our doctors and describe your symptoms.';

$lang['TERMS_title']="Allgemeine Geschäftsbedingungen / Terms &amp; Conditions";
$lang['TERMS_cont']="
			§ 1 Allgemeines
				(1) Diese Allgemeinen Geschäftsbedingungen gelten für sämtliche Angebote der Medlanes GmbH (nachfolgend Medlanes), die von den Nutzern über die Internetseite www.Medlanes.com oder andere Applikationen von Medlanes bezogen werden können.
				(2) Abweichende Vereinbarungen bedürfen der Schriftform. <br />
				<br />
				§ 2 Leistungsumfang
				(1) Medlanes bietet Gesundheitsinformationen und informative Dienstleistungen auf der Internetseite www.Medlanes.com und über andere Applikationen an. Diese werden anhand von Daten erstellt, die der Nutzer Medlanes überlässt. Das Angebot von Medlanes wird durch approbierte Ärzte unterstützt und der Dialog mit dem Nutzer erfolgt durch den Datenaustausch über die Onlineplattform Medlanes. Diese Informationen stellen grundsätzlich keine Diagnose oder Therapie sondern ausschließlich eine Unterstützung der Informationsfindung oder Internetrecherche dar. Für eine medizinische Untersuchung oder Behandlung ist ein Arzt vor Ort oder in dringenden Fällen eine Notfallambulanz aufzusuchen.
				(2) Die von Medlanes angebotenen Leistungen ersetzen nicht einen traditionellen Arztbesuch. Suchen Sie einen Arzt vor Ort auf oder in dringenden Fällen eine Notfallambulanz. <br />
				<br />
				§ 3 Vertragsschluss 
				(1) Das Vertragsverhältnis über die Nutzung der Leistungen von Medlanes kommt erst durch die Zustimmung dieser AGBs zustande.
				(2) Die Nutzung der Leistungen von Medlanes, insbesondere die Zusendung eines Sachverhaltes (Fall) gilt im Zweifel als Zustimmung.
				(3) Eine Anmeldung oder Nutzung der Dienstleistungen bei Medlanes ist nur möglich, sofern der Nutzer sämtliche in der Anmeldung abgefragten Daten wahrheitsgemäß und vollständig angegeben hat. Der Nutzer muss mindestens 18 Jahre alt und unbeschränkt geschäftsfähig sein und darf nicht unter Betreuung stehen, sofern Medlanes eine gesetzliche Vertretung nicht aktiv nachgewiesen wird. Es besteht kein Rechtsanspruch auf die Inanspruchnahme der Leistungen von Medlanes. Medlanes ist berechtigt, die Anmeldung ohne Angabe von Gründen zu verweigern. <br />
				<br />
				§ 4 Widerruf
				(1) Die Nutzer können die Vertragserklärungen innerhalb von 14 Tagen ohne Angabe von Gründen in Textform (z. B. Brief, Fax, E-Mail) widerrufen. Die Frist beginnt nach Erhalt dieser Belehrung in Textform, jedoch nicht vor Vertragsschluss und auch nicht vor Erfüllung der Informationspflichten gemäß Artikel 246 § 2 in Verbindung mit § 1 Abs. 1 und 2 EGBGB sowie unserer Pflichten gemäß § 312g Abs. 1 Satz 1 BGB in Verbindung mit Artikel 246 § 3 EGBGB. Zur Wahrung der Widerrufsfrist ist die rechtzeitige Absendung des Widerrufs. 
				(2) Der Widerruf ist zu richten an: Medlanes GmbH, vertreten durch die Geschäftsführer Emil Kendziorra und Erik Stoffregen, Stresemannstraße 66, 10963 Berlin, eMail: info(at)Medlanes.com
				(3) Im Falle eines wirksamen Widerrufs sind die beiderseits empfangenen Leistungen zurückzugewähren und ggf. gezogene Nutzungen (z. B. Zinsen) herauszugeben. Kann der Nutzer dem Anbieter die empfangenen Leistungen sowie Nutzungen (z.B. Gebrauchsvorteile) nicht oder teilweise nicht oder nur in verschlechtertem Zustand zurückgewähren bzw. herausgeben, muss der Nutzer Wertersatz leisten. Dies kann dazu führen, dass der Nutzer die vertraglichen Zahlungsverpflichtungen für den Zeitraum bis zum Widerruf gleichwohl erfüllen muss. Verpflichtungen zur Erstattung von Zahlungen müssen innerhalb von 30 Tagen erfüllt werden. Die Frist beginnt für den Nutzer mit der Absendung der Widerruferklärung, für Medlanes mit deren Empfang.
				(4) Das Widerrufsrecht erlischt vorzeitig, wenn der Vertrag von beiden Seiten auf ausdrücklichen Wunsch des Nutzers vollständig erfüllt ist. <br />
				<br />
				§ 5 Vergütung und Zahlungsbedingungen
				(1) Alle auf der Internetseite www.Medlanes.com angegebenen Preise verstehen sich einschließlich der ggf. anfallenden gesetzlichen Mehrwertsteuer.

				(2) Die Vergütung wird auf das erste Anfordern von Medlanes fällig, nachdem die von Medlanes bereitgestellten Leistungen in Anspruch genommen wurden. Medlanes behält sich vor nur nach Leistung eines Vorschusses tätig zu werden. Der Vorschuss wird vor Leistungserbringung fällig. Nimmt der der Nutzer die Leistung nicht in Anspruch oder ist die Leistung in anderer Weise gestört, erstattet Medlanes den Vorschuss zurück.
				(3) Medlanes akzeptiert folgende Zahlungsweisen: Kreditkarte, Lastschrift, PayPal. <br />
				<br />
				§ 6 Nutzungsrechte
				(1) Dem Nutzer stehen ausschließlich die nach diesen Allgemeinen Geschäftsbedingungen eingeräumten Rechte an dem Internetangebot zu. 
				(2) Die über die Online-Plattform von Medlanes und die weiteren Applikationen veröffentlichten Inhalte, Informationen, Bilder, Datenbanken und Softwareprogramme sind grundsätzlich urheberrechtlich geschützt und in der Regel Eigentum oder lizensiert von Medlanes.
				(3) Die Inhalte auf der Online-Plattform dürfen nur für persönliche und nicht für kommerzielle Zwecke genutzt oder vervielfältigt werden. Eine Weitergabe der Inhalte ist ohne ausdrückliche Zustimmung von Medlanes untersagt. <br />
				<br />
				§ 7 Nutzerdaten
				(1) Medlanes erhebt und nutzt für die Abwicklung der zwischen dem Nutzer und Medlanes geschlossenen Verträge Daten des Nutzers. Ohne die Einwilligung des Nutzers werden keine Daten erhoben, verarbeitet oder genutzt. Die mit Einwilligung erhobenen Daten werden nur verarbeitet oder genutzt soweit dies für die Abwicklung des Vertragsverhältnisses und zur Leistungserfüllung erforderlich ist.
				(2) Der Nutzer hat jederzeit und im Rahmen der Verfügbarkeit der Online-Plattform und sonstiger Applikationen von Medlanes die Möglichkeit, die von ihm gespeicherten Daten im Online-Profil abzurufen, zu ändern oder zu löschen. Dies gilt jedoch nicht für diejenigen Daten, für die eine gesetzliche Aufbewahrungspflicht besteht. Dies sind insbesondere die vom Nutzer über die Online-Plattform übermittelten Daten.
				(3) Im Übrigen wird auf die unter der Schaltfläche „Datenschutz“ abrufbare Datenschutzerklärung verwiesen. <br />
				<br />
				§ 8 Verfügbarkeit
				(1) Das Leistungsangebot steht in der Regel 24 Stunden am Tag zur Verfügung. Hiervon ausgenommen sind die Zeiten, in denen Datensicherungsarbeiten vorgenommen und Systemwartungs- oder Programmpflegearbeiten am System oder der Datenbank durchgeführt werden. Medlanes wird die hieraus entstehenden möglichen Störungen möglichst gering halten. 
				(2) Medlanes schließt jegliche Haftung wegen technischer oder sonstiger Störungen aus. <br />
				<br />
				§ 9 Haftung
				(1) Die von Medlanes bereitgestellten Inhalte und Leistungen werden sorgfältig nach dem jeweiligen Stand der Wissenschaft und des medizinischen Wissens erstellt. Medlanes übernimmt jedoch keine Gewähr für die Richtigkeit, Vollständigkeit, Korrektheit, Aktualität, Qualität oder Fehlerfreiheit der bereitgestellten Inhalte oder ihre Eignung für bestimmte Zwecke. Medlanes übernimmt keine Gewähr für die Richtigkeit der einzelnen Informationen. Es obliegt dem jeweiligen Nutzer, die Richtigkeit zu überprüfen oder einen Arzten aufzusuchen. Es wird insoweit auf § 2 der Allgemeinen Geschäftsbedingungen verwiesen.
				(2) Eine Haftung von Medlanes auf Schadensersatz, insbesondere wegen Verzugs, Nichterfüllung, Schlechterfüllung oder unerlaubter Handlung besteht nur bei Verletzung wesentlicher Vertragspflichten, auf deren Erfüllung in besonderem Maße vertraut werden durfte. Im Übrigen ist eine Haftung von Medlanes ausgeschlossen, es sei denn, es bestehen zwingende gesetzliche Regelungen. Der Haftungsausschluss gilt nicht für Vorsatz und grobe Fahrlässigkeit. 
				(3) Medlanes haftet nur für vorhersehbare Schäden. Die Haftung für mittelbare Schäden, insbesondere Mangelfolgeschäden, unvorhersehbare Schäden oder untypische Schäden sowie entgangenen Gewinn ist ausgeschlossen. Gleiches gilt für die Folgen von Arbeitskämpfen, zufälligen Schäden und höherer Gewalt.
				(4) Die vorstehenden Haftungsbeschränkungen gelten für sämtliche vertragliche und nichtvertragliche Ansprüche.
				(5) Zur Zahlungsabwicklung verwendet Medlanes externe Dienstleister woe PayPal und außerdem greift Medlanes auf die folgenden Drittanbieter zurück und verweist unter Freizeichnung von jeder Verantwortlichkeit und Haftung auf die entsprechenden AGBs:
				- Google <br />
				<br />
				§ 10 Pflichten des Nutzers
				(1) Der Nutzer darf das Leistungsangebot nur sachgerecht nutzen. Er wird insbesondere seinen Benutzernamen und das Passwort für den Zugang geheim halten, nicht weitergeben, keine Kenntnisnahme dulden oder ermöglichen und die erforderlichen Maßnahmen zur Gewährleistung der Vertraulichkeit ergreifen und bei einem Missbrauch oder Verlust dieser Angaben oder einem entsprechenden Verdacht dies dem Unternehmen anzuzeigen.
				(2) Der Nutzer ist verpflichtet Angaben, die für die Bereitstellung der individuellen ärztlischen Leistungen benötigt werden korrekt, umfassend und wahrheitsgemäß anzugeben. <br />
				<br />
				§ 11 Sperrung des Zugangs / Kündigung
				(1) Medlanes behält sich vor, bei Verdacht einer missbräuchlichen Nutzung oder wesentlichen Vertragsverletzung diesen Vorgängen nachzugehen, entsprechende Vorkehrungen zu treffen und bei einem begründeten Verdacht den Zugang des Nutzers zu sperren. Sollte der Verdacht ausgeräumt werden können, wird die Sperrung wieder aufgehoben, andernfalls steht Medlanes ein außerordentliches Kündigungsrecht zu.
				(2) Jeder Partei steht das Recht zur Kündigung aus wichtigem Grund zu. Die Kündigung bedarf der Textform (z.B. Email). Mit Wirksamwerden der Kündigung wird der Zugang des Nutzers zum Leistungsangebot von Medlanes gesperrt. <br />
				<br />
				§ 12 Änderungen
				(1) Medlanes hat das Recht, die AGB jederzeit gegenüber den Nutzern mit Wirkung für die Zukunft zu ändern. 
				(2) Eine beabsichtigte Änderung wird den Nutzern, die sich registriert haben, per E-Mail an die letzte dem Anbieter mitgeteilte Adresse mitgeteilt. Die jeweilige Änderung wird wirksam, wenn der jeweilige Nutzer ihr nicht innerhalb von zwei Wochen nach Absendung der E-Mail widerspricht. Für die Einhaltung der Zwei-Wochen-Frist ist die rechtzeitige Absendung des Widerspruchs maßgeblich. 
				(3) Widerspricht der Nutzer der Änderung innerhalb der Zwei-Wochenfrist, ist Medlanes berechtigt, das Vertragsverhältnis insgesamt außerordentlich fristlos zu beenden, ohne dass dem Nutzer hieraus irgendwelche Ansprüche gegen Medlanes erwachsen. Wird das Vertragsverhältnis nach dem wirksamen Widerspruch des Nutzers fortgesetzt, behalten die bisherigen AGB ihre Gültigkeit. <br />
				<br />
				§ 13 Schlussbestimmungen
				(1) Es gilt deutsches Recht.
				(2) Sollten einzelne Bestimmungen dieser AGB einschließlich dieser Bestimmung ganz oder teilweise unwirksam sein oder werden, bleibt die Wirksamkeit der übrigen Regelungen unberührt. Anstelle der unwirksamen oder fehlenden Bestimmungen treten die jeweiligen gesetzlichen Regelungen. <br />
				<br />
				Datenschutzbestimmungen <br />
				<br />
				<br />
				<br />
				§ 1 Erhebung personenbezogener Daten 
				Um die nach § 2 AGB vereinbarten Leistungen erbringen zu können, erhebt, verarbeitet und nutzt die 
				Medlanes GmbH i.G.
				vertreten durch die Geschäftsführer Emil Kendziorra und Erik Stoffregen
				Stressemannstraße 66, 10963 Berlin
				Handelsregisternummer: beantragt
				eMail: info[at]Medlanes.com
				– nachfolgend ”Medlanes” –
				die von dem Nutzer angegebenen Daten, sowie die IP-Adresse von dessen Endgerät (nachfolgend „personenbezogene Daten”). <br />
				<br />
				§ 2 Zweck der Erhebung, Verarbeitung und Nutzung 
				(1) Die personenbezogenen Daten erhebt, verarbeitet und nutzt Medlanes ausschließlich zu dem Zweck, die mit dem Nutzer im Sinne des § 2 AGB vereinbarten Leistungen zu erbringen.
				(2) Die personenbezogenen Daten werden ausschließlich grundsätzlich auf Servern in Deutschland gespeichert und verarbeitet, die den Deutschen Datenschutzbestimmungen unterliegen. Die personenbezogenen Daten werden darüber hinaus nicht an Dritte übermittelt.
				(3) Medlanes setzt „Cookies” zu dem Zweck ein, die Berechtigung des Nutzers zum Abruf der gewünschten Leistungen zu verifizieren und zur Analyse des Nutzungsverhaltens, um die Optimierung der Webseite im Sinne der Nutzer zu ermöglichen. <br />
				<br />
				§ 3 Schutz der Privatsphäre 
				Medlanes verpflichtet sich, die Privatsphäre des Nutzers zu schützen und versichert, die personenbezogenen Daten im Einklang mit dem Bundesdatenschutzgesetz und dem Telemediengesetz zu erheben, zu verarbeiten und zu nutzen und ausschließlich für die Erfüllung der unter § 2 definierten Zwecke zu verarbeiten und zu nutzen. Medlanes wird seine Mitarbeiter – insbesondere die angeschlossenen Ärzte - entsprechend verpflichten. <br />
				<br />
				§ 4 Rechte des Nutzers 
				(1) Der Nutzer hat bezüglich der personenbezogenen Daten die durch das Bundesdatenschutzgesetz gewährleisteten Rechte auf Auskunft, Berichtigung und Löschung. Diese Rechte kann der Nutzer per Post oder eMail direkt an Medlanes.. 
				(2) Der Nutzer ist berechtigt, die Leistungen von Medlanes ohne Angaben zu persönlichen Daten anonym zu beziehen. In diesem Fall kann er nur seine E-Mail-Adresse und anstelle seines Namens ein Pseudonym angeben. <br />
				<br />
				§ 5 Einwilligung 
				Sofern der Nutzer in die Bedingungen dieser Erklärung einwilligt, erklärt er sich mit der geregelten Nutzung der personenbezogenen Daten einverstanden. Die Einwilligung oder ihre Verweigerung erfolgt durch Klicken oder Nichtklicken auf das entsprechende Feld nach dem Hinweis auf diese Bestimmungen. Die erteilte oder verweigerte Einwilligung wird protokolliert; ihr Inhalt ist für den Nutzer jederzeit unter auf Anfrage abrufbar. Der Nutzer hat das Recht, jederzeit seine Einwilligung zu widerrufen und damit sein Konto mit allen Daten aufzulösen und zu löschen. <br />
				<br />
				§ 6 Zahlungsabwicklung/Drittanbieter  
				(1) Sofern der Nutzer zur Zahlungsabwicklung den Service von PayPal in Anspruch nimmt, wird der Nutzer per Hyperlink auf die Website von www.paypal.de weitergeleitet. Medlanes weist darauf hin, dass hier weitere personenbezogene Daten des Nutzers erhoben, verarbeitet und genutzt werden. Medlanes trifft insofern keine Verantwortlichkeit; es gelten ausschließlich die Nutzungs- und Datenschutzbestimmungen von PayPal (Europe) S.à r.l. et Cie, S.C.A..
				(2) Außerdem greift Medmedo auf die Leistungen der folgenden weiteren Drittanbieter zu und verweist freibleibend auf die entpsrechenden Nutzungs- und Datenschutzbestimmungen:
				- Google
			";


// Contact Page--------------------

$lang['CONTACT_TITLE'] = 'Qualified Medical Advice - Anywhere, Anytime, 24/7';
$lang['CONTACT_DESCRIPTION'] = 'Ask our doctors and describe your symptoms.';

$lang['contact_slider']="Fragen? </br>Kontaktieren Sie uns noch heute.";
$lang['contact_title1']="Unser Team steht für Sie bereit";
$lang['contact_cont1']="Medmedo schreibt Support groß. Wir stehen Ihnen jederzeit mit Rat und Tat zur Seite, egal wie wir Ihnen helfen können. Kontaktieren Sie uns - wir freuen uns auf Ihre Nachricht.";

$lang['contact_title2']="Telefonischer und eMail-Support";

$lang['contact_cont2']="In über 90% der Anfragen antworten wir innerhalb von 24h.";


$lang['contact_blue_box1_text1']="Kundensupport";
$lang['contact_blue_box1_text2']="0800 - 765 43 43";
$lang['contact_blue_box1_text3']="gebührenfrei 24 Stunden an 365 Tagen im Jahr";
$lang['contact_blue_box1_text4']="Rückrufservice";
$lang['contact_blue_box1_text5']="E-Mail";

$lang['contact_blue_box2_text1']="eMail-Support";
$lang['contact_blue_box2_text2']="info@medlanes.com";
$lang['contact_blue_box2_text3']="8:00 bis 18:00 Uhr";

$lang['contact_form_title']="Kontaktformular";
$lang['contact_form_cont']="Schreiben Sie uns! Wir freuen uns auch über Feedback und Anregungen.";

$lang['contact_form_label_surname']="Vorname";
$lang['contact_form_label_name']="Name";
$lang['contact_form_label_email']="eMail";
$lang['contact_form_label_msg']="Anfrage";

$lang['contact_completemsg']="<li class=msg>Danke für ihre Anfrage</li>";
$lang['contact_errmsg']="<li class=errorMsg>Error!</li>";
$lang['contact_form_label_send'] = "Senden";

// FAQ Page--------------------

$lang['FAQ_TITLE'] = 'Qualified Medical Advice - Anywhere, Anytime, 24/7';
$lang['FAQ_DESCRIPTION'] = 'Ask our doctors and describe your symptoms.';

$lang['FAQ_main_title']="Häufig gestellte Fragen";
$lang['FAQ_main_cont']="Guter Kundenservice steht bei Medmedo im Vordergrund. Hier finden Sie Antworten auf die häufigsten Fragen. Für alles Weitere kontaktieren Sie uns einfach direkt.";


$lang['FAQ_Tab-1']="Allgemein";
$lang['FAQ_Tab-2']="Medizin";
$lang['FAQ_Tab-3']="Sicherheit";
$lang['FAQ_Tab-4']="Bezahlung";

$lang['FAQ_points_title1']="1) Wer sind wir?";
$lang['FAQ_points_cont1']="Die Medlanes GmbH ist eine Telemhealth Firma mit Sitz in Berlin. Die Firma wurde von dem Arzt Emil Kendziorra und Erik Stoffregen gegründet um Medizin online zu bringen. Emil and Erik sind davon überzeugt, dass die Zukunft der Medizin online gestaltet wird. Medmedo hat das Ziel die Verfügbarkeit und Einfachheit von qualifiziertem medizinischen Rat zu verbessern und Patienten gesünder zu machen.";

$lang['FAQ_points_title2']="1) Was macht Medmedo besonders?";
$lang['FAQ_points_cont2']="Medmedo deckt einen großen Teil der Medizin online ab. Sie erhalten Zugang zu einem Netzwerk von erfahrenen Ärzten aus den verschiedensten Fachbereichen und somit schnelle, qualifizierte Beratung.";


$lang['FAQ_points_title3']="2) Kann ich Medmedo auch im Ausland nutzen?";
$lang['FAQ_points_cont3']="Natürlich, Medmedo ist international verfügbar! Nutzen Sie den Service ganz einfach wie gewohnt, über WLAN oder das mobile Netz (beachten Sie die Roaminggebühren Ihres Netzanbieters).";







$lang['FAQ_points2_title1']="1) Welche Fragen kann ich stellen?";
$lang['FAQ_points2_cont1']="Sie können jede Frage an unser Ärzte-Team stellen. Handelt es sich jedoch um eine Notfallsituation, sollte umgehend ein Notarzt kontaktiert werden.
";


$lang['FAQ_points2_title2']="2) Ist mein bisheriger Arzt online erreichbar?";
$lang['FAQ_points2_cont2']="Unser Ärztenetzwerk wächst ständig. Fragen Sie Ihren Arzt gerne, ob er Medmedo bereits kennt oder künftig nutzen möchte, um Ihnen einen besseren Zugang zu qualifizierter Beratung zu ermöglichen. Wir schicken gerne Informationen an interessierte Ärzte.
";


$lang['FAQ_points2_title3']="3) Für welche Fragen ist Medmedo konzipiert?";
$lang['FAQ_points2_cont3']="
Hier finden Sie einige Beispiele für typische Anwendungsfälle: </br>
- Ich fühle mich nicht gut und habe keine Zeit, einen Arzt aufzusuchen.</br>
- Mein Hausarzt hat geschlossen oder ist im Urlaub und ich habe eine Frage. </br>
- Ich bin gerade umgezogen und suche einen Arzt.</br>
- Ich hätte gerne eine Zweitmeinung.</br>
- Ich suche eine Antwort auf eine einfache Frage, möchte aber nicht lange auf einen Termin warten. </br>
- Ich bin im Urlaub und würde gerne einen Arzt kontaktieren, der meine Sprache spricht.</br>
- Es ist Wochenende und mein Kind hat Fieber. Soll ich ins Krankenhaus fahren?</br>

";












$lang['FAQ_points3_title1']="1) Sind meine Daten sicher?";
$lang['FAQ_points3_cont1']="Ja, Medmedo nutzt ausschließlich Software, die den höchsten Sicherheitsstandards entspricht. Alle Ihre Daten werden auf deutschen Servern gespeichert und können von Ihnen vollständig gelöscht werden.";


$lang['FAQ_points3_title2']="2) Wofür werden meine Informationen genutzt?";
$lang['FAQ_points3_cont2']="Ihre Angaben werden ausschließlich zweckgebunden für die Beantwortung durch den Arzt genutzt. Medmedo benutzt Ihre Daten für keinerlei andere Zwecke.";











$lang['FAQ_points4_title1']="1) Was kostet die Nutzung von Medmedo? Übernimmt meine Versicherung die Kosten?";
$lang['FAQ_points4_cont1']="
Die Medmedo App können Sie kostenlos herunterladen und nutzen. Wenn Sie einem Arzt eine Frage stellen, kostet dies einmalig €24. Die anfallenden Kosten werden dabei in den meisten Fällen nicht von den Krankenkassen übernommen.
";


$lang['FAQ_points4_title2']="2) Was kann ich tun, wenn ich mit dem Service unzufrieden bin?";
$lang['FAQ_points4_cont2']="
Ihre Meinung ist uns sehr wichtig! Wenn Sie nicht vollständig zufrieden sein sollten, bitten wir Sie unser Supportteam (support@Medlanes.com) zu kontaktieren. Wir helfen Ihnen gerne weiter und finden eine Lösung.
";



// About Page--------------------

$lang['ABOUT_TITLE'] = 'Qualified Medical Advice - Anywhere, Anytime, 24/7';
$lang['ABOUT_DESCRIPTION'] = 'Ask our doctors and describe your symptoms.';

$lang['ABOUT_BANNER_title1']="Wir bringen Medizin online!";
$lang['ABOUT_BANNER_title2']="Und machen es bezahlbar für jedermann.";

$lang['ABOUT_COMPANY_headline']="Das Unternehmen";
$lang['ABOUT_COMPANY_text']="Die Medlanes GmbH ist ein Unternehmen mit Sitz in Berlin, das sich auf Telemedizin und medizinische Onlineberatung spezialisiert hat. Seit unserer Gründung in 2013, haben wir den herkömmlichen Arztbesuch revolutioniert. Für unsere Innovationen im Gesundheitswesen wurden wir bereits mit mehreren Förderpreisen, unteranderem von Microsoft und der Bayer AG, ausgezeichnet. Unsere Ärzte sind jederzeit verfügbar und stellen die Gesundheit unserer Patienten immer in den Vordergrund. Keine langen Wege mehr zur Arztpraxis oder wochenlanges Warten auf einen Termnin bei kleinen Beschwerden. Unsere Ärzte sind immer für Sie da und beantworten Ihre Fragen innerhalb einer Stunde. ";

$lang['ABOUT_VISION_headline']="Unsere Vision";
$lang['ABOUT_VISION_text']="Medmedo wurde aus einem einfachen Grund gegründet. Wir möchten die bestmögliche Gesundheitsversorgung ermöglichen und vermeiden dabei die Nachteile eines üblichen Arztbesuches. Wir bieten sofortigen Zugang zu medizinischer Beratung, überall, jederzeit und zu einem erschwinglichen Preis. Wir glauben fest daran, dass der Zugang zur bestmöglichen gesundheitlichen Versorgung nicht durch Versicherungsmitgliedschaften, finanzielles Budget, Standort oder mangelnde Zeit limitiert werden sollte. Medmedo revolutioniert das Gesundheitswesen und ermöglicht eine Gesundheitsversorgung ohne Grenzen. ";

$lang['ABOUT_PEOPLE_headline']="Das Team";
$lang['ABOUT_PEOPLE_text']="Wir haben ein hochqualifiziertes Team aus Ärzten zusammengestellt, mit mehr als 15 verschiedenen Spezialisierungen in mehr als acht verschiedenen Ländern, auf zwei Kontinenten. Unser Team ist rund um die Uhr verfügbar, um Antworten auf Ihre medizinische Fragen zu finden. Wir kümmern uns um Sie!";

$lang['ABOUT_SOLUTION_headline']="Unsere Lösung";
$lang['ABOUT_SOLUTION_text']="Wir sind stolz Ihnen die Medmedo App zu präsentieren! Die Medmedo App ermöglicht es, Ihren Arzt ständig dabei zu haben. Wir sind für Sie verfügbar, egal wo Sie sich befinden und wann immer Sie einen ärztlichen Rat benötigen. Wir stehen Ihnen mit unserem einfachen, schnellen und günstigen Service für medizinische Fragen rund um die Uhr zur Verfügung! ";


//Career 



$lang['JOBS_title1']="Offene Stellen bei Medmedo";
$lang['JOBS_cont1']="Detailierte Stellenbeschreibungen werden im Mai 2014 veröffentlich. Momentan haben wir einige offene Positionen. Wir freuen uns auf Initiativbewerbungen aus allen Bereichen. <br />
<br />
Aktuell haben wir u.A. auch offene Positionen für Praktikanten in den Bereichen Marketing und Business development. ";

$lang['JOBS_heading']="Offene Stellen bei Medmedo";
$lang['JOBS_heading2']="Wir sind immer auf der Suche nach neuen Talenten um unser Team zu verstärken.";
$lang['JOBS_subheading']="Hier finden Sie unsere aktuellen offenen Praktika Stellen:";
$lang['JOBS_subheading-two']="Hier finden Sie unsere aktuellen Vollzeit Positionen:";
$lang['JOBS_list1']="Marketing & Sales Intern (m/f)";
$lang['JOBS_list2']="Technical Research & Development Intern (m/f)";
$lang['JOBS_list3']="Internship in Healthcare Startup in Berlin";
$lang['JOBS_list4']="Executive Assistant Intern (m/f)";
$lang['JOBS_list5']="Junior Marketing (m/f)";
$lang['JOBS_list6']="Junior Sales (m/f)";
$lang['JOBS_listHR']="HR Intern (m/f)";
$lang['JOBS_listBD']="Business Development Intern (m/f)";
$lang['JOBS_list7']="Social Media Intern (m/f)";
$lang['JOBS_list8']="HR/Recruiting Intern at Digital Health Startup (m/f)";
$lang['JOBS_list9']="Technical / Online Marketing Intern (m/f)";
$lang['JOBS_list10']="Sales Trainee (m/f)";


// Impressum Page--------------------

$lang['IMPRINT_TITLE'] = 'Qualified Medical Advice - Anywhere, Anytime, 24/7';
$lang['IMPRINT_DESCRIPTION'] = 'Ask our doctors and describe your symptoms.';

$lang['IMPRESSUM_main_title']="Imprint / Impressum";

$lang['IMPRESSUM_main_content']="Hinweise gem. § 6 des Teledienstegesetzes (TDG) in der Fassung des Gesetzes über rechtliche Rahmenbedingungen für den elektronischen Geschäftsverkehr vom 20.12.2001.";

$lang['IMPRESSUM_address']="Medlanes GmbH<br />
							Stresemannstraße 66A<br />
							10963 Berlin<br />
							<br />
							Fon +49 (30) 577 004 20<br />
							eMail: info@medlanes.com<br />
							<br />
							Geschäftsführer:: Emil Kendziorra, Erik Stoffregen";

$lang['IMPRESSUM_cont_title1']="Internetredaktion/Autor";

$lang['IMPRESSUM_cont_content1']="(Inhaltlich Verantwortlicher gem. § 10 Abs. 3 MDStV und gem.§ 6 MDStV [Staatsvertrag über Mediendienste]:<br />						<br />Emil Kendziorra";

$lang['IMPRESSUM_cont_title2']="Haftungshinweis:";

$lang['IMPRESSUM_cont_content2']="Alle Texte und Bilder sind urheberrechtlich geschützt. Die Veröffentlichung, Übernahme oder Nutzung von Texten, Bildern oder anderen Daten bedürfen der schriftlichen Zustimmung des Herausgebers – eine Ausnahme besteht bei Presseinformationen und den dort veröffentlichten Bildern. Die inhaltliche Verantwortung erfolgt durch die jeweils die Autoren. Trotz aller Bemühungen um möglichst korrekte Darstellung und Prüfung von Sachverhalten sind Irrtümer oder Interpretationsfehler möglich.<br />
							<br />
							Hiermit distanziert sich der Herausgeber ausdrücklich von allen Inhalten aller gelinkten Seiten auf der Homepage eierfabrik.de und macht sich ihren Inhalt nicht zu Eigen! Diese Erklärung gilt für alle auf dieser Homepage angebrachten Links. (Urteil zur “Haftung für Links” am Landgericht Hamburg vom 12. Mai 1998, AZ: 312 O 85/98)<br />
							<br />
							Der Herausgeber nimmt den Schutz Ihrer personenbezogenen Daten sehr ernst. Wir verarbeiten personenbezogene Daten, die beim Besuch auf unserer Webseiten erhoben werden, unter Beachtung der geltenden datenschutzrechtlichen Bestimmungen. Ihre Daten werden von uns weder veröffentlicht, noch unberechtigt an Dritte weitergegeben..";

$lang['IMPRESSUM_cont_title3']="Disclaimer";

$lang['IMPRESSUM_cont_content3']="Diese Website wurde mit größtmöglicher Sorgfalt erstellt. Die Medlanes GmbH i.G. übernimmt jedoch keine Garantie für die Vollständigkeit, Richtigkeit und Aktualität der enthaltenen Informationen. 
							
							Jegliche Haftung für Schäden, die direkt oder indirekt aus der Benutzung dieser Website entstehen, wird ausgeschlossen. Dies gilt auch für Links, auf die diese Website direkt oder indirekt verweist. Die Solidmedia hat keinen Einfluss auf die Gestaltung und die Inhalte der von uns gelinkten Seiten. Deshalb distanzieren wir uns hiermit ausdrücklich von allen Inhalten aller gelinkten Seiten fremder Anbieter. <br />
							<br />
							Bei Falschinformationen oder Fehlern bitten wir Sie, uns dies mitzuteilen. <br />
							<br />
							Diese Website benutzt Google Analytics, einen Webanalysedienst der Google Inc. („Google“). Google Analytics verwendet sog. „Cookies“, Textdateien, die auf Ihrem Computer gespeichert werden und die eine Analyse der Benutzung der Website durch Sie ermöglichen. Die durch den Cookie erzeugten Informationen über Ihre Benutzung dieser Website (einschließlich Ihrer IP-Adresse) wird an einen Server von Google in den USA übertragen und dort gespeichert. Google wird diese Informationen benutzen, um Ihre Nutzung der Website auszuwerten, um Reports über die Websiteaktivitäten für die Websitebetreiber zusammenzustellen und um weitere mit der Websitenutzung und der Internetnutzung verbundene Dienstleistungen zu erbringen. Auch wird Google diese Informationen gegebenenfalls an Dritte übertragen, sofern dies gesetzlich vorgeschrieben oder soweit Dritte diese Daten im Auftrag von Google verarbeiten. Google wird in keinem Fall Ihre IP-Adresse mit anderen Daten von Google in Verbindung bringen. Sie können die Installation der Cookies durch eine entsprechende Einstellung Ihrer Browser Software verhindern; wir weisen Sie jedoch darauf hin, dass Sie in diesem Fall gegebenenfalls nicht sämtliche Funktionen dieser Website vollumfänglich nutzen können. Durch die Nutzung dieser Website erklären Sie sich mit der Bearbeitung der über Sie erhobenen Daten durch Google in der zuvor beschriebenen Art und Weise und zu dem zuvor benannten Zweck einverstanden.";


// Home Page--------------------


$lang['SLIDER_headline1']="Sind Sie es leid, lange im Wartezimmer oder auf einen Arzttermin zu warten?";
$lang['SLIDER_button1']="Fragen Sie jetzt unsere Ärzte online!";
$lang['SLIDER_headline2']="Medizinische Beratung <br> jederzeit verfügbar.";
$lang['SLIDER_button2']="Fragen Sie jetzt unsere Ärzte online!";
$lang['SLIDER_headline3']="Konsultieren Sie einen Facharzt bequem von zu Hause - schnell und einfach.";
$lang['SLIDER_button3']="Fragen Sie jetzt unsere Ärzte online!";

$lang['SEEN']="Bekannt aus:";
$lang['SEEN_images']="<img style=\"width : 92% !important ; \"src=\"images/landingpage/concept_de.jpg\" alt=\"concept seen on\" />";

$lang['TESTIMONIAL_headline']="<b>Nutzungsbeispiele</b> für Medmedo: ";
$lang['TESTIMONIAL1']="“Ich war überrascht, dass ich so einfach und schnell professionellen Rat bekommen kann”";
$lang['TESTIMONIAL1_name']=""; //Paul
$lang['TESTIMONIAL2']="“Medmedo ist bequem und einfach. Einen Arzt zu konsultieren, ohne mein Büro zu verlassen, ist unbezahlbar.”";
$lang['TESTIMONIAL2_name']=""; //Matthias
$lang['TESTIMONIAL3']="“Ich habe mich mit meinen Symptomen alleine gelassen gefühlt, aber eine schnelle Aufklärung durch Medmedo hat mir die Sorgen genommen.”";
$lang['TESTIMONIAL3_name']=""; //Christiane
$lang['TESTIMONIAL4']="“Zuverlässige medizinische Beratung zu einem erschwinglichen Preis, ein Traum wird wahr.”";
$lang['TESTIMONIAL4_name']=""; //Caroline
$lang['TESTIMONIAL5']="“Als Mutter zweier Kinder habe ich oft keine Zeit meinen halben Tag im Wartezimmer zu verbringen. Medmedo spart Zeit und Geld.”";
$lang['TESTIMONIAL5_name']=""; //Lisa
$lang['TESTIMONIAL6']="“Ich bin nicht mehr so gut zu Fuß unterwegs. Ein Gesundheitscheck von zu Hause erspart mir unnötige Wege.”";
$lang['TESTIMONIAL6_name']=""; //Hendrik
$lang['TESTIMONIAL7']="“Die Freiheit einen Arzt von überall auf der Welt zu kontaktieren, möchte ich nicht mehr missen.”";
$lang['TESTIMONIAL7_name']=""; //Monika
$lang['TESTIMONIAL8']="“Die selbstständige Suche nach meinen Symptomen führte nur zu widersprüchlichen Informationen. Mit Medmedo habe ich Gewissheit!”";
$lang['TESTIMONIAL8_name']=""; //Karl
$lang['TESTIMONIAL9']="“Medmedo hat mir Sicherheit bezüglich meiner Symptome gegeben, schnell und günstig. Sehr zu empfehlen!”";
$lang['TESTIMONIAL9_name']=""; //Georg
$lang['TESTIMONIAL10']="“Medmedo bietet einen außergewöhnlichen Service, der mir die Sorgen um meine Tochter genommen hat.”";
$lang['TESTIMONIAL10_name']=""; //Viktoria & Lily
$lang['TESTIMONIAL11']="“Oft ist es schwer einen Arzt am Wochenende zu finden. Medmedo passt immer in meinen Terminplan.”";
$lang['TESTIMONIAL11_name']=""; //Jana
$lang['TESTIMONIAL12']="“Für kleinere Gesundheitsfragen brauche ich keinen Arzttermin. Medmedo ist der schnellste, einfachste und bequemste Weg.”";
$lang['TESTIMONIAL12_name']=""; //Dennis
$lang['TESTIMONIAL13']="”Ich reise viel. Da ist es oft schwer, einen deutschsprachigen Arzt zu finden. Diese Sorgen habe ich mit Medmedo nicht.”";
$lang['TESTIMONIAL13_name']=""; //Johannes
$lang['TESTIMONIAL14']="“Zu wissen, dass ich mich jederzeit auf die schnelle und qualifizierte medizinische Beratung durch Medmedo verlassen kann, lässt mich nachts ruhig schlafen.”";
$lang['TESTIMONIAL14_name']=""; //Richard
$lang['TESTIMONIAL15']="“Ich bin nicht sehr bewandert, was Computer und Internet betrifft. Aber Medmedo ist intuitiv und einfach.”";
$lang['TESTIMONIAL15_name']=""; //Linda
$lang['TESTIMONIAL16']="“Ich liebe Medmedo! So sollte medizinische Beratung im 21. Jahundert sein.”";
$lang['TESTIMONIAL16_name']=""; //Franziska

$lang['BENEFITS_headline']="<b>Qualifizierte Ärzte</b> beantworten Ihre medizinischen Fragen.";
$lang['BENEFITS_tab1']="Bequem";
$lang['BENEFITS_tab2']="Einfach";
$lang['BENEFITS_tab3']="Bezahlbar";
$lang['BENEFITS_tab4']="Zeitsparend";
$lang['BENEFITS_content1']="Medizinische Beratung durch Medmedo ist bequem! Sparen Sie sich den Aufwand Ihren Arzttermin Wochen im Voraus zu planen. Medmedo is überall und jederzeit verfügbar - auf Ihrem Smartphone oder im Web.";
$lang['BENEFITS_content2']="Medmedo ist einfach und intuitiv. Erhalten Sie sofort qualifizierte medizinische Beratung durch unsere ausgewählten Fachärzte - rund um die Uhr, sogar im Urlaub!   ";
$lang['BENEFITS_content3']="Durch eine qualifizierte medizinische Beratung können indirekte Kosten entstehen. Mit Medmedo sparen Sie Sie sich unnötige Wege zum Arzt und verlieren keine Zeit.";
$lang['BENEFITS_content4']="Warten Sie nicht länger auf Ihren Arzttermin oder im Wartezimmer. Mit Medmedo erhalten Sie medizinische Beratung 24 Stunden am Tag, 7 Tage die Woche.";

$lang['SPOTLIGHT_h1']="Unsere Ärzte";
$lang['SPOTLIGHT_desc']="Eine Vielzahl von Ärzten beantwortet Ihre Fragen - egal welche Fachrichtung, wir haben den Spezialisten für Sie.</br>Lernen Sie einige unserer Ärzte kennen, die uns dabei helfen, das Gesundheitswesen zu revolutionieren und unseren bequemen, schnellen und zeitsparenden Service bereit zu stellen.";
$lang['SPOTLIGHT_exp']="Erfahrung";
$lang['SPOTLIGHT_why']="Warum arbeiten Sie mit Medmedo?";
$lang['SPOTLIGHT_doctor1_name']="Dr. Thomas P.";
$lang['SPOTLIGHT_doctor1_specialty']="Hausarzt";
$lang['SPOTLIGHT_doctor1_location']="Düsseldorf";
$lang['SPOTLIGHT_doctor1_exp']="<p>Krankenhaus(4 Jahre)</p><p>Privatpraxis (17 Jahre)</p>   ";
$lang['SPOTLIGHT_doctor1_explanation']="Ich habe Medmedo gewählt, weil ich überzeugt bin, dass neue Technologien meinen medizinischen Alltag vereinfachen können. Mithilfe von Online-Konsultationen erhalte ich eine interdisziplinäre Übersicht von meinen Patienten, dies ermöglicht genauere Diagnosen.";
$lang['SPOTLIGHT_doctor2_name']="Dr. Marion N.";
$lang['SPOTLIGHT_doctor2_specialty']="Pädiatrie";
$lang['SPOTLIGHT_doctor2_location']="Offenbach";
$lang['SPOTLIGHT_doctor2_exp']="<p>Krankenhaus (7 Jahre)</p> ";
$lang['SPOTLIGHT_doctor2_explanation']="Viele Eltern denken, sie liegen mit der Selbstdiagnose ihrer Kinder richtig. Die Selbstdiagnose kann jedoch irreführend und gefährlich sein, insbesondere bei Kleinkindern. Mit Medmedo bin ich jederzeit erreichbar, um eine zuverlässige und qualifizierte Auskunft zu geben.";
$lang['SPOTLIGHT_doctor3_name']="Dr. Richard R.";
$lang['SPOTLIGHT_doctor3_specialty']="Hausarzt";
$lang['SPOTLIGHT_doctor3_location']="Wuppertal";
$lang['SPOTLIGHT_doctor3_exp']="<p>Privatpraxis(8 Jahre)</p>";
$lang['SPOTLIGHT_doctor3_explanation']="Viele meiner Patienten sind regelmäßig außerhalb Deutschlands unterwegs. Online-Konsultationen ermöglichen Ihnen, von überall mit Ihrem Hausarzt verbunden zu werden.";
$lang['SPOTLIGHT_doctor4_name']="Dr. Elisabeth B.";
$lang['SPOTLIGHT_doctor4_specialty']="Hausärztin";
$lang['SPOTLIGHT_doctor4_location']="Düren";
$lang['SPOTLIGHT_doctor4_exp']=" <p>Privatpraxis(16 Jahre)</p> ";
$lang['SPOTLIGHT_doctor4_explanation']="Mithilfe von Medmedo kann ich die Fragen jener Patienten beantworten, die ortsgebunden sind oder altersbedingt meine Praxis nicht mehr einfach erreichen können.";
$lang['SPOTLIGHT_doctor5_name']="Dr. Gerhard W.";
$lang['SPOTLIGHT_doctor5_specialty']="Dermatologie";
$lang['SPOTLIGHT_doctor5_location']="Nürnberg";
$lang['SPOTLIGHT_doctor5_exp']="<p>Forschung (8 Jahre)</p> <p>Krankenhaus (6 Jahre)</p><p>Privatpraxis (11 Jahre)</p>  ";
$lang['SPOTLIGHT_doctor5_explanation']="Ich bin nun mehr als 25 Jahre im Gesundheitswesen tätig. Medmedo bietet die einmalige Möglichkeit, meine Erfahrungen auf breitem Feld zu teilen. So kann ich behaupten, dass ich einen wertvollen Beitrag zur Gesellschaft leiste.";
$lang['SPOTLIGHT_doctor6_name']="Dr. David H.";
$lang['SPOTLIGHT_doctor6_specialty']="Diätologie";
$lang['SPOTLIGHT_doctor6_location']="Solingen";
$lang['SPOTLIGHT_doctor6_exp']="<p>Humanitäre Hilfe (3 Jahre)</p> <p>Privatpraxis (2 Jahre)</p> ";
$lang['SPOTLIGHT_doctor6_explanation']="Ich bin ein Verfechter der Medizinethik und überzeugt von der Idee, qualifizierte medizinische Beratung für jedermann zugänglich zu machen.";
$lang['SPOTLIGHT_doctor7_name']="Dr. Daniel  G.";
$lang['SPOTLIGHT_doctor7_specialty']="Pädiatrie";
$lang['SPOTLIGHT_doctor7_location']="Augsburg";
$lang['SPOTLIGHT_doctor7_exp']=" <p>Krankenhaus (8 Jahre)</p> ";
$lang['SPOTLIGHT_doctor7_explanation']="Patientenzustände sind verschieden und verändern sich ständig. Daher ist eine regelmäßige Überwachung unumgänglich. Medmedo macht dies möglich.";
$lang['SPOTLIGHT_doctor8_name']="Dr. Christiane F.";
$lang['SPOTLIGHT_doctor8_specialty']="Gastroenterologie";
$lang['SPOTLIGHT_doctor8_location']="Kiel";
$lang['SPOTLIGHT_doctor8_exp']="<p>Klinik (6 Jahre)</p> <p>Privatpraxis (10 Jahre)</p> ";
$lang['SPOTLIGHT_doctor8_explanation']="Als Fachärztin weiß ich, wie mühsam es ist, kurzfristig einen Termin zu bekommen. Online-Konsultationen sind zeitsparend sowohl für Patienten als auch für uns Ärzte.";


// Payment Page--------------------
$lang['PAYMENT_TITLE'] = 'Qualified Medical Advice - Anywhere, Anytime, 24/7';
$lang['PAYMENT_DESCRIPTION'] = 'Ask our doctors and describe your symptoms.';	

$lang['PAYMENT_PAYPAL'] = 'PayPal';	
$lang['PAYMENT_CREDITCARD'] = 'Kreditkarte';	
$lang['PAYMENT_CONSULTATION'] = 'Onlineberatung durch qualifizierten Arzt';
$lang['PAYMENT_PRICE'] = '€24';	
$lang['PAYMENT_CTA'] = 'Jetzt bezahlen & Arzt treffen'; 

$lang['PAYMENT_MB_h3'] = '30-Tage Geld-zurück-Garantie'; 
$lang['PAYMENT_MB_p'] = 'Wenn unsere Ärzte Ihnen nicht helfen, erhalten Sie alle Kosten in vollem Umfang zurückerstattet.'; 
$lang['PAYMENT_BCD_h3'] = 'Zertifizierte Ärzte'; 
$lang['PAYMENT_BCD_p'] = 'Jeder unserer Ärzte ist ein zertifizierter Arzt für Allgemeinmedizin oder Facharzt aus Deutschland.'; 
$lang['PAYMENT_ST_h3'] = 'Unsere Webseite ist sicher & geschützt'; 
$lang['PAYMENT_ST_p'] = 'Wir verhindern Sicherheitslücken aktiv durch 24/7-Überwachung und 256-bit SSL Verschlüsselung.'; 
$lang['PAYMENT_P_h3'] = 'Datenschutz ist uns wichtig'; 
$lang['PAYMENT_P_p'] = 'Wir teilen niemals persönliche oder medizinische Daten mit anderen. Nur Ärzte erhalten Zugriff zu Ihren medizinischen Daten.'; 

$lang['PAYMENT_LOOKING']="Suche nach geeigneten Ärzten...";
$lang['PAYMENT_PROCESSING']="Bitte warten Sie einige Sekunden während wir Ihre Daten verarbeiten</br>und eine Auswahl von Ärzten aus Ihrem Bundesland zusammenstellen.";
$lang['PAYMENT_FOUND']="Wir haben geeignete Ärzte für Sie gefunden!";
$lang['PAYMENT_CHOOSE']="Bitte wählen Sie einen der vorgeschlagenen Ärzte";
$lang['PAYMENT_CHECKOUT'] = 'Bezahlung und Beginn Ihrer Konsultation'; 

$lang['PAYMENT_CC_DETAILS']="Zahlungsinformationen";
$lang['PAYMENT_CC_NUMBER']="Kartennummer";
$lang['PAYMENT_CC_EXPIRATION']="Gültig Bis";
$lang['PAYMENT_CC_CVC']="CVC Code";

$lang['PAYMENT_DOCTOR1_NAME']="Dr. Johanna F. B.";
$lang['PAYMENT_DOCTOR1_JOB1']="Krankenhaus (12 Jahre)";
$lang['PAYMENT_DOCTOR1_JOB2']="Privatpraxis (9 Jahre)";

$lang['PAYMENT_DOCTOR2_NAME']="Dr. Thomas H. R.";
$lang['PAYMENT_DOCTOR2_JOB1']="Humanitäre Hilfe (2 Jahre)";
$lang['PAYMENT_DOCTOR2_JOB2']="Privatpraxis (24 Jahre)";

$lang['PAYMENT_DOCTOR3_NAME']="Dr. Michael S.";
$lang['PAYMENT_DOCTOR3_JOB1']="Krankenhaus (8 Jahre)";

$lang['PAYMENT_DOCTOR4_NAME']="Dr. Abhiraj G.";
$lang['PAYMENT_DOCTOR4_JOB1']="Humanitäre Hilfe (3 Jahre)";
$lang['PAYMENT_DOCTOR4_JOB2']="Privatpraxis(2 Jahre)";

$lang['PAYMENT_COST']="Ihre Beratungsgebühr beträgt <span class=\"bold\"> nur €24";
$lang['PAYMENT_SECURE']="Sichere Bezahlung durch PayPal";
$lang['PAYMENT_CODE']="D68GP5TYG2DH2";
$lang['PAYMENT_BUTTON']="Jetzt bezahlen & Arzt treffen";

// Thank You Page--------------------
$lang['THANKYOU_TITLE'] = 'Qualified Medical Advice - Anywhere, Anytime, 24/7';
$lang['THANKYOU_DESCRIPTION'] = 'Ask our doctors and describe your symptoms.';	

$lang['THANKYOU_SENDING'] ="Daten werden sicher übermittelt";
$lang['THANKYOU_THANKS'] ="Vielen Dank!";
$lang['THANKYOU_RECEIVED'] ="Wir haben Ihre Zahlung erhalten. </br> Ihr ausgewählter Arzt wird Ihnen in Kürze antworten.";
$lang['THANKYOU_SUPPORT'] ="Bei weiteren Fragen steht Ihnen unser Serviceteam gerne zur Verfügung.";

// Sign Up Thank You Page
$lang['STHANKYOU_TITLE'] = 'Qualified Medical Advice - Anywhere, Anytime, 24/7';
$lang['STHANKYOU_DESCRIPTION'] = 'Ask our doctors and describe your symptoms.';	

$lang['STHANKYOU_SENDING'] ="Daten werden sicher übermittelt";
$lang['STHANKYOU_THANKS'] ="Vielen Dank!";
$lang['STHANKYOU_RECEIVED'] ="Wir informieren Sie gerne sobald Medmedo in Deutschland verfügbar ist";
$lang['STHANKYOU_SUPPORT'] ="Bei weiteren Fragen steht Ihnen unser Serviceteam jederzeit zur Verfügung.";

// Conditions--------------------

$lang['FEVER_CONTENT1'] ="Beschreibung ";
$lang['FEVER_CONTENT2'] ="Anzeichen &amp; Symptome";
$lang['FEVER_CONTENT3'] ="Ursachen";
$lang['FEVER_CONTENT4'] ="Diagnose";
$lang['FEVER_CONTENT5'] ="Behandlung";
$lang['FEVER_CONTENT6'] ="Komplikationen";







//Tests Template 

$lang['TEST_CONTENT1'] ="Beschreibung" ;
$lang['TEST_CONTENT2'] ="Testzwecke" ;
$lang['TEST_CONTENT3'] ="Normalbereiche";
$lang['TEST_CONTENT4'] ="Ergebnisse";


//Medication_name Template 
$lang['MED_NAME_CONTENT1'] ="Übersicht" ;
$lang['MED_NAME_CONTENT2'] ="Wichtige Informationen" ;




//Medication_type Template 


$lang['MED_TYPE_CONTENT1'] ="Übersicht" ;
$lang['MED_TYPE_CONTENT2'] ="Beispiele" ;
$lang['MED_TYPE_CONTENT3'] ="Anwendungen";


//Side bar 


$lang['SEXUAL_SIDEBAR_BUTTON'] ="Fragen Sie jetzt einen Arzt";




$lang['ASK'] = 'Fragen Sie einen Arzt' ;
$lang['DSBOX1_DOCTOR_img'] = '<img src="./images/docs/Doctor1.png" id="doc_img" alt="Sidebar arzt bild"/>';
$lang['DSBOX1_DOCTOR_txt'] = 'Dr. Thomas Penning';
$lang['DSBOX1_SP_img'] = '<img src="./images/family_medicine.png" id="specialty_img" width="12px" height="12px">';
$lang['DSBOX1_SP_txt'] = 'Allgemeinmedizin';
$lang['DSBOX1_EXP_h'] = 'Erfahrungen';
$lang['DSBOX1_EXP_txt'] = '23 years of practice';
$lang['DSBOX1_RATING_h'] = 'Rating';
$lang['DSBOX1_RATING_txt'] = '4.9 (147 Ratings)';





//doctors


$lang['DOC_HEADER'] ="Partnerärzte und Ärztebeirat";
$lang['DOC_TEXT'] ="Wir entwickeln Medmedo zusammen mit Ärzten aus allen Fachrichtungen. Wir setzen auf das Fachwissen und die Erfahrung unserer Ärzte, daher wird unser Ärzteteam stetig erweitert.";




$lang['DOC1_NAME'] ="Dr. med. Jessica Männel";
$lang['DOC1_SPEC'] ="Fachärztin für Allgemeinmedizin";


$lang['DOC2_NAME'] ="Dr. med. Wolf Siepen";
$lang['DOC2_SPEC'] ="Facharzt für orth. Chirugie und Traumatologie";

$lang['DOC3_NAME'] ="Dr. med. Kathrin Hamann";
$lang['DOC3_SPEC'] ="Fachärztin für Allgemeinmedizin";

$lang['DOC4_NAME'] ="Beate Broermann";
$lang['DOC4_SPEC'] ="Dipl. Psychologin";

$lang['DOC5_NAME'] ="Christian Welsch";
$lang['DOC5_SPEC'] ="Facharzt für HNO-Heilkunde";

$lang['DOC6_NAME'] ="Stefan Paffrath";
$lang['DOC6_SPEC'] ="Dipl. Psychologe";


$lang['DOC7_NAME'] ="Dr. med. Ive Schaaf";
$lang['DOC7_SPEC'] ="Fachärztin für Phlebologie";

$lang['DOC8_NAME'] ="Dr. med. Norbert Scheufele";
$lang['DOC8_SPEC'] ="Facharzt für Frauenheilkunde und Geburtshilfe";

$lang['DOC9_NAME'] ="Dr.rer.nat.Uta-Maria Weigel";
$lang['DOC9_SPEC'] ="Heilpraktikerin";


$lang['DOC10_NAME'] ="Dr. med. Joachim Nowak";
$lang['DOC10_SPEC'] ="Facharzt für Orthopädie
";

?>
