<?php 

$lang['PAYMENT_'] = "Paiement";

$lang['PAYMENT_HEADING'] = "Choissisez l'urgence et le niveau de détail";
$lang['PAYMENT_SUBHEADING'] = "Choose how fast you need the information and how detailed it should be.";


// CHOOSE URGENCY PANEL
$lang['PAYMENT_CHOOSEPANEL'] = "Urgence";

$lang['PAYMENT_CHOOSEPANEL1'] = "Niveau de détail";
$lang['PAYMENT_CHOOSEPANEL2'] = "Bas";
$lang['PAYMENT_CHOOSEPANEL3'] = "Moyen";
$lang['PAYMENT_CHOOSEPANEL4'] = "Élevé";
$lang['PAYMENT_CHOOSEPANEL5'] = "Somme á payer SI vous satisfait par la réponse :";


// DEPOSIT FEE

$lang['PAYMENT_DEPOSIT1'] = "Payer les frais de médecin (100% satisfaction garantie)";
$lang['PAYMENT_DEPOSIT2'] = "et obtenir votre réponse en temps réel maintenant";
$lang['PAYMENT_DEPOSIT22'] = "Place your deposit of";
$lang['PAYMENT_DEPOSIT3'] = "Paiement par PayPal";
$lang['PAYMENT_DEPOSIT4'] = "Tes données sont sécurisés";
$lang['PAYMENT_DEPOSIT5'] = "En cliquant sur continuer vous indiquez que vous avez accepté les termes et conditions du service et que vous avez plus de 18 ans
.";

$lang['PAYMENT_DEPOSIT6'] = "Continuer á la derniére étape.";


// SIDEBAR 

$lang['PAYMENT_SIDEHEAD1'] = "Réseau de Médecins et Conseillers ";

$lang['PAYMENT_SIDEHEAD2'] = "Un de nos experts qualifés vous répondra sous peu.";


$lang['PAYMENT_SIDE_DOC1HEAD1'] = "Dr. med. Kathrin Hamann";
$lang['PAYMENT_SIDE_DOC1HEAD2'] = "Médecin généraliste";
$lang['PAYMENT_SIDE_DOC1TEXT3'] = "<p>- 12 ans d'experience.</p>";

$lang['PAYMENT_SIDE_DOC2HEAD1'] = "Dr. med. Wolf Siepen";
$lang['PAYMENT_SIDE_DOC2HEAD2'] = "Orthopédie et traumatologie";
$lang['PAYMENT_SIDE_DOC2TEXT3'] = "<p>- 12 ans d'experience.</p>";

$lang['PAYMENT_SIDE_DOC3HEAD1'] = "Dr. med. Norbert Scheufele";
$lang['PAYMENT_SIDE_DOC3HEAD2'] = "Gynécologie et obstétrique";
$lang['PAYMENT_SIDE_DOC3TEXT3'] = "<p>- 20 ans d'experience.</p>";
$lang['pricess'] = '$';


$lang['PAYMENT_AGREEMENT1'] = 'J\'ai lu et j\'accepte la' ;
$lang['PAYMENT_AGREEMENT2'] = '<a href="privacy.php?lang=de" target="_blank" >déclaration de confidentialité</a> et les';
$lang['PAYMENT_AGREEMENT3'] = '<a href="terms.php?lang=de" target="_blank" >termes et conditions d\'utilisations</a>';
$lang['PAYMENT_AGREEMENT4'] = 'et j\'ai 18 ans ou plus.';


$lang['PAYMENT_NEW1'] = "Choissisez vos options";
$lang['PAYMENT_NEW2'] = "Un de nos experts est prêt á répondre vos questions. Nous fournissons <b>100% garantie de satisfaction</b>.";
$lang['PAYMENT_NEW3'] = 'Les frais des experts pour une information <b id="detailsswap">trés détaillée</b> et <b id="urgencyswap">rapide</b>';
$lang['PAYMENT_NEW4'] = "Un spécialiste est prêt á vous répondre";
$lang['PAYMENT_NEW5'] = "Dr. med. J. Männel";
$lang['PAYMENT_NEW6'] = "Médecin généraliste 12 ans d'experience.";
$lang['PAYMENT_NEW7'] = "EXPERT VÉRIFIÉ";
$lang['PAYMENT_NEW8'] = "Évaluation";
$lang['PAYMENT_NEW9'] = "<b>4.85 / 5</b>";
$lang['PAYMENT_NEW10'] = "<b>Garantie de remboursement </b><br>Votre satisfaction est sûre, Vous payez seulement si vous êtes 100% satisfait. ";
$lang['PAYMENT_DOC_IMG'] = '<img src="images/payment/doc.png" alt="#"/>';


$lang['3step_1'] = "Décrivez vos symptômes";
$lang['3step_2'] = "Payer les frais de consultation";
$lang['3step_3'] = "Obtenir des conseils qualifiés";

$lang['DEP_SELECT'] = 'Choisir';
$lang['DEP_METHOD'] = 'Méthode de paiement';
$lang['DEP_CC'] = 'Carte de crédit';
$lang['DEP_PP'] = 'PayPal';
$lang['DEP_PP_TEXT'] = 'Vous serez dirigé á PayPal.';
$lang['DEP_CC_NR'] = 'Numéro de la carte';
$lang['DEP_CC_EXP'] = 'Date \'expiration';
$lang['DEP_CC_MONTH'] = '<span id="month">Mois</span>
						<ul class="dropdown">
							<li><a href="#">01 - Jan</a></li>
							<li><a href="#">02 - Feb</a></li>
							<li><a href="#">03 - Mar</a></li>
							<li><a href="#">04 - Apr</a></li>
							<li><a href="#">05 - May</a></li>
							<li><a href="#">06 - Jun</a></li>
							<li><a href="#">07 - Jul</a></li>
							<li><a href="#">08 - Aug</a></li>
							<li><a href="#">09 - Sep</a></li>
							<li><a href="#">10 - Oct</a></li>
							<li><a href="#">11 - Nov</a></li>
							<li><a href="#">12 - Dec</a></li>							
						</ul>';
$lang['DEP_CC_YEAR'] = '<span id="year">Année</span>
						<ul class="dropdown">
							<li><a href="#">2015</a></li>							
							<li><a href="#">2016</a></li>							
							<li><a href="#">2017</a></li>							
							<li><a href="#">2018</a></li>							
							<li><a href="#">2019</a></li>							
							<li><a href="#">2020</a></li>							
							<li><a href="#">2021</a></li>							
							<li><a href="#">2022</a></li>							
							<li><a href="#">2023</a></li>							
							<li><a href="#">2024</a></li>							
						</ul>';
$lang['DEP_CC_CVC'] = 'Code de sécurité';
$lang['DEP_SECURE'] = 'Vos données sont sécurisés.';
$lang['DEP_PRIVACY'] = 'Confidentialité';


?>