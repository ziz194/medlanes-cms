<?php

$lang['OPT_HEADING'] = 'Informations secondaires';
$lang['OPT_SUBHEAD'] = 'Votre médecin veut mieux vous répondre. en ajoutant des informations supplémentaires, votre question serais plus détaillées et rapide.';
$lang['OPT_SKIP'] = 'Passer cette étape';
$lang['OPT_SKIP2'] = 'Passer';
$lang['OPT_NAME'] = 'Nom:';
$lang['OPT_GENDER'] = 'Sexe:';
$lang['OPT_SELECT'] = 'Choisir';
$lang['OPT_MALE'] = 'Homme';
$lang['OPT_FEMALE'] = 'Femme';
$lang['OPT_AGE'] = 'Age';
$lang['OPT_PHOTOS'] = 'Joindre des photos';
$lang['OPT_FILES'] = 'Ajouter des fichiers';
$lang['OPT_MED_HISTORY'] = 'Entrez des informations sur votre histoire médicale (par exemple, les allergies, l\'hypertension artérielle, la BPCO, asthme, etc.)';
$lang['OPT_MEDICATION'] = 'Liste des médicaments fréquemment prises:';
$lang['OPT_SYMPTOMS'] = 'Depuis quand les symptômes commencent á se produisent?';

$lang['OPT_SYMPTOMS_DD'] = '<span id="sincewhen">Choisir</span>
					<ul class="dropdown">
						<li><a href="#">Aigu</a></li>
						<li><a href="#">Aujourd\'hui</a></li>
						<li><a href="#">Cette semaine</a></li>
						<li><a href="#">Ce mois</a></li>
						<li><a href="#">Chronique</a></li>
					</ul>';
$lang['OPT_CONTINUE'] = 'Continuer';



$lang['PAYMENT_NEW1'] = "Choissisez vos options";
$lang['PAYMENT_NEW2'] = "Un de nos experts est prêt á répondre vos questions. Nous fournissons <b>100% garantie de satisfaction</b>.";
$lang['PAYMENT_NEW3'] = 'Les frais des experts pour une information <b id="detailsswap">trés détaillée</b> et <b id="urgencyswap">rapide</b>';
$lang['PAYMENT_NEW4'] = "Un spécialiste est prêt á vous répondre";
$lang['PAYMENT_NEW5'] = "Dr. med. J. Männel";
$lang['PAYMENT_NEW6'] = "Médecin généraliste 12 ans d'experience.";
$lang['PAYMENT_NEW7'] = "EXPERT VÉRIFIÉ";
$lang['PAYMENT_NEW8'] = "Évaluation";
$lang['PAYMENT_NEW9'] = "<b>9,7 of 10</b> (Sanego)";
$lang['PAYMENT_NEW10'] = "<b>Garantie de remboursement </b><br>Votre satisfaction est sûre, Vous payez seulement si vous êtes 100% satisfait. ";




?>