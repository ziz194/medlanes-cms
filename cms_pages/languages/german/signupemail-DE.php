<?php 
$lang['SIGNUPEMAIL_SUBJECT'] = "Willkommen bei Medmedo!";
$lang['SIGNUPEMAIL_HEADLINE'] = "Danke für Ihr Interesse an Medmedo.";
$lang['SIGNUPEMAIL_MAIN'] = "Hallo und willkommen bei Medmedo! <br><br>Medmedo ist bequem, schnell und kostengünstig. Unsere qualifizierten Ärzte beantworten Ihre Fragen überall und rund um die Uhr. <br><br>Nach Zahlung einer geringen Servicegebühr wird sich einer unserer Experten in Kürze bei Ihnen melden. <br> Wir informieren Sie gerne, sobald unser günstiger Einsteigerservice für Sie verfügbar ist.";
$lang['SIGNUPEMAIL_SIGNATURE'] = "Herzliche Grüße, <br>Ihr Medlanes Team";
$lang['SIGNUPEMAIL_FOOTER'] = "Haben Sie Fragen oder Anregungen? Antworten Sie einfach direkt auf diese eMail!";
?>