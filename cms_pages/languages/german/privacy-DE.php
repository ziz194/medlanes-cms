<?php 


// PRIVACY HEADING

$lang['PRIVACY_HEADING1'] = 'Datenschutz';


// PRIVACY PARAG

$lang['PRIVACY_1'] = '
				<p><b>§ 1 Erhebung personenbezogener Daten </b></p>
				<p>Um die nach § 2 AGB vereinbarten Leistungen erbringen zu können, erhebt, verarbeitet und nutzt die</p>
				<div class="two" style="float:left;width:100%;margin-top:20px;margin-bottom:20px">
					<p>Medlanes GmbH, vertreten durch die Geschäftsführer Dr. Emil Kendziorra und Erik Stoffregen</p>
					<p>Oranienstr. 185, 10999 Berlin</p>
					<p>Telefon: +49 (0)30 577 004 20</p>
					<p>Telefax: +49 (0)6151 62749 799 </p>
					<p>E-Mail: info(at)medlanes.com </p>
					<p style="margin-top:20px">– nachfolgend ”Medlanes” –</p>
					<p>die von dem Nutzer angegebenen Daten, sowie die IP-Adresse von dessen Endgerät (nachfolgend „personenbezogene Daten”).</p></div>
';
$lang['PRIVACY_2'] = '
				<p><b>§ 2 Zweck der Erhebung, Verarbeitung und Nutzung </b></p>
				<p>(1)	Die personenbezogenen Daten erhebt, verarbeitet und nutzt Medlanes ausschließlich zu dem Zweck, die mit dem Nutzer im Sinne des § 2 AGB vereinbarten Leistungen zu erbringen.</p>
				<p>(2)	Die personenbezogenen Daten werden ausschließlich auf Servern von Medlanes in Deutschland gespeichert und verarbeitet. </p>
				<p>(3)	Medlanes ermöglicht es dem Nutzer, seine Zugangsdaten, die vor jedem neuen Besuch von Medlanes im Rahmen des log-in anzugeben sind, mittels eines sog. Cookies auf dem jeweiligen Rechner des Nutzers zu hinterlegen, um so den Anmeldevorgang zu erleichtern. Der Nutzer kann die Installation des Cookies auf seinem Rechner durch eine entsprechende Einstellung des Interbrowsers verhindern bzw. diesen Cookie jederzeit entfernen. Medlanes weist jedoch darauf hin, dass in diesem Fall eine automatische Anmeldung ohne erneute Eingabe der Zugangsdaten des Nutzers nicht möglich ist.</p>
';
$lang['PRIVACY_3'] = '
				<p><b>§ 3 Schutz der Privatsphäre </b></p>
				<p>Medlanes verpflichtet sich, die Privatsphäre der Nutzer zu schützen und versichert, die personenbezogenen Daten im Einklang mit dem Bundesdatenschutzgesetz und dem Telemediengesetz zu erheben, zu verarbeiten und zu nutzen und ausschließlich für die Erfüllung der unter § 2 definierten Zwecke zu verarbeiten und zu nutzen. Medlanes wird seine Mitarbeiter entsprechend verpflichten.</p>
';
$lang['PRIVACY_4'] = '
				<p><b>§ 4 Rechte des Nutzers</b></p>
				<p>Der Nutzer hat bezüglich der personenbezogenen Daten die durch das Bundesdatenschutzgesetz gewährleisteten Rechte auf Auskunft, Berichtigung und Löschung. Diese Rechte kann der Nutzer per Post oder Email gegenüber Medlanes oder gegenüber dem unter § 6 genannten Ansprechpartner ausüben. </p>
';
$lang['PRIVACY_5'] = '
				<p><b>§ 5 Einwilligung </b></p>
				<p>Sofern der Nutzer in die Bedingungen dieser Erklärung einwilligt, erklärt er sich mit der geregelten Nutzung der personenbezogenen Daten einverstanden. Die Einwilligung oder ihre Verweigerung erfolgt durch Klicken oder Nichtklicken auf das entsprechende Feld nach dem Hinweis auf diese Bestimmungen. Die erteilte oder verweigerte Einwilligung wird protokolliert; ihr Inhalt ist für den Nutzer jederzeit unter (Link) abrufbar. Der Nutzer hat das Recht, jederzeit seine Einwilligung zu widerrufen und damit sein Konto mit allen Daten aufzulösen und zu löschen.</p>
';
$lang['PRIVACY_6'] = '
				<p><b>§ 6 Bezahlung</b></p>
				<p>Transaktionsdaten, die potentiell auch persönliche Daten enthalten können, werden zum Zweck der Prozessierung der Bezahlung an PayLane Sp. z o.o., Adresse: Gdańsk at Arkońska 6/A3, zip code: 80-387, Firmennummer: 0000227278, übertragen.</p>
';

$lang['PRIVACY_7'] = '
				<p><b>§ 7 Ansprechpartner </b></p>
				<p>Ansprechpartner für sämtliche datenschutzbezogenen Fragen und Bitten sowie für die Ausübung der unter § 4 beschriebenen Rechte ist Erik Stoffregen. </p>
';
?>