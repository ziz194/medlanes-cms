<?php 

$lang['SERVICE_'] = "";


/*SERVICE MAIN HEADING*/
$lang['SERVICE_MAINHEADING'] = "Ein Arzt in der Tasche,  <br> überall und jederzeit!";

// SERVICE SUB MENU

$lang['SERVICE_SUBMENU1'] = "So funktioniert's";
$lang['SERVICE_SUBMENU2'] = "Warum Medmedo?";
$lang['SERVICE_SUBMENU3'] = "Nutzungsbeispiele";
$lang['SERVICE_SUBMENU4'] = "Preis & Empfehlungen";
// HOW

$lang['SERVICE_HOW1'] = "So funktioniert Medmedo";
$lang['SERVICE_HOW2'] = "Mit Medmedo werden Sie von zertifizierten Allgemein- oder Fachärzten beraten. Erhalten Sie qualifizierten, medizinischen Rat innerhalb von 24 Stunden in nur drei einfachen Schritten. Senden Sie uns Ihre Symptome und Fragen über unser Webformular und legen Sie selbst je nach Dringlichkeit und Detail einen Betrag fest. Unsere Ärzte werten Ihre Daten aus und senden Ihnen innerhalb von 24 Stunden eine Antwort. Ergeben sich daraus weitere Fragen, beantworten unsere Ärzte diese natürlich gerne.";


$lang['SERVICE_HOW_HEAD1'] = "Beschreiben Sie Ihre Symptome";
$lang['SERVICE_HOW_TEXT1'] = "Beschreiben Sie Ihre Symptome und stellen Sie Ihre Fragen einfach und schnell über unser Webformular. Sie haben auch die Möglichkeit, Bilder hochzuladen.";

$lang['SERVICE_HOW_HEAD2'] = "Bezahlung";
$lang['SERVICE_HOW_TEXT2'] = "Legen Sie je nach Dringlichkeit und Detailgrad selbst einen Betrag fest. Konnte Ihnen unser Arzt nicht helfen, erstatten wir Ihnen den vollen Betrag.";

$lang['SERVICE_HOW_HEAD3'] = "Qualifizierte medizinische Expertenmeinung";
$lang['SERVICE_HOW_TEXT3'] = "Ein qualifizierter und zertifizierter Arzt wird Ihre Fragen innerhalb von 24 Stunden beantworten. Holen Sie sich eine Expertenmeinung auf Knopfdruck!";


// WHY

$lang['SERVICE_WHY_HEADING'] = "Warum Medmedo?";

$lang['SERVICE_WHY_HEAD1'] = "8 min.";
$lang['SERVICE_WHY_TEXT1'] = "Durchschnittliche Zeit, die ein Arzt für Sie hat.";

$lang['SERVICE_WHY_HEAD2'] = "14 Tage";
$lang['SERVICE_WHY_TEXT2'] = "So lange warten Sie im Durchschnitt auf einen Termin.";

$lang['SERVICE_WHY_HEAD3'] = "3 Std.";
$lang['SERVICE_WHY_TEXT3'] = "Die Zeit, die Sie sich für einen Termin inkl. Anreise nehmen müssen.";

$lang['SERVICE_WHY_HEAD4'] = "80%";
$lang['SERVICE_WHY_TEXT4'] = "Anteil der Fragen, die man in den meisten Fachgebieten auch online beantworten kann.";

$lang['SERVICE_WHY_HEAD5'] = "50%";
$lang['SERVICE_WHY_TEXT5'] = "Anteil der Patienten, die von einem besseren Rat profitieren würden.";


/*SERVICE TABS*/
$lang['SERVICE_TABS_HEAD'] = "Mit Medmedo weniger...";

$lang['SERVICE_TABS_MAIN1'] = "Warten";
$lang['SERVICE_TABS_TEXT1'] = "Warten Sie nicht länger auf Ihren Arzttermin oder im Wartezimmer. Mit Medmedo erhalten Sie medizinische Hilfestellung 24 Stunden am Tag, 7 Tage die Woche.";

$lang['SERVICE_TABS_MAIN2'] = "Zahlen";
$lang['SERVICE_TABS_TEXT2'] = "Durch einen Arztbesuch können indirekte Kosten entstehen. Mit Medmedo sparen Sie sich unnötige Wege zum Arzt und verlieren keine Arbeitszeit.";

$lang['SERVICE_TABS_MAIN3'] = "Stress";
$lang['SERVICE_TABS_TEXT3'] = "Medizinische Begutachtung durch Medmedo ist bequem! Sparen Sie sich den Aufwand, Ihren Arzttermin Wochen im Voraus zu planen. Medmedo ist überall und jederzeit verfügbar - auf Ihrem Smartphone oder im Web.";

$lang['SERVICE_TABS_MAIN4'] = "Sorgen";
$lang['SERVICE_TABS_TEXT4'] = "Machen Sie sich keine Sorgen mehr über Ihre Gesundheit! Wenden Sie sich direkt an unsere Ärzte - auch bei Gesundheitsfragen zu Ihrer Familie. Mit weniger Sorgen gesünder leben und weniger krank sein!";

/*ALWAYS HERE*/

$lang['SERVICE_ALWAYS_HEADING1'] = "Nutzungsbeispiele";
$lang['SERVICE_ALWAYS_HEADING2'] = "Medmedo ist immer und überall für Sie verfügbar. Wir kombinieren die Einfachheit einer Onlinesuche mit der Qualität und Verlässlichkeit eines Besuches beim Arzt. Stellen Sie innerhalb von wenigen Minuten Ihre Fragen, laden ein Foto hoch und beantworten einige Rückfragen. Medmedo findet den richtigen Arzt für Sie und dieser antwortet in Kürze!";

$lang['SERVICE_ALWAYS_HEAD1'] = "Im Urlaub";
$lang['SERVICE_ALWAYS_TEXT1'] = "Im Ausland ist es oft schwer einen Arzt zu finden, der Sie versteht. Nutzen Sie Medmedo!";

$lang['SERVICE_ALWAYS_HEAD2'] = "Bei der Arbeit";
$lang['SERVICE_ALWAYS_TEXT2'] = "Verlieren Sie keine kostbare Arbeitszeit und holen Sie sich einen ersten Rat online.";

$lang['SERVICE_ALWAYS_HEAD3'] = "In der Nacht";
$lang['SERVICE_ALWAYS_TEXT3'] = "Medlanes ist nicht nur tagsüber verfügbar. Stellen Sie jederzeit Ihre Fragen und ein Arzt antwortet umgehend.";

$lang['SERVICE_ALWAYS_HEAD4'] = "Am Wochenende";
$lang['SERVICE_ALWAYS_TEXT4'] = "Praktisch alle Ärzte haben am Wochenende geschlossen! Medmedo beantwortet Ihre Fragen auch am Wochenende.";

$lang['SERVICE_ALWAYS_HEAD5'] = "Wenn's einfach gehen muss";
$lang['SERVICE_ALWAYS_TEXT5'] = "Nutzen Sie Ihr Smartphone oder Ihren PC und warten Sie nicht lange auf Antworten.";

$lang['SERVICE_ALWAYS_HEAD6'] = "Bei langen Entfernungen";
$lang['SERVICE_ALWAYS_TEXT6'] = "Vor allem auf dem Land ist der Arzt oft schwer zu erreichen. Nutzen Sie Medmedo für kurze medizinische Fragen!";


// PRICE

$lang['SERVICE_PRICE_HEADING1'] = "Preis & Empfehlungen";
$lang['SERVICE_PRICE_HEADING2'] = "Medmedo ist kostengünstig! Durch einen Arztbesuch können indirekte Kosten entstehen. Mit Medmedo sparen Sie Sie sich unnötige Wege zum Arzt und verlieren keine Arbeitszeit. Medmedo macht Medizin für jedermann bezahlbar!";

$lang['SERVICE_PRICE_1'] = "Qualifizierte Expertenmeinung";
$lang['SERVICE_PRICE_2'] = "Antworten per eMail inkl. bis zu drei weitere Rückfragen an unsere zertifizierten Allgemein- oder Fachärzte aus Deutschland.";
$lang['SERVICE_PRICE_3'] = "ab €15";

// USECASES REPLICATED FROM INDEX ONES

?>