<?php 

$lang['PAYMENTEMAIL_SUBJECT'] = "Wichtig: Ihre Frage an Medmedo";
$lang['PAYMENTEMAIL_HEADLINE'] = "Vielen Dank, dass Sie unseren Service nutzen";
$lang['PAYMENTEMAIL_MAIN'] = "Wir haben Ihre Frage erhalten! <br><br> Ein zuständiger Arzt wird Ihre Frage so schnell wie möglich beantworten. <br><br>Sollte jedoch wider Erwarten kein Arzt zur Beantwortung Ihrer Frage verfügbar sein, erstatten wir umgehend den vollen Betrag.";
$lang['PAYMENTEMAIL_SIGNATURE'] = "Herzliche Grüße, <br>Ihr Medlanes Team";
$lang['PAYMENTEMAIL_FOOTER'] = "Haben Sie Fragen oder Anregungen? Antworten Sie einfach auf diese eMail!";

?>