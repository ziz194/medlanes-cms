<?php 

$lang['PAYMENT_'] = "Bezahlung";

$lang['PAYMENT_HEADING'] = "Wählen Sie die Dringlichkeit und den Detailgrad aus";
$lang['PAYMENT_SUBHEADING'] = "Bitte legen Sie die Dringlichkeit und den Detailgrad fest.";


// CHOOSE URGENCY PANEL
$lang['PAYMENT_CHOOSEPANEL'] = "Dringlichkeit";

$lang['PAYMENT_CHOOSEPANEL1'] = "Detailgrad";
$lang['PAYMENT_CHOOSEPANEL2'] = "Niedrig";
$lang['PAYMENT_CHOOSEPANEL3'] = "Mittel";
$lang['PAYMENT_CHOOSEPANEL4'] = "Hoch";
$lang['PAYMENT_CHOOSEPANEL5'] = "Betrag, den Sie nur zahlen, WENN Sie mit der Antwort zufrieden waren:
";


// DEPOSIT FEE

$lang['PAYMENT_DEPOSIT1'] = "Bezahlung (100% Kundenzufriedenheit)";
$lang['PAYMENT_DEPOSIT2'] = "Leisten Sie einen Betrag von";
$lang['PAYMENT_DEPOSIT22'] = " und Sie erhalten Ihre Antwort sofort.";
$lang['PAYMENT_DEPOSIT3'] = "Bezahlung via PayPal";
$lang['PAYMENT_DEPOSIT4'] = "Ihre Informationen sind geschützt.";
$lang['PAYMENT_DEPOSIT5'] = "Mit “Fortsetzen” bestätigen Sie die AGBs und die Altersbestimmungen (18+).";
$lang['PAYMENT_DEPOSIT6'] = "Weiter zum letzten Schritt.";


// SIDEBAR 

$lang['PAYMENT_SIDEHEAD1'] = "Ärztenetzwerk";
$lang['PAYMENT_SIDEHEAD2'] = "Ein qualifizierter Arzt aus unserem Netzwerk antwortet in Kürze auf Ihre Frage.";


$lang['PAYMENT_SIDE_DOC1HEAD1'] = "Dr. med. Kathrin Hamann";
$lang['PAYMENT_SIDE_DOC1HEAD2'] = "Fachärztin für Allgemeinmedizin";
$lang['PAYMENT_SIDE_DOC1TEXT3'] = "<p>- 12 Jahre Praxiserfahrung</p>";

$lang['PAYMENT_SIDE_DOC2HEAD1'] = "Dr. med. Wolf Siepen";
$lang['PAYMENT_SIDE_DOC2HEAD2'] = "Facharzt für orth. Chirugie und Traumatologie";
$lang['PAYMENT_SIDE_DOC2TEXT3'] = "<p>- 12 Jahre Praxiserfahrung</p>";

$lang['PAYMENT_SIDE_DOC3HEAD1'] = "Dr. med. Norbert Scheufele";
$lang['PAYMENT_SIDE_DOC3HEAD2'] = "Facharzt für Frauenheilkunde und Geburtshilfe";
$lang['PAYMENT_SIDE_DOC3TEXT3'] = "<p>- 20 Jahre Praxiserfahrung</p>";
$lang['pricess'] = '€';


$lang['PAYMENT_AGREEMENT1'] = 'Ich habe die ';
$lang['PAYMENT_AGREEMENT2'] = '<a href="terms.php?lang=de" target="_blank" >AGBs</a> und die';
$lang['PAYMENT_AGREEMENT3'] = '<a href="privacy.php?lang=de" target="_blank" >Datenschutzbestimmungen</a>';
$lang['PAYMENT_AGREEMENT4'] = 'gelesen und akzeptiere sie. Ich bin über 18 Jahre alt.';




$lang['PAYMENT_NEW1'] = "Wählen Sie jetzt Ihre Optionen";
$lang['PAYMENT_NEW2'] = "Einer unserer Experten ist bereit, Ihre Fragen zu beantworten. Wir garantieren <b>100% Kundenzufriedenheit.";
$lang['PAYMENT_NEW3'] = 'Expertenhonorar für <b id="detailsswap">detailierte</b> und <b id="urgencyswap">dringende</b>Informationen:';
$lang['PAYMENT_NEW4'] = "Ein Facharzt ist für Sie bereit";
$lang['PAYMENT_NEW5'] = "Dr. med. J. Männel";
$lang['PAYMENT_NEW6'] = "Allgemeinärztin mit 12 Jahren Praxiserfahrung";
$lang['PAYMENT_NEW7'] = "GEPRÜFTE EXPERTIN";
$lang['PAYMENT_NEW8'] = "Bewertung";
$lang['PAYMENT_NEW9'] = "<b>9,7 von 10</b> (Sanego)";
$lang['PAYMENT_NEW10'] = "<b>Geld-Zurück-Garantie</b><br>Wir garantieren 100% Kundenzufriedenheit. Sie bezahlen nur, wenn Sie zufrieden sind. ";

$lang['3step_1'] = "Frage stellen";
$lang['3step_2'] = "Betrag wählen";
$lang['3step_3'] = "Antwort erhalten";

?>