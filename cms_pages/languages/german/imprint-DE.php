<?php 

$lang['IMPRINT_1'] = "Impressum";
$lang['IMPRINT_2'] = "Hinweise gem. § 6 des Teledienstegesetzes (TDG) in der Fassung des Gesetzes über rechtliche Rahmenbedingungen für den elektronischen Geschäftsverkehr vom 20.12.2001.";

$lang['IMPRINT_3'] = "Medlanes GmbH";
$lang['IMPRINT_4'] = "Oranienstr. 185";
$lang['IMPRINT_5'] = "10999 Berlin";
$lang['IMPRINT_6'] = "Tel. +49 (30) 577 004 20";
$lang['IMPRINT_7'] = "E-Mail: info@medlanes.com";
$lang['IMPRINT_8'] = "Geschäftsführer: Dr. Emil Kendziorra, Erik Stoffregen";
$lang['IMPRINT_9'] = "<b>Internetredaktion/Autor</b>";
$lang['IMPRINT_10'] = "(Inhaltlich Verantwortlicher gem. § 10 Abs. 3 MDStV und gem.§ 6 MDStV [Staatsvertrag über Mediendienste]:";
$lang['IMPRINT_11'] = "Dr. Emil Kendziorra";
$lang['IMPRINT_11-1'] = "Handelsregiester: HRB159023B (Amtsgericht Charlottenburg (Berlin)";
$lang['IMPRINT_11-2'] = "Ust.-ID.: DE295684112";
$lang['IMPRINT_11-3'] = "Rechtliche und medizinrechtliche Beratung: Vorberg & Partner (Partnergesellschaft), Vorsetzen 41, 20459 Hamburg - Tel.: 040-44140080";
$lang['IMPRINT_12'] = "				<p><b>Haftungshinweis:</b></p>
				<p>Alle Texte und Bilder sind urheberrechtlich geschützt. Die Veröffentlichung, Übernahme oder Nutzung von Texten, Bildern oder anderen Daten bedürfen der schriftlichen Zustimmung des Herausgebers – eine Ausnahme besteht bei Presseinformationen und den dort veröffentlichten Bildern. Die inhaltliche Verantwortung erfolgt durch die jeweils die Autoren. Trotz aller Bemühungen um möglichst korrekte Darstellung und Prüfung von Sachverhalten sind Irrtümer oder Interpretationsfehler möglich.</p>
				<br>
				<p>Hiermit distanziert sich der Herausgeber ausdrücklich von allen Inhalten aller gelinkten Seiten auf der Homepage eierfabrik.de und macht sich ihren Inhalt nicht zu Eigen! Diese Erklärung gilt für alle auf dieser Homepage angebrachten Links. (Urteil zur “Haftung für Links” am Landgericht Hamburg vom 12. Mai 1998, AZ: 312 O 85/98)</p>
				<br>
				<p>Der Herausgeber nimmt den Schutz Ihrer personenbezogenen Daten sehr ernst. Wir verarbeiten personenbezogene Daten, die beim Besuch auf unserer Webseiten erhoben werden, unter Beachtung der geltenden datenschutzrechtlichen Bestimmungen. Ihre Daten werden von uns weder veröffentlicht, noch unberechtigt an Dritte weitergegeben..</p>";
$lang['IMPRINT_13'] = "				<p><b>Disclaimer</b></p>
				<p>Diese Website wurde mit größtmöglicher Sorgfalt erstellt. Die Medlanes GmbH i.G. übernimmt jedoch keine Garantie für die Vollständigkeit, Richtigkeit und Aktualität der enthaltenen Informationen. Jegliche Haftung für Schäden, die direkt oder indirekt aus der Benutzung dieser Website entstehen, wird ausgeschlossen. Dies gilt auch für Links, auf die diese Website direkt oder indirekt verweist. Die Solidmedia hat keinen Einfluss auf die Gestaltung und die Inhalte der von uns gelinkten Seiten. Deshalb distanzieren wir uns hiermit ausdrücklich von allen Inhalten aller gelinkten Seiten fremder Anbieter. </p>
				<br>
				<p>Bei Falschinformationen oder Fehlern bitten wir Sie, uns dies mitzuteilen. </p>
				<br>
				<p>Diese Website benutzt Google Analytics, einen Webanalysedienst der Google Inc. („Google“). Google Analytics verwendet sog. „Cookies“, Textdateien, die auf Ihrem Computer gespeichert werden und die eine Analyse der Benutzung der Website durch Sie ermöglichen. Die durch den Cookie erzeugten Informationen über Ihre Benutzung dieser Website (einschließlich Ihrer IP-Adresse) wird an einen Server von Google in den USA übertragen und dort gespeichert. Google wird diese Informationen benutzen, um Ihre Nutzung der Website auszuwerten, um Reports über die Websiteaktivitäten für die Websitebetreiber zusammenzustellen und um weitere mit der Websitenutzung und der Internetnutzung verbundene Dienstleistungen zu erbringen. Auch wird Google diese Informationen gegebenenfalls an Dritte übertragen, sofern dies gesetzlich vorgeschrieben oder soweit Dritte diese Daten im Auftrag von Google verarbeiten. Google wird in keinem Fall Ihre IP-Adresse mit anderen Daten von Google in Verbindung bringen. Sie können die Installation der Cookies durch eine entsprechende Einstellung Ihrer Browser Software verhindern; wir weisen Sie jedoch darauf hin, dass Sie in diesem Fall gegebenenfalls nicht sämtliche Funktionen dieser Website vollumfänglich nutzen können. Durch die Nutzung dieser Website erklären Sie sich mit der Bearbeitung der über Sie erhobenen Daten durch Google in der zuvor beschriebenen Art und Weise und zu dem zuvor benannten Zweck einverstanden.</p>";

?>