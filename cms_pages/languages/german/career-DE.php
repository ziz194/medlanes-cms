<?php 

$lang['CAREER_'] = "";

// HEADING 

$lang['CAREER_HEADING'] = "Wir revolutionieren Medizin";


// SUB MENU

$lang['CAREER_SUBMENU_LINK1'] = "Visionen & Werte";
$lang['CAREER_SUBMENU_LINK2'] = "Unser Team";
$lang['CAREER_SUBMENU_LINK3'] = "Offene Stellen";
$lang['CAREER_SUBMENU_LINK4'] = "Bilder & Standort";


// SUBHEADING CAREER

$lang['CAREER_SUBHEADING1'] = "Wir sind Medlanes, sei dabei!";
$lang['CAREER_SUBHEADING2'] = "Wir bei Medlanes wollen das Gesundheitswesen revolutionieren und bringen zuverlässige medizinische Beratung online, leicht zugänglich und erschwinglich. Wir kombinieren die Bequemlichkeit des Webs mit der Qualität eines Arztbesuchs und ermöglichen somit sofortigen Zugriff auf die Medizin, überall und jederzeit.";
$lang['CAREER_SUBHEADING3'] = "Unser internationales Team arbeitet täglich Hand in Hand, um diesen bahnbrechenden Plan zu verwirklichen. Wir alle haben unterschiedliche Hintergründe, jedoch das gleiche Ziel: wir bringen Veränderung in die Welt der Medizin! Wir haben uns dem Lernen, Fortschritt und Erfolg verpflichtet. Jeder Tag bringt eine neue, spannende Herausforderung mit verschiedenen Projekten und einer Vielzahl von anspruchsvollen Aufgaben.";
$lang['CAREER_SUBHEADING4'] = "Wenn Du auch ein Teil unseres jungen, energischen und harmonischen Teams werden willst, dann sprich uns an! Wir sind immer auf der Suche nach neuen begeisterten Medlanern! Bewirb Dich direkt auf unsere offenen Stellen oder initiativ. Wir freuen uns, Dich kennen zu lernen!";


// VISION

$lang['CAREER_VISION_1'] = "Unsere Vision ist es, Medizin online zu bringen und das sind unsere Werte";

$lang['CAREER_VISION_2'] = "Einfluss!";
$lang['CAREER_VISION_3'] = "Wir wollen Einfluss nehmen!";
$lang['CAREER_VISION_4'] = "Wir wollen revolutionieren!";

$lang['CAREER_VISION_5'] = "Ergebnisse";
$lang['CAREER_VISION_6'] = "Wir sind ergebnisorientiert";
$lang['CAREER_VISION_7'] = "in allem, was wir tun";

$lang['CAREER_VISION_8'] = "Zusammenarbeit";
$lang['CAREER_VISION_9'] = "Wir sind ein Team und";
$lang['CAREER_VISION_10'] = "wir arbeiten als Team";

$lang['CAREER_VISION_11'] = "Feedback & Fortschritt";
$lang['CAREER_VISION_12'] = "Wir legen großen Wert auf ehrliches Feedback und";
$lang['CAREER_VISION_13'] = "wollen uns ständig weiter entwickeln";

// PERKS

$lang['CAREER_PERKS'] = "Die Perks";

$lang['CAREER_PERKS_TEXT1'] = "Jeden Montag beginnen wir die Woche mit einem gemütlichen Frühstück und Geschichten aus dem Wochenende.";

$lang['CAREER_PERKS_TEXT2'] = "M für ... multikulturell! Medlanes beschäftigt ein internationales Team im multikulturellen Berlin Kreuzberg.";

$lang['CAREER_PERKS_TEXT3'] = "Wolltest Du schon immer ein MVP sein? Jeden Monat krönen wir die beste Leistung mit einer Kleinigkeit!";

$lang['CAREER_PERKS_TEXT4'] = "TGIF! Jede Woche lassen wir mit einem kühlen Getränk ausklingen! Jeden letzten Freitag des Monats wächst das Team durch verschiedene Events enger zusammen.";

$lang['CAREER_PERKS_TEXT5'] = "Medlanes sorgt dafür, dass jedes Mitglied des Teams durch anspruchsvolle Aufgaben und regelmäßiges Feedback wächst. ";

$lang['CAREER_PERKS_TEXT6'] = "Zum ersten Mal in Berlin? Keine Sorge! Ein Willkommenspaket mit allen relevanten Informationen und ein paar netten Überraschungen wartet auf Dich!";

$lang['CAREER_PERKS_TEXT7'] = "Wir lassen keine Möglichkeit aus, um zu feiern: Geburtstage bleiben nicht unbemerkt! Auch Feiertage kommen nicht zu kurz... Halloween-Kostüm-Wettbewerb, Wichteln jemand?";

$lang['CAREER_PERKS_TEXT8'] = "Ein konstruktiver Bewertungsbogen von den Gründern hilft Dir, Deine Stärken, Schwächen, Leistungen und Fortschritte zu erkennen.";


// EMPLOYE STORIES
$lang['CAREER_EMPLOYE'] = "Mitarbeiterstories";
// EMPLOYE 1
$lang['CAREER_EMPLOYE_TEXT1'] = "“Medlanes gibt mir die Freiheit, neue Dinge auszuprobieren und mein eigenes Potenzial voll auszutesten. Ich habe die Möglichkeit, spannende Projekte anzupacken und mich unter internationalen und qualifizierten Kollegen weiter zu entwickeln.“ ";
$lang['CAREER_EMPLOYE_NAME1'] = "STEPHEN, MARKETING";
// EMPLOYE 2
$lang['CAREER_EMPLOYE_TEXT2'] = "“HR in einem Startup ist sehr aufregend! Ich habe die verantwortungsvolle Aufgabe, nach den größten Talenten zu suchen, um das bestmögliche Team aufzubauen. Ich genieße es, die unterschiedlichsten Menschen zu treffen und Teil eines multikulturellen Teams zu sein. “ ";
$lang['CAREER_EMPLOYE_NAME2'] = "JASON, HR";
// EMPLOYE 3
$lang['CAREER_EMPLOYE_TEXT3'] = "“Ich habe bisher jeden Tag bei Medlanes genossen, da jeder Tag unterschiedlich ist und eine neue Herausforderung bedeutet. Das fleißige und dynamische Umfeld motiviert mich, mein Bestes zu geben. Wir wissen aber auch, wie man Spaß und Arbeit verbindet.“ ";
$lang['CAREER_EMPLOYE_NAME3'] = "GILDAS, BUSINESS DEVELOPMENT";



// MEDLANES IMAGES PART HEADING

$lang['CAREER_PICTURES_HEADING'] = "Arbeiten im Herzen von Berlins Startup-Kultur.";
?>
