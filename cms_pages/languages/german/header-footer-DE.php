<?php 

// PAGE TITLES

$lang['TITLE_INDEX'] = 'Medlanes | Qualifizierte medizinische Informationen - Expertenmeinungen - Zweitmeinungen';
$lang['TITLE_PAYMENT'] = 'Medlanes | Qualifizierte medizinische Informationen - Expertenmeinungen - Zweitmeinungen';
$lang['TITLE_SERVICE'] = 'Medlanes | Qualifizierte medizinische Informationen - Expertenmeinungen - Zweitmeinungen';
$lang['TITLE_TERMS'] = 'Medlanes | Qualifizierte medizinische Informationen - Expertenmeinungen - Zweitmeinungen';
$lang['TITLE_PRIVACY'] = 'Medlanes | Qualifizierte medizinische Informationen - Expertenmeinungen - Zweitmeinungen';
$lang['TITLE_ABOUT'] = 'Medlanes | Qualifizierte medizinische Informationen - Expertenmeinungen - Zweitmeinungen';
$lang['TITLE_CONTACT'] = 'Medlanes | Qualifizierte medizinische Informationen - Expertenmeinungen - Zweitmeinungen';
$lang['TITLE_CAREER'] = 'Medlanes | Qualifizierte medizinische Informationen - Expertenmeinungen - Zweitmeinungen';
$lang['TITLE_ADD_QUESTION'] = 'Medlanes | Qualifizierte medizinische Informationen - Expertenmeinungen - Zweitmeinungen';
$lang['TITLE_THANKYOU'] = 'Medlanes | Qualifizierte medizinische Informationen - Expertenmeinungen - Zweitmeinungen';
$lang['TITLE_DOCTOR'] = 'Medlanes | Qualifizierte medizinische Informationen - Expertenmeinungen - Zweitmeinungen';
$lang['TITLE_FAQ'] = 'Medlanes | Qualifizierte medizinische Informationen - Expertenmeinungen - Zweitmeinungen';

// 3 step process

$lang['3step_1'] = 'Symptome beschreiben';
$lang['3step_2'] = 'Honorar festlegen';
$lang['3step_3'] = 'Expertenmeinung erhalten';

// HEADER MENU LINKS

$lang['link1'] = '<a href="service?lang=de">Unser Service</a>';
$lang['link2'] = '<a href="doctors?lang=de">Ärzte</a>';
$lang['link3'] = '<a href="faq?lang=de">FAQ</a>';
$lang['link4'] = '<a href="contact?lang=de">Kontakt</a>';
$lang['LOGOLINK'] = 'index.php?lang=de';
// HEADER MENU LINKS END HERE


/*FOOTER MENU LINKS START*/

// 1st menu
$lang['FOOTER_link1'] = '<a href="index.php?lang=de">Home</a>';
$lang['FOOTER_link2'] = '<a href="service?lang=de">Unser Service</a>';
$lang['FOOTER_link3'] = '<a href="doctors?lang=de">Ärzte</a>';
$lang['FOOTER_link4'] = '<a href="faq?lang=de">FAQ</a>';
$lang['FOOTER_link5'] = '<a href="contact?lang=de">Kontakt</a>';
// 2nd menu
$lang['FOOTER_link6'] = '<a href="about?lang=de">Über Medlanes</a>';
$lang['FOOTER_link7'] = '<a href="career?lang=de">Karriere</a>';
// 3rd menu
$lang['FOOTER_link8'] = '<a href="terms?lang=de">AGBs</a>';
$lang['FOOTER_link9'] = '<a href="privacy?lang=de">Datenschutz</a>';
$lang['FOOTER_link10'] = '<a href="imprint?lang=de">Impressum</a>';

$lang['FOOTER_SUPPORTEDBY'] = '<p>Unterstützt von:</p>';

$lang['FOOTER_COPYRIGHTS'] = '<p class="copyrights">Copyright © Medlanes GmbH 2014 Alle Rechte vorbehalten</p>';

/*FOOTER MENU LINKS END*/
?>