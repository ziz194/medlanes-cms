<?php 

$lang['ADDQUESTION_'] = "";


$lang['EMAIL_HEAD'] = "An welche eMail-Adresse soll die Antwort gesendet werden?";
$lang['emailsign'] = "eMail-Adresse ist nicht valide";
$lang['emailsign2'] = "eMail-Adresse bereits genutzt";

$lang['ADDQUESTION_1'] = "Stellen Sie hier Ihre Frage. Ein Arzt wartet bereits, um Ihre Frage zu beantworten.";
$lang['ADDQUESTION_2'] = "Wählen Sie den betroffenen Körperteil.";
$lang['ADDQUESTION_3'] = "Lokalisieren Sie das Problem. Sie können mehrere Punkte auswählen.";
$lang['ADDQUESTION_4'] = "Vorne";
$lang['ADDQUESTION_5'] = "Hinten";
$lang['ADDQUESTION_6'] = "Fügen Sie Ihre Symptome hinzu";
$lang['ADDQUESTION_7'] = "Bitte benennen Sie Ihre Symptome (z.B. Kopfschmerzen, Bauchschmerzen, Hautausschlag, etc.)";
$lang['ADDQUESTION_8'] = "Fügen Sie relevante Bilder oder Dokumente hinzu";
$lang['ADDQUESTION_9'] = "z.B. Aufnahmen, Testresultate, etc.";
$lang['ADDQUESTION_10'] = "Wir benötigen einige zusätzliche Informationen";
$lang['ADDQUESTION_11'] = "Seit wann treten die Symptome auf?";
$lang['ADDQUESTION_12'] = "Haben Sie ein Trauma oder eine Verletzung erlitten?";
$lang['ADDQUESTION_13'] = "Bitte nennen Sie alle relevanten Medikamente, die Sie momentan einnehmen";
$lang['ADDQUESTION_14'] = "Haben Sie spezielle Fragen?";
$lang['ADDQUESTION_15'] = "Weiter zum letzten Schritt.";

$lang['ADDQUESTION_yes'] = "Ja";
$lang['ADDQUESTION_no'] = "Nein";
$lang['ADDQUESTION_add'] = "Hinzufügen";
$lang['ADDQUESTION_upload'] = "Datei hochladen";




// ADD QUESTION

$lang['aq-1'] = 'Fragen Sie unsere Ärzte und beschreiben Sie Ihre Symptome';
$lang['aq-2'] = 'Bestimmen Sie das Problem';
$lang['aq-select'] = 'Wählen Sie anhand des Modells die betroffenen Körperteile aus';
$lang['aq-describe'] = 'Beschreiben Sie Ihre Symptome';
$lang['aq-list'] = 'Bitte geben Sie ihre Symptome an (z.B. Bauchschmerzen, Übelkeit, Kopfschmerzen, Hautausschlag, etc.)';
$lang['aq-photo1'] = 'Laden Sie relevante Fotos hoch';
$lang['aq-photo2'] = 'Foto anhängen';
$lang['aq-answer'] = 'Haben Sie spezielle Fragen an unsere Ärzte?';
$lang['aq-ph'] = 'Stellen Sie Ihre Frage(n) hier';              
$lang['aq-confirm'] = 'Bestätigung';
$lang['aq-process'] = 'Wir werden einen gegigneten Arzt für Sie finden und bearbeiten Ihre Anfrage so schnell wie möglich.';
$lang['aq-3'] = 'Vorderseite';
$lang['aq-4'] = 'Rückseite';
$lang['aq-5'] = 'Ihre Symptome';
$lang['aq-6'] = 'Mehr Informationen';
$lang['aq-7'] = 'Zusätzliche Informationen';
$lang['aq-8'] = 'Seit wann treten die Symptome auf?';
$lang['aq-9'] = 'Wählen';
$lang['aq-10'] = 'Akut';
$lang['aq-11'] = 'Heute';
$lang['aq-12'] = 'Diese Woche';
$lang['aq-13'] = 'Diesen Monat';
$lang['aq-chronic'] = 'Chronisch';
$lang['aq-yq'] = 'Ihre Frage';
$lang['aq-experience'] = 'Haben Sie ein Trauma oder eine Verletzung erlitten?';
$lang['aq-14'] = 'Ja';
$lang['aq-15'] = 'Nein';
$lang['aq-16'] = 'Welche Medikamente nehmen Sie zurzeit ein?';
$lang['aq-17'] = 'Zusätzliche Informationen zu Ihrer Krankengeschichte (z.B. Allergien, hoher Blutdruck, COPD, Asthma, etc.)';
$lang['aq-18'] = 'Zusätzliche Anhänge';
$lang['aq-19'] = 'Bild(er) hochladen';
$lang['aq-20'] = 'Der passende Arzt wird Ihnen innerhalb von wenigen Stunden zugewiesen.';
$lang['aq-21'] = 'Absenden';
$lang['aq-add'] = 'Hinzufügen';
$lang['aq-name'] = 'Ihr Name';
$lang['aq-email'] = 'Ihre eMail-Adresse';
$lang['aq-button'] = 'Ich möchte jetzt medizinischen Rat erhalten!';
$lang['aq-coupon'] = 'Geben Sie hier Ihren <b>Coupon</b> Code ein:';




// Add question end


// Body parts 

$lang['aq-bp-1'] = 'Kopf';
$lang['aq-bp-2'] = 'Arm (Rechts)';
$lang['aq-bp-3'] = 'Brust (Rechts)';
$lang['aq-bp-4'] = 'Brust (Links)';
$lang['aq-bp-5'] = 'Arm (Links)';
$lang['aq-bp-6'] = 'Bauch (Rechts)';
$lang['aq-bp-7'] = 'Bauch (Links)';
$lang['aq-bp-8'] = 'Hand (Rechts)';
$lang['aq-bp-9'] = 'Genitalbereich';
$lang['aq-bp-10'] = 'Hand (Links)';
$lang['aq-bp-11'] = 'Oberschenkel (Rechts)';
$lang['aq-bp-12'] = 'Oberschenkel (Links)';
$lang['aq-bp-13'] = 'Unterschenkel (Rechts)';
$lang['aq-bp-14'] = 'Unterschenkel (Links)';
$lang['aq-bp-15'] = 'Fuß (Rechts)';
$lang['aq-bp-16'] = 'Fuß (Links)';

$lang['aq-bp-33'] = 'Oberer Rücken (Links)';
$lang['aq-bp-44'] = 'Oberer Rücken (Rechts)';
$lang['aq-bp-66'] = 'Unterer Rücken (Rechts)';
$lang['aq-bp-77'] = 'Unterer Rücken (Links)';
$lang['aq-bp-99'] = 'Steiß';


$lang['ADDQUESTION_LPANEL'] = 'Perfekt! Sie haben bereits einen Account. Bitte loggen Sie sich ein.';
$lang['ADDQUESTION_LPANEL1'] = 'Das Passwort haben Sie per eMail mit Ihre ersten Frage erhalten.';
$lang['ADDQUESTION_LPANEL2'] = 'Passwort';
$lang['ADDQUESTION_LPANEL4'] = 'Add';

// Body parts end
?>
