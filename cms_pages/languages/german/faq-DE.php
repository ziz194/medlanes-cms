<?php 

$lang['FAQ_'] = "";

// FAQ HEADING

$lang['FAQ_HEADING'] = "Häufig gestellte Fragen";

// FAQ SUBS
$lang['FAQ_SUB1'] = "Häufig gestellte Fragen";
$lang['FAQ_SUB2'] = "Guter Kundenservice steht bei Medlanes im Vordergrund. Hier finden Sie Antworten auf die häufigsten Fragen. Für alles Weitere kontaktieren Sie uns einfach direkt.";

// FAQ TABS

$lang['FAQ_TABS_HEADING1'] = "Medizin";
$lang['FAQ_TABS_TEXT1_1'] = "<p><b>1) Welche Fragen kann ich stellen?</b></p>
						<p>Sie können jede Frage an unser Ärzteteam stellen. Handelt es sich jedoch um einen mediziinischen Notfall, sollte umgehend ein Notarzt kontaktiert werden.</p>";
$lang['FAQ_TABS_TEXT1_2'] = "<p><b>2) Ist mein bisheriger Arzt online erreichbar?</b></p>
						<p>Unser Ärztenetzwerk wächst ständig. Fragen Sie Ihren Arzt gerne, ob er Medmedo bereits kennt oder künftig nutzen möchte, um Ihnen einen besseren Zugang zur qualifizierten Meinung zu ermöglichen. Wir schicken gerne Informationen an interessierte Ärzte.</p>";
$lang['FAQ_TABS_TEXT1_3'] = "<p><b>3) Für welche Fragen ist Medmedo konzipiert?</b></p>
						<p>Hier finden Sie einige Beispiele für typische Anwendungsfälle:</p>
						<p>- Ich fühle mich nicht gut und habe keine Zeit, einen Arzt aufzusuchen.</p>
						<p>- Mein Hausarzt hat geschlossen oder ist im Urlaub und ich habe eine Frage.</p>
						<p>- Ich bin gerade umgezogen und suche einen Arzt.</p>
						<p>- Ich hätte gerne eine Zweitmeinung.</p>
						<p>- Ich suche eine Antwort auf eine einfache Frage, möchte aber nicht lange auf einen Termin warten.</p>
						<p>- Ich bin im Urlaub und würde gerne einen Arzt kontaktieren, der meine Sprache spricht.</p>
						<p>- Es ist Wochenende und mein Kind hat Fieber. Soll ich ins Krankenhaus fahren?</p>";

$lang['FAQ_TABS_HEADING2'] = "Sicherheit";
$lang['FAQ_TABS_TEXT2_1'] = "<p><b>1) Sind meine Daten sicher?</b></p>
			        	<p>Ja, Medlanes verwendet ausschließlich Software, die den höchsten Sicherheitsstandards entspricht. Alle Ihre Daten werden auf deutschen Servern gespeichert und können von Ihnen vollständig gelöscht werden.</p>";
$lang['FAQ_TABS_TEXT2_2'] = "<p><b>2) Wofür werden meine Informationen genutzt?</b></p>
			        	<p>Ihre Angaben werden ausschließlich zweckgebunden für die Beantwortung Ihrer Frage genutzt. Medlanes verwendet Ihre Daten für keinerlei andere Zwecke.</p>";

$lang['FAQ_TABS_HEADING3'] = "Bezahlung";
$lang['FAQ_TABS_TEXT3_1'] = "<p><b>1) Was kostet die Nutzung von Medmedo? Übernimmt meine Versicherung die Kosten?</b></p>
			        	<p>Sie können den Betrag zur Beantwortung Ihrer Frage je nach Dringlichkeit und Detailgrad selbst wählen. Die anfallenden Kosten werden dabei nicht von den Krankenkassen übernommen.</p>";
$lang['FAQ_TABS_TEXT3_2'] = "<p><b>2) Was kann ich tun, wenn ich mit Medmedo unzufrieden bin?</b></p>
			        	<p>Ihre Meinung ist uns sehr wichtig! Wenn Sie nicht vollständig zufrieden sein sollten, bitten wir Sie, unseren Kundenservice (service@medlanes.com) zu kontaktieren. Wir helfen Ihnen gerne weiter und finden eine Lösung.</p>";

$lang['FAQ_TABS_HEADING4'] = "Allgemein";
$lang['FAQ_TABS_TEXT4_1'] = "<p><b>1) Was macht Medmedo besonders?</b></p>
			        	<p>Medmedo deckt einen großen Teil der Medizin online ab. Sie erhalten Zugang zu einem Netzwerk von erfahrenen Ärzten aus den verschiedensten Fachbereichen und somit schnelle, qualifizierte Beratung.</p>";
$lang['FAQ_TABS_TEXT4_2'] = "<p><b>2) Kann ich Medmedo auch im Ausland nutzen?</b></p>
			        	<p>Natürlich, Medmedo ist international verfügbar! Nutzen Sie den Service ganz einfach wie gewohnt, über WLAN oder das mobile Netz (beachten Sie die Roaminggebühren Ihres Netzanbieters).</p>";



?>