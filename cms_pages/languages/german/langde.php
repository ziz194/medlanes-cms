<?php 
$lang['HEADER_ALLERGIE'] = "Allergien";
$lang['HEADER_SLEEP_DISORDER'] = "Schlafstörungen";
$lang['HEADER_COLD'] = "Erkältung, Husten, Grippe";
$lang['HEADER_DIABETES'] = "Diabetes";
$lang['HEADER_DIGESTIVE'] = "Verdauungsstörungen";


$lang['HEADER_EYE'] = "Augenerkrankungen";
$lang['HEADER_EAR'] = "Ohrenschmerzen";
$lang['HEADER_MENTAL'] = "Psychische Erkrankungen";
$lang['HEADER_SEXUAL'] = "Sexualität";
$lang['HEADER_DIET'] = "Ernährung";
$lang['HEADER_INFECTION'] = "Infektionskrankheiten";
$lang['HEADER_PAIN'] = "Schmerzen";
$lang['HEADER_SKIN'] = "Haare, Haut und Nägel" ;

$lang['HEADER_ARITHIS'] = "Arthritis" ;
$lang['HEADER_COPD'] = "Asthma und COPD" ;
$lang['HEADER_CHILDREN'] = "Kinderkrankheiten" ;
$lang['HEADER_ENDOCRINE'] = "Hormonelle Störungen" ;
$lang['HEADER_INJURIES'] = "Verletzungen" ;
$lang['HEADER_MEN'] = "Männerkrankheiten" ;
$lang['HEADER_ORAL'] = "Zahnerkrankungen" ;
$lang['HEADER_RESPIRATORY'] = "Atemwegserkrankungen" ;
$lang['HEADER_STD'] = "Geschlechtskrankheiten" ;
$lang['HEADER_WOMEN'] = "Frauenkrankheiten" ;

 ?>