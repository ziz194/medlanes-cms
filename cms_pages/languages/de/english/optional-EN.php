<?php 


$lang['OPT_HEADING'] = 'Zusätzliche Informationen';
$lang['OPT_SUBHEAD'] = 'Ihr Arzt möchte Ihre Frage bestmöglich beantworten. Durch zusätzliche Informationen kann Ihre Frage ausführlicher beantwortet werden.';
$lang['OPT_SKIP'] = 'Schritt überspringen';
$lang['OPT_SKIP2'] = 'Überspringen';
$lang['OPT_NAME'] = 'Name:';
$lang['OPT_GENDER'] = 'Geschlecht:';
$lang['OPT_SELECT'] = 'Auswählen';
$lang['OPT_MALE'] = 'Männlich';
$lang['OPT_FEMALE'] = 'Weiblich';
$lang['OPT_AGE'] = 'Alter:';
$lang['OPT_PHOTOS'] = 'Please attach any relevant files or pictures';
$lang['OPT_FILES'] = 'Attach Files';
$lang['OPT_MED_HISTORY'] = 'Relevante medizinische Krankheitsgeschichte (z.B. Allergien, Bluthochdruck, COPD, Asthma, etc.):';
$lang['OPT_MEDICATION'] = 'Bitte nennen Sie regelmäßig eingenommene Medikamente:';
$lang['OPT_SYMPTOMS'] = 'Seit wann treten die Symptome auf?';
$lang['OPT_SYMPTOMS_DD'] = '<span id="sincewhen">Auswählen</span>
					<ul class="dropdown">
						<li><a href="#">Akut</a></li>
						<li><a href="#">Heute</a></li>
						<li><a href="#">Diese Woche</a></li>
						<li><a href="#">Dieser Monat</a></li>
						<li><a href="#">Chronisch</a></li>
					</ul>';
$lang['OPT_CONTINUE'] = 'Weiter';





$lang['PAYMENT_NEW1'] = "Wählen Sie jetzt Ihre Optionen";
$lang['PAYMENT_NEW2'] = "Einer unserer Experten ist bereit, Ihre Fragen zu beantworten. Wir garantieren <b>100%ige Zufriedenheit.";
$lang['PAYMENT_NEW3'] = 'Expertenhonorar für <b id="detailsswap">hoch detailierte</b> und <b id="urgencyswap">schnelle</b>Informationen:';
$lang['PAYMENT_NEW4'] = "Ein Facharzt wartet auf Ihre Frage";
$lang['PAYMENT_NEW5'] = "Dr. med. J. Männel";
$lang['PAYMENT_NEW6'] = "Allgemeinärztin mit 12 Jahren Praxiserfahrung";
$lang['PAYMENT_NEW7'] = "GEPRÜFTE EXPERTIN";
$lang['PAYMENT_NEW8'] = "Bewertung";
$lang['PAYMENT_NEW9'] = "<b>9,7 von 10</b> (Sanego)";
$lang['PAYMENT_NEW10'] = "<b>Geld-Zurück-Garantie</b><br>Wir garantieren 100% Kundenzufriedenheit. Sollten Sie mit der Antwort nicht zufrieden sein, erhalten Sie Ihr Geld zurück!";


?>