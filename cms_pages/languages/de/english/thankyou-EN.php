<?php 

$lang['THANKYOU_1'] = "Herzlichen Dank für Ihre Frage. Was geschieht als nächstes?";
$lang['THANKYOU_2'] = "Sie erhalten Ihre Antwort direkt per E-Mail. Sie müssen nichts weiter tun.";
$lang['THANKYOU_3'] = "Falls kein passender Online Arzt für Sie gefunden wurde, erstatten wir Ihnen den Betrag umgehend zurück.";
$lang['THANKYOU_4'] = "<b>Haben Sie noch Fragen?</b>";
$lang['THANKYOU_5'] = "Unser Kundensupport ist jederzeit für erreichbar. Kontaktieren Sie uns!";
$lang['THANKYOU_6'] = 'E-Mail: <b style="padding-left:10px">service@medlanes.com</b>';
$lang['THANKYOU_7'] = 'Tel.:<b style="padding-left:35px">0800 / 765 43 43</b>';
$lang['THANKYOU_8'] = "";
$lang['THANKYOU_9'] = "";

?>