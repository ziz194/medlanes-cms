
<?php 

$lang['BXSLIDER'] = "<ul class='bxslider'>";



// HEADER TEXT

$lang['HEADER_1'] = "Unser Kundensupport ist für Sie da!";
$lang['HEADER_2'] = "<b>0800 / 765 43 43</b> oder <b>service@medlanes.com</b>";





// PAGE TITLES

$lang['TITLE_IMPRINT'] = "Medlanes | Qualifizierte medizinische Informationen - Expertenmeinungen - Zweitmeinungen | Impressum";
$lang['TITLE_INDEX'] = "Medlanes | Qualifizierte medizinische Informationen - Expertenmeinungen - Zweitmeinungen | Home";
$lang['TITLE_PAYMENT'] = "Medlanes | Qualifizierte medizinische Informationen - Expertenmeinungen - Zweitmeinungen | Bezahlung";
$lang['TITLE_PRIVACY'] = "Medlanes | Qualifizierte medizinische Informationen - Expertenmeinungen - Zweitmeinungen | Datenschutz";
$lang['TITLE_TERMS'] = "Medlanes | Qualifizierte medizinische Informationen - Expertenmeinungen - Zweitmeinungen | Allgemeine Geschäftsbedingungen";
$lang['TITLE_CONTACT'] = "Medlanes | Qualifizierte medizinische Informationen - Expertenmeinungen - Zweitmeinungen | Kontakt";
$lang['TITLE_THANKYOU'] = "Medlanes | Qualifizierte medizinische Informationen - Expertenmeinungen - Zweitmeinungen | Danke";

/*FOOTER LINKS*/

$lang['FOOTER1'] = '<a href="index.php?lang=de">Medlanes Mainpage</a>';
$lang['FOOTER2'] = '<a href="imprint.php?lang=de">Über Medlanes / Impressum</a>';
$lang['FOOTER3'] = '<a href="contact.php?lang=de">Support</a>';
$lang['FOOTER4'] = '<a href="terms.php?lang=de"> Allgemeine Geschäftsbedingungen</a>';
$lang['FOOTER5'] = '<a href="privacy.php?lang=de"> Datenschutz</a>';

$lang['FOOTER8'] = '<a href="#"> Presse</a>';
$lang['FOOTER9'] = '<a href="#"> Karriere</a>';
$lang['FOOTER10'] = '<p>Unterstützer <br> und Partner:</p>';
$lang['FOOTER11'] = '&copy; Medlanes GmbH - Alle Rechte vorbehalten.';


$lang['INDEX_1'] = "Schnelle Antworten von führenden Ärzten";
$lang['INDEX_2'] = "Unsere Experten beantworten Ihre Fragen. Wir garantieren <b>100% Kundenzufriedenheit.</b>";
$lang['INDEX_3'] = "Ihre Frage an unsere Fachärzte";
$lang['INDEX_4'] = "Ihre E-Mail-Adresse*";
$lang['INDEX_5'] = "Ihre Antwort erhalten";
$lang['INDEX_6'] = "*Ihre Daten sind verschlüsselt und werden nicht weitergegeben.";
$lang['INDEX_7'] = "Konzept bekannt aus:";



// IN THE PRESS
$lang['PRESS_0'] = "In den Nachrichten";
$lang['PRESS_1'] = "[...] 83 Prozent der befragten Regierungen unterstützt die Nutzung von Mobiltelefonen für Gesundheitsdienste.";
$lang['PRESS_2'] = "World Health Organization";
$lang['PRESS_3'] = "[...] es geht darum, dass wir alle technischen Möglichkeiten nutzen, damit der medizinische Fortschritt allen Patienten wirklich zugutekommt.";
$lang['PRESS_4'] = "Bundesminister für Gesundheit Hermann Gröhe in der FAZ";
$lang['PRESS_5'] = "Die Telemedizin [...] wird uns helfen können, um ärztliche Versorgung weitestgehend sicherzustellen.";
$lang['PRESS_6'] = "Ministerin für Soziales, Arbeit, Gesundheit und Demografie Sabine Bätzing-Lichtenthäler in Die Welt";
$lang['PRESS_7'] = "Mit modernsten Informations- und Kommunikationstechnologien können wir in Zukunft neue Elemente einer hochwertigen medizinischen Versorgung für die Patienten unterstützen.";
$lang['PRESS_8'] = "Co-Vorsitzender der eHealth Initiative ";
$lang['PRESS_9'] = "Einen Arzt online fragen: Die eigenständige Recherche eines medizinischen Problems ist nicht ganz ungefährlich. Eine Experten zu befragen, ist oft sinnvoll.";
$lang['PRESS_10'] = "Süddeutsche Zeitung";
$lang['PRESS_11'] = "Jeder Online Doktor wird bis auf Herz und Nieren geprüft.";
$lang['PRESS_12'] = "Wirtschaftswoche";

// TESTIMONIALS
$lang['TESTIMONIALS_0'] = "Unsere Kunden über uns";
$lang['TESTIMONIALS_1'] = "Ich bin sehr zufrieden mit den Informationen, die ich vom Doktor online erhalten habe! Meine Frage wurde schnell und verständlich beantwortet.";
$lang['TESTIMONIALS_2'] = "J. Schmidt, 34  <br> Salzgitter";
$lang['TESTIMONIALS_3'] = "Zu Beginn war ich sehr skeptisch, online einen Arzt zu fragen. Dann war ich angenehm überrascht mit der Antwort, die ich von meinem Online Doktor erhalten habe.";
$lang['TESTIMONIALS_4'] = "T. Wille, 31 <br> Meerbusch";
$lang['TESTIMONIALS_5'] = "Ein Freund hat mir empfohlen, bzgl. Rauchentwöhnung einen Arzt online zu fragen. Dank Medlanes bin ich seit Monaten Nichtraucher!";
$lang['TESTIMONIALS_6'] = "R. Teubner, 58 <br> Aalen";
$lang['TESTIMONIALS_7'] = "Nachdem ich meine Laborergebnisse bekommen habe, hatte ich keine Ahnung, was diese bedeuten. Nach Rücksprache mit einem Arzt online konnten alle Fragen geklärt werden.";
$lang['TESTIMONIALS_8'] = "C. Martin, 27 <br> Gera";
$lang['TESTIMONIALS_9'] = "Meine Tochter hat sich mitten in der Nacht seltsam verhalten, daher wollte ich online Ärzte nach Rat fragen. Medlanes konnte uns sofort weiterhelfen.";
$lang['TESTIMONIALS_10'] = "P. Weiß, 29 <br> Peine";
$lang['TESTIMONIALS_11'] ="Nach dem Termin mit meinem Arzt wollte ich eine zweite Meinung einholen. Die Möglichkeit, einfach online einen Doktor zu fragen, macht vieles einfacher.";
$lang['TESTIMONIALS_12'] = "B. Wilseder, 47 <br> Berlin";
$lang['TESTIMONIALS_13'] = 'Dr. Online beantwortet auch peinliche und diskrete Fragen.';
$lang['TESTIMONIALS_14'] = 'W. Thieme, 54  <br> Marl';
$lang['TESTIMONIALS_15'] = 'Mein Internet Doktor hat mir meine ersten Sorgen genommen und mich an an den zuständigen Facharzt verwiesen!';
$lang['TESTIMONIALS_16'] = 'G. Hardenberg, 36 <br> Bremerhaven';

// DOCTORS
$lang['DOCTORS_0'] = "Unsere Ärzte";
$lang['DOCTORS_1'] = "Dr. med. Jessica Männel";
$lang['DOCTORS_2'] = "Fachärztin für Allgemeinmedizin <br> 12 Jahre Praxiserfahrung";
$lang['DOCTORS_3'] = "Bewertung 4.9 / 5";
$lang['DOCTORS_4'] = "Dr. med. Wolf Siepen";
$lang['DOCTORS_5'] = "Facharzt für orth. Chirugie und Traumatologie <br> 12 Jahre Praxiserfahrung";
$lang['DOCTORS_6'] = "Bewertung 4.7 / 5";
$lang['DOCTORS_7'] = "Dr. med. Kathrin Hamann";
$lang['DOCTORS_8'] = "Fachärztin für Allgemeinmedizin <br> 14 Jahre Praxiserfahrung";
$lang['DOCTORS_9'] = "Bewertung 4.9 / 5";
$lang['DOCTORS_10'] = "Dr. med. Norbert Scheufele";
$lang['DOCTORS_11'] = "Facharzt für Frauenheilkunde und Geburtshilfe <br> 20 Jahre Praxiserfahrung";
$lang['DOCTORS_12'] = "Bewertung 4.4 / 5";
$lang['DOCTORS_13'] = "Christian Welsch";
$lang['DOCTORS_14'] = "Facharzt für HNO-Heilkunde <br> 16 Jahre Praxiserfahrung";
$lang['DOCTORS_15'] = "Bewertung 4.8 / 5";
$lang['DOCTORS_16'] ="Stefan Paffrath";
$lang['DOCTORS_17'] = "Dipl. Psychologe <br> 27 Jahre Praxiserfahrung";
$lang['DOCTORS_18'] = "Bewertung 4.8 / 5";




?>