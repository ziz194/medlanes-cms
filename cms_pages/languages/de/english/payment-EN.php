<?php 

$lang['PAYMENT_'] = "Bezahlung";

$lang['PAYMENT_HEADING'] = "Wählen Sie die Dringlichkeit und den Detailgrad aus";
$lang['PAYMENT_SUBHEADING'] = "Bitte legen Sie die Dringlichkeit und den Detailgrad fest.";


// CHOOSE URGENCY PANEL
$lang['PAYMENT_CHOOSEPANEL'] = "Dringlichkeit";

$lang['PAYMENT_CHOOSEPANEL1'] = "Detailgrad";
$lang['PAYMENT_CHOOSEPANEL2'] = "Niedrig";
$lang['PAYMENT_CHOOSEPANEL3'] = "Mittel";
$lang['PAYMENT_CHOOSEPANEL4'] = "Hoch";
$lang['PAYMENT_CHOOSEPANEL5'] = "Betrag, den Sie nur zahlen, WENN Sie mit der Antwort zufrieden waren:
";


// DEPOSIT FEE

$lang['PAYMENT_DEPOSIT1'] = "Bezahlung (100% Zufriedenheitsgarantie)";
$lang['PAYMENT_DEPOSIT2'] = "Leisten Sie eine Zahlung von";
$lang['PAYMENT_DEPOSIT22'] = " und Sie erhalten Ihre Antwort sofort.";
$lang['PAYMENT_DEPOSIT3'] = "Bezahlung via Paypal";
$lang['PAYMENT_DEPOSIT4'] = "Ihre Informationen sind geschützt. ";
$lang['PAYMENT_DEPOSIT5'] = "Mit “Fortsetzen” bestätigen Sie die AGBs und die Altersbestimmungen (18+).";
$lang['PAYMENT_DEPOSIT6'] = "Weiter zum letzten Schritt.";


// SIDEBAR 

$lang['PAYMENT_SIDEHEAD1'] = "Ärztenetzwerk";
$lang['PAYMENT_SIDEHEAD2'] = "Ein hoch qualifizierter Arzt aus unserem Netzwerk antwortet in Kürze auf Ihre Frage.";


$lang['PAYMENT_SIDE_DOC1HEAD1'] = "Dr. med. Kathrin Hamann";
$lang['PAYMENT_SIDE_DOC1HEAD2'] = "Fachärztin für Allgemeinmedizin";
$lang['PAYMENT_SIDE_DOC1TEXT3'] = "<p>- 12 Jahre Praxiserfahrung</p>";

$lang['PAYMENT_SIDE_DOC2HEAD1'] = "Dr. med. Wolf Siepen";
$lang['PAYMENT_SIDE_DOC2HEAD2'] = "Facharzt für orth. Chirugie und Traumatologie";
$lang['PAYMENT_SIDE_DOC2TEXT3'] = "<p>- 12 Jahre Praxiserfahrung</p>";

$lang['PAYMENT_SIDE_DOC3HEAD1'] = "Dr. med. Norbert Scheufele";
$lang['PAYMENT_SIDE_DOC3HEAD2'] = "Facharzt für Frauenheilkunde und Geburtshilfe";
$lang['PAYMENT_SIDE_DOC3TEXT3'] = "<p>- 20 Jahre Praxiserfahrung</p>";
$lang['pricess'] = '€';


$lang['PAYMENT_AGREEMENT1'] = 'Ich habe die ';
$lang['PAYMENT_AGREEMENT2'] = '<a href="terms.php?lang=de" target="_blank" >AGBs</a> und die';
$lang['PAYMENT_AGREEMENT3'] = '<a href="privacy.php?lang=de" target="_blank" >Datenschutzbestimmungen</a>';
$lang['PAYMENT_AGREEMENT4'] = 'gelesen und akzeptiere sie. Ich bin über 18 Jahre alt.';




$lang['PAYMENT_NEW1'] = "Wählen Sie jetzt Ihre Optionen";
$lang['PAYMENT_NEW2'] = "Bitte legen Sie Dringlichkeit und Detailgrad fest.";
$lang['PAYMENT_NEW3'] = 'Preis, den Sie bezahlen, wenn Sie mit der Antwort zufrieden sind: € ';
$lang['PAYMENT_NEW4'] = "Ein Facharzt wartet auf Ihre Frage";
$lang['PAYMENT_NEW5'] = "Dr. med. J. Männel";
$lang['PAYMENT_NEW6'] = "Allgemeinärztin mit 12 Jahren Praxiserfahrung";
$lang['PAYMENT_NEW7'] = "GEPRÜFTE EXPERTIN";
$lang['PAYMENT_NEW8'] = "Bewertung";
$lang['PAYMENT_NEW9'] = "<b>4.85 / 5</b>";
$lang['PAYMENT_NEW10'] = "<b>Geld-Zurück-Garantie</b> <br>Wir garantieren 100% Kundenzufriedenheit. Sollten Sie mit der Antwort nicht zufrieden sein, erhalten Sie Ihr Geld zurück!";
$lang['PAYMENT_DOC_IMG'] = '<img src="images/payment/doc.png" alt="#"/>';


$lang['3step_1'] = "Frage stellen";
$lang['3step_2'] = "Honorar wählen";
$lang['3step_3'] = "Antwort vom Experten";

$lang['DEP_SELECT'] = 'Auswählen';
$lang['DEP_METHOD'] = 'Zahlungsmethode:';
$lang['DEP_CC'] = 'Kreditkarte';
$lang['DEP_PP'] = 'PayPal';
$lang['DEP_WT'] = 'SOFORT';
$lang['DEP_PP_TEXT'] = 'Sie werden zu PayPal weitergeleitet.';
$lang['DEP_CC_NR'] = 'Kartennummer';
$lang['DEP_CC_EXP'] = 'Gültig bis:';
$lang['DEP_CC_MONTH'] = '<span id="month">Monat</span>
						<ul class="dropdown">
							<li><a href="#">01 - Jan.</a></li>
							<li><a href="#">02 - Feb.</a></li>
							<li><a href="#">03 - März</a></li>
							<li><a href="#">04 - Apr.</a></li>
							<li><a href="#">05 - Mai</a></li>
							<li><a href="#">06 - Juni</a></li>
							<li><a href="#">07 - Juli</a></li>
							<li><a href="#">08 - Aug.</a></li>
							<li><a href="#">09 - Sept.</a></li>
							<li><a href="#">10 - Okt.</a></li>
							<li><a href="#">11 - Nov.</a></li>
							<li><a href="#">12 - Dez.</a></li>							
						</ul>';
$lang['DEP_CC_YEAR'] = '<span id="year">Jahr</span>
						<ul class="dropdown">
							<li><a href="#">2015</a></li>							
							<li><a href="#">2016</a></li>							
							<li><a href="#">2017</a></li>							
							<li><a href="#">2018</a></li>							
							<li><a href="#">2019</a></li>							
							<li><a href="#">2020</a></li>							
							<li><a href="#">2021</a></li>							
							<li><a href="#">2022</a></li>							
							<li><a href="#">2023</a></li>							
							<li><a href="#">2024</a></li>							
						</ul>';
$lang['DEP_CC_CVC'] = 'Sicherheitscode';
$lang['DEP_SECURE'] = 'Ihre Informationen sind verschlüsselt.';
$lang['DEP_PRIVACY'] = 'Datenschutz';

$lang['PAY_CONTINUE'] = "Bezahlung";




?>