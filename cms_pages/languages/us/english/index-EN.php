<?php 

$lang['BXSLIDER'] = "<ul class='bxslider'>";
 
// PAGE TITLES

$lang['TITLE_IMPRINT'] = "Medlanes | Qualified medical information - Expert opinions - Second opinions | Imprint";
$lang['TITLE_INDEX'] = "Medlanes | Qualified medical information - Expert opinions - Second opinions | Home";
$lang['TITLE_PAYMENT'] = "Medlanes | Qualified medical information - Expert opinions - Second opinions | Payment";
$lang['TITLE_PRIVACY'] = "Medlanes | Qualified medical information - Expert opinions - Second opinions | Privacy Statement";
$lang['TITLE_TERMS'] = "Medlanes | Qualified medical information - Expert opinions - Second opinions | Terms of Service";
$lang['TITLE_CONTACT'] = "Medlanes | Qualified medical information - Expert opinions - Second opinions | Contact";
$lang['TITLE_THANKYOU'] = "Medlanes | Qualified medical information - Expert opinions - Second opinions | Thank you";


// HEADER TEXT

$lang['HEADER_1'] = "Validated medical information from medical experts!";
$lang['HEADER_2'] = "Our support team is ready to help <b>0800 / 765 43 43</b>";

/*FOOTER LINKS*/

$lang['FOOTER1'] = '<a href="index.php?lang=en">Medlanes Mainpage</a>';
$lang['FOOTER2'] = '<a href="imprint.php?lang=en">About Medlanes / Imprint</a>';
$lang['FOOTER3'] = '<a href="contact.php?lang=en">Support</a>';
$lang['FOOTER4'] = '<a href="terms.php?lang=en"> Terms & Conditions</a>';
$lang['FOOTER5'] = '<a href="privacy.php?lang=en"> Privacy Statement</a>';

$lang['FOOTER8'] = '<a href="#"> PR Contact</a>';
$lang['FOOTER9'] = '<a href="#"> Careers</a>';
$lang['FOOTER10'] = '<p>Supporters <br> and Partners</p>';
$lang['FOOTER11'] = '&copy; Medlanes GmbH - All Rights Reserved';

$lang['INDEX_1'] = "Quick answers by leading specialists";
$lang['INDEX_2'] = "Our experts answer your medical questions. A new question is answered every <b>9 seconds</b>. We provide a<b>100% satisfaction guarantee.</b>";
$lang['INDEX_3'] = "Your question to our experts";
$lang['INDEX_4'] = "Your email*";
$lang['INDEX_5'] = "Get your answer now";
$lang['INDEX_6'] = "*Your data is encrypted and will not be disclosed.";
$lang['INDEX_7'] = "Concept seen on:";
$lang['INDEX_8'] = "In the news";
$lang['INDEX_9'] = "Medlanes have seen a spike in traffic lately. Medlanes’ traffic increased by 36% over the last month and was visited by almost 400,000 people.";
$lang['INDEX_10'] = "Today, 10/8/2013";
$lang['INDEX_11'] = "Our customers about us";
$lang['INDEX_12'] = "“This app has improved my life vastly. I've had more questions than answers for a while now. It's unreasonable to call my doctor every 5 minutes.";
$lang['INDEX_13'] = "Xenia, 27 <br> Berlin";
$lang['INDEX_14'] = "Our doctors";
$lang['INDEX_15'] = "Dr. med. K. Hamann";
$lang['INDEX_16'] = "General Physician 12 years of practice.";
$lang['INDEX_17'] = "Rating: 4.9 / 5";
$lang['INDEX_18'] = "";
$lang['INDEX_19'] = "";


// IN THE PRESS
$lang['PRESS_0'] = "In den Nachrichten";
$lang['PRESS_1'] = "[...] 83 Prozent der befragten Regierungen unterstützt die Nutzung von Mobiltelefonen für Gesundheitsdienste.";
$lang['PRESS_2'] = "World Health Organization";
$lang['PRESS_3'] = "[...] es geht darum, dass wir alle technischen Möglichkeiten nutzen, damit der medizinische Fortschritt allen Patienten wirklich zugutekommt.";
$lang['PRESS_4'] = "Bundesminister für Gesundheit Hermann Gröhe in der FAZ";
$lang['PRESS_5'] = "Die Telemedizin [...] wird uns helfen können, um ärztliche Versorgung weitestgehend sicherzustellen.";
$lang['PRESS_6'] = "Ministerin für Soziales, Arbeit, Gesundheit und Demografie Sabine Bätzing-Lichtenthäler in Die Welt";
$lang['PRESS_7'] = "Mit modernsten Informations- und Kommunikationstechnologien können wir in Zukunft neue Elemente einer hochwertigen medizinischen Versorgung für die Patienten unterstützen.";
$lang['PRESS_8'] = "Co-Vorsitzender der eHealth Initiative ";
$lang['PRESS_9'] = "Einen Arzt online fragen: Die eigenständige Recherche eines medizinischen Problems ist nicht ganz ungefährlich. Eine Experten zu befragen, ist oft sinnvoll.";
$lang['PRESS_10'] = "Süddeutsch Zeitung";
$lang['PRESS_11'] = "Jeder Online Doktor wird bis auf Herz und Nieren geprüft.";
$lang['PRESS_12'] = "Wirtschaftswoche";

// TESTIMONIALS
$lang['TESTIMONIALS_0'] = "Unsere Kunden über uns";
$lang['TESTIMONIALS_1'] = "Ich bin sehr zufrieden mit den Informationen, die ich vom Doktor online erhalten habe! Meine Frage wurde schnell und verständlich beantwortet.";
$lang['TESTIMONIALS_2'] = "J. Schmidt, 46  <br> Salzgitter";
$lang['TESTIMONIALS_3'] = "Zu Beginn war ich sehr skeptisch, online einen Arzt zu fragen. Dann war ich angenehm überrascht mit der Antwort, die ich von meinem Online Doktor erhalten habe.";
$lang['TESTIMONIALS_4'] = "T. Wille, 59 <br> Meerbusch";
$lang['TESTIMONIALS_5'] = "Ein Freund hat mir empfohlen, bzgl. Rauchentwöhnung einen Arzt online zu fragen. Dank Medlanes bin ich seit Monaten Nichtraucher!";
$lang['TESTIMONIALS_6'] = "R. Teubner, 37 <br> Aalen";
$lang['TESTIMONIALS_7'] = "Nachdem ich meine Laborergebnisse bekommen habe, hatte ich keine Ahnung, was diese bedeuten. Nach Rücksprache mit einem Arzt online konnten alle Fragen geklärt werden.";
$lang['TESTIMONIALS_8'] = "C. Martin, 25 <br> Gera";
$lang['TESTIMONIALS_9'] = "Meine Tochter hat sich mitten in der Nacht seltsam verhalten, daher wollte ich online Ärzte nach Rat fragen. Medlanes konnte uns sofort weiterhelfen.";
$lang['TESTIMONIALS_10'] = "P. Weiß, 32 <br> Peine";
$lang['TESTIMONIALS_11'] ="Nach dem Termin mit meinem Arzt wollte ich eine zweite Meinung einholen. Die Möglichkeit, einfach online einen Doktor zu fragen, macht vieles einfacher.";
$lang['TESTIMONIALS_12'] = "B. Wilseder, 61 <br> Berlin";
$lang['TESTIMONIALS_13'] = 'Dr. Online beantwortet auch peinliche und diskrete Fragen.';
$lang['TESTIMONIALS_14'] = 'W. Thieme, 54  <br> Marl';
$lang['TESTIMONIALS_15'] = 'Mein Internet Doktor hat mir meine ersten Sorgen genommen und mich an an den zuständigen Facharzt verwiesen!';
$lang['TESTIMONIALS_16'] = 'G. Hardenberg, 36 <br> Bremerhaven';


// DOCTORS
$lang['DOCTORS_0'] = "Unsere Ärzte";
$lang['DOCTORS_1'] = "Dr. med. J. Männel";
$lang['DOCTORS_2'] = "Fachärztin für Allgemeinmedizin <br> 12 Jahre Praxiserfahrung";
$lang['DOCTORS_3'] = "Bewertung 4.9 / 5";
$lang['DOCTORS_4'] = "Dr. med. W. Siepen";
$lang['DOCTORS_5'] = "Facharzt für orth. Chirugie und Traumatologie <br> 12 Jahre Praxiserfahrung";
$lang['DOCTORS_6'] = "Bewertung 4.7 / 5";
$lang['DOCTORS_7'] = "Dr. med. K. Hamann";
$lang['DOCTORS_8'] = "Fachärztin für Allgemeinmedizin <br> 14 Jahre Praxiserfahrung";
$lang['DOCTORS_9'] = "Bewertung 4.9 / 5";
$lang['DOCTORS_10'] = "Dr. med. Norbert Scheufele";
$lang['DOCTORS_11'] = "Facharzt für Frauenheilkunde und Geburtshilfe <br> 20 Jahre Praxiserfahrung";
$lang['DOCTORS_12'] = "Bewertung 4.4 / 5";
$lang['DOCTORS_13'] = "C. Welsch";
$lang['DOCTORS_14'] = "Facharzt für HNO-Heilkunde <br> 16 Jahre Praxiserfahrung";
$lang['DOCTORS_15'] = "Bewertung 4.8 / 5";
$lang['DOCTORS_16'] ="S. Paffrath";
$lang['DOCTORS_17'] = "Dipl. Psychologe <br> 27 Jahre Praxiserfahrung";
$lang['DOCTORS_18'] = "Bewertung 4.8 / 5";


?>