<?php 


$lang['BXSLIDER'] = "<ul class='bxslider'>";

// PAGE TITLES

$lang['TITLE_IMPRINT'] = "Medlanes | Qualified medical information - Expert opinions - Second opinions | Imprint";
$lang['TITLE_INDEX'] = "Medlanes | Qualified medical information - Expert opinions - Second opinions | Home";
$lang['TITLE_PAYMENT'] = "Medlanes | Qualified medical information - Expert opinions - Second opinions | Payment";
$lang['TITLE_PRIVACY'] = "Medlanes | Qualified medical information - Expert opinions - Second opinions | Privacy Statement";
$lang['TITLE_TERMS'] = "Medlanes | Qualified medical information - Expert opinions - Second opinions | Terms of Service";
$lang['TITLE_CONTACT'] = "Medlanes | Qualified medical information - Expert opinions - Second opinions | Contact";
$lang['TITLE_THANKYOU'] = "Medlanes | Qualified medical information - Expert opinions - Second opinions | Thank you";


// HEADER TEXT

$lang['HEADER_1'] = "Our Customer Service team is here for you! ";
$lang['HEADER_2'] = "<b>(800) 413-7290</b> or <b>service@medlanes.com</b>";

/*FOOTER LINKS*/

$lang['FOOTER1'] = '<a href="index.php?lang=en">Medlanes Mainpage</a>';
$lang['FOOTER2'] = '<a href="imprint.php?lang=en">About Medlanes / Imprint</a>';
$lang['FOOTER3'] = '<a href="contact.php?lang=en">Support</a>';
$lang['FOOTER4'] = '<a href="terms.php?lang=en"> Terms & Conditions</a>';
$lang['FOOTER5'] = '<a href="privacy.php?lang=en"> Privacy Statement</a>';

$lang['FOOTER8'] = '<a href="#"> PR Contact</a>';
$lang['FOOTER9'] = '<a href="#"> Careers</a>';
$lang['FOOTER10'] = '<p>Supporters <br> and Partners</p>';
$lang['FOOTER11'] = '&copy; Medlanes, GmbH - All Rights Reserved';

$lang['INDEX_1'] = "Medical Advice at your Fingertips.";
$lang['INDEX_2'] = "Answers to your medical questions by leading specialists. New questions <br> answered every 8 seconds! <b>We guarantee 100% customer satisfaction.</b> ";
$lang['INDEX_3'] = "Your question to our experts";
$lang['INDEX_4'] = "Your eMail*";
$lang['INDEX_5'] = "Get your answer now";
$lang['INDEX_6'] = "*Your data is encrypted and will not be disclosed.";
$lang['INDEX_7'] = "Concept seen on:";

// IN THE PRESS
$lang['PRESS_0'] = "In the News";
$lang['PRESS_1'] = "Medlanes revolutionary online doctor platform has changed the way healthcare is accessed forever.";
$lang['PRESS_2'] = "The Globe and Mail";
$lang['PRESS_3'] = "Ask a doctor online platforms bring quality healthcare online, and make it affordable to all.";
$lang['PRESS_4'] = "National Post";
$lang['PRESS_5'] = "Healthcare innovations are few and far between, but Medlanes brings your doctor online.";
$lang['PRESS_6'] = "Toronto Star";
$lang['PRESS_7'] = "The access to quality doctors online is improving. Medlanes takes the online doctor paradigm to the next level.";
$lang['PRESS_8'] = "Windsor Star";
$lang['PRESS_9'] = "Getting medical questions answered online saves consumers time and money, while increasing efficacious treatment via online doctor platforms..";
$lang['PRESS_10'] = "Calgary Herald";
$lang['PRESS_11'] = "Medlanes unique online healthcare platform brings unprecedented access to your doctor, all on a convenient online platform.";
$lang['PRESS_12'] = "CBC News";

// TESTIMONIALS
$lang['TESTIMONIALS_0'] = "What Our Customers Are Saying";
$lang['TESTIMONIALS_1'] = "I wanted to see a online doctor and was happy to find Medlanes. I had a very embarrassing problem, and their platform enabled me to talk to doctors online without the face to face meeting!";
$lang['TESTIMONIALS_2'] = "Wilson R., 34 - Regina";
$lang['TESTIMONIALS_3'] = "At first I was skeptical about online doctor services. However, once I gave it a chance I was pleasantly surprised with the answer I received from my doctor. I had no idea these ask a doctor online platforms even existed!";
$lang['TESTIMONIALS_4'] = "Theresa G., 59 - Saskatoon";
$lang['TESTIMONIALS_5'] = "A friend referred me to talk to a doctor online about quitting smoking. The last time I touched a cigarette was in October 2014! Thanks to my online doctor with Medlanes, I have finally made my health a priority!                     ";
$lang['TESTIMONIALS_6'] = "Vincent M., 58 - Banff";
$lang['TESTIMONIALS_7'] = "After I received my lab results, I had no idea what they meant. My doctor did not take the time to explain them to me, but the online doctors with Medlanes took good care of me. Thank you!";
$lang['TESTIMONIALS_8'] = "Charles H., 27 - Whistler";
$lang['TESTIMONIALS_9'] = "Having to wait for a doctors appointment is not an option with my busy life style. I having my doctor online with the Medlanes service, I can speak with him 24 hours a day, 7 days a week.";
$lang['TESTIMONIALS_10'] = "Melissa B., 29 - Yellowknife";
$lang['TESTIMONIALS_11'] = "After the appointment with my physician, I opted for a second opinion as I just needed verification. So I discussed my worries with an online doctor from Medlanes and I could not be happier! It saved me from an unnecessary operation.";
$lang['TESTIMONIALS_12'] = "Daniel T., 37 - Winnipeg";
$lang['TESTIMONIALS_13'] = 'I did not know I was able to ask a doctor questions online! I skipped a trip to the emergency room, and my daughter is feeling better than ever. Thank you to Medlanes for this wonderful online doctor platform.';
$lang['TESTIMONIALS_14'] = 'Dennis H., 61 - Halifax';
$lang['TESTIMONIALS_15'] = 'Having access to my doctor online has been so helpful. No longer do I have to wait weeks for an appointment, spend hours in the dreaded waiting room, and leave the doctors office unsatisfied. Thank you, Medlanes!';
$lang['TESTIMONIALS_16'] = 'Jennifer W., 52 - Calgary';

// DOCTORS
$lang['DOCTORS_0'] = "Meet Our Doctors";
$lang['DOCTORS_1'] = "Chadwick White, MD";
$lang['DOCTORS_2'] = "Board Certified OBGYN <br> 13 years of experience and ready to help now!";
$lang['DOCTORS_3'] = "4.8/5 Stars";
$lang['DOCTORS_4'] = "Sandy Vaughn, MD";
$lang['DOCTORS_5'] = "Board Certified Physician <br> 11 years of experience";
$lang['DOCTORS_6'] = "4.9/5 Stars";
$lang['DOCTORS_7'] = "Alvin Lee, MD, MPH";
$lang['DOCTORS_8'] = "Board Certified Pediatrician <br> 15 years of experience and a trained online doctor.";
$lang['DOCTORS_9'] = "4.7/5 Stars";
$lang['DOCTORS_10'] = "Paul Cormier, MD, Ph.D.";
$lang['DOCTORS_11'] = "Board Certified in Family Medicine <br> 22 years of experience";
$lang['DOCTORS_12'] = "4.4/5 Stars";
$lang['DOCTORS_13'] = "Allison Waters, MD";
$lang['DOCTORS_14'] = "Board Certified in Psychiatry <br> 14 years of experience";
$lang['DOCTORS_15'] = "4.8/5 Stars";
$lang['DOCTORS_16'] ="Trey Bonaventure, MD";
$lang['DOCTORS_17'] = "Board Certified in Internal Medicine <br>  12 years of experience ";
$lang['DOCTORS_18'] = "4.9/5 Stars";