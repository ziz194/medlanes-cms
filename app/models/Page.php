<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Page extends Eloquent {
 
    /**
     * The database table used by the model.CRUD Service
     *
     * @var string
     */
    protected $table = 'pages';
    protected $primaryKey = 'idPage';

     public function languages()
     {
         return $this->belongsToMany('Language', 'languages_pages','idPage','idLanguage')->withPivot('languagePageUrl','link');
     } 

     public function contents()
    {
        return $this->hasMany('Content','idPage');
    }

 
}

