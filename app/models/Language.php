<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Language extends Eloquent {
 
    /**
     * The database table used by the model.CRUD Service
     *
     * @var string
     */
    protected $table = 'languages';
    protected $primaryKey = 'idLanguage';
    
    public function pages(){
         return $this->belongsToMany('Page','languages_pages','idLanguage','idPage')->withPivot('languagePageUrl','link');
    }


    public function brands(){
         return $this->belongsToMany('Brand','brands_languages','idLanguage','idBrand')->withPivot('defaultlanguage');
    }


    public function getLanguageById($id){
         $language = Language::find($id);
         return $language ;
    }

    public function findPages() {
         return $this->pages() ; 
    }

   public function templates(){
        return $this->hasMany('Template','idLanguage');
    }
 
}