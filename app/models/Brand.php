<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Brand extends Eloquent {
 
    /**
     * The database table used by the model.CRUD Service
     *
     * @var string
     */
    protected $table = 'brands';
    protected $primaryKey = 'idBrand';

    public function languages()
     {
         return $this->belongsToMany('Language', 'brands_languages','idBrand','idLanguage')->withPivot('defaultLanguage');
     } 
 
}