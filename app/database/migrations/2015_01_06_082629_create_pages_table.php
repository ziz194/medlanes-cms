<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

		 Schema::create('pages', function(Blueprint $table)
 			{
				$table->increments('idPage');
 				$table->string('namePage');
				$table->string('urlPage')->unique();
 				$table->string('createdBy');
 				$table->unsignedinteger('idTemplate') ;
 				$table->timestamps();//creates timestamps for created and updated at
 			});




			   Schema::table('pages', function(Blueprint $table)
    {
        $table->foreign('idTemplate')->references('idTemplate')->on('templates');
    }); 
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		 Schema::drop('pages');
	}

}
