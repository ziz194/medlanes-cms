<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
			 Schema::create('contents', function(Blueprint $table)
 			{
				$table->increments('idContent');
 				$table->string('nameContent');
				$table->string('typeContent') ;
 				$table->text('dataContent');
 				$table->timestamps();//creates timestamps for created and updated at
 			});


			    Schema::table('languages_pages', function(Blueprint $table)
    {
        $table->foreign('idPage')->references('idPage')->on('pages');
    });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('contents');
	}

}
