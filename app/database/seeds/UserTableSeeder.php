<?php
 
class UserTableSeeder extends Seeder {
 
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    DB::table('users')->delete();
 
        User::create(array(
 
            'first_name'    => 'Aziz',
            'last_name'     => 'Haddad',
            'email'         => 'aziz.haddad@medlanes.com',
            'username'      => 'admin',
            'password'      => Hash::make('admin') //hashes our password nicely for us
 
        ));
 
    }
 
}

?>