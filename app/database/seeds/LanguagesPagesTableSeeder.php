<?php
 
class LanguagesPagesTableSeeder extends Seeder {
 
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    DB::table('languages_pages')->delete();
 
        LanguagePage::create(array(
 
           'idPage' => 1,
           'idLanguage' =>2 ,
            'languagePageUrl' => 'http://www.medlanes.com/doctors?lang=de'

        ));

    
        LanguagePage::create(array(
 
           'idPage' => 1,
           'idLanguage' =>1 ,
            'languagePageUrl' => 'http://www.medlanes.com/doctors'

        ));


        LanguagePage::create(array(
 
           'idPage' => 2,
           'idLanguage' =>2 ,
            'languagePageUrl' => 'http://www.medlanes.com/faq?lang=de'

        ));


        LanguagePage::create(array(
 
           'idPage' => 2,
           'idLanguage' =>1 ,
            'languagePageUrl' => 'http://www.medlanes.com/faq'

        ));


        LanguagePage::create(array(
 
           'idPage' => 3,
           'idLanguage' =>1 ,
            'languagePageUrl' => 'http://www.medlanes.com/'

        ));


        LanguagePage::create(array(
 
           'idPage' => 3,
           'idLanguage' =>2 ,
            'languagePageUrl' => 'http://www.medlanes.com/?lang=de'

        ));

             
 
    }
 
}

?>