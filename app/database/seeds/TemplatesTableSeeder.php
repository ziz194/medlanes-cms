<?php
 
class TemplatesTableSeeder extends Seeder {
 
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    DB::table('templates')->delete();
 
        Template::create(array(
 
           'nameTemplate' => 'Condition Template',
           'urlTemplate' => '/CMS_Templates/condition.html'

        ));

           Template::create(array(
 
            'nameTemplate' => 'Test Template',
           'urlTemplate' => '/CMS_Templates/test.html'

        ));

 
    }
 
}

?>