<?php

class BrandController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$brands = Brand::all();
        return $brands ;
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
	    $new = new Brand;
		$languages = Language::all();
        $new->nameBrand   = Input::get('brandName');
        $new->domainBrand = Input::get('brandDomain');
        $new->folderBrand = Input::get('brandFolder');
      	$new->createdBy   = Input::get('createdBy');

        if($new->save()){
        	$result = File::makeDirectory('cms_pages/'.$new->folderBrand , 0777, true);
	  	    foreach ($languages as $language) {
	  	    if ((Input::get('brandLanguages'.$language->idLanguage))==true){  	  	                    
           		 $new->languages()->attach($language->idLanguage,array('defaultLanguage' => Input::get('defaultLanguage'.$language->idLanguage)));
           		 $result = File::makeDirectory('cms_pages/'.$new->folderBrand.'/'.$language->prefix.'_files', 0777, true);
           		 $result = File::makeDirectory('cms_pages/'.$new->folderBrand.'/'.$language->prefix.'_files/drafts', 0777, true);
        	 }
        	} 
        }
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$brand = Brand::find($id) ; 
        return $brand ; 
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$brand = Brand::find($id);
		if($brand){
			$brand->nameBrand    =   Input::get('nameBrand');
			$result = File::move('cms_pages/'.$brand->folderBrand , 'cms_pages/'.Input::get('folderBrand'));
			$brand->folderBrand     =   Input::get('folderBrand');
			$brand->domainBrand     =   Input::get('domainBrand');
  		    $brand->save() ; 

  		    return 'Brand has been updated' ;

		}

		else{

			return 'Brand cannot be found '  ;
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$brand = Brand::find($id);
		if (count(glob('cms_pages/'.$brand->folderBrand.'/*.php')) === 0 ) { 
			File::deleteDirectory('cms_pages/'.$brand->folderBrand) ; 
			echo 'Folder Deleted' ;
			$languages = Language::all(); 
			foreach ($languages as $language) {
				$brand->languages()->detach($language->idLanguage);

			}
			$brand->delete();
		}

	else {

		echo  'NO' ; 
	}

	}


}
