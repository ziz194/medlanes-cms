<?php

class PageController extends \BaseController {

	public function createDraft()
	{
		    $new = new Page;
		    $language = new Language ; 
	        $new->namePage 	 = Input::get('pageName');
	        $currentLanguageUrl = $language->getLanguageById(Input::get('currentLanguage'))  ; 
	        $brand = Brand::find(Input::get('idBrand')) ; 
    	    $languageFolderUrl = $currentLanguageUrl->prefix.'_files/drafts/' ;     
		    $new->urlPage = $brand->folderBrand.'/'.$languageFolderUrl.Input::get('pageUrl1') ;
		    $new->fileName = Input::get('pageUrl1') ;
	        $new->active  = 0;
	        $new->idTemplate = Input::get('idTemplate');
	        $new->tags = Input::get('tags');
        	//tags 
	        $tagString = Input::get('tags');
	        $tagsArray = explode(',', $tagString);
	  //       foreach ($tagsArray as $tag) {
	  //       $input['tagName'] = $tag ;
			// $rules = array('tagName' => 'unique:tags,nameTag');
			// $validator = Validator::make($input , $rules);

			// if ($validator->fails()) {
			// }
			// else{
			
   //      	$newTag = new Tag ; 
   //      	$newTag->nameTag = $tag ; 
   //      	$newTag->save() ;
		        
			// }
	  //       }
	        //end Tags
	        $new->idBrand = Input::get('idBrand') ;
	        $new->idLanguage = Input::get('currentLanguage') ;
	      	$new->editedBy  = Input::get('createdBy');
	      	$new->createdBy  = Input::get('createdBy');
	      	$new->link  = 1;

	      	   //displayed Values
	        	$displayedCheck = Input::get('displayedCheck') ;

				if ($displayedCheck == 0 ){
					$new->shownAuthor = Input::get('createdBy') ;
					$new->shownDate = date('Y-m-d H:i:s');
				}

				else{

					$new->shownAuthor = Input::get('shownAuthor') ;
					$new->shownDate = Input::get('shownDate') ;
				}


	        $input['url'] = Input::get('pageUrl1') ;
			$rules = array('url' => 'unique:pages,fileName');
			$validator = Validator::make($input , $rules);

			if ($validator->fails()) {
			    return 'NO' ;
			}
			else {

        //Get the template file name 

            $template = Template::find($new->idTemplate);
            $templateURL = $template->urlTemplate ; 
            $currentLanguageId = Input::get('currentLanguage') ;


        if($new->save()){
        	if($currentLanguageId==1) {
	         	  $otherlanguages = Language::all();
	         	  foreach ($otherlanguages as $otherlanguage) {
	         	  	$linkId = Input::get('linkId'.$otherlanguage->idLanguage);
	         	   	if($otherlanguage->idLanguage != 1){
	         	   		 $linkedPage = Page::find($linkId);
	         	   		  if($linkedPage != null){
	         	   		 $linkedPage->link = $new->idPage ;
	         	   		 $linkedPage->save() ;
	         	   		}
	         	   		 $new->link  = $new->idPage;
	         	   		 $new->save() ;
	         	  	}       	  
	         	 }
	         	 }

	    

  //The PHP code that will be inserted in the new page 

        $php = '<?php $idPage = "'.$new->idPage.'";include("../../../../'.$templateURL.'"); ?>' ;

    	// Create the page in the language specific folder 
   
        $bytes_written = File::put('cms_pages/'.$brand->folderBrand.'/'.$languageFolderUrl.Input::get('pageUrl1').'.php',$php);
			if ($bytes_written === false)
			{
			    die("Error writing to file");
			} 

        return $new;
 }
       else  return array('status'=>'Not Saved!');
	
}
}


  
public function activatePage($id)
	{
			$page = Page::find($id) ; 

		if($page) {

			//Change File Name
				$currentLanguageId = Input::get('currentLanguage') ;
				$currentBrandId = Input::get('idBrand') ;
             	$language=Language::find($currentLanguageId) ; 
             	$brand = Brand::find($currentBrandId);
             	$currentLanguageUrl = $language->getLanguageById(Input::get('currentLanguage'))  ; 
             	$languageFolderUrl = $currentLanguageUrl->prefix.'_files' ;  

             	//Delete the Draft file and create a new file in the brand folder ;   
             	$template = Template::find($page->idTemplate);
             	$templateURL = $template->urlTemplate ;
             	$php = '<?php $idPage = "'.$page->idPage.'";include("../../../'.$templateURL.'"); ?>' ;  
             	File::delete('cms_pages/'.$page->urlPage.'.php');
             	$bytes_written = File::put('cms_pages/'.$brand->folderBrand.'/'.$languageFolderUrl.'/'.Input::get('fileName').'.php',$php);
				if ($bytes_written === false)
				{
				    die("Error writing to file");
				} 
				$page->namePage = Input::get('namePage') ;
				$page->urlPage = $brand->folderBrand.'/'.$languageFolderUrl.'/'.Input::get('fileName') ;
				$page->fileName = Input::get('fileName') ;
				$page->idTemplate = Input::get('idTemplate') ;
				$page->editedBy  = Input::get('editedBy');
				$page->link  = 1;
				$page->active = 1 ;
				$page->idLanguage = Input::get('currentLanguage') ;

			   //displayed Values
	        	$displayedCheck = Input::get('displayedCheck') ;

				if ($displayedCheck == 1 ){
					
					$page->shownAuthor = Input::get('shownAuthor') ;
					$page->shownDate = Input::get('shownDate') ;
				}




        if($page->save()){

        	// Pushing the page to the app API 

 					$date1 = strtotime($page->created_at);
					$date2 = strtotime($page->updated_at);
					$created = date('Y-m-d H:i:s',$date1) ; 
					$updated =   date('Y-m-d H:i:s',$date2) ; 





        	if($page->idTemplate == 37 ||  $page->idTemplate == 39) {
        		$post_data = array(
		        'action' => 'new_cms_page',
		        'adminkey' => 'sumba3820',
		        'parameters' => array(
		        	'page_title' => $page->namePage,
		            'page_content' => $page->overviewText,
		            'page_overview_image' => $page->overviewImage,
		            'created' => $created,
		            'updated' => $updated,
		            'idPage' => $page->idPage,
		            'created_by' => $page->createdBy,
		            'active' => $page->active,
		            'idTemplate' => $page->idTemplate
		        	) 
				);

					$ch = curl_init();
	

					curl_setopt($ch, CURLOPT_URL,"https://api.medlanes.com/api_1/call/json");
					curl_setopt($ch, CURLOPT_POST, count($post_data));
					curl_setopt($ch, CURLOPT_POSTFIELDS,array("request"=> json_encode($post_data)));
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					$server_output = curl_exec ($ch);
					echo $server_output;

         echo('Created : '.$created) ; 
         echo('Updated : '.$updated) ;
					if ($errno = curl_errno($ch)) {
					    echo $errno;
					} 
					curl_close ($ch);

        	}
        }

        	// API End

				
			
        	if($currentLanguageId==1) {

	            	  $otherlanguages = Language::all();
	         	  foreach ($otherlanguages as $otherlanguage) {
	         	  	$linkId = Input::get('linkId'.$otherlanguage->idLanguage);
	         	   	if($otherlanguage->idLanguage != 1){
	         	   		 $linkedPage = Page::find($linkId);
	         	   		 if($linkedPage != null){
	         	   		 $linkedPage->link = $page->idPage ;
	         	   		 $linkedPage->save() ;
	         	   		}
	         	   		 $page->link  = $page->idPage;
	         	   		 $page->save() ;
	         	  	}       	  
	         	 }
			} 
			
	}

			else {


			echo 'Page not found' ;
		}
}


public function activateAllPagesByBrand($id)
	{
			$query = Page::where('idBrand',$id)->where('active',0); 
			$pages = $query->get() ;
			foreach ($pages as $page) {
				$template = Template::find($page->idTemplate);
             	$templateURL = $template->urlTemplate ;
             	$php = '<?php $idPage = "'.$page->idPage.'";include("../../../'.$templateURL.'"); ?>' ;  
             	File::delete('cms_pages/'.$page->urlPage.'.php');
             	$bytes_written = File::put('cms_pages/medlanes.com/en_files/'.$page->fileName.'.php',$php);
				if ($bytes_written === false)
				{
				    die("Error writing to file");
				} 

				$page->active = 1 ;
				$page->urlPage = 'medlanes.com/en_files/'.$page->fileName ;  
				$page->save() ;
				echo 'page activated' ;
			}
}









public function deactivatePage($id)
	{
			$page = Page::find($id) ; 

		if($page) {

			//Change File Name
				$currentLanguageId = Input::get('currentLanguage') ;
				$currentBrandId = Input::get('idBrand') ;
             	$language=Language::find($currentLanguageId) ; 
             	$brand = Brand::find($currentBrandId);
             	$currentLanguageUrl = $language->getLanguageById(Input::get('currentLanguage'))  ; 
             	$languageFolderUrl = $currentLanguageUrl->prefix.'_files/drafts' ;  

             	//Delete the Draft file and create a new file in the brand folder ;   
             	$template = Template::find($page->idTemplate);
             	$templateURL = $template->urlTemplate ;
             	$php = '<?php $idPage = "'.$page->idPage.'";include("../../../../'.$templateURL.'"); ?>' ;  
             	File::delete('cms_pages/'.$page->urlPage.'.php');
             	$bytes_written = File::put('cms_pages/'.$brand->folderBrand.'/'.$languageFolderUrl.'/'.Input::get('fileName').'.php',$php);
				if ($bytes_written === false)
				{
				    die("Error writing to file");
				} 
				$page->namePage = Input::get('namePage') ;
				$page->urlPage = $brand->folderBrand.'/'.$languageFolderUrl.'/'.Input::get('fileName') ;
				$page->fileName = Input::get('fileName') ;
				$page->idTemplate = Input::get('idTemplate') ;
				$page->editedBy  = Input::get('editedBy');
				$page->link  = 1;
				$page->active = 0 ;
				$page->idLanguage = Input::get('currentLanguage') ;
				$page->save() ;
			
        	if($currentLanguageId==1) {

	            	  $otherlanguages = Language::all();
	         	  foreach ($otherlanguages as $otherlanguage) {
	         	  	$linkId = Input::get('linkId'.$otherlanguage->idLanguage);
	         	   	if($otherlanguage->idLanguage != 1){
	         	   		 $linkedPage = Page::find($linkId);
	         	   		 if($linkedPage != null){
	         	   		 $linkedPage->link = $page->idPage ;
	         	   		 $linkedPage->save() ;
	         	   		}
	         	   		 $page->link  = $page->idPage;
	         	   		 $page->save() ;
	         	  	}       	  
	         	 }
			} 
			
	}

			else {


			echo 'Page not found' ;
		}
}

	public function updatePage($id)
	{
		$page = Page::find($id) ; 

		if($page) {

			//Change File Name
				$currentLanguageId = Input::get('currentLanguage') ;
             	$language=Language::find($currentLanguageId) ; 
             	$currentLanguageUrl = $language->getLanguageById(Input::get('currentLanguage'))  ; 
             	$brand = Brand::find(Input::get('idBrand')) ; 
             	$languageFolderUrl = $currentLanguageUrl->prefix.'_files' ;    

             	if ($page->active == 0 ){
             		File::move('cms_pages/'.$page->urlPage.'.php','cms_pages/'.$brand->folderBrand.'/'.$languageFolderUrl.'/drafts/'.Input::get('fileName').'.php') ;	
             		$page->urlPage = $brand->folderBrand.'/'.$languageFolderUrl.'/drafts/'.Input::get('fileName') ;

             	}  
             	else{
             	    File::move('cms_pages/'.$page->urlPage.'.php','cms_pages/'.$brand->folderBrand.'/'.$languageFolderUrl.'/'.Input::get('fileName').'.php') ;	
             	    $page->urlPage = $brand->folderBrand.'/'.$languageFolderUrl.'/'.Input::get('fileName') ;

             	}
				$page->namePage = Input::get('namePage') ;
				$page->fileName = Input::get('fileName') ;
				$page->idTemplate = Input::get('idTemplate') ;
				$page->editedBy  = Input::get('editedBy');
				$page->tags = Input::get('tags');
		        //tags 
		        $tagString = Input::get('tags');
		        $tagsArray = explode(',', $tagString);
		  //       foreach ($tagsArray as $tag) {
		  //       $input['tagName'] = $tag ;
				// $rules = array('tagName' => 'unique:tags,nameTag');
				// $validator = Validator::make($input , $rules);

				// if ($validator->fails()) {
				// }
				// else{
				
	   //      	$newTag = new Tag ; 
	   //      	$newTag->nameTag = $tag ; 
	   //      	$newTag->save() ;
			        
				// }
		  //       }
		        //end Tags
				$page->link  = 1;

				   //displayed Values
	        	$displayedCheck = Input::get('displayedCheck') ;

				if ($displayedCheck == 1 ){
					$page->shownAuthor = Input::get('shownAuthor') ;
					$page->shownDate = Input::get('shownDate') ;
				}

	
				$page->idLanguage = Input::get('currentLanguage') ;
				$input['url'] = $brand->folderBrand.'/'.$languageFolderUrl.'/'.Input::get('fileName') ;
			$rules = array('url' => 'unique:pages,fileName');
			$validator = Validator::make($input , $rules);

			if ($validator->fails()) {
			    return 'NO' ;
			}
			else {

				if ($page->save()) 

				{ 

		
			
			        	// Pushing the page to the app API 

        	if($page->idTemplate == 37 ||  $page->idTemplate == 39) {

        		if ($page->active == 1)
        		{

      				$date1 = strtotime($page->created_at);
					$date2 = strtotime($page->updated_at);
					$date3 = strtotime($page->shownDate);
					$created = date('Y-m-d H:i:s',$date1) ; 
					$updated =   date('Y-m-d H:i:s',$date2) ; 
					$shownDate =  date('Y-m-d H:i:s',$date3) ; 

        		$post_data = array(
		        'action' => 'new_cms_page',
		        'adminkey' => 'sumba3820',
		        'parameters' => array(
		        	'page_title' => $page->namePage,
		            'page_content' => $page->overviewText,
		            'page_overview_image' => $page->overviewImage,
		            'created' => $created,
		            'updated' => $updated,
		            'idPage' => $page->idPage,
		            'created_by' => $page->createdBy,
		            'active' => $page->active,
		            'idTemplate' => $page->idTemplate, 
		            'shownDate' => $shownDate
		        	) 
				);

					$ch = curl_init();
	

					curl_setopt($ch, CURLOPT_URL,"https://api.medlanes.com/api_1/call/json");
					curl_setopt($ch, CURLOPT_POST, count($post_data));
					curl_setopt($ch, CURLOPT_POSTFIELDS,array("request"=> json_encode($post_data)));
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					$server_output = curl_exec ($ch);
					echo $server_output;
					if ($errno = curl_errno($ch)) {
					    echo $errno;
					} 
					curl_close ($ch);

        	}
        }

        	// API End
			
        	if($currentLanguageId==1) {

	            	  $otherlanguages = Language::all();
	         	  foreach ($otherlanguages as $otherlanguage) {
	         	  	$linkId = Input::get('linkId'.$otherlanguage->idLanguage);
	         	   	if($otherlanguage->idLanguage != 1){
	         	   		 $linkedPage = Page::find($linkId);
	         	   		 if($linkedPage != null){
	         	   		 $linkedPage->link = $page->idPage ;
	         	   		 $linkedPage->save() ;
	         	   		 }
	         	   		 $page->link  = $page->idPage;
	         	   		 $page->save() ;


	         	  	}       	  
	         	 }
			} 
			
	}

}
}


		
}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$pages = Page::all();
        return $pages;

    }



	public function getPagesByDate($startDate,$endDate) 
	{ 
								
			$date1 = strtotime($startDate);
			$date2 = strtotime($endDate);
			$startDate = date('Y-m-d H:i:s',$date1) ; 
			$endDate =   date('Y-m-d H:i:s',$date2) ; 
  			$pages= Page::whereBetween('created_at',array($startDate,$endDate))->orderBy('created_at', 'desc')->get();     
    		return $pages ;
	}

	public function getTotalPagesByDate($startDate,$endDate) 
	{

			$date1 = strtotime($startDate);
			$date2 = strtotime($endDate);
			$startDate = date('Y-m-d H:i:s',$date1) ; 
			$endDate =   date('Y-m-d H:i:s',$date2) ; 
  			$totalPages = Page::whereBetween('created_at',array($startDate,$endDate))->orderBy('created_at', 'desc')->count();
  			return $totalPages ; 
  	}

   


    public function getPageTemplate($idPage)
	{


        $page = Page::find($idPage) ; 
		$templateId = $page->idTemplate ;
		$template = Template::find($templateId) ;
        $contents = $template->inputsTemplate ;
        return $contents ; 
	}

	public function renderPage($idPage)
	{


       return View::make('condition_template')->with('idPage',$idPage);

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{




		    $new = new Page;
		    $language = new Language ; 
	        $new->namePage 	 = Input::get('pageName');
	        $currentLanguageUrl = $language->getLanguageById(Input::get('currentLanguage'))  ; 
	        $brand = Brand::find(Input::get('idBrand')) ; 
    	    $languageFolderUrl = $currentLanguageUrl->prefix.'_files' ;   
	        $new->urlPage  	 = $brand->folderBrand.'/'.$languageFolderUrl.'/'.Input::get('pageUrl1') ;
	        $new->fileName = Input::get('pageUrl1') ;
	        $new->active  = Input::get('active') ;
	        $new->idTemplate = Input::get('idTemplate');
	        $new->tags = Input::get('tags');
	        //tags 
	        $tagString = Input::get('tags');
	  //       $tagsArray = explode(',', $tagString);
	  //       foreach ($tagsArray as $tag) {
	  //       $input['tagName'] = $tag ;
			// $rules = array('tagName' => 'unique:tags,nameTag');
			// $validator = Validator::make($input , $rules);

			// if ($validator->fails()) {
			// }
			// else{
			
   //      	$newTag = new Tag ; 
   //      	$newTag->nameTag = $tag ; 
   //      	$newTag->save() ;
		        
			// }
	  //       }
	        //end Tags



	        $new->idLanguage = Input::get('currentLanguage') ;
	        $new->idBrand = Input::get('idBrand') ;
	      	$new->createdBy  = Input::get('createdBy') ;
	      	$new->editedBy  = Input::get('createdBy');
	      	$new->link  = 0;
	    


	      	// Sub Page 
	      	$new->masterPage = Input::get('masterPageId') ;
	        $new->parameter = Input::get('parameter') ;

	        //displayed Values
	        	$displayedCheck = Input::get('displayedCheck') ;

				if ($displayedCheck == 0 ){
					$new->shownAuthor = Input::get('createdBy') ;
					$new->shownDate = date('Y-m-d H:i:s');
				}

				else{

					$new->shownAuthor = Input::get('shownAuthor') ;
					$new->shownDate = Input::get('shownDate') ;
				}


	      	$input['url'] = Input::get('pageUrl1') ;
			$rules = array('url' => 'unique:pages,fileName');
			$validator = Validator::make($input , $rules);

			if ($validator->fails()) {
			    return 'NO' ;
			}
			else {

        //Get the template file name 

            $template = Template::find($new->idTemplate);
            $templateURL = $template->urlTemplate ; 
            $currentLanguageId = Input::get('currentLanguage') ;


        if($new->save()){

        	// Pushing the page to the app API 

    //     	if($new->idTemplate == 37 ||  $new->idTemplate == 39) {
    //     		$post_data = array(
		  //       'action' => 'new_cms_page',
		  //       'adminkey' => 'sumba3820',
		  //       'parameters' => array(
		  //       	'page_title' => $new->namePage,
		  //           'page_content' => $new->overviewText,
		  //           'page_overview_image' => $new->overviewImage,
		  //           'created' => $new->created_at,
		  //           'updated' => $new->updated_at,
		  //           'idPage' => $new->idPage,
		  //           'created_by' => $new->createdBy,
		  //           'active' => $new->active,
		  //           'idTemplate' => $new->idTemplate
		  //       	) 
				// );

				// 	$ch = curl_init();
	

				// 	curl_setopt($ch, CURLOPT_URL,"https://api.medlanes.com/api_1/call/json");
				// 	curl_setopt($ch, CURLOPT_POST, count($post_data));
				// 	curl_setopt($ch, CURLOPT_POSTFIELDS,array("request"=> json_encode($post_data)));
				// 	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				// 	$server_output = curl_exec ($ch);
				// 	echo $server_output;
				// 	if ($errno = curl_errno($ch)) {
				// 	    echo $errno;
				// 	} 
				// 	curl_close ($ch);

    //     	}

        	// API End

        	if($currentLanguageId==1) {
	         	  $otherlanguages = Language::all();
	         	  foreach ($otherlanguages as $otherlanguage) {
	         	  	$linkId = Input::get('linkId'.$otherlanguage->idLanguage);
	         	   	if($otherlanguage->idLanguage != 1){
	         	   		 $linkedPage = Page::find($linkId);
	         	   		 if($linkedPage != null){
	         	   		 $linkedPage->link = $new->idPage ;
	         	   		 $linkedPage->save() ;
	         	   		 }
	         	   		 $new->link  = $new->idPage;
	         	   		 $new->save() ;
	         	  	}       	  
	         	 }
	         	 }

	         	

  //The PHP code that will be inserted in the new page 

        $php = '<?php $idPage = "'.$new->idPage.'";include("../../../'.$templateURL.'"); ?>' ;

    	// Create the page in the language specific folder 
   		if($new->masterPage == null){ 
        $bytes_written = File::put('cms_pages/'.$brand->folderBrand.'/'.$languageFolderUrl.'/'.Input::get('pageUrl1').'.php',$php);
			if ($bytes_written === false)
			{
			    die("Error writing to file");
			} 
		}

        return $new;
 }
       else  return array('status'=>'Not Saved!');
	
}
}



	
	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
    
        $page = Page::find($id); 
        return $page;


 
	}

		public function getPageByName($urlPage)
	{
    
        $page = Page::where('fileName',$urlPage)->first(); 
        return $page;

	}





	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$page = Page::find($id);

			$idBrand = $page->idBrand ;
			$brand = Brand::find($idBrand);
			$languageId = $page->idLanguage ; 
            $language=Language::find($languageId) ; 
            $languageFolderUrl = $language->prefix.'_files' ; 
            File::delete('cms_pages/'.$brand->folderBrand.'/'.$languageFolderUrl.'/'.$page->fileName.'.php');
            
            	foreach ($page->contents as $content) {
                $content->delete();
            }
             
				 $page->delete();



				echo 'The Page has been deleted successfully' ;
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function findLanguageUrl($idPage,$idLanguage)
	{
		
		$language = Language::find($idLanguage);
		// if($idLanguage == 1) {
		// 	$page2 = Page::where('link',$idPage)->where('idLanguage',$idLanguage)->where('idPage',$idPage)->first();
		// }
		// else{
			$linkPage = Page::find($idPage) ;
			$page2 = Page::where('link',$linkPage->link)->where('idLanguage',$idLanguage)->first();
		// }
		
        if($page2!=null){$page2->urlPage = $language->urlLanguage.$page2->urlPage.'.php' ;
        return $page2 ; 
    	}

    	else {

    		return 'No Translation in this Language available' ;
    	}
		

		
	}

		public function getPageUrl($idPage,$idLanguage)
	{
		
		$page = Page::find($idPage);
		$language = Language::find($idLanguage) ;
		$page->urlPage = $language->urlLanguage.$page->urlPage.'.php' ;
		return $page ;

		
	}



	public function getPagesbyLanguage($idLanguage)
	{
		

		$pages = Page::where('idLanguage',$idLanguage) ;
		return $pages->get() ;

		}
	public function getPagesbyBrandAndLanguage($idBrand,$idLanguage)
	{
		

		$pages = Page::where('idLanguage',$idLanguage)->where('idBrand',$idBrand) ;
		return $pages->get() ;

		}


		// Get Questions by page through tags 

		public function getQuestionsByPage($idPage) 
		{ 

			$page =  Page::find($idPage);
			$pageTags = $page->tags ;
			$tagarr = explode(',',$pageTags) ; 
			$query=Page::where('idTemplate', 18)->where('urlPage','!=' ,$page->urlPage);
			$query->where(function($query) use ($tagarr){
				foreach($tagarr as $tag){
					$query->orWhere('tags','LIKE','%'.$tag.'%');
				}	
			});
    		$questions= $query->orderBy(DB::raw('RAND()'))->take(5)->get();
    		return $questions ; 
		}

				public function getAllQuestionsByPage($idPage) 
		{ 

			$page =  Page::find($idPage);
			$pageTags = $page->tags ;
			$tagarr = explode(',',$pageTags) ; 
			$query=Page::where('idTemplate', 18)->where('urlPage','!=' ,$page->urlPage);
			$query->where(function($query) use ($tagarr){
				foreach($tagarr as $tag){
					$query->orWhere('tags','LIKE','%'.$tag.'%');
				}	
			});
    		$questions= $query->get();
    		return $questions ; 
		}

			public function getAllRelatedPages($idPage) 
		{ 

			$page =  Page::find($idPage);
			$pageTags = $page->tags ;
			$tagarr = explode(',',$pageTags) ; 
		    $query=Page::where('idPage','!=',$idPage)->where('idTemplate',14)->where('active',1);
			$query->where(function($query) use ($tagarr){
				foreach($tagarr as $tag){
					$query->orWhere('tags','LIKE','%'.$tag.'%');
				}	
			});
    		$relatedPages= $query->orderBy('namePage', 'ASC')->get();
    		return $relatedPages ; 
		}

			public function getRelatedPages($idPage) 
		{ 

			$page =  Page::find($idPage);
			$pageTags = $page->tags ;
			$tagarr = explode(',',$pageTags) ; 
		    $query=Page::where('idPage','!=',$idPage)->where('idTemplate',14)->where('active',1);
			$query->where(function($query) use ($tagarr){
				foreach($tagarr as $tag){
					$query->orWhere('tags','LIKE','%'.$tag.'%');
				}	
			});
    		$relatedPages= $query->orderBy(DB::raw('RAND()'))->take(5)->get();
    		return $relatedPages ; 
		}

		public function getRelatedSymptoms($idPage) 
		{ 

			$page =  Page::find($idPage);
			$pageTags = $page->tags ;
			$tagarr = explode(',',$pageTags) ; 

		    $query=Page::where('idPage','!=',$idPage)->where('idTemplate',32)->where('active',1);
			$query->where(function($query) use ($tagarr){
				foreach($tagarr as $tag){
					$query->orWhere('tags','LIKE','%'.$tag.'%');
				}	
			});
    		$relatedPages= $query->orderBy(DB::raw('RAND()'))->take(5)->get();
    		return $relatedPages ; 
		}

		public function getRelatedDiagnosis($idPage) 
		{ 

			$page =  Page::find($idPage);
			$pageTags = $page->tags ;
			$tagarr = explode(',',$pageTags) ; 

		    $query=Page::where('idPage','!=',$idPage)->where('idTemplate',33)->where('active',1);
			$query->where(function($query) use ($tagarr){
				foreach($tagarr as $tag){
					$query->orWhere('tags','LIKE','%'.$tag.'%');
				}	
			});
    		$relatedPages= $query->orderBy(DB::raw('RAND()'))->take(5)->get();
    		return $relatedPages ; 
		}
				public function getRelatedRiskFactors($idPage) 
		{ 

			$page =  Page::find($idPage);
			$pageTags = $page->tags ;
			$tagarr = explode(',',$pageTags) ; 

		    $query=Page::where('idPage','!=',$idPage)->where('idTemplate',34)->where('active',1);
			$query->where(function($query) use ($tagarr){
				foreach($tagarr as $tag){
					$query->orWhere('tags','LIKE','%'.$tag.'%');
				}	
			});
    		$relatedPages= $query->orderBy(DB::raw('RAND()'))->take(5)->get();
    		return $relatedPages ; 
		}

		public function getRelatedTreatments($idPage) 
		{ 

			$page =  Page::find($idPage);
			$pageTags = $page->tags ;
			$tagarr = explode(',',$pageTags) ; 

		    $query=Page::where('idPage','!=',$idPage)->where('idTemplate',35)->where('active',1);
			$query->where(function($query) use ($tagarr){
				foreach($tagarr as $tag){
					$query->orWhere('tags','LIKE','%'.$tag.'%');
				}	
			});
    		$relatedPages= $query->orderBy(DB::raw('RAND()'))->take(5)->get();
    		return $relatedPages ; 
		}

			public function getBlogPosts($articleStart,$tag,$searchQuery,$number) 
		{ 


			if($tag=='all'){
		    	$query=Page::whereIn('idTemplate',array(37,39))->where('active',1)->where('shown',1)->where('namePage','LIKE','%'.$searchQuery.'%');
			}
			else{
				$pageTags = $tag ;
				$tagarr = explode(',',$pageTags) ; 
				$query=Page::whereIn('idTemplate',array(37,39))->where('active',1)->where('shown',1)->where('namePage','LIKE','%'.$searchQuery.'%');
				$query->where(function($query) use ($tagarr){
				foreach($tagarr as $tag){
					$query->orWhere('tags','LIKE','%'.$tag.'%');
				}	
				});
			}
            if ($number=='all') {
    		$blogPosts= $query->orderBy('shownDate', 'desc')->get();
            }
            else{
            $blogPosts= $query->orderBy('shownDate', 'desc')->skip($articleStart)->take($number)->get();
            }
            
            $all_blogPosts = array();
            foreach ($blogPosts as $blog){
            	$blog->created_at = $blog->shownDate != null ? $blog->shownDate : $blog->created_at; 
            	$all_blogPosts[] = $blog;
            }
    		return $all_blogPosts ; 
		}



			public function getBlogPostsByDate($articleStart,$tag,$number,$startDate,$endDate) 
			{ 
					if($tag=='all'){
			    	$query=Page::whereIn('idTemplate',array(37,39))->where('active',1)->where('shown',1);
				}
					else{
						$pageTags = $tag ;
						$tagarr = explode(',',$pageTags) ; 
						$query= Page::whereIn('idTemplate',array(37,39))->where('active',1)->where('shown',1);
						$query->where(function($query) use ($tagarr){
						foreach($tagarr as $tag){
							$query->orWhere('tags','LIKE','%'.$tag.'%');
						}	
						});
					}
					if ($number=='all') {
						$blogPosts= $query->whereBetween('created_at',array($startDate,$endDate))->orderBy('created_at', 'desc')->get();   
					}
		            else{
		            	$blogPosts= $query->whereBetween('created_at',array($startDate,$endDate))->orderBy('created_at', 'desc')->skip($articleStart)->take($number)->get();
		            }

					$date1 = strtotime($startDate);
					$date2 = strtotime($endDate);
					$startDate = date('Y-m-d H:i:s',$date1) ; 
					$endDate =   date('Y-m-d H:i:s',$date2) ; 
          			$blogPosts= $query->whereBetween('created_at',array($startDate,$endDate))->orderBy('created_at', 'desc')->get();
            
    		return $blogPosts ;
			}








			public function totalPosts($articleStart,$tag,$searchQuery) 
		{ 
			if($tag=='all'){
		    	$query=Page::whereIn('idTemplate',array(37,39))->where('active',1)->where('shown',1)->where('namePage','LIKE','%'.$searchQuery.'%');;
			}
			else{
				$pageTags = $tag ;
				$tagarr = explode(',',$pageTags) ; 
				$query=Page::whereIn('idTemplate',array(37,39))->where('active',1)->where('shown',1)->where('namePage','LIKE','%'.$searchQuery.'%');;
				$query->where(function($query) use ($tagarr){
				foreach($tagarr as $tag){
					$query->orWhere('tags','LIKE','%'.$tag.'%');
				}	
				});
			}
    		$totalPosts= $query->count() ;
    		return $totalPosts ; 
		}


			public function getLastBlogPost() 
		{ 
		    $query=Page::whereIn('idTemplate',array(37,39))->where('active',1)->where('shown',1);
    		$blogPosts= $query->orderBy('created_at', 'desc')->remember(10)->first();
    		return $blogPosts ; 
		}


			public function getIdPageByParameter($parameter) 
		{ 
		    $query=Page::where('parameter','=',$parameter);
    		$page= $query->first();
    		return $page->idPage ; 
		}

			public function getPostsByTags() 
		{ 
		    $query=Page::whereIn('idTemplate',array(37,39))->where('active',1)->where('shown',1);
    		$blogPosts= $query->orderBy('created_at', 'asc')->take(5)->get();
    		return $blogPosts ; 
		}


				public function getRelatedBlogPosts($idPage) 
		{ 

			$page =  Page::find($idPage);
			$pageTags = $page->tags ;
			$tagarr = explode(',',$pageTags) ; 

		    $query=Page::whereIn('idTemplate',array(37,39))->where('active',1)->where('shown',1);
			$query->where(function($query) use ($tagarr){
				foreach($tagarr as $tag){
					$query->orWhere('tags','LIKE','%'.$tag.'%');
				}	
			});
    		$relatedPages= $query->orderBy(DB::raw('RAND()'))->take(5)->get();
    		return $relatedPages ; 
		}


		public function getPagesByNameAndTemplate($templates,$namePage) 
		{ 

			$templatesArr = explode(',', $templates);
		    $tagArr = explode(',',$namePage) ; 
		    $query=Page::whereIn('idTemplate',$templatesArr)->where('active',1)->where('shown',1) ;
		    $query->where(function($query) use ($tagArr){
				foreach($tagArr as $tag){
					$query->orWhere('tags','LIKE','%'.$tag.'%');
				}	
			});
    		$relatedPages= $query->orderBy('created_at', 'asc')->get();
    		return $relatedPages ; 
		}



		public function getPageApp($idPage) 
		{ 

		    $page =  Page::find($idPage) ; 
		    $contents = $page->contents ; 
		    $pageTags = $page->tags ; 
		    $pageTagArr = explode(',',$pageTags) ; 

		    $topicTags = $page->tags ; 
		    $topicTagArr = explode(',',$pageTags) ; 

		    // var_dump($pageTagArr);
		     
		    // var_dump($pageTags);
		     



		    $relatedPages=Page::whereIn('idTemplate',array(37,39))->where('active',1)->where('createdBy','!=','Xenia Schlegel')->where('idPage','!=',$idPage);
		    $relatedPages->where(function($relatedPages) use ($pageTagArr){
				foreach($pageTagArr as $tag){
				// if (strpos($tag,'health') === FALSE) {
					$relatedPages->orWhere('pages.tags','LIKE','%'.$tag.'%');
				// }
				}	
			});
// var_dump($relatedPages->toSql());
// $tags_some  = $relatedPages->select('tags')->take(5)->get();
// var_dump($tags_some->toArray());
// die();
//die();
			$relatedTopics=Topic::where('idTopic','!=',0) ;
		    $relatedTopics->where(function($relatedTopics) use ($pageTagArr){
				foreach($pageTagArr as $tagTopic){
					if (strpos($tagTopic,'health') === FALSE) {
					$relatedTopics->orWhere('tags','LIKE','%'.$tagTopic.'%');
				    }	
				}
			});


$data = array(
    'infos'  => $page,
    'contents' => $page->contents,
    'relatedTopics' => $relatedTopics->orderBy(DB::raw('RAND()'))->take(5)->get() ,
    'relatedPages' => $relatedPages->orderBy(DB::raw('RAND()'))->take(5)->get(),

);


return $data ; 

  	// echo 'Page Info : '.$page;
  	// var_dump($page->contents) ;
  	// echo 'Related Topics :' ;
  	// var_dump($relatedTopics); 
  	// echo 'Related Pages';
  	// var_dump($relatedPages);




}

}