<?php

class LanguageController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$languages = Language::all();
        return $languages;
	}
	public function getLanguagesByBrand($idBrand)
	{
		$brand = Brand::find($idBrand);
		$languages = $brand->languages()->get() ;
        return $languages;
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{

     $language = new Language;
     $language->nameLanguage     =   Input::get('languageName');
     $language->prefix     =   Input::get('languageURL');
     $language->domainLanguage     =   Input::get('languageDomain');
     $language->pathLanguage     =   'cms_pages/'.$language->prefix.'_files' ;
	 $language->save() ; 
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$language = Language::find($id) ; 
        return $language ; 
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		
				$language = Language::find($id);

					if($language){
						$language->nameLanguage    =   Input::get('nameLanguage');
						$result = File::move('cms_pages/'.$language->prefix.'_files' , 'cms_pages/'.Input::get('prefix').'_files');
						$language->prefix     =   Input::get('prefix');
						$language->pathLanguage =  'cms_pages/'.$language->prefix.'_files' ;
						$language->domainLanguage     =   Input::get('languageDomain');
					    $language->urlLanguage     =   'http://www.medlanes.com/medlanescms/cms_pages/'.$language->prefix.'_files' ;
			  		    $language->save() ; 

			  		    return 'Language has been updated' ;

					}

					else{

						return 'Language cannot be found '  ;
					}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$language = Language::find($id);
		$language->delete();


	}

}