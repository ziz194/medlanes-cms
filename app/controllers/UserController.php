<?php

class UserController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$users = User::all();
		foreach ($users as $user) {
		  $user->pageCount = Page::where('createdBy','LIKE','%'.$user->first_name.'%')->count();
			# code...
		}

		return $users ; 
  
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		 $user = new User;
			     $user->first_name     =   Input::get('firstName');
			     $user->last_name      =   Input::get('lastName');
			     $user->email   	   =   Input::get('email');
			     $user->username       =   Input::get('username');
			     $user->password       =   Hash::make(Input::get('password')) ;
			     $user->role           =   Input::get('role');
				 $user->save() ; 
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$user = User::find($id) ; 
        return $user ; 
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		 $user =  User::find($id);

		 if($user){
			     $user->first_name     =   Input::get('first_name');
			     $user->last_name      =   Input::get('last_name');
			     $user->email   	   =   Input::get('email');
			     $user->username       =   Input::get('username');
			     $user->password       =   Hash::make(Input::get('password')) ;
			     $user->role           =   Input::get('role');
				 $user->save() ; 
				}
	}



	public function pageCount($id)
	{
		 $user =  User::find($id);
		 $pageCount = Page::where('createdBy', 'LIKE','%'.$user->first_name.'%')->count();
		 return $pageCount ;  
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$user = User::find($id);
		$user->delete();
	}


}
