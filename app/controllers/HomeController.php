<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function Login(){
if(Auth::attempt(Input::only('password','username'))){
$time = new DateTime() ;
Auth::user()->last_login = $time->format('d-m-Y H:i:s');
Auth::user()->save();
return Auth::user();
}else{
return 'invalid username/pass combo';
}
}
 
public function Logout(){
Auth::logout();
return 'logged out';
}

}
