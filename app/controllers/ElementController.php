<?php

class ElementController extends \BaseController {

  



	public function updateElement($id)
	{
		$element = Element::find($id) ; 

		if($element) {

			//Change File Name

			if (!File::move('cms_elements/'.$element->urlElement.'.php', 'cms_elements/'.Input::get('urlElement').'.php')){
				die("Couldn't rename file");
				}


					$element->nameElement = Input::get('nameElement') ;
					$element->urlElement = Input::get('urlElement') ;
					$element->idTemplate = Input::get('idTemplate') ;
					$element->idLanguage = Input::get('currentLanguage') ;
					$element->tags = Input::get('tags');
					$element->save() ;	
		}

			else {


			echo 'Element not found' ;
		}
}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$elements = Element::all();
        return $elements;

    }

   


    public function getElementTemplate($idElement)
	{


        $element = Element::find($idElement) ; 
		$templateId = $element->idTemplate ;
		$template = Template::find($templateId) ;
        $contents = $template->inputsTemplate ;
        return $contents ; 
	}

	public function renderElement($idElement)
	{


       return View::make('condition_template')->with('idElement',$idElement);

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{

		    $new = new Element;
		    $language = new Language ; 
	        $new->nameElement 	 = Input::get('elementName');
	        $new->urlElement  	 = Input::get('elementUrl1');
	        $new->idTemplate = Input::get('idTemplate');
	        $new->tags = Input::get('tags');
          	$new->createdBy  = Input::get('createdBy');
          	$new->idLanguage = Input::get('currentLanguage') ;

          	$input['url'] = Input::get('elementUrl1');
					$rules = array('url' => 'unique:elements,urlElement');
					$validator = Validator::make($input , $rules);
					if ($validator->fails()) {
					    return 'NO' ;
					}

					else{


          
        //Get the template file name 

            $template = Template::find($new->idTemplate);
            $templateURL = $template->urlTemplate ; 
            $currentLanguageId = Input::get('currentLanguage') ;
            
        if($new->save()){
        //The PHP code that will be inserted in the new element 
        $php = '<?php $idElement = "'.$new->idElement.'";include("../'.$templateURL.'"); ?>' ;

         

        $bytes_written = File::put('cms_elements/'.$new->urlElement.'.php',$php);
			if ($bytes_written === false)
			{
			    die("Error writing to file");
			} 


        return $new;

    }
 
       else  return array('status'=>'Not Saved!');
	}
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
    
        $element = Element::find($id); 
        return $element;

	}


	public function getElementByNameAndLanguage($nameElement,$idLanguage)
	{
    
        $element = Element::where('nameElement',$nameElement)->where('idLanguage',$idLanguage)->first(); 
        return $element;

	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$element = Element::find($id);

		foreach ($element->contents as $content) {
                $content->delete();
            }

            	foreach ($element->contents as $content) {
                $content->delete();
            }
             

				$element->delete();

				//Delete the php file 

				

				File::delete('cms_elements/'.$element->urlElement.'.php');


				echo 'The Element has been deleted successfully' ;
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getElementUrl($idElement)
	{
		

	
		$element = Element::find($idElement) ; 
		    return $element->urlElement; 


	}






	public function getElementsbyLanguage($idLanguage)
	{
		

		$elements = Element::where('idLanguage',$idLanguage)->get() ; 
		    return $elements; 
		}


		// Get Questions by element through tags 

		public function getQuestionsByElement($idElement) 
		{ 

			$element =  Element::find($idElement);
			$elementTags = $element->tags ;
			$tagarr = json_decode($elementTags);
			$query=Element::where('idTemplate', 17)->where('urlElement','!=' ,$element->urlElement);
			$query->where(function($query) use ($tagarr){
				foreach($tagarr as $tag){
					$query->orWhere('tags','LIKE','%'.$tag.'%');
				}	
			});
    		$questions= $query->get() ; 
    		return $questions ; 
		}




public function getElementContents($idElement)
	{

			 $element = Element::find($idElement);
			 return $element->contents;


		}




	public function getElementsByDate($startDate,$endDate) 
	{ 
								
			$date1 = strtotime($startDate);
			$date2 = strtotime($endDate);
			$startDate = date('Y-m-d H:i:s',$date1) ; 
			$endDate =   date('Y-m-d H:i:s',$date2) ; 
  			$elements= Element::whereBetween('created_at',array($startDate,$endDate))->orderBy('created_at', 'desc')->get();     
    		return $elements ;
	}

	public function getTotalElementsByDate($startDate,$endDate) 
	{

			$date1 = strtotime($startDate);
			$date2 = strtotime($endDate);
			$startDate = date('Y-m-d H:i:s',$date1) ; 
			$endDate =   date('Y-m-d H:i:s',$date2) ; 
  			$totalElements = Element::whereBetween('created_at',array($startDate,$endDate))->orderBy('created_at', 'desc')->count();
  			return $totalElements ; 
  	}

  		public function getTotalElementsNumber() 
	{


  			$totalElements = Element::all()->count();
  			return $totalElements ; 
  	}



}
