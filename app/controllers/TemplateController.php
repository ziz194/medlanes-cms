<?php

class TemplateController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$templates = Template::all();
        return $templates;
	}


			public function getTemplatebyLanguage($idLanguage)
	{
		$language = Language::find($idLanguage) ; 
		$templates = $language->templates ;
		return $templates ; 


	}

		public function getPagesTemplatebyLanguage($idLanguage)
	{
		$templates = Template::where('idLanguage',$idLanguage)->where('type', 'Page') ; 
		return $templates->get() ; 


	}

		public function getTemplatesbyBrandAndLanguage($idBrand,$idLanguage)
	{
		

		$templates = Template::where('idLanguage',$idLanguage)->where('idBrand',$idBrand) ;
		return $templates->get() ;

		}

		public function getElementsTemplatebyLanguage($idLanguage)
	{
		$templates = Template::where('idLanguage',$idLanguage)->where('type', 'Element') ; 
		return $templates->get() ; 


	}



public function renderContent($id)
	{
		$template = Template::find($id) ; 
        $contents = File::get($template->urlTemplate);

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function showContent($id)
	{
		$template = Template::find($id) ; 
        $contents = $template->inputsTemplate ;
        return $contents ; 

	}

	public function show($id)
	{
		$template = Template::find($id) ; 
        return $template ; 

	}
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}




	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$template = Template::find($id);
		File::delete($template->urlTemplate);
		File::delete($template->inputsTemplate);
		$template->delete();
	}



		public function addTemplate()
	{


                $file =  Input::file('file') ;
                $inputsFile =  Input::file('inputsFile') ;
                $fileName =  $file->getClientOriginalName() ;
                $inputsFileName =  $inputsFile->getClientOriginalName() ;

                if ($file->isValid()){
                if($file->move('cms_pages/cms_templates/',$fileName))

                {
                	echo 'file moved' ; 
                }
            }

                      if ($inputsFile->isValid()){
                if($inputsFile->move('cms_pages/cms_templates/',$inputsFileName))

                {
                	echo 'inputs file moved' ; 
                }
            }
			     $template = new Template;
			     $template->nameTemplate     =   Input::get('name');
				 $template->urlTemplate    =   'cms_pages/cms_templates/'.$fileName ;
				 $template->inputsTemplate    =   'cms_pages/cms_templates/'.$inputsFileName ; 
				 $template->idLanguage =   Input::get('language');
				 $template->idBrand =   Input::get('brand');
				 $template->type =   Input::get('typeTemplate');


				 $template->save() ; 


	}


			public function updateTemplate()
	{
                  

                $id = Input::get('id') ;

				$template = Template::find($id);
                $file =  Input::file('file') ;
                $inputsFile =  Input::file('inputsFile') ;

                $fileName =  $file->getClientOriginalName() ;
                $inputsFileName =  $inputsFile->getClientOriginalName() ;


            if ($file->isValid()){
                if($file->move('cms_pages/cms_templates/',$fileName))

                {
                	echo 'file moved' ; 
                }
            }

            if ($inputsFile->isValid()){
                if($inputsFile->move('cms_pages/cms_templates/',$inputsFileName))

                {
                	echo 'inputs file moved' ; 
                }
            }

                File::delete($template->urlTemplate );
             	File::delete($template->inputsTemplate );


			     $template->nameTemplate     =   Input::get('name');
				 $template->urlTemplate    =   'cms_pages/cms_templates/'.$fileName ;
				 $template->inputsTemplate    =   'cms_pages/cms_templates/'.$inputsFileName ; 
				 $template->type =   Input::get('typeTemplate');
				 $template->save() ; 


	}




}
