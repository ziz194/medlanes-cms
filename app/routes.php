<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/




Route::get('/', function()
{
	return View::make('admin');
});



	


Route::group(array('prefix'=>'api/'),function(){

//Authentification Controller
Route::get('login/auth','HomeController@Login');
Route::get('login/destroy','HomeController@Logout');
//Page Controller
Route::get('pages/findLanguageUrl/{pages}/{languages}','PageController@findLanguageUrl');
Route::get('pages/getPagesbyLanguage/{languages}','PageController@getPagesbyLanguage');
Route::get('pages/getPagesbyBrandAndLanguage/{brands}/{languages}','PageController@getPagesbyBrandAndLanguage');
Route::get('pages/getPageTemplate/{pages}','PageController@getPageTemplate');
Route::get('pages/getPageUrl/{pages}/{languages}','PageController@getPageUrl');
Route::resource('pages','PageController');
Route::resource('articles','ArticleController');
Route::get('pages/getContentsbyPage/{pages}/{languages}','ContentController@getContentsbyPage');
Route::get('pages/getContents/{pages}','ContentController@getContents');
Route::get('pages/getTextAndPicture/{pages}','ContentController@getTextAndPicture');
Route::get('pages/getQuestionsByPage/{pages}','PageController@getQuestionsByPage');
Route::get('pages/getAllQuestionsByPage/{pages}','PageController@getAllQuestionsByPage');

Route::get('pages/getRelatedPages/{pages}','PageController@getRelatedPages');
Route::get('pages/getAllRelatedPages/{pages}','PageController@getAllRelatedPages');
Route::get('pages/getRelatedSymptoms/{pages}','PageController@getRelatedSymptoms');
Route::get('pages/getRelatedDiagnosis/{pages}','PageController@getRelatedDiagnosis');
Route::get('pages/getRelatedRiskFactors/{pages}','PageController@getRelatedRiskFactors');
Route::get('pages/getRelatedTreatments/{pages}','PageController@getRelatedTreatments');
Route::get('pages/getBlogPosts/{pages}/{tags}/{query}/{number}','PageController@getBlogPosts');
Route::get('pages/getBlogPostsByDate/{pages}/{tags}/{number}/{start}/{end}','PageController@getBlogPostsByDate');
Route::get('pages/totalPosts/{pages}/{tags}/{query}','PageController@totalPosts');
Route::get('pages/getLastBlogPost/{pages}/','PageController@getLastBlogPost');
Route::get('pages/getPostsByTags/{pages}/','PageController@getPostsByTags');
Route::get('pages/activateAllPagesByBrand/{pages}/','PageController@activateAllPagesByBrand');
Route::get('pages/getPagesByDate/{start}/{end}','PageController@getPagesByDate');
Route::get('pages/getTotalPagesByDate/{start}/{end}','PageController@getTotalPagesByDate');
Route::get('pages/getPageByName/{name}/','PageController@getPageByName');
Route::get('pages/getRelatedBlogPosts/{pages}','PageController@getRelatedBlogPosts');
Route::get('pages/getPagesByNameAndTemplate/{idTemplate}/{namePage}','PageController@getPagesByNameAndTemplate');
Route::get('pages/getPageApp/{idPage}/','PageController@getPageApp');











Route::post('pages/createDraft','PageController@createDraft');
Route::get('pages/getIdPageByParameter/{pages}/','PageController@getIdPageByParameter');




//Element Controller
Route::resource('elements','ElementController');
Route::get('elements/getElementUrl/{elements}','ElementController@getElementUrl');
Route::get('elements/getElementsbyLanguage/{languages}','ElementController@getElementsbylanguage');
Route::get('elements/getElementTemplate/{elements}','ElementController@getElementTemplate');
Route::get('elements/getContentsbyElement/{elements}/{languages}','ContentController@getContentsbyElement');
Route::get('elements/getElementByNameAndLanguage/{elements}/{languages}','ElementController@getElementByNameAndLanguage');
Route::get('elements/getElementContents/{elements}','ElementController@getElementContents');
Route::get('elements/getQuestionsByElement/{elements}','ElementController@getQuestionsByElement');
Route::get('elements/updateElement/{elements}/','ElementController@updateElement');
Route::get('elements/getElementsByDate/{start}/{end}','ElementController@getElementsByDate');
Route::get('elements/getTotalElementsByDate/{start}/{end}','ElementController@getTotalElementsByDate');
Route::get('elements/getTotalElementsNumber/{start}/','ElementController@getTotalElementsNumber');



//Update page configuration
Route::get('pages/updatePage/{pages}/','PageController@updatePage');
//Activate-Deactivate page 
Route::get('pages/activatePage/{pages}/','PageController@activatePage');
Route::get('pages/deactivatePage/{pages}/','PageController@deactivatePage');

//Language Controller
Route::resource('languages','LanguageController');
Route::get('languages/getLanguagesByBrand/{brands}','LanguageController@getLanguagesByBrand');


//Tag Controller
Route::resource('tags','TagController');

//Brand Controller
Route::resource('brands','BrandController');

//Users Controller
Route::resource('users','UserController');
Route::get('users/pageCount/{users}/','UserController@pageCount');

//Template Controller
Route::resource('templates','TemplateController');
Route::get('templates/getContent/{templates}','TemplateController@showContent') ;
Route::get('templates/getTemplateByLanguage/{languages}','TemplateController@getTemplateByLanguage') ;
Route::get('templates/getPagesTemplateByLanguage/{languages}','TemplateController@getPagesTemplateByLanguage') ;
Route::get('templates/getElementsTemplateByLanguage/{languages}','TemplateController@getElementsTemplateByLanguage') ;
Route::post('templates/addTemplate','TemplateController@addTemplate') ;
Route::post('templates/updateTemplate','TemplateController@updateTemplate') ;
Route::get('templates/getTemplatesbyBrandAndLanguage/{brands}/{languages}','TemplateController@getTemplatesbyBrandAndLanguage');


//Content Controller
Route::resource('contents','ContentController');
Route::post('contents/addContents','ContentController@addContents') ;
Route::post('contents/addElementContents','ContentController@addElementContents') ;

//Asset Controller
Route::post('assets/uploadAsset','AssetController@uploadAsset') ;
Route::get('assets/getAssetsByDate/{start}/{end}','AssetController@getAssetsByDate');
Route::get('assets/getTotalAssetsByDate/{start}/{end}','AssetController@getTotalAssetsByDate');
Route::get('assets/getTotalAssetsNumber/{start}/','AssetController@getTotalAssetsNumber');
Route::post('assets/updateAsset/','AssetController@updateAsset') ;


Route::resource('assets','AssetController');

//Update Scraped Content
// Route::get('contents/updateScrapedContent/{pages}/','ContentController@updateScrapedContent');
Route::post('contents/updateScrapedContent','ContentController@updateScrapedContent') ;

//Update Content
Route::post('contents/updateContent','ContentController@updateContent') ;

//Update Element Content
Route::post('contents/updateElementContent','ContentController@updateElementContent') ;


 
});

?>